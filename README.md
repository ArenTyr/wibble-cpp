<!--
WIBBLE_VERSION: "2024-03-19 17:50:11"
WIBBLE_DESC_START
  Wibble++ manual
WIBBLE_DESC_END
-->

# Wibble++

  - [Building](#building)
  - [Developer Documentation](#developer-documentation)
  - [Feature Summary](#feature-summary)
    - [Nodes (notes)](#nodes-(notes))
    - [Topics](#topics)
    - [Jotfiles (quicknotes)](#jotfiles-(quicknotes))
    - [Task](#task)
    - [Batch](#batch)
    - [Bookmark](#bookmark)
    - [Backup](#backup)
    - [Daily](#daily)
    - [Stats](#stats)
    - [Sync](#sync)
    - [Transfer](#transfer)
    - [Autofile](#autofile)
    - [Janitor](#janitor)
    - [Server](#server)
    - [React GUI](#react-gui)
    - [Native GUI](#native-gui)
  - [1. Installation](#1.-installation)
  - [2. Initialisation (`bootstrap`)](#2.-initialisation-(`bootstrap`))
  - [3. Create (`create`)](#3.-create-(`create`))
  - [4. Metadata (`metadata`)](#4.-metadata-(`metadata`))
    - [Search Fields / Filtering by metadata fields](#search-fields-/-filtering-by-metadata-fields)
    - [Getting Help](#getting-help)
  - [5. Edit (`edit`)](#5.-edit-(`edit`))
  - [6. Dump (`dump`) and View (`view`)](#6.-dump-(`dump`)-and-view-(`view`))
  - [7. Data (`data`)](#7.-data-(`data`))
  - [8. Delete (`delete`)](#8.-delete-(`delete`))
  - [9. Executing commands (`exec`)](#9.-executing-commands-(`exec`))
  - [10. Jotfiles (`jot`)](#10.-jotfiles-(`jot`))
  - [11. Topics (`topic`)](#11.-topics-(`topic`))
  - [12. Tasks (`task`)](#12.-tasks-(`task`))
  - [13. Bookmarks (`bookmark`)](#13.-bookmarks-(`bookmark`))
  - [14. Autofile (`autofile`), Sync (`sync`), and Janitor (`sync`)](#14.-autofile-(`autofile`),-sync-(`sync`),-and-janitor-(`sync`))
  - [Full Manual](#full-manual)

## Homepage / Documentation

Wibble++ has extremely extensive/thorough documentation. Please visit the [Wibble++ homepage](https://arent.neocities.org/wibble_cpp) to either view the full HTML manual online or download the full PDF manual.

## Building

- Run `make` within the `src/` directory. This will show you the viable build targets/options.

## Developer Documentation

The source code is extensively documented throughout with Doxygen documentation. Install and run `doxygen` in root directory. The generated documentation will be found under `doc/`.

For most users the HTML documentation at `doc/html/index.html` will be most convenient, so point your webbrowser at the generated file.

## Feature Summary

Wibble++ offers a related family of tooling to help you manage all of your files, manage all of your data, create and store structured notes and information, and do so in a plain-text first, format agnostic manner. Wibble++ grew out of Wibble (Perl), and has currently implemented about 90% of Wibble Perl's features, together with countless improvements and many entirely new/expanded features. The remaining small functionality present in Wibble (Perl) but not yet present in Wibble++ will be implemented over the coming weeks.

### Nodes (notes)

**Status**: 100% complete.

[ Pertinent subcommands: `archive`, `create`, `data`, `delete`, `dump`, `edit`, `exec`, `metadata`, `view` ]

- Create/edit Nodes of any plain-text filetype; format agnostic (e.g. md, org, adoc, etc.)
- Create/edit Nodes from templates
- Create/edit Nodes using pre-populated metadata schemas
- Create/edit Nodes using existing files as content
- Create/edit Nodes using custom associated **binary** filetypes (e.g. odt, xlsx, pptx, xcf)
- Create Notes + metadata entirely non-interactively /`batch` mode (scripting) [under development]
- Search/filter Nodes by any field/criteria
- Can handle **1,000,000+** Nodes with ease and with great speed
- Plain-text through and through: *database is plain-text*, can be directly (carefully) edited (if needed)
- Add unlimited arbitrary data/attachments to notes ("Data Directory"); add any file(s) or entire directories
- Add linked/associated external files to Nodes
- Add/define Node relationships (Parent/Sibling/Child)
- Quickly list/view/open linked data/external files
- Archive/Restore Nodes between main database and archive database
- Quickly view and dump Node metadata, content, or both
- Execute arbitrary shell commands or scripts against a single Node or set of Nodes

### Topics

**Status**: 100% complete.

[ Pertinent subcommand: `topic` ]

- Create date-sequenced, categorised "containers" of related notes/writing/information/files/materials
- Similar to a Node, except everything is added as separate "entries", and is designed for recurrent materials
- Used for managing/storing related information all on one particular theme (i.e. a 'topic')
- For example: `work:AGENDA_NOTES`, `work:PROJECT_A`, `recipes:Italian`, `recipes:French`
- Instantly search, filter, and find entries; add unlimited data files/attachments as separate 'data' entries
- Migrate/move entries from Topic to Topic
- Execute arbitrary shell commands or scripts against a single Topic entry or set of entries within a Topic

### Jotfiles (quicknotes)

**Status**: 100% complete.

[ Pertinent subcommand: `jot` ]

- Create "jotfiles": instantly add/view/delete to a given single file(s) with sections, directly from the console
- Rapidly search through Jotfile to find sections

### Task

**Status**: 100% complete.

[ Pertinent subcommand: `task` ]

- Powerful task and project management
- Quickly add and mark done todos/tasks from the command line
- Filter tasks by due date, priority, and category
- Assign tasks into task groups with completion statistics and ability to clone for recurrent tasks
- Turn a task into a 'project' by adding a description, arbitrary linked files, and managed 'project files'
- Associate and link Nodes, Topics, and Jotfiles to a given task

### Batch

**Status**: 100% complete.

[ Pertinent subcommand: `batch` ]

- Automate and non-interactively create new Nodes
- Automate and non-interactively create new Topics and Topic entries
- Automate and non-interactively create Jotfiles and Jotfile entries
- Automate and non-interactively create Task groups & Task items
- Automatically add data to Nodes, Topics, and Task items
- Log all operations into a detailed log file
- Ideal for scripting/automating all of Wibble++'s fundamental operations

### Bookmark

*Status*: 100% complete.

[ Pertinent subcommand: `bookmark` ]

- Rapidly bookmark and quickly access favourited Nodes, Jotfiles, Topic entries, or Task entries
- Save and rapidly access any particular Wibble command
- Save and rapidly access any particular general shell command or shell script
- Create your own command categories and shell/script bookmarks, giving you an effortlessly expandable CLI "menu" system

### Backup

**Status**: 100% complete.

[ Pertinent subcommand: `backup` ]

- Effortlessly and instantly backup all of your precious metadata, Nodes, Topics, Jotfiles, data, everything all in one command.
- Stored result in a compressed archive (tar.xz, tar.gz, 7z, or zip).

### Daily

**Status**: 100% complete.

[ Pertinent subcommand: `daily` ]

- Create and manage "daily" scripts that can be arbitrarily run individually or en masse
- Designed to supplement crontab jobs

### Data

**Status**: 100% complete.

[ Pertinent subcommand: `data` ]

- Copy, move, or link existing files/entire directory trees into Nodes
- Smoothly integrate all of your external files into your Node knowledge system
- Rapidly access/filter individual external/project files
- Never lose/keep all project files neatly organised in a contextually relevant location
- View, tree, or quickly cd into the relevant data directory

### Stats

**Status**: 0% complete.

[ Not implemented yet. ETA: January 2025 ]

[ Pertinent subcommand: `stats` ]

- View dump of all statistics; sorted tag counts, sorted file type counts, dump of all titles

### Sync

**Status**: 100% complete.

[ Pertinent subcommand: `sync` ]

- Streamlined and simple but effective file synchronisation
- Works on a semi-curated principle: inject timestamps and manually adjust to control/trigger updates
- View file differences and synchronise any arbitrary directory pair

### Transfer

**Status**: 60% complete.

[ Currently being implemented. ETA: January 2025 ]

[ Pertinent subcommand: `transfer` ]

- Export existing Nodes, Topics or Task items from Wibble++ to an export directory
- Import existing Nodes, Topics or Task items from an export directory into Wibble++
- Allows you to transfer invidually selected Nodes, Topics, or Task items, retaining all data and attachments associated with it

### Autofile

**Status**: 100% complete.

[ Pertinent subcommand: `autofile` ]

- Never lose or have to think about how to carefully file away documents ever again
- Automatically file into a structured hierarchy, with an associated brief descriptive title/metadata
- Instantly open or access any file via a rapid list or search

### Janitor

**Status**: 100% complete.

[  Pertinent subcommand: `janitor` ]

- An alternative for those ugly and annoying XDG-Desktop compliant "Trash" managers
- Instantly "trash" files into a specific trash directories
- Easily restore any such file into any desired directory
- Easily find/retrieve any trashed file
- Easily obliterate/permanently delete trashed files
- Acts as a drop-in replacement for 'rm' with suitable bash aliases, providing a much better (and less dangerous!) rm experience

### Server

**Status**: 0% complete. (The `React GUI` is ~95% complete, needs very minor porting from Wibble-Perl version).

[ Not implemented yet. ETA: Sometime 2025 ]

[  Pertinent subcommand: `server` ]

- Run Wibble++ as a JSON server, easily retrieving records and results/commands via RESTful transfers
- Provides a `React GUI`

### React GUI

**Status**: 95% complete. (Needs some very minor porting from Wibble-Perl version due to minor underlying database changes).

- Wibble GUI: React based in-browser GUI viewer
- Wibble GUI: View markdown, asciidoc, and org-mode format notes rendered beautifully
- Wibble GUI: View source code files rendered beautifully with syntax highlighting
- Wibble GUI: Effortlessly search by any combination of fields, show all results
- Wibble GUI: View listing of all note data, filetree, and direct links to all related notes
- Wibble GUI: View a note graph showing all notes/notes and connected/related notes

### Native GUI

**Status**: 0% complete.

[ Not implemented yet. ETA: Sometime 2025? ]

- Plan is to implement a native C++ GUI using FLTK.

# Tour / Quickstart

The full manual covers each command/feature in detail, but often the fastest way to understand and grasp the functionality of a piece of software is with a summary tour. 

So let us begin.

## 1. Installation

For detailed installation instructions, see the previous section, "installation". Suffice to say it is assumed you have a fully working `Wibble++` installation, and the binary is on your command-line `PATH`/is visible. 

## 2. Initialisation (`bootstrap`)

The first thing we need to do is build a `Wibble++` configuration file, since specifying every single configuration parameter via command line switches, though possible, is awkward and highly impractical to supply on every single invocation of the command (though, if you really wanted to, you could setup a long bash alias to do so...). The configuration file permanently saves all of these settings.

For a brand new installation, the very first thing to run is:

`wibble++ bootstrap`

Follow the guided prompts and specify the relevant options. If you just want to give Wibble++ a *try*, you can specify something like `/tmp/testing` for your Wibble++ root directory, and all of the generated and saved files will be stored there, and will not persist between reboots. Once you've decided that Wibble++ is something you do want to regularly use, just remember to delete this testing configuration file under `~/.config/wibble/wibble.toml` and run `bootstrap` again and this time point it at a proper permanent storage directory, e.g. `~/Documents/wibble` or suchlike. Forgetting to do and persisting valuable information into a temporary location under somewhere like `/tmp`, only to discover it is all gone after a reboot, would be a painful headache...

```
$ wibble++ bootstrap
→ Checking for operational 'ripgrep' command in PATH...
✓ Working ripgrep installation found.
───────────────────────────────────────────────────────
NOTE: 'Backup' directory (question 2) should be OUTSIDE root Wibble directory.
───────────────────────────────────────────────────────

━━━━━━━━━━━━━━━━━━━━ [   CONFIG   ] ━━━━━━━━━━━━━━━━━━━━
[ 1/10] Input root Wibble directory  :  ~/Documents/wibble
[ 2/10] Input backup Wibble directory:  ~/Documents/backup/wibble
[ 3/10] Input autofile directory     :  ~/Documents/autofile
[ 4/10] Command line editor command  :  vim
[ 5/10] GUI editor                   :  leafpad
[ 6/10] File manager                 :  pcmanfm
[ 7/10] Archive tool                 :  xarchiver
[ 8/10] Dump tool                    :  cat
[ 9/10] Pager command                :  bat
[10/10] View tool                    :  gxmessage
───────────────────────────────────────────────────────

━━━━━━━━━━━━━━━━━━━━ [ GENERATED ] ━━━━━━━━━━━━━━━━━━━━
Base directories & database files
───────────────────────────────────────────────────────
Main Wibble directory    : /home/aren/Documents/wibble
Backup Wibble directory  : /home/aren/Documents/backup/wibble
Autofile directory       : /home/aren/Documents/autofile
Wibble database directory: /home/aren/Documents/wibble/db
Main Wibble database     : /home/aren/Documents/wibble/db/main.wib.db
Archived Wibble database : /home/aren/Documents/wibble/db/ark.wib.db
───────────────────────────────────────────────────────
Core directories
───────────────────────────────────────────────────────
Bookmarks directory      : /home/aren/Documents/wibble/bookmarks
Jotfile directory        : /home/aren/Documents/wibble/jotfiles
Schemas directory        : /home/aren/Documents/wibble/schemas
Scripts directory        : /home/aren/Documents/wibble/scripts
Tasks directory          : /home/aren/Documents/wibble/tasks
Templates directory      : /home/aren/Documents/wibble/templates
Temporary/cache directory: /home/aren/.cache/wibble
Topics directory         : /home/aren/Documents/wibble/topics
───────────────────────────────────────────────────────
Executable commands
───────────────────────────────────────────────────────
Editor command           : vim
Dump tool command        : cat
GUI editor command       : leafpad
File manager command     : pcmanfm
Pager command            : bat
View tool command        : gxmessage
XDG open command         : xdg-open
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Create configuration file with above settings? (y/n) > y
✓ File successfully [over-]written: /home/aren/.config/wibble/wibble.toml
✓ Configuration file successfully written out to /home/aren/.config/wibble/wibble.toml.
```

## 3. Create (`create`)

At this point, we have Wibble++ installed and configured, but obviously we have no data of any kind; no notes, no tasks, no topic entries, nothing. So let us first create a `Node`, i.e. a specific note of a defined (plain-text) filetype, with associated metadata together with a "data directory" (if you end up adding data "into" the note.

Run 

`wibble++ create`

And follow the prompts. Only the "title" and "filetype" fields are *absolutely* required; the rest are there for your benefit to help you find, identify and filter the Node accordingly. Do not enter a '.' symbol when asked for the filetype/extension; this is implied. After running the command, the new Node will be opened using your configured command line editor. If you wish to instead have it open using your chosen GUI editor, pass in the `--gui-editor` or `-G` flag, e.g. `wibble++ create --gui-editor`. Now clearly the filetype (extension) means you can have the Node be of whatever type you like: enter `md` if you're intending on creating/editing a markdown file, `adoc` for asciidoc, `org` for `org-mode`, or good old `txt` for a straightforward ordinary text file. You could even create your own custom filetype. You'll notice that the newly created file has a long hashed identifier ending in your chosen file type (e.g. `2200317c-8535-439f-9190-66d6177d6577.md`).

```
$ wibble create
→ Verifying database integrity...
✓ Clean database file, passed integrity check.
✓ 1 record(s) present in database.
━━━━━━━━━━━━━━━━━━━━ [   CREATE   ] ━━━━━━━━━━━━━━━━━━━━
Title     *:  An example first Node
Filetype  *:  md
Description:  This is just an example
Project    :  Junk
Tags       :  #deleteme #example
Class      :  
Custom     :  

Add new/additional KeyPairs? (y/n) >  n
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


━━━━━━━━━━━━━━━━━━━━ [  CONFIRM  ] ━━━━━━━━━━━━━━━━━━━━
Date       : Mon, 08 Apr 2024 16:41:06 +0100
Title      : An example first Node
NoteId     : 2200317c-8535-439f-9190-66d6177d6577
Filetype   : md
Description: This is just an example
Project    : Junk
Tags       : #deleteme #example
Class      : ___NULL___
DataDirs   : ___NULL___
KeyPairs   : ___NULL___
Custom     : ___NULL___
LinkedIds  : ___NULL___
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Create record? (y/n) > y
→ Set up path.
→ No default template exists, proceeding with blank note.
✓ Record successfully added to database: /home/aren/Documents/wibble/db/main.wib.db
✓ New record successfully inserted.
→ Opening note 2200317c-8535-439f-9190-66d6177d6577.md from main database.
→ Executing command: vim /home/aren/Documents/wibble/notes/md/Junk/22/2200317c-8535-439f-9190-66d6177d6577.md
✓ Command successfully executed on record/file.
✓ Note or schema/template creation complete.
```

## 4. Metadata (`metadata`)

Save your Node (note) and quit your editor. You're back at the prompt. Now what? How do we quickly pull up and edit that file again? Well, perhaps the first thing to do is to inspect the metadata that has been saved as a result of your inputs after running `create`. The question is, how do we retrieve that particular Node?

Well, at the moment, we only have the *one* Node in our database, because that is all we've added. Consequently, if you run:

`wibble++ metadata`

you'll immediately pull that exact Node straight up, because it is the *only* available candidate, so will be automatically selected.

```
$ wibble metadata
━━━━━━━━━━━━━━━━━━━━ [  METADATA  ] ━━━━━━━━━━━━━━━━━━━━
→ No search criteria specified - displaying ALL records.
→ Verifying database integrity...
✓ Clean database file, passed integrity check.
✓ 1 record(s) present in database.
✓ Single candidate/match, automatically selecting.
━━━━━━━━━━━━━━━━━━━━ [   NOTE    ] ━━━━━━━━━━━━━━━━━━━━
Date       │ Mon, 08 Apr 2024 16:41:06 +0100
Note ID    │ 2200317c-8535-439f-9190-66d6177d6577
Title      │ An example first Node
Filetype   │ md
Description│ This is just an example
Project    │ Junk
Tags       │ #deleteme #example
Class      │ ___NULL___
DataDirs   │ ___NULL___
KeyPairs   │ ___NULL___
Custom     │ ___NULL___
LinkedIds  │ ___NULL___
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
```

As you can see, it shows you all of the field information based on your inputted values, together the field values that are automatically generated. However, having one Node is clearly non-typical, and once you've got a bunch of Nodes, let alone 5,000 or 50,000 Nodes, the ability to rapidly find and select and entry becomes increasingly important.

Let us first add another Node so that we have more than one singular candidate.

Run `wibble++ create` and input whatever you like, then save/quit from your editor to finish creating the additional Node. So now we have two. 

Run `wibble++ metadata` again, and this time instead of the one singular candidate being automatically selected, you'll instead be presented with an interactive numbered list. Simply input the number of the record you want, and press enter.

```
$ wibble++ metadata
━━━━━━━━━━━━━━━━━━━━ [  METADATA  ] ━━━━━━━━━━━━━━━━━━━━
→ No search criteria specified - displaying ALL records.
→ Verifying database integrity...
✓ Clean database file, passed integrity check.
✓ 2 record(s) present in database.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[1] ┃ ▒░▒▒░▒▒░▒ ┃ (5906) An uninteresting text
[2] ┃ ▒░▒▒░▒▒░▒ ┃ (2200) An example first Node
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Select choice [1:2] > 1

━━━━━━━━━━━━━━━━━━━━ [   NOTE    ] ━━━━━━━━━━━━━━━━━━━━
Date       │ Mon, 08 Apr 2024 16:07:04 +0100
Note ID    │ 59064450-e231-4366-b5d7-59959a8e2eba
Title      │ An uninteresting text
Filetype   │ txt
Description│ Just for demonstration
Project    │ General
Tags       │ ___NULL___
Class      │ ___NULL___
DataDirs   │ ___NULL___
KeyPairs   │ ___NULL___
Custom     │ ___NULL___
LinkedIds  │ ___NULL___
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

```

The metadata command also allows you to define and add Node relationships, allowing you to "group" related Nodes together. These are added via the various `--rel-add-...` commands. See the manual for detailed usage instructions on defining and working with relationships. 

### Search Fields / Filtering by metadata fields

At this point, we need an aside regarding how we filter by search fields. Running `wibble++ metadata` without any further options is equivalent to running the command `wibble++ metadata --everything` (or `-1` in short format). It basically shows you all defined Node records, ordered by creation date/time, with the most recent entries at the bottom of the list, i.e. closest to your input prompt. This means that your most recent/fresh Nodes are always visually nearest to your input selector, which makes sense given that you're probably more likely to want to pull up Nodes you've been working on recently, most of the time.

Nevertheless, a full list, whilst acceptable if you have a few dozen entries, starts to become somewhat annoying/unusable to visually scan once you have tens or indeed hundreds of entries, so clearly we want a way of filtering the list. Fortunately we can easily do so. By far the most common/likely field you'll filter by is the "title". So how do we filter by that? Easy:

`wibble++ metadata --title "Zutty terminal"` [A]

Note that the quotation marks for the parameter supplied to `--title` are only needed if you've got special characters (e.g. spaces or symbols). For example, we could equally well have searched for 

`wibble++ metadata --title Zutty` [B]

which is obviously a less restrictive search, since it will attempt to sub-string match on "Zutty" rather than "Zutty terminal". For example, suppose we had three Nodes, with the following titles:

1. Zutty - notes and configuration 
2. Zutty terminal - design features
3. Zutty my config file

So the first command (A) would match Node number 2 **only**; the second command (B) would match all three. Whether you want the more or less restrictive search, of course, depends, but the point is you can alter your search string accordingly based on how much you want to narrow results down. Meanwhile, if we entered 

`wibble++ metadata --title "Zutty - website and homepage details"` [C]

we would get no matches, since nothing matches the criteria supplied.

So we can see how we can easily restrict Node results by the value we specify for "title". Now naturally the same filtering process can be applied to **any** of the fields you see displayed on the metadata display of a record. They can also be combined. For example:

`wibble++ metadata --id dd77`

Would match (highly likely to be singular) any Node starting with NodeId "dd77...",

`wibble++ metadata --type md`

would display any Node matches that have filetype/end in extension `.md`, and 

`wibble++ metadata --type md --title "Zutty" --tags "#linux"`

would only match Nodes of filetype markdown, are titled/contain "Zutty" in the title, and have the presence of the tag "#linux".

Of course, sometimes you want to filter by many/multiple fields, so it can easiest to simply run:

`wibble++ metadata --menu`

Try it.

Meanwhile, the `KeyPairs` field/functionality is more elaborate, and essentially gives you an expandable unlimited list of available "key:value" pairings to search and filter by, for any given record. See the manual for detailed usage. Key pairs are useful for "advanced" usage, where you need to be able to add, categorise, filter, and search by a very specific or detailed set of search criteria, and potentially store very amounts of records that all can be precisely differentiated by very fine-grained metadata.

### Getting Help

The outline above shows you the basic methodology for filtering using *any* of the core Node commands (`archive`, `create`, `data`, `delete`, `dump`, `edit`, `exec`, `metadata`, `view`), the syntax and usage for the searching criteria is exactly the same for each command. In fact they all obey the same design/usage concept: first filter/provide the list of Node candidates, *then* select the candidate, then run the command. (Various `--batch` switches also extend this concept to allow doing something across a *set* of matches, but the essential principle is the same).

Now that we understand this, clearly we also want to know what are options are. So we ask for help:

`wibble++ metadata --help`

shows us the help that is specific to the `metadata` command. By contrast, running 

`wibble++ --help`

shows you the top-level help instructions, including the list of all of the sundry `wibble++` sub-commands.

Let's try another switch; try running

`wibble++ metadata -1 --md-id`

Select an entry and see the result. You should end up with something like `ff777da3-a0d8-4cd5-b4c7-61fbd7551c7` dumped to terminal. (The lack of newline is deliberate, to make this result convenient for use in scripts; it makes commands like (do not attempt to run this!) 

`wibble exec --exec-cmd "ls -lh" --id $(wibble++ metadata --md-id --id dd77 | tail -n 1) | tail -n 2 | head -n 1`

possible. On my set of Nodes, running the above outputs 

`-rw-r--r-- 1 aren aren 716 Mar 19 01:43 /home/aren/doc/wibble/notes/txt/Personal/dd/dd777da3-a0d8-4cd5-b4c7-61fbd7551c78.txt`

But we're jumping ahead of ourselves, and in any case there are easier ways of obtaining the underlying Node file with the built-in functionality. As you will see, each command typically has many additional switches that provides a great variety of facilities. Fortunately, you only need to know a small subset of them to get up and running/using Wibble++ effectively; the rest you will learn with time and use.

## 5. Edit (`edit`)

So we know how to create Nodes, and how to retrieve and view their metadata. In fact, we know the basic principles for how to search/filter any result set. So all that remains, at least in terms of the fundamentals, for working with Nodes, are the remaining commands.

Unsurprisingly, to edit an existing Node, we use the 'edit' command. Like the create command, passing in `-G` or `--gui-editor` will open it up for editing using your GUI rather than command line editor.

Anyway, run

`wibble++ edit`

to open an interactive list of all Nodes to edit, or something like 

`wibble++ edit --title "Linux"`

to present a list/edit any Node with "Linux" in the title. As before, all of the search/field filters are available. Experiment with them, or use

`wibble++ edit --menu`

Needless to say, to make the exercise more interesting, you might want to quickly create a bunch of junk Nodes first so you've got some set of results to filter by and play around with.

## 6. Dump (`dump`) and View (`view`)

These two closely related commands simply output the content of the Node. Which program they use to do so depends on how you've configured Wibble++. On my configuration, I have the following:

```toml
cli_editor              = "nvim"
dump_tool               = "cat"
fm_tool                 = "zutty -e mc"
gui_editor              = "emacsclient -n"
pager                   = "bat"
view_tool               = "gxmessage -file"
xdg_open_cmd            = "cleanopen"
```

As you can see, in my configuration, `dump` will call good old `cat`, immediately dumping the content to terminal, and my `view` tool is `gxmessage` (with necessary `-file` switch to make it read in content from an existing filename). 

Usage is similar to `metadata` and `edit`.

`wibble++ dump`

Will bring up your list of Nodes, select an entry and the file content will be immediate `cat` to  your STDOUT/terminal. Of course you can use whatever replacement for `cat` as you wish; for example `batcat` would be a viable option, if you have that installed.

`wibble++ view --title "Linux"`

will bring up a selector (or select automatically, if only one match) and then open up your Node in your view tool, for example `gxmessage`. If you prefer, you could assign another editor to the `view` command, effectively give you a choice of three immediately accessible editors to work on your Node you so wish; the first two via `edit` with the the optional `--gui-editor` flag, then then third one via `view`. Or even a fourth one via `dump` if instead of `cat` you want that to pass the filename to an editor as well. That all seems redundant though. The idea is:

- Use `edit` for editing (calling an editor command, configured terminal and GUI options)
- Use `dump` for "dumping" (calling a `cat` or similar command)
- Use `view` for viewing, using whatever terminal or graphical viewer command of your choice

## 7. Data (`data`)

The `data` command allows you to add/store any number of external files or entire directories "into" the Node, as an "attachment". Each Node has its own specific "data directory". To add a file or directory by **copying** it, use the `--copy-file`/`-C` switch. To add a file/directory by **moving** it, use the `--move-file`/`-M` switch. You can also **link** to a file/directory by adding a link; this is useful for associating any arbitrary number of potentially relevant files/directories to a given Node, that exist anywhere on the filesystem. Further switches exist to list the content of the data directory, run the `tree` command on the data directory (ensure you have the command installed; usually it is in a package simply called `tree`), directly open data files, or obtain the path/location of the data directory.

To add a file by moving it, specify/select your Node together with relevant switches:

`wibble++ data --copy-file ~/Downloads/th-1692580526.jpg --title "A test Node"`

You can verify the file has been copied like so:

`wibble++ data --ls-dd --title "A test Node"`

## 8. Delete (`delete`)

Use of `delete`, unsurprisingly, permanently removes a Node. The procedure is the usual: identify the candidate Node, then (after confirmation), execute the command, in this case, to remove the underlying Node file and update the database. The other important factor to be aware of is that deletion of a Node also deletes any attached Node data added via the `data` command, if it exists:

```
$ wibble++ delete --title "A test Node"
→ [1] Filtering by notes with Title      : A test Node
→ Verifying database integrity...
✓ Clean database file, passed integrity check.
✓ 1 record(s) present in filtered results.
✓ Single candidate/match, automatically selecting.
━━━━━━━━━━━━━━━━━━━━ [   NOTE    ] ━━━━━━━━━━━━━━━━━━━━
Date       │ Mon, 08 Apr 2024 15:56:59 +0100
Note ID    │ c4c75ecb-7608-49d0-830c-47611701f439
Title      │ A test Node
Filetype   │ txt
Description│ ___NULL___
Project    │ General
Tags       │ ___NULL___
Class      │ ___NULL___
DataDirs   │ [ DATA ]
KeyPairs   │ ___NULL___
Custom     │ ___NULL___
LinkedIds  │ ___NULL___
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

WARNING. Note has data directory.
Directory contains 24K of data.
Deletion operation will remove entire data directory: 
/home/aren/Documents/wibble/note_data/c4/c4c75ecb-7608-49d0-830c-47611701f439

Permanently delete above record?
Type 'delete' to confirm >  delete
✓ Record c4c75ecb-7608-49d0-830c-47611701f439 removed successfully
✓ Successfully removed data directory: /home/aren/Documents/wibble/note_data/c4/c4c75ecb-7608-49d0-830c-47611701f439

```

## 9. Executing commands (`exec`)

It is possible to execute arbitrary commands on the given Node file, and, since the metadata/data commands allow us to obtain the location/details of the data directory, by extension we can easily run arbitrary commands across the contents of the data directory as well.

To execute a command on a Node, find the Node, then supply the command "template", or set of commands you wish to use on the given Node files, as a parameter to `--exec-cmd` switch. For example:

`wibble++ exec --exec-cmd "ls -lh ___FILE___; echo '-----'; cat '___FILE___'; echo '-----'" --title "An uninteresting text"`

The above sequence lists the underlying file information, then outputs the file to console with a small horizontal line divider as header/footer. You will note that the special placeholder `___FILE___` gets substituted for the actual underlying filename of the Node.

## 10. Jotfiles (`jot`)

So far the examples we have described above all pertain to working with Nodes. However, Wibble++ has other types of special file, as well. One of those is a "Jotfile" (others are "Topics", "Tasks", and "Bookmarks").

Jotfiles, via the `jot` command give you an unlimited number of available "scratch" or quick note files, where you add entries (all into the one underlying file), without having to even invoke your editor. It is designed for literally quickly dumping URLs, things you want to remind yourself, code snippets, etc., all with minimal context-switching and interruption. It doesn't even use your editor, simply capturing and dumping the information directory from your console. Quickstart:

1. Create at least one jotfile via `wibble++ jot --create`
2. Add an entry via `wibble++ jot --add`; input title, then input content and press control-D to save/exit.
3. View a given entry via `wibble++ jot --view`.
3. Delete a given entry via `wibble++ jot --delete`.

## 11. Topics (`topic`)

The `topic` command provides the ability to define named topics, which exist under an organised name and group. The purpose is to provide a "container" through which you can add repeated entries, including data. The notion is most simply understood as a mechanism that allows you to keep adding something more-or-less equivalent to a Node, but of the **same repeated category/concept/or type of thing**. 

Examples of such a usage scenario would be for blog post entries, "daily standup" work notes, a "research file" for gathering all notes, papers, and materials on a particular ongoing project, an exercise log/diary, an inventory/log of actual work/tasks done on particular days and hours spent on a project: essentially anything that would benefit from consisting of time-ordered repeated "entries" under a theme, nicely indexed and easily searchable. The topic functionality is fairly complex and has many rich features, so refer to the full manual, but as a short-list, the following commands get you started:

1. Create a named topic via `wibble++ topic --create`
2. Add an entry to a topic via `wibble++ topic --add`
3. Edit entries via `wibble++ topic --edit`
4. Display entries via `wibble++ topic --dump`
5. Inventory the contents via `wibble++ topic --inventory-topic`
6. Add data files using `wibble++ topic --copy-data-file` or `--move-data-file`
7. Delete entries via `wibble++ topic --delete`

## 12. Tasks (`task`)

The `task` functionality  provides something that, on the one hand, is as simple as a set of "to-dos", with priorities, due-dates, and categories, and part of a group. On the other hand, they can be "extended" into something that provides something much closer to entire project management. Nevertheless, the minimal viable usage of it would be the following:

1. Add a task item via `wibble task --add`.
2. List open tasks via `wibble task --list`, or via the group-orientated view with `--group` instead.
3. Complete tasks via `wibble task --mark`, or from the group view menu `wibble task --group`.

Selecting a given task will bring up the interactive menu where you can see the large range of possibilities for adding/associating all sorts of files, associating Nodes, Jotfiles, Topics, and for adding "project" files. Refer to the manual for details.

## 13. Bookmarks (`bookmark`)

The `bookmark` functionality enables you to easily "favourite" given Nodes, Topic entries, Jotfiles, or even Task items. Furthermore, it also allows you to save any general shell command, including putting them into custom groups, in effect giving you a powerful menu system and an alternative to endless shell-aliases. Run the command `wibble++ bookmark` and explore the interactive menus to understand use, or refer to the manual.

## 14. Autofile (`autofile`), Sync (`sync`), and Janitor (`sync`)

Wibble++ also provides rich functionality for managing and neatly organising your files (`autofile`), provides a semi-curated elegant method of synchronising (textual) files between directories (`sync`), and offers a superior "trash" bucket/safe deletion wrapper (`janitor`). Refer to the relevant manual sections.

## Full Manual

This quickstart/tour is just an extremely brief surface level skim of all of the Wibble++ functionality. Please see the [full manual](https://arent.neocities.org/wibble_cpp) for in-depth usage instructions/details documenting its full capabilities. 
