/**
 * @file      cmd_daily.cpp
 * @brief     Handle "daily" subcommand.
 * @details
 *
 * This class has the dispatching logic for the "daily" command.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_daily.hpp"
#include "wibbledaily.hpp"
#include "wibbleexecutor.hpp"

/**
 * @brief Dispatching logic for the "daily" functionality of Wibble.
 * @param OPTS package/general configuration options struct
 * @param DAILY command line options struct for "daily"
 */
void wibble::WibbleCmdDaily::handle_daily(pkg_options& OPTS, daily_options& DAILY)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);
    WibbleDaily wd;

    if(DAILY.add)
    {
        std::string do_add = wc.console_write_prompt("Add new daily script? (y/n)", "", "");
        if(do_add == "y" || do_add == "Y") { wd.add_daily(OPTS, wc);                        }
        else                               { wc.console_write_error_red("User cancelled."); }
    }
    else if(DAILY.edit)                    { wd.edit_or_del_daily(OPTS, wc, false);         }
    else if(DAILY.ls)                      { wd.ls_daily(OPTS, wc);                         }
    else if(DAILY.del)                     { wd.edit_or_del_daily(OPTS, wc, true);          }
    else if(DAILY.exec_ds != "UNSET")      { wd.exec_script(OPTS, wc, DAILY.exec_ds);       }
    else
    {
        wc.console_write_success_green("No operation specified, defaulting to 'ls'.\n");
        wd.ls_daily(OPTS, wc);
    }
} 
