/**
 * @file      wibblerecord.hpp
 * @brief     Represents abstraction of a WibbleRecord (Node) record.
 * @details
 *
 * This class provides the data structure for the main Wibble record/Node
 * entry, together with various getters/setters, and core utility functions
 * for generating unique entry hashes and working with datetimes.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iomanip>

// UUID generation uses CPU instruction set for efficient/random generation
#include "external/uuid_v4.h"
#include "external/endianness.h"

#include "external/rang.hpp"

#include "wibblerecord.hpp"
#include "wibblesymbols.hpp"

//https://stackoverflow.com/questions/308390/convert-a-string-to-a-date-in-c
/**
 * @brief Converts a string representation of a date (+ optional time) to a std::time_t/epoch time value.
 * @param datestring String with date[-time] value to parse
 * @param ret_1900 Flag controlling whether to return "error" datetime of 1900-01-01 or current datetime
 * @param gen_time Flag controlling whether string also contains hh:mm:ss time value to convert, for specific time generation
 */
std::time_t wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str(const std::string& datestring, const bool ret_1900, const bool gen_time)
{
    std::time_t curr_t{std::time(nullptr)};
    struct std::tm* timeinfo; // pointer to shared static/global structure (ugly C legacy)
    //timeinfo = std::localtime(&curr_t);
    // mapping should be referenced against UTC time
    timeinfo = std::gmtime(&curr_t);

    try
    {

        std::size_t year_split = datestring.find("-");
        std::size_t month_split = datestring.find("-", year_split + 1);

        // preliminary guard to check that format is xxxx-xx-xx for YYYY-MM-DD, or YYYY-MM-DD hh:mm::ss
        if(! gen_time) { if(year_split != 4 || month_split != 7 || datestring.length() != 10) { throw std::invalid_argument("Bad date input"); } }
        else           { if(year_split != 4 || month_split != 7 || datestring.length() != 19) { throw std::invalid_argument("Bad date input"); } }
        
        std::string year_s = datestring.substr(0, year_split);
        std::string month_s = datestring.substr((year_split + 1), 2);
        std::string day_s = datestring.substr((month_split + 1), std::string::npos);

        int year = std::stoi(year_s);
        int month = std::stoi(month_s);
        int day = std::stoi(day_s);

        //std::cerr << "DEBUG: datestring = " << datestring << " values -> year = " << year << ", month = " << month << ", day = " << day << std::endl;
        if(year < 1900 || year > 2100 || month < 1 || month > 12 || day < 1 || day > 31) { throw std::invalid_argument("Bad date input"); }

        timeinfo->tm_year = year - 1900;
        timeinfo->tm_mon = month - 1; // months are 0-indexed in underlying shared global struct
        timeinfo->tm_mday = day;
        if(gen_time) // if true, string should also have hh:mm:ss time to convert, e.g. value such as "2024-03-13 19:12:33"
        {
            std::size_t time_component = datestring.find(" ");
            std::string hour_s    = datestring.substr((time_component + 1), 2);
            std::string minute_s  = datestring.substr((time_component + 4), 2);
            std::string seconds_s = datestring.substr((time_component + 7), 2);
            //std::cout << "DEBUG: got time components: " << hour_s << ":" << minute_s << ":" << seconds_s << std::endl;
            int hour    = std::stoi(hour_s);
            int minute  = std::stoi(minute_s);
            int seconds = std::stoi(seconds_s);

            if(hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59 && seconds >= 0 && seconds <= 59)
            {
                timeinfo->tm_hour = hour;
                timeinfo->tm_min = minute;
                timeinfo->tm_sec = seconds;
                //std::cerr << "Valid time specification: " << datestring << std::endl;
            }
            else
            {
                //std::cerr << "Invalid time specification: " << datestring << std::endl;
                throw std::invalid_argument("Bad time input"); 
            }
        }
        std::time_t t_succ = std::mktime(timeinfo);
        timeinfo = std::localtime(&t_succ);
        return t_succ;
    }
    catch(std::exception& ex) // conversion failed
    {
        // are we returning 1900-01-01 or current datetime in case of error?
        if(ret_1900) 
        {
            //std::cerr << "Bad timestring. Returning 1900-01-01..." << std::endl;
            // option to return 1900-01-01 00:00:01 as default "error" date[-time]
            timeinfo->tm_year = 0;
            timeinfo->tm_mon  = 0; // months are 0-indexed..
            timeinfo->tm_mday = 1; // ...but days are not
            timeinfo->tm_hour = 0;
            timeinfo->tm_min  = 0;
            timeinfo->tm_sec  = 1;
            std::time_t t_err = std::mktime(timeinfo);
            timeinfo = std::localtime(&t_err);
            return t_err;
        }
        else
        {
            //std::cerr << "Bad timestring. Using current time." << std::endl;
            std::time_t curr_t = std::time(nullptr);
            timeinfo = std::localtime(&curr_t);
            return curr_t;
        }
    }
}

/**
 * @brief Generate a UUIDv4 hash.
 */
std::string wibble::WibbleRecord::generate_UUIDv4()
{
    UUIDv4::UUIDGenerator<std::mt19937_64> uuidGenerator;
    UUIDv4::UUID uuid = uuidGenerator.getUUID();
    return uuid.str();
}

/**
 * @brief Generate a date in RFC5322 format.
 * @param input_time String representing either a custom datetime, or "" to generate it based on datetime now
 */
std::string wibble::WibbleRecord::generate_date_rfc5322(const std::string& input_time)
{
    std::time_t curr_t = std::time(nullptr);
    if(input_time != "") { curr_t = get_time_from_YYYY_MM_DD_str(input_time); }
    char tm_str[60];
    std::string curr_date;
    std::strftime(tm_str, sizeof(tm_str), "%a, %d %b %Y %T %z", std::localtime(&curr_t));
    return std::string(tm_str);
}

/**
 * @brief Generate a 4 digit year, YYYY.
 * @param input_time String representing either a custom datetime, or "" to generate it based on datetime now
 */
std::string wibble::WibbleRecord::generate_date_YYYY(const std::string& input_time)
{
    std::time_t curr_t = std::time(nullptr);
    if(input_time != "") { curr_t = get_time_from_YYYY_MM_DD_str(input_time); }
    char tm_str[5];
    std::string curr_date;
    std::strftime(tm_str, sizeof(tm_str), "%Y", std::localtime(&curr_t));
    return std::string(tm_str);
}

/**
 * @brief Generate a 2 digit month, MM.
 * @param input_time String representing either a custom datetime, or "" to generate it based on datetime now
 */
std::string wibble::WibbleRecord::generate_date_MM(const std::string& input_time)
{
    std::time_t curr_t = std::time(nullptr);
    if(input_time != "") { curr_t = get_time_from_YYYY_MM_DD_str(input_time); }
    char tm_str[5];
    std::string curr_date;
    std::strftime(tm_str, sizeof(tm_str), "%m", std::localtime(&curr_t));
    return std::string(tm_str);
}

/**
 * @brief Generate a 2 digit day-of-mont, DD.
 * @param input_time String representing either a custom datetime, or "" to generate it based on datetime now
 */
std::string wibble::WibbleRecord::generate_date_DD(const std::string& input_time)
{
    std::time_t curr_t = std::time(nullptr);
    if(input_time != "") { curr_t = get_time_from_YYYY_MM_DD_str(input_time); }
    char tm_str[5];
    std::string curr_date;
    std::strftime(tm_str, sizeof(tm_str), "%d", std::localtime(&curr_t));
    return std::string(tm_str);
}


/**
 * @brief Generate a full ISO YYYY-MM-DD hh:mm:ss datetime string.
 * @param input_time String representing either a custom datetime, or "" to generate it based on datetime now
 */
std::string wibble::WibbleRecord::generate_date_YMD(const std::string& input_time)
{
    std::time_t curr_t = std::time(nullptr);
    if(input_time != "") { curr_t = get_time_from_YYYY_MM_DD_str(input_time); }
    char tm_str[60];
    std::string curr_date;
    std::strftime(tm_str, sizeof(tm_str), "%Y-%m-%d %H:%M:%S", std::localtime(&curr_t));
    return std::string(tm_str);
}

/**
 * @brief Generate a guaranteed unique hash by combining current timestamp down to seconds with first 8 UUID digits/chars.
 */
std::string wibble::WibbleRecord::generate_entry_hash()
{
    std::string un_hsh = generate_timestamp("");
    un_hsh.append("_" + generate_UUIDv4().substr(0,8));
    return un_hsh;
}


/**
 * @brief Transform a user input of a "key" + "value" pair into its serialized representation for saving to database file.
 *
 * Function also serves as mechanism for "deleting" a KeyPair, since inputting a blank key or value will
 * cause it not to be added to the resultant set; this also handles serves as input verification as well,
 * since a key-value pair should never have a blank key or value (in our usage/definition of it here).
 *
 * @param wc general console utility object
 * @param keypairs set of resultant keypairs strings to store result into/add to; mutate in-place
 * @param prefill_key prefilled key for prompt
 * @param prefill_val prefilled value for prompt
 */
bool wibble::WibbleRecord::keypair_helper(WibbleConsole& wc, std::set<std::string>& keypairs, const std::string& prefill_key, const std::string& prefill_val)
{
        std::string key = wc.console_write_prompt("Input Key  :", "", prefill_key);
        std::string value;
        WibbleRecord::remove_keypair_delim(key);
        wc.trim(key);
        if(key != "")
        {
            value = wc.console_write_prompt("Input Value:", "", prefill_val);
            WibbleRecord::remove_keypair_delim(value);
            wc.trim(value);
        }

        if(key != "" && value != "")
        {
            keypairs.insert(KP_DELIM + key + ":" + value + KP_DELIM);
            return true;
        }

        return false;
}

/**
 * @brief Read a sequence of potential keypairs/tokenise a string to attempt conversion into WibbleRecord KeyPairs
 *
 * https://stackoverflow.com/questions/53849/how-do-i-tokenize-a-string-in-c
 *
 * C++ (17) does not have a convenient string split/tokenising functionality, unless one uses Boost
 * 
 * @param wc general console utility object
 * @param kp_string string value consisting of KeyPair candidates
 */
std::vector<std::string> wibble::WibbleRecord::generate_keypair_vector(WibbleConsole& wc, std::string& kp_string)
{
    if(kp_string == "") { return std::vector<std::string>(); }
    
    // chomp whitespace/tokenise
    std::regex reg("\\s+");
    // we remove any keypair delimiters, if user input them,
    // because we will reassemble later, ensuring chhomping of extraneous whitespace
    remove_keypair_delim(kp_string);
    // trim again
    wc.trim(kp_string);
        
    std::sregex_token_iterator iter(kp_string.begin(), kp_string.end(), reg, -1);
    std::sregex_token_iterator end;

    std::vector<std::string> kp(iter, end);
    return kp;
}

/**
 * @brief Get and check any existing KeyPairs; prompt and get user input for any additional others
 * @param wc general console utility object
 * @param kp_string string value consisting of KeyPair candidates
 */
std::string wibble::WibbleRecord::get_set_keypair_field(WibbleConsole& wc, std::string& kp_string)
{
    std::string kp;
    std::vector<std::string> kp_list = generate_keypair_vector(wc, kp_string);
    std::set<std::string> key_pairs;

    if(kp_list.size() > 0 )
    {
        wc.console_write_header("KEYPAIRS");
        wc.console_write_yellow("\nNOTE: Input blank key or value to remove existing KeyPair.\n\n");
    }

    // allow user to update (or remove) any existing KeyPairs, if present
    for(auto const& kp_item: kp_list)
    {
        try
        {
            std::size_t key_sep = kp_item.find(':');
            std::string key  = kp_item.substr(0, key_sep);
            std::string val  = kp_item.substr(key_sep + 1, kp_item.length() - key_sep - 1);
            keypair_helper(wc, key_pairs, key, val);
        }
        catch(std::exception& ex) {}
    }

    // allow user to optionally add new KeyPairs, if desired
    std::string add_kp   = wc.console_write_prompt("\nAdd new/additional KeyPairs? (y/n) > ", "", "");
    if(add_kp == "y" || add_kp == "Y")
    {
        wc.console_write_hz_divider();
        wc.console_write_yellow("NOTE: Enter blank key or value to stop adding.\n\n");
        bool keep_adding;
        do { keep_adding = keypair_helper(wc, key_pairs); } while(keep_adding);
    }
    for(auto const& val: key_pairs) { kp.append(val + " "); } 

    return kp;
}

/**
 * @brief Generate a string in numbered month format, e.g. "03-march" or "09-september".
 * @param manual_prx Integer representing manual month override (1 = jan, 12 = dec) rather than current month
 */
std::string wibble::WibbleRecord::generate_numbered_month(const int& manual_prx)
{
    int month_num = 0;

    if(manual_prx != 0)
    {
        month_num = manual_prx;
    }
    else
    {
        std::time_t curr_t = std::time(nullptr);
        char tm_str[5];
        std::string curr_date;
        std::strftime(tm_str, sizeof(tm_str), "%m", std::localtime(&curr_t));

        month_num = std::stoi(tm_str);
    }

    switch(month_num)
    {
    case 1:
        return "01-january";
    case 2:
        return "02-february";
    case 3:
        return "03-march";
    case 4:
        return "01-april";
    case 5:
        return "05-may";
    case 6:
        return "06-june";
    case 7:
        return "07-july";
    case 8:
        return "08-august";
    case 9:
        return "09-september";
    case 10:
        return "10-october";
    case 11:
        return "11-november";
    case 12:
        return "12-december";
    default:
        return "ERROR";
    }

}

/**
 * @brief Generate a timestamp down to seconds resolution, YYYYMMDDddhhss.
 * @param input_time String representing either a custom datetime, or "" to generate it based on datetime now
 */
std::string wibble::WibbleRecord::generate_timestamp(const std::string& input_time)
{
    std::time_t curr_t = std::time(nullptr);
    if(input_time != "") { curr_t = get_time_from_YYYY_MM_DD_str(input_time); }
    char tm_str[15];
    std::string curr_date;
    std::strftime(tm_str, sizeof(tm_str), "%Y%m%d%H%M%S", std::localtime(&curr_t));
    return std::string(tm_str);
}


/**
 * @brief Generate displayed textual flairs in record listing, indicating presence of data or linked files.
 * @param condensed_view Flag indicating whether to use short form (one line per record), or extended/detailed listing
 */
void wibble::WibbleRecord::_print_data_flairs(const bool condensed_view) const
{
    std::string data_ind;
    std::string no_data_ind;
    std::string lf_ind;
    std::string no_lf_ind;
    std::string rn_ind;
    std::string no_rn_ind;
    std::string bin_ind;
    std::string no_bin_ind;

    const std::string DATA_TOKEN  = "[ DATA ]";
    const std::string LF_TOKEN    = "[ LF ]";
    const std::string RN_TOKEN    = "[ RELATIONSHIPS ]";

    if(condensed_view)
    {
        data_ind    = "▒D▒";    
        no_data_ind = "▒░▒";
        lf_ind      = "▒L▒";
        no_lf_ind   = "▒░▒";
        rn_ind      = "▒R▒";
        no_rn_ind   = "▒░▒";
        bin_ind     = "";
        no_bin_ind  = "";
    }
    else
    {
        data_ind    = "  ▓▒ HAS DATA ▒▓";
        no_data_ind = "  ░░ NO  DATA ░░";
        lf_ind      = "  ▓▒  LINKED  ▒▓";
        no_lf_ind   = "  ░░ NO LINK  ░░";
        rn_ind      = "  ▓▒  RELATED ▒▓";
        no_rn_ind   = "  ░░ NO RELTD ░░";
        bin_ind     = "  ▓ BIN ▓";
        no_bin_ind  = "  ░ TXT ░";
        //DATA_TOKEN = "[ DATA ]";
        //LF_TOKEN    = "[ LF ]";
    }

    if(ENABLECOLOUR)
    {

        if(dd_field == "___NULL___" && lids_field == "___NULL___")
        {
            std::cout << rang::fgB::black << no_data_ind << no_lf_ind << no_rn_ind << rang::fgB::gray;

            if(bin_record)  
                                         { std::cout << rang::fgB::cyan << bin_ind     << rang::fgB::gray;  }
            else                         { std::cout << rang::fgB::yellow << no_bin_ind << rang::fgB::gray; }

            if(condensed_view) { std::cout <<  " ┃ "; }
            else               { std::cout << '\n';   }
        }
        else
        {
            if(dd_field.find(DATA_TOKEN) != std::string::npos)
                                         { std::cout << rang::fgB::green << data_ind   ; }
            else                         { std::cout << rang::fgB::black << no_data_ind; }

            if(dd_field.find(LF_TOKEN) != std::string::npos)
                                         { std::cout << rang::fgB::cyan  << lf_ind     << rang::fgB::gray; }
            else                         { std::cout << rang::fgB::black << no_lf_ind << rang::fgB::gray;  }

            if(lids_field.find(RN_TOKEN) != std::string::npos)
                                         { std::cout << rang::fgB::red   << rn_ind     << rang::fgB::gray; }
            else                         { std::cout << rang::fgB::black << no_rn_ind << rang::fgB::gray;  }

            if(bin_record)  
                                         { std::cout << rang::fgB::cyan   << bin_ind    << rang::fgB::gray; }
            else                         { std::cout << rang::fgB::yellow << no_bin_ind << rang::fgB::gray; }

            if(condensed_view) { std::cout <<  " ┃ "; }
            else               { std::cout << '\n';   }
        }

    }
    else
    {
        if(dd_field == "___NULL___" && lids_field == "___NULL___")
        {
            std::cout <<  no_data_ind << no_lf_ind << no_rn_ind;
            if(bin_record)  
                                         { std::cout << bin_ind   ; }
            else                         { std::cout << no_bin_ind; }

            std::cout << " ┃ ";
        }
        else
        {
            if(dd_field.find(DATA_TOKEN) != std::string::npos)
                                         { std::cout << data_ind   ; }
            else                         { std::cout << no_data_ind; }

            if(dd_field.find(LF_TOKEN) != std::string::npos)
                                         { std::cout << lf_ind   ; }
            else                         { std::cout << no_lf_ind; }

            if(lids_field.find(RN_TOKEN) != std::string::npos)
                                         { std::cout << rn_ind   ; }
            else                         { std::cout << no_rn_ind; }

            if(bin_record)  
                                         { std::cout << bin_ind   ; }
            else                         { std::cout << no_bin_ind; }

            if(condensed_view) { std::cout <<  " ┃ "; }
            else               { std::cout << '\n';   }
        }
    }
}

/**
 * @brief Output an individual record listing to console, condensed/one-line variant.
 * @param counter Long value indicating current record number, 1-indexed
 * @param tot_records Long value indicating total record count in display, used for calcuating padding width
 */
std::string wibble::WibbleRecord::pretty_print_record_condensed(unsigned long counter, unsigned long tot_records) const
{
    if(counter == 0) { std::cout << "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" << '\n'; }
    if(ENABLECOLOUR)
    {
        std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
        std::cout << std::setfill('0') << std::setw(calc_padding_width(tot_records));
        std::cout << ++counter << "]" << rang::style::reset << " ┃ ";
    }
    else
    {
        std::cout << "[" << std::setfill('0') << std::setw(calc_padding_width(tot_records));
        std::cout << ++counter << "]" << " ┃ ";
    }

    _print_data_flairs(true);
    
    if(counter == tot_records)
    {
        pretty_print_record_condensed();
        std::cout << "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" << '\n';
        return "";
    }
    else
    {
        return pretty_print_record_condensed();
    }
}

/**
 * @brief Output an individual record listing to console, condensed/one-line variant without numbered wrapper.
 */
std::string wibble::WibbleRecord::pretty_print_record_condensed() const
{
    if(ENABLECOLOUR)
    {
        std::cout << rang::fgB::yellow << rang::style::bold << "(" << id_field.substr(0,4)
                  << ")" << rang::style::reset << " ";

        if(bin_record) { std::cout << rang::fgB::cyan << title_field << rang::style::reset << '\n'; }
        else           { std::cout << title_field << '\n';                                          }
    }
    else
    {
        std::cout << "(" << id_field.substr(0,4) << ") " << title_field << '\n';
    }
    return "";
}

/**
 * @brief Output an individual record listing to console, expanded/multiline variant.
 * @param counter Long value indicating current record number, 1-indexed
 * @param tot_records Long value indicating total record count in display, used for calcuating padding width
 */
std::string wibble::WibbleRecord::pretty_print_record_full(unsigned long counter, unsigned long tot_records) const
{
    if(ENABLECOLOUR)
    {
        std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
        std::cout << std::setfill('0') << std::setw(calc_padding_width(tot_records));
        std::cout << ++counter << "]" << rang::style::reset;

        _print_data_flairs(false);
        return pretty_print_record_full();
    }
    else
    {
        std::cout << "[" << std::setfill('0') << std::setw(calc_padding_width(tot_records));
        std::cout << ++counter << "] ";
        _print_data_flairs(false);
        return pretty_print_record_full_nc();
    }

}

/**
 * @brief Output an individual record listing to console, expanded/multiline variant, no colour.
 */
std::string wibble::WibbleRecord::pretty_print_record_full_nc() const
{
    std::cout << "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" << '\n';
    std::cout << "Date       │ " << date_field  << "\n";
    std::cout << "Note ID    │ " << id_field    << "\n";
    std::cout << "Title      │ " << title_field << "\n";
    std::cout << "Filetype   │ " << type_field  << "\n";
    std::cout << "Description│ " << desc_field  << "\n";
    std::cout << "Project    │ " << proj_field  << "\n";
    std::cout << "Tags       │ " << tags_field  << "\n";
    std::cout << "Class      │ " << class_field << "\n";
    std::cout << "DataDirs   │ " << dd_field    << "\n";
    std::cout << "KeyPairs   │ " << kp_field    << "\n";
    std::cout << "Custom     │ " << cust_field  << "\n";
    std::cout << "LinkedIds  │ " << lids_field  << "\n";
    std::cout << "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" << '\n';
    return "";
}

/**
 * @brief Output an individual record listing to console, expanded/multiline variant, colourised.
 */
std::string wibble::WibbleRecord::pretty_print_record_full() const
{
    std::ostringstream output;
    print.console_write_header("NODE ");
    print.console_write_yellow_bold("Date       │ "); print.console_write(date_field);
    print.console_write_yellow_bold("Note ID    │ "); print.console_write(id_field);
    print.console_write_yellow_bold("Title      │ "); print.console_write(title_field);
    print.console_write_yellow_bold("Filetype   │ "); print.console_write(type_field);
    print.console_write_yellow_bold("Description│ "); print.console_write(desc_field);
    print.console_write_yellow_bold("Project    │ "); print.console_write(proj_field);
    print.console_write_yellow_bold("Tags       │ "); print.console_write(tags_field);
    print.console_write_yellow_bold("Class      │ "); print.console_write(class_field);
    print.console_write_yellow_bold("DataDirs   │ "); print.console_write(dd_field);
    print.console_write_yellow_bold("KeyPairs   │ "); print.console_write(kp_field);
    print.console_write_yellow_bold("Custom     │ "); print.console_write(cust_field);
    print.console_write_yellow_bold("LinkedIds  │ "); print.console_write(lids_field);
    print.console_write_hz_heavy_divider();
    return output.str();
}


/**
 * @brief Turn a WibbleRecord into a textual dump of fields.
 */
const std::string wibble::WibbleRecord::serialize_record(const WibbleRecord& record)
{
    std::string output;
    output.append("Date: "        + record.date_field + "\n");
    output.append("Title: "       + record.title_field + "\n"); 
    output.append("NoteId: "      + record.id_field + "\n"); 
    output.append("Description: " + record.desc_field + "\n"); 
    output.append("Filetype: "    + record.type_field + "\n"); 
    output.append("Project: "     + record.proj_field + "\n"); 
    output.append("Tags: "        + record.tags_field + "\n"); 
    output.append("Class: "       + record.class_field + "\n"); 
    output.append("DataDirs: "    + record.dd_field + "\n"); 
    output.append("KeyPairs: "    + record.kp_field + "\n"); 
    output.append("Custom: "      + record.cust_field + "\n"); 
    output.append("LinkedIds: "   + record.lids_field + "\n"); 
    output.append("\n");
    return output;

}

/**
 * @brief Provides the default Wibble Node template for user to edit; deliberately includes all possible metadata templated fields.
 */
std::string wibble::WibbleRecord::get_default_template_str()
{
    std::string dft = "Edit the contents of default template to suit!\n";
                    dft.append("\n");
                    dft.append("All possible field expansions/field placeholders are listed.\n");
                    dft.append("\n");
                    dft.append("Note metadata fields\n");
                    dft.append("====================\n");
                    dft.append("\n");
                    dft.append("Date is: ___DATE___\n");
                    dft.append("Note ID is: ___NOTEID___\n");
                    dft.append("Title is: ___TITLE___\n");
                    dft.append("Description is: ___DESCRIPTION___\n");
                    dft.append("Filetype is: ___FILETYPE___\n");
                    dft.append("Project is: ___PROJECT___\n");
                    dft.append("Tags is: ___TAGS___\n");
                    dft.append("Class is: ___CLASS___\n");
                    dft.append("Custom is: ___CUSTOM___\n");

    return dft;
}
