/**
 * @file      wibbleio.cpp
 * @brief     Provides various wrappers and convenience functions around I/O operations.
 * @details
 *
 * Contains various (mostly static) functions related to writing, copying, moving,
 * and appending to files/stringstreams, employing C++17 <filesystem>; also
 * various helper functions regarding the Wibble database and directory operations.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <sstream>

#include "wibbleio.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Used by 'sync'; ensure file is a TEXT file, not binary file
 *
 * May cause false positives with 'dirty' text files containing one or more
 * control characters; this behaviour I consider a benefit rather than deficit.
 *
 * @param fn String with filename/path
 */
bool wibble::WibbleIO::check_if_text_file(const std::string& fn)
{
    try
    {
        std::ifstream istr(fn);
        if(istr)
        {
            //std::cerr << "OPENED: " << fn << std::endl;
            std::streambuf* pbuf = istr.rdbuf();
            do
            {
                char ch = pbuf->sgetc();
                // check for presence of a control character that isn't a whitespace/newline character
                if(std::iscntrl(static_cast<unsigned char>(ch)) && ! std::isspace(static_cast<unsigned char>(ch)))
                { return false; }
            }
            while ( pbuf->snextc() != EOF );
            istr.close();
            if(pbuf) { pbuf = nullptr; }

            return true;
        }
        return false;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in check_if_binary_file(): " << ex.what() << std::endl;
        return false;
    }
}


/**
 * @brief Delete record from database, create backup copy of database
 * @param wib_db_path Filename/path to database file to remove entry from
 * @param line_number_index Line number representing start of record
 */
bool wibble::WibbleIO::delete_as_new_database_file(const std::string& wib_db_path, long line_number_index)
{
    return update_as_new_database_file("", wib_db_path, line_number_index, true);
}

// overload to default to update
/**
 * @brief Update record in database, create backup copy of database
 * @param serialized_record String representing the new record to insert
 * @param wib_db_path Filename/path to database file to remove entry from
 * @param line_number_index Line number representing start of record
 */
bool wibble::WibbleIO::update_as_new_database_file(const std::string& serialized_record,
                                                   const std::string& wib_db_path, long line_number_index)
{
    return update_as_new_database_file(serialized_record, wib_db_path, line_number_index, false);
}

/**
 * @brief Either update or delete record in database, create backup copy of database
 * @param serialized_record String representing the new record to insert
 * @param wib_db_path Filename/path to database file to remove entry from
 * @param line_number_index Line number representing start of record
 * @param delete_record Flag controlling whether to delete rather than update
 */
bool wibble::WibbleIO::update_as_new_database_file(const std::string& serialized_record,
                                                   const std::string& wib_db_path, long line_number_index, bool delete_record)
{
    // guard clauses
    if(delete_record && serialized_record != "")
    {
        std::cerr << "Error: deletion requested but new record contents supplied!" << std::endl;
        return false;
    }

    if((! delete_record) && serialized_record == "")
    {
        std::cerr << "Error: update requested but new record is empty!" << std::endl;
        return false;
    }

    std::stringstream result_stream;
    result_stream << std::ifstream(wib_db_path).rdbuf();

    const int OFFSET = 2;

    std::ofstream new_wib_db;
    new_wib_db.open(wib_db_path + ".new", std::ios::out);
    try
    {
        if(new_wib_db.is_open())
        {
            //std::cerr << "DEBUG: OPENED: " << wib_db_path + ".new" << " for writing" << std::endl;
            std::string line;
            long local_index = 1;
            const int WIB_RECORD_LENGTH = 11; //!< Critical field representing Node record length (is length - 1, due to decrement occuring post skip)
            int record_len_skip = WIB_RECORD_LENGTH;
            bool skip_lines = false;

            while(std::getline(result_stream, line))
            {
                if(skip_lines)
                {
                    //std::cerr << "Skipping record lines: " << record_len_skip << std::endl;
                    if(record_len_skip <= 0)
                    {
                        skip_lines = false;
                    }
                    --record_len_skip;
                    continue;
                }

                if(local_index != (line_number_index - OFFSET))
                {
                    //std::cerr << "Copying line: " << line << std::endl;
                    new_wib_db << line << '\n';
                    ++local_index;
                    continue;
                }

                if(local_index == (line_number_index - OFFSET))
                {
                    //std::cerr << "Appending or deleting. Found matched line: " << line << std::endl;
                    ++local_index;
                    // add in the new record... for deletion, just skip those number of lines...
                    if(! delete_record) { new_wib_db << serialized_record; }
                    skip_lines = true;
                }
            }
        }
        new_wib_db.close();
    }
    catch(const std::exception& err)
    {
        std::cerr << "Error writing out new database file!" << std::endl;
        return false;
    }

    //copy file
    //std::cerr << "DEBUG: src: " << wib_db_path << std::endl;
    //std::cerr << "DEBUG: dest: " << wib_db_path + ".new" << std::endl;

    bool mk_backup = wcopy_file(wib_db_path, wib_db_path + ".bak");
    if(mk_backup)
    {
        //std::cout << "Backup successfully created: " << wib_db_path << ".bak" << std::endl;
        bool copy_success = wcopy_file(wib_db_path + ".new", wib_db_path);
        if(copy_success)
        {
            // housekeeping - only retain the backup (*.bak), don't need to also keep the (*.new) file as well
            if(std::filesystem::exists(std::filesystem::path(wib_db_path + ".new")))
            { std::filesystem::remove(std::filesystem::path(wib_db_path + ".new")); }
        }
        return copy_success;
    }
    else
    {
        std::cout << "Unable to create backup database. Aborting." << std::endl;
        return false;
    }
}

/**
 * @brief Write out file based on string contents using either truncation (overwriting) or appending.
 * @param file_contents String with new file contents
 * @param file_dest String with destination filename/path
 * @param overwrite Whether to overwrite/truncate or append to destination file (if exists)
 */
bool wibble::WibbleIO::_write_out_file(const std::string& file_contents, const std::string& file_dest, bool overwrite)
{
    WibbleConsole wc;
    try
    {
        std::ofstream file_output;
        if(overwrite)
        {
            // truncate flag, destination file will be OVERWRITTEN
            file_output.open(file_dest, std::ios::out | std::ios::trunc);
        }
        else
        {
            // append flag
            file_output.open(file_dest, std::ios::out | std::ios::app);
        }

        if(file_output.is_open())
        {
            file_output << file_contents;
            file_output.close();
            if(overwrite) { wc.console_write_success_green("File successfully [over-]written: " + file_dest); }
            else          { wc.console_write_success_green("File successfully written: " + file_dest);        }
            return true;
        }
        else
        {
            wc.console_write_error_red("Error opening output file: " + file_dest);
            return false;
        }
    }
    catch(const std::exception& err)
    {
        return false;
    }
}

/**
 * @brief Wrapper to write out file appending content.
 * @param file_contents String with new file contents
 * @param file_dest String with destination filename/path
 */
bool wibble::WibbleIO::write_out_file_append(const std::string& file_contents, const std::string& file_dest)
{
    return _write_out_file(file_contents, file_dest, false);
}

/**
 * @brief Wrapper to write out file truncating/overwriting content.
 * @param file_contents String with new file contents
 * @param file_dest String with destination filename/path
 */
bool wibble::WibbleIO::write_out_file_overwrite(const std::string& file_contents, const std::string& file_dest)
{
    return _write_out_file(file_contents, file_dest, true);
}

/**
 * @brief Wrapper to initialise/write out a new database file.
 * @param db_header String with new database file header content
 * @param wib_db_path String with destination filename/path for new database file
 */
bool wibble::WibbleIO::init_database_file(const std::string& db_header, const std::string& wib_db_path)
{
    return WibbleIO::append_to_database_file(db_header, wib_db_path, true);
}

/**
 * @brief Wrapper to append to existing database file (add new record).
 * @param serialize_record String with new Wibble Node record/new content
 * @param wib_db_path String with destination filename/path for new database file
 * @param write_header Flag for infomational message regarding new record or for database header/initialisation
 */
bool wibble::WibbleIO::append_to_database_file(const std::string& serialized_record, const std::string& wib_db_path, bool write_header)
{
    wibble::WibbleConsole wc;

    try
    {
        std::ofstream wib_db;
        wib_db.open(wib_db_path, std::ios::out | std::ios::app);
        if(wib_db.is_open())
        {
            wib_db << serialized_record;
            wib_db.close();
            std::string msg = "";
            if(! write_header) { msg = "Record successfully added to database: " + wib_db_path;             }
            else               { msg = "Initialised database with GNU Recutils compatible metadata header"; }
            wc.console_write_success_green(msg);
            return true;
        }
        else
        {
            wc.console_write_error_red("Error opening database file to add new record.");
            return false;
        }
    }
    catch(const std::exception& err)
    {
        return false;
    }
}

/**
 * @brief Wrapper to read in existing datbase file into a string.
 * @param wib_db_path String with filename/path for database file to read in
 */
std::string wibble::WibbleIO::read_in_database_file(const std::string& wib_db_path)
{
    try
    {
        std::ifstream wib_db;
        wib_db.open(wib_db_path, std::ifstream::in);
        if(! wib_db.good() ) { throw std::invalid_argument("BAD FILE") ; }
        std::string line;
        std::string db_file;
        while (std::getline(wib_db, line))
        {
            //std::cout << line << std::endl;
            db_file.append(line);
        }
        wib_db.close();
        return db_file;
    }
    catch(std::exception const& err)
    {
        std::cout << "I/O error: " << err.what() << std::endl;
        return "ERROR";
    }
}

/**
 * @brief Helper function to ensure new Node/note path exists prior to writing out new Node/note file
 * @param wib_path String with path to root Wibble directory
 * @param note_id String with ID of the note
 * @param ftype String with filetype/extension of the note
 * @param proj String with project value of the note
 * @param note_not_data Flag indicating whether to instead generate path pointing at associated Node/note data directory
 * @param archived Flag indicating whether to create a path in archived subtree
 */
bool wibble::WibbleIO::create_note_path(const std::string& wib_path, const std::string& note_id, const std::string& ftype,
                                        const std::string& proj, bool note_not_data, bool archived)
{
    std::string fs_path;

    if(note_not_data)
    {
        if(! archived) { fs_path = wib_path + "/notes/"          + ftype + "/" + proj + "/" + note_id.substr(0, 2); }
        else           { fs_path = wib_path + "/archived_notes/" + ftype + "/" + proj + "/" + note_id.substr(0, 2); }
    }
    else              { fs_path = wib_path + "/note_data/" + note_id.substr(0,2); }

    return create_path(fs_path);
}

/**
 * @brief Helper function to recursively create any directories as per indicated path (i.e equivalent to mkdir -p)
 * @param fs_path String with path to create
 */
bool wibble::WibbleIO::create_path(const std::string& fs_path)
{
    //std::cerr << "DEBUG: Creating path: " << fs_path << std::endl;
    if(std::filesystem::exists(std::filesystem::path(fs_path))) { return true; }
    else { return std::filesystem::create_directories(std::filesystem::path(fs_path)); }
}

/**
 * @brief Helper function to generate a std::fileystem::path pointing at desired Node/note directory
 * @param wib_path String with path to root Wibble directory
 * @param note_id String with ID of the note
 * @param note_ft String with filetype/extension of the note
 * @param proj String with project value of the note
 * @param archived Flag indicating whether to point to path in archived subtree
 */
std::filesystem::path wibble::WibbleIO::generate_note_dir(const std::string& wib_path, const std::string& note_id, const std::string& note_ft,
                                                          const std::string& proj, const bool archived)
{
    std::string path_base = "";
    if(! archived) { path_base = "/notes"; }
    else           { path_base = "/archived_notes"; }
    std::filesystem::path note_path{wib_path + path_base + "/" + note_ft + "/" + proj + "/" + note_id.substr(0, 2)};

    return note_path;
}

/**
 * @brief Helper function to generate a std::fileystem::path pointing at desired output Node/note file
 * @param wib_path String with path to root Wibble directory
 * @param note_id String with ID of the note
 * @param note_ft String with filetype/extension of the note
 * @param proj String with project value of the note
 * @param archived Flag indicating whether to point to path in archived subtree
 */
std::filesystem::path wibble::WibbleIO::generate_note_path(const std::string& wib_path, const std::string& note_id, const std::string& note_ft,
                                                           const std::string& proj, const bool archived)
{
    std::string note_dir = generate_note_dir(wib_path, note_id, note_ft, proj, archived);
    note_dir.append("/" + note_id + "." + note_ft);
    std::filesystem::path full_note_path{note_dir};
    return full_note_path;
}

/**
 * @brief Helper function to copy an existing file as a new note file at destination.
 * @param wib_path String with path to root Wibble directory
 * @param note_id String with ID of the note
 * @param note_ft String with filetype/extension of the note
 * @param proj String with project value of the note
 * @param archived Flag indicating whether to point to path in archived subtree
 */
bool wibble::WibbleIO::copy_new_note_file(const std::string& wib_path, const std::string& note_id, const std::string& note_ft,
                                          const std::string& proj, const std::string& src_file, bool archived) const
{
    std::filesystem::path file_abs_path{src_file};
    std::filesystem::path dest_file = generate_note_path(wib_path, note_id, note_ft, proj, archived);
    return wcopy_file(file_abs_path, dest_file);
}

/**
 * @brief Helper function to obtain parent directory for given path.
 * @param fs_path String with path to obtain parent directory for
 */
std::string wibble::WibbleIO::get_parent_dir_path(const std::string& fs_path)
{
    return std::filesystem::path(fs_path).parent_path();
}

// used for identifying files in identical directory hierarchy location in two different roots, i.e. file collision/mappings
// for WibbleSync
/**
 * @brief Helper function to obtain subtree path minus a root/base directory prefix
 * @param base_dir String representing common root prefix path/base parent
 * @param fs_path String to mutate representing new path minus base prefix
 */
std::string wibble::WibbleIO::get_path_minus_base_dir(const std::string& base_dir, const std::string& fs_path)
{
    try
    {
        // handle case of relative directory from current dir
        if(base_dir == "") { return fs_path; }
        else
        {
            std::string bd = base_dir; WibbleUtility::remove_trailing_slash(bd);
            return fs_path.substr(bd.length() + 1, (fs_path.length() - bd.length()));
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in get_path_minus_root_dir(): " << ex.what() << std::endl;
        return "";
    }
}

/**
 * @brief Clean up directory iff empty.
 * @param fs_path String with full path of directory to check if empty
 */
bool wibble::WibbleIO::remove_dir_if_empty(const std::string& fs_path)
{
    if(std::filesystem::exists(std::filesystem::path(fs_path)) &&
       std::filesystem::is_empty(std::filesystem::path(fs_path)))
    {
        return std::filesystem::remove(std::filesystem::path(fs_path));
    }
    else { return false; }
}


/**
 * @brief Copy file wrapper (overload accepting strings): overwrites desination if exists.
 * @param src_file String with full filepath of source file/directory
 * @param dest_file String with full filepath of destination file/directory
 */
bool wibble::WibbleIO::wcopy_file(const std::string& src_file, const std::string& dest_file)
{
    return wcopy_file(std::filesystem::path(src_file), std::filesystem::path(dest_file));
}

/**
 * @brief Copy file wrapper, fully recursive: overwrites desination if exists.
 * @param src_file String with full filepath of source file/directory
 * @param dest_file String with full filepath of destination file/directory
 */
bool wibble::WibbleIO::wcopy_file(const std::filesystem::path& src_file, const std::filesystem::path& dest_file)
{
    try
    {
        //std::cerr << "Copying: " << src_file << " to: " << dest_file << std::endl;
        // recursive and overwrite
        const auto fs_options = std::filesystem::copy_options::overwrite_existing | std::filesystem::copy_options::recursive;
        std::filesystem::copy(src_file, dest_file, fs_options);
        return true;
    }
    catch(std::filesystem::filesystem_error& ex)
    {
        std::cerr << "Error copying file: " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Move file wrapper, fully recursive: attempts rename() first; if fails, uses copy then remove_all().
 * @param src_file String with full filepath of source file/directory
 * @param dest_file String with full filepath of destination file/directory
 */
bool wibble::WibbleIO::wmove_file(const std::filesystem::path& src_file, const std::filesystem::path& dest_file)
{
    try
    {
        //std::cerr << "Moving: " << src_file << " to: " << dest_file << std::endl;
        std::filesystem::rename(src_file, dest_file);
        return true;
    }
    catch(std::filesystem::filesystem_error& ex)
    {
        //std::cerr << "Rename failed (different filesystems?), attempting copy-and-move.\n";
        // rename() fails if files are on different block devices, hence copy-and-delete is required
        try
        {
            bool do_copy = wcopy_file(src_file, dest_file);
            if(do_copy)
            {
                bool rem = std::filesystem::remove_all(src_file);
                if(! rem)
                {
                    std::cerr << "Unable to remove source file/directory: ";
                    std::cerr <<  src_file << ". Manual deletion required." << std::endl;
                }
                return true;
            }
            else { return false; }
        }
        catch(std::filesystem::filesystem_error& ex)
        {
            std::cerr << "Error moving file: " << ex.what() << std::endl;
            return false;
        }
    }
}

/**
 * @brief Move Node/note to/from archive to/from main database.
 * @param to_archive Flag indicating whether Node is being moved to (or from) the archive
 * @param wib_path String with path to root Wibble directory
 * @param note_id String with ID of the note
 * @param note_ft String with filetype/extension of the note
 * @param proj String with project value of the note
 */
bool wibble::WibbleIO::migrate_note_file(bool to_archive, const std::string& wib_path, const std::string& note_id,
                                         const std::string ftype, const std::string proj)
{
    try
    {
        bool create_dest = false;
        if(to_archive) { create_dest = create_note_path(wib_path, note_id, ftype, proj, true, true);  }
        else           { create_dest = create_note_path(wib_path, note_id, ftype, proj, true, false); }

        if(create_dest)
        {
            //const std::filesystem::path src_file = to_archive ? generate_note_path(wib_path, note_id, ftype, proj, false);
            std::filesystem::path src_file;
            std::filesystem::path dest_file;
            std::filesystem::path src_dir;

            // Add in a specific support/workaround for Freeplane
            // if user has defined binary file mapping (mm)
            // Freeplane automatically creates a <filename-minus-extension>_files
            // directory if assets are DnD onto mindmaps
            std::filesystem::path src_mm_dir = "___NULL___";
            std::filesystem::path dest_mm_dir = "___NULL___";
            if(to_archive)
            {
                src_file  = generate_note_path(wib_path, note_id, ftype, proj, false);
                dest_file = generate_note_path(wib_path, note_id, ftype, proj, true);
                src_dir   =  generate_note_dir(wib_path, note_id, ftype, proj, false);

                //Freeplane accommodation
                if(ftype == "mm")
                {
                    src_mm_dir   = generate_note_path(wib_path, note_id, "mm", proj, false);
                    dest_mm_dir  = generate_note_path(wib_path, note_id, "mm", proj, true);
                }
            }
            else // restoring FROM the archive
            {
                src_file  =  generate_note_path(wib_path, note_id, ftype, proj, true);
                dest_file =  generate_note_path(wib_path, note_id, ftype, proj, false);
                src_dir   =  generate_note_dir(wib_path, note_id, ftype, proj, true);

                if(ftype == "mm")
                {
                    src_mm_dir   = generate_note_path(wib_path, note_id, "mm", proj, true);
                    dest_mm_dir  = generate_note_path(wib_path, note_id, "mm", proj, false);
                }
            }

            // handle special case of freeplane directories
            if(src_mm_dir != "___NULL__" && dest_mm_dir != "___NULL___") { WibbleUtility::set_freeplane_dirs(src_mm_dir, dest_mm_dir); }

            bool copy_note = wcopy_file(src_file, dest_file);
            bool copied_mm_files = false;

            // copy freeplane directory if exists (special case)
            if(std::filesystem::exists(std::filesystem::path(src_mm_dir)))
            {
                bool copy_mm_files = wcopy_file(src_mm_dir, dest_mm_dir);
                if(copy_mm_files)
                {
                    copied_mm_files = true;
                    WibbleConsole().console_write_success_green("Freeplane files directory detected and migrated.");
                }
                else
                { throw std::invalid_argument("Unable to copy Freeplane directory"); }
            }

            if(copy_note)
            {
                bool delete_file = std::filesystem::remove_all(src_file);
                if(! delete_file) { std::cerr << "Error deleting file: " << src_file << "\n"; }

                if(copied_mm_files)
                {
                    bool delete_mm_dir = std::filesystem::remove_all(src_mm_dir);
                    if(! delete_mm_dir) { std::cerr << "Error deleting Freplane directory: " << src_mm_dir << "\n"; }
                }

                bool delete_dir = true;
                if(std::filesystem::is_empty(src_dir))
                {
                    delete_dir = std::filesystem::remove(src_dir);
                    if(! delete_dir) { std::cerr << "Error deleting empty directory.\n"; }

                    // removing empty parents/housekeeping
                    std::filesystem::path parent_dir_main_proj = wib_path + "/notes/" + ftype + "/" + proj;
                    std::filesystem::path parent_dir_archived_proj = wib_path + "/archived_notes/" + ftype + "/" + proj;
                    std::string parent_dir_main_type = wib_path + "/notes/" + ftype;
                    std::string parent_dir_archived_type = wib_path + "/archived_notes/" + ftype;
                    if(std::filesystem::is_empty(parent_dir_main_proj))     { std::filesystem::remove(parent_dir_main_proj);     }
                    if(std::filesystem::is_empty(parent_dir_archived_proj)) { std::filesystem::remove(parent_dir_archived_proj); }
                    if(std::filesystem::is_empty(parent_dir_main_type))     { std::filesystem::remove(parent_dir_main_type);     }
                    if(std::filesystem::is_empty(parent_dir_archived_type)) { std::filesystem::remove(parent_dir_archived_type); }

                }
                else { std::cerr << "DEBUG: src_dir is NOT empty, NOT removing = " << src_dir << '\n'; }

                return delete_dir;
            }
            else { return false; } // copy failed
        }
        else
        {
            return false;
        }
    }
    catch(std::filesystem::filesystem_error& er)
    {
        std::cerr << "Error migrating note file: " << er.what() << '\n';
        return false;
    }
}

/**
 * @brief Read in a metadata schema file.
 * @param schema_f_name Name of schema file to read in
 * @param Root directory containing schema files
 */
const std::string wibble::WibbleIO::read_in_schema_file(const std::string& schema_f_name, const std::string& schemas_dir)
{
    try
    {
        std::filesystem::path schema_file_path = schemas_dir + "/" + schema_f_name;

        if(! std::filesystem::exists(schema_file_path))
        {
            std::cerr << "DEBUG: Error, invalid schema file: " << schema_file_path << std::endl;
            return "";
        }
        std::stringstream  schema_stream;
        schema_stream << std::ifstream(schema_file_path).rdbuf();
        return schema_stream.str();
    }
    catch(std::exception const& e){
        std::cout << "I/O error: " << e.what() << std::endl;
        return "";
    }

}

/**
 * @brief Read file into a stringstream; used everywhere
 * @param input_file Full filename/path to file to read into the stringstream
 */
std::stringstream wibble::WibbleIO::read_file_into_stringstream(const std::string &input_file)
{
    std::stringstream ss;
    try
    {
        if(! std::filesystem::exists(std::filesystem::path(input_file)))
        { throw std::invalid_argument("Input file: '" + input_file + "' not found."); }
        //std::cerr << "DEBUG: reading file : " << input_file << std::endl;
        ss << std::ifstream(input_file).rdbuf();
        return ss;
    }
    catch(std::exception& ex)
    {
        //std::cerr << "DEBUG: I/O error: '" << ex.what() << "'" << std::endl;
        return ss;
    }
}
