/**
 * @file      wibblebinlist.hpp
 * @brief     Decomposes the user binary mapping configuration for support of binary files (header file).
 * @details
 *
 * This class parses the users "binary_mapping" field to yield the defined 
 * user custom binary file associations/pairings: given extension with 
 * given editing/opening tool and optional viewer. Also ensures that user
 * has setup a default stub/prototype/template file to use as a "blank"
 * for new copies.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBINLIST_H
#define WIBBINLIST_H

#include <string>
#include <set>

namespace wibble
{
    class WibBinList
    {
    private:
        static bool do_setup;
        static std::set<std::string> bin_set;
    public:
        std::set<std::string> gen_bin_list(const std::string& bin_mapping, const std::string& t_mplate_dir, const bool silence_mode = false);
        //void set_display();
    };
}
#endif
