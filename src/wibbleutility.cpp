/**
 * @file      wibbleutility.cpp
 * @brief     General purpose library file with many shared functions.
 * @details
 *
 * Many static methods to perform common operations used in typically multiple
 * disparate functions/places.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <algorithm> 
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <limits>
#include <set>
#include <string>
#include <vector>

#include "external/rang.hpp"

#include "wibbleexit.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

namespace wibble
{
    using WibResultList = std::unique_ptr<std::vector<WibbleRecord>>;
}

//https://stackoverflow.com/questions/10030626/replace-char-in-string-with-some-string-inplace
template <typename InputIt1, typename OutputIt, typename T, typename InputIt2>
OutputIt wibble::WibbleUtility::_replace_with_range_copy(InputIt1 first, InputIt1 last, OutputIt d_first,
                                                         const T& old_value, InputIt2 new_first, InputIt2 new_last)
{
    InputIt1 next;
    while (true)
    {
        if (first == last) return d_first;
        next = std::find(first, last, old_value);
        d_first = std::copy(first, next, d_first);
        if (next == last) return d_first;
        d_first = std::copy(new_first, new_last, d_first);
        first = std::next(next);
    }
}

/**
 * @brief Utility method for in-place replacement of a character within a string.
 * @param input string to mutate/perform replacement into 
 * @param replacment_string string with replacment value/expansion
 * @param char_to_replace individual character to replace/expand with replacement_string value
 */
void wibble::WibbleUtility::_replace_char(std::string& input, const std::string& replacement_string, char char_to_replace)
{
    if (replacement_string.empty())
    {
        input.erase(std::remove(input.begin(), input.end(), char_to_replace), input.end());
        return;
    }
    if (replacement_string.size() == 1)
    {
        std::replace(input.begin(), input.end(), char_to_replace, replacement_string.front());
        return;
    }

    const auto first_instance = std::find(input.begin(), input.end(), char_to_replace);
    auto count = std::count(first_instance, input.end(), char_to_replace);
    const auto extra_size = count * (replacement_string.size() - 1);
    const auto new_size = input.size() + extra_size;

    if (input.capacity() < new_size)
    {
        std::string aux;
        aux.reserve(new_size);
        _replace_with_range_copy(input.cbegin(), input.cend(), std::back_inserter(aux),
                                 char_to_replace, replacement_string.cbegin(), replacement_string.cend());
        input.swap(aux);
        return;
    }

    input.resize(new_size);

    const auto rlast = std::make_reverse_iterator(first_instance);
    const auto rfirst = input.rbegin();
    const auto old_rfirst = rfirst + extra_size;

    _replace_with_range_copy(old_rfirst, rlast, rfirst, char_to_replace,
                             replacement_string.crbegin(), replacement_string.crend());
}


/**
 * @brief Utility method for in-place replacement ___FILES___ placeholder for batch command expansion.
 * @param exec_cmd string with command template to expand
 * @param fn_list string with list of files/arguments to supply to insert for ___FILES___
 */
void wibble::WibbleUtility::build_batch_exec_cmd(std::string& exec_cmd, std::string& fn_list)
{
    // possible placeholder expansion
    const std::string DELIM = "___FILES___";
    std::size_t start_subs_delim = exec_cmd.find(DELIM);
    if(start_subs_delim == std::string::npos)
    {
        exec_cmd = exec_cmd + " " + fn_list;
    }
    else
    {
        std::string pre_cmd = exec_cmd.substr(0, start_subs_delim);
        std::string end_cmd = exec_cmd.substr(start_subs_delim + DELIM.length(), std::string::npos);
        exec_cmd = pre_cmd + fn_list + end_cmd;
    }
}


/**
 * @brief Utility method to either get or set the execution command or script properly.
 * @param wc general console utility object
 * @param scripts_base_dir string with path to localised scripts directory
 * @param run_std_cmd flag to control whether to execute shell command
 * @param run_scr_cmd flag to control whether to execute/expand script rather than shell command
 * @param exec_cmd string with command/shell execution template 
 * @param exec_scr string with script execution template
 */
bool wibble::WibbleUtility::check_prompt_set_exec_cmds_scripts(WibbleConsole& wc, const std::string& scripts_base_dir, const bool& run_std_cmd,
                                                       const bool& run_scr_cmd, std::string& exec_cmd, std::string& exec_script)
{
    if(! run_std_cmd && ! run_scr_cmd) { return false; }

    wc.trim(exec_cmd);
    wc.trim(exec_script);

    std::string script_arg = exec_script;

    // get input/command if they haven't provided it
    if(run_std_cmd && exec_cmd == "")
    {
        exec_cmd = wc.console_write_prompt("Input execution command/command template: ", "", "");
        wc.trim(exec_cmd);
    }

    if(run_scr_cmd && exec_script != "")
    {   exec_script = scripts_base_dir + "/" + exec_script; }
    else if(run_scr_cmd && exec_script == "")
    {
        wc.console_write_green(ARW + "Available scripts:\n");
        std::system(("ls -p \"" + scripts_base_dir + "\" | grep -v /").c_str());
        exec_script = wc.console_write_prompt("Specify name of script to use: ", "", "");
        wc.trim(exec_script);
        exec_script = scripts_base_dir + "/" + exec_script;
    }

    if(run_scr_cmd)
    { 
        if(! std::filesystem::exists(std::filesystem::path(exec_script)))
        {
            wc.console_write_red(ARW + "Scripts directory: " + scripts_base_dir + "\n");
            wc.console_write_error_red("No script named '" + script_arg + "' found! Aborting."); 
            wc.console_write_green(ARW + "Available scripts:\n");
            std::system(("ls -p \"" + scripts_base_dir + "\" | grep -v /").c_str());
            throw wibble::wibble_exit(1);
        }
    }

    return true;
}

/**
 * @brief Important safety wrapper against potentially destructive file/directory moving.
 * @param wc general console utility object
 * @param src string with path to source file/directory
 * @param dest string with path to destination file/directory
 */
void wibble::WibbleUtility::confirm_file_move_or_exit(WibbleConsole& wc, const std::string& src, const std::string& dest, const bool bypass)
{
    // ensure stream flushing for input (after potential mass record retrieval with sync_with_stdio = false)
    std::cout << std::unitbuf;
    if(! bypass)
    {
        std::cout << '\n';
        wc.console_write_red_bold("━━━━━━━━━━ [ CONFIRM FILE/DIRECTORY MOVE  ] ━━━━━━━━━━━\n");
        wc.console_write_red_bold("Source     : "); wc.console_write_red(src + "\n");
        wc.console_write_red_bold("Destination: "); wc.console_write_red(dest + "\n");
        wc.console_write_hz_divider();
        wc.console_write_red_bold("Removing   : "); wc.console_write_red(src + "\n");
        wc.console_write_hz_divider();
        wc.console_write_red("Source will be "); wc.console_write_red_bold("DELETED ");
        wc.console_write_red("after "); wc.console_write_red_bold("SUCCESSFUL ");
        wc.console_write_red("copy operation to destination.\n");
        wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
        wc.console_write_red_bold("WARNING. File/directory move requested. Confirm operation above.\n");
        wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
        std::string resp = wc.console_write_prompt("\nConfirm file/directory MOVE operation (y/n) > ", "", "");
        wc.trim(resp);
        if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); throw wibble::wibble_exit(0); }
        std::cout << '\n';
    }
}

/**
 * @brief Helper method to create a backup file, before overwriting existing file with new replacment.
 * @param bkup_dir string with path to backup directory/location
 * @param new_file string with path to new replacement file file/directory
 * @param orig_file string with path to original file/directory to backup then replace
 */
bool wibble::WibbleUtility::create_backup_and_overwrite_file(const std::string& bkup_dir, const std::string& new_file,
                                                                      const std::string& orig_file)
{
    try
    {
        const auto fs_options = std::filesystem::copy_options::overwrite_existing;
        const std::string backup_fn = bkup_dir + "/" + std::string(std::filesystem::path(orig_file).filename()); 
        // create backup copy then overwrite original
        std::filesystem::copy(std::filesystem::path{orig_file}, std::filesystem::path{backup_fn}, fs_options);
        std::filesystem::rename(std::filesystem::path{new_file}, std::filesystem::path(orig_file));
        return true;
    }
    catch(std::filesystem::filesystem_error& fse)
    {
        std::cerr << "Error in create_backup_and_overwrite_file(): " << fse.what() << std::endl;
        return false;
    }
 
}


/**
 * @brief Bright red warning message when user attempts to run feature under development
 * @param feature_name string with name of feature/command under development
 * @param eta string with estimated descriptive date when feature will be ready
 */
void wibble::WibbleUtility::dev_warning(WibbleConsole& wc, const std::string& feature_name, const std::string& eta)
{
    wc.console_write_red_bold("━━━━━━━━━ [ !! FEATURE UNDER DEVELOPMENT!!  ] ━━━━━━━━━\n");
    wc.console_write_red("- Feature '" + feature_name + "' is currently under development.\n");
    wc.console_write_red("- It is not recommended for use yet.\n");
    wc.console_write_red_bold("- ETA for completion of this feature: " + eta + "\n");
    wc.console_write_red_bold("*** PROCEED AT YOUR OWN RISK. You have been warned... ***\n");
    wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n\n");
}


/**
 * @brief Important helper method to expand "~" symbol to current user's home directory, as per UNIX conventions.
 * @param fs_path string with filename path to replace/expand tilde symbols into
 */
void wibble::WibbleUtility::expand_tilde(std::string& fs_path)
{
    const char* env_home = std::getenv("HOME");
    if(env_home)
    {
        _replace_char(fs_path, std::string(env_home), '~');

    }
    else
    {
        std::cerr << "Warning: Unable to get environment variable $HOME" << std::endl;
    }
}

/**
 * @brief Helper method to find/extract brief description line, used by "daily" scripts functionality
 * @param input_file string with filename path to search for token within
 */
std::string wibble::WibbleUtility::find_file_wibble_desc(const std::string& input_file)
{
    
    const std::string DELIM = "WIBBLE_DESC:";
    bool found_title = false;
    std::string line;
    std::string title = "<UNSET>";

    std::stringstream input_ss;
    try
    {
        input_ss = WibbleIO::read_file_into_stringstream(input_file);
        while(std::getline(input_ss, line, '\n') && found_title == false) 
        {
            size_t delim_index = line.find(DELIM);
            if(delim_index != std::string::npos && line.find(delim_index + (DELIM.length() + 1) != std::string::npos))
            {
                title = line.substr((delim_index + DELIM.length() + 1), std::string::npos);
                found_title = true;
            }
        }

    }
    catch(std::exception& ex) { }

    std::stringstream().swap(input_ss);

    return title;
}

/**
 * @brief Helper method to ensure/indicate that string is not just whitespace characters.
 * @param s_input input string to check 
 */
bool wibble::WibbleUtility::check_string_has_content(const std::string& s_input)
{
    // return true if the string has content i.e. != whitespace only
    return ! (std::all_of(s_input.begin(), s_input.end(), isspace));
}



/**
 * @brief Helper method to extract the binary mapping structure.
 * @param wc WibbleConsole utility object
 * @param bin_mapping configuration key/string with binary file associations/commands 
 * @return Integer indexed map pointing to a pair consisting of the extension plus the 
 *         pair of programs used to edit and view that filetype
 */
std::map<int, std::pair<std::string, std::pair<std::string, std::string>>> wibble::WibbleUtility::export_binary_tool_mapping(WibbleConsole& wc, const std::string& bin_mapping)
{
    std::vector<std::string> mapping_list;
    std::map<int, std::pair<std::string, std::pair<std::string, std::string>>> exec_prog;
    
    // early exit if bin mapping is empty
    if(bin_mapping == "UNSET" || bin_mapping == "") { return exec_prog; }
    
    std::stringstream bin_ss{bin_mapping};
    std::string token;
    while(std::getline(bin_ss, token, ','))
    {
        wc.trim(token);
        mapping_list.push_back(token);
    }
    std::stringstream().swap(bin_ss);
    auto index = 0;
    for(auto const& prg: mapping_list)
    {
        index++;
        std::size_t ext_delim = prg.find(":");
        if(ext_delim == std::string::npos) { wc.console_write_red(ARW + "Bad mapping: " + prg + ". Skipping\n"); continue; }
        const std::string exten   = prg.substr(0, ext_delim);
        const std::string mapping = prg.substr(ext_delim + 1, (prg.length() - 1 - ext_delim));
        
        std::size_t alt_delim = mapping.find("%");
        if((alt_delim != std::string::npos))
        {
            const std::string main_prog = mapping.substr(0, alt_delim);
            const std::string alt_prog  = mapping.substr(alt_delim + 1, (mapping.length() - 1 - alt_delim));
            exec_prog[index] = std::make_pair(exten, std::make_pair(main_prog, alt_prog));
        }
        else
        {
            exec_prog[index] = std::make_pair(exten, std::make_pair(mapping, NONE_IDENT));
        }    
    }
    
    return exec_prog;
}

/**
 * @brief Helper method to obtain the program used to edit or view a binary (non-plaintext) Node.
 * @param wc WibbleConsole utility object
 * @param ftype file extension to check against 
 * @param bin_mapping configuration key/string with binary file associations/commands 
 * @param edit_not_view flag to optionally switch to binary view tool (if user has defined one)
 */
std::string wibble::WibbleUtility::get_binary_tool(WibbleConsole& wc, const std::string& ftype, const std::string& bin_mapping, const bool edit_not_view)
{
    if(bin_mapping == "UNSET") { return ""; }
    else
    {
        // split the command delimited list
        try
        {
            std::vector<std::string> mapping_list;
            std::map<std::string, std::string> exec_prog;
            std::stringstream bin_ss{bin_mapping};
            std::string token;
            while(std::getline(bin_ss, token, ','))
            {
                wc.trim(token);
                mapping_list.push_back(token);
            }
            for(auto const& prg: mapping_list)
            {
                std::size_t ext_delim = prg.find(":");
                if(ext_delim == std::string::npos) { wc.console_write_red(ARW + "Bad mapping: " + prg + ". Skipping\n"); continue; }
                const std::string exten   = prg.substr(0, ext_delim);
                const std::string mapping = prg.substr(ext_delim + 1, (prg.length() - 1 - ext_delim));

                std::size_t alt_delim = mapping.find("%");
                if((alt_delim != std::string::npos))
                {
                    const std::string main_prog = mapping.substr(0, alt_delim);
                    const std::string alt_prog  = mapping.substr(alt_delim + 1, (mapping.length() - 1 - alt_delim));
                    if(edit_not_view) { exec_prog[exten] = main_prog; }
                    else              { exec_prog[exten] = alt_prog;  }
                }
                else
                {
                    exec_prog[exten] = mapping;
                }
           } 
           std::stringstream().swap(bin_ss);
           
           if(exec_prog.count(ftype) == 1)
           {
               wc.console_write_green(ARW + "Matched defined binary filetype/association: ");
               wc.console_write_green_bold(ftype + "\n");
               return exec_prog[ftype];
           }
           else { return ""; }
            
        }
        catch(std::exception& ex)
        {
            wc.console_write_error_red("Invalid binary mapping configuration: " + bin_mapping + "\n");
            return "";
        }
    }
}

/**
 * @brief Specific helper/workeraround to support Freeplane when used as a binary file type
 * @param src_mm_dir Source Freeplane '_files' directory (if created/exists, user added attachments to mindmap)
 * @param dest_mm_dir Freeplane '_files' directory
 */
void wibble::WibbleUtility::set_freeplane_dirs(std::filesystem::path& src_mm_dir, std::filesystem::path& dest_mm_dir)
{
    std::string src_mm_dir_s = std::string(src_mm_dir);
    std::string dest_mm_dir_s = std::string(dest_mm_dir);

    // replace '.' with '_' in file extension component
    auto const src_ext_sep = src_mm_dir_s.find_last_of('.'); 
    src_mm_dir_s = src_mm_dir_s.substr(0, src_ext_sep) + "_files"; 
    auto const dest_ext_sep = dest_mm_dir_s.find_last_of('.'); 
    dest_mm_dir_s = dest_mm_dir_s.substr(0, dest_ext_sep) + "_files";
                                                      
    src_mm_dir = std::filesystem::path(src_mm_dir_s);
    dest_mm_dir =std::filesystem::path(dest_mm_dir_s);
}

/**
 * @brief Wrapper method to return a vector/pointer to set of WibbleRecords.
 * @param wib_e WibbleExecutor general environment/execution object
 * @param SEARCH struct with of search settings/values to generate database query with
 */
wibble::WibResultList wibble::WibbleUtility::get_record_set(WibbleExecutor& wib_e, wibble::search_filters& SEARCH)
{
    return wib_e.perform_search(SEARCH);
}

/**
 * @brief Convenience function to retrieve WibbleRecord/Node for a given ID.
 * @param node_id the unique ID/hash for this particular Node
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param archive_search Flag controlling whether to search through archived Nodes instead
 */
wibble::WibbleRecord wibble::WibbleUtility::get_record_for_id(const std::string node_id, WibbleConsole& wc, WibbleExecutor& wib_e, const bool archive_search)
{
    search_filters s;
    s.exact = true;
    s.id = node_id;
    s.archive_db = archive_search;
    return get_record_interactive(wc, wib_e, s);
}

/**
 * @brief Perform a full text/expensive search on Node content rather than search the database index.
 *
 * Use ripgrep to search through all files/full text recursively, then use this result set of filename
 * paths to extract the Node ID from the given filename in order to determine the matching Node record.
 *
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param rg_exp string containing the ripgrep regular expression/search term to use
 * @param node_root_path string with path to base Node directory to search from, to find and reconstruct Node results
 * @param archived_nodes Flag controlling whether to search through archived Nodes instead
 * @param archived_nodes Flag controlling whether to perform a fixed/literal string search rather than regular expressioin expansion
 */
wibble::WibResultList wibble::WibbleUtility::get_wibble_records_fulltext(WibbleConsole& wc, WibbleExecutor& wib_e, const std::string rg_exp, const std::string& node_root_path,
                                                         const std::string& WORKFILE, const bool archived_nodes, const bool fixed_str_search)
{
    // dampen output for clean result presentation
    wc.silence_output();
    // cd into directory to remove extraneous part of path results
    std::string rg_cmd = "cd \"" + node_root_path + "\" && rg --vimgrep ";
    
    if(fixed_str_search) { rg_cmd.append("-F \""); }
    else                 { rg_cmd.append("-i \""); } // case-insensitive is default

    rg_cmd.append(rg_exp + "\" > " + WORKFILE);
    //std::cerr << "DEBUG: built rg --vimgrep cmd: " << rg_cmd << std::endl;
    std::system(rg_cmd.c_str());
    std::map<std::string, unsigned long> node_matches;
    WibResultList results (new std::vector<wibble::WibbleRecord>); 
    unsigned long max_result_count = 0;
    try
    {
        std::string line;
        std::stringstream rg_results = WibbleIO::read_file_into_stringstream(WORKFILE);
        // 1. find IDs of matches
        while(std::getline(rg_results, line, '\n'))
        {
            unsigned long spl_index = line.find(COLON_DELIM);
            if(spl_index != std::string::npos)
            {
                std::size_t dir_sep_offset = 0;
                std::size_t srch_offset = -1;
                int dir_sep_count = 0;
                while(dir_sep_count < 3)
                {
                    //std::cout << "DEBUG: ITER = " << dir_sep_count << ", DIR_SEP_OFFSET = " << srch_offset << std::endl; 
                    dir_sep_offset = line.find("/", srch_offset + 1);
                    if(dir_sep_offset == std::string::npos) { throw std::invalid_argument("Bad line match"); }
                    else { ++dir_sep_count; srch_offset = dir_sep_offset; }
                }
                std::size_t ext_offset = line.find(".");
                const int ext_length = spl_index - ext_offset;
                // extract out just the id part of the filename
                const std::string node_id = line.substr(dir_sep_offset + 1, spl_index - dir_sep_offset - ext_length - 1);
                if(node_matches.count(node_id) == 1)
                {
                    node_matches[node_id] = ++node_matches[node_id];
                    if(node_matches[node_id] > max_result_count) { max_result_count = node_matches[node_id]; }
                }
                else
                {    
                    node_matches[node_id] = 1;
                }
            }
        }

        // 2. retrieve WibbleRecords for each id
        if(node_matches.size() > 0) { wc.console_write_header("MATCHES", false); }
        for(auto const& [id, count]: node_matches)
        {
            WibbleRecord w = get_record_for_id(id, wc, wib_e, archived_nodes);
            if(w.get_id() != "___NULL___")
            {
                std::cout << "Count: ";
                if(count == max_result_count)
                {
                    std::cout << rang::fgB::green << rang::style::bold << std::setfill('0') <<
                    std::setw(std::to_string(max_result_count).length()) << count << rang::style::reset << " ┃ ";
                }
                else
                {
                    std::cout << rang::fgB::red << rang::style::bold << std::setfill('0') <<
                    std::setw(std::to_string(max_result_count).length()) << count << rang::style::reset << " ┃ ";
                }
                wc.console_write_yellow_bold("(" + w.get_id().substr(0,4) + ") ", false);
                wc.console_write(w.get_title(), false);
                results->emplace_back(w);
            }
        }

        wc.enable_output();
        wc.console_write_hz_heavy_divider();
    }
    catch(std::exception& ex)
    {
        std::cerr << "Malformed search: " << ex.what() << std::endl;
        throw wibble::wibble_exit(0);
    }

    return results;
}


/**
 * @brief Wrapper method to interactively select a single record from a result set of WibbleRecords.
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param SEARCH struct with of search settings/values to generate database query with
 */
wibble::WibbleRecord wibble::WibbleUtility::get_record_interactive(WibbleConsole& wc, WibbleExecutor& wib_e, search_filters& SEARCH)
{
    WibbleRecord w;
    WibResultList results = get_record_set(wib_e, SEARCH);
    long tot_results = wib_e.get_tot_results(results);

    // if single-only mode is enabled, immediately exit if results are not exactly 1 (i.e. for scripting)
    if(SEARCH.single_only && tot_results != 1) { throw wibble::wibble_exit(1); }
    
    if(tot_results == 0) { wc.console_write_error_red("No results for search. Exiting."); throw wibble::wibble_exit(0); }
    else if(tot_results == 1)
    {
        wc.console_write_success_green("Single candidate/match, automatically selecting.");
        w = wib_e.get_record_from_list(results, 1);
    }
    else
    {
        unsigned long selection = wib_e.select_valid_record_or_exit(results, SEARCH.extended_view);
        w = wib_e.get_record_from_list(results, selection);
    }

    return w;
}

/**
 * @brief Wrapper method to interactively select either a single record (in list) or return result subset of WibbleRecords.
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param SEARCH struct with of search settings/values to generate database query with
 * @param opt_correct integer correction factor for controlling display count of total matched results
 */
wibble::WibResultList wibble::WibbleUtility::get_records_multi_interactive(WibbleConsole& wc, WibbleExecutor& wib_e, search_filters& SEARCH, int opt_correct)
{
    wc.silence_output();
    WibResultList results = get_record_set(wib_e, SEARCH);
    long tot_results = wib_e.get_tot_results(results);
    if(! SEARCH.single_only) { wc.enable_output(); }

    // if single-only mode is enabled, immediately exit if results are not exactly 1 (i.e. for scripting)
    if(SEARCH.single_only && tot_results != 1) { throw wibble::wibble_exit(1); }

    if(tot_results == 0) { wc.console_write_error_red("No results for search. Exiting."); throw wibble::wibble_exit(0); }
    else if(tot_results == 1)
    {
        wc.console_write_success_green("Single candidate/match, automatically selecting.");
        return results;
    }
    else
    {
        wc.console_write_hz_divider();
        wc.console_write_green(ARW + "" + std::to_string(tot_results - opt_correct) + " matches found.\n");
        std::string resp;
        return wib_e.range_select_record_selection(results, SEARCH.extended_view);
    }
}

/**
 * @brief Check setting for file parameter or get user input if empty; return empty if file does not exist.
 * @param wc general console utility object
 * @param input string with file path to check and mutate/return
 */
void wibble::WibbleUtility::fix_file_param_get_input(WibbleConsole& wc, std::string& input) 
{
    std::cout << std::unitbuf;
    // get file input if not entered, check that file exists
    if(input == "")
    {
        input = wc.console_write_prompt("Enter file: ", "", "");
        wc.trim(input);
        wc.console_write("\n");
    }

    remove_trailing_slash(input);

    if(! std::filesystem::exists(std::filesystem::path(input))) { input = ""; }
}

/**
 * @brief Obtain the users full search settings; used to provide "menu" search functionality.
 * @param wc general console utility object
 * @param SEARCH struct to mutate in-place with desired search values/settings
 */
void wibble::WibbleUtility::menu_search_set_filters(WibbleConsole& wc, search_filters& SEARCH)
{
    std::string es, fx;

    wc.console_write_header("SEARCH ");
    wc.console_write_hz_divider();
    es   = wc.console_write_prompt("Perform an archive search? (y/n): ", "", "n");
    wc.trim(es); if(es == "y" || es == "Y") { SEARCH.archive_db = true;         }
    es   = wc.console_write_prompt("Perform exact/case sensitive search? (y/n): ", "", "n");
    wc.trim(es); if(es == "y" || es == "Y") { SEARCH.exact = true;         }
    es   = wc.console_write_prompt("Perform fixed string/non-regexp search? (y/n): ", "", "n");
    wc.trim(fx); if(fx == "y" || fx == "Y") { SEARCH.fixed_strings = true; }
    wc.console_write_hz_divider();

    SEARCH.date   = wc.console_write_prompt("Date       : ", "", SEARCH.date);  wc.trim(SEARCH.date);  // 1  
    SEARCH.title  = wc.console_write_prompt("Title      : ", "", SEARCH.title); wc.trim(SEARCH.title); // 2  
    SEARCH.id     = wc.console_write_prompt("NoteId     : ", "", SEARCH.id);    wc.trim(SEARCH.id);    // 3  
    SEARCH.desc   = wc.console_write_prompt("Description: ", "", SEARCH.desc);  wc.trim(SEARCH.desc);  // 4  
    SEARCH.type   = wc.console_write_prompt("Type       : ", "", SEARCH.type);  wc.trim(SEARCH.type);  // 5  
    SEARCH.proj   = wc.console_write_prompt("Proj       : ", "", SEARCH.proj);  wc.trim(SEARCH.proj);  // 6  
    SEARCH.tags   = wc.console_write_prompt("Tags       : ", "", SEARCH.tags);  wc.trim(SEARCH.tags);  // 7  
    SEARCH.cls    = wc.console_write_prompt("Class      : ", "", SEARCH.cls);   wc.trim(SEARCH.cls);   // 8  
    SEARCH.dd     = wc.console_write_prompt("DataDirs   : ", "", SEARCH.dd);    wc.trim(SEARCH.dd);    // 9  
    SEARCH.kp     = wc.console_write_prompt("KeyPairs   : ", "", SEARCH.kp);    wc.trim(SEARCH.kp);    //10  
    SEARCH.cust   = wc.console_write_prompt("Custom     : ", "", SEARCH.cust);  wc.trim(SEARCH.cust);  //11  
    SEARCH.lids   = wc.console_write_prompt("LinkedIds  : ", "", SEARCH.lids);  wc.trim(SEARCH.lids);  //12  

    wibble::WibbleRecord::remove_whitespace(SEARCH.type);
    wibble::WibbleRecord::remove_whitespace(SEARCH.proj);
}


/**
 * @brief Present a numbered file selection list based on an input file list.
 * @param cache_dir string with path to temporary working directory
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param file_list string with input file to parse, one line per file/directory
 * @param sub_ptn string with optional placeholder value to look for
 * @param sub_expansion string with target value to insert for placeholder/template value
 */
bool wibble::WibbleUtility::open_file_selector(const std::string& cache_dir, WibbleConsole& wc, WibbleExecutor& wib_e, const std::string& file_list,
                                               const std::string& sub_ptn, const std::string& sub_expansion, const bool hide_remove_link)
{
    // ensure buffering is synced
    std::cout << std::unitbuf;
    try
    {
        if(! std::filesystem::exists(std::filesystem::path(file_list))) { return false; }

        std::stringstream lf_ss = wibble::WibbleIO::read_file_into_stringstream(file_list);
        
        std::vector<std::string> lfs;
        std::string line;
        unsigned long entries = 0;
        while(std::getline(lf_ss, line, '\n')) 
        {
            ++entries;
            if(sub_ptn != "")
            {
                std::size_t pos   = line.find(sub_ptn);
                std::size_t end_t = pos + sub_ptn.length(); 
                if(pos != std::string::npos && end_t < line.length())
                {
                    std::string nl = line.substr(0, pos) + sub_expansion + line.substr(end_t, line.length() - 1 - pos);
                    line = nl;
                }

            }
            lfs.push_back(line);
        }

        if(entries == 0) { wc.console_write_error_red("No files found/match criteria. Aborting."); return false; }

        unsigned long counter = 0;
        if(ENABLECOLOUR)
        {
            for(const auto& entry: lfs)
            {
                std::cout << rang::fgB::yellow << rang::style::bold << "[";
                std::cout << std::setfill('0') << std::setw(std::to_string(entries).length());
                std::cout << ++counter << "]" << rang::style::reset << " ┃ ";
                std::cout << entry << '\n';
            }

            if(! hide_remove_link)
            {
                std::cout << rang::fgB::yellow << rang::style::bold << "[";
                std::cout << std::setfill(' ') << std::setw(std::to_string(entries).length());
                std::cout << "D" << "]" << rang::style::reset << " ┃ ";
                std::cout << "[ Remove link from list ]" << '\n';
            }

        }
        else
        {
            for(const auto& entry: lfs)
            {
                std::cout << "[" << std::setfill('0') << std::setw(std::to_string(entries).length());
                std::cout << ++counter << "]" << " ┃ ";
                std::cout << entry << '\n';
            }

            if(! hide_remove_link)
            {
                std::cout << "[" << std::setfill('0') << std::setw(std::to_string(entries).length());
                std::cout << "D" << "]" << " ┃ ";
                std::cout << "[ Remove link from list ]" << '\n';
            }
        }

        std::string resp = wc.console_write_prompt("Select item [1:" + std::to_string(entries) + "] > ", "", "");
        unsigned long choice = 0;

        wc.trim(resp);

        // for removing items
        if(! hide_remove_link && (resp == "D" || resp == "d"))
        {
            wc.console_write_yellow("This will remove the link. The underlying file will remain untouched.\n");
            resp = wc.console_write_prompt("Select item to remove [1:" + std::to_string(entries) + "] > ", "", "");
            try { choice = std::stol(resp); } catch (std::exception& ex) { }
            if(choice == 0) { wc.console_write_error_red("Invalid selection, aborting."); return true; }
            else
            {
                return wibble::WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, lfs.at(choice - 1), file_list, true); 
            }
        }
        
        try { choice = std::stol(resp); } catch (std::exception& ex) { }
        //unsigned long choice = wc.console_present_record_selector(entries);
        if(choice == 0) { wc.console_write_error_red("Invalid selection, aborting."); }
        else
        {
            wc.console_write_success_green("Opening file '" + lfs.at(choice - 1) + "'.");
            wib_e.run_exec_expansion(wib_e.get_xdg_open_cmd(), lfs.at(choice - 1));
        }

        return true;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in open_file_selector() " << ex.what() << std::endl;
        return false;
    }

}

/**
 * @brief Present a warning/confirmation before executing a mass batch execution across multiple files.
 * @param wc general console utility object
 * @param exec_cmd_str string with the fully expanded target command proposed to be run
 */
void wibble::WibbleUtility::prompt_confirm_run_batch_exec(WibbleConsole& wc, const std::string exec_cmd_str)
{
    //FIXME: Add flag to skip warning...
    wc.console_write_red("Proposed execution:\n");
    wc.console_write_hz_divider();
    wc.console_write(exec_cmd_str);
    wc.console_write_hz_divider();
    wc.console_write_red_bold("CAUTION. Verify command before proceeding.\n");
    std::string do_exec = wc.console_write_prompt("Confirm execution of above input (y/n) >", "", "");
    if(do_exec != "y" && do_exec != "Y") { wc.console_write_error_red("User aborted."); }
    else
    {
        std::system(exec_cmd_str.c_str());
    }
}

/**
 * @brief Recursive helper function used to remove/delete empty directories; used by "autofile" functionality.
 * @param base_dir root directory to initiate search from
 * @param empty_dirs set of strings with directory paths confirmed empty
 */
std::set<std::string> wibble::WibbleUtility::_do_prune_empty_directories(const std::string& base_dir, std::set<std::string>& empty_dirs)
{
    for(auto const& dir_entry: std::filesystem::recursive_directory_iterator{base_dir})
    {
        if(dir_entry.is_directory())                     { _do_prune_empty_directories(dir_entry.path(), empty_dirs); }
        if(std::filesystem::is_empty(dir_entry.path()))  { empty_dirs.insert(dir_entry.path());                       } 
    }

    return empty_dirs;
}

/**
 * @brief Recursive function used to remove/delete empty directories; maximum depth is limited by deliberate "autofile" path structure/iterations.
 *
 * The "autofile" functionality ONLY creates a directory structure up to 5 deep, before copying/moving content there:  
 * <1:year>/<2:month>/<3:category>/<4:day num>/<5:designated file or directory>  
 * 
 * Directories only become empty AFTER deletion... so we need to keep pruning.
 * 
 * Empty directories further deeper down the tree, within the original autofiled content, are not our concern;
 * in any case, user may very well wish that any given empty directories in the targeted content
 * are retained. This function merely keeps the autofile "parent" structures/managed root part of tree clean.
 * This is needed if user autofiles something, then subsequently deletes it; this may leave empty parent directories
 * no longer needed.
 *
 * @param wc general console utility object
 * @param base_dir root directory to initiate search from
 * @param no_auto flag to control whether empty directory deletion is interactive/confirmed
 */
void wibble::WibbleUtility::prune_empty_directories(WibbleConsole& wc, const std::string& base_dir, const bool no_auto)
{
    wc.console_write("");
    wc.console_write_header("PRUNING");
    wc.console_write("");
    std::set<std::string> remove_list;
    std::set<std::string> empty_dirs;

    // Safety valve/max repetitions (recursion cycles)/max dir depth
    // Targets leaves in tree (empty dirs)
    const int MAXITERS = 5;
    int i = 0;
    bool prune_complete = true;
    do
    {
        ++i;
        empty_dirs.clear();
        remove_list.clear();
        remove_list = _do_prune_empty_directories(base_dir, empty_dirs);
        for(auto const& f: remove_list)
        {
            if(no_auto) // interactive branch
            {
                std::string confirm_delete = wc.console_write_prompt("Empty directory: " + f + "\nDelete above empty directory? (y/n) > ", "", "");
                wc.trim(confirm_delete);
                if(confirm_delete == "y")
                {
                    //std::cout << "Deleting: " << f << '\n';
                    // paranoia double check
                    if(std::filesystem::is_empty(std::filesystem::path(f)))
                    {
                        std::filesystem::remove(std::filesystem::path(f));
                        wc.console_write_green("\n" + ARW + "Deleted empty directory: " + f + "\n\n");
                    }
                }
                else
                {
                    // break loop, user doesn't want to keep pruning
                    prune_complete = false;
                    wc.console_write_red(ARW + "Exiting pruning procedure.\n");
                    i = MAXITERS;
                    break;
                }
            }
            else // danger zone...
            {
                // paranoia double check; should definitely already be empty by this point, but no harm
                // checking again before a potentially highly destructive operation, for safety
                if(std::filesystem::is_empty(std::filesystem::path(f)))
                {
                    std::filesystem::remove(std::filesystem::path(f));
                    wc.console_write_green(ARW + "Deleted empty directory: " + f + '\n');
                }
            }
        }
    }
    while(remove_list.size() > 0 && i < MAXITERS);

    if(prune_complete) { wc.console_write(""); wc.console_write_success_green("All empty autofiled directories pruned under " + base_dir); }
}

/**
 * @brief Helper function that expands/eliminates tildes ("~") and verifies whether resultant filepath exists/is readable.
 * @param wc general console utility object
 * @param filepath string with potential filepath to expand/check
 */
bool wibble::WibbleUtility::check_expand_file_path_or_exit(WibbleConsole& wc, std::string& filepath)
{
    expand_tilde(filepath);
    if(std::filesystem::exists(std::filesystem::path(filepath))) { return true; }
    else
    {
        wc.console_write_error_red("File '" + filepath + "' is not readable/valid. Aborting.");
        throw wibble::wibble_exit(1);
    }
}

/**
 * @brief Generalised function that removes or updates a line, or matching set of lines, with new result or removal.
 * @param cache_dir string with path to temporary working directory
 * @param wc general console utility object
 * @param unique_line_match string with hashed/unique/identifying value to find on given line
 * @param index_fn string with path to file to process
 * @param allow_mult flag to control whether presence of multiple lines matching pattern/hash is considered still valid
 * @param update_line optional string with replacement value for case or updating rather than simply deleting line
 */
bool wibble::WibbleUtility::remove_or_update_line_from_file(const std::string& cache_dir, WibbleConsole& wc,
                                                            const std::string& unique_line_match, const std::string& index_fn,
                                                            const bool allow_mult, const std::string& update_line)
{
    std::stringstream input_ss = wibble::WibbleIO::read_file_into_stringstream(index_fn);
    std::string line;

    int remove_count = 0;
    std::ofstream new_index;
    new_index.open(index_fn + ".new", std::ios::out | std::ios::trunc);
    while(std::getline(input_ss, line, '\n')) 
    {
        // no match, so just write the line
        if(line.find(unique_line_match) == std::string::npos)
        {
            new_index << line + '\n';
        }
        else
        {
            if(update_line != "")
            {
                new_index << update_line + '\n';
            }
            // remove count should only ever be ONE if we have properly supplied a unique deletion hash
            ++remove_count;
            continue;
        }
    } 

    // close up resources
    std::stringstream().swap(input_ss);
    new_index.close();

    bool proceed = false;

    // an override to allow deletion of multiple lines in case of non-unique hash
    if(remove_count > 1 && allow_mult == true) { remove_count = 1; }
    
    if(remove_count == 0)      { wc.console_write_error_red("No deletion candidate found. Index left untouched.");                    }
    else if(remove_count > 1)  { wc.console_write_error_red("Error: multiple deletion candidate lines found. Index left untouched."); }
    else if(remove_count == 1) { proceed = true; }

    if(proceed) { return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, index_fn + ".new", index_fn); }
    else
    {
        // cleanup redundant "*.new" file
        if(std::filesystem::exists(std::filesystem::path(index_fn + ".new")))
        { std::filesystem::remove(std::filesystem::path(index_fn + ".new")); }
        return false;
    }
}


/**
 * @brief Generalised function that merges two index files based on the unique key hash lines
 * @param cache_dir string with path to temporary working directory
 * @param wc general console utility object
 * @param target_file the destination file to be merged into
 * @param src_file the file to merge entries from
 * @param overwrite_collisions whether to overwrite collided keys with new entry value or skip
 */
bool wibble::WibbleUtility::merge_index_file_entries(const std::string& cache_dir, WibbleConsole& wc,
                                                     const std::string& target_file, const std::string& src_file,
                                                     const bool overwrite_collisions)
{
    std::stringstream target_ss = WibbleIO::read_file_into_stringstream(target_file);
    std::stringstream src_ss    = WibbleIO::read_file_into_stringstream(src_file);
    
    std::string target_line;
    std::string src_line;

    auto change_count = 0;
    std::ofstream new_index;
    new_index.open(target_file + ".new", std::ios::out | std::ios::trunc);
    
    // pass 1: preserve original lines, update any changed lines
    while(std::getline(target_ss, target_line, '\n')) 
    {
        while(std::getline(src_ss, src_line, '\n'))
        {
            auto index_col = src_line.find("|");
            if(index_col != std::string::npos)
            {
                const std::string hash_id = src_line.substr(0, index_col);
                if(target_line.find(hash_id) == std::string::npos)
                {
                    continue;
                }
                else
                {
                    if(overwrite_collisions)
                    {
                        target_line = src_line;
                        ++change_count;
                    }
                }
            }
        }
        
        new_index << target_line + '\n';
        // clear the eofbit and failbit
        src_ss.clear();
        src_ss.seekg(0);
    } 
    
    // ensure streams are reset
    target_ss.clear();
    target_ss.seekg(0);
    src_ss.clear();
    src_ss.seekg(0);    
    
    wc.console_write_yellow(ARW + "[MERGE] Lines overwritten: " + std::to_string(change_count) + "\n");
    
    // pass 2: add any new lines
    auto new_entries = 0;
    while(std::getline(src_ss, src_line, '\n')) 
    {
        auto index_col = src_line.find("|");
        if(index_col != std::string::npos)
        {
            const std::string hash_id = src_line.substr(0, index_col);
            auto new_entry = true;
            while(std::getline(target_ss, target_line, '\n'))
            {
                if(target_line.find(hash_id) != std::string::npos)
                {
                    new_entry = false;
                    break;
                }
            }
            if(new_entry)
            {
                ++new_entries;
                new_index << src_line + '\n';
            }
            
            // clear the eofbit and failbit
            target_ss.clear();
            target_ss.seekg(0);
        }
    }   
   
    wc.console_write_yellow(ARW + "[MERGE] New entries added: " + std::to_string(new_entries) + "\n");
    
    // close up resources
    std::stringstream().swap(target_ss);
    std::stringstream().swap(src_ss);
    new_index.close();
    
    wc.console_write_green(ARW + "[MERGE] Merge finished on file: " + target_file + "\n");
    return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, target_file + ".new", target_file);
}

/**
 * @brief Trim a trailing slash from a filepath to standardise path format/specification.
 * @param path string containing the path to check/potentially remove slash from
 */
void wibble::WibbleUtility::remove_trailing_slash(std::string& path)
{
    if     (path.length() <= 1 && path.back() == '/') { path = "";                                }
    else if(path.back() == '/')                       { path = path.substr(0, path.length() - 1); }
}

/**
 * @brief Helper function to allow Wibble++ to change directory upon exit; requires some user bashrc aliases to work.
 * @param dest_dir string containing new path/enviroment value of new target directory
 */
void wibble::WibbleUtility::cd_helper(const std::string &dest_dir)
{
    try
    {
        std::string CF_DIR = "~/.config/wibble";
        expand_tilde(CF_DIR);
        WibbleIO::create_path(CF_DIR);
        std::string CD_FILE = CF_DIR + "/current_dir.wib";
        std::string dest = dest_dir == "" ? "WIB_DIR='.'" : "WIB_DIR='" + dest_dir + "'";
        expand_tilde(dest);
        std::ofstream(CD_FILE).write(dest.c_str(), dest.length());
    }
    catch(std::exception& ex)
    { std::cerr << "Critical error in cd_helper: " << ex.what() << '\n'; throw wibble::wibble_exit(1); }
}

/**
 * @brief Helper function to allow Wibble++ to enter Node directory (or data directory) upon exit; requires some user bashrc aliases to work.
 * @param wib_e WibbleExecutor general environment/execution object
 * @param w WibbleRecord with record we wish to cd into (data or note directory)
 * @param archived flag controlling whether record is to be found in archived database or not
 * @param txt_not_data flag controlling whether to return Node containing directory (of text file), or Node data directory
 */
void wibble::WibbleUtility::cd_record(WibbleExecutor& wib_e, const WibbleRecord w, const bool archived, const bool txt_not_data)
{
    std::string dest = wib_e._build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, txt_not_data);
    if(txt_not_data) { dest = WibbleIO::get_parent_dir_path(dest); }
    cd_helper(dest);
}
