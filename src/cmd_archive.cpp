// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_archive.cpp
 * @brief     Handle "archive" subcommand.
 * @details
 *
 * This static function simply dispatches out to the underlying functions
 * within WibbleExecutor to actually perform the archiving/unarchiving
 * operations.
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n 
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>  
 * 
 * 
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *  
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr. 
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_archive.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Dispatching logic for the "archive" operation on Wibble nodes
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 * @param ARCHIVE command line options/parameters supplied via switches
 */
void wibble::WibbleCmdArchive::handle_archive(pkg_options& OPTS, search_filters& SEARCH, archive_options ARCHIVE)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);
    WibbleRecord w;
    //wib_e.display_configuration();

    if(SEARCH.archive_db == true)
    {
        wc.console_write_green(ARW + "NOTE: Archive option specified to 'archive' sub-command has no effect.\n");
        wc.console_write_green(ARW + "Use '--restore' switch to 'archive' sub-command if restoring a note from the archive database to main database.\n"); 
        wc.console_write_green(ARW + "'archive' sub-command by default will move selected note from main database to archive database.\n");
    }

    // are we restoring (i.e. moving) an archived node back to the main database?
    if(! ARCHIVE.restore_from_archive) { SEARCH.archive_db = false;  }
    else
    {
        wc.console_write_green(ARW + "Restore option selected. Record will be restored from the archive database back into the main database.\n");
        SEARCH.archive_db = true;
    }

    w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH); 

    // dispatch to WibbleExecutor to actually perform the operation
    bool archive_result = wib_e.archive_note_confirm(w, SEARCH.archive_db, OPTS.paths.wibble_store, OPTS.files.main_db, OPTS.files.archived_db);
    if(! archive_result)
    {
        if(! ARCHIVE.restore_from_archive) { wc.console_write_error_red("Error archiving Node.");    }
        else                               { wc.console_write_error_red("Error un-archiving Node."); }
    }
    else
    {
        if(! ARCHIVE.restore_from_archive) { wc.console_write_success_green("Node successfully archived.");             }
        else                               { wc.console_write_success_green("Node successfully un-archived/restored."); }
    }
} 
