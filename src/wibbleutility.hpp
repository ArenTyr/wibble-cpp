/**
 * @file      wibbleutility.hpp
 * @brief     General purpose library file with many shared functions (header file).
 * @details
 *
 * Many static methods to perform common operations used in typically multiple
 * disparate functions/places.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEUTILITY_H
#define WIBBLEUTILITY_H

#include <set>

#include "wibbleexecutor.hpp"

namespace wibble
{
    class WibbleUtility
    {
    private:
        static std::set<std::string> _do_prune_empty_directories(const std::string& base_dir, std::set<std::string>& empty_dirs);
        template <typename InputIt1, typename OutputIt, typename T, typename InputIt2>
        static OutputIt _replace_with_range_copy(InputIt1 first, InputIt1 last, OutputIt d_first, const T& old_value, InputIt2 new_first, InputIt2 new_last);
        static void _replace_char(std::string& input, const std::string& replacement_string, char char_to_replace);

    public:
        static void cd_helper(const std::string &dest_dir = "");
        static void cd_record(WibbleExecutor& wib_e, const WibbleRecord w, const bool archived, const bool txt_not_data);
        static void build_batch_exec_cmd(std::string& exec_cmd, std::string& fn_list);
        static void confirm_file_move_or_exit(WibbleConsole& wc, const std::string& src, const std::string& dest, const bool bypass = false);
        static bool check_prompt_set_exec_cmds_scripts(WibbleConsole& wc, const std::string& scripts_base_dir, const bool& run_std_cmd,
                                                       const bool& run_scr_cmd, std::string& exec_cmd, std::string& exec_script);

        static std::string find_file_wibble_desc(const std::string& input_file);
        static void set_freeplane_dirs(std::filesystem::path& src_mm_dir, std::filesystem::path& dest_mm_dir);
        static void menu_search_set_filters(WibbleConsole& wc, search_filters& SEARCH);
        static void prompt_confirm_run_batch_exec(WibbleConsole& wc, const std::string exec_cmd_str);
        static bool open_file_selector(const std::string& cache_dir, WibbleConsole& wc, WibbleExecutor& wib_e, const std::string& file_list,
                                       const std::string& sub_ptn = "", const std::string& sub_expansion = "", const bool hide_remove_link = false);
        static bool check_string_has_content(const std::string& s_input);
        static bool check_expand_file_path_or_exit(WibbleConsole& wc, std::string& filepath);
        static bool create_backup_and_overwrite_file(const std::string& bkup_dir, const std::string& new_file,
                                                     const std::string& orig_file);
        static void dev_warning(WibbleConsole& wc, const std::string& feature_name, const std::string& eta);
        static void expand_tilde(std::string& fs_path);
        static void fix_file_param_get_input(WibbleConsole& wc, std::string& input); 
        
        static std::map<int, std::pair<std::string, std::pair<std::string, std::string>>> export_binary_tool_mapping(WibbleConsole& wc, const std::string& bin_mapping);
        static std::string get_binary_tool(WibbleConsole& wc, const std::string& ftype, const std::string& bin_mapping, const bool edit_not_view);
        static WibbleRecord get_record_for_id(const std::string node_id, WibbleConsole& wc, WibbleExecutor& wib_e, const bool archive_search = false);
        static WibResultList get_wibble_records_fulltext(WibbleConsole& wc, WibbleExecutor& wib_e, const std::string rg_exp, const std::string& node_root_path,
                                                         const std::string& WORKFILE, const bool archived_nodes, const bool fixed_str_search); 
        static WibbleRecord get_record_interactive(WibbleConsole& wc, WibbleExecutor& wib_e, search_filters& SEARCH);
        static WibResultList get_records_multi_interactive(WibbleConsole& wc, WibbleExecutor& wib_e, search_filters& SEARCH, int opt_correct = 0);
        static WibResultList get_record_set(WibbleExecutor& wib_e, search_filters& SEARCH);
        static void prune_empty_directories(WibbleConsole& wc, const std::string& base_dir, const bool no_auto = true);
        static void remove_trailing_slash(std::string& path);
        static bool remove_or_update_line_from_file(const std::string& cache_dir, WibbleConsole& wc, const std::string& unique_line_match,
                                                    const std::string& index_fn, const bool allow_mult = false, const std::string& update_line = "");
        static bool merge_index_file_entries(const std::string& cache_dir, WibbleConsole& wc, const std::string& target_file,
                                             const std::string& src_file, const bool overwrite_collisions);
    };
}
#endif
