/**
 * @file      wibbledbqueryinterface.hpp
 * @brief     Interface/parent virtual class for definining core database typology (header file).
 * @details
 *
 * Specifies core fields/field specifications and querying interface
 * for core database plain-text format.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_DBQUERYIFACE_H
#define WIBBLE_DBQUERYIFACE_H

#include <string>

namespace wibble
{
    class WibbleDBQueryInterface
    {
    public:
        // ensure object resources get cleaned when polymorphic child instances are deleted
        virtual ~WibbleDBQueryInterface() {};
        std::string date_q;
        std::string title_q;
        std::string id_q;
        std::string desc_q;
        std::string type_q;
        std::string proj_q;
        std::string tags_q;
        std::string cls_q;
        std::string dd_q;
        std::string kp_q;
        std::string cust_q;
        std::string lids_q;

        bool dump_all;
        bool exact_search;
        bool fixed_strings;
        
        // field character indices/"width" ("Date: ", "Title: ", etc.)
        /**
         * @brief Database field "widths"/substring indexes for pattern matching correct field name; "Date:", "Title:" etc.
         */
        enum WIB_INDICES { W_DATE = 6,  W_TITLE = 7,  W_NOTEID = 8, W_DESC = 13,
                           W_TYPE = 10, W_PROJ = 9,   W_TAGS = 6,   W_CLS = 7,
                           W_DD = 10,   W_KP = 10,    W_CUST = 8,   W_LIDS = 11 };

        // field identifiers for switch statements
        /**
         * @brief Database field enumeration.
         */
        enum WIB_FIELDS { F_DATE, F_TITLE, F_NOTEID, F_DESC,
                          F_TYPE, F_PROJ,  F_TAGS,   F_CLS,
                          F_DD,   F_KP,    F_CUST,   F_LIDS};

        // default to title search
        WIB_FIELDS active_search_field = F_TITLE;

        virtual bool set_query_expression_for_field(WIB_FIELDS, std::string) = 0; 
        virtual std::string generate_query() const = 0;
        virtual long get_existing_record_line_num(const std::string&, const std::string&) const = 0;
    };

    typedef WibbleDBQueryInterface::WIB_FIELDS SEARCH_FIELD;
}
#endif
