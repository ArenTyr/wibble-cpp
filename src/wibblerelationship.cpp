/**
 * @file      wibblerelationship.cpp
 * @brief     Provides functionality for Node relationships.
 * @details
 *
 * Management class for defining, retrieving, and editing Node relationships.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_structs.hpp"
#include "wibbleexit.hpp"
#include "wibblerelationship.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"

namespace wibble
{
    using rel_nodes = WibbleRelationship::related_nodes;
    using rel_type = WibbleRelationship::relationship_type;
}

/**
 * @brief Add a Node relationship definition to the set of relations for a Node (wrapper for single entry).
 * @param record the WibbleRecord/Node to update the metadata relationship into 
 * @param fn path/filename to Node specific related Nodes list
 * @param new_rltn the new relationship/Node linkage record/entry to add
 * @param archived flag indicating whether to operate on archived Nodes/database
 */
void wibble::WibbleRelationship::_append_to_rl_list(WibbleRecord& record, const std::string& fn, const std::string& new_rltn, const bool archived)
{
    std::set<std::string> add_list;
    add_list.insert(new_rltn);
    return _append_to_rl_list(record, fn, add_list, archived);
}

/**
 * @brief Add Node relationship[s] definition[s] to the set of relations for a Node.
 * @param record the WibbleRecord/Node to update the metadata relationship into 
 * @param fn path/filename to Node specific related Nodes list
 * @param new_rltn set of relationships/Node linkage record/entries to add
 * @param archived flag indicating whether to operate on archived Nodes/database
 */
void wibble::WibbleRelationship::_append_to_rl_list(WibbleRecord& record, const std::string& fn, const std::set<std::string>& new_rltn, const bool archived)
{
    bool add_new_rl_line = false;
    std::string flatten;
    //std::cout << "DEBUG: Attempting to update file: " << fn << std::endl;
    if(! std::filesystem::exists(std::filesystem::path(fn))) //initialising new file, first relationship
    {
        WibbleIO::create_path(WibbleIO::get_parent_dir_path(fn));
        for(auto const& ln: new_rltn) { flatten.append(ln + '\n'); }
        add_new_rl_line = WibbleIO::write_out_file_overwrite(flatten, fn);
    }
    else
    {
        // read in existing lines first
        try
        {
            std::stringstream rl_ss{WibbleIO::read_file_into_stringstream(fn)};
            std::string line;
            std::set<std::string> rels;
            // use set to enforce de-duplication
            while(std::getline(rl_ss, line)) { rels.insert(line); }
            std::stringstream().swap(rl_ss);

            // add in new node list, de-duplicating via set
            for(auto const& nd: new_rltn) { rels.insert(nd); }
            // flatten into output multiline string
            for(auto const& ln: rels) { flatten.append(ln + '\n'); }

            add_new_rl_line = WibbleIO::write_out_file_overwrite(flatten, fn);
        }
        catch(std::exception& ex)
        {
            std::cerr << "Error in _append_to_rl_list: " << ex.what() << std::endl;
            throw wibble::wibble_exit(1);
        }
    }

    if(add_new_rl_line)
    {
        std::string rec_ident = REC_IDENT;
        record.set_lids(std::move(rec_ident));
        wib_e.op_update_existing_record(record, archived);
    }
}

/**
 * @brief Helper function to obtain the filepath to the Node specific Node relationship file.
 * @param record the WibbleRecord/Node which is having a relationship defined for
 * @param archived flag indicating whether to operate on archived Nodes/database
 */
std::string wibble::WibbleRelationship::determine_rn_filename(WibbleExecutor& we, const WibbleRecord& record, const bool archived)
{

    return we._build_note_path(record.get_id(), record.get_type(), record.get_proj(), archived, false) + WIB_HID_DIR + REL_NODES;
}

/**
 * @brief Output function to display the relationships to console.
 * @param nds the related_nodes struct containing all of the Node relationships for this Node
 * @param expanded flag indicating whether to display in verbose/expanded manner
 */
void wibble::WibbleRelationship::_display_relationship(const related_nodes& nds, const bool expanded)
{
    unsigned long i = 0;
    wc.console_write_green_bold("\n━━━━━━━━━━━━━━━━━━━━ [ PARENT OF ] ━━━━━━━━━━━━━━━━━━━━\n\n");

    // this could theoretically "wrap around", but only in case of an insane number of linked nodes
    std::size_t tot_count = _rel_count(nds);
    if(nds.parent_ids.size() == 0)
    {
        wc.console_write("[ NONE ]");
    }
    else
    {
        for(auto const& n: nds.parent_ids)
        {
            wc.silence_output();
            WibbleRecord w = _get_record_for_id(n);
            wc.enable_output();
            if(expanded) { w.pretty_print_record_full(i, tot_count); }
            else         { w.pretty_print_record_condensed(i, tot_count); }
            ++i;
        }
        wc.console_write("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    }
    i = 0;
    wc.console_write_green_bold("\n━━━━━━━━━━━━━━━━━━━━━ [ SIBLINGS ] ━━━━━━━━━━━━━━━━━━━━\n\n");
    if(nds.sibling_ids.size() == 0)
    {
        wc.console_write("[ NONE ]");
    }
    else
    {
        for(auto const& n: nds.sibling_ids)
        {
            wc.silence_output();
            WibbleRecord w = _get_record_for_id(n);
            wc.enable_output();
            if(expanded) { w.pretty_print_record_full(i, tot_count); }
            else         { w.pretty_print_record_condensed(i, tot_count); }
            wc.enable_output();
            ++i;
        }
        wc.console_write("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    }

    i = 0;
    wc.console_write_green_bold("\n━━━━━━━━━━━━━━━━━━━━  [ CHILD OF ] ━━━━━━━━━━━━━━━━━━━━\n\n");
    if(nds.child_ids.size() == 0)
    {
        wc.console_write("[ NONE ]");
    }
    else
    {
        for(auto const& n: nds.child_ids)
        {
            wc.silence_output();
            WibbleRecord w = _get_record_for_id(n);
            wc.enable_output();
            if(expanded) { w.pretty_print_record_full(i, tot_count); }
            else         { w.pretty_print_record_condensed(i, tot_count); }
            wc.enable_output();
            ++i;
        }
        wc.console_write("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    }

    wc.console_write_green_bold("\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n\n");
}


/**
 * @brief Wrapper function to display the relationships to console in condensed/compact view.
 * @param nds the related_nodes struct containing all of the Node relationships for this Node
 */
void wibble::WibbleRelationship::_display_compact_rlt(const related_nodes& nds)
{
    _display_relationship(nds, false);
}

/**
 * @brief Wrapper function to display the relationships to console in expanded/full view.
 * @param nds the related_nodes struct containing all of the Node relationships for this Node
 */
void wibble::WibbleRelationship::_display_full_rlt(const related_nodes& nds)
{
    _display_relationship(nds, true);
}

/**
 * @brief Wrapper function to confirm multi-selection of candidate Nodes before relationship operation.
 * @param selection the list/pointer to WibbleRecord vector that contains set of Node results
 */
void wibble::WibbleRelationship::_check_and_display(const WibbleRecord& w, WibResultList& selection)
{
    if(selection->size() == 0) { wc.console_write_error_red("No valid Nodes selected. Aborting."); throw wibble::wibble_exit(0); }

    // do not bother with additional display just for a single record
    if(selection->size() > 1)
    {
        _display_selection(w, selection);
        std::string resp = wc.console_write_prompt("Proceed using above Nodes? (y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); throw wibble::wibble_exit(0); }
    }
}

/**
 * @brief Display the list of targeted Nodes that will be used for the relationship operation.
 * @param selection the list/pointer to WibbleRecord vector that contains set of Node results
 */
void wibble::WibbleRelationship::_display_selection(const WibbleRecord& w, WibResultList& selection)
{
    wc.console_write_hz_divider();
    std::size_t tot = selection->size();
    unsigned long i = 0;
    for(auto const& record: *selection)
    {
        if(record.get_id() != w.get_id())
        {
            record.pretty_print_record_condensed(i, tot);
            ++i;
        }
    }
    wc.console_write("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
}

/**
 * @brief Wrapper around convenience function to retrieve WibbleRecord/Node for a given ID.
 * @param node_id the unique ID/hash for this particular Node
 */
wibble::WibbleRecord wibble::WibbleRelationship::_get_record_for_id(const std::string& node_id)
{
    return WibbleUtility::get_record_for_id(node_id, wc, wib_e);
}

/**
 * @brief Display short summary information regarding the Node's linkages/relationships.
 * @param nds the related_nodes struct containing all of the Node relationships for this Node
 */
void wibble::WibbleRelationship::_display_stats(const related_nodes& nds)
{
    wc.console_write_hz_divider();
    wc.console_write_yellow_bold("Parent of : "); wc.console_write(std::to_string(nds.parent_ids.size()) + " nodes.");
    wc.console_write_yellow_bold("Sibling of: "); wc.console_write(std::to_string(nds.sibling_ids.size()) + " nodes.");
    wc.console_write_yellow_bold("Child of  : "); wc.console_write(std::to_string(nds.child_ids.size()) + " nodes.");
    wc.console_write_hz_divider();
    if(nds.parent_ids.size()  == 0 &&
       nds.sibling_ids.size() == 0 &&
       nds.child_ids.size()   == 0)
    { wc.console_write_yellow_bold("Node currently has no defined relationships.\n"); }
    else
    {
        std::size_t tot = nds.parent_ids.size() + nds.sibling_ids.size() + nds.child_ids.size();
        wc.console_write_yellow_bold("Node currently has " + std::to_string(tot) + " defined relationships.\n");
    }
}

/**
 * @brief Interactive menu to dispatch the various relationship operations.
 * @param record the WibbleRecord/Node to perform the relationship function on
 */
void wibble::WibbleRelationship::relationship_menu(WibbleRecord& w, const bool archived)
{
    std::cout << std::unitbuf;
    wc.console_write_header("RELATIONS");
    wc.console_write_yellow_bold("1. "); wc.console_write_yellow(" Show relationships (expanded)\n");
    wc.console_write_yellow_bold("2. "); wc.console_write_yellow(" Show relationships (condensed)\n");
    wc.console_write_yellow_bold("3. "); wc.console_write_yellow(" Add relationship as 'Parent' Node\n");
    wc.console_write_yellow_bold("4. "); wc.console_write_yellow(" Add relationship as 'Sibling' Node\n");
    wc.console_write_yellow_bold("5. "); wc.console_write_yellow(" Add relationship as 'Child' Node\n");
    wc.console_write_yellow_bold("6. "); wc.console_write_yellow(" Remove/delete individual relationship\n");
    wc.console_write_yellow_bold("7. "); wc.console_write_yellow(" Remove/delete "); wc.console_write_yellow_bold("ALL ");
                                         wc.console_write_yellow("relationships on Node\n");
    wc.console_write("\n");
    std::string resp = wc.console_write_prompt("Select option [1:6] > ", "", "");
    try
    {
        wc.trim(resp);
        wc.console_write("\n");
        int choice = std::stoi(resp);
        switch(choice)
        {
        case 1:
            display_node_relationships(w, archived, true);
            break;
        case 2:
            display_node_relationships(w, archived, false);
            break;
        case 3:
            add_node_relationships(w, relationship_type::PARENT_OF);
            break;
        case 4:
            add_node_relationships(w, relationship_type::SIBLING_OF);
            break;
        case 5: 
            add_node_relationships(w, relationship_type::CHILD_OF);
            break;
        case 6:
            remove_node_relationship(w, archived);
            break;
        case 7:
            resp = wc.console_write_prompt("This will detach all relationships from the Node. Proceed? (y/n) > ", "", "");
            if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); throw wibble::wibble_exit(0); }
            remove_all_relationships_for_record(w, archived);
            break;
        default:
            throw std::invalid_argument("Unknown option");
        }
    }
    catch(std::exception& ex) { wc.console_write_error_red("Invalid selection. Exiting."); }
}


bool wibble::WibbleRelationship::remove_all_relationships_for_record(WibbleRecord& record, const bool archived)
{
    std::cout << std::unitbuf;
    rel_nodes nds =_get_related_nodes(record, archived);
    std::size_t all_rel_count = _rel_count(nds);

    if(all_rel_count == 0)
    {
        wc.console_write_error_red("Node has no relationships currently defined. Nothing to remove.");
        return false;
    }
    
    wc.console_write_yellow_bold("Node currently has " + std::to_string(all_rel_count) + " relationship(s) defined.\n\n");
    wc.console_write_hz_divider();
    wc.console_write_red_bold("WARNING. All/[" + std::to_string(all_rel_count) + "] relationship(s) will be permanent deleted. This cannot be undone.\n");
    wc.console_write_hz_divider(); 

    std::string resp = wc.console_write_prompt("Input 'delete' to proceed > ", "", "");
    wc.console_write("\n");

    if(resp != "delete") { wc.console_write_error_red("User cancelled."); return false; }

    try
    {
        for(auto const& rl: nds.parent_ids)
        {
            bool no_rels_left = all_rel_count <= 1;
            bool c = _purge_relationships_for_id(record, rl, archived, "P|", no_rels_left);
            if(c){ --all_rel_count;                          }
            else { throw std::invalid_argument("Bad purge"); }
        }
        
        for(auto const& rl: nds.sibling_ids)
        {
            bool no_rels_left = all_rel_count <= 1;
            bool c = _purge_relationships_for_id(record, rl, archived, "S|", no_rels_left);
            if(c){ --all_rel_count;                          }
            else { throw std::invalid_argument("Bad purge"); }
        }

        for(auto const& rl: nds.child_ids)
        {
            bool no_rels_left = all_rel_count <= 1;
            bool c = _purge_relationships_for_id(record, rl, archived, "C|", no_rels_left);
            if(c){ --all_rel_count;                          }
            else { throw std::invalid_argument("Bad purge"); }
        }
    }
    catch(std::exception& ex) { std::cerr << "Error in remove_all_relationships(): << " << ex.what() << std::endl; return false; }

    wc.console_write_success_green("Node relationships removed successfully.");

    return true;
}

/**
 * @brief Remove/delete a defined relationship between two Nodes.
 *
 * The function needs to ensure that both pairwise relations are deleted;
 * i.e. in case of parent-child, the parent linkage needs removing, and the
 * corresponding child linkage needs removing. Finally, a check needs to be
 * made to see if there are any remaining associations for the two given Nodes;
 * if there are none, the main database file needs to be updated to indicate that
 * the Node has no relationships, so the related "flair" does not show in the listing.
 * 
 * @param record the WibbleRecord/Node to perform the relationship function on
 * @param archived flag indicating whether to operate on archived Nodes/database
 */
bool wibble::WibbleRelationship::remove_node_relationship(WibbleRecord& record, const bool archived)
{
    std::cout << std::unitbuf;
    rel_nodes nds =_get_related_nodes(record, archived);
    std::size_t all_rel_count = _rel_count(nds);
    _display_compact_rlt(nds);
    wc.console_write("\n");
    wc.console_write_hz_heavy_divider();
    wc.console_write_yellow("Select relationship type to remove:\n\n");
    if(nds.parent_ids.size() > 0)  { wc.console_write_yellow_bold("[P]"); wc.console_write(" to remove a defined parent relationship.");  }
    if(nds.sibling_ids.size() > 0) { wc.console_write_yellow_bold("[S]"); wc.console_write(" to remove a defined sibling relationship."); }
    if(nds.child_ids.size() > 0)   { wc.console_write_yellow_bold("[C]"); wc.console_write(" to remove a defined child relationship.");   }

    std::string resp = wc.console_write_prompt("\n> ", "", "");
    wc.trim(resp);
    std::vector<std::string> choices;
    std::string prefix;
    if((resp == "P" || resp == "p") && nds.parent_ids.size() > 0 )      { choices = nds.parent_ids;  prefix = "P|"; }
    else if((resp == "S" || resp == "s") && nds.sibling_ids.size() > 0) { choices = nds.sibling_ids; prefix = "S|"; }
    else if((resp == "C" || resp == "c") && nds.child_ids.size() > 0  ) { choices = nds.child_ids;   prefix = "C|"; }
    else { wc.console_write_error_red("Invalid selection, exiting."); throw wibble::wibble_exit(1);  }

    wc.console_write("\n");
    unsigned long i = 0; std::size_t tot = choices.size();
    for(auto const& item: choices)
    {
        wc.silence_output();
        WibbleRecord w = _get_record_for_id(item);
        wc.enable_output();
        w.pretty_print_record_condensed(i, tot); 
        wc.enable_output();
        ++i;
    }
         
    resp = wc.console_write_prompt("\nSelect relationship number to permanently remove\n> ", "", "");
    try
    {
        wc.trim(resp);
        unsigned long l = std::stol(resp);
        if(l == 0 || l > choices.size()) { throw std::invalid_argument("Bad choice"); }
        const bool no_rels_left = all_rel_count <= 1;
        return _purge_relationships_for_id(record, choices.at(l - 1), archived, prefix, no_rels_left);
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid choice. Exiting.");
        wibble::wibble_exit(1);
    }

    return false;
}

/**
 * @brief Simple function to generate the correct relationship entry.
 * @param trgt_id the Node ID of the destination/linked Node
 * @param R relationship_type enum switch to ensure corect identifer (P = Parent, S = Sibling, C = Child)
 */
std::string wibble::WibbleRelationship::_define_node_relationship(const std::string& trgt_id, relationship_type R)
{
    switch(R)
    {
    case PARENT_OF:
        return "P|" + trgt_id;
    case SIBLING_OF:
        return "S|" + trgt_id;
    case CHILD_OF:
        return "C|" + trgt_id;
    default:
        std::cerr << "Invalid node relationship type" << std::endl;
        throw wibble::wibble_exit(1);
    }
}

/**
 * @brief Interactive menu to dispatch the various relationship operations.
 * @param record the WibbleRecord/Node to perform the relationship function on
 * @param archived flag indicating whether to operate on archived Nodes/database
 */
wibble::rel_nodes wibble::WibbleRelationship::_get_related_nodes(const WibbleRecord& record, const bool archived)
{
    rel_nodes nds;
    const std::string related_nodes_fn = determine_rn_filename(wib_e, record, archived);
    if(record.get_lids() != "___NULL__" && std::filesystem::exists(std::filesystem::path(related_nodes_fn)))
    {
        try
        {
            std::stringstream nodes_ss = WibbleIO::read_file_into_stringstream(related_nodes_fn);
            std::string line;
            while(std::getline(nodes_ss, line))
            {
                std::size_t sep = line.find(FIELD_DELIM);
                std::string node_id = line.substr(sep + 1, line.length() - sep - 1);
                wc.trim(node_id);
                if(sep == std::string::npos || node_id == "" ) { throw std::invalid_argument("Corrupted relationship file"); }
                if(line.at(0) == 'P')      { nds.parent_ids.push_back(node_id);  }
                else if(line.at(0) == 'S') { nds.sibling_ids.push_back(node_id); }
                else if(line.at(0) == 'C') { nds.child_ids.push_back(node_id);   }
            
            }
            std::stringstream().swap(nodes_ss);
        }
        catch(std::exception& ex) { wc.console_write_error_red("Error processing related Nodes list."); }
    }
  
    return nds; 
}

/**
 * @brief Main function to define a new Node relationship, or set of relations.
 *
 * One user has selected their "target" Node, they have the option of using the
 * menu filtered results to select all displayed candidates ('*'). Doing so means
 * we have a 1-to-N update situation.
 *
 * Two steps:  
 * 1. Identify and update other Node of pair related list (i.e. the "destination"), for all results  
 * 2. Update our target/source node related list file with relationship.  
 *
 * Each step above requires updating two files:  
 * a) The Node related list (at destination)  
 * b). The main database file with the update to "LinkedIds" field   
 *
 * Finally we also just need to udpate the target related list, which can be done in one hit.  
 * 
 * @param record the WibbleRecord/Node to perform the relationship function on
 * @param R relationship_type enum with type of relationship we are defining from source to sinks
 */
bool wibble::WibbleRelationship::add_node_relationships(WibbleRecord& record, relationship_type R)
{
    WibResultList results;
    search_filters SEARCH;
    std::set<std::string> src_node_rel_list;
    std::string dest_node_rel;

    std::cout << std::unitbuf;
    wibble::WibbleUtility::menu_search_set_filters(wc, SEARCH);

    // for a given type of relationship, iterate over all of the matched destination records
    // and update each of their related Node list file
    switch(R)
    {
    case PARENT_OF:
        results = wibble::WibbleUtility::get_records_multi_interactive(wc, wib_e, SEARCH, 1);
        _check_and_display(record, results);
        // destination nodes are to be defined as children of THIS one
        dest_node_rel = _define_node_relationship(record.get_id(), WibbleRelationship::relationship_type::CHILD_OF);
        for(auto& item: *results)
        {
            // don't relate itself to itself
            if(item.get_id() == record.get_id()) { continue; }
            
            _append_to_rl_list(item, determine_rn_filename(wib_e, item, SEARCH.archive_db), dest_node_rel, SEARCH.archive_db);
            wc.console_write_yellow(ARW + "Child node relationship defined into to " + item.get_id() + ".\n");
            src_node_rel_list.insert(_define_node_relationship(item.get_id(), WibbleRelationship::relationship_type::PARENT_OF));
        }
        break;
     case SIBLING_OF:
        results = wibble::WibbleUtility::get_records_multi_interactive(wc, wib_e, SEARCH, 1);
        _check_and_display(record, results);
        // destination nodes are to be defined as siblings of THIS one
        dest_node_rel = _define_node_relationship(record.get_id(), WibbleRelationship::relationship_type::SIBLING_OF);
        for(auto& item: *results)
        {
            if(item.get_id() == record.get_id()) { continue; }

            _append_to_rl_list(item, determine_rn_filename(wib_e, item, SEARCH.archive_db), dest_node_rel, SEARCH.archive_db);
            wc.console_write_yellow(ARW + "Sibling node relationship defined into to " + item.get_id() + ".\n");
            src_node_rel_list.insert(_define_node_relationship(item.get_id(), WibbleRelationship::relationship_type::SIBLING_OF));
        }
        break;
    case CHILD_OF:
        results = wibble::WibbleUtility::get_records_multi_interactive(wc, wib_e, SEARCH, 1);
        _check_and_display(record, results);
        // destination nodes are to be defined as parents of THIS one
        dest_node_rel = _define_node_relationship(record.get_id(), WibbleRelationship::relationship_type::PARENT_OF);
        for(auto& item: *results)
        {
            if(item.get_id() == record.get_id()) { continue; }

            _append_to_rl_list(item, determine_rn_filename(wib_e, item, SEARCH.archive_db), dest_node_rel, SEARCH.archive_db);
            wc.console_write_yellow(ARW + "Parent node relationship defined into to " + item.get_id() + ".\n");
            src_node_rel_list.insert(_define_node_relationship(item.get_id(), WibbleRelationship::relationship_type::CHILD_OF));
        }
        break;
    }

    // now we can finally just update our target/source Node linked file all in one hit with the set of IDs/relations
    _append_to_rl_list(record, determine_rn_filename(wib_e, record, SEARCH.archive_db), src_node_rel_list, SEARCH.archive_db);
    switch(R)
    {
    case PARENT_OF:
            wc.console_write_yellow(ARW + "Parent node relationships defined for " + record.get_id() + ".\n");
            break;
    case SIBLING_OF:
            wc.console_write_yellow(ARW + "Sibling node relationships defined for " + record.get_id() + ".\n");
            break;
    case CHILD_OF:
            wc.console_write_yellow(ARW + "Child node relationships defined for " + record.get_id() + ".\n");
            break;
    }

    wc.console_write_success_green("Relationships successfully added.");

    return true;
}

/**
 * @brief Wrapper function to display the Node relationships to console.
 * @param record the WibbleRecord/Node to display the relationships for
 * @param archived flag indicating whether to operate on archived Nodes/database
 * @param extended_listing flag to control whether to show the extended/full view
 */
void wibble::WibbleRelationship::display_node_relationships(const WibbleRecord& record, const bool archived, const bool extended_listing)
{
    rel_nodes nds =_get_related_nodes(record, archived); 
    record.pretty_print_record_full();
    if(! extended_listing) { _display_compact_rlt(nds); }
    else                   { _display_full_rlt(nds);    }
    _display_stats(nds);
} 

/**
 * @brief Remove the matched IDs from the relationship files to remove the relationship.
 *
 * Deletion is always pairwise; both source and destination need updating. Either, both,
 * or none will need updating on the database file if they have gone from a state of
 * existing relationships to no relationships.
 *
 * @param record the source WibbleRecord/Node to we are purging against
 * @param node_id the Node identifer/hash to find the target relationship file
 * @param archived flag indicating whether to operate on archived Nodes/database
 * @param prefix relationship type prefix character in order to match correct lines
 * @param no_rels_left flag indicating whether an update to database file is necessary if WibbleRecord/Node will have zero relationships remaining
 */
bool wibble::WibbleRelationship::_purge_relationships_for_id(WibbleRecord& record, const std::string& node_id, const bool archived, const std::string prefix, const bool no_rels_left)
{
    const std::string src_lids_file = determine_rn_filename(wib_e, record, archived);
    bool removal_src = false; 
    if(std::filesystem::exists(std::filesystem::path(src_lids_file)))
    {
        const std::string src_removal_line = prefix + node_id;
        removal_src = wibble::WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, src_removal_line, src_lids_file);
        if( ! removal_src) { wc.console_write_error_red("Error updating relationships for source Node."); return false; }
        wc.console_write_yellow(ARW + "Removed node relationship in source relationship file.\n");
    }
    else
    { wc.console_write_error_red("Source Node relationships file missing."); return false; }

    std::string invert_prefix;
    if(prefix == "P|") { invert_prefix = "C|"; }
    else if(prefix == "S|") { invert_prefix = "S|"; }
    else if(prefix == "C|") { invert_prefix = "P|"; }
    else { return false; }

    wc.silence_output();
    WibbleRecord dr = _get_record_for_id(node_id);
    wc.enable_output();
    const std::string dest_lids_file = determine_rn_filename(wib_e, dr, archived);
    const std::string dest_removal_line = invert_prefix + record.get_id();

    if(std::filesystem::exists(std::filesystem::path(dest_lids_file)))
    {
        bool removal_dest = wibble::WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, dest_removal_line, dest_lids_file);
        if(! removal_dest) { wc.console_write_error_red("Error updating relationships for destination Node."); return false; }
        wc.console_write_yellow(ARW + "Removed node relationship in target relationship file.\n");
    }
    else
    { wc.console_write_error_red("Target Node relationships file missing."); return false; }

    // now check whether to purge metadata in main database
    // 1. src node
    if(no_rels_left)
    {
        std::string rec_ident = NONE_IDENT;
        record.set_lids(std::move(rec_ident));
        bool upd_src = wib_e.op_update_existing_record(record, archived);
        if(! upd_src) { wc.console_write_error_red("Error updating database with new relationship information."); return false; }
    }
    // 2. dest node
    rel_nodes nds =_get_related_nodes(dr, archived);
    std::size_t all_rel_count = _rel_count(nds);
    // note: here should be test for zero, since all line purging is done
    if(all_rel_count == 0)
    {
        std::string rec_ident = NONE_IDENT;
        dr.set_lids(std::move(rec_ident));
        bool upd_dest = wib_e.op_update_existing_record(dr, archived);
        if(! upd_dest) { wc.console_write_error_red("Error updating database with new relationship information."); return false; }
    }
    
    wc.console_write_success_green("Node relationship removed pairwise successfully.");
    return true;
    
}

/**
 * @brief Simple helper function to calculate total number of relationships of any type for given Node.
 * @param nds the related_nodes struct containing all of the Node relationships for this Node
 */
std::size_t wibble::WibbleRelationship::_rel_count(const rel_nodes& nds)
{
    return nds.parent_ids.size() + nds.child_ids.size() + nds.sibling_ids.size();
}
