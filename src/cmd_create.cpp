// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_create.cpp
 * @brief     Handle "create" subcommand.
 * @details
 *
 * This class is handles the dispatching logic for adding a new Wibble
 * Node (i.e. note). Handles possibility of creating a new node using
 * an optional template and/or schema (or from an existing file for content).
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_create.hpp"
#include "cmd_structs.hpp"
#include "wibbleexit.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Dispatching logic for the "create" operation on Wibble nodes.
 * @param OPTS package/general configuration options struct
 * @param CREATE command line options/parameters supplied via switches
 */
void wibble::WibbleCmdCreate::handle_create(pkg_options& OPTS, create_options& CREATE)
{
    WibbleConsole wc;
    // CLI11 logic ensures that create_from_file and t_mplate cannot be both set simultaneously
    // makes no sense to have both --from-file and --template active (see setup_create_options)
    std::string create_from_file = "";
    std::string schema_file = "";
    std::string t_mplate_file = "";
    //std::cout << "DEBUG: cmd_create - Using main DB: " << OPTS.files.main_db << std::endl;

    bool create_complete = false;
    WibbleExecutor wib_e(OPTS);
    //wib_e.display_configuration();

    // get all records for side effect of always verifying the database before proceeding;
    // will exit if database verification fails (void result/throw away return list)
    // operation is computationally cheap/near instantaneous, and database integrity is everything
    search_filters CHK_SRCH; CHK_SRCH.everything = true;
    wibble::WibbleUtility::get_record_set(wib_e, CHK_SRCH);

    // parse options
    if(CREATE.opt_from_file   != "UNSET") { create_from_file = CREATE.opt_from_file; } 
    if(CREATE.opt_schema      != "UNSET") { schema_file = CREATE.opt_schema;         } 
    if(CREATE.opt_t_mplate    != "UNSET") { t_mplate_file = CREATE.opt_t_mplate;     } 


    // simply list the defined template/schema files - do not create anything
    if(CREATE.opt_list)
    {
        std::string tree_schemas = "tree " + OPTS.paths.schemas_dir;
        std::string tree_templates = "tree " + OPTS.paths.templates_dir;
        wc.console_write_hz_heavy_divider();
        wc.console_write_green_bold("Defined schemas:\n");
        wc.console_write_hz_divider();
        std::system(tree_schemas.c_str());
        wc.console_write_hz_heavy_divider();
        wc.console_write_green_bold("Defined templates:\n");
        wc.console_write_hz_divider();
        std::system(tree_templates.c_str());
        wc.console_write_hz_heavy_divider();
        throw wibble::wibble_exit(0);
    } 

    bool success = true;
    // create a new node using a metadata schema file (and optionally template as well)
    if(schema_file != "")
    {
        //std::cout << "DEBUG: Create from schema value: " << schema_file << std::endl;
        create_complete = true;
        success = wib_e.op_create_note_using_schema_file(schema_file, t_mplate_file, create_from_file, CREATE.use_gui_editor);
        // this branch already handles templates + from-file as well, so block off any future branches
        t_mplate_file = "";
        create_from_file = "";
    }

    // create a new node using content from an existing file
    if(create_from_file != "")
    {
        // create from file logic
        //std::cout << "DEBUG: Create from file value: " << create_from_file << std::endl;
        wibble::WibbleUtility::expand_tilde(create_from_file);
        create_complete = true;
        success = wib_e.op_create_note_from_file(create_from_file, false);
    }

    // setup a new node using placeholder content from a template
    if(t_mplate_file != "")
    {
        //std::cout << "DEBUG: Create from template value: " << t_mplate_file << std::endl;
        wibble::WibbleUtility::expand_tilde(t_mplate_file);
        create_complete = true;
        success = wib_e.op_create_note_from_template_file(t_mplate_file, CREATE.use_gui_editor);
    }

    // these branches handle creation of a new schema or template, or a filetype default template 
    if(CREATE.create_schema)           { success = wib_e.create_schema_interactive(); create_complete = true;        }
    if(CREATE.create_default_template) { success = wib_e.create_template_interactive(true); create_complete = true;  }
    if(CREATE.create_template)         { success = wib_e.create_template_interactive(false); create_complete = true; }

    if(! create_complete) { success = wib_e.op_create_note_interactive(CREATE.use_gui_editor); }

    if(! success) { wc.console_write_error_red("Error creating note or schema/template.");        }
    else          { wc.console_write_success_green("Node or schema/template creation complete."); }
}
