// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_drawer.hpp
 * @brief     Handle "drawer" subcommand (header file).
 * @details
 *
 * This class dispatches out the relevant logic for dealing with transient
 * files via the drawer command
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n 
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>  
 * 
 * 
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *  
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr. 
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CMD_DRAWER_H
#define CMD_DRAWER_H

#include "cmd_structs.hpp"
#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleCmdDrawer
    {
    private:

    public:
        static void handle_drawer(pkg_options& OPTS, drawer_options& DRAW);
    };
}
#endif
