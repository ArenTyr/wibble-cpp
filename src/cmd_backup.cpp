// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_backup.cpp
 * @brief     Handle "backup" subcommand.
 * @details
 *
 * This class generates a bash shell command which executes to
 * generate a compressed archive of all of the user's Wibble data.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filesystem>

#include "cmd_backup.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Generate the bash/sh shell command
 * @param OPTS package/general configuration options struct
 * @param BACKUP command line options/parameters supplied via switches to backup command
 */
std::string wibble::WibbleCmdBackup::_generate_shell_backup_command(pkg_options& OPTS, backup_options& BACKUP)
{
    std::string base_datetime = wibble::WibbleRecord::generate_date_YMD("");
    std::string archive_datetime = wibble::WibbleRecord::remove_whitespace(base_datetime);
    archive_datetime = wibble::WibbleRecord::remove_colon(archive_datetime);
    std::string backup_cmd = "";
    const std::string bkup_dir = OPTS.paths.backup_location + "/wibble_backup";
    const std::string backup_workdir = "\" \"" + bkup_dir + "\" ";
    std::string config_file = "~/.config/wibble/wibble.toml";
    WibbleUtility::expand_tilde(config_file);
    std::string backup_command = "#!/usr/bin/env bash\n";
    backup_command.append("# Wibble++ automated backup script generated at " + archive_datetime + "\n\nset -e \n\n");
    backup_command.append("mkdir -p \"" + bkup_dir + "\"\n\n" +
                          "# Copy all Wibble++ data\n" +
                          "echo '-> Copying archived nodes'\n" +
                          "cp -a \"" + OPTS.paths.wibble_store  + "/archived_notes" + backup_workdir + "\n" +
                          "echo '-> Copying nodes'\n" +
                          "cp -a \"" + OPTS.paths.wibble_store + "/notes" + backup_workdir + "\n" +
                          "echo '-> Copying bookmarks'\n" +
                          "cp -a \"" + OPTS.paths.bookmarks_dir + backup_workdir + "\n" +
                          "echo '-> Copying databases'\n" +
                          "cp -a \"" + OPTS.paths.wibble_db_dir + backup_workdir + "\n" +
                          "echo '-> Copying janitor config'\n" +
                          "cp -a \"" + OPTS.paths.wibble_store + "/janitor" + backup_workdir + "\n" +
                          "echo '-> Copying jotfiles'\n" +
                          "cp -a \"" + OPTS.paths.jot_dir +  backup_workdir + "\n" +
                          "echo '-> Copying schemas'\n" +
                          "cp -a \"" + OPTS.paths.schemas_dir + backup_workdir + "\n" +
                          "echo '-> Copying scripts'\n" +
                          "cp -a \"" + OPTS.paths.scripts_dir + backup_workdir + "\n" +
                          "echo '-> Copying tasks'\n" +
                          "cp -a \"" + OPTS.paths.tasks_dir + backup_workdir + "\n" +
                          "echo '-> Copying templates'\n" +
                          "cp -a \"" + OPTS.paths.templates_dir + backup_workdir + "\n" +
                          "echo '-> Copying topics'\n" +
                          "cp -a \"" + OPTS.paths.topics_dir + backup_workdir + "\n" +
                          "echo '-> Copying Wibble++ configuration file'\n" +
                          "[[ -f " + config_file + " ]] && " +
                          "cp -a \"" + config_file + backup_workdir + "\n" +
                          "echo '-> Copying autofile categories'\n" +
                          "[[ -f \"" + OPTS.paths.autofile_dir + "/autofile_categories.wib\" ]] && " +
                          "cp -a \"" + OPTS.paths.autofile_dir + "/autofile_categories.wib" + backup_workdir + "\n" +
                          "echo '-> Copying autofile index'\n" +
                          "[[ -f \"" + OPTS.paths.autofile_dir + "/autofile_index.wib\" ]] && " +
                          "cp -a \"" + OPTS.paths.autofile_dir + "/autofile_index.wib" + backup_workdir + "\n");

    std::string log_msg;
    std::string archive_basename;

    if(BACKUP.full_backup)
    {
        backup_command.append("echo '-> Copying all node DATA'\n");
        backup_command.append("cp -a \"" + OPTS.paths.wibble_store + "/note_data" + backup_workdir + "\n");
        backup_command.append("echo '-> Copying drawers'\n");
        backup_command.append("cp -a \"" + OPTS.paths.drawers_dir + backup_workdir);
        backup_command.append("\ncd \"" + OPTS.paths.backup_location + "\"\n");
        archive_basename = "wibble-FULL-backup-";
        log_msg = "[   FULL   ] backup made at: " + base_datetime;
    }
    else
    {
        backup_command.append("echo '-> SKIPPING node data/attachments and drawers'\n");
        backup_command.append("\ncd \"" + OPTS.paths.backup_location + "\"\n");
        archive_basename = "wibble-backup-";
        log_msg = "[ standard ] backup made at: " + base_datetime;
    }

    backup_command.append("\n# Create the compressed archive\n");
    backup_command.append("echo '-> Creating archive...'\n");
    backup_command.append("echo '================================================='\n");

    if(BACKUP.xz_compression)
    { archive_datetime = archive_basename + archive_datetime + ".tar.xz"; backup_cmd = "tar -cJvf " + archive_datetime + " wibble_backup/"; }
    if(BACKUP.lz_compression)
    {
        archive_datetime = archive_basename + archive_datetime + ".tar.lz";
        backup_cmd = "echo '-> Constructing tar.lz archive...'\n";
        backup_cmd.append("tarlz -9 -cvf " + archive_datetime + " wibble_backup/ && ");
        backup_cmd.append("echo '-> Checking file differences...' && ");
        backup_cmd.append("tarlz -df " + archive_datetime + " && ");
        backup_cmd.append("echo '-> Checking archive integrity...' && ");
        backup_cmd.append("lzip -tv " + archive_datetime);
    }
    else if(BACKUP.gz_compression)
    { archive_datetime = archive_basename + archive_datetime + ".tar.gz"; backup_cmd = "tar -czvf " + archive_datetime + " wibble_backup/"; }
    else if(BACKUP._7z_compression)
    { archive_datetime = archive_basename + archive_datetime + ".7z";     backup_cmd = "7z a " + archive_datetime + " wibble_backup/"; }
    else if(BACKUP.zip_compression)
    { archive_datetime = archive_basename + archive_datetime + ".zip";    backup_cmd = "zip -r " + archive_datetime + " wibble_backup/"; }
    else // default to xz
    { archive_datetime = archive_basename + archive_datetime + ".tar.xz"; backup_cmd = "tar -cJvf " + archive_datetime + " wibble_backup/"; }

    backup_command.append(backup_cmd + "\n");
    backup_command.append("echo '* Wibble++ " + log_msg + " [file: " + archive_datetime + "]' >> \"" + OPTS.paths.backup_location + "/wibble-backup.log\"\n");
    backup_command.append("echo '================================================='\n");
    backup_command.append("echo '-> Archive written: " + archive_datetime + "'\n");
    backup_command.append("echo '-> Archive creation into " + OPTS.paths.backup_location + " complete.' && sleep 0.5\n");
    backup_command.append("find \"" + OPTS.paths.backup_location + "/wibble_backup\" -delete\n");

    //std::cerr << "Built backup command:\n" << backup_command << std::endl;
    return backup_command;
}

/**
 * @brief Static function to dispatch out to the backup functions, or display backup log.
 * @param OPTS package/general configuration options struct
 * @param BACKUP command line options/parameters supplied via switches to backup command
 */
void wibble::WibbleCmdBackup::handle_backup(pkg_options& OPTS, backup_options& BACKUP)
{
    WibbleConsole wc;
    if(BACKUP.list_backup)
    {
        const std::string log_file = OPTS.paths.backup_location + "/wibble-backup.log";
        if(std::filesystem::exists(std::filesystem::path(log_file)))
        {
            wc.console_write_header("BACKUPS");
            std::system(("cat " + log_file).c_str());
            wc.console_write_hz_divider();
            wc.console_write_success_green("Backup history listed.");
        }
        else
        {
            wc.console_write_error_red("No backup history/log file detected. Have you created a backup yet?");
        }
    }
    else
    {
        bool backup_success = _do_backup(wc, OPTS, BACKUP);
        if(backup_success)
        {
            wc.console_write_hz_heavy_divider();
            wc.console_write_yellow(ARW + "Note: Remember to a have a backup policy in place for your autofile directory/data.\n");
            wc.console_write_yellow(ARW + "Autofiled data should be stored entirely outside your main Wibble++ store/directory.\n");
            if(BACKUP.full_backup)
            {
                wc.console_write_hz_heavy_divider();
                wc.console_write_success_green("FULL backup of all Wibble++ data completed successfully.");
            }
            else
            {
                wc.console_write_hz_divider();
                wc.console_write_yellow(ARW + "Important note: Node data/attachments NOT backed up.\n");
                wc.console_write_yellow(ARW + "Use '--full-backup' switch to enable complete data/attachment backup as well.\n");
                wc.console_write_hz_heavy_divider();
                wc.console_write_success_green("Standard backup of Wibble++ data completed successfully.");
            }
        }
        else
        { wc.console_write_error_red("Backup operation did NOT complete successfully. Check output for errors.");                                             }
    }
}

/**
 * @brief Wrapper function to execute generated backup shell command.
 * @param OPTS package/general configuration options struct
 * @param BACKUP command line options/parameters supplied via switches to backup command
 */
bool wibble::WibbleCmdBackup::_do_backup(WibbleConsole& wc, pkg_options& OPTS, backup_options& BACKUP)
{
    std::string bkup_cmd = _generate_shell_backup_command(OPTS, BACKUP);
    const std::string bkup_script = OPTS.paths.tmp_dir + "/wibble_backup.sh";
    bool wo = WibbleIO::write_out_file_overwrite(bkup_cmd, bkup_script);
    int cmd_exec = 1;
    if(wo)
    {
        wc.console_write_yellow(ARW + "Running generated script '" + bkup_script + "'");
        wc.console_write("");
        wc.console_write_hz_heavy_divider();
        cmd_exec = std::system(("/usr/bin/bash " + bkup_script).c_str());
    }
    else   { wc.console_write_error_red("Error generating backup script.");    }

    if(cmd_exec == 0) { return true;  }
    else              { return false; }
}
