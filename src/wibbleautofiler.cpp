/**
 * @file      wibbleautofiler.cpp
 * @brief     Provides "autofile" functionality.
 * @details
 *
 * This class provides all of the 'autofiling' functionality that Wibble
 * offers.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <set>

#include "wibbleautofiler.hpp"
#include "wibbleexit.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Add a new autofiling category.
 */
void wibble::WibbleAutofiler::add_new_category()
{
    wc.console_write_header("NEW CAT");
    std::string cat = wc.console_write_prompt("Enter new category > ", "", "");
    wc.trim(cat);
    wibble::WibbleRecord::str_to_lowercase(cat);
    wibble::WibbleRecord::sanitise_input(cat);
    std::vector<std::string> existing_cats = _read_in_categories();
    std::set<std::string> cat_map;
    for(auto const& ext_c: existing_cats) {cat_map.insert(ext_c); }
    cat_map.insert(cat);
    wc.console_write_hz_heavy_divider();
    _update_category_file(cat_map);
}

/**
 * @brief Edit existing autofile categories.
 */
void wibble::WibbleAutofiler::edit_categories()
{
    std::vector<std::string> existing_cats = _read_in_categories();
    std::set<std::string> cat_map;
    std::set<std::string> repl_cat_map;
    repl_cat_map.insert("general");
    if(existing_cats.size() <= 1) { wc.console_write_error_red("Only default category 'general' exists, nothing to edit. Add some categories first."); }
    else
    {
        for(auto const& ext_c: existing_cats) {cat_map.insert(ext_c); }

        std::size_t cat_tot = cat_map.size() - 1;
        wc.console_write_yellow_bold("\nNOTE: ");
        wc.console_write_yellow("To remove an existing category, simply backspace until blank.\n\n");
        auto i = 0;
        for(auto const& item: cat_map)
        {
            if(item == "general") { continue; }
            std::string newc = wc.console_write_prompt("[" + std::to_string(++i) + "/" + std::to_string(cat_tot) + "] > ", "", item);
            wc.trim(newc);
            wibble::WibbleRecord::str_to_lowercase(newc);
            wibble::WibbleRecord::sanitise_input(newc);
            if(newc != "") { repl_cat_map.insert(newc); }
        }
        wc.console_write("");
        wc.console_write_hz_heavy_divider();
        _update_category_file(repl_cat_map);
    }
}

/**
 * @brief Ask for confirmation before copying or moving file/directory.
 * @param src String providing source file/directory
 * @param dest String providing destination file/directory
 * @param description String providing description of proposed autofiled file/directory
 * @param int Integer flag indicating either copy or move operation
 * @param bool Flag indicating whether to skip prompt and proceed auomatically
 */
void wibble::WibbleAutofiler::_confirm_file_operation(const std::string& src, const std::string& dest,  const std::string& description, const int& mode, const bool no_prompt)
{
    if(mode == 1)
    {
        wc.console_write_header("COPY");
        wc.console_write_green_bold("Source     : ");
    }
    else if(mode == 2)
    {
        wc.console_write_header("MOVE");
        wc.console_write_green_bold("Source     : ");
    }
    wc.console_write(src);
    wc.console_write_green_bold("Destination: ");
    wc.console_write(dest);
    wc.console_write_green_bold("Description: ");
    std::string desc = description;
    wc.trim(desc);
    if(description == "___NONE___") { wc.console_write("[ No description provided ]"); }
    else                            { wc.console_write(description);                   }
    wc.console_write_hz_divider();

    if(! no_prompt)
    {
        std::string resp = wc.console_write_prompt("Perform above file operation? (y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); throw wibble::wibble_exit(0); }
        std::cout << '\n';
    }
}

/**
 * @brief Create destination path for autofiling copy/move operation.
 * @param category String providing category/containing directory name
 * @param manual_YMD Optional string to provide an override date (YYYY-MM-DD) rather than using current datetime
 */
std::string wibble::WibbleAutofiler::_generate_dest_dir(const std::string& category, const std::string& manual_YMD)
{
    const std::string dest_cat = (category == "") ? "general" : category;
    if(manual_YMD != "")
    {
        wc.console_write_green(ARW + "Attempting manual date override with date string: '" + manual_YMD + "'\n");
        try
        {
            // check datestring is sane
            std::time_t chk = WibbleRecord::get_time_from_YYYY_MM_DD_str(manual_YMD, true, false);
            //if(chk == -2208988799) // Epoch "error" time of 1900-01-01 00:00:01
            if(chk == NULLDATETIME) // Epoch "error" time of 1900-01-01 00:00:01
            { throw std::invalid_argument("Bad datetime input"); }
                
            const std::string YYYY = manual_YMD.substr(0,4);
            const std::string MM = manual_YMD.substr(5,2);
            const std::string DD = manual_YMD.substr(8,2);

            //const int y = std::stoi(YYYY);
            const int m = std::stoi(MM);
            //const int d = std::stoi(DD);

            return YYYY + SEP + WibbleRecord::generate_numbered_month(m) + SEP + dest_cat + SEP + DD + SEP;
        }
        catch(std::exception& ex)
        {
            wc.console_write_error_red("Invalid YYYY-MM-DD datestring: " + manual_YMD); 
            wc.console_write_red(ARW + "Date string should be in ISO YYYY-MM-DD format\n");
            wc.console_write_red(ARW + "e.g. \"2017-11-28\", \"2006-01-05\", \"2024-02-15\"\n");
            throw wibble::wibble_exit(1);
        }
    }
    else
    {
        return WibbleRecord::generate_date_YYYY("") + SEP +
               WibbleRecord::generate_numbered_month(std::stoi(WibbleRecord::generate_date_MM(""))) + SEP +
               dest_cat + SEP + WibbleRecord::generate_date_DD("") + SEP;
    }
}

/**
 * @brief Create destination path for autofiling copy/move operation.
 * @param category String providing category/containing directory name
 * @param manual_YMD Optional string to provide an override date (YYYY-MM-DD) rather than using current datetime
 */
void wibble::WibbleAutofiler::_display_file_listing(std::vector<std::pair<std::string, std::string>>& file_listing)
{
    unsigned long index = 0;
    std::size_t file_results = file_listing.size();
    for(auto const& entry: file_listing) { wc.console_write_padded_selector(++index, file_results, entry.first + '\n'); }
}

/**
 * @brief Provide interactive menu for operations on selected autofiled file/directory.
 * @param file_selection String pair/tuple of file/directory path and description.
 */
void wibble::WibbleAutofiler::_display_file_options(std::pair<std::string, std::string>& file_selection)
{
    const std::string f_path = af_base_dir + SEP + file_selection.first;
    const std::string par_dir = std::filesystem::path(f_path).parent_path();

    bool is_dir = (std::filesystem::is_directory(std::filesystem::path(f_path))) ? true : false;
    wc.console_write_header("OPERATION");
    wc.console_write_green_bold("Filename   : ");
    wc.console_write(file_selection.first);
    std::string fs_type = "File";
    std::string file_or_dir = "file";
    std::string delete_warn = "file";
    if(is_dir) { fs_type = "Directory"; file_or_dir = "directory"; delete_warn = "entire directory"; }
    wc.console_write_green_bold("Type       : ");
    wc.console_write(fs_type);
    wc.console_write_green_bold("Description: ");
    std::string desc = file_selection.second;
    wc.trim(desc);
    if(desc == "___NONE___") { desc = "[ No description provided ]"; }
    wc.console_write(desc);
    wc.console_write_hz_divider();
    wc.console_write_yellow_bold("1. Open " + file_or_dir + " with default application.\n");
    wc.console_write_yellow_bold("2. Run command on " + file_or_dir + ".\n");
    wc.console_write_yellow_bold("3. Open containing directory with file manager.\n");
    wc.console_write_yellow_bold("4. Permanently "); wc.console_write_red_bold("delete " + delete_warn + ".\n");
    wc.console_write_hz_heavy_divider();

    
    std::string resp = wc.console_write_prompt("\nInput choice [1:4] > ", "", "");
    std::cout << '\n';
    try
    {
        wc.trim(resp);
        int choice = std::stoi(resp);
        std::string cmd;
        bool upd_idx = false;
        switch(choice)
        {
        case 1:
            wib_e.run_exec_expansion(wib_e.get_xdg_open_cmd(), f_path);
            break;
        case 2:
            cmd = wc.console_write_prompt("Input command. (Optionally use '___FILE___' as filename placeholder.)\n> ", "", "");
            std::cout << '\n';
            wc.console_write_hz_divider();
            std::cout << '\n';
            wc.trim(cmd);
            wib_e.run_exec_expansion(cmd, f_path);
            break;
        case 3:
            wib_e.run_exec_expansion(wib_e.get_fm_tool(), par_dir);
            break;
        case 4:
            // warn/confirm with user before deleting anything
            wc.console_write_red_bold("Pending deletion: "); wc.console_write(f_path + '\n');
            if(is_dir) { wc.console_write_red_bold("WARNING. Above directory and all files contained within it will be deleted.\n\n"); }
            else       { wc.console_write_red_bold("WARNING. File will be permanently deleted.\n\n"); }
            resp = wc.console_write_prompt("Input 'delete' to confirm > ", "", "");
            std::cout << '\n';
            if(resp == "delete")
            {
                upd_idx = WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, file_selection.first, af_base_dir + AF_INDEX_FILE, true);
                if(upd_idx && is_dir)
                {
                    std::filesystem::remove_all(f_path);
                    wc.console_write_success_green("Directory successfully deleted.");
                    WibbleUtility::prune_empty_directories(wc, af_base_dir, false);
                }
                else if(upd_idx && (! is_dir))
                {
                    std::filesystem::remove(f_path);
                    wc.console_write_success_green("File successfully deleted.");
                    WibbleUtility::prune_empty_directories(wc, af_base_dir, false);
                }
                else if((! upd_idx) && is_dir)     { wc.console_write_success_green("Error deleting directory.");      }
                else if((! upd_idx) && (! is_dir)) { wc.console_write_success_green("Error deleting file.");           }
            }
            else { wc.console_write_error_red("Aborting."); }
            break;
        default:
            throw std::invalid_argument("Unknown option");
            break;
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid choice, exiting.");
    }

}

/**
 * @brief Get an optional description for file/directory about to be autofiled from user.
 * @param s_desc Optional pre-set description/item title if supplied on command line
 */
std::string wibble::WibbleAutofiler::_get_set_description(const std::string& s_desc)
{
    if(s_desc != "")
    {
        std::string n_desc = s_desc;
        wc.trim(n_desc);
        if(n_desc == "") { return "___NONE___"; }
        else             { return n_desc;       }
    }
    else
    {
        std::string resp = wc.console_write_prompt("Input optional description (leave blank for none)\n> ", "", "");
        std::cout << '\n';
        wc.trim(resp);
        if(resp == "") { return "___NONE___"; }
        else           { return resp;         }
    }
}

/**
 * @brief Get an optional description for file/directory about to be autofiled from user.
 * @param src_file String representing source file/directory path
 * @param dest_file String representing source file/directory path
 * @param mode Integer flag representing mode; currently 1 = copy, 2 = move
 */
void wibble::WibbleAutofiler::_execute_autofile(const std::string& src_file, const std::string& dest_file,
                                                const std::string& description, const int& mode)
{
    if(std::filesystem::exists(std::filesystem::path(dest_file)))
    {
        wc.console_write_red(ARW + "File/directory already exists: '" + dest_file + "'.\n");
        wc.console_write_error_red("The designated file/directory has already been autofiled.");
        throw wibble::wibble_exit(1);
    }
    if(mode != 1 && mode != 2) { wc.console_write_error_red("Invalid mode: . " + std::to_string(mode)); throw wibble::wibble_exit(1); }

    const std::string par_dir = std::filesystem::path(dest_file).parent_path();
    _update_index_or_exit(dest_file, description);
    bool ensure_dr = wibble::WibbleIO::create_path(par_dir);
    if(! ensure_dr) { wc.console_write_error_red("Error. Unable to create output directory: " + par_dir); throw wibble::wibble_exit(1); }
    if(mode == 1)
    {
        bool copy_file = wibble::WibbleIO::wcopy_file(src_file, dest_file);
        copy_file ? wc.console_write_success_green("Successfully filed [COPIED] into " + dest_file) :
                    wc.console_write_error_red("Error filing  " + dest_file);
    }
    else if(mode == 2)
    {
        bool move_file = wibble::WibbleIO::wmove_file(src_file, dest_file);
        move_file ? wc.console_write_success_green("Successfully filed [MOVED] into " + dest_file) :
                    wc.console_write_error_red("Error filing  " + dest_file);
    }
}

/**
 * @brief Obtain/set filing category; acts as a parent folder in tree under month hierarchy.
 */
std::string wibble::WibbleAutofiler::_get_set_filing_category(const std::string& s_category)
{
    // check if pre-set
    if(s_category != "")
    {
        std::string preset_cat = s_category;
        wc.trim(preset_cat);
        wibble::WibbleRecord::str_to_lowercase(preset_cat);
        wibble::WibbleRecord::sanitise_input(preset_cat);

        if(preset_cat == "")
        {
            wc.console_write_error_red("Invalid pre-set category name: '" + s_category + "'. Aborting.");
            throw wibble::wibble_exit(1);
        }

        return preset_cat;
    }
    else
    {
        std::vector<std::string> categories = _read_in_categories();
        unsigned long index = 0;
        std::size_t cat_size = categories.size();
        wc.console_write_header("CATEGORIES");
        for(auto const& cat: categories)
        {
            ++index;
            wc.console_write_padded_selector(index, cat_size, cat + '\n'); 
        }
        wc.console_write_hz_heavy_divider();
        unsigned long choice = wc.console_present_record_selector(cat_size);
        if(choice != 0) { return categories.at(choice - 1); }
        else            { wc.console_write_error_red("Invalid category. Aborting."); throw wibble::wibble_exit(1); }
    }
}

/**
 * @brief Scan the root autofile index file and obtain the file/directory list of autofiled items
 * @param filten_fn String representing optional filter to filter file/directory/path names by
 * @param filten_desc String representing optional filter to filter autofiled item descriptions by
 */
std::vector<std::pair<std::string, std::string>> wibble::WibbleAutofiler::_read_in_autofiles(const std::string& filter_fn, const std::string& filter_desc)
{
    std::vector<std::pair<std::string, std::string>> af_files;
    // lowercase for case_insensitve search
    std::string fn_fltr   = filter_fn;   WibbleRecord::str_to_lowercase(fn_fltr); 
    std::string desc_fltr = filter_desc; WibbleRecord::str_to_lowercase(desc_fltr); 
    
    const std::string af_index_file = af_base_dir + AF_INDEX_FILE;
    if(std::filesystem::exists(af_index_file))
    {
        std::stringstream af_ss = wibble::WibbleIO::read_file_into_stringstream(af_index_file);
        std::string line;
        while(getline(af_ss, line, '\n'))
        {
            std::size_t sep = line.find(FIELD_DELIM);
            if(sep != std::string::npos)
            {
                std::pair<std::string, std::string> cat{line.substr(0, sep), line.substr(sep + 1, line.length() - 1 - sep)};
                if(filter_fn == "" && filter_desc == "") { af_files.push_back(cat); }
                else // filters in effect
                {
                    bool do_add = false;
                    // we need both branches to allow filtering logic case of BOTH filename and description together
                    if(filter_fn != "")
                    {
                        // always do lowercase-to-lowercase comparison, i.e. case insensitive
                        std::string fn_copy = cat.first; WibbleRecord::str_to_lowercase(fn_copy);
                        if(fn_copy.find(fn_fltr) != std::string::npos) { do_add = true;  }  
                        else                                           { do_add = false; }
                    }

                    if(filter_desc != "")
                    {
                        std::string desc_copy = cat.second; WibbleRecord::str_to_lowercase(desc_copy);
                        if(desc_copy.find(desc_fltr) != std::string::npos) { do_add = true;  }  
                        else                                               { do_add = false; }
                    }

                    if(do_add) { af_files.push_back(cat); }
                }
            }
        }

        std::stringstream().swap(af_ss);
    }
    return af_files;
}

/**
 * @brief Scan the root autofile category list file and obtain the available filing categories.
 */
std::vector<std::string> wibble::WibbleAutofiler::_read_in_categories()
{
    // use a set to ensure de-duplication
    std::set<std::string> af_categories;
    std::vector<std::string> af_cat;
    
    const std::string af_c_file = af_base_dir + AF_CAT_FILE;
    if(std::filesystem::exists(af_c_file))
    {
        std::stringstream af_ss = wibble::WibbleIO::read_file_into_stringstream(af_c_file);
        std::string line;
        while(getline(af_ss, line, '\n'))
        {
            wc.trim(line);
            if(line.length() >= 1) { af_categories.insert(line); }
        }

        std::stringstream().swap(af_ss);
    }
    // general is default, ensure it always exists
    af_categories.insert("general");
    for(auto const& item: af_categories) { af_cat.push_back(item); }
    return af_cat;
}

/**
 * @brief Method to confirm then write out the updated category file.
 * @param cat_map set of string values pertaining to each category, to be written out one line per category.
 */
void wibble::WibbleAutofiler::_update_category_file(const std::set<std::string>& cat_map)
{
    unsigned long index = 0;
    std::size_t csize = cat_map.size();
    std::string new_cat_file = "";
    for(auto const& item: cat_map)
    {
        wc.console_write_padded_selector(++index, csize, item + '\n');
        new_cat_file.append(item + '\n');
    }
    wc.console_write_hz_heavy_divider();
    std::string resp = wc.console_write_prompt("\nWrite out new category list? (y/n) > ", "", "");
    if(resp == "y" || resp == "Y")
    {

        const std::string af_c_file = af_base_dir + AF_CAT_FILE;
        bool backup;
        if(std::filesystem::exists(af_c_file))
        {
            backup = wibble::WibbleIO::wcopy_file(af_c_file, cache_dir + AF_CAT_FILE);
        }
        else
        {
            wc.console_write_yellow(ARW + "No category file yet exists, writing out new file.\n");
            backup = true;
        }
        
        if(backup)
        {
            bool upd_cat = wibble::WibbleIO::write_out_file_overwrite(new_cat_file, af_c_file);
            if(upd_cat) { wc.console_write_success_green("Updated category list successfully written out."); }
            else        { wc.console_write_error_red("Error writing out new category list.");                }
        }
        else  { wc.console_write_error_red("Error creating backup copy of category list. Aborting.");        }

    }
    else { wc.console_write_error_red("User cancelled."); }
}


/**
 * @brief Either update the autofile index with the new intended autofile candidate or immediately exit on failure.
 * @param dest_file String representing destination (relative to autofile root) for file/directory
 * @param description String representing description of new autofiled item
 */
void wibble::WibbleAutofiler::_update_index_or_exit(const std::string& dest_file, const std::string& description)
{
    try
    {
        if(dest_file.find(af_base_dir) != 0) { throw wibble::wibble_exit(1); }

        const std::string rel_path = dest_file.substr(af_base_dir.length() + 1, (dest_file.length() - af_base_dir.length() - 1));
        const std::string record = rel_path + FIELD_DELIM + description + '\n';
        bool upd_indx = WibbleIO::write_out_file_append(record, af_base_dir + AF_INDEX_FILE);

        if(! upd_indx) { throw wibble::wibble_exit(1); }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error updating index. Aborting."); 
        throw wibble::wibble_exit(1); 
    }
}

/**
 * @brief Placeholder function for doing nothing/[old] default case.
 */
void wibble::WibbleAutofiler::null_operation()
{
    wc.console_write_error_red("No operation specified. Either '--copy' or '--move' needs to be specified.");
}

/**
 * @brief If no copy or move operation specified, list all autofiles, possibly with disk usage statistics.
 * @param filter_fn Optional string representing file/directory/path substring match to filter by
 * @param filter_desc Optional string representing item description substring match to filter by
 */
void wibble::WibbleAutofiler::just_list(const std::string& filter_fn, const std::string& filter_desc)
{
    wc.console_write_header("FILES");
    bool filter_on = false;
    if(filter_fn != "")   { wc.console_write_green(ARW + "Active filename filter in effect   : "); wc.console_write_yellow(filter_fn + "\n");   filter_on = true; }
    if(filter_desc != "") { wc.console_write_green(ARW + "Active description filter in effect: "); wc.console_write_yellow(filter_desc + "\n"); filter_on = true; }
    std::cout << '\n';
    std::vector<std::pair<std::string, std::string>> af_files = _read_in_autofiles(filter_fn, filter_desc);
    _display_file_listing(af_files);
    wc.console_write_hz_heavy_divider();
    wc.console_write_yellow_bold(std::to_string(af_files.size()) + " files indexed.\n");
    wc.console_write_hz_divider();
    if(! filter_on) { std::system(("du -hs " + af_base_dir).c_str()); }
}

/**
 * @brief Get user selection for opening/operating on a autofiled item.
 * @param filter_fn Optional string representing file/directory/path substring match to filter by
 * @param filter_desc Optional string representing item description substring match to filter by
 */
void wibble::WibbleAutofiler::open_file_action(const std::string& filter_fn, const std::string& filter_desc)
{
    if(filter_fn != "")   { wc.console_write_green(ARW + "Active filename filter in effect   : "); wc.console_write_yellow(filter_fn + "\n");   }
    if(filter_desc != "") { wc.console_write_green(ARW + "Active description filter in effect: "); wc.console_write_yellow(filter_desc + "\n"); }
    std::cout << '\n';
    wc.console_write_header("SELECT");
    std::vector<std::pair<std::string, std::string>> af_files = _read_in_autofiles(filter_fn, filter_desc);
    std::size_t file_results = af_files.size();
    _display_file_listing(af_files);

    if(af_files.size() == 1)
    {
        wc.console_write_success_green(ARW + "Single candidate, automatically selecting.");
        _display_file_options(af_files.at(0));
    }
    else if(af_files.size() > 1)
    {
        unsigned long sel = wc.console_present_record_selector(file_results);
        if(sel != 0)
        {
            _display_file_options(af_files.at(sel - 1));
        }
        else { wc.console_write_error_red("Invalid selection. Exiting."); }
    }
    else
    {
        if(filter_fn != "")   { wc.console_write_red(ARW + "No matches with filename filter: '" + filter_fn + "'\n");      }
        if(filter_desc != "") { wc.console_write_red(ARW + "No matches with description filter: '" + filter_desc + "'\n"); }
        if(filter_fn == "" && filter_desc == "") { wc.console_write_error_red("Nothing autofiled. Autofile some files first."); }
    }

}

/**
 * @brief Main wrapper function for dispatching out the actual autofile operation.
 * @param fn String representing file/directory/path to autofile
 * @param mode Integer flag representing either copy = 1 or move = 2 operation
 * @param dry_run Flag controlling whether to simply display proposed operation rather than perform it
 * @param custom_date Optional string with potential custom date override in YYYY-MM-DD if user does not wish to file with current date
 * @param no_prompt Flag controlling whether to bypass any confirmatory prompts/act non-interactively
 * @param s_category Optional pre-set category if supplied on command line
 * @param s_desc Optional pre-set description/item title if supplied on command line
 */
void wibble::WibbleAutofiler::perform_autofile(const std::string& fn, const int& mode, const bool dry_run,
                                               const std::string& custom_date, const bool no_prompt, const std::string& s_category,
                                               const std::string& s_desc)
{
    std::string src_file = fn;
    wibble::WibbleUtility::expand_tilde(src_file);
    wibble::WibbleUtility::remove_trailing_slash(src_file);
    
    const std::string category = _get_set_filing_category(s_category);
    const std::string description = _get_set_description(s_desc);
    const std::string gen_dir = (custom_date == "") ? _generate_dest_dir(category) + std::string(std::filesystem::path(src_file).filename()) :
                                                      _generate_dest_dir(category, custom_date) + std::string(std::filesystem::path(src_file).filename());

    const std::string dest_file = af_base_dir + SEP + gen_dir; 
    if(dry_run)
    {
        wc.console_write_yellow_bold(ARW + "Dry run enabled. Showing proposed file operation.\n");
        _confirm_file_operation(src_file, dest_file, description, mode, true);
    }
    else
    {
        // ensure file/directory exists/is readable first
        if(! std::filesystem::exists(std::filesystem::path(src_file)))
        {
            wc.console_write_error_red("File/directory '" + src_file + "' does not exist/is not readable. Aborting.");
            throw wibble::wibble_exit(1);
        }

        _confirm_file_operation(src_file, dest_file, description, mode, no_prompt);
        _execute_autofile(src_file, dest_file, description, mode);
    }
}
