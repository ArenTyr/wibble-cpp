/**
 * @file      wibbletransfer.hpp
 * @brief     Class to execute all batch functionality (header file).
 * @details
 *
 * Provides the "transfer" command functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLETRANSFER_H
#define WIBBLETRANSFER_H

#include "wibbleexecutor.hpp"
#include "wibbletopics.hpp"

namespace wibble
{
    class WibbleTransfer
    {
    public:
        enum x_item_type
        {
            NODE,
            TASK_GROUP,
            TOPIC,
            BIN_MAP,
            ERR
        };
    private:
        unsigned int node_count = 0;
        unsigned int tg_count= 0 ;
        unsigned int topic_count = 0;
        unsigned int bm_count = 0;
        void _add_all_to_set();
        void _add_nodes_to_set();
        void _add_tgs_to_set();
        void _add_topics_to_set();
        void _add_bin_maps_to_set();
        bool _add_bundle_entry(const std::string& path, const std::string& desc, const std::string& type, const std::string& id);
        x_item_type _map_x_type(const std::string& token) const;
        std::string _render_x_type(const x_item_type& type) const;
        void _prep_path(WibbleConsole& wc, std::string& path);
        typedef struct bundle_payload
        {
            unsigned int node_count;
            unsigned int tg_count;
            unsigned int topic_count;
            unsigned int bm_count;
            bundle_payload():
                node_count{0},
                tg_count{0},
                topic_count{0},
                bm_count{0} {};
        } bundle_payload;
        typedef struct xfer_item
        {
            const x_item_type type;
            const std::string ext;
            const std::string desc;
            const std::string id;
            xfer_item(const x_item_type& i_type, const std::string& i_ext, const std::string& i_desc, const std::string& i_id):
                type{i_type},
                ext{i_ext},
                desc{i_desc},
                id{i_id} {};
        } xfer_item;
        bundle_payload payload;
        std::vector<xfer_item> xfer_bundle;
        
        bool _do_import(WibbleConsole& wc, pkg_options& OPTS, const bool overwrite, const std::string& bundle_dir);
        void _display_import_set(WibbleConsole& wc);
        bool _import_confirm(WibbleConsole& wc, pkg_options& OPTS, const std::string& bundle_dir);
        static void propagate_data_if_exists(WibbleConsole &wc, pkg_options &OPTS, const std::string &bundle_dir,
                                      WibbleExecutor &wib_e, WibbleRecord &imp_record, std::filesystem::path data_source,
                                      std::filesystem::path data_dest_dir, std::filesystem::path data_dest_file);
        static bool _process_topic(WibbleConsole &wc, const std::string& bundle_dir, const bool overwrite, const std::string& topic_name,
                                   const pkg_options& OPTS, const bool data_topic, WibbleTopics& topic_destination,
                                   WibbleTopics& topic_source);
        static bool _import_bin_map(WibbleConsole& wc, pkg_options& OPTS, const bool overwrite, const std::string& bundle_dir);
        static bool _import_node(WibbleConsole& wc, pkg_options& OPTS, const xfer_item& item, const bool overwrite, const std::string& bundle_dir);
        static bool _import_task_group(WibbleConsole& wc, pkg_options& OPTS, const xfer_item& item, const bool overwrite, const std::string& bundle_dir);
        static bool _import_topics(WibbleConsole& wc, pkg_options& OPTS, const bool overwrite, const std::string& bundle_dir);
        bool _parse_bundle_file(WibbleConsole& wc, const std::string& bundle_def);
        void _pretty_print_bundle_contents(WibbleConsole& wc);
        void _set_bundle_counts();
    public:
        bool create_bundle(WibbleConsole& wc, const std::string& path);
        bool export_binary_mapping(WibbleConsole& wc, pkg_options& OPTS, const std::string& path);
        bool export_nodes(WibbleConsole& wc, pkg_options& OPTS, search_filters& SEARCH, const std::string& path, const bool archived);
        bool export_task_group(WibbleConsole& wc, pkg_options& OPTS, const std::string& path);
        bool export_topic(WibbleConsole& wc, pkg_options& OPTS, const std::string& path);
        bool import_bundle(WibbleConsole& wc, pkg_options& OPTS, const std::string& bundle_file_or_path);
    };
}
#endif
