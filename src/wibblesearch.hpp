/**
 * @file      wibblesearch.hpp
 * @brief     Build and execute a "search" pipeline for retreiving database results (header file).
 * @details
 *
 * This class is responsible for building up a complete search query/
 * search chain. In case of searching multiple fields, this means multiple
 * iterations/"frames"; since this operates on a very rapidly/drastically
 * reducing temporary file each time, in practice search multiple files
 * is extremely fast. Searching logic implies a logical "&&"/AND operation;
 * i.e. the venn diagram representing the set of records that satisfy the
 * criteria input in all active search fields.
 *
 * More sophisticated logical filtering, is, by design, not offered,
 * since in practice it has little day-to-day value, and would considerably
 * increase the complexity of the searching logic.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_SEARCH_H
#define WIBBLE_SEARCH_H

#include <queue>
#include "wibbledbrgdatabase.hpp"

namespace wibble
{
    using WibResultList = std::unique_ptr<std::vector<WibbleRecord>>;
    class WibbleSearch
    {
    public:
        struct search_frame
        {
            SEARCH_FIELD field;
            std::string query;
            bool exact_search;
            bool fixed_string;
            search_frame(): field{SEARCH_FIELD::F_TITLE},
                            query{"NULL"},
                            exact_search{false},
                            fixed_string{false} {};
        };

        bool add_search_frame(search_frame search_iteration);
        WibResultList execute_search_pipeline();
    private:
        const std::string DB_FILE;
        const std::string WORKFILE;
        const std::string WORKFILE2;
        const std::map<std::string, std::string> bin_map;

        WibbleConsole wc;
        WibbleRGDatabase w_rg;
        std::queue<search_frame> search_queue;
        WibbleDBRGQuery* query;
    public:
         WibbleSearch(const std::string db, const std::string wrk_file, const std::set<std::string>& bin_map):
            DB_FILE{db}, WORKFILE{wrk_file}, WORKFILE2{wrk_file + "2"}, w_rg{bin_map}, query(new WibbleDBRGQuery) {};

        ~WibbleSearch()
        {
            delete query;
        }
    };
}
#endif
