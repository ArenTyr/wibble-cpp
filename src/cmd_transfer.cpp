// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_transfer.cpp
 * @brief     Handle "transfer" subcommand.
 * @details
 *
 * This class dispatches out the relevant logic for importing/exporting
 * selected Nodes and Topics, together with data.
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n 
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>  
 * 
 * 
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *  
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr. 
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_transfer.hpp"
#include "wibbletransfer.hpp"
#include "wibblerecord.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"
#include "wibbleexit.hpp"
#include <iostream>

/**
 * @brief Static function to dispatch out to the various import/export functions.
 * @param OPTS package/general configuration options struct
 * @param BACKUP command line options/parameters supplied via switches to backup command
 */
void wibble::WibbleCmdTransfer::handle_transfer(pkg_options& OPTS, search_filters& SEARCH, transfer_options& XFER)
{
    WibbleConsole wc;
    //WibbleUtility::dev_warning(wc, "transfer", "Janaury 2025");

    int modes = 0;

    if(XFER.create_bundle)        { modes += 1; }
    if(XFER.import_bundle)        { modes += 1; }
    if(XFER.dl_bundle != "UNSET") { modes += 1; }
    if(XFER.export_bin_map)       { modes += 1; }
    if(XFER.export_nodes)         { modes += 1; }
    if(XFER.export_task_group)    { modes += 1; }
    if(XFER.export_topic)         { modes += 1; }
    
    WibbleUtility::remove_trailing_slash(XFER.dir_path);
    if(XFER.dl_bundle == "UNSET" && XFER.dir_path == "UNSET")
    {
        wc.console_write_error_red("'--path' argument must be supplied for all options apart from '--dl-bundle'");   
    }
    else if(XFER.dl_bundle != "UNSET" && XFER.dir_path != "UNSET")
    {
        wc.console_write_error_red("'--path' argument should not be supplied with '--dl-bundle' option");   
    }
    else if(modes != 1)
    {
        wc.console_write_error_red("Zero or multiple modes selected.");
        wc.console_write_red("\nPlease select ONE mode from:\n\n"); 
        wc.console_write_red(" --create-bundle\n"); 
        wc.console_write_red(" --dl-bundle\n"); 
        wc.console_write_red(" --import-bundle\n"); 
        wc.console_write_red(" --export-bin-map\n"); 
        wc.console_write_red(" --export-nodes\n"); 
        wc.console_write_red(" --export-task-group\n"); 
        wc.console_write_red(" --export-topic\n"); 
    }
    else
    {
        if(XFER.create_bundle)
        {
            WibbleTransfer().create_bundle(wc, XFER.dir_path);
        }
        else if(XFER.dl_bundle != "UNSET")
        {
            auto use_checksum_url = XFER.dl_bundle_sha256 != "UNSET" ? true : false;
            
            if(XFER.dl_bundle.length() < 7 || XFER.dl_bundle.substr(0,4) != "http")
            {
               wc.console_write_error_red("Please supply a valid URL to a wibb bundle file.");
            }
            else if(use_checksum_url && (XFER.dl_bundle_sha256.length() < 7 || XFER.dl_bundle_sha256.substr(0,4) != "http"))
            {
               wc.console_write_error_red("Please supply a valid URL to a wibb SHA256 checksum file.");
            }
            else
            {
                wc.console_write_yellow(ARW + "Checking for wget command...\n");
                bool use_wget = true;
                bool use_curl = true;
                int wget_present = std::system("type wget > /dev/null");
                if(wget_present != 0)
                {
                    wc.console_write_yellow(ARW + "wget not found. Checking for curl command...");
                    use_wget = false;
                    int curl_present = std::system("type curl > /dev/null");
                    if(curl_present != 0)
                    {
                        wc.console_write_yellow(ARW + "curl not found. Please install either wget or curl.");
                        use_curl = false;
                    }
                }
                
                const auto dl_dir = OPTS.paths.tmp_dir + "/" + WibbleRecord::generate_entry_hash();
                auto setup_dir = true;
                auto orig_path = std::filesystem::current_path();
                try 
                {
                    std::filesystem::create_directory(dl_dir);                
                    std::filesystem::current_path(dl_dir);
                }
                catch(std::exception& err)
                {
                    setup_dir = false;
                }
                
                if(setup_dir)
                {
                    wc.console_write_green(ARW + "Bundle download URL: " + XFER.dl_bundle + "\n");
                    if(use_checksum_url)
                    {
                        wc.console_write_green(ARW + "SHA256 download URL: " + XFER.dl_bundle_sha256 + "\n");
                    }
                    wc.console_write_yellow(ARW + "Attempting to download bundle file into " + dl_dir);
                    wc.console_write("");
                    if(use_wget)
                    {
                       int get_wibb = std::system(("wget -nc --show-progress --content-disposition '" + XFER.dl_bundle + "'").c_str());
                       // attempt to get checksum file if it exists
                       if(get_wibb == 0)
                       {
                           if(! use_checksum_url) { std::system(("wget -nc --show-progress --content-disposition '" + XFER.dl_bundle + ".sha256'").c_str()); }
                           else                   { std::system(("wget -nc --show-progress --content-disposition '" + XFER.dl_bundle_sha256 + "'").c_str()); }
                       }
                       else
                       {
                           std::filesystem::current_path(orig_path);
                           std::filesystem::remove_all(dl_dir);
                       }
                    }
                    else if(use_curl)
                    {
                       int get_wibb = std::system(("curl --progress-bar -O -J '" + XFER.dl_bundle + "'").c_str());
                       if(get_wibb == 0)
                       {
                          if(! use_checksum_url) { std::system(("curl --progress-bar -O -J '" + XFER.dl_bundle + ".sha256'").c_str()); }
                          else                   { std::system(("curl --progress-bar -O -J '" + XFER.dl_bundle_sha256 + "'").c_str()); }
                       }
                       else
                       {
                           std::filesystem::current_path(orig_path);
                           std::filesystem::remove_all(dl_dir);  
                       }
                    }
                    
                    // store the necessary filenames
                    std::system("find ${PWD} -name \"*.wibb\" -print | head -n 1 > _wibdownload.txt");
                    std::system("find ${PWD} -name \"*.wibb.sha256\" -print | head -n 1 > _wibdownload256.txt");
                    
                    if(std::filesystem::exists("./_wibdownload.txt"))
                    {
                        auto dl_name = (WibbleIO::read_file_into_stringstream("./_wibdownload.txt")).str();
                        wc.trim(dl_name); 
                        if(WibbleTransfer().import_bundle(wc, OPTS, dl_name))
                        { wc.console_write_success_green("Data imported successfully from downloaded bundle."); }
                        else
                        {wc.console_write_error_red("No data imported/Error importing data from downloaded bundle."); }                
                    
                        // clean up
                        std::filesystem::current_path(orig_path);
                        std::filesystem::remove_all(dl_dir);
                    }
                    else
                    {
                        wc.console_write_error_red("Unable to download wibb archive. Check command/URL.");    
                    }
                }
                else
                {
                    wc.console_write_error_red("Unable to create temporary output directory.");    
                }
            }
        }
        else if(XFER.import_bundle)
        {
            if(WibbleTransfer().import_bundle(wc, OPTS, XFER.dir_path))
            { wc.console_write_success_green("Data imported successfully from bundle."); }
            else
            {wc.console_write_error_red("No data imported/Error importing data from bundle."); }
        }
        else if(XFER.export_bin_map)
        {
            WibbleTransfer().export_binary_mapping(wc, OPTS, XFER.dir_path);
        }
        else if(XFER.export_nodes)
        {
            WibbleTransfer().export_nodes(wc, OPTS, SEARCH, XFER.dir_path, SEARCH.archive_db);
        }
        else if(XFER.export_task_group)
        {
            WibbleTransfer().export_task_group(wc, OPTS, XFER.dir_path);
        }
        else if(XFER.export_topic)
        {
            WibbleTransfer().export_topic(wc, OPTS, XFER.dir_path);
        }
    }
    
    // get rid of any extraction files, if they exist
    if(std::filesystem::exists(OPTS.paths.tmp_dir + "/.WIB_extract"))
    {
        std::filesystem::remove_all(OPTS.paths.tmp_dir + "/.WIB_extract");
    }
    
}
