/**
 * @file      wibbleversion.cpp
 * @brief     Provides latest build information.   
 * @details
 *
 * This class has latest build/version values injected via sed from the
 * Makefile. Simply provides information as to the latest running build.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibbleversion.hpp"

std::string wibble::WibbleVersion::get_version()
{
    // this is dynamically replaced in build/Makefile via sed/shell script
    const std::string CURRENT_VERSION = "20250226153654";
    return "Version:\n\nWibble++_" + CURRENT_VERSION + "\n";
}

std::string wibble::WibbleVersion::get_build_info()
{
    // this is dynamically replaced in build/Makefile via sed/shell script
    const std::string AUTHOR = "Wibble++ by Aren Tyr <https://www.codeberg.org/ArenTyr>\n";
    const std::string EMAIL = "aren.t.developer.l7b06@8shield.net\n\n";
    const std::string BUILD_INFO = "Built at: ";
    const std::string BUILD_DATE = "Wed, 26 Feb 2025 15:36:54 +0000";
    return AUTHOR + EMAIL + BUILD_INFO + BUILD_DATE + "\n";
}
