/**
 * @file      wibbledbinterface.hpp
 * @brief     Interface/parent virtual class defining core database interface.
 * @details
 *
 * Pure abstract/virtual class to inherit. Theoretical notion is possibility
 * of reimplementing using something other than ripgrep as backend, should
 * the need or some good reason to do so ever come into existence.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef WIBBLE_DBINTF_H
#define WIBBLE_DBINTF_H

#include <memory>
#include <vector>

#include "wibblerecord.hpp"
#include "wibbledbqueryinterface.hpp"
#include "wibblemetaschema.hpp"

// Pure abstract class for overriding
// Current implemented backend is ripgrep "wibblergdatabase.hpp" et al.
namespace wibble
{
    using WibResultList = std::unique_ptr<std::vector<WibbleRecord>>;
    class WibbleDBInterface
    {
    private:
        virtual bool persist_new_record(const wibble::WibbleRecord& record, const std::string& wib_db_path) const = 0;
        virtual bool persist_existing_record(const wibble::WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const = 0;

    public:
        virtual wibble::WibbleRecord select_record(WibResultList& w, unsigned long s) = 0;
        virtual WibResultList get_all_wibble_records(const std::string& wib_db_path, const std::string& WORKFILE) const = 0;
        virtual WibResultList get_wibble_records(const WibbleDBQueryInterface* query_exp, const std::string& wib_db_path, const std::string& WORKFILE) const = 0;
        virtual const metadata_schema generate_blank_schema() const = 0;
        virtual const metadata_schema parse_schema_file(const std::string& schema) const = 0;
        virtual WibbleRecord create_new_record_interactive(const metadata_schema ms, const std::string& t_mplate_ft) const = 0;
        virtual bool update_existing_record(wibble::WibbleRecord&,
                                            std::string&& da_f, std::string&& ti_f, std::string&& id_f,
                                            std::string&& de_f, std::string&& ty_f, std::string&& pr_f,
                                            std::string&& tg_f, std::string&& cl_f, std::string&& dd_f,
                                            std::string&& kp_f, std::string&& cu_f, std::string&& li_f) const = 0;
        virtual bool delete_record(const std::string& note_id, const std::string& wib_db_path, const std::string& WORKFILE) const = 0;
        virtual bool delete_record(const wibble::WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const = 0;
        virtual bool export_record(const wibble::WibbleRecord& record, const std::string& wib_db_path) const = 0;
        virtual bool persist_record(bool create_new, const wibble::WibbleRecord& record, const std::string& wib_db_path,
                                    const std::string& WORKFILE, const std::string& wibble_store) const = 0;
        virtual bool perform_template_substitutions(const wibble::WibbleRecord& record, const std::string& wib_db_path) const = 0;
    };
}
#endif
