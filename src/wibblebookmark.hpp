/**
 * @file      wibblebookmark.hpp
 * @brief     Main class with all bookmark operations (header file).
 * @details
 *
 * This class contains all of the "bookmark" functionality: the
 * ability to quickly store and retrieve frequently accessed
 * Wibble Nodes, Topic entries, Jotfiles, and Task entries.
 *
 * Futhermore it also offers the capability to "bookmark"/alias any 
 * Wibble command, or any arbritrary shell command. Such shell commands
 * can either be simple single-line/small commands, or instead can be an
 * entire custom script (sh, perl, python, whatever).
 *
 * Finally, the facility exists to create any number of custom
 * categories/"groups", each of which can contain any such selection
 * of defined command bookmarks. This provides the facility to build up
 * a very convenient dynamic "menu" of preferred commands, suitably organised,
 * thus saving the user from the burden of endless shell aliases,
 * remembering what/where particular shell commands or scripts do/exist,
 * or having to continually consult man pages and documentation to remember
 * particularly necessary switches or commands.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef WIBBLE_BOOKMARK_H
#define WIBBLE_BOOKMARK_H
#include <map>
#include <string>
#include <vector>
#include "cmd_structs.hpp"
#include "wibbleexecutor.hpp"

namespace wibble
{
    typedef struct bookmark_env
    {
        std::string env_node_bm_file;
        std::string env_gen_exec_bm_file;
        std::string env_jotfile_bm_file;
        std::string env_task_bm_file;
        std::string env_topic_bm_file;
        std::string env_wib_exec_bm_file;

        std::string env_cat_map_file;
        std::string env_cust_exec_bm_cat;

        bool env_cust_exec_cat_add_bm;
        bool env_cust_exec_cat_edit_bm;
        bool env_cust_exec_cat_del_bm;
        bool env_cust_exec_cat_sel_bm;

        bool env_add_cust_exec_cat;
        bool env_rm_cust_exec_cat;

    } bookmark_env;

    class WibbleBookmark
    {
    public:
        // forward declarations
        struct bookmark_entry; 
        struct cat_bm_entry;
    private:
        const std::string exec_args;
        
        const std::string cache_dir;
        const std::string cat_map_file;
        const std::string cust_exec_bm_cat;
        const std::string gen_exec_bm_file;
        const std::string jotfile_bm_file;
        const std::string node_bm_file;
        const std::string task_bm_file;
        const std::string topic_bm_file;
        const std::string wib_exec_bm_file;

        const bool cust_exec_cat_add_bm;
        const bool cust_exec_cat_del_bm;
        const bool cust_exec_cat_sel_bm;

        const bool add_cust_exec_cat;
        const bool rm_cust_exec_cat;

        const std::string CUST_PREFIX = "_CUST_";

        std::map<std::string, std::vector<bookmark_entry>> bookmarks;


        const pkg_options& b_OPTS;
        enum bookmark_type
        {
            NODE_BM,
            TOPIC_BM,
            JF_BM,
            TASK_BM,
            WEXEC_BM,
            GENEXEC_BM,
            CUST_EXEC_BM,
            HIB_CUST_EXEC_BM,
            UNDEF
        };

        WibbleConsole wc;
        WibbleExecutor wib_e;
 

        //bool _add_cmd_bm(const std::string& cat_name);
        bool _add_cmd_bm(const cat_bm_entry& custom_entry);
        bool _add_new_custom_bm();
        bool _add_new_custom_category();
        bool _add_new_general_cmd_bm();
        bool _add_new_jotfile_bm();
        bool _add_new_node_bm();
        bool _add_new_task_bm();
        bool _add_new_topic_bm();
        bool _add_new_wibble_cmd_bm();
        void _add_new_bm_menu();
        void _anum(const std::string& ltr);

        void _cnum(int entry_num);
        bool _check_cat_name_is_unique(const std::string& category_name);
        std::string _check_delete_script_warning(const std::string& exec_scr, const std::string& title, const bool custom_entry);

        bool _delete_existing_custom_cmd_bm(bookmark_entry& entry, cat_bm_entry& cbm_def);
        bool _delete_existing_general_cmd_bm(bookmark_entry& entry);
        bool _delete_existing_jotfile_bm(bookmark_entry& entry);
        bool _delete_existing_node_bm(bookmark_entry& entry);
        bool _delete_existing_task_bm(bookmark_entry& entry);
        bool _delete_existing_topic_bm(bookmark_entry& entry);
        bool _delete_existing_wibble_cmd_bm(bookmark_entry& entry);

        void _dispatch_add_bm_action(const int& choice);
        void _dispatch_bm_action(const int& choice);
        void _dispatch_category(const int& choice);
        void _delete_bm_menu();
        void _dispatch_delete_bm_action(const int& choice);
        void _dispatch_edit_bm_action(const int& choice);
        void _dispatch_node_action_menu(WibbleRecord& w, const bool archived);
        void _dispatch_topic_action_menu(const std::string& fn);

        void _edit_bm_menu();
        std::string _edit_cmd_wrapper(const std::string& cmd_field);
        bool _edit_existing_custom_cmd_bm(bookmark_entry& entry, cat_bm_entry& cbm_def);
        bool _edit_existing_jotfile_bm(bookmark_entry& entry);
        bool _edit_existing_node_bm(bookmark_entry& entry);
        bool _edit_existing_task_bm(bookmark_entry& entry);
        bool _edit_existing_topic_bm(bookmark_entry& entry);
        bool _edit_existing_general_cmd_bm(bookmark_entry& entry);
        bool _edit_existing_wibble_cmd_bm(bookmark_entry& entry);
        void _edit_bm_script(const std::string& script_fn);

        std::string _generate_entry_hsh();
        std::vector<bookmark_entry> _get_all_cust_category_ids();
        std::vector<bookmark_entry> _get_all_cust_exec_bm();
        std::vector<bookmark_entry> _get_all_cust_exec_bm_for_cat(const std::string& cat_name);
        std::vector<bookmark_entry> _get_all_gen_exec_bm();
        std::vector<bookmark_entry> _get_all_hib_cust_category_ids();
        std::vector<bookmark_entry> _get_all_jotfile_bm();
        std::vector<bookmark_entry> _get_all_node_bm();
        std::vector<bookmark_entry> _get_all_task_bm();
        std::vector<bookmark_entry> _get_all_topic_bm();
        std::vector<bookmark_entry> _get_all_wib_exec_bm();
        std::vector<std::string>    _get_all_cust_category_names();
        cat_bm_entry _get_display_custom_categories(const int& c_sel = -1, const bool hib_categories = false);

        void _pack_all_bm_data();
        bookmark_entry _pack_entry_from_line(const std::string& line, const int& type);
        std::vector<bookmark_entry> _process_bm_file(const int bm_type, const std::string& cust_cat_file = "");
        unsigned long _prompt_entry(const int& d_sel);

        std::vector<std::string> _reduce_to_names(const std::vector<bookmark_entry>& cat_names);
        bool _remove_custom_category();
        bool _remove_line_from_file(const std::string& unique_line_match, const std::string& index_fn, const std::string& update_line = "");
        bool _remove_line_from_index(bookmark_entry& entry, const std::string& cust_index_file = "");
        bool _rename_cust_category_entries(const std::string& cat_file, const std::string& existing_sn, const std::string& new_sn);
        void _render_menu();

        void _select_bm_edit_menu();

        bool _update_bookmark_line(const std::string& orig_line, const std::string& repl_line, const std::string& update_file);
    public:
        typedef struct cat_bm_entry
        {
            std::string short_name;
            std::string full_id;
            cat_bm_entry():
                short_name{"___NULL___"},
                full_id{"___NULL___"} {};
        } cat_bm_entry;
 
        typedef struct bookmark_entry
        {
            enum bookmark_type type;
            bool archived_node;

            std::string field1; 
            std::string field2; 
            std::string field3; 
            std::string field4; 
            std::string field5; 
            std::string field6; 
            
            std::string cust_cat_name;
            std::string cust_exec_id;
            std::string title;
            bookmark_entry():
                type{UNDEF},
                archived_node{false},
                field1{"___NULL___"},
                field2{"___NULL___"},
                field3{"___NULL___"},
                field4{"___NULL___"},
                field5{"___NULL___"},
                field6{"___NULL___"},
                cust_cat_name{"___NULL___"},
                cust_exec_id{"___NULL___"},
                title{"___NULL___"}
                {};
               
        } bookmark_entry;

        WibbleBookmark(bookmark_env BE, pkg_options& OPTS, const std::string& exec_a) :
            exec_args           {exec_a},
            cache_dir           {OPTS.paths.tmp_dir},
            cat_map_file        {BE.env_cat_map_file},
            cust_exec_bm_cat    {BE.env_cust_exec_bm_cat},
            gen_exec_bm_file    {BE.env_gen_exec_bm_file},
            jotfile_bm_file     {BE.env_jotfile_bm_file},
            node_bm_file        {BE.env_node_bm_file},
            task_bm_file        {BE.env_task_bm_file},
            topic_bm_file       {BE.env_topic_bm_file},
            wib_exec_bm_file    {BE.env_wib_exec_bm_file},
            cust_exec_cat_add_bm{BE.env_cust_exec_cat_add_bm},
            cust_exec_cat_del_bm{BE.env_cust_exec_cat_del_bm},
            cust_exec_cat_sel_bm{BE.env_cust_exec_cat_sel_bm},
            add_cust_exec_cat   {BE.env_add_cust_exec_cat},
            rm_cust_exec_cat    {BE.env_rm_cust_exec_cat},
            b_OPTS{OPTS},
            wib_e{OPTS}
            {
                _pack_all_bm_data();
            };

        // public wrappers
        void add_gen_exec_bm();
        void add_jotfile_bm();
        void add_new_node_bm();
        void add_task_bm();
        void add_topic_bm();
        void add_wib_exec_bm();

        // select operations
        bool select_cust_exec_bm(const int& op = 1, const int& c_sel = -1, const int& d_sel = -1);
        bool select_gen_exec_bm(const int& op = 1, const int& d_sel = -1);
        bool select_jotfile_bm(const int& op = 1, const int& d_sel = -1);
        bool select_node_bm(const int& op = 1, const int& d_sel = -1);
        bool select_task_bm(const int& op = 1, const int& d_sel = -1);
        bool select_topic_bm(const int& op = 1, const int& d_sel = -1);
        bool select_wib_exec_bm(const int& op = 1, const int& d_sel = -1);

        // delete operations
        void delete_gen_exec_bm(const int& d_sel = -1);
        void delete_jotfile_bm(const int& d_sel = -1);
        void delete_node_bm(const int& d_sel = -1);
        void delete_task_bm(const int& d_sel = -1);
        void delete_topic_bm(const int& d_sel = -1);
        void delete_wib_exec_bm(const int& d_sel = -1);

        // edit operations
        void edit_gen_exec_bm(const int& d_sel = -1);
        void edit_jotfile_bm(const int& d_sel = -1);
        void edit_node_bm(const int& d_sel = -1);
        void edit_task_bm(const int& d_sel = -1);
        void edit_topic_bm(const int& d_sel = -1);
        void edit_wib_exec_bm(const int& d_sel = -1);
        
        // custom category operations
        void cust_add_category();
        void cust_ren_category(const int& c_sel = -1);
        void cust_rm_category();
        void cust_category_add_bm(const int& c_sel);
        void cust_category_del_bm(const int& c_sel, const int& d_sel);
        void cust_category_edit_bm(const int& c_sel, const int& d_sel);
        void cust_category_activate(const int& c_sel = -1);
        void cust_category_hibernate(const int& c_sel = -1);

        // display operations
        void display_node_bm();
        void display_topic_bm();
        void display_jotfile_bm();
        void display_task_bm();
        void display_wib_exec_bm();
        void display_gen_exec_bm();
        void display_custom_bm(const std::string& cat_name, const std::vector<bookmark_entry>& category);

        static std::string index_to_lookup_name(const int& choice);

        // main menu
        void select_bm_menu();
        void select_bm_edit_menu();
    };

    static const std::string BM_CAT_1     = "1_node_bm";
    static const std::string BM_CAT_2     = "2_topic_bm";
    static const std::string BM_CAT_3     = "3_jotfile_bm";
    static const std::string BM_CAT_4     = "4_task_bm";
    static const std::string BM_CAT_5     = "5_wib_exec_bm";
    static const std::string BM_CAT_6     = "6_gen_exec_bm";
    static const std::string CUST_CAT_MAP = "7_category_map";
}

typedef wibble::WibbleBookmark::bookmark_entry wbe;

#endif
