/**
 * @file      wibblejotter.hpp
 * @brief     Provides the Jotfile functionality ("jot") (header file).
 * @details
 *
 * This class provides the functionality related to the single-file
 * sectioned "Jotfile" operations: essentially a single file that can
 * be parsed and worked on in sections for quickly retrieving, adding,
 * or removing quick notes to, simply from the console, no editor needed.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_JOTTER_H
#define WIBBLE_JOTTER_H

#include "wibbleexecutor.hpp"

namespace wibble
{
    class WibbleJotter
    {
    public:
        /**
         * @brief Struct defining a Jotfile.
         */
        typedef struct jotfile
        {
            std::string fn;       //!< Underlying filename of jotfile
            std::string name;     //!< Short name of jotfile
            std::string category; //!< Category of jotfile
            unsigned long index;  //!< Numbered index (auto-incremented) of jotfile
            jotfile():
                fn{""},
                name{""},
                category{""},
                index{0} {};
        } jotfile;

        /**
         * @brief Struct defining a Jotfile entry.
         */
        typedef struct jf_entry
        {
            std::string date;        //!< Datetime stamp of entry
            std::string title;       //!< Title of entry
            unsigned long starts_at; //!< Starting line number/index of start of entry
            unsigned long ends_at;   //!< Ending line number/index of end of entry
            jf_entry():
                date{""},
                title{""},
                starts_at{0},
                ends_at{0} {};
        } jf_entry;

    private:

        // core delimiters and constants that are pattern match to determine boundaries of Jotfile entries
        const std::string DELIM = "|";
        const std::string JF_ENTRY_PREFIX  = "\n\n[>->->->->------------ [ ";
        const std::string JF_ENTRY_POSTFIX = " ] ------------>->->->->]\n";
        const std::string NEW_DATE_PLACHDR = "----- [ NEW ] -----";
        const std::string TIT_OPEN         = "[= ";
        const std::string TIT_CLOSE        = "=]\n";
        const std::string JF_ENTRY_TERM    = "[<-<-<-<-<-------------------------------------------------<-<-<-<-<]\n\n";
        const unsigned int HEADER_WIDTH = 64;

        const std::string cache_dir;
        const std::string jotfile_base_dir;
        const std::string filter_titles;

        std::vector<jotfile> jotfiles;

        void _add_new_entry(WibbleConsole& wc, const std::string& jotfile_fn) const;
        void _cache_warning(WibbleConsole& wc) const;
        bool _do_delete_confirm(WibbleConsole& wc, const std::string& jotfile_fn) const;
        unsigned long _find_numeric_gap(const std::vector<jotfile>& jotfiles) const;
        jotfile _decompose_fn_to_jotfile(const std::string& fn) const;
        void _gather_jotfiles(WibbleConsole& wc, const std::string& jot_base_dir);
        std::string _get_jotfile_entry(const jf_entry& entry, const std::string& jf_fn) const;
        std::vector<jf_entry> _parse_jotfile_into_entries(WibbleConsole& wc, const std::string& jf_fn, const bool do_delete = false) const;
        void _rename_existing_jotfile(WibbleConsole& wc, const std::string& jotfile_fn);
        bool _write_out_replacement_file(const std::string& orig_jf, const jf_entry& entry_to_update,
                                         const std::string& new_title, const std::string& new_content, bool expurge_entry = false) const;
        jf_entry select_entry(std::vector<jf_entry>& entries, WibbleConsole& wc, unsigned long& preselect) const;
        unsigned long select_jotfile(WibbleConsole& wc);
        void display_delete_or_edit_entry(WibbleConsole& wc, int mode, const jf_entry& jf, const std::string& jf_fn);
        void _jotfile_operation(WibbleConsole& wc, const int& op, const std::string& direct_jf = "", const unsigned long& preselect_jf = 0, const unsigned long& preselect_entry = 0);
    public:

        void append_new_entry(WibbleConsole& wc);
        bool batch_add_new_entry(WibbleConsole& wc, const std::string& jotfile_fn, std::string& entry_title, const std::string& entry_text) const;
        bool create_jotfile(WibbleConsole& wc, const std::string& d_category = "", const std::string& d_name = "") const;
        void delete_jotfile_entry(WibbleConsole& wc, const unsigned long& preselect_jf = 0, const unsigned long& preselect_entry = 0);
        void direct_add_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn);
        void direct_delete_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn);
        void direct_edit_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn);
        void direct_view_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn);
        void dump_jotfile(WibbleConsole& wc, WibbleExecutor& wib_e, const unsigned long& preselect_jf = 0);
        void edit_jotfile_entry(WibbleConsole& wc, const unsigned long& preselect_jf = 0, const unsigned long& preselect_entry = 0);
        void list_jotfiles(WibbleConsole& wc, const bool selector = false) const;
        const std::string get_jotfile(WibbleConsole& wc, const bool rel_path = false) const;
        bool direct_remove_jotfile(WibbleConsole& wc, const std::string& jotfile_fn);
        void rename_jotfile(WibbleConsole& wc, const unsigned long& preselect_jf = 0);
        void remove_jotfile(WibbleConsole& wc);
        void view_jotfile_entry(WibbleConsole& wc, const unsigned long& preselect_jf = 0, const unsigned long& preselect_entry = 0);
        WibbleJotter(WibbleConsole& wc, const std::string& jot_base_dir, const std::string& cache_dir, const std::string& filter_titles = ""):
            cache_dir{cache_dir},
            jotfile_base_dir{jot_base_dir},
            filter_titles{filter_titles}
        {
            _gather_jotfiles(wc, jot_base_dir);
        };
    };
}
#endif
