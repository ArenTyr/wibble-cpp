/**
 * @file      wibbletask.cpp
 * @brief     Provides all of the "task" functionality.
 * @details
 *
 * This class provides all of the functionality related to task
 * and project management. 'Tasks' can be added, put into groups,
 * and associated with Nodes, Topics, Jotfiles. They can be
 * 'extended' into a 'project' with detailed description,
 * task-specific Jotfile, and custom project files added/stored
 * with the task entry. Priority, due date and category can be set
 * and edited; theese criteria can be used for filtering and finding
 * relevant tasks. Finally they can completed, cloned, and reactivated.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "external/rang.hpp"

#include "wibbleactions.hpp"
#include "wibbleexit.hpp"
#include "wibbleexecutor.hpp"
#include "wibblejotter.hpp"
#include "wibbletask.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"

namespace wibble
{
    // handy aliases
    using te     = WibbleTask::task_entry;
    using tle    = std::vector<WibbleTask::task_entry>;
    using tgs    = WibbleTask::task_group;
    using tg_set = WibbleTask::task_group_set;
}

/**
 * @brief Add or edit a detailed (inline) description for task using GNU ReadLine library.
 * @param entry Task entry struct with task entry details to add/edit description into
 */
bool wibble::WibbleTask::_add_edit_inline_task_details(task_entry& entry)
{
    try
    {
        wc.console_write_header("DETAILS");
        const std::string desc_file = _get_desc_file_for_task(entry.task_id);
        std::string prefill = "";
        if(std::filesystem::exists(std::filesystem::path(desc_file)))
        {
            std::stringstream ss = wibble::WibbleIO::read_file_into_stringstream(desc_file);
            prefill = ss.str();
            std::stringstream().swap(ss);
        }
        std::string details = wc.console_write_prompt_ml("Add/edit task details/description. Press Ctrl-D when done.>\n", "", prefill);

        wc.trim(details);
        if(details != "")
        {
            wibble::WibbleIO::create_path(wibble::WibbleIO::get_parent_dir_path(desc_file));
            wibble::WibbleIO::write_out_file_overwrite(details, desc_file);
        }
        else
        {
            if(std::filesystem::exists(std::filesystem::path(desc_file)))
            { std::filesystem::remove(std::filesystem::path(desc_file)); }
        }

        return true;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _add_edit_inline_task_details(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Add a (new) task entry to a given task group.
 * @param tg task_group struct with task grup to add entry into
 * @param task_id string with unique task entry identifier/hash
 * @param category string with task category/tag task is labelled with
 */
bool wibble::WibbleTask::_add_task_to_group(const task_group& tg, const std::string& task_id, const std::string& category) const
{

    try
    {
        const std::string tg_file = tasks_base_dir + OPEN_GROUP_PATH + tg.fn.substr(0,4) + "/" + tg.fn;
        //const std::string tg_file = tasks_base_dir + OPEN_GROUP_PATH + wibble::WibbleRecord::generate_date_YYYY("") + "/" + tg.fn;
        if(! std::filesystem::exists(tg_file))
        { wc.console_write_error_red("Task group file: '" + tg_file + "' does not exist/is not readable."); return false; }

        std::ofstream new_group_file;
        new_group_file.open(tg_file + ".new", std::ios::out | std::ios::trunc);
        // updated first line
        const std::string def_line = tg.group_id + FIELD_DELIM + std::to_string(tg.com_tasks)
                                     + FIELD_DELIM + std::to_string(tg.tot_tasks + 1) + FIELD_DELIM
                                     + tg.group_name + '\n';
        new_group_file << def_line;
        // now ids
        for(auto const& id: tg.nodes)
        {
            new_group_file << id.first + "|" + id.second + '\n';
        }
        // don't forget new id
        new_group_file << "+_" + task_id + "|" + category + '\n';
        new_group_file.close();

        // create backup in cache dir
        return WibbleUtility::create_backup_and_overwrite_file(cache_dir, tg_file + ".new", tg_file);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _add_task_to_group(): " << ex.what() << std::endl;
        return false;
    }
}


/**
 * @brief Adjust the properties/settings of an existing task entry.
 * @param entry task entry struct with task entry details to add/edit description into
 * @param closed_group flag indicating whether task exists in a closed group [file]
 */
bool wibble::WibbleTask::_adjust_task_properties(task_entry& entry, bool closed_group)
{
    print_task_detailed(entry);
    std::string resp = wc.console_write_prompt("Edit the above task? (y/n) > ", "", "");
    if(resp != "y" && resp != "Y") { return false; }

    bool do_update = false;
    // if blank, category will remain untouched
    std::string existing_category = "";

    task_entry replacement_entry = entry;

    // title
    std::string new_title = wc.console_write_prompt("Task title            *: ", "", entry.title); wc.trim(new_title);
    wibble::WibbleRecord::remove_pipe(new_title);
    if(new_title == "") { wc.console_write_error_red("Title cannot be blank, title will be unchanged."); }
    else if(new_title == entry.title) { } // no change
    else                { do_update = true; replacement_entry.title = new_title; }

    // category
    std::string new_category = wc.console_write_prompt("Task category         *: ", "", entry.category); wc.trim(new_category);
    wibble::WibbleRecord::sanitise_input(new_category);
    if(new_category == "") { wc.console_write_error_red("Category cannot be blank, category will be unchanged."); }
    else if(new_category == entry.category) { } // no change
    else                   { do_update = true; existing_category = entry.category; replacement_entry.category = new_category; }

    // priority
    int new_priority = 0;
    std::string inp_priority = wc.console_write_prompt("Task priority [1-99] *: ", "", std::to_string(entry.priority)); wc.trim(inp_priority);
    try { new_priority = std::stoi(inp_priority); if(new_priority < 1 || new_priority > 99) { new_priority = 0; } } catch (std::exception& ex) { new_priority = 0; }
    if(inp_priority == "") { wc.console_write_error_red("Priority cannot be blank, priority will be unchanged."); }
    if(new_priority == 0)  { wc.console_write_error_red("Invalid priority input. Priority will be unchanged."); }
    else if(new_priority == entry.priority) { } // no change
    else { do_update  = true; replacement_entry.priority = new_priority; }


    // due date
    std::string prefill_date = entry.due_date;
    if(prefill_date == "___NONE___") { prefill_date = ""; }
    std::string new_dd = wc.console_write_prompt("Task due date         : ", "", prefill_date); wc.trim(new_dd);
    // validate newly updated date
    if(new_dd != "")
    {
        if(new_dd != entry.due_date) // guard against pointless same update
        {
            std::string new_due_date = _get_valid_datetime_str_or_NONE(new_dd);
            if(new_due_date == "___NONE___") { wc.console_write_error_red("Invalid due date input. Due date will be unchanged."); }
            else { do_update = true; replacement_entry.due_date = new_due_date; }
        }
    }

    if(do_update)
    {
        return _update_task_details(replacement_entry, closed_group, existing_category);
    }
    else
    { wc.console_write_error_red("Nothing to update, aborting."); return false; }
}

/**
 * @brief Wrapper function to apply a filter of set of task IDs to given task file before parsing.
 * @param fn Task file with entries to process/work upon
 * @param id_list set containing unique IDs to filter by
 * @param closed_group flag indicating whether task exists in a closed group [file]
 */
wibble::tle wibble::WibbleTask::_parse_task_file_with_filter(const std::string& fn, const std::set<std::string>& id_list, const bool& mark_completed)
{
    //std::cout << "DEBUG: using fn: " << fn << std::endl;
    std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(fn);
    return _parse_task_file(task_ss, fn, id_list, mark_completed);
}

/**
 * @brief Parse a given task entry (i.e. task list) file with optional filter of selected IDs, optionally mark task as complete
 * @param fn Task file with entries to process/work upon
 * @param id_list set containing unique IDs to filter by, can be empty for no filter/all tasks in file
 * @param mark_completed flag to mark a task as completed; in conjunction with id_list can target an individual task entry
 */
wibble::tle wibble::WibbleTask::_parse_task_file(std::stringstream& task_ss, const std::string& fn, const std::set<std::string>& id_list, const bool& mark_completed)
{
    tle task_entries;
    try
    {
        std::string line;
        while(std::getline(task_ss, line, '\n'))
        {
            te task;
            //20240205135820_c2ea8a92|S|5|___NONE___|general|Testing

            std::size_t task_id_sep  = line.find(FIELD_DELIM);
            std::size_t task_ext_sep = line.find(FIELD_DELIM, task_id_sep + 1);
            std::size_t task_pri_sep = line.find(FIELD_DELIM, task_ext_sep + 1);
            std::size_t task_due_sep = line.find(FIELD_DELIM, task_pri_sep + 1);
            std::size_t task_cat_sep = line.find(FIELD_DELIM, task_due_sep + 1);

            task.task_id = line.substr(0, task_id_sep);                                                      // #1. id
            if(line.substr(task_id_sep + 1, 1) == "E") { task.extended = true; }                             // #2. extended (default is not extended)
            std::string priority   = line.substr(task_ext_sep + 1, (task_pri_sep - 1 - task_ext_sep));
            try { task.priority = std::stoi(priority);  } catch (std::exception& ex) { task.priority = 50; } // #3. priority
            task.due_date = line.substr(task_pri_sep + 1, (task_due_sep - 1 - task_pri_sep));                // #4. due date
            task.category = line.substr(task_due_sep + 1, (task_cat_sep - 1 - task_due_sep));                // #5. category
            task.title = line.substr(task_cat_sep + 1, (line.length() - task_cat_sep));                      // #6. title
            task.fn = fn;
            if(mark_completed) { task.completed = true; }

            //std::cout << "DEBUG: got vals:" << std::endl;
            //std::cout << task.task_id  << std::endl; std::cout << " -> " << id_list.size() << std::endl;
            //std::cout << task.extended << std::endl;
            //std::cout << task.priority << std::endl;
            //std::cout << task.due_date << std::endl;
            //std::cout << task.category << std::endl;
            //std::cout << task.title    << std::endl;

            // use optional filter
            if(id_list.empty()) { task_entries.push_back(task);                                          }
            else                { if(id_list.count(task.task_id) == 1) { task_entries.push_back(task); } }
        }


        std::stringstream().swap(task_ss);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _parse_task_file() : " << ex.what() << std::endl;
    }

    return task_entries;
}

/**
 * @brief Parse a given task group definition file.
 * @param group_ss stringstream pointing at group file to process
 */
wibble::tgs wibble::WibbleTask::_parse_group_file(std::stringstream& group_ss)
{
    task_group tg;
    try
    {
        std::string line;
        bool first_line = true;
        while(std::getline(group_ss, line, '\n'))
        {
            // group metadata on first line
            if(first_line)
            {
                // abce132|3|6|group title
                std::size_t g_id_sep   = line.find(FIELD_DELIM);
                std::size_t com_tk_sep = line.find(FIELD_DELIM, g_id_sep + 1);
                std::size_t tot_tk_sep = line.find(FIELD_DELIM, com_tk_sep + 1);
                tg.group_id = line.substr(0, g_id_sep);
                std::string com_t_num = line.substr(g_id_sep + 1, (com_tk_sep - 1 - g_id_sep));
                std::string tot_t_num = line.substr(com_tk_sep + 1, (tot_tk_sep - 1 - com_tk_sep));
                try { tg.com_tasks = std::stoi(com_t_num); } catch (std::exception& ex) { tg.com_tasks = -1; }
                try { tg.tot_tasks = std::stoi(tot_t_num); } catch (std::exception& ex) { tg.com_tasks = -1; }
                tg.group_name = line.substr(tot_tk_sep + 1, (line.length() - tot_tk_sep));

                first_line = false;

                //std::cout << tg.group_name << " " << tg.com_tasks << " " << tg.tot_tasks << std::endl;
                continue;
            }

            // now gather up the ids
            std::size_t cat_sep  = line.find(FIELD_DELIM);
            std::string node_id  = line.substr(0, cat_sep);
            std::string category = line.substr(cat_sep + 1, line.length() - cat_sep);

            std::pair<std::string, std::string> node_cat{node_id, category};
            tg.nodes.push_back(node_cat);
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _parse_group_file() : " << ex.what() << std::endl;
    }

    return tg;
}

/**
 * @brief Read in (accumulate) from file all of the closed/completd tasks.
 * @param silent flag indicating whether to suppress any console output
 */
void wibble::WibbleTask::_accumulate_closed_tasks(const bool silent)
{
    _accumulate_tasks(false, silent);
}

/**
 * @brief Read in (accumulate) from file all of the open/todo tasks.
 * @param silent flag indicating whether to suppress any console output
 */
void wibble::WibbleTask::_accumulate_open_tasks(const bool silent)
{
    _accumulate_tasks(true, silent);
}

/**
 * @brief Parent function for obtaining all of the current tasks (open or closed).
 * @param o_tasks flag indicating whether to read in/process open tasks (or instead, closed)
 * @param silent flag indicating whether to suppress any console output
 */
void wibble::WibbleTask::_accumulate_tasks(const bool o_tasks, const bool silent)
{
    std::string tasks_path;
    if(o_tasks)    { tasks_path = tasks_base_dir + OPEN_TASK_PATH;   }
    else           { tasks_path = tasks_base_dir + CLOSED_TASK_PATH; }
    const std::filesystem::path tasks_dir{tasks_path};
    if(! silent) { wc.console_write_green(ARW + "Processing tasks under: " + tasks_path + '\n'); }
    if(std::filesystem::exists(tasks_dir))
    {
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{tasks_dir})
        {
            if(dir_entry.is_regular_file())
            {
                //std::cerr << "Parsing: " << dir_entry.path() << '\n';
                std::stringstream task_fl_ss = wibble::WibbleIO::read_file_into_stringstream(dir_entry.path());
                // pass empty filter set
                tle tasks = _parse_task_file(task_fl_ss, std::filesystem::path(dir_entry).filename(), std::set<std::string>(), false);
                // add the YYYY_ stub to hash, since every file needs a unique hash and categories
                // get re-used from year to year
                const auto year_stub = std::string(std::filesystem::path(dir_entry).filename()).substr(0,4);
                // add into our open/closed task map
                if(tasks.size() > 0)
                {
                    const std::string task_cat = tasks[0].category;
                    if(o_tasks) { open_tasks[year_stub + " " + task_cat] = tasks;   }
                    else        { closed_tasks[year_stub + " " + task_cat] = tasks; }
                }
                std::stringstream().swap(task_fl_ss);
            }
        }
    }
}

/**
 * @brief Function for allowing addition/attachment/creation of a 'project' file, plain-text with any extension, to a task.
 * @param entry task entry struct with task to attach to/create project file inside task directory for
 */
bool wibble::WibbleTask::_add_project_files(task_entry& entry)
{
    std::string ext = wc.console_write_prompt("Input file extension/file type: ", "", "txt");
    wibble::WibbleRecord::sanitise_input(ext);
    wc.trim(ext);
    if(ext == "") { wc.console_write_error_red("Filetype cannot be blank. Aborting."); return false;   }
    std::string title = wc.console_write_prompt("Input title/name of document  : ", "", "");
    if(title == "") { wc.console_write_error_red("Filetype cannot be blank. Aborting."); return false; }
    try
    {
        const std::string dd = _get_data_dir_for_task(entry.task_id);
        const std::string proj_dir = dd + entry.task_id + "_proj/";
        bool setup_dir = wibble::WibbleIO::create_path(proj_dir);
        if(setup_dir)
        {
            const std::string proj_file = wibble::WibbleRecord::generate_entry_hash() + "." + ext;

            const std::string bin_edit_cmd = WibbleUtility::get_binary_tool(wc, ext, bin_mapping, true);
            bool wo = false;
            if(bin_edit_cmd != "") { wo = WibbleIO::wcopy_file(templates_dir + "/bin_default/default." + ext, proj_dir + proj_file); }
            else                   { wo = wibble::WibbleIO::write_out_file_overwrite(" ", proj_dir + proj_file);                     }
            if(wo)
            {
                const std::string record = proj_file + DELIM + ext + DELIM + title + '\n';
                const std::string pr_idx = _get_proj_index_for_task(entry.task_id);
                _write_history(entry, 12, "File: '" + proj_file + "'. ");
                wo = wibble::WibbleIO::write_out_file_append(record, pr_idx);
                return wo;
            }
            else
            {
                wc.console_write_error_red("Unable to write project file. Aborting.");
                return false;
            }
        }
        else
        {
            wc.console_write_error_red("Unable to setup project directory. Aborting.");
            return false;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _add_extended_notes(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Function for setting/ensuring default group exists and task is assigned into it, if not in a custom group.
 */
wibble::tgs wibble::WibbleTask::_set_default_group()
{
    const std::string YYYY = wibble::WibbleRecord::generate_date_YYYY("");
    const std::string YMD  = wibble::WibbleRecord::generate_date_YMD("");
    try
    {
        const std::string df_group_path = tasks_base_dir + OPEN_GROUP_PATH + YYYY + "/";
        const std::string df_group_file = df_group_path + YYYY + DF_GROUP_FN_STUB;
        if(! std::filesystem::exists(df_group_file))
        {
            wibble::WibbleIO::create_path(df_group_path);
            wc.console_write_success_green("Default group file does not exist, initialising.");
            std::ofstream new_group_file;
            new_group_file.open(df_group_file, std::ios::out | std::ios::trunc);
            new_group_file << YYYY + DF_GROUP_ID_STUB + "|0|0|General - " + YYYY + " - (default group)\n";
            new_group_file.close();
            task_group tg;
            tg.fn         = YYYY + DF_GROUP_FN_STUB;
            tg.group_name = DF_GROUP + YMD + " ]";
            tg.group_id   = YYYY + DF_GROUP_ID_STUB;
            tg.com_tasks  = 0;
            tg.tot_tasks  = 0;
            return tg;
        }
        else
        {
            return task_groups[YYYY + DF_GROUP_FN_STUB];
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error writing to default group file. Aborting.");
        throw wibble::wibble_exit(1);
    }
}


/**
 * @brief Function for setting 'extended' flag on task to indicate presence of additional data.
 * @param entry task entry struct with task to set flag for
 */
bool wibble::WibbleTask::_set_extended_task_attribute(task_entry& task)
{
    if(task.extended) { return true; }
    else
    {
        task.extended = true; // now mark/persist entry as extended
        return _update_existing_task_entry(task);
    }
}


/**
 * @brief Function for reading in all (open) task group definitions.
 * @param silent flag indicating whether to suppress any console output
 */
void wibble::WibbleTask::_accumulate_task_groups(const bool silent)
{
    const std::string tasks_group_path = tasks_base_dir + OPEN_GROUP_PATH;
    const std::filesystem::path tasks_group_dir{tasks_group_path};
    if(! silent) { wc.console_write_green(ARW + "Processing task group definitions under: " + tasks_group_path + '\n'); }
    if(std::filesystem::exists(tasks_group_dir))
    {
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{tasks_group_dir})
        {
            if(dir_entry.is_regular_file())
            {
                //std::cerr << "Parsing: " << dir_entry.path() << '\n';
                std::stringstream group_fl_ss = wibble::WibbleIO::read_file_into_stringstream(dir_entry.path());
                tgs tg = _parse_group_file(group_fl_ss);
                tg.fn = std::filesystem::path(dir_entry).filename();
                task_groups[tg.fn] = tg;
            }
        }
    }
}

/**
 * @brief Function for reading in all closed task group definitions.
 * @param silent flag indicating whether to suppress any console output
 */
void wibble::WibbleTask::_accumulate_closed_task_groups(const bool silent)
{
    const std::string tasks_group_path = tasks_base_dir + CLOSED_GROUP_PATH;
    const std::filesystem::path tasks_group_dir{tasks_group_path};
    if(! silent) { wc.console_write_green(ARW + "Processing closed task group definitions under: " + tasks_group_path + '\n'); }
    if(std::filesystem::exists(tasks_group_dir))
    {
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{tasks_group_dir})
        {
            if(dir_entry.is_regular_file())
            {
                //std::cerr << "Parsing: " << dir_entry.path() << '\n';
                std::stringstream group_fl_ss = wibble::WibbleIO::read_file_into_stringstream(dir_entry.path());
                tgs tg = _parse_group_file(group_fl_ss);
                tg.fn = std::filesystem::path(dir_entry).filename();
                task_groups_archived[tg.fn] = tg;
            }
        }
    }
}

/**
 * @brief Associate/link an arbitrary external file/directory with task entry.
 * @param entry task entry struct with task to set flag for
 */
bool wibble::WibbleTask::_associate_linked_file_with_task(task_entry& entry, const std::string& data_file, const std::string& data_action)
{
    std::string lf;
    if(data_file == "")
    {
        wc.console_write_header("LINK FILE");
        std::cout << '\n';
        lf = wc.console_write_prompt("Input path to file (absolute or relative)\n>", "", "");
    }
    else { lf = data_file; }
    wc.trim(lf);
    wibble::WibbleUtility::expand_tilde(lf);
    wibble::WibbleUtility::remove_trailing_slash(lf); // important to ensure filename() works as desired
    wibble::WibbleUtility::fix_file_param_get_input(wc, lf);

    if(lf == "") { wc.console_write_error_red("File must exist and/or filename cannot be blank. Aborting."); return false; }
    std::string resp;
    if(data_action == "")
    {
        wc.console_write_yellow_bold("\nSelect option:\n\n");
        wc.console_write_yellow("a. "); wc.console_write_yellow_bold("Link"); wc.console_write_yellow(" to file.\n");
        wc.console_write_yellow("b. "); wc.console_write_yellow_bold("Copy"); wc.console_write_yellow(" file into associated task data directory.\n");
        wc.console_write_yellow("c. "); wc.console_write_yellow_bold("Move"); wc.console_write_yellow(" file into associated task data directory.\n");

        resp = wc.console_write_prompt("\nSelect choice [a:c] > ", "", "");
    }
    else { resp = data_action; }

    if(resp != "a" && resp != "A" && resp != "b" && resp != "B" && resp != "c" && resp != "C")
    {
        wc.console_write_error_red("Invalid choice. Exiting.");
        return false;
    }

    const std::string task_lf = _get_task_data_lf_fn(entry.task_id);
    const std::string task_dd = _get_data_dir_for_task(entry.task_id);
    const std::string lf_dd = _get_proj_data_dir_for_task(entry.task_id) + WIB_LINKED;
    const std::string fn_stub = std::string(std::filesystem::path(lf).filename());

    if(resp == "a" || resp == "A")
    {
        bool setup_dir = wibble::WibbleIO::create_path(task_dd);
        if(setup_dir)
        {
            _set_extended_task_attribute(entry);
            _write_history(entry, 6, "File: '" + fn_stub + "'. ");

            return wibble::WibbleIO::write_out_file_append(lf + "\n", task_lf);
        }
        else { wc.console_write_error_red("Error creating output directory. Aborting."); return false; }
    }
    else if(resp == "b" || resp == "B")
    {
        //std::cerr << "DEBUG: trying to copy '" << lf << "' to '" << lf_dd + fn_stub << "'" << std::endl;
        if(std::filesystem::exists(std::filesystem::path(lf_dd + fn_stub)))
        { wc.console_write_error_red("Destination file already exists. Aborting.", false); return false; }

        bool setup_dir = wibble::WibbleIO::create_path(lf_dd);
        if(setup_dir)
        {
            bool cf = wibble::WibbleIO::wcopy_file(lf, lf_dd + fn_stub);
            if(cf)
            {
                _set_extended_task_attribute(entry);
                wc.console_write_success_green("Copy successful.");
                _write_history(entry, 7, "File: '" + fn_stub + "'. ");
                return wibble::WibbleIO::write_out_file_append("___TASKDIR___" + fn_stub + "\n", task_lf);
            }
        }
        else { wc.console_write_error_red("Error creating output directory. Aborting."); return false; }
    }
    else if(resp == "c" || resp == "C")
    {

        if(std::filesystem::exists(std::filesystem::path(lf_dd + fn_stub)))
        { wc.console_write_error_red("Destination file already exists. Aborting."); return false; }

        const bool direct = (data_action == "c") ? true : false;
        wibble::WibbleUtility::confirm_file_move_or_exit(wc, lf, lf_dd + fn_stub, direct);
        bool setup_dir = wibble::WibbleIO::create_path(lf_dd);
        if(setup_dir)
        {
            bool mf = wibble::WibbleIO::wmove_file(lf, lf_dd + fn_stub);
            if(mf)
            {
                _set_extended_task_attribute(entry);
                wc.console_write_success_green("Move successful.");
                _write_history(entry, 8, "File: '" + fn_stub + "'. ");
                return wibble::WibbleIO::write_out_file_append("___TASKDIR___" + fn_stub + "\n", task_lf);
            }
            else { wc.console_write_error_red("Error creating output directory. Aborting."); return false; }
        }
        else { wc.console_write_error_red("Error creating output directory. Aborting."); return false; }
    }

    return false;
}

/**
 * @brief Associate/link a Jotfile with task entry.
 * @param entry task entry struct with task to set flag for
 * @param OPTS pkg_options environment settings/configuration struct
 */
bool wibble::WibbleTask::_associate_jotfile_with_task(task_entry& entry, const pkg_options& OPTS)
{

    bool has_task_jf = false;
    const std::string df_jotfile = _get_task_specific_jf_fn(entry.task_id);
    if(std::filesystem::exists(std::filesystem::path(df_jotfile))) { has_task_jf = true; }

    wc.console_write_header("JOTFILES");
    if(! has_task_jf)
    {
        wc.console_write_yellow("1. ");
        wc.console_write_yellow_bold("Add new ");
        wc.console_write_yellow("task specific jotfile and add an entry.\n");
    }
    else
    {
        wc.console_write_yellow("1. ");
        wc.console_write_yellow_bold("Add ");
        wc.console_write_yellow("entry to existing task specific jotfile.\n");
    }

    wc.console_write_yellow("2. ");
    wc.console_write_yellow_bold("Associate");
    wc.console_write_yellow(" general jotfile ");
    wc.console_write_yellow_bold("jotfile ");  wc.console_write_yellow("with task.\n");
    wc.console_write_hz_divider();
    std::string resp = wc.console_write_prompt("\nSelect action > ", "", "");
    wc.trim(resp);

    std::cout << '\n';
    int choice = 0;
    try
    {
        choice = std::stoi(resp);
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid choice, exiting.");
        return false;
    }

    bool result = false;
    WibbleJotter wj{wc, OPTS.paths.jot_dir, OPTS.paths.tmp_dir};
    std::string wrk_jf;
    std::string jf_fn;
    switch(choice)
    {
    case 1:
        if(has_task_jf) { wc.console_write_green(ARW + "Default task jotfile already exists, appending entry.\n"); }
        result = wibble::WibbleIO::create_path(_get_data_dir_for_task(entry.task_id));
        if(result)
        {
            wj.direct_add_jotfile_entry(wc, df_jotfile);
            _write_history(entry, 11, "Jotfile: '" + df_jotfile + "'. ");
            return _set_extended_task_attribute(entry);
        }
        else { return false; }
    case 2:
        wrk_jf = wj.get_jotfile(wc);
        result = wibble::WibbleIO::write_out_file_append(wrk_jf, _get_task_data_jf_fn(entry.task_id));
        if(result)
        {
            wc.console_write_success_green("Jotfile successfully associated with task.");
            _write_history(entry, 11, "Jotfile: '" + df_jotfile + "'. ");
            return _set_extended_task_attribute(entry);
        }
        else { return false; }
    default:
        wc.console_write_error_red("Invalid choice, exiting.");
        return false;
    }
}

/**
 * @brief Associate/link a Node with task entry.
 * @param entry task entry struct with task to set flag for
 * @param OPTS pkg_options environment settings/configuration struct
 */
bool wibble::WibbleTask::_associate_node_with_task(task_entry& entry, const pkg_options& OPTS)
{

    search_filters SEARCH;
    SEARCH.menu_search = true;
    wibble::WibbleUtility::menu_search_set_filters(wc, SEARCH);

    WibbleExecutor wib_e{OPTS};
    WibbleRecord w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH);

    if(w.get_id() == "___NULL___") { wc.console_write_error_red("No valid node selected."); return false; }

    const std::string task_node_data_dir = _get_data_dir_for_task(entry.task_id);
    const std::string task_node_data_fn  = _get_task_data_nodes_fn(entry.task_id);
    //std::cout << "DEBUG: task_node_data_fn = " << task_node_data_fn << std::endl;
    bool create_path = wibble::WibbleIO::create_path(task_node_data_dir);
    if(create_path)
    {
        const std::string record = w.get_id() + "|(" + w.get_id().substr(0,4) + ") " + w.get_title() + '\n';

        _write_history(entry, 9, "Node: '" + w.get_id() + "'. ");
        bool update_record = wibble::WibbleIO::write_out_file_append(record, task_node_data_fn);
        if(update_record)
        {
            return _set_extended_task_attribute(entry);
        }
        else
        {
            wc.console_write_error_red("Error updating task data file.");
            return false;
        }
    }
    else
    {
        wc.console_write_error_red("Error creating task data directory.");
        return false;
    }
}

/**
 * @brief Associate/link a Topic entry with task entry.
 * @param entry task entry struct with task to set flag for
 * @param OPTS pkg_options environment settings/configuration struct
 */
bool wibble::WibbleTask::_associate_topic_with_task(task_entry& entry, const pkg_options& OPTS)
{
    WibbleExecutor wib_e{OPTS};
    std::string topic_record = WibbleActions().generate_topic_entry_link_interactive(wc, wib_e);
    if(topic_record == "") { wc.console_write_error_red("No topic selected. Exiting."); throw wibble::wibble_exit(1); }

    const std::string task_topic_data_dir = _get_data_dir_for_task(entry.task_id);
    const std::string task_topic_data_fn  = _get_task_data_topic_fn(entry.task_id);
    //std::cout << "DEBUG: topic_node_data_fn = " << task_topic_data_fn << std::endl;

    bool create_path = wibble::WibbleIO::create_path(task_topic_data_dir);
    if(create_path)
    {
        _write_history(entry, 10, "Topic entry: '" + topic_record.substr(0,23) + "'. ");
        wibble::WibbleIO::write_out_file_append(topic_record, task_topic_data_fn);
        return _set_extended_task_attribute(entry);
    }
    else
    {
        return false;
    }
}

/**
 * @brief Construct/derive the list of task categories by walking directory of task files and scanning filename.
 */
std::set<std::string> wibble::WibbleTask::_build_category_list()
{
    std::set<std::string> categories {"general"};
    try
    {
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{tasks_base_dir + OPEN_TASK_PATH})
        {
            if(dir_entry.is_regular_file())
            {
                try
                {
                    auto cat_start  = std::string(dir_entry.path().filename()).find("_");
                    auto cat_fn_len = std::string(dir_entry.path().filename()).length();
                    categories.insert(std::string(dir_entry.path().filename()).substr(cat_start + 1, cat_fn_len - 5 - cat_start));
                }
                catch(std::exception& ex) { }
            }
        }
    }
    catch(std::exception& ex) {}
    return categories;
}

/**
 * @brief Copy all task project files as a new Topic data entry/directory
 * @param OPTS General configuration data structure
 * @param dd_path Filesystem path to task data directory/project files
 */
bool wibble::WibbleTask::_copy_project_into_topic(const pkg_options& OPTS, std::string& dd_path)
{
    return WibbleActions().copy_project_files_into_topic(wc, OPTS, dd_path);
}

/**
 * @brief Copy all task project files into a an existing Node's data directory
 * @param wib_e General execution object
 * @param dd_path Filesystem path to task data directory/project files
 * @param lf_list Filesystem path to task linked file list, if it exists
 */
bool wibble::WibbleTask::_copy_project_into_node(const pkg_options& OPTS, std::string& dd_path, const std::string& lf_list)
{
    return WibbleActions().copy_project_files_into_node(wc, OPTS, dd_path, lf_list);
}

/**
 * @brief Close (or re-open) a given task group.
 * @param task_group task group struct to modify
 * @param reopen flag controlling whether to close an active group, or reopn a closed group
 */
void wibble::WibbleTask::_close_task_group(const task_group& tg, const bool reopen)
{
    wc.console_write_hz_divider();
    wc.console_write("");
    if(! reopen) { wc.console_write_red(ARW + "Task group '" + tg.group_name + "' will be closed.\n"); }
    else         { wc.console_write_red(ARW + "Task group '" + tg.group_name + "' will be reopend.\n"); }
    wc.console_write("");
    std::string resp;
    if(! reopen) { resp = wc.console_write_prompt("Close task group '" + tg.group_name + "'? (y/n) > ", "", "");  }
    else         { resp = wc.console_write_prompt("Reopen task group '" + tg.group_name + "'? (y/n) > ", "", ""); }
    wc.trim(resp);
    if(resp == "y" || resp == "Y")
    {
        const std::string tg_fn = ! reopen ? _get_open_group_dir(tg.group_id.substr(0,4)) + tg.fn : _get_closed_group_dir(tg.group_id.substr(0,4)) + tg.fn;
        const std::string tg_cd = ! reopen ? _get_closed_group_dir(tg.group_id.substr(0,4))       : _get_open_group_dir(tg.group_id.substr(0,4));

        std::string tg_ar = tg_cd + tg.fn;

        // if archiving the "default" group, need to generate a new filename for it
        if(tg.fn.substr(4, tg.fn.length() - 4) == DF_GROUP_FN_STUB)
        {
            const std::string hash = wibble::WibbleRecord::generate_entry_hash();
            tg_ar = tg_cd + tg.fn.substr(0,4) + hash.substr(5,hash.length() - 4) + "_default.wib";
        }

        bool setup_path = wibble::WibbleIO::create_path(tg_cd);
        if(setup_path)
        {
            bool bkup = wibble::WibbleIO::wcopy_file(tg_fn, cache_dir + "/" + tg.fn);
            if(! bkup) { wc.console_write_error_red("Warning: Error creating backup file."); }
            bool archived = wibble::WibbleIO::wmove_file(tg_fn, tg_ar);
            if(archived)
            {
                if(! reopen) { wc.console_write_success_green("Task group closed/archived."); }
                else         { wc.console_write_success_green("Task group reopened.");        }
            }
            else
            {
                if(! reopen) { wc.console_write_error_red("Error archiving task group."); }
                else         { wc.console_write_error_red("Error reopening task group."); }
            }
        }
        else
        {
            wc.console_write_error_red("Error setting up output directory.");
        }
    }
    else
    {
        wc.console_write_error_red("User cancelled.");
        throw wibble::wibble_exit(1);
    }
}


/**
 * @brief Calculate/display the task group statistics section.
 * @param tot_comp total number of completed tasks
 * @param tot_open total number of open tasks
 */
void wibble::WibbleTask::_display_group_completion_stats(unsigned long tot_com, unsigned long tot_open)
{
    if(tot_open == 0) { wc.console_write("EMPTY: no open tasks."); }
    wc.console_write_hz_divider();
    wc.console_write_yellow_bold("Completed: "); wc.console_write_green(std::to_string(tot_com) + "\n");
    wc.console_write_yellow_bold("Open     : "); wc.console_write_green(std::to_string(tot_open) + "\n");
    double perc_complete = ((double)tot_com / (double)(tot_open + tot_com)) * 100;
    if(tot_com == 0)       { perc_complete = 0.0;    }
    else if(tot_open == 0) { perc_complete = 100.00; }
    wc.console_write_yellow_bold("% done   : ");
    if(ENABLECOLOUR) { std::cout << rang::fgB::red << rang::style::bold << std::setprecision(1) << std::fixed << perc_complete << "\n" << rang::style::reset; }
    else             { std::cout << std::setprecision(1) << std::fixed << perc_complete << "\n"; }
    wc.console_write_hz_heavy_divider();
}

/**
 * @brief Dispatcher for task group command input.
 * @param r_ans string (character) input from user to select operation
 * @param task_group current task group in effect
 * @param bool flag controlling menu display based on whether task group is currently open/active or closed/archived
 */
wibble::te wibble::WibbleTask::_dispatch_task_group_command(const std::string& r_ans, const task_group& selected_group, const bool closed_group)
{

    if(! closed_group) { if(r_ans == "c" || r_ans == "C") { _close_task_group(selected_group);  } }
    else               { if(r_ans == "o" || r_ans == "O") { _reopen_task_group(selected_group); } }

    if(r_ans == "D" || r_ans == "d") { _duplicate_task_group(selected_group);        }
    if(! closed_group) { if(r_ans == "A" || r_ans == "a") { return add_new_task("", selected_group.group_name); } }

    return task_entry();
}

/**
 * @brief Entirely remove a task entry.
 * @param task task entry struct with task to set flag for
 * @param closed_group flag controlling whether task entry is part of a open or closed group
 */
bool wibble::WibbleTask::_delete_task(const task_entry& task, const bool closed_group)
{
    print_task_detailed(task);
    wc.console_write_red("The above task will be permanently removed.\n");
    wc.console_write_hz_divider();
    if(task.extended)
    {
        wc.console_write_red_bold("Task data directory: ");
        wc.console_write(_get_data_dir_for_task(task.task_id) + "\n");
        wc.console_write_red_bold("WARNING. Task contains extended data.\n");
        wc.console_write_red_bold("Task project files, task details, links to nodes, link to topics, and all extended task associated data will be deleted.\n\n");
        wc.console_write_red("Ensure to backup anything of value in above directory first.\n");
    }

    wc.console_write_hz_divider();
    std::string resp = wc.console_write_prompt("\nInput 'delete' to proceed > ", "", "");

    if(resp != "delete") { wc.console_write_error_red("User aborted."); return false; }
    else
    {
        const std::string task_file  = (! task.completed) ? _get_open_taskfile_fn(task.category, task.task_id.substr(0,4)) :
            _get_closed_taskfile_fn(task.category, task.task_id.substr(0,4));

        const std::string group_dir  = (! closed_group)   ? _get_open_group_dir(task.task_id.substr(0,4)) :
            _get_closed_group_dir(task.task_id.substr(0,4));

        bool remove_task_entry = _remove_task_from_taskfile(task.task_id, task_file);
        bool remove_group_entry = _remove_task_from_groupfile(task.task_id, task.completed, _find_groupfile_for_task(task.task_id, group_dir));

        bool remove_data_dir_maybe = true;
        const std::string task_dd  = _get_data_dir_for_task(task.task_id);
        const std::string parent_dd = wibble::WibbleIO::get_parent_dir_path(task_dd);
        const std::string parent_2dd = wibble::WibbleIO::get_parent_dir_path(parent_dd);
        if(std::filesystem::exists(std::filesystem::path(task_dd)))
        {
            remove_data_dir_maybe = std::filesystem::remove_all(std::filesystem::path(task_dd));
            // clean up parent dir if needed/empty - needs to be parent-of-parent due to trailing / on task_dd
            wibble::WibbleIO::remove_dir_if_empty(parent_2dd);
            if(remove_data_dir_maybe) { wc.console_write_success_green("Task data directory successfully removed.");                         }
            else                      { wc.console_write_error_red("Error removing task data directory. Manual deletion may be necessary."); }
        }

        if(remove_task_entry && remove_group_entry) { _write_history(task, 5); return true;                                 }
        else                                        { wc.console_write_success_green("Error removing task."); return false; }
    }
}

/**
 * @brief Build/associate and optionally output the tasks for given task group with a map.
 * @param index shared iterator for incrementing task entries/associating with map
 * @param group_name string/human readable title of task group
 * @param task_num_map map associating index with task entry, mutated by function
 * @param task vector of task entries
 * @param show_completed flag controlling whether to show open or closed tasks
 * @param selector flag controlling whether to display user input selector
 * @param all_results total count of all task entries in group, used for display padding width in input selector
 */
void wibble::WibbleTask::_build_display_tasks_in_group(unsigned long& index, const std::string& group_name,
                                                       std::map<std::size_t, task_entry>& task_num_map,
                                                       std::vector<task_entry>& tasks, const bool show_completed,
                                                       const bool selector, const std::size_t all_results)
{
    if(show_completed)
    {
        wc.console_write_yellow_bold("Listing COMPLETED tasks within group: "); wc.console_write_green_bold(group_name + "\n");
        wc.console_write_hz_divider();
    }
    else
    {
        wc.console_write_yellow_bold("Listing OPEN tasks within group     : "); wc.console_write_green_bold(group_name + "\n");
        wc.console_write_hz_divider();
    }

    // determine padding needed
    unsigned int max_cat_width = 0;
    // determine longest category name for visually nice vertical alignment
    for(auto const& task: tasks) { if(task.category.length() > max_cat_width) { max_cat_width = task.category.length(); } }

    for(auto const& task: tasks)
    {
            ++index;
            task_num_map[index] = task;
            if(selector) { wc.console_write_padded_selector(index, all_results, ""); }
            _print_task_condensed(task, task.completed, max_cat_width);
    }
    if(tasks.size() == 0) { wc.console_write_red("<EMPTY>\n"); }

    wc.console_write_hz_divider();
}


/**
 * @brief Duplicate an entire task group as a new group with entirely fresh task entries (i.e. new IDs).
 * @param tg task_group struct to duplicate
 */
void wibble::WibbleTask::_duplicate_task_group(const task_group& tg)
{
    wc.console_write_yellow_bold("This will replicate the current group of tasks (closed & open) as a new group of tasks.\n");
    wc.console_write_yellow_bold("Note:\n");
    wc.console_write_yellow("1. Category, priority, and task title ");
    wc.console_write_yellow_bold("will ");
    wc.console_write_yellow("be preserved/replicated in duplicates.\n");
    wc.console_write_yellow("2. Due date will ");
    wc.console_write_yellow_bold("not ");
    wc.console_write_yellow("be preserved/replicated in duplicates.\n");
    wc.console_write_yellow("3. Extended task specific data, such as links and attachments will ");
    wc.console_write_yellow_bold("not ");
    wc.console_write_yellow("be preserved.\n");
    wc.console_write_yellow("4. The status of all existing tasks in the original group is ");
    wc.console_write_yellow_bold("unaffected ");
    wc.console_write_yellow("by this cloning operation.\n");
    std::string resp = wc.console_write_prompt("Proceed? (y/n) > ", "", "");
    if(resp == "y" || resp == "Y")
    {
        task_group ng = tg;
        ng.com_tasks = 0;
        ng.group_id = wibble::WibbleRecord::generate_entry_hash();
        ng.fn = wibble::WibbleRecord::generate_timestamp("") + "_" + tg.group_name.substr(0, (tg.group_name.length() - 22));

        // get rid of old/existing datetime
        const int DATE_STUB = 24;
        const std::string group_short_title = tg.group_name.substr(0, (tg.group_name.length() - DATE_STUB));
        std::string group_st_fn = group_short_title;
        wibble::WibbleRecord::sanitise_input(group_st_fn);
        const std::string datetime_suffix = " [ " + wibble::WibbleRecord::generate_date_YMD("") + " ]";
        ng.group_name = group_short_title + datetime_suffix;

        const std::string group_path = tasks_base_dir + OPEN_GROUP_PATH + tg.fn.substr(0,4) + "/";
        const std::string group_file = group_path + "/" + tg.fn;
        wibble::WibbleIO::create_path(group_path);
        std::string dup_group = "";
        dup_group.append(ng.group_id + "|0|" + std::to_string(ng.tot_tasks) + DELIM + ng.group_name + '\n');

        std::pair<wibble::tle, wibble::tle> group_tasks = _get_tasks_for_id_list(tg.nodes);
        std::set<std::string> category_list;
        std::vector<task_entry> history_vector;
        const std::string YYYY = wibble::WibbleRecord::generate_date_YYYY("");

        // now generate a completely fresh set of new ids and build up category list for all tasks
        for(auto& entry: group_tasks.first)
        {
            entry.task_id = wibble::WibbleRecord::generate_entry_hash();
            category_list.insert(entry.category);
            // now add the ids to the new group file string
            dup_group.append("+_" + entry.task_id + DELIM + entry.category + '\n');
            history_vector.push_back(entry);
        }

        for(auto& entry: group_tasks.second)
        {
            entry.task_id = wibble::WibbleRecord::generate_entry_hash();
            category_list.insert(entry.category);
            dup_group.append("+_" + entry.task_id + DELIM + entry.category + '\n');
            history_vector.push_back(entry);
        }

        const std::string gp_fn = _get_open_group_dir(YYYY) + wibble::WibbleRecord::generate_timestamp("") + "_" + group_st_fn + ".wib";

        // persist replacment group file
        bool wrote_group_file = wibble::WibbleIO::write_out_file_overwrite(dup_group, gp_fn);
        if(wrote_group_file) { wc.console_write_success_green("New duplicated task group successfully added."); }

        // category list now contains a de-duplicated sorted list of categories
        for(auto& category: category_list)
        {
            std::string category_tasks;
            // we duplicate both closed and open tasks
            for(auto const& entry: group_tasks.first)
            {
                if(entry.category == category)
                {
                    // all new tasks are "simple"; we don't copy task data. Due date is also not cloned
                    category_tasks.append(entry.task_id + "|S|" + std::to_string(entry.priority) + DELIM
                                          + "___NONE___" + DELIM + entry.category + DELIM + entry.title + '\n');
                }
            }
            for(auto const& entry: group_tasks.second)
            {
                if(entry.category == category)
                {
                    // all new tasks are "simple"; we don't copy task data. Due date is also not cloned
                    category_tasks.append(entry.task_id + "|S|" + std::to_string(entry.priority) + DELIM
                                          + "___NONE___" + DELIM + entry.category + DELIM + entry.title + '\n');
                }
            }
            const std::string cat_fn = _get_open_task_dir(YYYY) + YYYY + "_" + category + ".wib";

            // persist the new task file
            bool wrote_task_file = wibble::WibbleIO::write_out_file_append(category_tasks, cat_fn);
            if(wrote_task_file) { wc.console_write_success_green("Duplicated tasks successfully added to taskfile/category: '" + category + "'"); }
        }

        // update the history log with the new duplicated tasks
        _write_history(history_vector, 3);
    }
    else { wc.console_write_error_red("User cancelled."); }
}


/**
 * @brief Menu/dispatching logic for main task group menu.
 * @param tot_results number of open tasks remaining in group
 * @param closed_group flag controlling whether group is open/closed and hence viable menu options
 * @param show_sort flag controlling whether to show task sorting menu options
 */
std::string wibble::WibbleTask::_present_group_task_command(const unsigned long tot_results, const bool closed_group, const bool show_sort)
{
    if(! closed_group)
    {
        wc.console_write_padded_selector(0, 0, "", "A"); wc.console_write_red("[A]dd"); wc.console_write_yellow(" new task to group.\n");
        if(tot_results == 0)
        { wc.console_write_padded_selector(0, 0, "", "C"); wc.console_write_red("[C]lose"); wc.console_write_yellow(" task group.\n"); }
    }
    else { wc.console_write_padded_selector(0, 0, "", "O"); wc.console_write_red("Re-[o]pen"); wc.console_write_yellow(" task group.\n"); }
    wc.console_write_padded_selector(0, 0, "", "D"); wc.console_write_red("[D]uplicate/clone"); wc.console_write_yellow(" entire task group.\n");
    if(show_sort)
    {
        wc.console_write_padded_selector(0, 0, "", "P"); wc.console_write_yellow("Sort group by"); wc.console_write_red(" [p]riority.\n");
        wc.console_write_padded_selector(0, 0, "", "U"); wc.console_write_yellow("Sort group by"); wc.console_write_red(" d[u]e date.\n");
        wc.console_write_padded_selector(0, 0, "", "Y"); wc.console_write_yellow("Sort group by"); wc.console_write_red(" categor[y].\n");
    }
    wc.console_write_padded_selector(0, 0, "", "Q"); wc.console_write_red("[Q]uit"); wc.console_write_yellow(" to command line.\n");
    std::string r_ans = wc.console_write_prompt("\nSelect option > ", "", "");
    wc.trim(r_ans);
    return r_ans;
}



/**
 * @brief Simplified semantic wrapper for removing task from groupfile (i.e. process of completing a task).
 * @param task_id string with unique task ID/hash
 * @param completed flag controlling whether task is currently open/closed
 * @param groupfile filename/path to group file to operate upon
 */
bool wibble::WibbleTask::_remove_task_from_groupfile(const std::string& task_id, const bool completed, const std::string& groupfile)
{
    return _remove_task_from_taskfile(task_id, groupfile, true, completed);
}

/**
 * @brief Remove task from a given task file or group file.
 * @param task_id string with unique task ID/hash
 * @param taskfile filename/path to task file/group file to operate upon
 * @param group_header_line flag controlling whether adjust first header line, if operating upon a task group file
 * @param completed flag controlling whether task is currently open/closed
 */
bool wibble::WibbleTask::_remove_task_from_taskfile(const std::string& task_id, const std::string& taskfile,
                                                    const bool group_header_line, const bool completed)
{
    try
    {
        std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(taskfile);
        std::string line;
        std::string output = "";
        bool adj_first_line = false;
        if(group_header_line) { adj_first_line = true; }
        while(getline(task_ss, line, '\n'))
        {
            // only for group file - we need to decrement/alter header line
            if(adj_first_line) { output.append(_mark_entry_in_group_header(line, true, completed)); adj_first_line = false; continue; }

            if(line.find(task_id) == std::string::npos) { output.append(line + '\n'); }
            else                                        { continue;                   }
        }
        std::stringstream().swap(task_ss);
        bool updated_file = wibble::WibbleIO::write_out_file_overwrite(output, taskfile + ".new");
        if(updated_file) { return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, taskfile + ".new", taskfile); }
        else             { wc.console_write_error_red("Error creating new output task file. Aborting."); return false;             }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _remove_task_from_taskfile(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Wrapper to re-open/re-activate a task group that has been closed.
 * @param tg task group to operate upon.
 */
void wibble::WibbleTask::_reopen_task_group(const task_group& tg)
{
    _close_task_group(tg, true);
}

/**
 * @brief Present a interactive selector for, or list, available task groups.
 * @param preselect_group String to override interactivity and explicitly select the group with matching ID
 * @param closed_groups flag to specify whether we're dealing with the set of open or closed groups
 */
wibble::tgs wibble::WibbleTask::_select_group_from_list(const std::string& preselect_group, const bool ls_only, const bool closed_groups)
{
    try
    {
        if(closed_groups) { wc.console_write_red(ARW + "Working with archived groups.\n"); }
        std::size_t g_size;

        if(! closed_groups) { g_size = task_groups.size();          }
        else                { g_size = task_groups_archived.size(); }

        if(g_size == 0 && ! ls_only && ! closed_groups)
        {
            int tries = 0;
            const int MAXTRIES = 3;
            std::string resp;
            bool valid_input = false;
            wc.console_write_green(ARW + "No custom task group definitions found. Create a new task group?\n");
            do
            {
                ++tries;
                resp = wc.console_write_prompt("(y/n) > ", "", "");
                if(resp != "y" && resp != "Y" && resp != "n" && resp != "N")
                {
                    wc.console_write_red(ARW + "Please input either 'y' or 'n'.\n");
                }
                else
                {
                    valid_input = true;
                }
            }
            while(valid_input == false && tries < MAXTRIES);
            if(resp == "y" || resp == "Y") { return add_new_group(); }
            wc.console_write_green(ARW + "Assigning to default group.\n");
            return _set_default_group();
        }

        std::size_t index = 0;
        int pad_count = 0;
        const int PADWIDTH = 6;

        unsigned long comb_c_tasks = 0;
        unsigned long comb_t_tasks = 0;

        if     (preselect_group == "" && ls_only && ! closed_groups) { wc.console_write_header("GROUPS "); }
        else if(preselect_group == "" && ls_only && closed_groups)   { wc.console_write_header("ARCHIVED "); }

        std::map<std::size_t, std::string> choice_to_gn;

        std::map<std::string, task_group>& tgroup = closed_groups ? task_groups_archived : task_groups;
        for(auto const& [fn, t]: tgroup)
        {
            // auto selector bypass
            if(preselect_group != "" && t.group_name == preselect_group) { return t; }

            comb_c_tasks += t.com_tasks;
            comb_t_tasks += t.tot_tasks;

            ++index;
            choice_to_gn[index] = t.fn;
            // do not display group listing if we're preselecting
            if(preselect_group == "")
            {
                wc.console_write_padded_selector(index, g_size, "");

                std::string com_tasks = std::to_string(t.com_tasks);
                std::string tot_tasks = std::to_string(t.tot_tasks);

                if(t.com_tasks < t.tot_tasks)
                {
                    //[9999/9999]
                    wc.console_write_red_bold("[");
                    wc.console_write_green_bold(com_tasks);
                    wc.console_write_red_bold("/");
                    wc.console_write_red_bold(tot_tasks);
                    wc.console_write_red_bold("]");
                }
                else
                {
                    wc.console_write_green_bold("[" + com_tasks + "/" + tot_tasks + "]");
                }

                // keep things pretty with vertical alignment up to 999 tasks (i.e. potentially [999/999] )
                pad_count = PADWIDTH - (com_tasks.length() + tot_tasks.length());
                if(pad_count < 0) { pad_count = 0; } // only if they add an insane number of tasks, i.e. > 999
                std::string padding = "";
                padding.insert(0, pad_count, ' ');
                wc.console_write(padding + " ┃ " + t.group_name);

                //std::cout << "Task group id       : " << t.group_id << std::endl;
                //std::cout << "Task group name     : " << t.group_name << std::endl;
                //std::cout << "Task group com tasks: " << t.com_tasks << std::endl;
                //std::cout << "Task group tot tasks: " << t.tot_tasks << std::endl;
                //std::cout << "Node ids            : ";
                //for(auto const& id: t.node_ids) { std::cout << id << " "; }
                //std::cout << std::endl;
            }
        }

        if(! ls_only)
        {
            std::string resp = "";
            if(! closed_groups)
            {
                wc.console_write_padded_selector(0, g_size, "", "a");
                wc.console_write_yellow("Create new task group.\n");
            }

            if(closed_groups) { if(tgroup.size() == 0) { wc.console_write_error_red("No closed groups present. Exiting."); throw wibble::wibble_exit(0); } }
            resp = wc.console_write_prompt("\nSelect task group > ", "", "");
            if(! closed_groups) { if(resp == "a" || resp == "A") { return add_new_group(); } }
            unsigned long choice = 0;
            try { choice = std::stol(resp); } catch(std::exception& ex) { choice = 0; }
            if(choice == 0) { wc.console_write_error_red("Invalid group selection. Aborting."); throw wibble::wibble_exit(1); }
            else
            {
                if(! closed_groups) { return task_groups[choice_to_gn[choice]];          }
                else                { return task_groups_archived[choice_to_gn[choice]]; }
            }
        }
        else
        {
            wc.console_write_hz_divider();
            wc.console_write_yellow("Total completed tasks: "); wc.console_write_green(std::to_string(comb_c_tasks) + "\n");
            wc.console_write_yellow("Total tasks          : "); wc.console_write_green(std::to_string(comb_t_tasks) + "\n");
            wc.console_write_yellow("Total pending tasks  : "); wc.console_write_red(std::to_string(comb_t_tasks - comb_c_tasks) + "\n");
            tot_completed_tasks = comb_c_tasks;
        }

        // set to default if we reach here
        return _set_default_group();
    }
    catch(std::exception& ex)
    {
        throw wibble::wibble_exit(1);
    }
}


/**
 * @brief Serialize/flatten a task_entry item to a field separated single-line string.
 * @param entry task entry struct to serialize
 */
std::string wibble::WibbleTask::serialize_task_entry(const task_entry& entry)
{
    const std::string extended = entry.extended ? "|E|" : "|S|";
    return entry.task_id + extended + std::to_string(entry.priority) + "|"
         + entry.due_date + "|" + entry.category + "|" + entry.title + '\n';
}

/**
 * @brief Update/modify an existing task entry.
 * @param entry task entry struct to serialize
 */
bool wibble::WibbleTask::_update_existing_task_entry(const task_entry& entry)
{
    const std::string task_fn = ! entry.completed ? _get_open_taskfile_fn(entry.category, entry.task_id.substr(0,4)) :
                                                    _get_closed_taskfile_fn(entry.category, entry.task_id.substr(0,4));
    try
    {
        std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(task_fn);
        std::string line;
        bool updated_record = false;
        std::ofstream new_task_file;
        new_task_file.open(task_fn + ".new", std::ios::out | std::ios::trunc);

        while(std::getline(task_ss, line, '\n'))
        {
            if(updated_record == false)
            {
                if(line.find(entry.task_id) != std::string::npos)
                {
                    new_task_file << serialize_task_entry(entry);
                    updated_record = true;
                }
                else
                {
                    new_task_file << line + '\n';
                }
            }
            else
            { new_task_file << line + '\n'; }
        }
        new_task_file.close();
        std::stringstream().swap(task_ss);

        if(updated_record)
        {
            return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, task_fn + ".new", task_fn);
        }
        else
        {
            // no record changed, so discard new file
            std::filesystem::remove(std::filesystem::path(task_fn + ".new"));
            return false;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in update_existing_task_entry(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Update the associated group file when a task has been closed/completed [in a given task file].
 * @param task_id string with unique hash of task entry
 */
bool wibble::WibbleTask::_update_task_to_closed_in_group_file(const std::string& task_id)
{
    try
    {

        bool task_marked = false;
        bool file_found = false;
        std::string header_line;
        std::string line;
        std::string updated_gf = "";
        // 1. Determine the group file the task exists in, scan through all of the group files
        const std::string task_group_dir = _get_open_group_dir(task_id.substr(0,4));
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{task_group_dir})
        {
            if(dir_entry.is_regular_file() && task_marked == false)
            {
                bool first_line = true;
                //std::cerr << "Inspecting: " << dir_entry.path() << '\n';
                std::stringstream group_ss = wibble::WibbleIO::read_file_into_stringstream(dir_entry.path());
                while(std::getline(group_ss, line, '\n'))
                {
                    // save first line for later
                    if(first_line) { header_line = line; first_line = false; continue; }
                    if(line.find(task_id) != std::string::npos)
                    {
                        // update flag to indicate we've found the line/correct file match
                        file_found = true;
                        // replace the leading '+' character with '-'
                        updated_gf.append('-' + line.substr(1, (line.length() - 1)) + '\n');
                    }
                    else { updated_gf.append(line + '\n'); }
                }
                std::stringstream().swap(group_ss);

                // 2. Once we've found the group file, update it
                if(file_found)
                {
                    std::string new_header = _mark_entry_in_group_header(header_line);
                    updated_gf = new_header + updated_gf;

                    const std::string orig_group_fn = std::string(dir_entry.path());
                    const std::string rep_group_fn = orig_group_fn + ".new";
                    std::ofstream(rep_group_fn).write(updated_gf.c_str(), updated_gf.length());

                    task_marked = WibbleUtility::create_backup_and_overwrite_file(cache_dir, rep_group_fn, orig_group_fn);
                }
                else { updated_gf.clear(); }
            }
        }
        return task_marked;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _update_task_to_closed_in_group_file(): " << ex.what() << std::endl;
        return false;
    }

}

/**
 * @brief Cleanse input and add a new task entry.
 * @param title title/todo line that is the new task
 * @param category the category new task will be assigned to/tagged with
 * @param due_date optional due date for time sensitive tasks
 * @param priority integer value representing importance of the task, 0 = min, 99 = max
 * @param group the task group the task will be assigned to
 */
wibble::te wibble::WibbleTask::_validate_and_add_task(const std::string& title, std::string& category,
                                                const std::string& due_date, const int priority,
                                                const task_group& group)
{
    try
    {
        // downcase and remove any whitespace or "/" or "\" or "|" symbol from category
        wibble::WibbleRecord::str_to_lowercase(category);
        wibble::WibbleRecord::sanitise_input(category);

        // 2023-01-01: YYYY-MM-DD ISO date format
        // ___NONE___: placeholder for no due date
        const std::string YYYY = wibble::WibbleRecord::generate_date_YYYY("");
        const std::string task_id = wibble::WibbleRecord::generate_entry_hash();
        const std::string valid_date = _get_valid_datetime_str_or_NONE(due_date);
        // all tasks start off as "[S]imple" rather than "e[X]tended"
        const std::string t_entry = task_id + "|S|" + std::to_string(priority) + "|" + valid_date + "|" + category + "|" + title + '\n';
        const std::string task_dir = _get_open_task_dir(YYYY);
        const std::string group_dir = _get_open_group_dir(YYYY);
        const std::string task_fn = _get_open_taskfile_fn(category, YYYY);

        task_entry task;
        task.category = category;
        task.due_date = valid_date;
        task.fn = task_fn;
        task.extended = false;
        task.priority = priority;
        task.task_id = task_id;
        task.title = title;

        bool setup_task_dir  = wibble::WibbleIO::create_path(task_dir);
        bool setup_group_dir = wibble::WibbleIO::create_path(group_dir);
        bool wrote_task_file = false;
        if(setup_task_dir && setup_group_dir) { wrote_task_file = wibble::WibbleIO::write_out_file_append(t_entry, task_fn); }

        if(wrote_task_file)
        {
            _write_history(task_id, category, priority, title);
            // add to group
            bool add_to_group = _add_task_to_group(group, task_id, category);
            if(add_to_group) { wc.console_write_success_green("Task successfully added to group '" + group.group_name + "'"); }
            else             { wc.console_write_error_red("Error adding task to group. Assigning to default.");               }

            return task;
        }
        else
        {
            return task_entry();
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _valid_and_add_task(): " << ex.what() << std::endl;
        return task_entry();
    }
}

/**
 * @brief Update a given task in its specific task file.
 * @param task struct containing the task entry/data
 * @param taskfile string with filename/path to taskfile to update
 */
bool wibble::WibbleTask::_update_task_in_taskfile(const task_entry& task, const std::string& taskfile)
{
    try
    {
        std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(taskfile);
        std::string line;
        std::string output = "";
        bool do_write = false;
        while(getline(task_ss, line, '\n'))
        {
            if(line.find(task.task_id) == std::string::npos) { output.append(line + '\n'); }
            else
            {
                output.append(serialize_task_entry(task));
                do_write = true;
            }
        }

        std::stringstream().swap(task_ss);

        if(do_write)
        {
            const std::string new_tf_fn = taskfile + ".new";
            bool new_tf = wibble::WibbleIO::write_out_file_overwrite(output, new_tf_fn);
            if(new_tf)
            {
                bool result = wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, new_tf_fn, taskfile);
                if(result) { _write_history(task, 4); }
                return result;
            }
            else { wc.console_write_error_red("Error writing out new taskfile. Aborting."); return false;          }
        }
        else
        {
            wc.console_write_green(ARW + "Nothing to update. Skipping write out.\n");
            return true;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _update_task_in_taskfile(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Propagate any changes in task properties/data to task files/persist to disk.
 * @param task struct containing the task entry/data
 * @param closed_group flag controlling whether we are dealing with a task in a closed group
 * @param existing_category optional string storing old category in case of categoery alteration
 */
bool wibble::WibbleTask::_update_task_details(const task_entry& task, const bool closed_group, const std::string& existing_category)
{
    // has the category changed? if existing_category string is present, task.category contains the NEW category to assign to
    const std::string category = (existing_category == "") ? task.category : existing_category;

    const std::string task_file  = (! task.completed) ? _get_open_taskfile_fn(category, task.task_id.substr(0,4)) :
                                                        _get_closed_taskfile_fn(category, task.task_id.substr(0,4));

    const std::string group_dir  = (! closed_group)   ? _get_open_group_dir(task.task_id.substr(0,4)) :
                                                        _get_closed_group_dir(task.task_id.substr(0,4));
    if(existing_category != "")
    {
        bool remove_task_entry = _remove_task_from_taskfile(task.task_id, task_file);
        if(remove_task_entry)
        {
            const std::string new_entry = serialize_task_entry(task);
            const std::string new_tf = (! task.completed) ? _get_open_taskfile_fn(task.category, task.task_id.substr(0,4)) :
                                                            _get_closed_taskfile_fn(task.category, task.task_id.substr(0,4));

            // ensure to use append since the re-assigned category (file) may well already exist
            bool add_new_task = wibble::WibbleIO::write_out_file_append(new_entry, new_tf);
            if(add_new_task)
            {
                _write_history(task, 4);
                return _update_task_in_groupfile(task, _find_groupfile_for_task(task.task_id, group_dir));
            }
            else
            {
                wc.console_write_error_red("Error assigning task to new task category.");
                return false;
            }
        }
        else
        {
            wc.console_write_error_red("Error removing task from old task category.");
            return false;
        }
    }
    else
    {
        bool update_task_file  = _update_task_in_taskfile(task, task_file);
        bool update_group_file = _update_task_in_groupfile(task, _find_groupfile_for_task(task.task_id, group_dir));

        return (update_task_file && update_group_file);
    }
}

/**
 * @brief Propagate any changes in task properties/data to task group files/persist to disk.
 * @param task struct containing the task entry/data
 * @param groupfile string with filename to group file to update
 */
bool wibble::WibbleTask::_update_task_in_groupfile(const task_entry& task, const std::string& groupfile)
{
    try
    {
        std::stringstream group_ss = wibble::WibbleIO::read_file_into_stringstream(groupfile);
        std::string line;
        std::string output = "";
        bool do_write = false;
        while(getline(group_ss, line, '\n'))
        {
            if(line.find(task.task_id) == std::string::npos) { output.append(line + '\n'); }
            else
            {
                std::string record = (! task.completed) ? "+_" : "-_";
                record.append(task.task_id + DELIM + task.category + '\n');
                output.append(record);
                do_write = true;
            }
        }

        std::stringstream().swap(group_ss);

        if(do_write)
        {
            const std::string new_gf_fn = groupfile + ".new";
            bool new_gf = wibble::WibbleIO::write_out_file_overwrite(output, new_gf_fn);
            if(new_gf) { return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, new_gf_fn, groupfile); }
            else       { wc.console_write_error_red("Error writing new group file. Aborting."); return false;             }
        }
        else
        {
            wc.console_write_green(ARW + "Nothing to update. Skipping write out.\n");
            return true;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _update_task_in_groupfile(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Write to the history task file (overload using task_id string)
 * @param category the category of the task
 * @param priority the priority of the task
 * @param title the title of the task
 */
bool wibble::WibbleTask::_write_history(const std::string& id, const std::string& category, const int& priority, const std::string& title)
{
    task_entry te;
    te.task_id = id;
    te.category = category;
    te.priority = priority;
    te.title = title;

    return _write_history(te, 1);
}

/**
 * @brief Write to the history task file (overload for changing a single entry)
 * @param entry task_entry struct with the task to write out
 * @param op history write option flag
 * @param pre_text any optional text to prepend to entry
 */
bool wibble::WibbleTask::_write_history(const task_entry& entry, int op, const std::string& pre_text)
{
    std::vector<task_entry> wrapper;
    wrapper.push_back(entry);
    return _write_history(wrapper, op, pre_text);
}

/**
 * @brief Write to the history task file en masse.
 * @param tasklist vector of task_entry structs with the tasks to write out
 * @param op history write option flag
 * @param pre_text any optional text to prepend to entries
 */
bool wibble::WibbleTask::_write_history(const std::vector<task_entry>& tasklist, int op, const std::string& pre_text)
{
    std::string write_out = "";
    for(auto const& entry: tasklist)
    {
        std::string msg = "- [" + wibble::WibbleRecord::generate_date_YMD("") + "] - ";

        switch(op)
        {
        case 1: // add new
            msg.append("[   ADDED ]: ");
            break;
        case 2: // mark done
            msg.append("[    DONE ]: ");
            break;
        case 3: // duplicated
            msg.append("[  CLONED ]: ");
            break;
        case 4: // update
            msg.append("[ UPDATED ]: ");
            break;
        case 5: // delete
            msg.append("[ DELETED ]: ");
            break;
        case 6: // Linked file
            msg.append("[ LINK FL ]: ");
            break;
        case 7: // Copied file
            msg.append("[ COPY FL ]: ");
            break;
        case 8: // Moved file
            msg.append("[ MOVE FL ]: ");
            break;
        case 9: // Node associated
            msg.append("[ NASSOCI ]: ");
            break;
        case 10: // Topic associated
            msg.append("[ TASSOCI ]: ");
            break;
        case 11: // Jotfile file
            msg.append("[ JASSOCI ]: ");
            break;
        case 12: // Project file action
            msg.append("[ PROJ FL ]: ");
            break;
        case 13: // Migrate/copy to Node
            msg.append("[  C NODE ]: ");
            break;
        case 14: // Migrate/copy to Node
            msg.append("[ C TOPIC ]: ");
            break;
        default: // add new
            std::cerr << "Unknown history action. Doing nothing." << std::endl;
            return false;
        }

        msg.append(pre_text);
        msg.append("Title: '" + entry.title + "'. Id: '" + entry.task_id + "'. Priority: '" + std::to_string(entry.priority) + "'. Category: '" + entry.category + "'.\n");
        write_out.append(msg);
    }
    bool writ_hist = wibble::WibbleIO::write_out_file_append(write_out, tasks_base_dir + "/" + TASK_HISTORY);
    if(writ_hist) { wc.console_write_success_green("History file successfully updated."); }
    else          { wc.console_write_error_red("Error updating history file.");           }
    return writ_hist;
}

/**
 * @brief Add a new task group interactively.
 */
wibble::tgs wibble::WibbleTask::add_new_group(const std::string& auto_group_name)
{
    try
    {
        std::string gn;

        if(auto_group_name == "")
        {
            int tries = 0;
            const int MAXTRIES = 3;
            do
            {
                ++tries;
                gn = wc.console_write_prompt("Task group name               *: ", "", ""); wc.trim(gn);
                if(gn == "") { wc.console_write_error_red("Group name cannot be blank."); }
            }
            while(gn == "" && tries < MAXTRIES);
        }
        else { gn = auto_group_name; }

        if(gn == "")
        {
            wc.console_write_error_red("Maximum attempts reached. Setting to default group.");
            return _set_default_group();
        }
        std::string title = gn;
        wibble::WibbleRecord::remove_pipe(title);
        title.append(" [ " + wibble::WibbleRecord::generate_date_YMD("") + " ]");

        //wibble::WibbleRecord::remove_whitespace(gn);
        //wibble::WibbleRecord::remove_slash(gn);
        //wibble::WibbleRecord::remove_pipe(gn);
        wibble::WibbleRecord::sanitise_input(gn);

        task_group tg;
        tg.fn         = wibble::WibbleRecord::generate_timestamp("") + "_" + gn + ".wib";
        tg.group_name = title;
        tg.group_id   = wibble::WibbleRecord::generate_entry_hash();
        tg.com_tasks  = 0;
        tg.tot_tasks  = 0;

        const std::string group_path = tasks_base_dir + OPEN_GROUP_PATH + tg.fn.substr(0,4) + "/";
        const std::string group_file = group_path + "/" + tg.fn;
        wibble::WibbleIO::create_path(group_path);
        std::ofstream new_group_file;
        new_group_file.open(group_file, std::ios::out | std::ios::trunc);
        new_group_file << tg.group_id + "|0|0|" + tg.group_name + '\n';
        new_group_file.close();

        // reparse everything so state is now fully up to date
        open_tasks.clear();
        _gather_tasks(true);

        return tg;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in add_new_group(): '" << ex.what() << "', setting to default group" << std::endl;
        return _set_default_group();
    }

}

/**
 * @brief Add a new task entry interactively or optionally with pre-filled title/group
 * @param task_title optional string with task title/todo line
 * @param prefill_group optional string with group ID to add to
 */
wibble::te wibble::WibbleTask::add_new_task(const std::string& task_title, const std::string& prefill_group)
{
    // task file format:
    // dir/file:  open_tasks/2024/2024_general.wib
    //20240204192936_4fe19068|S|5|___NONE___|general|Testing again
    //20240204192953_6007f8e3|S|8|2024-02-02|general|Test entry
    //20240204193459_d7b39a62|S|5|2030-30-30|general|Another test entry

    const int MAXTRIES = 3;
    int tries = 0;
    std::string title, category;
    task_group group;

    if(task_title == "")
    {
        do { ++tries; title = wc.console_write_prompt("Input title/summary of task   *: ", "", ""); wc.trim(title); }
        while(title == "" && tries < MAXTRIES);
    }
    else { title = task_title; }

    if(title == "") { wc.console_write_error_red("Task title cannot be blank."); throw wibble::wibble_exit(1); }
    tries = 0;
    std::cout << '\n';
    do
    {  ++tries;
        category = _present_category_list();
    }
    while(category == "" && tries < MAXTRIES);

    if(category == "") { wc.console_write_error_red("Category cannot be blank."); throw wibble::wibble_exit(1); }

    std::cout << '\n';
    std::string due_date  = wc.console_write_prompt("Input due date for task (YYYY-MM-DD)\n(leave blank for no due date)  : ", "", "");
    std::string priority  = wc.console_write_prompt("Input priority [1-99]\n(1 = min, 99 = max)           *: ", "", "50");

    tries = 0;
    bool valid_group = false;
    if(prefill_group != "")
    {
        group = _select_group_from_list(prefill_group, false, false);
        valid_group = true;
    }
    else
    {
        do
        {
            ++tries;
            std::string add_group = wc.console_write_prompt("Add into custom group? (y/n)  *: ", "", "n");
            if(add_group == "y" || add_group == "Y")      { group = _select_group_from_list(); valid_group = true; }
            else if(add_group == "n" || add_group == "N") { group = _set_default_group(); valid_group = true;      }
            else { wc.console_write_red(ARW + "Please input either 'y' or 'n'\n"); }
        }
        while(valid_group == false && tries < MAXTRIES);
    }
    if(! valid_group)            { wc.console_write_error_red("Invalid group input. Aborting."); throw wibble::wibble_exit(1); }
    if(group.fn == "___NULL___") { wc.console_write_error_red("Invalid group input. Aborting."); throw wibble::wibble_exit(1); }

    wc.trim(due_date);
    wc.trim(priority);

    int p_int;
    try
    {
        p_int = std::stoi(priority);

        if(p_int > 99 || p_int < 1) { throw std::invalid_argument("Outside range"); }
    }
    catch(std::invalid_argument& ex)
    {
        wc.console_write_error_red("Warning: Invalid input for priority: '" + priority + "'. Defaulting to 50.\n");
        p_int = 50;
    }
    catch(std::out_of_range& ex)
    {
        wc.console_write_red("Warning: Invalid input for priority: '" + priority + "'. Defaulting to 50.\n");
        p_int = 50;
    }

    wc.console_write_hz_divider();
    task_entry task = _validate_and_add_task(title, category, due_date, p_int, group);
    bool add_result = task.task_id != "___NULL___" ? true : false;

    if(add_result) { wc.console_write_success_green("New task added successfully."); }
    else           { wc.console_write_error_red("Error adding task.");               }

    return task;
}

/**
 * @brief Batch/automatically add data to a task item based on ID
 * @param task_id unique task-specific identifier
 * @param data_file the file or directory to copy or move
 * @param data_action whether to copy or move the file
 */
bool wibble::WibbleTask::batch_add_task_data(const std::string task_id, const std::string& data_file, const std::string& data_action)
{
    bool found_item = false;
    WibbleTask::task_entry ti;
    for(auto const& [group_id, o_entries]: open_tasks)
    {
        if(found_item) { break; }
        for(auto const& item: o_entries)
        {
            if(item.task_id == task_id)
            {
                ti = item;
                found_item = true;
                break;
            }
        }
    }

    if(! found_item) // try closed tasks as fallback if still not found
    {
        for(auto const& [group_id, c_entries]: closed_tasks)
        {
            if(found_item) { break; }
            for(auto const& item: c_entries)
            {
                if(item.task_id == task_id)
                {
                    ti = item;
                    found_item = true;
                    break;
                }
            }

        }
    }


    if(! found_item) { wc.console_write_error_red("No match for task ID: '" + task_id + "'."); return false; }
    else
    {
        // found the task ID, so do the data operation
        return _associate_linked_file_with_task(ti, data_file, data_action);
    }
}

/**
 * @brief Batch/automatically add a task item to an existing group by name
 * @param title task item title/brief description/todo
 * @param category task item category
 * @param due_date task item due_date
 * @param priority integer value with priority
 * @param t_group_name string identifying the task group
 */
std::string wibble::WibbleTask::batch_add_task_item(const std::string& title, std::string& category, const std::string& due_date,
                                             const int priority, const std::string& t_group_name)
{
    task_group group = _get_task_group_for_name(t_group_name);
    if(group.group_id == "___NULL___") { wc.console_write("", false); dump_group_names(); wc.console_write("", false); return "INVALID_GROUP"; }

    const int p_priority = (priority == NULL_INT_VAL) ? 50 : priority;
    task_entry task = _validate_and_add_task(title, category, due_date, p_priority, group);
    return task.task_id != "___NULL___" ? task.task_id : "INVALID_TASK";
}

/**
 * @brief Helper function to determine a given task group based on its name
 * @param t_group_name string identifying the task group
 */
wibble::tgs wibble::WibbleTask::_get_task_group_for_name(const std::string& t_group_name)
{
    task_group def_group;
    for(auto const& [fn, t_group]: task_groups) { if(t_group.group_name == t_group_name) { def_group = t_group; } }
    return def_group;
}

/**
 * @brief Filter tasks by a vector of IDs; used for constructing a filtered task_entry list
 * @param ids vector of std::pair<s, s> with first = task id (plus completion status), second = task category
 */
std::pair<wibble::tle, wibble::tle> wibble::WibbleTask::_get_tasks_for_id_list(const std::vector<std::pair<std::string, std::string>>& ids)
{
    std::pair<tle,tle> full_task_list;
    std::map<std::string, std::string> open_task_items;
    std::map<std::string, std::string> closed_task_items;
    // 1. pack the maps, organised by category (i.e. file) - we only want to open/read a given task file once
    for(auto const& item: ids)
    {
        // open tasks start with '+' in task group file
        if(item.first.at(0) == '+')
        {
            // map unique id to category, use +_ or -_ prefix to determine open/closed status
            open_task_items[item.first.substr(2, item.first.length() - 2)] = item.second;
        }
        else
        {
            closed_task_items[item.first.substr(2, item.first.length() - 2)] = item.second;
        }
    }

    // 2. Now construct sets in order to build up a filename list
    // a) open tasks
    std::set<std::string> open_categories;
    for(auto const& [id, category]: open_task_items)
    {
        // prefix with first 4 digits from ID whichh captures correct year indexing
        // handles case where task groups cross year boundaries, hence use different
        // task files to prevent them getting too large and keep them nicely organised
        open_categories.insert(id.substr(0,4) + "_" + category);
    }

    // b) closed tasks
    std::set<std::string> closed_categories;
    for(auto const& [id, category]: closed_task_items)
    {
        closed_categories.insert(id.substr(0,4) + "_" + category);
    }

    // 3. build up the filename list
    // file format: task_lists/<year>/<year>_<category>.wib
    // a) open tasks
    std::map<std::string, std::string> open_filename_tuples;
    for(auto const& open_category: open_categories)
    {
        const std::string fn = tasks_base_dir + OPEN_TASK_PATH + open_category.substr(0,4) + "/" + open_category + ".wib";
        open_filename_tuples[open_category] = fn;
    }
    // b) closed tasks
    std::map<std::string, std::string> closed_filename_tuples;
    for(auto const& closed_category: closed_categories)
    {
        const std::string fn = tasks_base_dir + CLOSED_TASK_PATH + closed_category.substr(0,4) + "/" + closed_category + ".wib";
        closed_filename_tuples[closed_category] = fn;
    }

    // 4. Now we can fetch all the tasks; we want to do it by file, since we only want to read file in once
    // a) open tasks
    tle open_tasks;
    for(auto const& category: open_categories)
    {
        std::set<std::string> id_list;
        for(auto const& [id, cat_name]: open_task_items)
        {
            if(category == cat_name) { id_list.insert(id); }
        }
        tle tasks_from_file = _parse_task_file_with_filter(open_filename_tuples[category], id_list, false);
        open_tasks.insert(open_tasks.end(), tasks_from_file.begin(), tasks_from_file.end());
    }
    // b) closed tasks
    tle closed_tasks;
    for(auto const& category: closed_categories)
    {
        std::set<std::string> id_list;
        for(auto const& [id, cat_name]: closed_task_items)
        {
            if(category == cat_name) { id_list.insert(id); }
        }

        tle tasks_from_file = _parse_task_file_with_filter(closed_filename_tuples[category], id_list, true);
        closed_tasks.insert(closed_tasks.end(), tasks_from_file.begin(), tasks_from_file.end());
    }

    full_task_list.first  = open_tasks;
    full_task_list.second = closed_tasks;
    return full_task_list;
}

/**
 * @brief List and optionally select from all completed tasks.
 * @param selector flag to dictate whether we are presenting a user input selector
 */
wibble::te wibble::WibbleTask::_list_completed_tasks(const bool selector)
{
    if(selector) { return _list_open_tasks("", "", true, true);  }
    else         { return _list_open_tasks("", "", false, true); }
}

/**
 * @brief List/build task map and optionally select from all open or closed tasks.
 * @param cat_fltr optional string filter to substring match on category names
 * @param title_fltr optional string filter to substring match on task entry titles/todos
 * @param selector flag to dictate whether we are presenting a user input selector
 * @param c_tasks flag to dictate whether we are scanning open or closed tasks
 */
wibble::te wibble::WibbleTask::_list_open_tasks(const std::string& cat_fltr, const std::string& title_fltr, const bool selector, const bool c_tasks)
{
    unsigned long index = 0;
    std::map<std::size_t, task_entry> task_num_map;
    unsigned long all_tasks = 0;

    std::map<std::string, std::vector<task_entry>>& task_map = (! c_tasks) ? open_tasks : closed_tasks;

    if(title_fltr != "") { wc.console_write_green(ARW + "Active title filter substring match in effect: "); wc.console_write_yellow(title_fltr + "\n");  }
    if(cat_fltr != "")   { wc.console_write_green(ARW + "Active category filter substring match in effect: "); wc.console_write_yellow(cat_fltr + "\n"); }

    // Determine total open tasks in advance with filtering
    for(auto const& [category, list]: task_map)
    {
        if(cat_fltr == "" && title_fltr == "")      // 1. No filters
        { all_tasks += list.size();                                                                                 }
        else if(cat_fltr != "" && title_fltr != "") // 2. Both filters
        {
            if(category.find(cat_fltr) != std::string::npos)
            { for(auto const& task: list) { if(task.title.find(title_fltr) != std::string::npos) { ++all_tasks; } } }
        }
        else if(cat_fltr == "" && title_fltr != "") // 3. Title filter without category filter
        { for(auto const& task: list) { if(task.title.find(title_fltr) != std::string::npos) { ++all_tasks; } }     }
        else if(cat_fltr != "" && title_fltr == "") // 4. Category filter without title filter
        { if(category.find(cat_fltr) != std::string::npos) { all_tasks += list.size(); }                            }
    }

    for(auto const& [category, list]: task_map)
    {
        if(cat_fltr == "" || category.find(cat_fltr) != std::string::npos)
        {
            wc.console_write_hz_heavy_divider();
            wc.console_write_yellow_bold("Listing tasks in category: "); wc.console_write_green_bold(category + "\n");
            wc.console_write_hz_heavy_divider();
            for(auto const& task: list)
            {
                if(title_fltr == "" || task.title.find(title_fltr) != std::string::npos)
                {
                    ++index;
                    task_num_map[index] = task;
                    if(selector) { wc.console_write_padded_selector(index, all_tasks, ""); }
                    (! c_tasks) ?  _print_task_condensed(task, false, 0, false) : _print_task_condensed(task, true, 0, false);
                }
            }
        }
    }

    // summary for listing completed tasks
    if(c_tasks)
    {
        wc.console_write_hz_heavy_divider();
        wc.console_write_success_green(std::to_string(all_tasks) + " completed tasks.");
        wc.console_write_hz_heavy_divider();
    }

    if(selector)
    {
        unsigned long ans = wc.console_present_record_selector(all_tasks);
        if(ans != 0) { return task_num_map[ans]; }
        else         { return task_entry();      }
    }
    else
    {
        return task_entry();
    }
}

/**
 * @brief List any attached 'project' files associated with a task entry, present project menu
 * @param task_id string with unique task ID/hash
 */
std::string wibble::WibbleTask::_list_project_files(const std::string& task_id)
{
    unsigned int files = 0;
    unsigned int maxlength = 0;
    int total = 0;
    std::map<int, std::string> proj_list;
    proj_list[files] = "ADD";
    try
    {
        const std::string proj_index = _get_proj_index_for_task(task_id);
        if(std::filesystem::exists(std::filesystem::path(proj_index)))
        {
            std::stringstream proj_ss = wibble::WibbleIO::read_file_into_stringstream(proj_index);
            auto pos = proj_ss.tellg();

            std::string line;
            std::size_t id_sep;
            std::size_t ft_sep;
            std::string file_id;
            std::string file_ex;
            std::string file_ti;

            // accumulate total files/lines, and calculate spacing
            while(getline(proj_ss, line, '\n'))
            {
                ++total;
                id_sep  = line.find(DELIM);
                ft_sep  = line.find(DELIM, id_sep + 1);
                file_id = line.substr(0, id_sep);
                file_ex = line.substr(id_sep + 1, ft_sep - 1 - id_sep);
                if(file_ex.length() > maxlength) { maxlength = file_ex.length(); }
            }

            // go back to beginning of stream
            proj_ss.clear();
            proj_ss.seekg(pos);

            while(getline(proj_ss, line, '\n'))
            {
                ++files;
                id_sep  = line.find(DELIM);
                ft_sep  = line.find(DELIM, id_sep + 1);
                file_id = line.substr(0, id_sep);
                file_ex = line.substr(id_sep + 1, ft_sep - 1 - id_sep);
                file_ti = line.substr(ft_sep + 1, line.length() - 1 - ft_sep);
                //std::cout << "DEBUG: got " << line << '\n' << file_id << " / " << file_ex << " / " << file_ti << std::endl;
                std::string padding = "";
                int padcount = maxlength - file_ex.length();
                if(padcount > 0) { padding.insert(0, padcount, ' '); }
                wc.console_write_padded_selector(files, total, ""); wc.console_write_yellow_bold(file_ex + padding);
                wc.console_write(" ┃ " + file_ti);
                proj_list[files] = file_id;
            }
        }
        wc.console_write_padded_selector(0, files, "", "a");
        wc.console_write_yellow_bold("[   Add a new project file       ]\n");
        wc.console_write_padded_selector(0, files, "", "c");
        wc.console_write_yellow_bold("[ cd into project directory      ]\n");
        wc.console_write_padded_selector(0, files, "", "o");
        wc.console_write_yellow_bold("[   Open project directory       ]\n");
        wc.console_write_padded_selector(0, files, "", "p");
        wc.console_write_yellow_bold("[  Output project directory      ]\n");
        wc.console_write_padded_selector(0, files, "", "n");
        wc.console_write_yellow_bold("[  Copy project files into Node  ]\n");
        wc.console_write_padded_selector(0, files, "", "t");
        wc.console_write_yellow_bold("[  Copy project files into Topic ]\n");
        wc.console_write_hz_divider();

        std::string resp = wc.console_write_prompt("Select option > ", "", "");
        wc.trim(resp);
        if(resp == "a" || resp == "A")      { return proj_list[0];                         }
        else if(resp == "c" || resp == "C") { proj_list[0] = "CD";    return proj_list[0]; }
        else if(resp == "o" || resp == "O") { proj_list[0] = "OPEN";  return proj_list[0]; }
        else if(resp == "n" || resp == "N") { proj_list[0] = "NODE";  return proj_list[0]; }
        else if(resp == "p" || resp == "P") { proj_list[0] = "DIR";   return proj_list[0]; }
        else if(resp == "t" || resp == "T") { proj_list[0] = "TOPIC"; return proj_list[0]; }
        else
        {
            unsigned int choice = 0;
            try { choice = std::stoi(resp); } catch(std::exception& ex) { }
            if(choice <= 0 || choice > files) { return "";                }
            else                              { return proj_list[choice]; }
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _list_project_files(): " << ex.what() << std::endl;
        return "";
    }

}

/**
 * @brief Display the tasks associated with a given task group, using any specified filtering/sorting critera.
 * @param selected_group given task group to operate upon
 * @param closed_group flag controlling whether we are dealing with a task in a closed group
 */
wibble::te wibble::WibbleTask::_list_tasks_in_group(const bool selector, const task_group& selected_group, const bool closed_group)
{
    unsigned long index = 0;
    std::pair<tle,tle> tasks = _get_tasks_for_id_list(selected_group.nodes);
    auto all_results = tasks.first.size() + tasks.second.size();
    unsigned long tot_first_results = tasks.first.size();
    unsigned long tot_second_results = tasks.second.size();
    // completed/closed tasks
    wc.console_write_header("GROUP");
    std::map<std::size_t, task_entry> task_num_map;

    // closed tasks
    _build_display_tasks_in_group(index, selected_group.group_name, task_num_map, tasks.second, true, selector, all_results);
    //if(tot_second_results == 0) { wc.console_write("EMPTY: no tasks completed yet."); }
    // open tasks
    _build_display_tasks_in_group(index, selected_group.group_name, task_num_map, tasks.first, false, selector, all_results);
    //if(tot_first_results == 0) { wc.console_write("EMPTY: no open tasks."); }
    _display_group_completion_stats(tot_second_results, tot_first_results);

    if(selector)
    {
        std::string r_ans = _present_group_task_command(tot_first_results, closed_group);
        std::cout << '\n';
        task_entry t = _dispatch_task_group_command(r_ans, selected_group, closed_group);

        // ensure loop if adding tasks
        if((r_ans == "A" || r_ans == "a") && t.task_id != "___NULL___")
        {
            t.task_id = "ADD";
            return t;
        }
        else if(t.task_id != "___NULL___") { return t; }

        // sorting option
        if(r_ans == "U" || r_ans == "u" || r_ans == "P" || r_ans == "p" || r_ans == "y" || r_ans == "y")
        {
            // replace/mutate original map, with new sorted results
            index = 0;
            task_num_map.clear();

            if(r_ans == "U" || r_ans == "u") // rank by due date, soonest at top of list
            {
                std::sort(tasks.first.begin(), tasks.first.end(),
                          [](te const& a, te const& b) { return a.due_date < b.due_date; });
                std::sort(tasks.second.begin(), tasks.second.end(),
                          [](te const& a, te const& b) { return a.due_date < b.due_date; });
            }
            else if(r_ans == "P" || r_ans == "p") // rank by priority, highest value at top of list
            {
                std::sort(tasks.first.begin(), tasks.first.end(),
                          [](te const& a, te const& b) { return a.priority > b.priority; });
                std::sort(tasks.second.begin(), tasks.second.end(),
                          [](te const& a, te const& b) { return a.priority > b.priority; });
            }
            else // rank by category, alphabetical ascending
            {
                std::sort(tasks.first.begin(), tasks.first.end(),
                          [](te const& a, te const& b) { return a.category < b.category; });
                std::sort(tasks.second.begin(), tasks.second.end(),
                          [](te const& a, te const& b) { return a.category < b.category; });
            }

            // rebuild completed + open task lists after sort
            _build_display_tasks_in_group(index, selected_group.group_name, task_num_map, tasks.second, true, selector, all_results);
            if(tot_second_results == 0) { wc.console_write("EMPTY: no tasks completed yet."); }
            _build_display_tasks_in_group(index, selected_group.group_name, task_num_map, tasks.first, false, selector, all_results);
            if(tot_first_results == 0) { wc.console_write("EMPTY: no open tasks."); }
            _display_group_completion_stats(tot_second_results, tot_first_results);
            // don't show sort options again
            r_ans = _present_group_task_command(tot_first_results, closed_group, false);
            task_entry t = _dispatch_task_group_command(r_ans, selected_group, closed_group);

            // ensure loop if adding tasks
            if((r_ans == "A" || r_ans == "a") && t.task_id != "___NULL___")
            {
                closed_tasks.clear();
                open_tasks.clear();
                _gather_tasks();
                t.task_id = "ADD";
                return t;
            }
            else if(t.task_id != "___NULL___") { return t; }
        }

        unsigned long ans = 0;
        try { ans = std::stol(r_ans); } catch (std::exception& ex) { }
        if(ans != 0) { return task_num_map[ans]; }
        else         { return task_entry();      }
    }
    else
    {
        return task_entry();
    }
}

/**
 * @brief Remove an entry from a taskfile.
 * @param task_id unique identifier hash for the task entry
 * @param taskfile_fn filename of the taskfile to excise the entry from
 */
bool wibble::WibbleTask::_excise_entry_from_open_task_file(const std::string& task_id, const std::string& taskfile_fn)
{
    try
    {
        //FIXME: this function may be redundant due to _remove_line_from_file function
        //std::cout << "DEBUG: taskfile_fn: " << taskfile_fn << std::endl;
        std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(taskfile_fn);
        std::string line;
        std::ofstream repl_task_file;
        repl_task_file.open(taskfile_fn + ".new", std::ios::out | std::ios::trunc);
        while(std::getline(task_ss, line, '\n'))
        {
            if(line.find(task_id) == std::string::npos) { repl_task_file << line + '\n'; }
            else                                        { continue;                      } //excise the task
        }
        repl_task_file.close();
        std::stringstream().swap(task_ss);

        return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, taskfile_fn + ".new", taskfile_fn);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _excise_from_open_task_file(): " << ex.what() << std::endl;
        return false;
    }
}


/**
 * @brief Extract the created date from the serialized date string in a task entry.
 * @param task_id unique identifier hash for the task entry
 */
std::string wibble::WibbleTask::_extract_created_date(const std::string& task_id) const
{
    return task_id.substr(0,4) + "-" + task_id.substr(4,2) + "-" + task_id.substr(6,2)
        + " " + task_id.substr(8,2) + ":" + task_id.substr(10,2);
}

/**
 * @brief Helper function to determine the group file that contains the task_entry.
 * @param task_id unique identifier hash for the task entry
 * @param group_dir parent/root directory to search for the entry within (year prefixed)
 */
const std::string wibble::WibbleTask::_find_groupfile_for_task(const std::string& task_id, const std::string& group_dir)
{
    try
    {
        for (auto const& dir_entry : std::filesystem::directory_iterator{group_dir})
        {
            if(dir_entry.is_regular_file())
            {
                //std::cerr << "Parsing: " << dir_entry.path() << '\n';
                const std::string fn = dir_entry.path();
                std::stringstream group_ss = wibble::WibbleIO::read_file_into_stringstream(fn);
                std::string group_to_str = group_ss.str();
                std::stringstream().swap(group_ss);
                // have we found it?
                if(group_to_str.find(task_id) != std::string::npos) { return fn; }
            }
        }

        // if we reach here, no matches
        return "";
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _find_groupfile_for_task(): " << ex.what() << std::endl;
        return "";
    }
}

/**
 * @brief Main wrapper function to read in all task entries/files.
 * @param silent flag determining whether to supress console output
 */
void wibble::WibbleTask::_gather_tasks(const bool silent)
{
    //std::cout << "In _gather_tasks() with " << tasks_base_dir << std::endl;
    try
    {
        // gather all the open tasks
        _accumulate_open_tasks(silent);
        // gather all the group tasks
        _accumulate_task_groups(silent);
        wc.console_write_hz_divider(silent);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _gather_tasks: " << ex.what() << std::endl;
    }
}

/**
 * @brief Helper function to return the closed task group directory for Wibble environment task directory setting.
 * @param year four character YYYY year string (e.g. "2024")
 */
const std::string wibble::WibbleTask::_get_closed_group_dir(const std::string& year) const
{
    return tasks_base_dir + CLOSED_GROUP_PATH + year + "/";
}

/**
 * @brief Helper function to return the closed task file directory for Wibble environment task directory setting.
 * @param year four character YYYY year string (e.g. "2024")
 */
const std::string wibble::WibbleTask::_get_closed_task_dir(const std::string& year) const
{
    return tasks_base_dir + CLOSED_TASK_PATH + year + "/";
}

/**
 * @brief Helper function to return the closed task file filename for given category and year.
 * @param category string with exact category name
 * @param year four character YYYY year string (e.g. "2024")
 */
const std::string wibble::WibbleTask::_get_closed_taskfile_fn(const std::string& category, const std::string& year) const
{
    return _get_closed_task_dir(year) + year + "_" + category + ".wib";
}

/**
 * @brief Helper function to return total number of completed tasks.
 */
int wibble::WibbleTask::_get_completed_tasks() const
{
    return tot_completed_tasks;
}

/**
 * @brief Helper function to output task specific data directory for all extended data/task project files.
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_data_dir_for_task(const std::string& task_id) const
{
    const std::string YYYY = task_id.substr(0,4);
    return tasks_base_dir + TASK_DATA_PATH + YYYY + "/" + task_id + "/";
}

/**
 * @brief Helper function to return the inline description file for a task entry
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_desc_file_for_task(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_desc.wib";
}

/**
 * @brief Helper function to return the open task group directory for Wibble environment task directory setting.
 * @param year four character YYYY year string (e.g. "2024")
 */
const std::string wibble::WibbleTask::_get_open_group_dir(const std::string& year) const
{
    return tasks_base_dir + OPEN_GROUP_PATH + year + "/";
}

/**
 * @brief Helper function to return the open task file directory for Wibble environment task directory setting.
 * @param year four character YYYY year string (e.g. "2024")
 */
const std::string wibble::WibbleTask::_get_open_task_dir(const std::string& year) const
{
    return tasks_base_dir + OPEN_TASK_PATH + year + "/";
}

/**
 * @brief Helper function to return the open task file filename for given category and year.
 * @param category string with exact category name
 * @param year four character YYYY year string (e.g. "2024")
 */
const std::string wibble::WibbleTask::_get_open_taskfile_fn(const std::string& category, const std::string& year) const
{
    return _get_open_task_dir(year) + year + "_" + category + ".wib";
}

/**
 * @brief Helper function to return the project data directory associated with a specific task entry.
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_proj_data_dir_for_task(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_proj/";
}


/**
 * @brief Function to return the project index (managed project file listing) for a given task entry.
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_proj_index_for_task(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_proj.wib";
}

/**
 * @brief Function to open/work with the task specific Jotfile.
 * @param entry task entry struct to obtain specific Jotfile for
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::_get_jotfile_perform_action(const task_entry& entry, const pkg_options& OPTS)
{
    try
    {
        bool has_task_jf = false;
        bool has_assc_jf = false;

        const std::string df_jotfile = _get_task_specific_jf_fn(entry.task_id);
        if(std::filesystem::exists(std::filesystem::path(df_jotfile))) { has_task_jf = true; }

        const std::string assoc_jf = _get_task_data_jf_fn(entry.task_id);
        if(std::filesystem::exists(std::filesystem::path(assoc_jf)))   { has_assc_jf = true; }

        // [ filename : title ] tuple
        std::vector<std::pair<std::string, std::string>> jf_entries;

        std::stringstream jf_ss;
        std::string line;
        // 1. Get details of default task-local jotfile (if exists)
        if(has_task_jf)
        {
            std::pair<std::string, std::string> jf_entry;
            jf_entry.first  = df_jotfile;
            jf_entry.second = "[ Default task local jotfile ]";
            jf_entries.push_back(jf_entry);
        }

        // 2. Now associated jotfiles
        if(has_assc_jf)
        {
            jf_ss = wibble::WibbleIO::read_file_into_stringstream(assoc_jf);
            while(std::getline(jf_ss, line, '\n'))
            {
                std::size_t pos = line.find(DELIM);
                if(pos != std::string::npos)
                {
                    std::pair<std::string, std::string> jf_entry;
                    jf_entry.first  = line.substr(0, pos);
                    jf_entry.second = line.substr(pos + 1, line.length() - 1 - pos);
                    jf_entries.push_back(jf_entry);
                }
            }
            std::stringstream().swap(jf_ss);
        }

        std::cout << '\n';
        wc.console_write_header("SELECT ");
        const std::size_t res = jf_entries.size();
        int choice = 0;
        std::map<int, std::string> fn_map;
        unsigned int maxlength = 0;
        // for nice vertical alignment
        for(auto const& jf: jf_entries)
        {
            if(std::string(std::filesystem::path(jf.first).filename()).length() > maxlength)
            { maxlength = std::string(std::filesystem::path(jf.first).filename()).length(); }
        }
        for(auto const& jf: jf_entries)
        {
            ++choice;
            fn_map[choice] = jf.first;
            std::string sfn = std::filesystem::path(jf.first).filename();
            if(sfn.length() < maxlength) { sfn.insert(0, (maxlength - sfn.length()), ' '); }
            wc.console_write_padded_selector(choice, res, ""); wc.console_write_green_bold(sfn); wc.console_write(" ┃ " + jf.second);
        }
        wc.console_write_hz_divider();
        unsigned long sel = wc.console_present_record_selector(res);
        if(sel != 0 && sel <= res)
        {
            WibbleActions wa;
            std::string res = wa.jotfile_action_menu(wc, OPTS.paths.jot_dir, OPTS.paths.tmp_dir, fn_map[sel]);
            if(res != "") // deletion request, need to update index
            {
                wc.console_write_green(ARW + "Jotfile was removed, updating task jotfile associations.\n");
                bool index_upd = _excise_entry_from_open_task_file(res, assoc_jf);
                if(index_upd) { wc.console_write_success_green("Task jotfile associations successfully updated."); }
            }
        }
        else
        {
            wc.console_write_error_red("Invalid selection. Aborting.");
        }

    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _get_jotfile_perform_action(): " << ex.what() << std::endl;
    }
}

/**
 * @brief Function to open/work with a Topic entry associated to a task.
 * @param entry task entry struct to obtain specific Topic entry for
 */
wibble::bm wibble::WibbleTask::_get_topic_record_from_task_list(const task_entry& entry)
{
    WibbleActions wa;
    std::vector<bm> bm_topics;
    const std::string topic_task_file = _get_task_data_topic_fn(entry.task_id);
    //std::cout << "DEBUG: topic_task_file = " << topic_task_file << std::endl;
    try
    {
        std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(topic_task_file);
        std::string line;
        int choice = 0;
        while(std::getline(task_ss, line, '\n'))
        {
            bm entry = wa.deserialize_topic_bm(line);
            if(entry.field4 != "___NULL___") { bm_topics.push_back(entry); }
        }

        std::stringstream().swap(task_ss);

        wc.console_write_header("SELECT");
        const std::size_t res = bm_topics.size();
        for(auto const& e: bm_topics)
        {
            ++choice;
            wc.console_write_padded_selector(choice, res, ""); wc.console_write(e.field5 + ":" + e.field2 + " " + e.title);
        }
        wc.console_write_hz_divider();
        unsigned long sel = wc.console_present_record_selector(res);
        if(sel != 0 && sel <= res)
        {
            return bm_topics.at(sel - 1);
        }
        else { return bm(); }

    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _get_topic_record_from_task_list(): " << ex.what() << std::endl;
        return bm();
    }
}

/**
 * @brief Function to open/work with a Node entry associated to a task.
 * @param entry task entry struct to obtain specific Topic entry for
 * @param wib_e WibbleExecutor envrionment/execution object
 */
wibble::WibbleRecord wibble::WibbleTask::_get_node_record_from_task_list(const task_entry& entry, WibbleExecutor& wib_e)
{
    const std::string node_task_file = _get_task_data_nodes_fn(entry.task_id);
    try
    {
        std::stringstream task_ss = wibble::WibbleIO::read_file_into_stringstream(node_task_file);
        std::string line;
        int choice = 0;
        std::map<int, std::string> node_choice;
        std::vector<std::string> title_list;
        while(std::getline(task_ss, line, '\n'))
        {
            std::size_t sep = line.find(FIELD_DELIM);
            if(sep != std::string::npos)
            {
                ++choice;
                node_choice[choice] = line.substr(0, sep);
                title_list.push_back(line.substr(sep + 1, line.length() - sep));
            }
        }

        std::stringstream().swap(task_ss);

        std::cout << '\n';
        wc.console_write_header("SELECT");

        choice = 0;
        const std::size_t res = title_list.size();
        for(auto const& e: title_list)
        {
            ++choice;
            wc.console_write_padded_selector(choice, res, ""); wc.console_write(e);
        }

        wc.console_write_hz_divider();
        unsigned long sel = wc.console_present_record_selector(res);
        if(sel != 0 && sel <= res)
        {
            search_filters SEARCH;
            SEARCH.id = node_choice[res];
            return wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH);
        }
        else
        {
            return WibbleRecord();
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _get_node_record_from_task_list(): " << ex.what() << std::endl;
        return WibbleRecord();
    }
}

/**
 * @brief Helper function to obtain the task specific associated Jotfile list
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_task_data_jf_fn(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_jotfiles.wib";
}

/**
 * @brief Helper function to obtain the task specific linked file list
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_task_data_lf_fn(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_lf.wib";
}

/**
 * @brief Helper function to obtain the task specific associated Node list
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_task_data_nodes_fn(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_nodes.wib";
}

/**
 * @brief Helper function to obtain the task specific associated Topic list
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_task_data_topic_fn(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_topics.wib";
}

/**
 * @brief Helper function to obtain the task specific default/task local Jotfile
 * @param task_id string with unique task entry hash/identifier
 */
const std::string wibble::WibbleTask::_get_task_specific_jf_fn(const std::string& task_id) const
{
    return _get_data_dir_for_task(task_id) + task_id + "_default_jf.wib";
}

/**
 * @brief Helper function to obtain the target Topic entry filename associated with a particular task entry
 * @param entry task entry struct to obtain specific Topic entry for
 * @param wib_e WibbleExecutor envrionment/execution object
 */
const std::string wibble::WibbleTask::_get_topic_fn_from_task_list(const task_entry& entry, WibbleExecutor& wib_e)
{
    std::string fn = _get_topic_record_from_task_list(entry).field4;
    if(fn == "___NULL___") { std::cerr << "Error: NULL topic." << std::endl; return ""; }
    else { return wib_e.get_topics_dir() + "/" + fn; }
}

/**
 * @brief Wrapper function to ensure that either a valid datetime is set, or none/placeholder for none
 * @param due_date string with ISO YYYY-MM-DD [hh:mm:ss] format datetime string to parse
 */
const std::string wibble::WibbleTask::_get_valid_datetime_str_or_NONE(const std::string& due_date) const
{
    if(due_date == "NONE" || due_date == "") { return "___NONE___"; }

    std::time_t task_due_datetime = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str(due_date, true);
    char tm_str[60];
    std::strftime(tm_str, sizeof(tm_str), "%Y-%m-%d", std::gmtime(&task_due_datetime));
    const std::string gen_date = std::string(tm_str);
    if(gen_date == "1900-01-01")
    {
        wc.console_write_red("Warning: Specified date of '" + due_date + "' is invalid.\n");
        wc.console_write_red("Date should be input as YYYY-MM-DD, e.g. '2024-09-28'.\n");
        wc.console_write_red("Defaulting to none/no due date.\n");
        return "___NONE___";
    }
    else { return gen_date; }
}

/**
 * @brief Wrapper function to "mark" i.e. set task to done/complete
 * @param entry task entry struct to mark complete
 */
bool wibble::WibbleTask::_mark_task_as_done(const task_entry& entry)
{
    // guard clause
    if(entry.completed) { std::cerr << "Task already done." << std::endl; return false; }

    bool move_from_task_file = _move_task_to_closed_from_taskfile(entry);
    if(! move_from_task_file) { wc.console_write_error_red("Error completing task in task file.");  }
    bool update_group_file  = _update_task_to_closed_in_group_file(entry.task_id);
    if(! move_from_task_file) { wc.console_write_error_red("Error completing task in group file."); }
    _write_history(entry, 2);
    if(move_from_task_file && update_group_file) { return true;  }
    else                                         { return false; }
}

/**
 * @brief Helper function to move the completed task from open to closed task entry list
 * @param entry task entry struct to write out to closed, remove from open task list file
 */
bool wibble::WibbleTask::_move_task_to_closed_from_taskfile(const task_entry& entry)
{
    try
    {
        const std::string YYYY = entry.task_id.substr(0,4);
        const std::string task_file = _get_open_taskfile_fn(entry.category, YYYY);
        const std::string completed_task_dir = _get_closed_task_dir(YYYY);
        const std::string closed_task_fn = _get_closed_taskfile_fn(entry.category, YYYY);

        // ensure necessary paths exist
        wibble::WibbleIO::create_path(completed_task_dir);

        std::string task_entry_string = serialize_task_entry(entry);

        bool wrote_task_file = wibble::WibbleIO::write_out_file_append(task_entry_string, closed_task_fn);
        if(wrote_task_file)
        {
            // now remove from open task file
            return _excise_entry_from_open_task_file(entry.task_id, task_file);
        }
        else
        {
            wc.console_write_error_red("Error adding task to completed task file.");
            return false;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _move_task_to_closed_from_taskfile(): " << ex.what() << std::endl;
        return false;
    }

}

/**
 * @brief Interactive wrapper function for "marking" (i.e. completing) open tasks
 * @param cat_fltr optional string filter to substring match on category names
 * @param title_fltr optional string filter to substring match on task entry titles/todos
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::_mark_task_done(const std::string& cat_fltr, const std::string& title_fltr, const pkg_options& OPTS)
{
    task_entry e = _list_open_tasks(cat_fltr, title_fltr, true);
    if(e.task_id != "___NULL___") { _present_task_action_menu(e, OPTS, false); }
}

/**
 * @brief Obtain a task entry definition for a single specific task_id + category
 * @param task_id string with unique task entry hash/identifier
 * @param category string with exact category name
 */
wibble::te wibble::WibbleTask::_parse_task_file_for_single_id(const std::string& task_id, const std::string& category)
{
    try
    {
        const std::set<std::string> id_list { task_id };
        const std::string open_task_file   = _get_open_taskfile_fn(category, task_id.substr(0,4));
        const std::string closed_task_file = _get_closed_taskfile_fn(category, task_id.substr(0,4));
        tle results;
        // try open tasks first
        if(std::filesystem::exists(std::filesystem::path(open_task_file)))
        {
            results = _parse_task_file_with_filter(open_task_file, id_list, false);
        }

        // if we've found a match, return it
        if(results.size() == 1) { return results.at(0); }
        else if(std::filesystem::exists(std::filesystem::path(closed_task_file))) // try closed tasks next
        {
            // overwrite it
            results = _parse_task_file_with_filter(closed_task_file, id_list, false);
        }

        // if we've found a matched in closed tasks, return that
        if(results.size() == 1) { results.at(0).completed = true; return results.at(0); }
        else                    { return task_entry();                                  } // no results
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _parse_task_file_for_single_id(): " << ex.what() << std::endl;
        return task_entry();
    }
}

/**
 * @brief Present an interactive menu of task specific operations and dispatch
 * @param entry task entry struct to operate upon
 * @param OPTS pkg_options environment settings/configuration struct
 * @param closed_group flag controlling whether to present options relevant for open or closed task group
 */
void wibble::WibbleTask::_present_task_action_menu(task_entry& entry, const pkg_options& OPTS, const bool closed_group)
{
    print_task_detailed(entry);
    if(entry.completed) { wc.console_write_yellow_bold("Task has been"); wc.console_write_green_bold(" COMPLETED.\n"); }
    else                { wc.console_write_yellow_bold("Task is"); wc.console_write_red_bold(" OPEN.\n\n");            }

    wc.console_write_header("ACTIONS");
    if(! entry.completed) { wc.console_write_yellow("1. Mark task as "); wc.console_write_yellow_bold("COMPLETE.\n"); }
    else                  { wc.console_write_red("1. Mark task as COMPLETE.\n");    }
    wc.console_write_yellow("2. Add/edit task "); wc.console_write_yellow_bold("description/details.\n");
    wc.console_write_yellow("3. Add/associate "); wc.console_write_yellow_bold("jotfile ");  wc.console_write_yellow("with task.\n");
    wc.console_write_yellow("4. Associate "); wc.console_write_yellow_bold("node ");         wc.console_write_yellow("with task.\n");
    wc.console_write_yellow("5. Associate "); wc.console_write_yellow_bold("topic ");        wc.console_write_yellow("entry with task.\n");
    wc.console_write_yellow("6. "); wc.console_write_yellow_bold("Link, copy, or move ");    wc.console_write_yellow("file/directory into task data.\n");
    wc.console_write_yellow("7. Manage "); wc.console_write_green_bold("project files ");    wc.console_write_yellow("associated with task.\n");
    wc.console_write_yellow("8. Adjust task "); wc.console_write_yellow_bold("properties "); wc.console_write_yellow("(title/priority/due date).\n");
    wc.console_write_yellow("9. Permanently "); wc.console_write_yellow_bold("delete ");     wc.console_write_yellow("task.\n");
    if(entry.extended)
    {
        wc.console_write_yellow("10. Manage "); wc.console_write_green_bold("associated");
        wc.console_write_yellow(" task data (node/topic/jotfiles).\n");
        wc.console_write_yellow("11. Manage "); wc.console_write_green_bold("linked");
        wc.console_write_yellow(" files (links & stored files).\n");
    }
    wc.console_write_hz_divider();
    wc.console_write_yellow("q. "); wc.console_write_yellow_bold("Quit "); wc.console_write_yellow("from menu.\n");
    wc.console_write_hz_heavy_divider();

    std::string resp = wc.console_write_prompt("\nSelect action > ", "", "");
    wc.trim(resp);

    if(resp == "q" || resp == "Q") { resp = "-1"; }

    int choice = 0;
    try
    {
        choice = std::stoi(resp);
    }
    catch(std::exception& ex)
    {
        choice = 0;
    }

    bool result = false;
    std::string pf = "";
    switch(choice)
    {
    case -1: //silently exit; don't throw exception here
        break;
    case 0:
        wc.console_write_error_red("Invalid choice, exiting.");
        break;
    case 1:
        if(! entry.completed)
        {
            print_task_detailed(entry);
            resp = wc.console_write_prompt("Mark above task as complete? (y/n) > ", "", "");
            if(resp == "y" || resp == "Y")
            {
                result = _mark_task_as_done(entry);
                if(result) { wc.console_write_success_green("Task marked as complete."); }
            }
            else { wc.console_write_error_red("User cancelled."); }
        }
        else { wc.console_write_error_red("Task has already been marked done."); }
        break;
    case 2:
        result = _add_edit_inline_task_details(entry);
        if(result) { wc.console_write_success_green("Details/description added to task."); }
        break;
    case 3:
        result = _associate_jotfile_with_task(entry, OPTS);
        if(result) { wc.console_write_success_green("Jotfile association complete."); }
        break;
    case 4:
        result = _associate_node_with_task(entry, OPTS);
        if(result) { wc.console_write_success_green("Node associated with task."); }
        break;
    case 5:
        result = _associate_topic_with_task(entry, OPTS);
        if(result) { wc.console_write_success_green("Topic entry associated with task."); }
        break;
    case 6:
        result = _associate_linked_file_with_task(entry);
        if(result) { wc.console_write_success_green("File associated with task."); }
        break;
    case 7:
        pf = _list_project_files(entry.task_id);
        if(pf == "ADD") { result = _add_project_files(entry);        }
        else if(pf == "CD")
        {
            const std::string proj_dir = _get_proj_data_dir_for_task(entry.task_id);
            WibbleUtility::cd_helper(proj_dir);
            result = true;
        }
        else if(pf == "DIR" || pf == "OPEN" )
        {
            WibbleExecutor wib_e{OPTS};
            const std::string a = wib_e.get_fm_tool();
            const std::string proj_dir = _get_proj_data_dir_for_task(entry.task_id);
            if(pf == "OPEN" ) { wib_e.run_exec_expansion(wib_e.get_fm_tool(), proj_dir); }
            else              { wc.console_write("\n" + proj_dir + "\n");                }
            result = true;
        }
        else if(pf == "NODE")
        {
            wc.console_write_green("\nThis will copy all project files into the selected Node data directory.\n");
            const std::string copy_prj = wc.console_write_prompt("Proceed? (y/n) > ", "", "");
            wc.console_write("");
            if(copy_prj == "y" || copy_prj == "Y")
            {
                std::string proj_dir = _get_proj_data_dir_for_task(entry.task_id);
                std::string lf_list = _get_task_data_lf_fn(entry.task_id);
                bool success = _copy_project_into_node(OPTS, proj_dir, lf_list);
                if(success)
                {
                    _write_history(entry, 13);
                    wc.console_write("");
                    wc.console_write_header("COPIED");
                    wc.console_write_green("Task project files copied/propagated into Node data directory.\n");
                    wc.console_write_green("\nThe files now exist in two locations.\n");
                    wc.console_write_green("\nAfter verifying all are present/correct within the Node data,\n");
                    wc.console_write_green("consider deleting the task item to permanently remove all \n");
                    wc.console_write_green("the now duplicated files.\n");
                    wc.console_write_hz_divider();
                }
                throw wibble::wibble_exit(0);
            }
        }
        else if(pf == "TOPIC")
        {
            wc.console_write_green("\nThis will copy all project files as a data directory entry into the selected Topic.\n");
            const std::string copy_prj = wc.console_write_prompt("Proceed? (y/n) > ", "", "");
            wc.console_write("");
            if(copy_prj == "y" || copy_prj == "Y")
            {
                std::string proj_dir = _get_proj_data_dir_for_task(entry.task_id);
                bool success = _copy_project_into_topic(OPTS, proj_dir);
                if(success)
                {
                    _write_history(entry, 14);
                    wc.console_write("");
                    wc.console_write_header("COPIED");
                    wc.console_write_green("Task project files copied/propagated into Topic data directory.\n");
                    wc.console_write_green("\nThe files now exist in two locations.\n");
                    wc.console_write_green("\nAfter verifying all are present/correct within the Topic data,\n");
                    wc.console_write_green("consider deleting the task item to permanently remove all \n");
                    wc.console_write_green("the now duplicated files.\n");
                    wc.console_write_hz_divider();
                }
                throw wibble::wibble_exit(0);
            }

        }
        else if(pf == "") { wc.console_write_error_red("Invalid selection."); }
        else
        {
            result = _perform_project_file_action(entry.task_id, pf, OPTS);
        }
        if(result) { wc.console_write_success_green("Project files operation complete."); }
        break;
    case 8:
        result = _adjust_task_properties(entry, closed_group);
        if(result) { wc.console_write_success_green("Task properties adjusted."); }
        break;
    case 9:
        result = _delete_task(entry, closed_group);
        if(result) { wc.console_write_success_green("Task successfully deleted."); }
        break;
    case 10:
        _present_task_extension_menu(entry, OPTS);
        break;
    case 11:
        _present_lf_extension_menu(entry, OPTS);
        break;
    default:
        wc.console_write_error_red("Invalid selection.");
    }
}

/**
 * @brief Present an interactive menu of project specific operations and dispatch
 * @param task_id string with unique task entry hash/identifier
 * @param pf string with filename/path to task specific project index file
 * @param OPTS pkg_options environment settings/configuration struct
 */
bool wibble::WibbleTask::_perform_project_file_action(const std::string& task_id, const std::string& pf, const pkg_options& OPTS)
{
    wc.console_write_header("PROJECT");
    wc.console_write_yellow("1. Open file with "); wc.console_write_yellow_bold("CLI editor.\n");
    wc.console_write_yellow("2. Open file with "); wc.console_write_yellow_bold("GUI editor.\n");
    wc.console_write_yellow("3. Dump file to ");   wc.console_write_yellow_bold("console.\n");
    wc.console_write_yellow("4. View file ");      wc.console_write_yellow_bold("with viewer.\n");
    wc.console_write_yellow("5. Permanently ");    wc.console_write_red_bold("delete ");        wc.console_write_yellow("file.\n");
    wc.console_write_hz_divider();
    wc.console_write_yellow("q. "); wc.console_write_yellow_bold("Quit "); wc.console_write_yellow("from menu.\n");
    wc.console_write_hz_heavy_divider();

    std::string resp = wc.console_write_prompt("\nSelect action > ", "", "");
    wc.trim(resp);

    if(resp == "q" || resp == "Q") { throw wibble::wibble_exit(0); }

    int choice = 0;
    try
    {
        choice = std::stoi(resp);
    }
    catch(std::exception& ex)
    {
        return false;
    }

    WibbleExecutor wib_e{OPTS};
    const std::string prj_file_path = _get_proj_data_dir_for_task(task_id) + pf;

    std::filesystem::path chk_pf{prj_file_path};
    std::string pf_extn = chk_pf.extension();
    // filesystem library includes "." in extension (not sure why they chose that...)
    if(pf_extn.length() >= 2 && pf_extn.at(0) == '.') { pf_extn = pf_extn.substr(1, pf_extn.length() - 1); }

    const std::string bin_edit_cmd = WibbleUtility::get_binary_tool(wc, pf_extn, bin_mapping, true);
    const std::string bin_view_cmd = WibbleUtility::get_binary_tool(wc, pf_extn, bin_mapping, false);

    switch(choice)
    {
    case 1:
        if(bin_edit_cmd == "") { wib_e.run_exec_expansion(wib_e.get_cli_editor(), prj_file_path);  }
        else                   { wib_e.run_exec_expansion(bin_edit_cmd, prj_file_path);            }
        break;
    case 2:
        if(bin_edit_cmd == "") { wib_e.run_exec_expansion(wib_e.get_gui_editor(), prj_file_path);  }
        else                   { wib_e.run_exec_expansion(bin_edit_cmd, prj_file_path);            }
        break;
    case 3:
        if(bin_view_cmd == "") { wib_e.run_exec_expansion(wib_e.get_dump_tool(), prj_file_path);  }
        else                   { wib_e.run_exec_expansion(bin_view_cmd, prj_file_path);            }
        break;
    case 4:
        if(bin_view_cmd == "") { wib_e.run_exec_expansion(wib_e.get_view_tool(), prj_file_path);  }
        else                   { wib_e.run_exec_expansion(bin_view_cmd, prj_file_path);            }
        break;
    case 5:
        wc.console_write_hz_divider();
        wc.console_write_red_bold("Pending deletion: "); wc.console_write_red(pf + "\n");
        wc.console_write_hz_divider();
        resp = wc.console_write_prompt("Really delete above file? Input 'delete' to confirm > ", "", "");
        if(resp == "delete")
        {
            //std::cout << "DEBUG: prj_file_path = " << prj_file_path << std::endl;
            if(std::filesystem::exists(std::filesystem::path(prj_file_path)))
            {
                std::filesystem::remove(std::filesystem::path(prj_file_path));
            }
            const std::string pi = _get_proj_index_for_task(task_id);
            return _excise_entry_from_open_task_file(pf, pi);
        }
        else { wc.console_write_error_red("User cancelled."); }
        break;
    default:
        break;
    }

    return true;
}

/**
 * @brief Present an interactive category selector/add new category selector
 */
std::string wibble::WibbleTask::_present_category_list()
{
    std::set<std::string> categories = _build_category_list();
    std::map<int,std::string> cat_map;
    int i = 0;
    for(auto const& c: categories)
    {
        ++i;
        cat_map[i] = c;
        wc.console_write_padded_selector(i, categories.size(), c + "\n");
    }
    wc.console_write_padded_selector(0, 0, "[ Input new category ]", "A");
    std::cout << "\n\n";
    std::string resp = wc.console_write_prompt("Select category/option > ", "", "");
    wc.trim(resp);
    if(resp == "a" || resp == "A")
    {
        std::string category = wc.console_write_prompt("Input category for task       *: ", "", "general");
        wc.trim(category);
        return category;
    }
    else
    {
        unsigned int choice = 0;
        try { choice = std::stoi(resp); } catch (std::exception& ex) {}

        if(choice >= 1 && choice <= categories.size()) { return cat_map[choice]; }
        else
        {
            wc.console_write_red("Invalid choice. Assigning to 'general' category.\n");
            return "general";
        }
    }
}

/**
 * @brief Present the task linked file interactive menu/opener
 * @param task task_entry struct that has the associated linked file list
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::_present_lf_extension_menu(const task_entry& entry, const pkg_options& OPTS)
{
    const std::string task_lf = _get_task_data_lf_fn(entry.task_id);
    const std::string lf_dd = _get_proj_data_dir_for_task(entry.task_id) + WIB_LINKED;
    WibbleExecutor wib_e{OPTS};
    wibble::WibbleUtility::open_file_selector(cache_dir, wc, wib_e, task_lf, TASKDIR_PLCHDR, lf_dd);
}

/**
 * @brief Present the task 'extension' menu, i.e. for working with associating Jotfiles, Nodes, and Topic entries
 * @param task task_entry struct that has the associated linked file list
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::_present_task_extension_menu(const task_entry& entry, const pkg_options& OPTS)
{
    // determine which menu options to present
    bool has_jf     = false;
    bool has_df_jf  = false;
    bool has_nodes  = false;
    bool has_topics = false;

    if(std::filesystem::exists(std::filesystem::path(_get_task_data_jf_fn(entry.task_id))))     { has_jf     = true; }
    if(std::filesystem::exists(std::filesystem::path(_get_task_specific_jf_fn(entry.task_id)))) { has_df_jf  = true; }
    if(std::filesystem::exists(std::filesystem::path(_get_task_data_nodes_fn(entry.task_id))))  { has_nodes  = true; }
    if(std::filesystem::exists(std::filesystem::path(_get_task_data_topic_fn(entry.task_id))))  { has_topics = true; }

    wc.console_write("\n");
    wc.console_write_header("ASSOC ");

    if(has_jf || has_df_jf)
                   { wc.console_write_green("a. [ ACTIVE ] Select associated Jotfile and open Jotfile action menu\n"); }
    else           { wc.console_write_red  ("a. [INACTIVE] Select associated Jotfile and open Jotfile action menu\n"); }

    if(has_nodes)  { wc.console_write_green("b. [ ACTIVE ] Select associated Node and open Node action menu\n"); }
    else           { wc.console_write_red  ("b. [INACTIVE] Select associated Node and open Node action menu\n"); }

    if(has_topics) { wc.console_write_green("c. [ ACTIVE ] Select associated Topic entry and Topic action menu\n"); }
    else           { wc.console_write_red  ("c. [INACTIVE] Select associated Topic entry and Topic action menu\n"); }

    wc.console_write_hz_divider();

    std::string resp = wc.console_write_prompt("\nSelect option: [a:e] >", "", "");
    wc.trim(resp);

    int choice = 0;
    if(resp == "a" || resp == "A") { if(has_jf || has_df_jf) { choice = 1;} }
    if(resp == "b" || resp == "B") { if(has_nodes)           { choice = 2;} }
    if(resp == "c" || resp == "C") { if(has_topics)          { choice = 3;} }

    switch(choice)
    {
    case 1:
        _get_jotfile_perform_action(entry, OPTS);
       break;
    case 2:
        _present_task_node_action_menu(entry, OPTS);
       break;
    case 3:
        _present_task_topic_action_menu(entry, OPTS);
       break;
    default:
        wc.console_write_error_red("Invalid selection. Exiting.");
        break;
    }
}

/**
 * @brief Screen output function for printing a task in condensed format, i.e. one task entry per line.
 * @param task task_entry struct that has the associated linked file list
 * @param padding integer value indicating whitespace padding value for nice vertical alignment
 * @param show_cat flag indicating whether to also display the category column
 */
void wibble::WibbleTask::_print_task_condensed(const task_entry& entry, const bool completed, const int padding, const bool show_cat)
{
    if(completed && ENABLECOLOUR)
    { std::cout << rang::fgB::green << TCK << rang::style::reset << " ┃ (" << _extract_created_date(entry.task_id) << ") ┃ "; }
    else if(! completed && ENABLECOLOUR)
    { std::cout << rang::fgB::red << CRS << rang::style::reset << " ┃ (" << _extract_created_date(entry.task_id) << ") ┃ "; }
    else if(completed)
    { std::cout << TCK << " ┃ (" << _extract_created_date(entry.task_id) << ") ┃ "; }
    else
    { std::cout << CRS << " ┃ (" << _extract_created_date(entry.task_id) << ") ┃ "; }

    if(show_cat)
    {
        std::string spacer = "";
        int padcount = padding - entry.category.length();
        if(padcount > 0) { spacer.insert(0, padcount, ' '); }
        wc.console_write_yellow(entry.category + spacer);
        std::cout << " ┃ ";
    }
    if(entry.extended) { if(ENABLECOLOUR) { wc.console_write_green_bold("E"); } else { std::cout << "E"; } }
    else               { std::cout << "S"; }
    // traffic light priority visual cue
    if(ENABLECOLOUR)
    {
        if     (entry.priority <= 33)
        { std::cout << " ┃ " << rang::fgB::green << std::setfill('0') << std::setw(2) << entry.priority;  }
        else if(entry.priority <= 66)
        { std::cout << " ┃ " << rang::fgB::yellow << std::setfill('0') << std::setw(2) << entry.priority; }
        else
        { std::cout << " ┃ " << rang::fgB::red << std::setfill('0') << std::setw(2) << entry.priority;    }
        std::cout << rang::style::reset;
    }
    else { std::cout << " ┃ " << std::setfill('0') << std::setw(2) << entry.priority; }

    if(entry.due_date == "___NONE___") { std::cout << " ┃    NONE    ┃ "; }
    else
    {
        std::cout << " ┃ " << rang::fgB::yellow << rang::style::bold;
        std::cout << entry.due_date << rang::style::reset << " ┃ ";
    }

    // ID can be extracted from display with wibble++ task --list | grep <search term> | tail -n 1 | cut -c 59-80
    std::cout << entry.task_id << " ┃ ";
    wc.console_write(wc.set_pretty_width(entry.title, TASK_DISPLAY_WIDTH));
}

/**
 * @brief Screen output function for showing detailed output of a particular task
 * @param task task_entry struct that has the associated linked file list
 */
void wibble::WibbleTask::print_task_detailed(const task_entry& entry) const
{
    wc.console_write("\n");
    wc.console_write_header("TASK ");
    wc.console_write_yellow_bold("Task created : "); wc.console_write(_extract_created_date(entry.task_id));
    wc.console_write_yellow_bold("Task category: "); wc.console_write(entry.category);
    wc.console_write_yellow_bold("Task title   : "); wc.console_write(entry.title);
    wc.console_write_yellow_bold("Task id      : "); wc.console_write(entry.task_id);
    wc.console_write_yellow_bold("Task type    : ");
    if(! entry.extended) { wc.console_write("Simple"); }
    else                 { wc.console_write("Extended"); }
    wc.console_write_yellow_bold("Task priority: "); wc.console_write(std::to_string(entry.priority));
    wc.console_write_yellow_bold("Task due date: ");
    if(entry.due_date == "___NONE___") { wc.console_write("NONE");         }
    else                               { wc.console_write(entry.due_date); }
    wc.console_write_hz_divider();
    std::string details = "";
    const std::string desc_file = _get_desc_file_for_task(entry.task_id);
    try
    {
        if(std::filesystem::exists(std::filesystem::path(desc_file)))
        {
            std::stringstream ss = wibble::WibbleIO::read_file_into_stringstream(desc_file);
            details = ss.str();
            std::stringstream().swap(ss);
        }
    }
    catch(std::exception& ex) { std::cerr << "Error reading in description file" << ex.what() << std::endl; }

    if(details != "")
    {
        wc.console_write_header("DETAILS");
        wc.console_write(details);
        wc.console_write_hz_divider();
    }
}

/**
 * @brief Present the task Topic 'action' menu, i.e. for working with associating Topics
 * @param task task_entry struct that has the associated linked file list
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::_present_task_topic_action_menu(const task_entry& entry, const pkg_options& OPTS)
{
    WibbleExecutor wib_e{OPTS};
    std::string topic_fn = _get_topic_fn_from_task_list(entry, wib_e);
    if(topic_fn != "") { wibble::WibbleActions().topic_action_menu(wc, wib_e, topic_fn); }
    else               { wc.console_write_error_red("Malformed topic entry.");           }
}

/**
 * @brief Present the task Node 'action' menu, i.e. for working with associating Nodes
 * @param task task_entry struct that has the associated linked file list
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::_present_task_node_action_menu(const task_entry& entry, const pkg_options& OPTS)
{
    WibbleExecutor wib_e{OPTS};
    WibbleRecord w = _get_node_record_from_task_list(entry, wib_e);
    wibble::WibbleActions().node_action_menu(wc, w, wib_e, false);
}

/**
 * @brief Helper function to adjust the task group header line to the new correct values.
 * @param header_line string with the current header line to adjust
 * @param decrement flag for indicating how to adjust completed task count in case of completion or removal of task entry
 * @param dec_completed flag for handling edge case of task removal/purging rather than simply completion
 */
std::string wibble::WibbleTask::_mark_entry_in_group_header(const std::string& header_line, const bool decrement, const bool dec_completed)
{

    try
    {
        std::size_t c_task_delim = header_line.find(FIELD_DELIM);
        std::size_t t_task_delim = header_line.find(FIELD_DELIM, c_task_delim + 1);
        std::size_t title_delim  = header_line.find(FIELD_DELIM, t_task_delim + 1);

        std::string id = header_line.substr(0, c_task_delim);
        int comp_tasks = std::stoi(header_line.substr(c_task_delim + 1, (t_task_delim - 1 - c_task_delim)));
        int tot_tasks = std::stoi(header_line.substr(t_task_delim + 1, (title_delim - 1 - t_task_delim)));
        std::string title = header_line.substr(title_delim + 1, header_line.length() - title_delim);

        // increment completed tasks
        if(! decrement) { comp_tasks++; }
        else // for handling case where entries are entirely purged from group file
        {
            tot_tasks--;
            if(dec_completed) { comp_tasks--; }
        }
        return id + FIELD_DELIM + std::to_string(comp_tasks) + FIELD_DELIM + std::to_string(tot_tasks) + FIELD_DELIM + title + '\n';

    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _mark_entry_in_group_header(): " << ex.what() << std::endl;
        return "";
    }
}

/**
 * @brief Public accessor/main wrapper function to complete a task (i.e. mark it done)
 * @param cat_fltr optional string filter to substring match on category names
 * @param title_fltr optional string filter to substring match on task entry titles/todos
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::complete_task(const std::string& cat_fltr, const std::string& title_fltr, const pkg_options& OPTS)
{
    std::string cat_filter = cat_fltr;
    std::string title_filter = title_fltr;
    wibble::WibbleRecord::str_to_lowercase(cat_filter);
    wibble::WibbleRecord::str_to_lowercase(title_filter);
    _mark_task_done(cat_filter, title_filter, OPTS);
}

/**
 * @brief Simple function to dump the log of task actions/task history.
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::display_history(const pkg_options& OPTS)
{
    const std::string hist_file = tasks_base_dir + "/" + TASK_HISTORY;
    if(std::filesystem::exists(std::filesystem::path(hist_file)))
    {
        WibbleExecutor wib_e{OPTS};
        wib_e.run_exec_expansion(wib_e.get_dump_tool(), hist_file);
    }
    else
    { wc.console_write_error_red("No task history found. Add/work with some tasks first."); }
}

/**
 * @brief Simple function to dump all of the Task group names to console for easy extraction.
 */
void wibble::WibbleTask::dump_group_names()
{
    for(auto const& [fn, t_group]: task_groups) { wc.console_write(t_group.group_name, false); }
}

/**
 * @brief Public accessor to list all completed tasks.
 */
void wibble::WibbleTask::list_completed()
{
    _accumulate_closed_tasks();
    // gather all the group tasks
    _accumulate_closed_task_groups();
    _select_group_from_list("", true, true);
    _list_completed_tasks();
}

/**
 * @brief Public accessor to list all completed tasks, but with filters in place.
 * @param cat_fltr optional string filter to substring match on category names
 * @param title_fltr optional string filter to substring match on task entry titles/todos
 */
void wibble::WibbleTask::list_tasks(const std::string& cat_fltr, const std::string& title_fltr)
{
    std::string cat_filter = cat_fltr;
    wibble::WibbleRecord::str_to_lowercase(cat_filter);
    std::string title_filter = title_fltr;
    wibble::WibbleRecord::str_to_lowercase(title_filter);
    _select_group_from_list("", true, false);
    _list_open_tasks(cat_filter, title_filter);
}

/**
 * @brief Interactive wrapper function to list tasks in a closed group.
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::list_tasks_in_closed_group(const pkg_options& OPTS)
{
    _accumulate_closed_task_groups();
    task_group tg = _select_group_from_list("", false, true);
    task_entry e = _list_tasks_in_group(true, tg, true);
    if(e.task_id != "___NULL___") { _present_task_action_menu(e, OPTS, true); }
}


wibble::tg_set wibble::WibbleTask::export_task_group()
{
    task_group_set tg_set;
    tg_set.tg = _select_group_from_list();
    wc.console_write("");
    std::pair<tle,tle> tasks = _get_tasks_for_id_list(tg_set.tg.nodes);
    tg_set.tasks = tasks;
    return tg_set;
}

/**
 * @brief Interactive function to list tasks in a selected open or closed group.
 * @param OPTS pkg_options environment settings/configuration struct
 */
void wibble::WibbleTask::list_tasks_in_group(const pkg_options& OPTS)
{
    task_group tg;
    task_entry e;
    e.task_id = "start";

    tg = _select_group_from_list();
    // setup a loop so user can keep adding/working with tasks
    while(e.task_id != "___NULL___")
    {
        if(tg.fn != "___NULL___")
        {
            e = _list_tasks_in_group(true, tg);
            if(e.task_id != "___NULL___" && e.task_id != "ADD") { _present_task_action_menu(e, OPTS, false); }

            if(e.task_id != "___NULL___")
            {
                closed_tasks.clear();
                open_tasks.clear();
                _gather_tasks(true);
                tg = _select_group_from_list(tg.group_name, false, false);
            }
            else { tg.fn = "___NULL___"; }
        }
        else { e.task_id = "___NULL___"; }
    }
}

/**
 * @brief Interactive wrapper function to select a specifc task entry from a presented numerical list.
 */
wibble::te wibble::WibbleTask::pick_task_entry()
{
    task_group tg = _select_group_from_list();
    if(tg.fn != "___NULL___")
    {
        return _list_tasks_in_group(true, tg);
    }
    else { return task_entry(); }
}

/**
 * @brief Public wrapper to dump entire task status to screen: all completed tasks, all open tasks.
 */
void wibble::WibbleTask::show_everything()
{
    wc.console_write_red_bold("\n\n━━━━━━━━━━━━━━━━━ [ COMPLETED TASKS ] ━━━━━━━━━━━━━━━━━\n\n");
    _accumulate_closed_tasks();
    // gather all the group tasks
    _accumulate_closed_task_groups();
    _select_group_from_list("", true, true);
    _list_completed_tasks();
    wc.console_write_red_bold("\n\n━━━━━━━━━━━━━━━━━━━━ [ OPEN TASKS ] ━━━━━━━━━━━━━━━━━━━\n\n");
    _select_group_from_list("", true, false);
    _list_open_tasks();
    wc.console_write_hz_heavy_divider();
    wc.console_write_success_green(std::to_string(tot_completed_tasks) + " completed tasks.");
    wc.console_write_hz_heavy_divider();
}

/**
 * @brief Function that selects a specific task for a given ID.
 * @param task_id string with unique task entry hash/identifier
 * @param category string with exact category name
 * @param OPTS pkg_options environment settings/configuration struct
 */
bool wibble::WibbleTask::select_task_for_id(const std::string& task_id, const std::string& category, const pkg_options& OPTS)
{
    task_entry e = _parse_task_file_for_single_id(task_id, category);
    if(e.task_id != "___NULL___")  { _present_task_action_menu(e, OPTS, false); return true;  }
    else                           { return false;                                            }
}

