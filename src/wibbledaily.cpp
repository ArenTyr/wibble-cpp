/**
 * @file      wibbledaily.cpp
 * @brief     Provides functionality for setting up numbered "daily" scripts.
 * @details
 *
 * This class provides functionality to allow the user to setup convenient
 * scripts, automatically incremented/numbered, for typical routine jobs.
 * Rather like a semi-manual crontab. Individual or all scripts can be run;
 * designed to be used for recurrent reminders, or deliberately triggering
 * perodic events to terminal etc.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "external/rang.hpp"

#include "wibbledaily.hpp"
#include "wibbleexit.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Ensure "daily" directory exists as subdirectory under Wibble scripts dir.
 * @param wc Reference to WibbleConsole utility object
 * @param scripts_dir String providing path to root Wibble scripts directory
 */
bool wibble::WibbleDaily::_create_daily_dir_if_needed(WibbleConsole& wc, const std::string& scripts_dir)
{
    if(! std::filesystem::exists(std::filesystem::path(scripts_dir + "/daily")))
    {
        wc.console_write_yellow("No daily/ directory found under " + scripts_dir + ". Create it?\n");
        std::string resp = wc.console_write_prompt("(y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); return false; }
        else if( ! wibble::WibbleIO::create_path(scripts_dir + "/daily"))
        { wc.console_write_error_red("Error creating directory."); return false;  }

        return true;
    }
    else { return true; }

}

/**
 * @brief Prompt to open/edit daily script using either CLI or terminal editor.
 * @param wc Reference to WibbleConsole utility object
 * @param sf String providing path to script file to edit
 * @param padded String with numeric padded digits as part of the filename numbering system
 * @param cli Reference to CLI editor command
 * @param gui Reference to GUI editor command
 */
void wibble::WibbleDaily::_open_script_with_editor_prompt(WibbleConsole& wc, const std::string& sf, const std::string& padded,
                                                          const std::string& cli, const std::string& gui)
{
    wc.console_write_header("EDIT " + padded);
    wc.console_write("1. Edit daily script " + padded + " with CLI editor.");
    wc.console_write("2. Edit daily script " + padded + " with GUI editor.");
    wc.console_write_hz_divider();
    std::string option = wc.console_write_prompt("Select option > ", "", "");

    if(option == "1")      { std::system((cli + " " + sf).c_str()); }
    else if(option == "2") { std::system((gui + " " + sf).c_str()); }
    else { wc.console_write_error_red("Invalid choice. Exiting.");  }
}

/**
 * @brief Read in all daily script files from their specific directory
 * @param daily_dir String with path to daily script directory
 */
std::vector<std::string> wibble::WibbleDaily::_get_scripts(const std::string& daily_dir)
{
    std::vector<std::string> scripts;
    for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{daily_dir})
    {
        if(dir_entry.is_regular_file()) { scripts.push_back(dir_entry.path().filename()); }
    }
    std::sort(scripts.begin(), scripts.end());
    return scripts;
}

/**
 * @brief Helper function to identify lowest number (including gaps) for use with script filename.
 * @param scripts Vector of strings containing current existing script filenames
 */
unsigned long wibble::WibbleDaily::_find_numeric_gap(std::vector<std::string>& scripts)
{
    unsigned long counter = 0;
    unsigned long index = 0;
    const std::string DELIM = "_";
    for(auto const& entry: scripts)
    {
        if(entry.find(DELIM) != std::string::npos)
        {
            ++counter;
            std::string num_stub = entry.substr(0, entry.find(DELIM));
            //std::cout << "DEBUG: " << num_stub << std::endl;
            index = std::stol(num_stub);
            if(counter < index)
            {
                //std::cout << "counter = " << counter << std::endl;
                break;
            }
        }
    }
    if(counter == 1 && scripts.size() > 1) { return 1;           }
    else if(counter == scripts.size())     { return counter + 1; }
    else                                   { return counter;     }
}

/**
 * @brief Add a new daily script file.
 * @param OPTS package/general configuration options struct
 * @param wc Reference to WibbleConsole utility object
 */
void wibble::WibbleDaily::add_daily(pkg_options& OPTS, WibbleConsole& wc)
{
    std::string title = wc.console_write_prompt("Add brief title/description: ", "", "");
    wc.trim(title);
    if(title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); }
    else
    {
        const std::string daily_dir = OPTS.paths.scripts_dir + "/daily";
        try
        {
            // switch to signal
            bool exit_cmd = false;
            // ensure we're setup
            exit_cmd = ! _create_daily_dir_if_needed(wc, OPTS.paths.scripts_dir);

            if(! exit_cmd)
            {
                unsigned long file_index = 1;
                std::vector<std::string> scripts = _get_scripts(daily_dir);
                if(! scripts.empty()) { file_index = _find_numeric_gap(scripts); }

                // https://stackoverflow.com/questions/6143824/add-leading-zeroes-to-string-without-sprintf
                auto padded = std::to_string(file_index); padded.insert(0, 4U - std::min(std::string::size_type(4), padded.length()), '0');
                std::string fo = "#!/bin/bash\n";
                fo.append("# WIBBLE_DESC: (" + padded + "_daily) - " + title + "\n");
                fo.append("# (NOTE: Leave above WIBBLE_DESC line intact for description to show in wibble daily list)\n");
                fo.append("# ---------------------------------------------------\n\n");
                fo.append("echo 'Hello from " + padded + "_daily'\n");
                const std::string fn = daily_dir + "/" + padded + "_" + "daily";

                if(std::filesystem::exists(std::filesystem::path(fn))) { wc.console_write_error_red("Error: script file already exists."); }
                else
                {
                    bool wo = wibble::WibbleIO::write_out_file_overwrite(fo, fn);
                    if(wo)
                    {
                        wc.console_write_success_green("New daily script file written successfully.");
                        _open_script_with_editor_prompt(wc, fn, padded, OPTS.exec.cli_editor, OPTS.exec.gui_editor);
                        std::system(("chmod u+x " + fn).c_str());
                        wc.console_write_green(ARW + "Note/warning: Executable bit now set on script file " + fn + "\n");
                    }
                    else { wc.console_write_error_red("Error - unable to create output file: " + fn); }
                }
            }
        }
        catch(std::exception& ex)
        {
            std::cerr << "Error in add_daily(): " << ex.what() << std::endl;
        }

    }

}

/**
 * @brief List existing daily script files.
 * @param OPTS package/general configuration options struct
 * @param wc Reference to WibbleConsole utility object
 */
std::vector<std::string> wibble::WibbleDaily::ls_daily(pkg_options& OPTS, WibbleConsole& wc)
{
    bool scripts_dir_valid = _create_daily_dir_if_needed(wc, OPTS.paths.scripts_dir);
    if(! scripts_dir_valid) { throw wibble::wibble_exit(0); }
    const std::string daily_dir = OPTS.paths.scripts_dir + "/daily";
    std::vector<std::string> scripts = _get_scripts(daily_dir);
    int counter = 0;
    for(auto const& script: scripts)
    {
        if(ENABLECOLOUR)
        {
            std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
            std::cout << std::setfill('0') << std::setw(std::to_string(scripts.size()).length());
            std::cout << ++counter << "]" << rang::style::reset << " ┃ ";
        }
        else
        {
            std::cout << "[" << std::setfill('0') << std::setw(std::to_string(scripts.size()).length());
            std::cout << ++counter << "]" << " ┃ ";
        }

        std::string name = script + ": ";
        std::string title = wibble::WibbleUtility::find_file_wibble_desc(daily_dir + "/" + script);

        wc.console_write_yellow(name);
        wc.console_write(title);
    }

    if(scripts.size() == 0) { wc.console_write_red(ARW + "No daily scripts present. Add one with '--add'.\n"); }
    return scripts;
}

/**
 * @brief List existing daily script files.
 * @param OPTS package/general configuration options struct
 * @param wc Reference to WibbleConsole utility object
 * @param do_delete Flag controlling option to delete rather than edit
 */
void wibble::WibbleDaily::edit_or_del_daily(pkg_options& OPTS, WibbleConsole& wc, const bool do_delete)
{
    const std::string daily_dir = OPTS.paths.scripts_dir + "/daily";
    bool exit_cmd = false;
    // ensure we're setup
    if(! std::filesystem::exists(std::filesystem::path(daily_dir)))
    {
        wc.console_write_yellow("No daily/ directory found under " + OPTS.paths.scripts_dir + ". Nothing to delete.");
        exit_cmd = true;
    }

    if(! exit_cmd)
    {
        std::string resp;
        auto scripts = ls_daily(OPTS, wc);
        if(do_delete) { resp = wc.console_write_prompt("\nInput number of entry you wish to delete\n> ", "", ""); }
        else          { resp = wc.console_write_prompt("\nInput number of entry you wish to edit\n> ", "", ""); }
        if(resp != "")
        {
            try
            {
                unsigned long selection = std::stol(resp);
                if(selection < 1 || selection > scripts.size()) { throw std::invalid_argument("Outside range"); }
                std::string script_name = scripts[selection - 1];
                const std::string script_fn = daily_dir + "/" + script_name;
                if(do_delete)
                {
                    wc.console_write_red("\nPending deletion:\n\n" + script_name + "\n\n");
                    resp = wc.console_write_prompt("Input 'delete' if you wish to delete the above entry\n> ", "", "");
                    if(resp == "delete")
                    {
                        if(std::filesystem::exists(std::filesystem::path(script_fn)))
                        { std::filesystem::remove(std::filesystem::path(script_fn)); }
                        wc.console_write_success_green("Script entry " + script_name + " deleted.");
                    }
                    else { wc.console_write_error_red("User aborted."); }
                }
                else
                {
                    std::string padded = script_name.substr(0, script_name.find("_"));
                    _open_script_with_editor_prompt(wc, script_fn, padded, OPTS.exec.cli_editor, OPTS.exec.gui_editor);
                }
            }
            catch(std::exception& ex)
            {
                wc.console_write_error_red("Invalid input.");
            }
        }
        else { wc.console_write_error_red("User aborted."); }
    }
}

/**
 * @brief Execute a given daily script, or all if passed "a"/"A"/"all".
 * @param OPTS package/general configuration options struct
 * @param wc Reference to WibbleConsole utility object
 * @param exec_num String containing number of script to execute, or letter a/A/"all" to run all
 */
void wibble::WibbleDaily::exec_script(pkg_options& OPTS, WibbleConsole& wc, const std::string& exec_num)
{
    try
    {
        const std::string daily_dir = OPTS.paths.scripts_dir + "/daily";
        std::vector<std::string> scripts = _get_scripts(daily_dir);

        if(exec_num == "a" || exec_num == "A" || exec_num == "all")
        {
            wc.console_write_success_green("Executing ALL daily scripts");
            for(auto const& script: scripts)
            {
                const std::string script_to_exec = daily_dir + "/" + script;
                std::system(script_to_exec.c_str());
            }
        }
        else if(exec_num != "")
        {
            unsigned long script_num = std::stol(exec_num);
            if(script_num >= 1 && script_num <= scripts.size())
            {
                wc.console_write_success_green("Executing daily script " + std::to_string(script_num));
                const std::string script_to_exec = daily_dir + "/" + scripts[script_num - 1];
                std::system(script_to_exec.c_str());
            }
            else { throw std::invalid_argument("Outside range"); }

        }
        else
        {
            ls_daily(OPTS, wc);
            std::string resp = wc.console_write_prompt("Input number of script you wish to execute > ", "", "");
            unsigned long script_num = std::stol(resp);
            if(script_num >= 1 && script_num <= scripts.size())
            {
                wc.console_write_success_green("Executing daily script " + std::to_string(script_num));
                const std::string script_to_exec = daily_dir + "/" + scripts[script_num - 1];
                std::system(script_to_exec.c_str());
            }
            else { throw std::invalid_argument("Outside range"); }
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Unable to find a matching script numbered '" + exec_num + "'.");
        wc.console_write_red(ARW + "Available scripts:\n\n");
        ls_daily(OPTS, wc);
    }
}
