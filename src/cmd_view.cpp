/**
 * @file      cmd_view.cpp
 * @brief     Handle "view" subcommand.
 * @details
 *
 * This simple class supplies the view functionality for Wibble Nodes.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_view.hpp"
#include "wibbleutility.hpp"
 
/**
 * @brief Dispatching logic for the "view" operation on Wibble Nodes.
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 * @param VIEW command line options struct
 */
void wibble::WibbleCmdView::handle_view(pkg_options& OPTS, search_filters& SEARCH, view_options& VIEW)
{
    WibbleConsole wc; WibbleExecutor wib_e(OPTS); WibbleRecord w;
    w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH); 
    bool view_result = wib_e.note_pipe_through_view_tool(w, SEARCH.archive_db, VIEW.render);
    if(! view_result) { wc.console_write_error_red("Error viewing note."); }
} 
