/**
 * @file      cmd_init.cpp
 * @brief     Handle "init" subcommand.
 * @details
 *
 * Critical command used to setup the core Wibble++ environment, and
 * assist the user in creating an initial configuration file (TOML format),
 * saving them from writing it from scratch. Also ensures presence of
 * ripgrep, which is a mandatory tool that Wibble++ needs in order to
 * operate.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_init.hpp"
#include "wibbleexit.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Determine location for output file; check we're not clobbering existing config file.
 * @param wc Reference to WibbleConsole utility object
 * @param opts init_options struct that contains all configuration settings
 */
std::string wibble::WibbleCmdInit::_set_output_filename(WibbleConsole& wc, init_options opts)
{
    std::string DEFAULT_CONFIG = "~/.config/wibble/wibble.toml";
    wibble::WibbleUtility::expand_tilde(DEFAULT_CONFIG);
    std::string output_file;

    // determine whether we're creating a fresh default config, or writing out to a custom filename
    if(opts.custom_init_file == "UNSET")
    {
        if(std::filesystem::exists(std::filesystem::path(DEFAULT_CONFIG)))
        {
            wc.console_write_error_red("A configuration file already exists at: " + DEFAULT_CONFIG);
            wc.console_write_red("1. Rename, delete, or move the file if you want to run this command and generate a fresh configuration.\n");
            wc.console_write_red("2. Alternatively, run 'init' command with --custom-config path/to/new/config.toml to write configuration out a to a custom filename.\n");
            throw wibble::wibble_exit(1);
        }
        output_file = DEFAULT_CONFIG;
    }
    else
    { 
        if(std::filesystem::exists(std::filesystem::path(opts.custom_init_file)))
        {
            wc.console_write_error_red("An existing file is present at: " + opts.custom_init_file + ". Aborting.");
            throw wibble::wibble_exit(1);
        }
        output_file = opts.custom_init_file;
    }

    return output_file;
}

/**
 * @brief Ensure presence of installed/working ripgrep/rg command.
 * @param wc Reference to WibbleConsole utility object
 */
void wibble::WibbleCmdInit::_check_for_ripgrep_cmd(WibbleConsole& wc)
{
    // ensure ripgrep is installed, Wibble++ is useless without it...
    wc.console_write_green(ARW + "Checking for operational 'ripgrep' command in PATH...\n");
    int rg_present = std::system("echo 'hello ripgrep' | rg 'hello ripgrep' > /dev/null");
    if(rg_present != 0)
    {
        wc.console_write_error_red("Ripgrep command ('rg') not found on PATH. Is ripgrep installed?");
        throw wibble::wibble_exit(1);
    }
    else { wc.console_write_success_green("Working ripgrep installation found."); }
}

/**
 * @brief Simple wrapper to ensure file paths are clean and expanded (~/foo to /home/user/foo).
 * @param wc Reference to WibbleConsole utility object
 * @param answer User input/string containing file path
 */
void wibble::WibbleCmdInit::_clean_response(WibbleConsole& wc, std::string& answer)
{
    wibble::WibbleUtility::expand_tilde(answer);
    wc.trim(answer);
}

/**
 * @brief Display confirmation of user's inputted settings before proceeding.
 *
 * Contains various sensible default values to avoid unnecessary additional
 * questions. Expert users can manually edit the generated file to replace 
 * any values with custom choices, if they know/understand exactly what they're
 * doing.
 * 
 * @param wc Reference to WibbleConsole utility object
 * @param answers init_options struct that contains all configuration settings
 */
void wibble::WibbleCmdInit::_print_confirm_config(WibbleConsole& wc, init_settings& answers)
{
    std::string cache_dir = "~/.cache/wibble";
    _clean_response(wc, cache_dir);

    wc.console_write("");
    wc.console_write_header("GENERATED");
    wc.console_write_yellow_bold("Base directories & database files\n");
    wc.console_write_hz_divider();
    wc.console_write_green("Main Wibble directory    : "); std::cout << answers.main_wibble_dir << '\n';
    wc.console_write_green("Backup Wibble directory  : "); std::cout << answers.backup_wibble_dir << '\n';
    wc.console_write_green("Autofile directory       : "); std::cout << answers.autofile_dir << '\n';
    wc.console_write_green("Wibble database directory: "); std::cout << answers.main_wibble_dir << "/db" << '\n';
    wc.console_write_green("Main Wibble database     : "); std::cout << answers.main_wibble_dir << "/db/main.wib.db" << '\n';
    wc.console_write_green("Archived Wibble database : "); std::cout << answers.main_wibble_dir << "/db/ark.wib.db" << '\n';
    wc.console_write_hz_divider();
    wc.console_write_yellow_bold("Core directories\n");
    wc.console_write_hz_divider();
    wc.console_write_green("Bookmarks directory      : "); std::cout << answers.main_wibble_dir << "/bookmarks" << '\n';
    wc.console_write_green("Drawers directory        : "); std::cout << answers.main_wibble_dir << "/drawers" << '\n';
    wc.console_write_green("Jotfile directory        : "); std::cout << answers.main_wibble_dir << "/jotfiles" <<'\n';
    wc.console_write_green("Schemas directory        : "); std::cout << answers.main_wibble_dir << "/schemas" << '\n';
    wc.console_write_green("Scripts directory        : "); std::cout << answers.main_wibble_dir << "/scripts" << '\n';
    wc.console_write_green("Tasks directory          : "); std::cout << answers.main_wibble_dir << "/tasks" << '\n';
    wc.console_write_green("Templates directory      : "); std::cout << answers.main_wibble_dir << "/templates" << '\n';
    wc.console_write_green("Temporary/cache directory: "); std::cout << cache_dir << '\n';
    wc.console_write_green("Topics directory         : "); std::cout << answers.main_wibble_dir << "/topics" << '\n';
    wc.console_write_hz_divider();
    wc.console_write_yellow_bold("Executable commands\n");
    wc.console_write_hz_divider();
    wc.console_write_green("Editor command           : "); std::cout << answers.cli_editor << '\n';
    wc.console_write_green("Dump tool command        : "); std::cout << answers.dump_cmd << '\n';
    wc.console_write_green("GUI editor command       : "); std::cout << answers.gui_editor << '\n';
    wc.console_write_green("File manager command     : "); std::cout << answers.fm_cmd << '\n';
    wc.console_write_green("Pager command            : "); std::cout << answers.pager_cmd << '\n';
    wc.console_write_green("View tool command        : "); std::cout << answers.view_cmd << '\n';
    wc.console_write_green("XDG open command         : "); std::cout << "xdg-open" << '\n';
    wc.console_write_hz_heavy_divider();
    wc.console_write("");
    std::string confirm = wc.console_write_prompt("Create configuration file with above settings? (y/n)", ">", "");
    _clean_response(wc, confirm);
    if(confirm != "y" && confirm != "Y") { wc.console_write_error_red("User aborted."); throw wibble::wibble_exit(0); }
}

/**
 * @brief Get user's choices for programs and storage location for Wibble.
 *
 * Not all configuration values have a question; some sensible default values
 * are used to avoid burdensome volume of questions. For programs, fairly
 * typical Linux choices are preinput (editable), to suggest to the user what value
 * might be sensible. Since there is no one "standard" Linux desktop, aim is
 * simply to offer a guide as to typical programs/locations to use.
 *
 * Expert users can manually edit the generated file to replace 
 * all configuration keys with custom values, if they know/understand exactly
 * what they're doing.
 * 
 * @param wc Reference to WibbleConsole utility object
 * @param answers init_options struct that contains all configuration settings
 */
wibble::WibbleCmdInit::init_settings wibble::WibbleCmdInit::_get_user_options(WibbleConsole& wc)
{
    init_settings answers;

    try
    {
        wc.console_write_hz_divider();
        wc.console_write_yellow("NOTE: "); wc.console_write("'Backup' directory (question 2) should be OUTSIDE root Wibble directory.");
        wc.console_write_hz_divider(); wc.console_write("");
        wc.console_write_header("CONFIG");
        answers.main_wibble_dir   = wc.console_write_prompt("[ 1/10] Input root Wibble directory  : ", "", "~/Documents/wibble");
        answers.backup_wibble_dir = wc.console_write_prompt("[ 2/10] Input backup Wibble directory: ", "", "~/Documents/backup/wibble");
        answers.autofile_dir      = wc.console_write_prompt("[ 3/10] Input autofile directory     : ", "", "~/Documents/autofile");
        answers.cli_editor        = wc.console_write_prompt("[ 4/10] Command line editor command  : ", "", "vim");
        answers.gui_editor        = wc.console_write_prompt("[ 5/10] GUI editor                   : ", "", "emacs");
        answers.fm_cmd            = wc.console_write_prompt("[ 6/10] File manager                 : ", "", "thunar");
        answers.ark_cmd           = wc.console_write_prompt("[ 7/10] Archive tool                 : ", "", "xarchiver");
        answers.dump_cmd          = wc.console_write_prompt("[ 8/10] Dump tool                    : ", "", "cat");
        answers.pager_cmd         = wc.console_write_prompt("[ 9/10] Pager command                : ", "", "bat");
        answers.view_cmd          = wc.console_write_prompt("[10/10] View tool                    : ", "", "gxmessage");
        wc.console_write_hz_divider();

        // trim answers and expand tilde symbol
        _clean_response(wc, answers.main_wibble_dir);
        _clean_response(wc, answers.backup_wibble_dir);
        _clean_response(wc, answers.autofile_dir);
        _clean_response(wc, answers.cli_editor);
        _clean_response(wc, answers.gui_editor);
        _clean_response(wc, answers.fm_cmd);
        _clean_response(wc, answers.ark_cmd);
        _clean_response(wc, answers.dump_cmd);
        _clean_response(wc, answers.pager_cmd);
        _clean_response(wc, answers.view_cmd);

        if(answers.main_wibble_dir.length() < 1)
        { wc.console_write_error_red("A valid Wibble root directory must be specified.");                                throw wibble::wibble_exit(1); }
        if(answers.backup_wibble_dir.length() < 1)
        { wc.console_write_error_red("A valid Wibble backup directory must be specified.");                              throw wibble::wibble_exit(1); }
        if(answers.autofile_dir.length() < 1)
        { wc.console_write_error_red("A valid autofile directory must be specified.");                                   throw wibble::wibble_exit(1); }
        if(answers.cli_editor.length() < 1)
        { wc.console_write_error_red("A valid command line editor must be specified.");                                  throw wibble::wibble_exit(1); }
        if(answers.gui_editor.length() < 1)
        { wc.console_write_error_red("A valid GUI editor must be specified.");                                           throw wibble::wibble_exit(1); }
        if(answers.fm_cmd.length() < 1)
        { wc.console_write_error_red("A valid Wibble file manager command must be specified.");                          throw wibble::wibble_exit(1); }
        if(answers.ark_cmd.length() < 1)
        { wc.console_write_error_red("A valid Wibble archive manager command must be specified.");                       throw wibble::wibble_exit(1); }
        if(answers.dump_cmd.length() < 1)
        { wc.console_write_error_red("A valid Wibble dump command must be specified.");                                  throw wibble::wibble_exit(1); }
        if(answers.pager_cmd.length() < 1)
        { wc.console_write_error_red("A valid Wibble pager command must be specified (e.g. 'less', 'most'.");            throw wibble::wibble_exit(1); }
        if(answers.view_cmd.length() < 1)
        { wc.console_write_error_red("A valid Wibble viewing tool command must be specified (e.g. 'cat', 'gxmessage'."); throw wibble::wibble_exit(1); }

        _print_confirm_config(wc, answers);
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error generating configuation.");
    }

    return answers;
}

/**
 * @brief Translate all user inputs into an output string for writing to configuration file.
 * @param user_opts init_options struct that contains all configuration settings
 */
std::string wibble::WibbleCmdInit::_serialize_user_opts(init_settings& user_opts)
{
    std::string config = "# Wibble++ configuration. Generated at "; config.append(wibble::WibbleRecord::generate_date_YMD("") + '\n');
    config.append("# Core paths\n");
    config.append("\n");
    config.append("autofile_dir            = \""); config.append(user_opts.autofile_dir);      config.append("\"\n"); 
    config.append("backup_location         = \""); config.append(user_opts.backup_wibble_dir); config.append("\"\n"); 
    config.append("bookmarks_dir           = \""); config.append(user_opts.main_wibble_dir);   config.append("/bookmarks\"\n");
    config.append("drawers_dir             = \""); config.append(user_opts.main_wibble_dir);   config.append("/drawers\"\n");
    config.append("jot_dir                 = \""); config.append(user_opts.main_wibble_dir);   config.append("/jotfiles\"\n"); 
    config.append("schemas_dir             = \""); config.append(user_opts.main_wibble_dir);   config.append("/schemas\"\n"); 
    config.append("scripts_dir             = \""); config.append(user_opts.main_wibble_dir);   config.append("/scripts\"\n"); 
    config.append("tasks_dir               = \""); config.append(user_opts.main_wibble_dir);   config.append("/tasks\"\n"); 
    config.append("templates_dir           = \""); config.append(user_opts.main_wibble_dir);   config.append("/templates\"\n"); 
    config.append("topics_dir              = \""); config.append(user_opts.main_wibble_dir);   config.append("/topics\"\n"); 
    config.append("tmp_dir                 = \""); config.append("~/.cache/wibble");           config.append("\"\n"); 
    config.append("wibble_db_dir           = \""); config.append(user_opts.main_wibble_dir);   config.append("/db\"\n"); 
    config.append("wibble_store            = \""); config.append(user_opts.main_wibble_dir);   config.append("\"\n"); 
    config.append("\n");
    config.append("# Core files\n");
    config.append("archived_db             = \""); config.append(user_opts.main_wibble_dir);   config.append("/db/ark.wib.db\"\n"); 
    config.append("main_db                 = \""); config.append(user_opts.main_wibble_dir);   config.append("/db/main.wib.db\"\n"); 
    config.append("\n");
    config.append("# Core tools\n");
    config.append("cli_editor              = \""); config.append(user_opts.cli_editor);        config.append("\"\n"); 
    config.append("dump_tool               = \""); config.append(user_opts.dump_cmd);          config.append("\"\n"); 
    config.append("fm_tool                 = \""); config.append(user_opts.fm_cmd);            config.append("\"\n"); 
    config.append("gui_editor              = \""); config.append(user_opts.gui_editor);        config.append("\"\n"); 
    config.append("pager                   = \""); config.append(user_opts.pager_cmd);         config.append("\"\n"); 
    config.append("view_tool               = \""); config.append(user_opts.view_cmd);          config.append("\"\n"); 
    config.append("xdg_open_cmd            = \""); config.append("xdg-open");                  config.append("\"\n"); 
    config.append("# Custom binary file support/associations (advanced). See manual.\n");
    config.append("# This value can also be generated using guided prompts by\n");
    config.append("# running 'wibble++ config --gen-bin-mapping'\n");
    config.append("# binary_mapping          = \"\""); 

    return config;
}

/**
 * @brief Main entry point for the "init" configuration file generator.
 * @param user_opts init_options struct that contains all configuration settings
 */
void wibble::WibbleCmdInit::init_config(init_options opts)
{
    WibbleConsole wc;

    std::string output_file = _set_output_filename(wc, opts);
    _check_for_ripgrep_cmd(wc);
    init_settings user_opts = _get_user_options(wc);
    bool write_config = wibble::WibbleIO::write_out_file_overwrite(_serialize_user_opts(user_opts), output_file);

    if(write_config)
    {
        wc.console_write_success_green("Configuration file successfully written out to " + output_file + ".");
    }
    else
    {
        wc.console_write_error_red("Error writing configuration file to " + output_file + "!");
    }
}
