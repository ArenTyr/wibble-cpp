/**
 * @file      wibbletopicmanager.cpp
 * @brief     Various algorithms and functions providing Topics backend.
 * @details
 *
 * This class does all of the work providing the Topic functionality and actions.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>

#include "wibbleexit.hpp"
#include "wibbletopicmanager.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Construct path/filename to a given Topic entry/item.
 * @param wc general console utility object
 * @param topic_base_dir root Topic directory (based on environment)
 * @param topic topic_def structure providing all of the Topic entry metadata
 * @param filename string with (relative/local) filename of Topic entry
 * @param txt_not_data flag indicating type of Topic entry; adjust path accordingly
 */
std::string wibble::WibbleTopicManager::_build_topic_entry_full_filename(WibbleConsole& wc,
                                                                         const std::string& topic_base_dir,
                                                                         topic_def& topic,
                                                                         const std::string& filename, const bool txt_not_data)
{
    std::string full_fn;
    if(txt_not_data) { full_fn = topic_base_dir + "/store/" + topic.category + "/" + topic.id + "/" + filename;      }
    else             { full_fn = topic_base_dir + "/data_store/" + topic.category + "/" + topic.id + "/" + filename; }
    wc.trim(full_fn);
    return full_fn;
}

/**
 * @brief Check whether we have actual Topic entry or placeholder.
 * @param topic topic_def structure providing all of the topic entry metadata
 */
bool wibble::WibbleTopicManager::_check_null_topic(topic_def& topic)
{
    if(topic.name == "___NULL___") { return true;  }
    else                           { return false; }
}

/**
 * @brief Wrapper to use Topic default template file, if it is exists, when adding new topic (text) entry.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object.
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param template_dir string with path to Wibble template directory (environment specific)
 * @param topic_entry_fn string with full path to Topic entry file
 */
void wibble::WibbleTopicManager::_check_use_default_template(WibbleConsole& wc, WibbleTopics& wt, topic_def& topic,
                                                             const std::string& template_dir, const std::string& topic_entry_fn)
{
    const std::string default_template = template_dir + "/" + topic.id;
    if(std::filesystem::exists(std::filesystem::path(default_template)))
    {
        wc.console_write_success_green("Default topic template found for topic ID " + topic.id);
        return _check_use_template(wc, wt, topic, default_template, topic_entry_fn);
    }
}

/**
 * @brief Construct Topic entry using a template file.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param t_mplate_path string with path to template file to use
 * @param topic_entry_fn string with full path to Topic entry file
 */
void wibble::WibbleTopicManager::_check_use_template(WibbleConsole& wc, WibbleTopics& wt, topic_def& topic,
                                                     const std::string t_mplate_path, const std::string& topic_entry_fn)
{
    bool do_copy = wibble::WibbleIO::wcopy_file(t_mplate_path, topic_entry_fn);
    if(do_copy)
    {
        // do not do template substitutions on binary file type...
        std::filesystem::path chk_tf{topic_entry_fn};
        std::string tf_extn = chk_tf.extension();
        // filesystem library includes "." in extension (not sure why they chose that...)
        if(tf_extn.length() >= 2 && tf_extn.at(0) == '.') { tf_extn = tf_extn.substr(1, tf_extn.length() - 1); }
        const std::string bin_edit_cmd = WibbleUtility::get_binary_tool(wc, tf_extn, bin_mapping, true);

        bool do_subs = true;
        if(bin_edit_cmd == "") { do_subs = wt.topic_template_substitutions(topic_entry_fn, topic); }

        if(do_subs) { wc.console_write_success_green("Topic template substitutions completed.");   }
        else        { wc.console_write_error_red("Error executing topic template substitutions."); }
    }
    else  { wc.console_write_error_red("Warning. Unable to copy template file."); }
}


/**
 * @brief Interactive confirmation prompt for migrating Topic entry.
 * @param wc general console utility object
 * @param existing_fn string with existing path to current Topic entry
 * @param src_topic_name string with name of existing Topic entry is part of
 * @param dest_topic_name string with name of proposed Topic to migrate entry into
 */
void wibble::WibbleTopicManager::_confirm_proceed_migration_or_exit(WibbleConsole&wc, const std::string& existing_fn,
                                        const std::string& src_topic_name, const std::string& dest_topic_name)
{
    wc.console_write_red("Confirm migration of '" + existing_fn + "' from topic " +
                         src_topic_name + " to " + dest_topic_name + ".\n");
    std::string resp = wc.console_write_prompt("Move/migrate topic entry? (y/n) > ", "", "");
    if(resp != "y" && resp != "Y")
    {
        wc.console_write_error_red("User aborted.");
        throw wibble::wibble_exit(0);
    }
}

/**
 * @brief Standardised adjustment of raw string into Topic category : name pair for use as qualified Topic name/identifer
 * @param topic_name string with raw input value for topic name
 */
void wibble::WibbleTopicManager::_expand_topic_name(std::string& topic_name)
{
    try
    {
        if(topic_name != "")
        {
            std::string category = topic_name.substr(0, topic_name.find(":"));
            size_t index = topic_name.find(":");
            if(index == std::string::npos)
            {
                wibble::WibbleRecord::str_to_uppercase(topic_name);
                topic_name = "general:" + topic_name;
            }
            else
            {
                wibble::WibbleRecord::str_to_lowercase(category);
                std::string topic = topic_name.substr(index + 1, std::string::npos);
                wibble::WibbleRecord::str_to_uppercase(topic);

                topic_name = category + ":" + topic;
            }
        }
    }
    catch(std:: out_of_range& ex) { }
}

/**
 * @brief Warning/confirmation when migrating Topic entry across Topics with different configure filetypes/extensions
 * @param src_topic_type string with existing Topic filetype/extension
 * @param dest_topic_type string with destination Topic filetype/extension
 */
void wibble::WibbleTopicManager::_filetype_migration_confirm_or_exit(WibbleConsole& wc, const std::string& src_topic_type, const std::string& dest_topic_type)
{
    wc.console_write_red("WARNING. Destination topic has different filetype from source filetype.\n\n");
    wc.console_write_red("Source topic filetype     : " + src_topic_type + "\n");
    wc.console_write_red("Destination topic filetype: " + dest_topic_type + "\n\n");
    wc.console_write_red_bold("Still proceed with migration?\n\n");
    std::string resp = wc.console_write_prompt("Proceed? (y/n) > ", "", "");
    if(resp != "y" && resp != "Y")
    {
        wc.console_write_error_red("User aborted.");
        throw wibble::wibble_exit(0);
    }
}


/**
 * @brief Generate a new temporary Topic index when applying a filter across Topic entry titles
 * @param wc general console utility object
 * @param topic_base_dr string with path to configured Topic root directory
 * @param tmp_dir string with path to Wibble cache/temporary directory
 * @param topic_id string with unique Topic entry hash/identifier
 * @param filter_expression string containing substring pattern to match against
 * @param txt_not_data flag indicating type of Topic entry; text or data
 */
std::string wibble::WibbleTopicManager::_filter_topic_tmp_index(WibbleConsole& wc, const std::string& topic_base_dir,
                                                                const std::string tmp_dir, const std::string topic_id,
                                                                const std::string& filter_expression, const bool txt_not_data)
{
    wc.console_write_green(ARW + "Active filter in effect: "); wc.console_write_yellow(filter_expression + '\n');
    std::string topic_index = topic_base_dir + "/topic_indexes/" + topic_id;
    if(txt_not_data) { topic_index.append(".wib");      }
    else             { topic_index.append(".data.wib"); }
    std::string rg_cmd = "rg -iF \"" + filter_expression + "\" \"" + topic_index + "\" > " + tmp_dir + "/wibble-workfile";
    //std::cerr << "DEBUG: built rg cmd: " << rg_cmd << std::endl;
    bool run_rg = std::system(rg_cmd.c_str());
    if(run_rg == 0) { topic_index = tmp_dir + "/wibble-workfile"; }
    else            { topic_index = "";                           }

    return topic_index;
}

/**
 * @brief Function to select the specified Topic (based on qualified name/topic ID), or exit
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object.
 * @param topic_name string with unique Topic identifier/topic name
 */
wibble::topic_def wibble::WibbleTopicManager::_get_topic_or_exit(WibbleConsole& wc, WibbleTopics& wt, std::string& topic_name)
{
    wibble::WibbleRecord::str_to_uppercase(topic_name);
    _expand_topic_name(topic_name);

    topic_def topic = wt.get_topic_for_name(topic_name);
    if(_check_null_topic(topic))
    {
        wc.console_write_error_red("Unable to find any matching topic named '" + topic_name + "'");
        wc.console_write_green(ARW + "Listing available topics.\n");
        wt.list_available_topics(wc);
        throw wibble::wibble_exit(1);
    }

    return topic;
}

/**
 * @brief Function to parse template specification and set/check specified template file exists for use, or exit
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param t_mplate_ref string with template specification (e.g. 'org:expenses', 'txt:todo_list')
 * @param t_mplate_dir string with path to configured template directory
 */
std::string wibble::WibbleTopicManager::_get_template_path_or_exit(WibbleConsole& wc, WibbleExecutor& wib_e,
                                                                   const std::string& t_mplate_ref, const std::string& t_mplate_dir)
{
        std::string t_mplate;
        std::string ft;
        wib_e._set_template_ft_and_name(t_mplate_ref, ft, t_mplate);
        std::string t_mplate_path = t_mplate_dir + "/" + ft + "/" + t_mplate;
        if(t_mplate == "" || ft == "")
        {
            wc.console_write_error_red("Template filetype and/or name cannot be blank. Template: '" + t_mplate + "'. Filetype: '" + ft + "'.");
            wc.console_write_error_red("Check your template path/specification. Aborting.");
            throw wibble::wibble_exit(1);
        }

        if(! std::filesystem::exists(std::filesystem::path(t_mplate_path)))
        {
            wc.console_write_error_red("Unable to find a template named '" + t_mplate + "'.");
            wc.console_write_error_red("Source templates from templates directory via <filetype>:<template name> syntax.");
            wc.console_write_error_red("e.g. txt:my_template references '<template dir>/txt/my_template'.");
            wc.console_write_error_red("Check your template path/specification. Aborting.");
            throw wibble::wibble_exit(1);
        }

        return t_mplate_path;
}

/**
 * @brief Core dispatcher/logic for filtering and running command on resultant Topic entry, or set of entries
 *
 * Logic here is quite complex due to many possible pipeline routes (giving great power to the user):
 * 1. no filter -> no ripgrep -> default exec command on singular entry (dump)
 * 2. no filter -> no ripgrep -> specified standard command on singular entry (view, edit, etc.)
 * 3. no filter -> no ripgrep -> specified custom command on singular entry (e.g. "ls -lh")
 * 4. no filter -> no ripgrep -> specified script command on singular entry (e.g. "ls -lh")
 * 5. no filter -> no ripgrep -> specified custom command on batch set of entries (potentially all)
 * 6. no filter -> no ripgrep -> specified script command on batch set of entries (potentially all)
 * ... and permutations on above chain, e.g.
 * m. no filter -> ripgrep -> specified custom command on batch set of entries
 * ...
 * n. filter -> ripgrep -> specified script command on batch set of entries
 * ...
 * filter can be on or off, ripgrep on or off, and batch mode on or off, before passing the result set
 * through to the matching standard command, custom command, or custom script
 *
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param wt WibbleTopics Topic data object
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param topic_name string with unique Topic identifier/topic name
 */
void wibble::WibbleTopicManager::_op_on_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleConsole& wc,
                                                    WibbleExecutor& wib_e, WibbleTopics& wt, topic_def& topic,
                                                    const std::string& topic_name)
{

    const std::string bin_edit = WibbleUtility::get_binary_tool(wc, topic.type, OPTS.exec.bin_mapping, true);
    const std::string bin_view = WibbleUtility::get_binary_tool(wc, topic.type, OPTS.exec.bin_mapping, false);

    wc.console_write_success_green("Working with topic '" + topic_name + "'"); wc.console_write("");

    std::string topic_index;
    // --filter
    if(TOPIC.filter_topic_entry != "UNSET")
    {
        topic_index = _filter_topic_tmp_index(wc, OPTS.paths.topics_dir, OPTS.paths.tmp_dir,
                                              topic.id, TOPIC.filter_topic_entry, true);
    }
    else { topic_index = OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".wib"; }

    bool batch_exec = false;
    bool run_std_cmd = false;
    bool run_scr_cmd = false;
    bool rg_results = false;
    std::string exec_script;

    // check whether we're running a batch operation or not
    if(TOPIC.batch_exec) { batch_exec = true; }

    // check whether we're using specified command or something within scripts/
    if(TOPIC.exec_cmd_on_topic_entries != "UNSET")             { run_std_cmd = true; }
    else if(TOPIC.exec_script_cmd_on_topic_entries != "UNSET") { run_scr_cmd = true; }

    // returns false if no specified exec or script cmd command line option passed
    bool do_run = wibble::WibbleUtility::check_prompt_set_exec_cmds_scripts(wc, OPTS.paths.scripts_dir,
                                                                            run_std_cmd, run_scr_cmd, TOPIC.exec_cmd_on_topic_entries,
                                                                            TOPIC.exec_script_cmd_on_topic_entries);

    WibbleTopics::rg_matches rg_set;
    if(TOPIC.grep_topic_entry != "UNSET")
    {
        rg_set = _generate_ripgrep_result_index(wc, wt, OPTS.paths.topics_dir, OPTS.paths.tmp_dir,
                                                topic_index, topic, TOPIC.grep_topic_entry);
        rg_results = true;
    }

    // setup switching logic/state mapping
    /**
     * @brief Execution state enumeration; setup switching logic/state mapping.
     */
    enum exec_op
    {
        BATCH_EXEC_STD_CMD_NO_GREP,  //!< Batch execution across set of entries, standard command, no ripgrep filtering
        BATCH_EXEC_STD_CMD_GREP,     //!< Batch execution across set of entries, standard command, with ripgrep filtering
        BATCH_EXEC_SCR_CMD_NO_GREP,  //!< Batch execution across set of entries, script command, no ripgrep filtering
        BATCH_EXEC_SCR_CMD_GREP,     //!< Batch execution across set of entries, script command, with ripgrep filtering
        SINGLE_EXEC_STD_CMD_NO_GREP, //!< Single execution across single entry, standard command, no ripgrep filtering
        SINGLE_EXEC_STD_CMD_GREP,    //!< Single execution across single entry, standard command, with ripgrep filtering
        SINGLE_EXEC_SCR_CMD_NO_GREP, //!< Single execution across single entry, script command, no ripgrep filtering
        SINGLE_EXEC_SCR_CMD_GREP,    //!< Single execution across single entry, script command, with ripgrep filtering
        SINGLE_EXEC_STD_OPTION,      //!< Default fallback: single execution across single entry, standard command, no ripgrep filtering
        NO_RUN_OPTION,               //!< Batch execution requested, but no command supplied or script filename input
        DEL_ENTRY                    //!< Delete the topic entry
    };

    int op = -1;

    // assign to correct switch
    if(do_run)
    {
        if     (batch_exec && run_std_cmd && rg_results)    { op = BATCH_EXEC_STD_CMD_GREP;     }
        else if(batch_exec && run_std_cmd)                  { op = BATCH_EXEC_STD_CMD_NO_GREP;  }
        else if(batch_exec && run_scr_cmd && rg_results)    { op = BATCH_EXEC_SCR_CMD_GREP;     }
        else if(batch_exec && run_scr_cmd)                  { op = BATCH_EXEC_SCR_CMD_NO_GREP;  }
        else if(batch_exec && !run_std_cmd && !run_scr_cmd) { op = NO_RUN_OPTION;               }
        else if(run_std_cmd && rg_results)                  { op = SINGLE_EXEC_STD_CMD_GREP;    }
        else if(run_std_cmd)                                { op = SINGLE_EXEC_STD_CMD_NO_GREP; }
        else if(run_scr_cmd && rg_results)                  { op = SINGLE_EXEC_SCR_CMD_GREP;    }
        else if(run_scr_cmd)                                { op = SINGLE_EXEC_SCR_CMD_NO_GREP; }
    }
    else if(TOPIC.delete_entry_from_topic != "UNSET")       { op = DEL_ENTRY;                   }
    else if(rg_results)                                     { op = SINGLE_EXEC_STD_CMD_GREP;    }
    else                                                    { op = SINGLE_EXEC_STD_OPTION;      }

    // dispatch
    std::string topic_fn;
    switch(op)
    {
    case BATCH_EXEC_STD_CMD_GREP:
        wt.execute_command_on_batch_set(wc, OPTS.paths.topics_dir, topic, TOPIC.exec_cmd_on_topic_entries, true, rg_set.rs);
        break;

    case BATCH_EXEC_STD_CMD_NO_GREP:
        wt.execute_command_on_batch_set_build_trs(wc, OPTS.paths.topics_dir, topic, topic_index, TOPIC.exec_cmd_on_topic_entries, true);
        break;

    case BATCH_EXEC_SCR_CMD_GREP:
        wt.execute_command_on_batch_set(wc, OPTS.paths.topics_dir, topic, TOPIC.exec_script_cmd_on_topic_entries, true, rg_set.rs);
        break;

    case BATCH_EXEC_SCR_CMD_NO_GREP:
        wt.execute_command_on_batch_set_build_trs(wc, OPTS.paths.topics_dir, topic, topic_index, TOPIC.exec_script_cmd_on_topic_entries, true);
        break;

    case SINGLE_EXEC_STD_CMD_GREP:
    case SINGLE_EXEC_SCR_CMD_GREP:
        topic_fn = _ripgrep_topic_content_get_entry(wc, wt, OPTS.paths.topics_dir, OPTS.paths.tmp_dir,
                                                                       topic_index, topic);
        _run_exec_on_topic_entry(OPTS, TOPIC, wib_e, topic_fn, bin_edit, bin_view);
        break;

    case SINGLE_EXEC_STD_CMD_NO_GREP:
    case SINGLE_EXEC_SCR_CMD_NO_GREP:
    case SINGLE_EXEC_STD_OPTION:
    default:
        topic_fn = wt.select_topic_entry_from_list(wc, OPTS.paths.topics_dir, topic_index, topic);
        _run_exec_on_topic_entry(OPTS, TOPIC, wib_e, topic_fn, bin_edit, bin_view);
        break;

    case NO_RUN_OPTION:
        wc.console_write_error_red("Batch operation requested but no script or command supplied! Aborting.");
        break;

    case DEL_ENTRY:
        _topic_entry_delete(wc, wt, OPTS.paths.topics_dir, topic_index, topic);
        break;
    }
}

/**
 * @brief Function to either open or delete a data (i.e. defined as non plain-text) Topic entry
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wc general console utility object
 * @param wib_e WibbleExecutor general environment/execution object
 * @param wt WibbleTopics Topic data object
 * @param topic topic_def structure providing all of the topic entry metadata
 */
void wibble::WibbleTopicManager::_open_or_delete_data_entry(const pkg_options& OPTS, topic_options& TOPIC,
                                                            WibbleConsole& wc, WibbleExecutor& wib_e, WibbleTopics& wt, topic_def& topic)
{
        std::string topic_data_index;
        if(TOPIC.filter_topic_entry != "UNSET")
        {
            topic_data_index = _filter_topic_tmp_index(wc, OPTS.paths.topics_dir, OPTS.paths.tmp_dir,
                                                       topic.id, TOPIC.filter_topic_entry, false);
        }
        else { topic_data_index = OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".data.wib"; }

        std::string data_topic_fn = wt.select_topic_entry_from_list(wc, OPTS.paths.topics_dir, topic_data_index, topic, true);
        if(data_topic_fn != "")
        {
            const unsigned long DELIM = data_topic_fn.find("|");
            const std::string stub_fn = data_topic_fn.substr(0, DELIM);
            const std::string att_dir = OPTS.paths.topics_dir + "/data_store/" + topic.category + "/" + topic.id;
            const std::string att_fn = att_dir + "/" + stub_fn;

            if(TOPIC.open_data_file != "UNSET")
            {
                wib_e.run_exec_expansion(OPTS.exec.xdg_open_cmd, att_fn);
            }
            else if(TOPIC.delete_data_file != "UNSET")
            {
                wc.console_write("FILE  ┃ " + stub_fn);
                wc.console_write("TITLE ┃ " + data_topic_fn.substr(DELIM + 1, std::string::npos) + "\n");
                wc.console_write_red("Really delete above entry? Deletion is PERMANENT.\n\n");
                std::string confirm = wc.console_write_prompt("Input 'delete' to remove >", "", "");
                if(confirm == "delete") { wt.remove_topic_entry(wc, topic, OPTS.paths.topics_dir, stub_fn, false); }
                else                    { wc.console_write_error_red("Deletion cancelled by user.");             }
            }
            else if(TOPIC.cd_data_dir != "UNSET")
            {
                if(std::filesystem::is_directory(std::filesystem::path(att_fn)))
                { WibbleUtility::cd_helper(att_fn);                              } // is dir; cd into it
                else
                { WibbleUtility::cd_helper(att_dir + "/" + stub_fn.substr(0,4)); } // is file; cd into YYYY dir
            }
        }
}

/**
 * @brief On screen display confirmation for selected Topic entry (prior to migration)
 * @param wc general console utility object
 * @param existing_title string with defined title for Topic entry, or parsed from Topic data entry
 * @param set_data_title in place mutation of existing data title, to prevent title expanding during migration
 * @param txt_not_data flag controlling whether Topic entry is a defined data entry or not
 */
void wibble::WibbleTopicManager::_preview_confirm_migrate_entry(WibbleConsole& wc, const std::string& existing_fn,
                                                                const std::string& existing_title,
                                                                std::string& set_data_title, const bool txt_not_data)

{
    if(! txt_not_data)
    {
        unsigned long DELIM = existing_title.find("|");

        if(DELIM != std::string::npos)
        {
            set_data_title = existing_title.substr(DELIM + 2, std::string::npos);
            wc.trim(set_data_title);
        }
    }

    wc.console_write_green("Selected:\n\n");
    wc.console_write("FILE  ┃ " + existing_fn);
    wc.console_write("TITLE ┃ " + existing_title  + "\n");
}

/**
 * @brief Function to actually ensure copying and cleanup of Topic entry when migrating it between Topics.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param topic_base_dir string with configured root Topic directory
 * @param existing_fn string with current/old filename
 * @param src_fn string with full path to current/old Topic entry
 * @param new_fn string with full path to new Topic entry
 * @param txt_not_data flag controlling whether Topic entry is a defined data entry or not
 * @param data_entry_insert_result flag indicating data entry insertion/migration prior to this function (for data entry migration only)
 */
bool wibble::WibbleTopicManager::_remove_entry_post_migration(WibbleConsole& wc, WibbleTopics& wt, topic_def& topic, const std::string topic_base_dir,
                                                              const std::string& existing_fn, const std::string& src_fn,
                                                              const std::string& new_fn, const bool txt_not_data,
                                                              const bool data_entry_insert_result)
{
    bool copy_result = true;
    // no need to copy data file contents, already performed by add_topic_data_entry
    if(txt_not_data) { copy_result = wibble::WibbleIO::wcopy_file(src_fn, new_fn); }
    else             { copy_result = data_entry_insert_result; }

    if(copy_result)
    {
        bool remove_result;

        if(txt_not_data) { remove_result = wt.remove_topic_entry(wc, topic, topic_base_dir, existing_fn, true);  }
        else             { remove_result = wt.remove_topic_entry(wc, topic, topic_base_dir, existing_fn, false); }

        if(remove_result)
        {
            std::filesystem::remove(std::filesystem::path(src_fn));
            return true;
        }
        else { return false; }
    }
    else
    {
        wc.console_write_error_red("Error copying topic entry file.");
        return false;
    }
}

/**
 * @brief Rename a topic entry; wrapper function passes through to WibbleTopics
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param topic_name string with unique Topic identifier/topic name
 */
void wibble::WibbleTopicManager::_rename_topic_entry_wrapper(pkg_options& OPTS, topic_options& TOPIC, WibbleConsole& wc,
                                                             WibbleTopics& wt, topic_def& topic,
                                                             const std::string& topic_name)
{
    wc.console_write_success_green("Working with topic '" + topic_name + "'"); wc.console_write("");
    const bool txt_not_data = TOPIC.alter_ent_title != "UNSET" ? true : false;
    const std::string orig_topic_index = txt_not_data ? OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".wib" :
                                                        OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".data.wib";
    std::string search_index;
    // --filter
    if(TOPIC.filter_topic_entry != "UNSET")
    {
        search_index = _filter_topic_tmp_index(wc, OPTS.paths.topics_dir, OPTS.paths.tmp_dir,
                                              topic.id, TOPIC.filter_topic_entry, txt_not_data);
    }
    else
    {
        if(txt_not_data) { search_index = OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".wib";      }
        else             { search_index = OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".data.wib"; }
    }

    const std::string topic_fn = wt.select_topic_entry_from_list(wc, OPTS.paths.topics_dir, search_index, topic, true);
    if(topic_fn != "") { wt.rename_topic_entry(wc, OPTS.paths.tmp_dir, orig_topic_index, topic_fn, txt_not_data);  }
    else               { wc.console_write_error_red("No valid entry selected.");                                   }
}


/**
 * @brief Run ripgrep across topic entries for content filter
 * @param topic_entry_dir directory of particular Topic to ripgrep
 * @param tmp_dir string with path to Wibble cache/temporary directory
 * @param grep_str search string/regular expression to supply to ripgrep
 */
bool wibble::WibbleTopicManager::_exec_ripgrep_for_index(const std::string& topic_entry_dir, const std::string& tmp_dir,
                                                         const std::string& grep_str)
{
    // cd into topic storage directory to remove extraneous part of path results
    std::string rg_cmd = "cd \"" + topic_entry_dir + "\" && rg --vimgrep -iF \""
        + grep_str + "\" > " + tmp_dir + "/wibble-workfile-rg";
    //std::cerr << "DEBUG: built rg --vimgrep cmd: " << rg_cmd << std::endl;
    int run_rg = std::system(rg_cmd.c_str());
    if(run_rg == 0) { return true;  }
    else            { return false; }

}

/**
 * @brief Wrapper function to obtain filtered result set after running ripgrep across Topic entry content/directory
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic_base_dir string with configure root Topic directory
 * @param tmp_dir string with path to Wibble cache/temporary directory
 * @param topic_index string with path to Topic index file
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param grep_str search string/regular expression to supply to ripgrep
 */
wibble::WibbleTopics::rg_matches wibble::WibbleTopicManager::_generate_ripgrep_result_index(WibbleConsole& wc, WibbleTopics& wt,
                                                                         const std::string& topic_base_dir,
                                                                         const std::string& tmp_dir, const std::string& topic_index,
                                                                         topic_def& topic, const std::string& grep_str)
{

    wc.console_write_green(ARW + "Active ripgrep on topic entry content in effect: "); wc.console_write_yellow(grep_str + '\n');
    const std::string topic_entry_dir = topic_base_dir + "/store/" + topic.category + "/" + topic.id;
    bool run_rg = _exec_ripgrep_for_index(topic_entry_dir, tmp_dir, grep_str);
    if(! run_rg) { wc.console_write_error_red("Warning: no matches from ripgrep search."); }
    return wt.rg_vimgrep_matches(wc, tmp_dir + "/wibble-workfile-rg", topic_index);

}


/**
 * @brief Wrapper function to interactively select Topic entry after running ripgrep to filter viable results.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic_base_dir string with configure root Topic directory
 * @param tmp_dir string with path to Wibble cache/temporary directory
 * @param topic_index string with path to Topic index file
 * @param topic topic_def structure providing all of the topic entry metadata
 * @param grep_str search string/regular expression to supply to ripgrep
 */
std::string wibble::WibbleTopicManager::_ripgrep_topic_content_get_entry(WibbleConsole& wc, WibbleTopics& wt,
                                                                         const std::string& topic_base_dir,
                                                                         const std::string& tmp_dir, const std::string& topic_index,
                                                                         topic_def& topic)
{
    const std::string topic_entry_dir = topic_base_dir + "/store/" + topic.category + "/" + topic.id;
    return wt.get_topic_fn_from_rg_vimgrep(wc, topic_index, topic_entry_dir, tmp_dir + "/wibble-workfile-rg");
}


/**
 * @brief Wrapper function to execute command on singular topic entry/file.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wib_e WibbleExecutor general environment/execution object
 * @param topic_fn string with full path to Topic entry file
 * @param bin_edit string with potential command to run for editing/opening custom binary file
 * @param bin_view string with potential command to run for viewing custom binary file
 */
void wibble::WibbleTopicManager::_run_exec_on_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleExecutor& wib_e,
                                                          const std::string& topic_fn, const std::string& bin_edit, const std::string& bin_view)
{
    if(TOPIC.exec_cmd_on_topic_entries != "UNSET") { wib_e.run_exec_expansion(TOPIC.exec_cmd_on_topic_entries, topic_fn);             }
    else if(TOPIC.render)                          { wib_e.run_exec_expansion(OPTS.paths.wibble_store + "/render.sh", topic_fn);      }
    else if(TOPIC.exec_script_cmd_on_topic_entries != "UNSET") { wib_e.run_exec_expansion(TOPIC.exec_cmd_on_topic_entries, topic_fn); }
    else if(TOPIC.gui_edit_topic_entry != "UNSET") { if(bin_edit == "") { wib_e.run_exec_expansion(OPTS.exec.gui_editor, topic_fn);   }
                                                                   else { wib_e.run_exec_expansion(bin_edit, topic_fn);           }   }
    else if(TOPIC.edit_topic_entry != "UNSET")     { if(bin_edit == "") { wib_e.run_exec_expansion(OPTS.exec.cli_editor, topic_fn);   }
                                                                   else { wib_e.run_exec_expansion(bin_edit, topic_fn);           }   }
    else if(TOPIC.dump_topic_entry != "UNSET")     { if(bin_view == "") { wib_e.run_exec_expansion(OPTS.exec.dump_tool, topic_fn);    }
                                                                   else { wib_e.run_exec_expansion(bin_view, topic_fn);           }   }
    else if(TOPIC.view_topic_entry != "UNSET")     { if(bin_edit == "") { wib_e.run_exec_expansion(OPTS.exec.view_tool, topic_fn);    }
                                                                   else { wib_e.run_exec_expansion(bin_view, topic_fn);           }   }
}

/**
 * @brief Wrapper function to interactively select the Topic or exit.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 */
wibble::topic_def wibble::WibbleTopicManager::set_ensure_topic_or_exit(WibbleConsole& wc, WibbleTopics& wt)
{
    topic_def sel_topic = wt.get_topic_for_name(wt.list_available_topics(wc, true));
    // exit if empty/null topic
    if(_check_null_topic(sel_topic))
    {
        wc.console_write_error_red("Invalid selection or empty/no available topics.");
        wc.console_write_red("Ensure to create a topic first in order to add entries.\n");

        if(wt.get_hibernated_status())
        {
            wc.console_write_yellow("\n" + ARW + "Note: Hibernated topic(s) detected.\n" + ARW + "Reactivate them with '--reactivate' if you wish to work with them again.\n");
        }
        throw wibble::wibble_exit(1);
    }

    return sel_topic;
}


/**
 * @brief Wrapper function to interactively delete a Topic entry after confirmation from user.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic_base_dir string with configure root Topic directory
 * @param topic_index string with path to Topic index file
 * @param topic topic_def structure providing all of the topic entry metadata
 */
void wibble::WibbleTopicManager::_topic_entry_delete(WibbleConsole& wc, WibbleTopics& wt,
                                                     const std::string topic_base_dir,
                                                     const std::string& topic_index, topic_def& topic)
{
    std::string topic_fn = wt.select_topic_entry_from_list(wc, topic_base_dir, topic_index, topic, true);
    if(topic_fn != "")
    {
        const unsigned long DELIM = topic_fn.find("|");
        wc.console_write("FILE  ┃ " + topic_fn.substr(0, DELIM));
        wc.console_write("TITLE ┃ " + topic_fn.substr(DELIM + 1, std::string::npos) + "\n");
        wc.console_write_red("Really delete above entry? Deletion is PERMANENT.\n\n");
        std::string confirm = wc.console_write_prompt("Input 'delete' to remove >", "", "");
        if(confirm == "delete") { wt.remove_topic_entry(wc, topic, topic_base_dir, topic_fn.substr(0, DELIM), true); }
        else                    { wc.console_write_error_red("Deletion cancelled by user.");             }
    }
}

/**
 * @brief Wrapper function to setup requested Topic name to parse for operation.
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 */
std::string wibble::WibbleTopicManager::_set_topic_entry_operation_or_dump(topic_options& TOPIC, WibbleConsole& wc, WibbleTopics& wt)
{
    std::string topic_name;

    if(TOPIC.gui_edit_topic_entry != "UNSET")           { topic_name = TOPIC.gui_edit_topic_entry;                                              }
    else if(TOPIC.edit_topic_entry != "UNSET")          { topic_name = TOPIC.edit_topic_entry;                                                  }
    else if(TOPIC.inventory_topic != "UNSET")           { topic_name = TOPIC.inventory_topic;                                                   }
    else if(TOPIC.delete_entry_from_topic != "UNSET")   { topic_name = TOPIC.delete_entry_from_topic;                                           }
    else if(TOPIC.dump_topic_entry != "UNSET")          { topic_name = TOPIC.dump_topic_entry;                                                  }
    else if(TOPIC.alter_ent_title != "UNSET")           { topic_name = TOPIC.alter_ent_title;                                                   }
    else if(TOPIC.alter_dat_title != "UNSET")           { topic_name = TOPIC.alter_dat_title;                                                   }
    else if(TOPIC.exec_cmd_on_topic_entries != "UNSET") { topic_name = TOPIC.exec_topic_for_cmd; if(topic_name == "UNSET") { topic_name = ""; } }
    else if(TOPIC.exec_script_cmd_on_topic_entries != "UNSET")
                                                 { topic_name = TOPIC.exec_topic_for_script_cmd; if(topic_name == "UNSET") { topic_name = ""; } }
    else if(TOPIC.view_topic_entry != "UNSET")          { topic_name = TOPIC.view_topic_entry;                                                  }
    else // default to dump
    {
        wc.console_write_success_green("No operation specified, defaulting to 'dump'.");
        topic_name = wt.list_available_topics(wc, true);
        TOPIC.dump_topic_entry = topic_name;
        if(topic_name == "") { wc.console_write_error_red("Invalid selection, exiting."); throw wibble::wibble_exit(1); }
    }

    return topic_name;
}


/**
 * @brief Interactive function to add a new entry to an existing Topic.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::add_new_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);

    topic_def topic;
    bool valid_ff = false;
    bool valid_t_mplate = false;
    std::string t_mplate_path;

    // check whether we're creating from a file
    if(TOPIC.add_from_file != "UNSET")
    {
        wc.console_write_green(ARW + "Using file content from file: "); wc.console_write_yellow(TOPIC.add_from_file + '\n');
        valid_ff = wibble::WibbleUtility::check_expand_file_path_or_exit(wc, TOPIC.add_from_file);
    }

    // check whether we're creating from a template
    if(TOPIC.add_with_template != "UNSET")
    {
        t_mplate_path = _get_template_path_or_exit(wc, wib_e, TOPIC.add_with_template, OPTS.paths.templates_dir);
        valid_t_mplate = true;
        wc.console_write_green(ARW + "Using template: "); wc.console_write_yellow(t_mplate_path + '\n');
    }

    // ensure the topic is set
    if(TOPIC.add_entry_to_topic == "") { topic = set_ensure_topic_or_exit(wc, wt);  }
    else                               { topic = _get_topic_or_exit(wc, wt, TOPIC.add_entry_to_topic); }

    // add the new topic
    wc.console_write_success_green("Working with topic '" + topic.name + "'"); wc.console_write("");
    std::string topic_entry;
    if(TOPIC.specify_title == "UNSET")
    {
        if(TOPIC.override_date == "UNSET") { topic_entry = wt.add_topic_entry(wc, topic, OPTS.paths.topics_dir, "");                  }
        else                               { topic_entry = wt.add_topic_entry(wc, topic, OPTS.paths.topics_dir, TOPIC.override_date); }
    }
    else // title already supplied on command line, use it, no prompts
    {
        if(TOPIC.override_date == "UNSET")
        { topic_entry = wt.add_topic_entry(wc, topic, OPTS.paths.topics_dir, "", TOPIC.specify_title);                  }
        else
        { topic_entry = wt.add_topic_entry(wc, topic, OPTS.paths.topics_dir, TOPIC.override_date, TOPIC.specify_title); }
    }

    // exit if we cannot create the new topic entry file
    if(topic_entry == "") { wc.console_write_error_red("Error creating new topic entry."); throw wibble::wibble_exit(1); }

    // copy/use template if selected
    if(valid_t_mplate) { _check_use_template(wc, wt, topic, t_mplate_path, topic_entry); }

    //copy/use from-file if selected
    if(valid_ff)
    {
        bool ff = wibble::WibbleIO::wcopy_file(TOPIC.add_from_file, topic_entry);
        if(ff)
        { wc.console_write_success_green("New entry successfully created using file contents from " + TOPIC.add_from_file + "."); }
        else
        { wc.console_write_error_red("Error using file content from " + TOPIC.add_from_file + ". Check it exists/is readable.", false);  }
    }

    // ...otherwise, check for any _default_ topic template
    if(! valid_t_mplate && ! valid_ff) { _check_use_default_template(wc, wt, topic, OPTS.paths.templates_dir, topic_entry); }

    if(TOPIC.specify_title != "UNSET")
    { wc.console_write_success_green("New Topic entry successfully added to Topic with ID " + topic.id + ".", false);         }
    else // interactive branch, open new topic entry with editor
    {
        const std::string bin_cmd = WibbleUtility::get_binary_tool(wc, topic.type, OPTS.exec.bin_mapping, true);
        if(bin_cmd != "")                               { wib_e.run_exec_expansion(bin_cmd, topic_entry); }
        else if(TOPIC.gui_edit_topic_entry != "UNSET")  { wib_e.run_exec_expansion(OPTS.exec.gui_editor, topic_entry); }
        else                                            { wib_e.run_exec_expansion(OPTS.exec.cli_editor, topic_entry); }
    }
}


/**
 * @brief Function to hibernate (i.e. hide) or reactivate a selected Topic.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param wt WibbleTopics Topic data object
 * @param do_hiberation flag controlling whether operation is to hibernate an active topic, or reactivate a hibernated topic
 */
void wibble::WibbleTopicManager::_do_hibernation_action(pkg_options& OPTS, WibbleTopics& wt, const bool do_hibernate)
{
    WibbleConsole wc;

    if(do_hibernate) { wc.console_write_yellow_bold("Listing topics available for hibernation\n");  }
    else             { wc.console_write_yellow_bold("Listing topics available for reactivation\n"); }

    topic_def topic = set_ensure_topic_or_exit(wc, wt);
    if(topic.id       != "___NULL___" &&
       topic.category != "___NULL___" &&
       topic.name     != "___NULL___" &&
       topic.id       != "" &&
       topic.category != "" &&
       topic.name     != "")
    {
        const std::string topic_fn = OPTS.paths.topics_dir + "/topic_defs/" + topic.category + "_" + topic.name + ".wib";

        bool update_status = false;
        if(do_hibernate) { update_status = wt.flip_active_status(false, topic_fn); }
        else             { update_status = wt.flip_active_status(true, topic_fn);  }

        if(update_status)
        {
            if(do_hibernate) { wc.console_write_success_green("Topic has been put into hibernation/hidden.");          }
            else             { wc.console_write_success_green("Topic has been reactivated/taken out of hibernation."); }
        }
        else
        {
            if(do_hibernate) { wc.console_write_error_red("Error encountered putting topic into hibernation/hidden."); }
            else             { wc.console_write_error_red("Error encountered reaactivating topic.");                   }
        }
    }
}


/**
 * @brief Accessor function to hibernate a selected active Topic.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::hibernate_a_topic(pkg_options& OPTS, WibbleTopics& wt)
{
    _do_hibernation_action(OPTS, wt, true);
}

/**
 * @brief Accessor function to reactivate a hibernated Topic.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::reactivate_hibernated_topic(pkg_options& OPTS, WibbleTopics& wt)
{
    _do_hibernation_action(OPTS, wt, false);
}

/**
 * @brief Interactive function edit/create the Topic default template.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::topic_default_template(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);

    topic_def topic;

    // ensure the topic is set
    if(TOPIC.create_edit_default_template == "") { topic = set_ensure_topic_or_exit(wc, wt);                              }
    else                                         { topic = _get_topic_or_exit(wc, wt, TOPIC.create_edit_default_template); }

    const std::string default_template = OPTS.paths.templates_dir + "/" + topic.id;
    const std::string bin_edit_cmd = WibbleUtility::get_binary_tool(wc, topic.type, bin_mapping, true);

    bool unopened_df = false;
    if(! std::filesystem::exists(std::filesystem::path(default_template)))
    {
        const std::string dft = wt.get_default_topic_template_str();
        // check whether we need to setup a binary topic default file...
        if(bin_edit_cmd == "")
        {
            try                       { std::ofstream(default_template).write(dft.c_str(), dft.length()); unopened_df = true; }
            catch(std::exception& ex) { std::cerr << "Error writing default template: " << ex.what() << std::endl;            }
        }
        else
        {
            // failure case should not happen, always check for presence of template files on startup
            bool setup_stub = WibbleIO::wcopy_file(OPTS.paths.templates_dir + "/bin_default/default." + topic.type, default_template);
            if(! setup_stub) { wc.console_write_error_red("Error setting up binary template for Topic. Aborting."); throw wibble::wibble_exit(1); }
        }
    }

    wc.console_write_header("TEMPLATE");
    if(bin_edit_cmd == "")
    {
        wc.console_write("1. Edit default topic template for topic ID " + topic.id + " with CLI editor.");
        wc.console_write("2. Edit default topic template for topic ID " + topic.id + " with GUI editor.");
    }
    else
    {
        wc.console_write("1. Edit binary topic template for topic ID " + topic.id + " with associated tool.");
        wc.console_write("2. Edit binary topic template for topic ID " + topic.id + " with associated tool.");
    }
    wc.console_write_hz_divider();
    std::string option = wc.console_write_prompt("Select option > ", "", "");


    if(option == "1")
    {
        if(bin_edit_cmd == "") { wib_e.run_exec_expansion(OPTS.exec.cli_editor, default_template); }
        else                   { wib_e.run_exec_expansion(bin_edit_cmd, default_template); }
    }
    else if(option == "2")
    {
        if(bin_edit_cmd == "") { wib_e.run_exec_expansion(OPTS.exec.gui_editor, default_template); }
        else                   { wib_e.run_exec_expansion(bin_edit_cmd, default_template); }
    }
    else
    {
        wc.console_write_error_red("Invalid choice. Exiting.");
        if(unopened_df && std::filesystem::exists(std::filesystem::path(default_template)))
        { std::filesystem::remove(std::filesystem::path(default_template)); }
    }
}

/**
 * @brief Function/dispatcher to handle the various operations on Topic entries that are 'data'.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::dispatch_data_topic_entry(const pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt)
{
    // #6. All topic data operations

    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);

    std::string topic_name;
    topic_def topic;

    // ensure we have a topic to attach the data file into
    if(TOPIC.set_data_topic != "UNSET" && TOPIC.set_data_topic != "")
    {
        topic_name = TOPIC.set_data_topic;
        topic = _get_topic_or_exit(wc, wt, topic_name);
    }
    else { topic = set_ensure_topic_or_exit(wc, wt); }

    if(TOPIC.copy_data_file != "UNSET")
    {
        WibbleUtility::remove_trailing_slash(TOPIC.copy_data_file);

        if(TOPIC.override_data_date == "UNSET") { wt.add_topic_data_entry(wc, topic, OPTS.paths.topics_dir, TOPIC.copy_data_file, false, ""); }
        else { wt.add_topic_data_entry(wc, topic, OPTS.paths.topics_dir, TOPIC.copy_data_file, false, TOPIC.override_data_date); }
    }
    else if(TOPIC.move_data_file != "UNSET")
    {
        WibbleUtility::remove_trailing_slash(TOPIC.move_data_file);

        if(TOPIC.override_data_date == "UNSET") { wt.add_topic_data_entry(wc, topic, OPTS.paths.topics_dir, TOPIC.move_data_file, true, ""); }
        else { wt.add_topic_data_entry(wc, topic, OPTS.paths.topics_dir, TOPIC.move_data_file, true, TOPIC.override_data_date); }
    }
    else if(TOPIC.open_data_file != "UNSET" || TOPIC.delete_data_file != "UNSET" || TOPIC.cd_data_dir != "UNSET")
    {
        _open_or_delete_data_entry(OPTS, TOPIC, wc, wib_e, wt, topic);
    }
}

/**
 * @brief Function/accessor to obtain a topic entry filename; used by the bookmark functionality.
 * @param wc general console utility object
 * @param wt WibbleTopics Topic data object
 * @param topic_base_dir string with configured root Topic directory
 * @param tmp_dir string with path to Wibble cache/temporary directory
 * @param filter_exp string containing substring pattern to match against (optional)
 * @param grep_exp string containing substring pattern to supply to ripgrep for filtering against entry content (optional)
 * @param txt_not_data flag controlling whether Topic entry is a defined data entry or not
 */
wibble::topic_def wibble::WibbleTopicManager::get_topic_entry_fn(WibbleConsole& wc, WibbleTopics& wt, const std::string& topic_base_dir, const std::string& tmp_dir,
                               const std::string& filter_exp, const std::string& grep_exp, const bool txt_not_data)
{

    // get the topic
    topic_def topic = set_ensure_topic_or_exit(wc, wt);
    // set the index, possibly filtered
    std::string topic_index;
    if(filter_exp != "")
    {
        if(txt_not_data)
        {
            topic_index = _filter_topic_tmp_index(wc, topic_base_dir, tmp_dir,
                                                  topic.id, filter_exp, true);
        }
        else
        {
            topic_index = _filter_topic_tmp_index(wc, topic_base_dir, tmp_dir,
                                                  topic.id, filter_exp, false);
        }
    }
    else
    {
        if(txt_not_data)
        {
            topic_index = topic_base_dir + "/topic_indexes/" + topic.id + ".wib";
        }
        else
        {
            topic_index = topic_base_dir + "/topic_indexes/" + topic.id + ".data.wib";
        }
    }

    // if Topic entry is a plain-text/non-data entry, and ripgrep expression supplied, run it
    if(grep_exp != "" && txt_not_data)
    {
            WibbleTopics::rg_matches rg_set;

            rg_set = _generate_ripgrep_result_index(wc, wt, topic_base_dir, tmp_dir,
                                                    topic_index, topic, grep_exp);
            topic.bk_filename = _ripgrep_topic_content_get_entry(wc, wt, topic_base_dir, tmp_dir, topic_index, topic);
    }
    else
    {
        if(txt_not_data) { topic.bk_filename = wt.select_topic_entry_from_list(wc, topic_base_dir, topic_index, topic); }
        else // data filename
        {
            topic.bk_filename = wt.select_topic_entry_from_list(wc, topic_base_dir, topic_index, topic, true);
            if(topic.bk_filename != "") // fix data path
            {
                const unsigned long DELIM = topic.bk_filename.find("|");
                const std::string stub_fn = topic.bk_filename.substr(0, DELIM);
                topic.bk_filename = topic_base_dir + "/data_store/" + topic.category + "/" + topic.id + "/" + stub_fn;
            }
        }
    }

    if(topic.bk_filename == "") { wc.console_write_error_red("No result selected. Exiting."); throw wibble::wibble_exit(1); }
    return topic;
}

/**
 * @brief Parent function to migrate (i.e. move) a topic entry from one topic to another.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::migrate_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);
    topic_def topic;
    bool txt_topic = true;

    // flag that determines whether we're working with a text entry or data attachment
    if(TOPIC.migrate_data_topic != "UNSET") { txt_topic = false; }

    if(TOPIC.migrate_topic == "" || TOPIC.migrate_data_topic == "") { topic = set_ensure_topic_or_exit(wc, wt); }
    else if(txt_topic) { topic = _get_topic_or_exit(wc, wt, TOPIC.migrate_topic);                                }
    else               { topic = _get_topic_or_exit(wc, wt, TOPIC.migrate_data_topic); txt_topic = false;        }

    std::string topic_index;
    std::string topic_fn;

    if(txt_topic) { topic_index = OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".wib"; }
    else          { topic_index = OPTS.paths.topics_dir + "/topic_indexes/" + topic.id + ".data.wib"; }

    topic_fn = wt.select_topic_entry_from_list(wc, OPTS.paths.topics_dir, topic_index, topic, true);

    // if no topic has been selected, abort
    if(topic_fn == "") { throw wibble::wibble_exit(0); }

    // ensure clean title on topic migration, i.e. do not "grow" title with additional date/filename section
    const int DATE_OFFSET  = 22;
    //const inst DATA_OFFSET = 52;

    unsigned long DELIM = topic_fn.find("|");
    std::string entry_title;
    std::string existing_fn    = topic_fn.substr(0, DELIM);
    std::string existing_title = topic_fn.substr(DELIM + 1, std::string::npos);
    wc.trim(existing_fn);
    wc.trim(existing_title);

    _preview_confirm_migrate_entry(wc, existing_fn, existing_title, entry_title, txt_topic);
    topic_def dest_topic = set_ensure_topic_or_exit(wc, wt);

    if(txt_topic && (topic.type != dest_topic.type))
    {
        _filetype_migration_confirm_or_exit(wc, topic.type, dest_topic.type);
    }

    if(dest_topic.id == topic.id)
    {
        wc.console_write_error_red("Destination topic cannot be the same as the origin! Aborting.");
        throw wibble::wibble_exit(1);
    }

    _confirm_proceed_migration_or_exit(wc, existing_fn, topic.name, dest_topic.name);

    std::string src_fn = _build_topic_entry_full_filename(wc, OPTS.paths.topics_dir, topic, existing_fn, txt_topic);
    std::string new_fn = "pending";
    bool data_copied = true;
    if(txt_topic)
    {
        // returns "" on failure, so we can then check this
        new_fn = wt.add_topic_entry(wc, dest_topic, OPTS.paths.topics_dir, "",
                                    existing_title.substr(DATE_OFFSET, (existing_title.size() - DATE_OFFSET)));
    }
    else
    {
        // returns false on failure
        data_copied = wt.add_topic_data_entry(wc, dest_topic, OPTS.paths.topics_dir, src_fn, false, "",
                                              entry_title, true);
    }

    // copy contents/file and remove original
    // check that either the add_topic_entry or add_topic_data entry worked correctly
    if(std::filesystem::exists(std::filesystem::path(src_fn)) && new_fn != "" && data_copied)
    {
        bool migration_result = _remove_entry_post_migration(wc, wt, topic, OPTS.paths.topics_dir, existing_fn, src_fn, new_fn, txt_topic, data_copied);
        if(migration_result)
        {
            if(txt_topic) { wc.console_write_success_green("Topic entry migrated successfully.");     }
            else          { wc.console_write_success_green("Topic data file migrated successfully."); }
        }
        else
        {
            if(txt_topic) { wc.console_write_error_red("Error migrating topic entry.");     }
            else          { wc.console_write_error_red("Error migrating topic data file."); }
        }
    }
    else
    {
        wc.console_write_error_red("Error adding new topic entry. Aborting migration operation. Original file untouched.");
        wc.console_write_red(src_fn + "\n");
    }
}

/**
 * @brief Main entry point; obtain the Topic and run the specified command on an entry/entries (or simply inventory the Topic).
 * @param OPTS pkg_options environment settings/configuration struct
 * @param TOPIC topic_options command line arguments/settings supplied to invocation
 * @param wt WibbleTopics Topic data object
 */
void wibble::WibbleTopicManager::process_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt)
{

    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);

    // set flags from string command line options to determine operation
    std::string topic_name = _set_topic_entry_operation_or_dump(TOPIC, wc, wt);

    topic_def topic;

    // ensure the topic is set
    if(topic_name == "") { topic = set_ensure_topic_or_exit(wc, wt);  }
    else                 { topic = _get_topic_or_exit(wc, wt, topic_name); }

    // either inventory topic, rename an entry, or run external command on topic
    if(TOPIC.inventory_topic != "UNSET") { wt.inventory_topic(wc, OPTS.paths.topics_dir, topic);                       }
    else if(TOPIC.alter_ent_title != "UNSET" || TOPIC.alter_dat_title != "UNSET")
                                         { _rename_topic_entry_wrapper(OPTS, TOPIC, wc, wt, topic, topic.name);        }
    else                                 { _op_on_topic_entry(OPTS, TOPIC, wc, wib_e, wt, topic, topic.name);          }
}
