/**
 * @file      cmd_task.cpp
 * @brief     Handle "task" subcommand.
 * @details
 *
 * This class handles the dispatching logic for the "task" subcomand.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_task.hpp"
#include "wibbletask.hpp"

/**
 * @brief Dispatching logic for the "task" functionality.
 * @param OPTS package/general configuration options struct
 * @param TASK command line options/parameters supplied via switches for "task"
 */
void wibble::WibbleCmdTask::handle_task(pkg_options& OPTS, task_options& TASK)
{
    const bool suppress = (TASK.dump_group_names) ? true : false;
    WibbleConsole wc; 
    if(suppress) { wc.silence_output();}
    wc.console_write_header("TASK ");
    WibbleTask wtsk{OPTS.paths.tasks_dir, OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping, suppress};

    if (TASK.add_task != "UNSET")       { wtsk.add_new_task(TASK.add_task);         }
    else if(TASK.mark_task != "UNSET")
    {
        if(TASK.task_title_filter != "UNSET") { wtsk.complete_task(TASK.mark_task,TASK.task_title_filter, OPTS); }
        else                                  { wtsk.complete_task(TASK.mark_task, "", OPTS);                    }
    }
    else if(TASK.list_tasks != "UNSET")
    {
        if(TASK.task_title_filter != "UNSET") { wtsk.list_tasks(TASK.list_tasks, TASK.task_title_filter); }
        else                                  { wtsk.list_tasks(TASK.list_tasks, "");                     }
    } 
    else if(TASK.list_completed)        { wtsk.list_completed();                    }
    else if(TASK.show_everything)       { wtsk.show_everything();                   }
    else if(TASK.show_history)          { wtsk.display_history(OPTS);               }
    else if(TASK.view_tasks_for_group)  { wtsk.list_tasks_in_group(OPTS);           }
    else if(TASK.closed_task_groups)    { wtsk.list_tasks_in_closed_group(OPTS);    }
    else if(TASK.dump_group_names)      { wtsk.dump_group_names();                  }
    else
    {
        wc.console_write_success_green("No operation specified. Listing available tasks.");
        wtsk.list_tasks("", "");
    }
} 
