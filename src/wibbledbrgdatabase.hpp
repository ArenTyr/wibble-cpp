/**
 * @file      wibbledbrgdatabase.hpp
 * @brief     Provides all of the core plain-text "database" functionality (header file).
 * @details
 *
 * Provides all of the features related to the plain-text database
 * behind all of the Wibble "Nodes", with functionality for searching, persisting,
 * deleting and editing of records; associating and attaching and working with the related
 * Node data directory and all the files found within it.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEDBRG_H
#define WIBBLEDBRG_H

#include <set>

#include "wibbledbinterface.hpp"
#include "wibbledbrgquery.hpp"

namespace wibble
{
    class WibbleRGDatabase : WibbleDBInterface
    {
    private:
        const std::set<std::string> bin_map;

        // specialisations
        void _database_field_error(WibbleConsole& wc, const long& record_index, const std::string& line, const unsigned long line_num) const;
        long _get_line_number_for_record(const std::string& note_id, const std::string& wib_db_path, const std::string& WORKFILE) const;
        long _get_line_number_for_record(const WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const;
        bool _perform_delete(const std::string& wib_db_path, long& line_match) const;
        bool _validate_result(long line_match) const;
        bool persist_new_record(const WibbleRecord& record, const std::string& wib_db_path) const override;
        bool persist_existing_record(const WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const override;

        WibResultList select_database_records(const WibbleDBRGQuery& filter_expression, const std::string& wib_db_path, const std::string& WORKFILE) const;
        WibResultList deserialize_wibble_record(const std::vector<std::array<std::string, 13>>& rg_results) const;
        const std::string get_placeholder_replacement(const std::string& TOKEN, const WibbleRecord& record) const;

    public:
        static const std::string _rgdb_header();
        unsigned long result_size(WibResultList& w);
        WibbleRecord select_record(WibResultList& w, unsigned long s) override;
        WibResultList get_all_wibble_records(const std::string& wib_db_path, const std::string& WORKFILE) const override;
        WibResultList get_wibble_records(const WibbleDBQueryInterface* query_exp, const std::string& wib_db_path, const std::string& WORKFILE) const override;
        const metadata_schema generate_blank_schema() const override;
        const metadata_schema parse_schema_file(const std::string& schema) const override;
        WibbleRecord create_new_record_interactive(const metadata_schema ms, const std::string& t_mplate_ft) const override;
        bool update_existing_record(WibbleRecord& w,
                                    std::string&& da_f, std::string&& ti_f, std::string&& id_f,
                                    std::string&& de_f, std::string&& ty_f, std::string&& pr_f,
                                    std::string&& tg_f, std::string&& cl_f, std::string&& dd_f,
                                    std::string&& kp_f, std::string&& cu_f, std::string&& li_f) const override;
        bool delete_record(const std::string& note_id, const std::string& wib_db_path, const std::string& WORKFILE) const override;
        bool delete_record(const WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const override;
        bool export_record(const wibble::WibbleRecord& record, const std::string& wib_db_path) const override;
        bool migrate_record(const WibbleRecord& record, const std::string& wibble_store, const std::string& main_wib_db_path,
                            const std::string& ark_wib_db_path, const std::string& WORKFILE, bool from_main_to_archive) const;
        bool persist_record(bool create_new, const WibbleRecord& record, const std::string& wib_db_path,
                            const std::string& WORKFILE, const std::string& wibble_store) const override;
        bool perform_template_substitutions(const WibbleRecord& record, const std::string& wib_db_path) const override;
        WibbleRGDatabase(const std::set<std::string>& node_bin_map = std::set<std::string>()): bin_map{node_bin_map}{};
    };
}
#endif
