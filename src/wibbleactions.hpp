/**
 * @file      wibbleactions.hpp
 * @brief     Helper/utility class for some shared functions (header file).
 * @details
 *
 * Common functionality/methods used by both the "bookmark" and "task"
 * features amongst others, hence extraction into a common shared library.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEACTIONS_H
#define WIBBLEACTIONS_H

#include "wibblebookmark.hpp"
#include "wibbleutility.hpp"

namespace wibble
{

    using bm = wibble::WibbleBookmark::bookmark_entry;
    class WibbleActions
    {
    private:
    public:
        bm deserialize_topic_bm(const std::string& line);
        bool copy_project_files_into_node(WibbleConsole& wc, const pkg_options& OPTS, std::string& project_dir, const std::string& lf_list);
        bool copy_project_files_into_topic(WibbleConsole& wc, const pkg_options& OPTS, std::string& project_dir);
        void expand_exec(WibbleExecutor& wib_e, const std::string& cmd);
        void expand_exec(WibbleExecutor& wib_e, const std::string& exec_template, const std::string& fn);
        std::string jotfile_action_menu(WibbleConsole& wc, const std::string& jotfile_dir,
                                 const std::string& tmp_dir, const std::string& jotfile_fn);
        void node_action_menu(WibbleConsole& wc, WibbleRecord& w, WibbleExecutor& wib_e, const bool archived);
        void topic_action_menu(WibbleConsole& wc, WibbleExecutor& wib_e, const std::string& fn);
        std::string generate_topic_entry_link_interactive(WibbleConsole& wc, WibbleExecutor& wib_e);
    };
}
#endif
