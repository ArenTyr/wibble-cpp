/**
 * @file      wibbleexecutor.cpp
 * @brief     General execution/worker class.
 * @details
 *
 * General execution/worker class, that stores the current working environment,
 * various std::system() execution wrappers, and other utility
 * functions for working and operating with Wibble Nodes (WibbleRecord).
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <regex>
#include "wibblebinlist.hpp"
#include "wibbleexecutor.hpp"
#include "wibbleexit.hpp"
#include "wibblesearch.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

// ============ 1. Accessors ============
/**
 * @brief Getter for class private variable storing archive database filepath.
 */
std::string wibble::WibbleExecutor::get_archived_db() const
{
    return archived_db;
}

/**
 * @brief Getter for class private variable storing main database filepath.
 */
std::string wibble::WibbleExecutor::get_main_db() const
{
    return main_db;
}

/**
 * @brief Getter for class private variable storing backup directory location.
 */
std::string wibble::WibbleExecutor::get_backup_location() const
{
    return backup_location;
}

/**
 * @brief Getter for class private variable storing bookmarks directory location.
 */
std::string wibble::WibbleExecutor::get_bookmarks_dir() const
{
   return bookmarks_dir;
}

/**
 * @brief Getter for class private variable storing Jotfile directory location.
 */
std::string wibble::WibbleExecutor::get_jot_dir() const
{
    return jot_dir;
}

/**
 * @brief Getter for class private variable storing scripts directory location.
 */
std::string wibble::WibbleExecutor::get_scripts_dir() const
{
    return scripts_dir;
}

/**
 * @brief Getter for class private variable storing Tasks directory location.
 */
std::string wibble::WibbleExecutor::get_tasks_dir() const
{
    return tasks_dir;
}

/**
 * @brief Getter for class private variable storing templates directory location.
 */
std::string wibble::WibbleExecutor::get_templates_dir() const
{
    return templates_dir;
}


/**
 * @brief Getter for class private variable storing Topics directory location.
 */
std::string wibble::WibbleExecutor::get_topics_dir() const
{
    return topics_dir;
}

/**
 * @brief Getter for class private variable storing temporary/cache directory.
 */
std::string wibble::WibbleExecutor::get_tmp_dir() const
{
    return tmp_dir;
}

/**
 * @brief Getter for class private variable storing main parent Wibble directory.
 */
std::string wibble::WibbleExecutor::get_wibble_store() const
{
    return wibble_store;
}

/**
 * @brief Getter for class private variable storing user's preferred command line editor command
 */
std::string wibble::WibbleExecutor::get_cli_editor() const
{
    return cli_editor;
}

/**
 * @brief Getter for class private variable storing user's preferred dump/cat tool command.
 */
std::string wibble::WibbleExecutor::get_dump_tool() const
{
    return dump_tool;
}

/**
 * @brief Getter for class private variable storing user's preferred file manager tool command.
 */
std::string wibble::WibbleExecutor::get_fm_tool() const
{
    return fm_tool;
}

/**
 * @brief Getter for class private variable storing user's preferred GUI editor command.
 */
std::string wibble::WibbleExecutor::get_gui_editor() const
{
    return gui_editor;
}

/**
 * @brief Getter for class private variable storing user's preferred pager command.
 */
std::string wibble::WibbleExecutor::get_pager() const
{
    return pager;
}

/**
 * @brief Getter for class private variable storing user's preferred viewing/alternate cat tool command.
 */
std::string wibble::WibbleExecutor::get_view_tool() const
{
    return view_tool;
}

/**
 * @brief Getter for class private variable storing user's preferred xdg-open or alternate desktop resource opener command.
 */
std::string wibble::WibbleExecutor::get_xdg_open_cmd() const
{
    return xdg_open_cmd;
}

/**
 * @brief Getter for class private variable storing user's custom binary association mapping
 */
std::string wibble::WibbleExecutor::get_bin_mapping() const
{
    return bin_mapping;
}


// ============ 2. Internal helper functions ============
/**
 * @brief Constructs a path to a Wibble Node (note) or data directory based on the current configured environment.
 * @param note_id String with ID of the note
 * @param ftype String with filetype/extension of the note
 * @param proj String with project value of the note
 * @param archived Flag indicating whether node/note has been archived
 * @param note_not_data Flag indicating whether to instead generate path pointing at associated Node/note data directory
 */
std::string wibble::WibbleExecutor::_build_note_path(const std::string& note_id, const std::string& ftype, const std::string& proj,
                                             const bool& archived, const bool& note_not_data, const std::string& override_path) const
{
    const std::string store_path = (override_path == "") ? wibble_store : override_path;
    if(note_not_data)
    {
        if(! archived)
        { return store_path + "/notes/"
                + ftype + "/" + proj + "/" + note_id.substr(0, 2) + "/" + note_id + "." + ftype; }
        else
        { return store_path + "/archived_notes/"
                + ftype + "/" + proj + "/" + note_id.substr(0, 2) + "/" + note_id + "." + ftype; }
    }
    else
    {
        return store_path + "/note_data/" + note_id.substr(0, 2) + "/" + note_id;
    }
}

/**
 * @brief Initialises/creates a new Node/note, and returns the generated WibbleRecord (Node).
 * @param ms Struct containing the metadata_schema to build/pre-populate metadata with
 * @param t_mplate_f String containing any pre-populated filetype value from the metadata schema, if value set/exists
 */
const wibble::WibbleRecord wibble::WibbleExecutor::_initialise_new_note(const metadata_schema& ms, const std::string& t_mplate_ft) const
{
    return w_rg.create_new_record_interactive(ms, t_mplate_ft);
}

/**
 * @brief Non-interactively/batch creates a new Node/note, and returns the generated WibbleRecord (Node).
 * @param BATCH Struct containing the supplied batch Node parameters/values
 */
const wibble::WibbleRecord wibble::WibbleExecutor::_parse_batch_node_input(const batch_options& BATCH)
{
    try
    {
        std::string title, type, desc, proj, tags, cls, kp, cust;
        title = BATCH.node_title;        wc.trim(title);
        type  = BATCH.node_filetype;     wc.trim(type); WibbleRecord::sanitise_input(type);
        desc  = BATCH.node_description;  wc.trim(desc);
        proj  = BATCH.node_project;      wc.trim(proj); WibbleRecord::sanitise_input(proj);
        tags  = BATCH.node_tags;         wc.trim(tags);
        cls   = BATCH.node_class;        wc.trim(cls);
        kp    = BATCH.node_keypairs;     wc.trim(kp); //FIXME: needs validation
        cust  = BATCH.node_custom;       wc.trim(cust);

        // validation
        if(title == "" || title == "UNSET") { throw std::invalid_argument("Bad title");    }
        if(type == ""  || type == "UNSET" ) { throw std::invalid_argument("Bad filetype"); }
        if(desc == ""  || desc == "UNSET" ) { desc = "___NULL___";                         }
        if(proj == ""  || proj == "UNSET" ) { proj = "General";                            }
        if(tags == ""  || tags == "UNSET" ) { tags = "___NULL___";                         }
        if(cls == ""   || cls == "UNSET"  ) { cls = "___NULL___";                          }
        if(kp == ""    || kp == "UNSET"   ) { kp = "___NULL___";                           }
        if(cust == ""  || cust == "UNSET" ) { cust = "___NULL___";                         }

        std::string gen_date = WibbleRecord::generate_date_rfc5322("");
        std::string gen_id   = WibbleRecord::generate_UUIDv4();

        //const bool bin_type = (bin_map.count(type) == 0) ? false : true;
        // return the built record, throw away temporaries
        return WibbleRecord (std::move(gen_date),
                             std::move(title),
                             std::move(gen_id),
                             std::move(desc),
                             std::move(type),
                             std::move(proj),
                             std::move(tags),
                             std::move(cls),
                             std::move("___NULL___"),
                             std::move(kp),
                             std::move(cust),
                             std::move("___NULL___"),
                             false);
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Bad input: " + std::string(ex.what()), false);
        return WibbleRecord (std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             std::move("ERROR"),
                             false);

    }
}


std::vector<wibble::WibbleSearch::search_frame> wibble::WibbleExecutor::_generate_search_frames_for_keypair_string(WibbleConsole& wc, std::string kp_search)
{
    std::vector<WibbleSearch::search_frame> sr;
    try
    {
        std::vector<std::string> kp = WibbleRecord::generate_keypair_vector(wc, kp_search);
        std::vector<std::string> valid_kp;
        for(auto& item: kp)
        {
            // remove any space
            wc.trim(item);
            // keypairs must have a ":" separator; i.e. format must be key:val or :val
            std::size_t key_sep = item.find(':');
            if(key_sep == std::string::npos)
            {
                wc.console_write_red(ARW + "Warning. Skipping invalid keypair definition: " + item + '\n');
                continue;
            }
            else
            {
                if(key_sep == 0) { item = item + KP_DELIM; valid_kp.push_back(item); }
                else
                {
                    // reconstitute and rebracket any item
                    std::string key = item.substr(0, key_sep);
                    std::string val = item.substr(key_sep + 1, item.length() - key_sep - 1);
                    // trim any internal space
                    wc.trim(key); wc.trim(val);
                    item = KP_DELIM + key + ":" + val  + KP_DELIM;
                    valid_kp.push_back(item);
                }
            }
        }

        for(auto& item: valid_kp)
        {
            if(item.length() > 0)
            {
                WibbleSearch::search_frame s;
                s.field = SEARCH_FIELD::F_KP;
                s.query = item;
                sr.push_back(s);
            }
        }
    }
    catch(std::exception& ex) { std::cerr << "Error in generate_search_frames_for_keypair_string(): " << ex.what() << '\n'; }

    return sr;
}



/**
 * @brief Ensures path/containing directory of new Node/note exists/is created before proceeding.
 * @param note_id String with ID of the note
 * @param ftype String with filetype/extension of the note
 * @param proj String with project value of the note
 */
bool wibble::WibbleExecutor::_setup_note_path(const std::string& note_id, const std::string& ftype, const std::string& proj) const
{
    bool path_create = wio.create_note_path(wibble_store, note_id, ftype, proj, true, false);

    if(path_create) { wc.console_write_green(ARW + "Set up path."); wc.console_write(""); return true;                          }
    else            { wc.console_write_error_red("Unable to set up note path.", false); wc.console_write(""); return false; }
}

// ============ 3. Note manipulations/operations ============
/**
 * @brief Interactive confirmation wrapper around Node/note archiving/unarchiving.
 * @param w WibbleRecord/Node to archive/unarchive
 * @param restore_from_archive Flag indicating whether we're restoring/unarchiving rather than archiving
 * @param wibble_store String with root Wibble directory
 * @param main_db_path String with filename of main database file
 * @param ark_db_path String with filename of archived database file
 */
bool wibble::WibbleExecutor::archive_note_confirm(const WibbleRecord& w, const bool restore_from_archive, const std::string& wibble_store, const std::string& main_db_path, const std::string& ark_db_path) const
{
    std::cout << std::unitbuf;
    w.pretty_print_record_full();
    // confirmation check
    if(! restore_from_archive)
    {
        std::string response = wc.console_write_prompt("\nMove above record into archive database? (y/n) > ", "", "");
        if(response != "y" && response != "Y")
        {
            wc.console_write_error_red("User aborted. Exiting.");
            throw wibble::wibble_exit(0);
        }
    }
    else
    {
        std::string response = wc.console_write_prompt("\nRestore above record from archive database back into main database?\n(y/n) > ", "", "");
        if(response != "y" && response != "Y")
        {
            wc.console_write_error_red("User aborted. Exiting.");
            throw wibble::wibble_exit(0);
        }
    }

    if(! restore_from_archive) { return   _archive_note(w, wibble_store, main_db_path, ark_db_path); }
    else                       { return _unarchive_note(w, wibble_store, main_db_path, ark_db_path); }

}

/**
 * @brief Move/migrate Node/note record into archive.
 * @param w WibbleRecord/Node to archive/unarchive
 * @param wibble_store String with root Wibble directory
 * @param main_db_path String with filename of main database file
 * @param ark_db_path String with filename of archived database file
 */
bool wibble::WibbleExecutor::_archive_note(const WibbleRecord& w, const std::string& wibble_store,
                                           const std::string& main_db_path, const std::string& ark_db_path) const
{
    return w_rg.migrate_record(w, wibble_store, main_db_path, ark_db_path, tmp_dir + WORKSTUB, true);
}

/**
 * @brief Move/migrate Node/note record from archive (restore).
 * @param w WibbleRecord/Node to archive/unarchive
 * @param wibble_store String with root Wibble directory
 * @param main_db_path String with filename of main database file
 * @param ark_db_path String with filename of archived database file
 */
bool wibble::WibbleExecutor::_unarchive_note(const WibbleRecord& w, const std::string& wibble_store,
                                             const std::string& main_db_path, const std::string& ark_db_path) const
{
    return w_rg.migrate_record(w, wibble_store, main_db_path, ark_db_path, tmp_dir + WORKSTUB, false);
}


/**
 * @brief Link/associate a file or directory with a Node/note.
 * @param w WibbleRecord/Node to add linked file to
 * @param db_path String with filename of working database file
 * @param WORKFILE String with filename to temporary working/cache file
 * @param archive Flag indicating whether we're working with an archived Node/note
 * @param f_name String with filename of file or directory to "link" to Node/note
 */
bool wibble::WibbleExecutor::op_add_linked_file_to_note(WibbleRecord& w, const std::string& db_path, const std::string& WORKFILE, bool archived, std::string& f_name)
{
    try
    {
        wibble::WibbleUtility::expand_tilde(f_name);
        f_name = std::filesystem::absolute(std::filesystem::path(f_name));

        std::string LF_TOKEN = "[ LF ]";
        std::string data_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, false) + "/.wibble";
        bool dd_exists = true;
        if(! std::filesystem::exists(std::filesystem::path(data_path))) { dd_exists = wibble::WibbleIO::create_path(data_path);    }
        if(! dd_exists) { wc.console_write_error_red("Error creating data directory: " + data_path + ". Aborting."); return false; }

        bool file_op = wibble::WibbleIO::write_out_file_append(f_name + "\n", data_path + "/linked_files.wib");
        if(file_op)
        {
            wc.console_write_success_green("Linked file successfully added to note data directory.");
            std::string note_id = w.get_id();
            std::string curr_dd = w.get_dd();
            if(curr_dd == "___NULL___") { w.set_dd(std::move(LF_TOKEN));                 }
            else if(curr_dd.find(LF_TOKEN) != std::string::npos) { /* already present */ }
            else
            {
                std::string new_dd = curr_dd + " " + LF_TOKEN;
                w.set_dd(std::move(new_dd));
            }
            return w_rg.persist_record(false, w, db_path, WORKFILE, wibble_store);
        }
        else
        {
            return false;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error op_add_linked_file_to_note() " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Copy or move a file/directory into Node/note data directory.
 * @param w WibbleRecord/Node to attach data to
 * @param db_path String with filename of working database file
 * @param WORKFILE String with filename to temporary working/cache file
 * @param archive Flag indicating whether we're working with an archived Node/note
 * @param f_name String with filename of file or directory to "link" to Node/note
 * @param copy_not_move Flag indicating whether to add data via copying or moving operation
 */
bool wibble::WibbleExecutor::op_add_data_to_note(WibbleRecord& w, const std::string& db_path, const std::string& WORKFILE,
                                                 bool archived, std::string& f_name, bool copy_not_move, const bool bypass_check)
{
    WibbleUtility::expand_tilde(f_name);
    WibbleUtility::remove_trailing_slash(f_name);

    if(f_name == "")
    {
        wc.console_write_error_red("Filename cannot be blank. Aborting.", false);
        return false;
    }

    if(! std::filesystem::exists(std::filesystem::path(f_name)))
    {
        wc.console_write_error_red("File/directory does not exist: " + f_name + ". Aborting.", false);
        return false;
    }
    else
    {
        std::string DATA_TOKEN = "[ DATA ]";
        std::string data_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, false);
        std::string dest_f_name = "";

        // want to preserve/emplace the directory if copying/moving entire directory
        if(std::filesystem::is_directory(std::filesystem::path(f_name)))
        { dest_f_name = data_path + "/" + WibbleIO::get_path_minus_base_dir(WibbleIO::get_parent_dir_path(f_name), f_name); }
        else
        { dest_f_name = data_path + "/" + std::string(std::filesystem::path(f_name).filename());                            }

        // safety check when MOVING file/directory
        if(! copy_not_move) { wibble::WibbleUtility::confirm_file_move_or_exit(wc, f_name, dest_f_name, bypass_check); }

        bool dd_exists = true;
        if(! std::filesystem::exists(std::filesystem::path(data_path))) { dd_exists = wibble::WibbleIO::create_path(data_path);           }
        if(! dd_exists) { wc.console_write_error_red("Error creating data directory: " + data_path + ". Aborting.", false); return false; }


        wc.console_write_green(ARW + "Data destination: " + dest_f_name + "\n");
        bool file_op;
        if(copy_not_move) { file_op = wibble::WibbleIO::wcopy_file(f_name, dest_f_name); }
        else
        {
            file_op = wibble::WibbleIO::wmove_file(f_name, dest_f_name);
        }

        if(file_op)
        {
            wc.console_write_success_green("File/directory successfully added to node data directory.");
            std::string note_id = w.get_id();
            std::string curr_dd = w.get_dd();
            if(curr_dd == "___NULL___") { w.set_dd(std::move(DATA_TOKEN));                 }
            else if(curr_dd.find(DATA_TOKEN) != std::string::npos) { /* already present */ }
            else
            {
                std::string new_dd = DATA_TOKEN + " " + curr_dd;
                w.set_dd(std::move(new_dd));
            }
            return w_rg.persist_record(false, w, db_path, WORKFILE, wibble_store);
        }
        else
        {
            wc.console_write_error_red("Error executing file operation.", false);
            return false;
        }
    }
}

/**
 * @brief Copy/use default template file for new Node/note, if exists.
 * @param note_id String with ID value of new Node/note
 * @param ft String with filetype/extension of new Node/note
 * @param proj String with project value of the note
 */
void wibble::WibbleExecutor::_copy_default_template_if_exists(const std::string& note_id, const std::string& ft, const std::string& proj) const
{
    std::filesystem::path default_template = templates_dir + "/" + ft + "/default";
    if(! std::filesystem::exists(default_template))
    { wc.console_write_green(ARW + "No default template exists, proceeding with blank note.\n"); }
    else
    {
        bool copy_note_file = wio.copy_new_note_file(wibble_store, note_id, ft, proj, default_template, false);
        if(! copy_note_file)
        { wc.console_write_error_red("Warning. Error copying default template file. Proceeding with blank file."); }
        else
        { wc.console_write_success_green("Default template for filetype '" + ft + "' used."); }
    }
}

/**
 * @brief Create a new note interactively using existing file content.
 * @param f_name String with existing file content/path of file to use
 * @param f_name String with filetype/extension of new Node/note to create
 * @param open_afterwards Flag indicating whether to edit newly created Node/note afterwards
 * @param gui_edit Flag indicating whether to edit newly created file using GUI rather than CLI editor
 */
bool wibble::WibbleExecutor::op_create_node_batch(const batch_options& BATCH)
{
    WibbleRecord record = _parse_batch_node_input(BATCH);
    if(record.get_id() == "ERROR") { return false; }
    bool setup_paths = _setup_note_path(record.get_id(), record.get_type(), record.get_proj());
    if(! setup_paths) { return false; }
    bool copy_note_file = wio.copy_new_note_file(wibble_store, record.get_id(), record.get_type(), record.get_proj(), BATCH.content_file, false);
    if(copy_note_file)
    {
        bool result = w_rg.persist_record(true, record, main_db, "", wibble_store);
        if(result) { wc.console_write_success_green("Node with ID " + record.get_id() + " successfully created.", false); }
        else       { wc.console_write_error_red("Error inserting new record.", false); return false;                      }
    }
    else { wc.console_write_error_red("Error copying new note file.", false); wc.console_write(""); return false; }

    return true;
}


/**
 * @brief Interactively create a new note; parent function.
 * @param gui_edit Flag indicating whether to edit newly created file using GUI rather than CLI editor
 */
bool wibble::WibbleExecutor::op_create_note_interactive(const bool gui_edit)
{
    wibble::WibbleRecord record = _initialise_new_note(w_rg.generate_blank_schema(), "");
    bool setup_paths = _setup_note_path(record.get_id(), record.get_type(), record.get_proj());
    if(! setup_paths) { return false; }

    _copy_default_template_if_exists(record.get_id(), record.get_type(), record.get_proj());
    bool result = w_rg.persist_record(true, record, main_db, "", wibble_store);
    if(result)
    {
        wc.console_write_success_green("New record successfully inserted.");
        return note_open_with_editor(record.get_id(), record.get_type(), record.get_proj(), gui_edit, false);
    }
    else       { wc.console_write_error_red("Error inserting new record."); return false;          }
}

/**
 * @brief Interactively create a new note employing a metadata schema file.
 * @param f_name Filename of schema file
 * @param t_mplate_name Filename of optional template file
 * @param from_file Filename of optional existing file to use as new Node/note file content
 * @param gui_edit Flag indicating whether to edit newly created file using GUI rather than CLI editor
 */
bool wibble::WibbleExecutor::op_create_note_using_schema_file(const std::string f_name, const std::string& t_mplate_name, const std::string& from_file, bool gui_edit)
{
    std::string ft = "";
    std::string t_mplate = "";

    // this should never occur, but defensive programming:
    if(t_mplate_name != "" && from_file != "")
    {
        wc.console_write_error_red("A template cannot be used together in conjunction with from-file. Aborting.");
        return false;
    }

    // 1.Check for valid schema file
    if(! std::filesystem::exists(std::filesystem::path(schemas_dir + "/" + f_name)))
    { wc.console_write_error_red("Undefined schema file: " + f_name + ". Aborting."); return false; }

    bool copy_template = false;
    bool copy_from_file = false;
    std::string t_mplate_path = "";


    // 2. Check for valid template file, if template option is active. Set copy flag
    if(t_mplate_name != "")
    {
        _set_template_ft_and_name(f_name, ft, t_mplate);
        if(t_mplate == "") { return ""; }
        t_mplate_path = _check_for_template(ft, t_mplate);
        if(t_mplate_path == "")
        {
            // template option was specified, but no valid template found...
            return false;
        }
        else
        {
            copy_template = true;
        }
    }

    // 3. Check for valid from-file file, if from-file option is active. Set copy flag
    if(from_file != "")
    {
        if(std::filesystem::exists(std::filesystem::path(from_file)))
        {
            copy_from_file = true;
            wc.console_write_success_green("Using existing file: " + from_file);
        }
        else
        {
            wc.console_write_error_red("File " + from_file + " not readable/does not exist. Aborting.");
            return false;
        }
    }

    // 3. Now parse the schema file
    metadata_schema ms = w_rg.parse_schema_file(wio.read_in_schema_file(f_name, schemas_dir));

    wc.console_write_success_green("Using schema: " + f_name);
    wibble::WibbleRecord record = _initialise_new_note(ms, ft);
    bool setup_paths = _setup_note_path(record.get_id(), record.get_type(), record.get_proj());

    if(! setup_paths) { return false; }

    // copy template, if specified/needed
    if(copy_template)
    {
        bool copy_note_file = wio.copy_new_note_file(wibble_store, record.get_id(), record.get_type(), record.get_proj(), t_mplate_path, false);
        if(! copy_note_file) { wc.console_write_error_red("Warning. Error copying template file. Aborting."); return false; }
    }
    else // if not, copy the default template (if it is exists)
    {
        _copy_default_template_if_exists(record.get_id(), record.get_type(), record.get_proj());
    }

    // create from file, if specified/needed
    if(copy_from_file)
    {
        bool copy_note_file = wio.copy_new_note_file(wibble_store, record.get_id(), record.get_type(), record.get_proj(), from_file, false);
        if(! copy_note_file) { wc.console_write_error_red("Warning. Error copying file to use as note content. Aborting."); return false; }
    }

    bool result = w_rg.persist_record(true, record, main_db, "", wibble_store);
    if(result)
    {
        wc.console_write_success_green("New record successfully inserted.");
        return note_open_with_editor(record.get_id(), record.get_type(), record.get_proj(), gui_edit, false);
    }
    else { wc.console_write_error_red("Error inserting new record."); return false; }
}

/**
 * @brief Parse template file to use from user input (i.e. org:food -> templates/org/food.org, txt:general/todo -> templates/txt/general/todo.txt).
 * @param f_name String with schema file specification to parse
 * @param ft String with destination filetype to set
 * @param t_mplate String with destination template name to set
 */
void wibble::WibbleExecutor::_set_template_ft_and_name(const std::string& f_name, std::string& ft, std::string& t_mplate)
{
    try
    {
        ft = f_name.substr(0, f_name.find(":"));
        t_mplate = f_name.substr(f_name.find(":") + 1, std::string::npos);
    }
    catch(std:: out_of_range& ex)
    {
        std::cerr << "DEBUG: Bad template file" << std::endl;
        ft = "";
        t_mplate = "";
    }

}

/**
 * @brief Determine whether underlying environment-specific template file exists; return path if so, else ""
 * @param ft String with destination filetype to set
 * @param t_mplate String with destination template name to set
 */
std::string wibble::WibbleExecutor::_check_for_template(const std::string& ft, const std::string& t_mplate_name)
{
    std::string t_mplate_path = templates_dir + "/" + ft + "/" + t_mplate_name;
    wc.console_write_success_green("Using template: " + t_mplate_path);
    if(! std::filesystem::exists(std::filesystem::path(t_mplate_path)))
    {
        //wibble::WibbleConsole().console_write_error_red("Template file " + t_mplate_path + " not found. Aborting.");
        wc.console_write_error_red("Template file " + t_mplate_path + " not found. Aborting.");
        //std::cerr << "DEBUG: Template file " << t_mplate_path << " not found. Aborting." << std::endl;
        return "";
    }
    else { return t_mplate_path; }
}

/**
 * @brief Create a new note interactively using a template file.
 * @param f_name String with template file name to use
 * @param gui_edit Flag indicating whether to edit newly created file using GUI rather than CLI editor
 */
bool wibble::WibbleExecutor::op_create_note_from_template_file(const std::string f_name, const bool gui_edit)
{
    std::string ft = "";
    std::string t_mplate = "";

    _set_template_ft_and_name(f_name, ft, t_mplate);
    if(t_mplate == "") { return ""; }

    std::string t_mplate_path = _check_for_template(ft, t_mplate);
    if(t_mplate_path == "") { return false; }

    return op_create_note_from_file(t_mplate_path, true, ft, gui_edit);
}

/**
 * @brief Create a new note interactively using existing file content.
 * @param f_name String with existing file content/path of file to use
 * @param f_name String with filetype/extension of new Node/note to create
 * @param open_afterwards Flag indicating whether to edit newly created Node/note afterwards
 * @param gui_edit Flag indicating whether to edit newly created file using GUI rather than CLI editor
 */
bool wibble::WibbleExecutor::op_create_note_from_file(const std::string f_name, bool open_afterwards, const std::string& ft, const bool gui_edit)
{
    WibbleRecord record = _initialise_new_note(w_rg.generate_blank_schema(), ft);
    bool setup_paths = _setup_note_path(record.get_id(), record.get_type(), record.get_proj());
    if(! setup_paths) { return false; }
    bool copy_note_file = wio.copy_new_note_file(wibble_store, record.get_id(), record.get_type(), record.get_proj(), f_name, false);
    if(copy_note_file)
    {
        bool result = w_rg.persist_record(true, record, main_db, "", wibble_store);
        if(result) { wc.console_write_success_green("New record successfully inserted."); }
        else       { wc.console_write_error_red("Error inserting new record."); return false; }
    }
    else { wc.console_write_error_red("Error copying new note file."); std::cout << std::endl; return false; }

    if(open_afterwards) { return note_open_with_editor(record.get_id(), record.get_type(), record.get_proj(), gui_edit, false); }
    else                { return true; }
}

/**
 * @brief Delete an existing Node/note.
 * @param note_id String with ID of the note
 * @param wib_db String with filename/path to database file
 * @param tmp_dir String with filename/path of cache dir/temporary directory
 */
//bool wibble::WibbleExecutor::op_remove_node(const std::string& note_id, const std::string& wib_db, const std::string tmp_dir)
bool wibble::WibbleExecutor::op_remove_node(const WibbleRecord& w, const bool archived, const std::string& wib_db, const std::string tmp_dir)
{
    bool update_db = w_rg.delete_record(w.get_id(), wib_db, tmp_dir + WORKSTUB);
    if(update_db) // now purge the actual Node files from disk
    {
        const std::string node_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, true);
        const std::string par_path = WibbleIO::get_parent_dir_path(node_path);
        const std::string proj_par_path = WibbleIO::get_parent_dir_path(par_path);
        const std::string ft_par_path = WibbleIO::get_parent_dir_path(proj_par_path);
        // accommodate cleanup of Freeplane _mindmap directory
        const std::string node_stem = std::filesystem::path(node_path).stem();
        const std::string mm_files = par_path + "/" + node_stem + "_files";
        std::uintmax_t node_del_count = 0;
        // use remove all in case Node is actually special directory type like wdir
        node_del_count = std::filesystem::remove_all(node_path);
        if(std::filesystem::exists(std::filesystem::path(mm_files)))
        {
            std::filesystem::remove_all(mm_files);
            wc.console_write_success_green("Removed Freeplane attachment files.");
        }
        // housekeeping
        // remove each directory parent in turn, ONLY if empty
        if(std::filesystem::is_empty(par_path))      { std::filesystem::remove(par_path);      }
        if(std::filesystem::is_empty(proj_par_path)) { std::filesystem::remove(proj_par_path); }
        if(std::filesystem::is_empty(ft_par_path))   { std::filesystem::remove(ft_par_path);   }
        if(node_del_count > 0) { wc.console_write_success_green("Node removed from disk.");    }
    }
    return true;
}

/**
 * @brief Update an existing Node/note.
 * @param w WibbleRecord/Node to update/adjust
 * @param use_archive_db Flag controlling whether we're updating an archived record
 */
bool wibble::WibbleExecutor::op_update_existing_record(const WibbleRecord& w, const bool use_archive_db) const
{
    if(! use_archive_db)
    { return w_rg.persist_record(false, w, main_db, tmp_dir + WORKSTUB, wibble_store);     }
    else
    { return w_rg.persist_record(false, w, archived_db, tmp_dir + WORKSTUB, wibble_store); }
}

// ============ 3. Note external dispatch/exec operations ============
/**
 * @brief Update an existing Node/note.
 * @param note_id String with ID of the note
 * @param ftype String with filetype/extension of the note
 * @param proj String with project value of the note
 * @param gui_edit Flag indicating whether to edit newly created file using GUI rather than CLI editor
 * @param archived Flag indicating whether node/note has been archived
 */
bool wibble::WibbleExecutor::note_open_with_editor(const std::string& note_id, const std::string& ftype, const std::string& proj, const bool gui_edit, const bool archived)
{
    std::string cmd;
    std::string note_path;
    if(! archived)
    {
        note_path = _build_note_path(note_id, ftype, proj, false, true);
        wc.console_write_green(ARW + "Opening Node " + note_id + "." + ftype + " from main database.\n");
    }
    else
    {
        note_path = _build_note_path(note_id, ftype, proj, true, true);
        wc.console_write_green(ARW + "Opening Node " + note_id + "." + ftype + " from archived database.\n");
    }

    // get/check binary mapping
    const std::string bin_edit_cmd = WibbleUtility::get_binary_tool(wc, ftype, bin_mapping, true);
    if(bin_edit_cmd != "")
    {
        if(! std::filesystem::exists(std::filesystem::path(note_path)))
        {
            wc.console_write_green(ARW + "Setting up binary stub file at: " + note_path + "\n");
            bool setup_stub = WibbleIO::wcopy_file(templates_dir + "/bin_default/default." + ftype, note_path);
            if(! setup_stub) { wc.console_write_error_red("Error creating output binary file: " + note_path); throw wibble::wibble_exit(1); }
            else
            {
                return run_exec_expansion(bin_edit_cmd, note_path);
            }
        }
        else
        {
            wc.console_write_green(ARW + "Opening binary Node file: " + note_path);
            return run_exec_expansion(bin_edit_cmd, note_path);
        }
    }
    else // standard, non-binary file
    {
        // ensure file exists (though most editors can create new file from command line)
        if(! std::filesystem::exists(std::filesystem::path(note_path)))
        {
            std::ofstream(note_path).put(' ');
            std::ofstream(note_path).close();
        }

        if(gui_edit) { return run_exec_expansion(gui_editor, note_path); }
        else         { return run_exec_expansion(cli_editor, note_path); }
    }
}

/**
 * @brief Open a linked file associated with a Node/note.
 * @param w WibbleRecord/Node that has the linked file attached.
 * @param archived Flag indicating whether node/note has been archived
 */
bool wibble::WibbleExecutor::note_open_linked_file(const WibbleRecord&w, const bool archived)
{
    std::string lf_list;
    if(! archived) { lf_list = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), false, false) + "/.wibble/linked_files.wib"; }
    else           { lf_list = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), true, false)  + "/.wibble/linked_files.wib"; }

    return wibble::WibbleUtility::open_file_selector(tmp_dir, wc, *this, lf_list);
}

/**
 * @brief Open a linked file associated with a Node/note with a filename/path filter.
 * @param w WibbleRecord/Node that has the linked file attached.
 * @param archived Flag indicating whether node/note has been archived
 * @param filter String with substring to filter linked file list by
 */
bool wibble::WibbleExecutor::note_dd_open_individual_file(WibbleRecord&w, const bool archived, const std::string& filter)
{

    std::string dd_fl;
    if(filter == "") { dd_fl = generate_dd_list(w, archived, tmp_dir + "/wibble-workfile"); }
    else
    {
        wc.console_write_green(ARW + "Filtering data directory results by term: "); wc.console_write_yellow(filter + "\n");
        dd_fl = generate_dd_list(w, archived, tmp_dir + "/wibble-workfile", filter);
    }

    if(dd_fl != "")
    {
        return wibble::WibbleUtility::open_file_selector(tmp_dir, wc, *this, dd_fl, "", "", true);
    }
    else
    {
        return false;
    }
}

/**
 * @brief List, tree, or run custom command on Node/note data directory.
 * @param w WibbleRecord/Node that has the linked file attached.
 * @param archived Flag indicating whether node/note has been archived
 * @param int Switch controlling directory operation
 * @param dir_cmd String with custom shell command to run on directory
 */
bool wibble::WibbleExecutor::note_dd_ls_view_dump(WibbleRecord& w, const bool archived, const int cmd, std::string dir_cmd) const
{
    if(dir_cmd == "") { dir_cmd = "ls -Alh --color"; } // default failsafe...
    std::string data_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, false);
    wc.console_write_green(ARW + "Listing contents of: " + data_path + '\n');
    if(std::filesystem::exists(std::filesystem::path(data_path)))
    {
        int ls_dd;
        switch(cmd)
        {
        default:
        case 1:
            ls_dd = std::system(("ls -Alh --color " + data_path).c_str());
            break;
        case 2:
            ls_dd = std::system(("tree -Alh --du " + data_path).c_str());
            break;
        case 3:
            ls_dd = std::system((dir_cmd + " " + data_path).c_str());
            break;
        }
        if(ls_dd == 0) { wc.console_write_success_green("Note data directory operation complete.");     return true;   }
        else           { wc.console_write_error_red("Error performing note data directory operation."); return false;  }
    }
    else
    {
        wc.console_write_error_red("Note has no data directory. Add some data to note to create one.");
        return false;
    }
}

/**
 * @brief Output content of Node/note file through user's 'dump' tool (e.g. cat).
 * @param record WibbleRecord/Node to output
 * @param archived Flag indicating whether node/note has been archived
 */
bool wibble::WibbleExecutor::note_pipe_through_dump_tool(const WibbleRecord& record, const bool archived)
{
    const std::string bin_cmd = WibbleUtility::get_binary_tool(wc, record.get_type(), bin_mapping, false);
    if(bin_cmd != "") { return note_exec_cmd(record, bin_cmd, archived);   }
    else              { return note_exec_cmd(record, dump_tool, archived); }
}

/**
 * @brief Output content of Node/note file through user's 'view' tool (e.g. gxmessage).
 * @param record WibbleRecord/Node to output
 * @param archived Flag indicating whether node/note has been archived
 * @param render_node Flag indicating whether to instead pass node/note through render script
 */
bool wibble::WibbleExecutor::note_pipe_through_view_tool(const WibbleRecord& record, const bool archived, const bool render_node)
{
    if(render_node)
    {
        if(! std::filesystem::exists(render_script))
        {
            wc.console_write_error_red("Render script not present/readable at: '" + render_script + "'. Aborting");
            return false;
        }
        else { return note_exec_cmd(record, render_script, archived); }
    }
    else
    {
        const std::string bin_cmd = WibbleUtility::get_binary_tool(wc, record.get_type(), bin_mapping, false);
        if(bin_cmd != "") { return note_exec_cmd(record, bin_cmd, archived);   }
        else              { return note_exec_cmd(record, view_tool, archived); }
    }
}

/**
 * @brief Run an arbitrary command on the Node/note file (or any other file), with optional templated expansion for ___FILE___.
 * @param exec_template String with command to run, containing optional ___FILE___ templating
 * @param note_path Filename/path of Node/note or item to run command on
 */
bool wibble::WibbleExecutor::run_exec_expansion(const std::string& exec_template, const std::string note_path) const
{
    if(exec_template == "" || note_path == "") { return false; }
    std::string built_exec_cmd;
    bool cmd_result;
    try
    {
        std::size_t start_file_delim = 0;
        start_file_delim = exec_template.find(FILE_DELIM, 0);

        // 1. simple case: just run <cmd> <filename>
        // no ___FILE__ template found, so simply try the command...
        if(start_file_delim == std::string::npos)
        {
            built_exec_cmd = exec_template + " " + note_path;
        }
        else
        {
            // 2. templated case: run <cmd> [ <filename> | <other cmds> | ... ]
            // keep splicing in actual filename in place of ___FILE__ placeholder
            built_exec_cmd = _splice_exec_cmd(start_file_delim, exec_template, FILE_DELIM, note_path);
        }

        wc.console_write_green(ARW + "Executing command: " + built_exec_cmd); wc.console_write("");
        wc.console_write_hz_divider();
        cmd_result = std::system(built_exec_cmd.c_str());
        wc.console_write_hz_divider();

        if(cmd_result == 0) { wc.console_write_success_green("Command successfully executed on record/file."); return true;    }
        else                { wc.console_write_error_red("Error executing command on record/file.");           return false;   }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error executing command on note.");
        wc.console_write_red("Requested execution template: " + exec_template);
        return false;
    }
}

/**
 * @brief Run multiple substitutions of a placeholder value in an execution template
 *
 * This function assumes/starts on the basis that the pattern does exist in the string
 * and should be used on this basis, having first identified its initial offset.
 *
 * @param start_offset position to start searching/replacing in string with
 * @param exec_template string with execution template to "fill in"
 * @param placeholder to search and substitute for
 * @param sub_val replacement value to put in for subsitute
 */
std::string wibble::WibbleExecutor::_splice_exec_cmd(const std::size_t start_offset, const std::string& exec_template,
                                                     const std::string& placeholder, const std::string& sub_val) const
{
    std::size_t start_file_delim = start_offset;
    const int OFFSET = placeholder.length();
    std::string built_exec_cmd = exec_template;

    while(start_file_delim != std::string::npos)
    {
        std::string pre_str = built_exec_cmd.substr(0, start_file_delim);
        std::string end_str = built_exec_cmd.substr(start_file_delim + OFFSET, std::string::npos);
        built_exec_cmd = pre_str + sub_val + end_str;
        start_file_delim = built_exec_cmd.find(placeholder);
    }

    return built_exec_cmd;
}


/**
 * @brief Wrapper around exec_expansion function for Wibble Node/notes.
 * @param record WibbleRecord/Node to run command on
 * @param exec_template String with command to run, containing optional ___FILE___ templating
 * @param archived Flag indicating whether node/note has been archived
 */
bool wibble::WibbleExecutor::note_exec_cmd(const WibbleRecord& record, const std::string& exec_template, const bool archived) const
{
    std::string note_path;
    if(! archived)
    {
        note_path = _build_note_path(record.get_id(), record.get_type(), record.get_proj(), false, true);
        wc.console_write_green(ARW + "Processing Node " + record.get_id() + "." + record.get_type() + " from main database.\n");
    }
    else
    {
        note_path = _build_note_path(record.get_id(), record.get_type(), record.get_proj(), true, true);
        wc.console_write_green(ARW + "Processing Node " + record.get_id() + "." + record.get_type() + " from archived database.\n");
    }

    return run_exec_expansion(exec_template, note_path);

}

/**
 * @brief Execution wrapper for Node/notes that allows separate substiutions for both ___FILE__ and ___EXEC___ placeholders.
 * @param record WibbleRecord/Node to run command on
 * @param exec_template String with templated command to expand
 * @param exec_cmd String with command to insert for ___EXEC___ placeholder
 * @param archived Flag indicating whether Node/note has been archived
 */
bool wibble::WibbleExecutor::note_exec_arbitrary_cmd(const WibbleRecord& record, const std::string& exec_template, const std::string& exec_cmd, const bool archived) const
{
    try
    {
        std::string note_path;
        std::string built_exec_cmd;
        bool cmd_result;
        if(! archived)
        {
            note_path = _build_note_path(record.get_id(), record.get_type(), record.get_proj(), false, true);
            wc.console_write_green(ARW + "Processing note " + record.get_id() + "." + record.get_type() + " from main database.\n");
        }
        else
        {
            note_path = _build_note_path(record.get_id(), record.get_type(), record.get_proj(), true, true);
            wc.console_write_green(ARW + "Processing note " + record.get_id() + "." + record.get_type() + " from archived database.\n");
        }

        std::size_t start_subst_delim = 0;
        built_exec_cmd = exec_template;

        // 1. substitute for ___FILE___
        start_subst_delim = exec_template.find(FILE_DELIM, 0);

        if(start_subst_delim != std::string::npos)
        {
            built_exec_cmd = _splice_exec_cmd(start_subst_delim, exec_template, FILE_DELIM, note_path);
        }

        // 2. substitute for ___EXEC___
        // build_exec_cmd now contains our half-substituted template
        start_subst_delim = built_exec_cmd.find(EXEC_DELIM, 0);

        if(start_subst_delim != std::string::npos)
        {
            built_exec_cmd = _splice_exec_cmd(start_subst_delim, built_exec_cmd, EXEC_DELIM, exec_cmd);
        }

        wc.console_write_green(ARW + "Executing command: " + built_exec_cmd); wc.console_write("");
        cmd_result = std::system(built_exec_cmd.c_str());

        if(cmd_result == 0) { return true; }
        else                { return false; }
    }
    catch(std::exception& ex)
    {
        //std::cerr << "Error executing command on note: " << ex.what() << std::endl;
        wc.console_write_error_red("Error executing command on note.");
        wc.console_write_red("Requested command: " + exec_cmd);
        wc.console_write_red("Requested execution template: " + exec_template);
        return false;
    }
    return true;
}

// ============ 4. Utility ============
/**
 * @brief Utility function, mainly for debugging, displaying current WibbleExecutor environment/configuration.
 */
void wibble::WibbleExecutor::display_configuration()
{
    std::cout << "================= [ FILES ] ===================" << std::endl;

    std::cout << "Archived DB             : " << get_archived_db() << std::endl;
    std::cout << "Main DB                 : " << get_main_db() << std::endl;

    std::cout << "================== [ PATHS ] ==================" << std::endl;

    std::cout << "Backup location         : " << get_backup_location() << std::endl;
    std::cout << "Bookmarks directory     : " << get_bookmarks_dir() << std::endl;
    std::cout << "Jotfile directory       : " << get_jot_dir() << std::endl;
    std::cout << "Tasks directory         : " << get_tasks_dir() << std::endl;
    std::cout << "Temporary directory     : " << get_tmp_dir() << std::endl;
    std::cout << "Topics directory        : " << get_topics_dir() << std::endl;
    std::cout << "Wibble storage directory: " << get_wibble_store() << std::endl;

    std::cout << "================ [ PROGRAMS ] =================" << std::endl;

    std::cout << "CLI editor              : " << get_cli_editor() << std::endl;
    std::cout << "Dump tool               : " << get_dump_tool() << std::endl;
    std::cout << "File manager            : " << get_fm_tool() << std::endl;
    std::cout << "GUI editor              : " << get_gui_editor() << std::endl;
    std::cout << "Pager tool              : " << get_pager() << std::endl;
    std::cout << "View tool               : " << get_view_tool() << std::endl;

    std::cout << "================== [ END ] ===================" << std::endl;
}

/**
 * @brief Helper method to display records to screen.
 * @param tot_results Long value with total number of results.
 */
void wibble::WibbleExecutor::_print_node_records(WibResultList& results, const long& tot_results, const bool extended_view)
{
    long l = 0;

    // performance optimisation for large result sets
    // record display functions entirely avoid std::endl so we can control flushing
    std::ios::sync_with_stdio(false);

    for(auto& result: *results)
    {
        if(extended_view) { result.pretty_print_record_full(l, tot_results);      }
        else              { result.pretty_print_record_condensed(l, tot_results); }
        ++l;
    }

    // re-enable sync
    std::ios::sync_with_stdio(true);
    std::cout << std::flush;
}

/**
 * @brief Allow range selection of Node records from user.
 * @param results Pointer to vector containing set of WibbleRecords that represent result set
 * @param extended_view Flag controlling whether to use extended/verbose/multiline record listing
 */
wibble::WibResultList wibble::WibbleExecutor::range_select_record_selection(WibResultList& results, const bool extended_view)
{
    long tot_results = w_rg.result_size(results);
    _print_node_records(results, tot_results, extended_view);

    std::cout << std::unitbuf;
    wc.console_write_yellow("\n" + ARW + "Select item(s) using comma separated list/numbered ranges.\n" + ARW + "Input '*' for all records.\n\n");
    const std::set<int> res = wc.console_present_range_record_selector(tot_results);
    WibResultList subset_results (new std::vector<WibbleRecord>);
    if(res.size()  == 1 && res.count(NULL_INT_VAL) == 1) { return std::move(results); } // case of '*' input
    for(auto const& item: res) { subset_results->emplace_back(results->at(item - 1)); }
    return subset_results;
}

/**
 * @brief Core function that presents a user input selector from a list of WibbleRecord/Node results.
 * @param results Pointer to vector containing set of WibbleRecords that represent result set
 * @param extended_view Flag controlling whether to use extended/verbose/multiline record listing
 */
long wibble::WibbleExecutor::select_valid_record_or_exit(WibResultList& results, const bool extended_view)
{
    long tot_results = w_rg.result_size(results);
    _print_node_records(results, tot_results, extended_view);

    long response = wc.console_present_record_selector(tot_results);
    if(response == 0) { wc.console_write_error_red("Invalid record selection. Exiting."); throw wibble::wibble_exit(0); }
    return response;
}

/**
 * @brief Minor helper function to get total results for padding purposes/on-screen display formatting.
 * @param results Pointer to vector containing set of WibbleRecords that represent result set
 */
long wibble::WibbleExecutor::get_tot_results(WibResultList& results)
{
    if(results) { return w_rg.result_size(results); }
    else        { return 0; }
}

/**
 * @brief Core function to actually parse and retrieve set of WibbleRecord/Node results from search.
 * @param SEARCH Set of query/search filters to filter WibbleRecord set with
 */
wibble::WibResultList wibble::WibbleExecutor::perform_search(wibble::search_filters& SEARCH, const std::string& override_db)
{
    bool search_initiated = false;
    std::string DB_FILE = "";
    if(override_db == "")
    {
        if(! SEARCH.archive_db) { DB_FILE = main_db; }
        else                    { DB_FILE = archived_db; }
    }
    else { DB_FILE = override_db; }

    const std::string WORKFILE = tmp_dir + WORKSTUB;

    // clean start; obliterate any previous search results, if they exist
    if(std::filesystem::exists(std::filesystem::path(WORKFILE)))
    { std::filesystem::remove(std::filesystem::path(WORKFILE)); }
    if(std::filesystem::exists(std::filesystem::path(WORKFILE + "2")))
    { std::filesystem::remove(std::filesystem::path(WORKFILE + "2")); }

    //std::cerr << "DEBUG: Using DB_FILE = " << DB_FILE << '\n';
    //std::cerr << "DEBUG: Using WORKFILE = " << WORKFILE << '\n';
    // handle case of brand new database/first run of Wibble - main db
    if(! std::filesystem::exists(std::filesystem::path(main_db)))
    {
        wibble::WibbleIO::init_database_file(wibble::WibbleRGDatabase::_rgdb_header(), main_db);
    }

    // handle case of brand new database/first run of Wibble - archive db
    if(! std::filesystem::exists(std::filesystem::path(archived_db)))
    {
        wibble::WibbleIO::init_database_file(wibble::WibbleRGDatabase::_rgdb_header(), archived_db);
    }

    // handle case of brand new database for override/export database
    if(! std::filesystem::exists(std::filesystem::path(DB_FILE)))
    {
        wibble::WibbleIO::init_database_file(wibble::WibbleRGDatabase::_rgdb_header(), DB_FILE);
    }

    // handle case of just dumping all records, early exit
    if(SEARCH.everything)
    {
        //wc.console_write_green(ARW + "Bypassing search, ALL records requested.\n");
        return w_rg.get_all_wibble_records(DB_FILE, WORKFILE);
    }

    // handle case of full-text/content search, alternative exit path
    if(SEARCH.content_srch != "")
    {

        if(SEARCH.date  != "" ||
           SEARCH.title != "" ||
           SEARCH.id    != "" ||
           SEARCH.desc  != "" ||
           SEARCH.type  != "" ||
           SEARCH.proj  != "" ||
           SEARCH.tags  != "" ||
           SEARCH.cls   != "" ||
           SEARCH.dd    != "" ||
           SEARCH.kp    != "" ||
           SEARCH.cust  != "" ||
           SEARCH.lids  != "")
        {
            wc.console_write_error_red("Full content search cannot be combined with a database/field search. Aborting.");
            throw wibble::wibble_exit(1);
        }
        const std::string node_root_dir = (! SEARCH.archive_db ) ? wibble_store + "/notes" : wibble_store + "/archived_notes";

        if(! SEARCH.archive_db)
        {
            wc.console_write_yellow(ARW + "Performing FULL-TEXT search through all Nodes.\n");
        }
        else
        {
            wc.console_write_yellow(ARW + "Performing FULL-TEXT search through all ");
            wc.console_write_red("archived "); wc.console_write_yellow("Nodes.\n");
        }
        wc.console_write_yellow(ARW + "Search term: "); wc.console_write_green_bold(SEARCH.content_srch + '\n');

        return WibbleUtility::get_wibble_records_fulltext(wc, *this, SEARCH.content_srch, node_root_dir, WORKFILE,
                                                          SEARCH.archive_db, SEARCH.fixed_strings);
    }

    wibble::WibbleSearch ws(DB_FILE, WORKFILE, WibBinList().gen_bin_list(bin_mapping, templates_dir));

    // ensure no garbage on search fields
    wc.trim(SEARCH.date);
    wc.trim(SEARCH.title);
    wc.trim(SEARCH.id);
    wc.trim(SEARCH.desc);
    wc.trim(SEARCH.type);
    wc.trim(SEARCH.proj);
    wc.trim(SEARCH.tags);
    wc.trim(SEARCH.cls);
    wc.trim(SEARCH.dd);
    wc.trim(SEARCH.kp);
    wc.trim(SEARCH.cust);
    wc.trim(SEARCH.lids);

    bool exact_on = false;
    bool fixed_on = false;
    if(SEARCH.exact)         { exact_on = true; }
    if(SEARCH.fixed_strings) { fixed_on = true; }

    // inspect for and determine whether we need to add a search frame/iteration for
    // each particular field

    // # 1/12
    if(SEARCH.date != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_DATE;
        s.field = s_field;
        s.query = SEARCH.date;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 2/12
    if(SEARCH.title != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_TITLE;
        s.field = s_field;
        s.query = SEARCH.title;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 3/12
    if(SEARCH.id != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_NOTEID;
        s.field = s_field;
        s.query = SEARCH.id;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 4/12
    if(SEARCH.desc != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_DESC;
        s.field = s_field;
        s.query = SEARCH.desc;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 5/12
    if(SEARCH.type != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_TYPE;
        s.field = s_field;
        s.query = SEARCH.type;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 6/12
    if(SEARCH.proj != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_PROJ;
        s.field = s_field;
        s.query = SEARCH.proj;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 7/12
    if(SEARCH.tags != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_TAGS;
        s.field = s_field;
        s.query = SEARCH.tags;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 8/12
    if(SEARCH.cls != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_CLS;
        s.field = s_field;
        s.query = SEARCH.cls;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 9/12
    if(SEARCH.dd != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_DD;
        s.field = s_field;
        s.query = SEARCH.dd;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 10/12
    if(SEARCH.kp != "")
    {
        // dispatch out to the KeyPair assistance function; each KeyPair
        // needs its own search frame, since they are filtered individually
        std::vector<WibbleSearch::search_frame> kp_search = _generate_search_frames_for_keypair_string(wc, SEARCH.kp);
        for(auto const& itm: kp_search) { ws.add_search_frame(itm); }
        if(kp_search.size() > 0)        { search_initiated = true;  }
    }

    // # 11/12
    if(SEARCH.cust != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_CUST;
        s.field = s_field;
        s.query = SEARCH.cust;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    // # 12/12
    if(SEARCH.lids != "")
    {
        wibble::WibbleSearch::search_frame s;
        wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_LIDS;
        s.field = s_field;
        s.query = SEARCH.lids;
        if(exact_on) { s.exact_search = true; }
        if(fixed_on) { s.fixed_string = true; }
        ws.add_search_frame(s);
        search_initiated = true;
    }

    if(! search_initiated)
    {
        wc.console_write_green(ARW + "No search criteria specified - displaying ALL records.\n");
        // entirely bypass ripgrep search operation
        return w_rg.get_all_wibble_records(DB_FILE, tmp_dir + WORKSTUB);
    }
    return ws.execute_search_pipeline();
}


/**
 * @brief Wrapper for selecting a particular record based on user input.
 * @param list Vector (pointer at) of WibbleRecords to select from
 * @param response Index/number of record to retrieve
 */
wibble::WibbleRecord wibble::WibbleExecutor::get_record_from_list(WibResultList& list, const unsigned long response)
{
    return w_rg.select_record(list, response - 1);
}


/**
 * @brief Function to let the user interactively create a new metadata schema.
 */
bool wibble::WibbleExecutor::create_schema_interactive()
{
    wc.console_write_header("SCHEMA");
    std::string s_name = wc.console_write_prompt("Schema name *: ", "", "");
    wc.trim(s_name);
    if(s_name == "")
    {
        wc.console_write_error_red("Schema name cannot be empty. Aborting.");
        return false;
    }

    wibble::WibbleRecord::sanitise_input(s_name);

    const std::string of_schema = schemas_dir + "/" + s_name;
    const std::string of_path = WibbleIO::get_parent_dir_path(of_schema);

    if(! std::filesystem::exists(std::filesystem::path(of_path)))
    {
        if(! wibble::WibbleIO::create_path(of_path)) { wc.console_write_error_red("Unable to write out to schema directory."); return false; }
    }

    if (std::filesystem::exists(std::filesystem::path(of_schema)))
    {
        wc.console_write_error_red("A defined schema with that filename/location already exists! Aborting.");
        return false;
    }

    wibble::WibbleRecord::sanitise_input(s_name);

    wc.console_write_yellow("\nFor the following fields/prompts that follow, input the\n");
    wc.console_write_yellow("default value you want pre-filled. Note that inputting\n");
    wc.console_write_yellow("a prefilled filetype value will cause that filetype to be\n");
    wc.console_write_yellow("automatically used when employing the schema.\n\n");
    wc.console_write_yellow("Leave blank to skip a field.\n\n");

    std::string s_des, title, type, desc, proj, tags, cls, kp, cust;

    s_des = wc.console_write_prompt("Optional schema description: ", "", ""); wc.trim(s_des);
    wc.console_write("");
    title = wc.console_write_prompt("Title      : ", "", ""); wc.trim(title);
    type  = wc.console_write_prompt("Filetype   : ", "", ""); wc.trim(type); WibbleRecord::sanitise_input(type);
    desc  = wc.console_write_prompt("Description: ", "", ""); wc.trim(desc);
    proj  = wc.console_write_prompt("Project    : ", "", ""); wc.trim(proj); WibbleRecord::sanitise_input(proj);
    tags  = wc.console_write_prompt("Tags       : ", "", ""); wc.trim(tags);
    cls   = wc.console_write_prompt("Class      : ", "", ""); wc.trim(cls);
    cust  = wc.console_write_prompt("Custom     : ", "", ""); wc.trim(cust);
    //kp    = wc.console_write_prompt("KeyPairs   : ", "", ""); wc.trim(kp);

    std::string add_kp   = wc.console_write_prompt("\nDefine KeyPairs? (y/n) > ", "", "");
    std::set<std::string> key_pairs;
    if(add_kp == "y" || add_kp == "Y")
    {
        wc.console_write_hz_divider();
        wc.console_write_yellow("NOTE: Enter blank key or value to stop adding.\n\n");
        bool keep_adding;
        do { keep_adding = WibbleRecord::keypair_helper(wc, key_pairs); } while(keep_adding);
    }
    for(auto const& val: key_pairs) { kp.append(val + " "); }


    std::string schema_content = "";
    wc.console_write("");
    wc.console_write_header("CONFIRM");
    if(s_des != "") { wc.console_write_yellow_bold("Schema desc: "); wc.console_write(s_des);  schema_content.append("# " + s_des + "\n");          }
    if(title != "") { wc.console_write_yellow_bold("Title      : "); wc.console_write(title);  schema_content.append("TITLE    | " + title + "\n"); }
    if(type != "")  { wc.console_write_yellow_bold("Filetype   : "); wc.console_write(type);   schema_content.append("FILETYPE | " + type + "\n");  }
    if(desc != "")  { wc.console_write_yellow_bold("Description: "); wc.console_write(desc);   schema_content.append("DESC     | " + desc + "\n");  }
    if(proj != "")  { wc.console_write_yellow_bold("Project    : "); wc.console_write(proj);   schema_content.append("PROJECT  | " + proj + "\n");  }
    if(tags != "")  { wc.console_write_yellow_bold("Tags       : "); wc.console_write(tags);   schema_content.append("TAGS     | " + tags + "\n");  }
    if(cls != "")   { wc.console_write_yellow_bold("Class      : "); wc.console_write(cls);    schema_content.append("CLASS    | " + cls + "\n");   }
    if(kp != "")    { wc.console_write_yellow_bold("KeyPairs   : "); wc.console_write(kp);     schema_content.append("KEYPAIRS | " + kp + "\n");    }
    if(cust != "")  { wc.console_write_yellow_bold("Custom     : "); wc.console_write(cust);   schema_content.append("CUSTOM   | " + cust + "\n");  }

    wc.console_write_hz_heavy_divider();

    std::string resp = wc.console_write_prompt("Create the following schema definition? (y/n) > ", "", "");
    wc.console_write("");
    if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); return false; }

    bool wo = WibbleIO::write_out_file_overwrite(schema_content, of_schema);
    if(wo)
    {
        wc.console_write_success_green("Schema definition written out successfully.");
        wc.console_write_green(ARW + "Use with wibble++ create --schema " + s_name + "\n");
    }
    else
    { wc.console_write_error_red("Error creating output schema definition."); return false; }

    return true;
}


/**
 * @brief Passthrough function to directly persist a Node to database file from import
 * @param w WibbleRecord/Node to actually export
 * @param update flag controlling whether to update existing Node
 * @param archive_db flag indicating whether Node exists in archive database
 */
bool wibble::WibbleExecutor::create_node_passthrough(WibbleRecord& w, const bool archive_db, const bool update)
{
    if(update) { return op_update_existing_record(w, archive_db); }
    else
    {
        if(! archive_db) { return w_rg.persist_record(true, w, main_db, tmp_dir + WORKSTUB, wibble_store);     }
        else             { return w_rg.persist_record(true, w, archived_db, tmp_dir + WORKSTUB, wibble_store); }
    }
}

/**
 * @brief Passthrough function to export/persist a Node to new datagbase file for import
 * @param w WibbleRecord/Node to actually export
 * @param export_path Root path/base directory of exported content
 * @param archived flag indicating whether Node exists in archive database
 */
bool wibble::WibbleExecutor::export_node(const WibbleRecord& w, const std::string& export_path, const bool archived)
{
    // 1. First check to ensure Node has not already been exported to this destination; return immediately if so
    // 2. Replicate the database entry/database metadata
    // 3. Copy the actual Node content file itself from orignal tree -> export tree
    // 4. Check if Node has a data dir; if it exists, copy data directory -> export tree

    const std::string export_nd_path = export_path + "/wibble_bundle/nodes";
    const std::string export_db_file = "/nodes.wib";
    // #0 Setup initial export path/db file
    bool create_path = WibbleIO::create_path(export_nd_path);
    if(! create_path) { wc.console_write_error_red("Error creating export path: '" + export_nd_path + "'."); return false; }

    // #1 Check record is not already exported
    wc.silence_output();
    search_filters SEARCH;
    SEARCH.exact = true;
    SEARCH.id = w.get_id();
    const WibResultList existing_results = perform_search(SEARCH, export_nd_path + export_db_file);
    wc.enable_output();
    if(existing_results->size() == 1) { wc.console_write_success_green("Node " + w.get_id() + " already exported into destination."); return false; }

    // #2 Replicate metadata/database entry
    const bool persist = w_rg.export_record(w, export_nd_path + export_db_file);
    if(! persist) { wc.console_write_error_red("Error exporting Node database record into '" + export_nd_path + export_db_file + "'."); return false; }

    // #3 Copy the actual Node content file itself
    const bool create_content_path = WibbleIO::create_note_path(export_nd_path, w.get_id(), w.get_type(), w.get_proj(), true, archived);
    if(! create_content_path) { wc.console_write_error_red("Error creating Node content export path."); return false; }
    const std::string node_src_file_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, true);
    const std::string node_dest_file_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, true, export_nd_path);
    if(! std::filesystem::exists(std::filesystem::path(node_src_file_path)))
    { wc.console_write_error_red("No Node file located at: '" + node_src_file_path + "'."); return false; }
    const bool copy_ct_file = WibbleIO::wcopy_file(node_src_file_path, node_dest_file_path);
    if(! copy_ct_file) { wc.console_write_error_red("Error copying Node content file."); return false; }

    // #4 Check if Node has a data directory present; if so, replicate that too
    const std::string node_src_data_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, false);
    if(std::filesystem::exists(std::filesystem::path(node_src_data_path)))
    {
        wc.console_write_green(ARW + "Node has data directory. Propagating.\n");
        const std::string node_dest_data_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, false, export_nd_path);
        const bool create_data_dest_path = WibbleIO::create_path(WibbleIO::get_parent_dir_path(node_dest_data_path));
        if(! create_data_dest_path) { wc.console_write_error_red("Error creating output data directory."); return false; }
        const bool copy_data_file = WibbleIO::wcopy_file(node_src_data_path, node_dest_data_path);
        if(! copy_data_file) { wc.console_write_error_red("Error copying Node data directory."); return false; }
    }

    wc.console_write_success_green("Node " + w.get_id() + " successfully exported.");
    return true;
}

/**
 * @brief Function to let user interactively create a new template for a given filetype.
 * @param create_default Flag controlling whether this should be the filetype 'default' template
 */
bool wibble::WibbleExecutor::create_template_interactive(const bool create_default)
{
    wc.console_write_header("TEMPLATE");
    std::string t_type = wc.console_write_prompt("Template filetype *: ", "", "");
    wc.trim(t_type);
    if(t_type == "")
    {
        wc.console_write_error_red("Template filetype cannot be empty. Aborting.");
        return false;
    }

    // get/check whether it is a binary type
    const std::string bin_edit_cmd = WibbleUtility::get_binary_tool(wc, t_type, bin_mapping, true);
    if(create_default && bin_edit_cmd != "")
    {
        wc.console_write_error_red("Filetype '" + t_type + "' is registered as a binary type.");
        wc.console_write_red("It therefore has a default template. Edit the file at:\n\n");
        wc.console_write(" " + templates_dir + "/bin_default/default." + t_type + "\n");
        wc.console_write_red("If you wish to edit the default template for this filetype.\n");
        return false;
    }
    std::string t_name = "default";
    if(! create_default)
    {
        t_name = wc.console_write_prompt("Template name     *: ", "", "");
        wc.trim(t_name);
    }

    if(t_name == "")
    {
        wc.console_write_error_red("Template name cannot be empty. Aborting.");
        return false;
    }

    wibble::WibbleRecord::sanitise_input(t_type);
    wibble::WibbleRecord::sanitise_input(t_name);

    wc.console_write_hz_divider();
    if(create_default) { wc.console_write_success_green("Creating default template for filetype: " + t_type);                  }
    else               { wc.console_write_success_green("Creating a template named '" + t_name + "' for filetype: " + t_type); }

    if(bin_edit_cmd == "")
    {
        wc.console_write_hz_divider();
        wc.console_write("1. Edit template with CLI editor.");
        wc.console_write("2. Edit template with GUI editor.");
        wc.console_write_hz_divider();
    }
    const std::string template_ph = templates_dir + "/" + t_type;
    const std::string template_fn = templates_dir + "/" + t_type + "/" + t_name;
    bool unopened_df = false;
    if(! std::filesystem::exists(std::filesystem::path(template_fn)))
    {
        if(wibble::WibbleIO::create_path(template_ph))
        {
            if(bin_edit_cmd == "")
            {
                const std::string dft = wibble::WibbleRecord::get_default_template_str();
                try                       { std::ofstream(template_fn).write(dft.c_str(), dft.length()); unopened_df = true; }
                catch(std::exception& ex)
                { wc.console_write_error_red("Error creating template: "); std::cerr << ex.what() << std::endl; return false; }
            }
            else // binary template
            {
                bool setup_stub = WibbleIO::wcopy_file(templates_dir + "/bin_default/default." + t_type, template_fn);
                if(! setup_stub) { wc.console_write_error_red("Unable to create template output file: " + template_fn); return false; }
            }
        }
        else
        {
            wc.console_write_error_red("Unable to create template output directory: " + template_ph);
            return false;
        }
    }

    if(bin_edit_cmd == "")
    {
        std::string option = wc.console_write_prompt("Select option > ", "", "");
        if(option == "1")      { run_exec_expansion(cli_editor, template_fn); }
        else if(option == "2") { run_exec_expansion(gui_editor, template_fn); }
        else
        {
            wc.console_write_error_red("Invalid choice. Exiting.");
            if(unopened_df && std::filesystem::exists(std::filesystem::path(template_fn)))
            { std::filesystem::remove(std::filesystem::path(template_fn)); }
            return false;
        }
    }
    else { return run_exec_expansion(bin_edit_cmd, template_fn); }

    wc.console_write_success_green("Template for filetype: " + t_type + " created successfully.");
    wc.console_write_success_green("Template filename/path: " + template_fn);
    return true;
}


/**
 * @brief Process/determine file listing within the Node/note data directory.
 * @param w WibbleRecord/Node to attach data to
 * @param archive Flag indicating whether we're working with an archived Node/note
 * @param WORKFILE String with filename to temporary working/cache file
 * @param filter String with optional filename substring to match against/filter by
 */
std::string wibble::WibbleExecutor::generate_dd_list(WibbleRecord& w, const bool& archived, const std::string WORKFILE, const std::string& filter) const
{
    try
    {
        const std::string the_path = _build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived, false);
        const std::filesystem::path dd_path{the_path};
        // std::cerr << "Data directory is: " << the_path << std::endl;
        wc.console_write_green(ARW + "Processing data directory content: " + the_path + '\n');
        if(std::filesystem::exists(dd_path))
        {
            std::string file_list;
            for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{dd_path})
            {
                if(dir_entry.is_regular_file())
                {
                    if(filter != "")
                    {
                        if(std::string(dir_entry.path()).find(filter) != std::string::npos)
                        {
                            file_list.append(std::string(dir_entry.path()) + "\n");
                        }
                    }
                    else { file_list.append(std::string(dir_entry.path()) + "\n"); }
                }
            }

            // remove any existing workfile
            if(std::filesystem::exists(std::filesystem::path(WORKFILE))) { std::filesystem::remove(WORKFILE); }
            std::ofstream(WORKFILE).write(file_list.c_str(), file_list.length());
            return WORKFILE;
        }
        else
        {
            wc.console_write_error_red("No data directory found at: '" + the_path + "'.");
            return "";
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error processing directory: " << ex.what() << std::endl;
        return "";
    }
}
