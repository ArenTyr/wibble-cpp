/**
 * @file      cmd_topic.cpp
 * @brief     Handle "topic" subcommand.
 * @details
 *
 * This class handles the dispatching logic for the "topic"
 * functionality of Wibble++.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_topic.hpp"
#include "wibbleexit.hpp"
#include "wibbletopics.hpp"
#include "wibbletopicmanager.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Dispatching logic for the "topic" functionality.
 * @param OPTS package/general configuration options struct
 * @param TOPIC command line options/parameters supplied via switches
 */
void wibble::WibbleCmdTopic::handle_topic(pkg_options& OPTS, topic_options& TOPIC)
{
    WibbleConsole wc;
    WibbleTopics wt{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
    WibbleTopicManager wtm{OPTS.exec.bin_mapping};

    // reactive a hidden/hibernated topic
    if(TOPIC.reactivate_topic)
    {
        // fill the map only with hibernated topics
        wt.accumulate_topic_map(wc, OPTS.paths.topics_dir, true);
        wtm.reactivate_hibernated_topic(OPTS, wt);
        throw wibble::wibble_exit(0);
    }

    // otherwise, for everything else, we want all of active/unhibernated topics
    wt.accumulate_topic_map(wc, OPTS.paths.topics_dir);

    if(TOPIC.hibernate_topic)
    {
        wtm.hibernate_a_topic(OPTS, wt);
        throw wibble::wibble_exit(0);
    }
    
    // flags which allow use of a custom date for the new entry rather than current datetime
    wc.trim(TOPIC.override_date);
    wc.trim(TOPIC.override_data_date);

    if(TOPIC.add_entry_to_topic != "UNSET") 
    {
        // #1. Add a new topic entry
        wtm.add_new_topic_entry(OPTS, TOPIC, wt); 
    }
    else if(TOPIC.migrate_topic != "UNSET" ||
            TOPIC.migrate_data_topic != "UNSET")
    {
        // #2. Migrate an existing topic entry
        wtm.migrate_topic_entry(OPTS, TOPIC, wt);
    }
    else if(TOPIC.create_edit_default_template != "UNSET")
    {
        // #3. Edit default template for a topic
        wtm.topic_default_template(OPTS, TOPIC, wt);
    }
    else if(TOPIC.create_topic)
    {
        // #4. Create an entirely new topic
        wt.create_new_topic(wc, OPTS.paths.topics_dir);
    }
    else if(TOPIC.list_topics)
    {
        // #5. List currently existing topics
        wt.list_available_topics(wc);
    }
    else if(TOPIC.describe_topics)
    {
        // #6. Describe currently existing topics
        wt.describe_topic_definitions(wc);
    }
    else if(TOPIC.configure_existing_topic != "UNSET")
    {
        // #7. Configure (edit) an existing topic
        std::cerr << "configure() not implemented yet" << std::endl;
    }
    else if(TOPIC.expunge_topic)
    {
        // #8. Permanently remove an entire topic
        wt.expunge_topic(wc, OPTS.paths.topics_dir);
    }
    else if(TOPIC.copy_data_file != "UNSET"   ||
            TOPIC.move_data_file != "UNSET"   ||
            TOPIC.open_data_file != "UNSET"   ||
            TOPIC.delete_data_file != "UNSET" ||
            TOPIC.cd_data_dir != "UNSET")

    {
        // #9. add new topic data file entry (copy/move),
        // or open/delete existing topic data file entry
        wtm.dispatch_data_topic_entry(OPTS, TOPIC, wt);
    }
    else 
    {
        // #10. Everything related to viewing/filtering/opening topic entries (NON data):
        // --edit, --delete, --exec-cmd, --view, etc...
        wtm.process_topic_entry(OPTS, TOPIC, wt);
    } 
} 
