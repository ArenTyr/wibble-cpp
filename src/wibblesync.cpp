/**
 * @file      wibblesync.cpp
 * @brief     Provides functionality for the "sync" subcommand.
 * @details
 *
 * This class provides the functionality for the "sync" subcommand.
 * This offers a form of semi-automated file versioning based
 * on manual curation/injection of WIBBLE_VERSION time stamps.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibbleexit.hpp"
#include "wibblesync.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Copies entirely new tracked files from source directory to destination directory, processing file_list vector.
 * @param base_dir String with value of base/root source directory
 * @param dest_base String with value of base/root destination directory
 * @param file_list Vector of file_info structs with file information/metadata
 */
void wibble::WibbleSync::_copy_new_files(std::string base_dir, std::string dest_base, std::vector<file_info>& file_list)
{
    if(file_list.size() == 0) { wc.console_write_green(ARW + "Nothing to do. No new timestamped files.\n"); }
    else
    {
        wibble::WibbleUtility::remove_trailing_slash(base_dir);
        wibble::WibbleUtility::remove_trailing_slash(dest_base);
        for(auto const& f: file_list)
        {
            const std::string src_file = f.file_path;
            const std::string dest_file = dest_base + std::filesystem::path::preferred_separator + wibble::WibbleIO::get_path_minus_base_dir(base_dir, f.file_path);
            const std::string dest_dir = std::filesystem::path(dest_file).parent_path();
            //std::cerr << "Dest dir : " << dest_dir << std::endl;
            bool ensure_dir = wibble::WibbleIO::create_path(dest_dir);
            if(ensure_dir)
            {
                bool copy_file = wibble::WibbleIO::wcopy_file(src_file, dest_file);
                wc.console_write_green(ARW + "Copying "); wc.console_write_green_bold("[NEW]"); wc.console_write_green(" file    :"); 
                if(copy_file) { wc.console_write_green_bold(" [  OK ] ");   }
                else          { wc.console_write_red_bold  (" [ ERR ] ");   }
                wc.console_write(src_file + " > " + dest_file);
            }
            else { wc.console_write_error_red("Unable to create output directory: " + dest_dir + ". Check permissions."); }
        }
    }
}

/**
 * @brief Attempts to convert the detected timestamp time string to a time_t value.
 * @param datetimestring String with the human readable ISO timestamp YYYY-MM-DD hh:mm:ss
 */
time_t wibble::WibbleSync::_extract_version_timestamp(const std::string& datetimestring)
{
    return wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str(datetimestring, true, true);
}

/**
 * @brief Walks through directory tree and processes/collects file information.
 * @param root_dir String representing base directory to recurse down into 
 */
std::vector<wibble::WibbleSync::file_info> wibble::WibbleSync::_parse_directory_tree(std::string root_dir)
{
    wibble::WibbleUtility::remove_trailing_slash(root_dir);
    std::vector<file_info> file_list;
    if(! std::filesystem::exists(root_dir)) { /*wc.console_write_red(ARW + "Note: Directory not readable/does not exist: " + root_dir + "\n"); */return file_list; } 

    for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{root_dir})
    {
        if(dir_entry.is_regular_file() && wibble::WibbleIO::check_if_text_file(std::string(dir_entry.path())))
        {
            const std::string input_file = std::string(dir_entry.path());
            wc.console_write_green(ARW + "Inspecting file: " + input_file + "\n");
            std::stringstream file_ss = wibble::WibbleIO::read_file_into_stringstream(input_file);
            std::string line;
            std::string t_stamp = "";
            time_t timestamp{0};
            bool first_line = false;
            bool found_timestamp = false;
            bool found_desc = false;
            bool desc_app = false;
            std::string description = "";
            while(getline(file_ss, line, '\n'))
            {
                std::size_t ver_index = line.find(VER_TOKEN);
                std::size_t desc_open = line.find(DESC_START_TOKEN);
                std::size_t desc_close = line.find(DESC_END_TOKEN);

                if(ver_index != std::string::npos)
                {
                    std::size_t timestamp_open = line.find("\"", ver_index + VER_TOKEN.length());
                    std::size_t timestamp_close = line.find("\"", timestamp_open + 1);

                    if(timestamp_open != std::string::npos && timestamp_close != std::string::npos)
                    {
                        t_stamp = line.substr(timestamp_open + 1, (timestamp_close - 1 - timestamp_open));
                        //std::cout << "DEBUG: got timestamp: '" << t_stamp << "'" << std::endl;
                        timestamp = _extract_version_timestamp(t_stamp);
                        found_timestamp = true;
                    }
                }
                
                // most of the below logic is related to extracting the description
                // whether single line or multiline
                if(desc_open != std::string::npos)
                {
                    desc_app = true;
                    first_line = true;
                    // handle case of a single line description
                    if(desc_close != std::string::npos)
                    {
                        std::size_t desc_line = desc_close - (desc_open + DESC_START_TOKEN.length());
                        description = line.substr(desc_open + DESC_START_TOKEN.length() + 1, desc_line - 1);
                        desc_app = false;
                        //std::cout << "DEBUG: single line desc: '" << description << "'" << std::endl;
                        found_desc = true;
                    }
                }

                // multi-line description
                if(desc_app && desc_close == std::string::npos)
                {
                    std::string desc_line;
                    if(first_line)
                    {
                        desc_line = line.substr(desc_open + DESC_START_TOKEN.length(), line.length() - desc_open);
                        wc.trim(desc_line);
                        if(desc_line.length() > 0) { description.append(desc_line + '\n'); }
                        first_line = false;
                    }
                    else
                    {
                        // truncate/remove first two characters: assumption is descriptions will usually
                        // be present in source code files/inside a comment block or similar
                        desc_line =  line.substr(2, line.length() - 2);
                        wc.trim(desc_line);
                        if(desc_line.length() > 0) { description.append(desc_line + '\n'); }
                    }
                }

                // stop adding to description
                if(desc_close != std::string::npos)
                {
                    if(desc_app)
                    {
                        found_desc = true;
                        // remove trailing newline
                        if(description.length() >= 1) { description = description.substr(0, description.length() - 1); }
                    }
                    desc_app = false;
                }

                // can immediately stop reading file if we've got timestamp and description
                if(found_timestamp && found_desc) { break; } 
            }

            file_info wfile{};
            wfile.file_path = std::string(dir_entry.path());
            wfile.rel_path = wibble::WibbleIO::get_path_minus_base_dir(root_dir, dir_entry.path());
            wfile.file_name = std::string(std::filesystem::path(dir_entry.path()).filename());

            if(found_timestamp && t_stamp != "" && timestamp != NULLDATETIME)
            {
                wfile.timestamp = timestamp;
                wfile.time_string = t_stamp;
                if(found_desc) { wfile.file_desc = description;             }
                else           { wfile.file_desc = "No description\n";      }
            }
            else
            {
                wfile.timestamp = 0;
                wfile.time_string = "NONE";
                if(found_desc) { wfile.file_desc = description;             }
                else           { wfile.file_desc = "No description\n";      }
                //std::cerr << "t_stamp = " << t_stamp << '\n';
                //std::cerr << "timestamp = " << timestamp << '\n';
                //std::cerr << "no timestamp file = " << dir_entry.path() << '\n';
            }

            file_list.push_back(wfile);
        }
        else if(dir_entry.is_directory()) { wc.console_write_yellow(ARW + "Entering directory: " + std::string(dir_entry.path()) + "\n"); }
        else
        {
            wc.console_write_yellow(ARW + "Skipping file (binary or non-regular file): " + std::string(dir_entry.path()) + "\n");
        }
    }

    return file_list;
}

/**
 * @brief Display summary of individual file result after parsing synchronisation candidates/directory walk.
 * @param file_info Struct containing all file information
 * @param type Integer switch to control synchronisation indicator graphic: 1 = untracked, 2 = no update, 3 = update, 4 = reverse synchronise, 5 = new file
 * @param ARCHIVE command line options/parameters supplied via switches
 */
void wibble::WibbleSync::_print_summary(const file_info& f, const int type, const bool condensed)
{
        switch(type)
        {
        case 1:
            wc.console_write_grey_bold("──────");
            break;
        case 2:
            wc.console_write_yellow_bold("◄────►");
            break;
        case 3:
            wc.console_write_green_bold("─────►");
            break;
        case 4:
            wc.console_write_red_bold("◄─────");
            break;
        case 5:
            wc.console_write_green_bold("─────►");
            break;
        }

        wc.console_write(" " + f.rel_path + " [ " + f.time_string + " ]"); 

        if(! condensed)
        {
            wc.console_write("       " + f.file_path);
            wc.console_write_yellow("Description:\n");
            wc.console_write_yellow("---\n");
            wc.console_write(f.file_desc);
            wc.console_write_yellow("---\n");
        }
}


/**
 * @brief Actually perform copy/update/overwrite synchornisation across file list.
 * @param file_list Vector of tuples of (source, destination) file_info structs
 * @param reverse_pairwise Flag to reverse operation; used for reverse synchronisation when copying newer destination files back to source
 */
void wibble::WibbleSync::_synchronise_files(std::vector<std::pair<file_info, file_info>>& file_list, const bool reverse_pairwise)
{
    if(file_list.size() == 0) { wc.console_write_green(ARW + "Nothing to do. All files are up to date.\n"); }
    else
    {
        for(auto const& p: file_list)
        {
            const std::string ft        = (! reverse_pairwise) ? "[UPDATED]"       : "[NEWER]";
            const std::string src_file  = (! reverse_pairwise) ? p.first.file_path  : p.second.file_path;
            const std::string dest_file = (! reverse_pairwise) ? p.second.file_path : p.first.file_path;
            const std::string spacing   = (! reverse_pairwise) ? "" : "  ";
            bool copy_file = wibble::WibbleIO::wcopy_file(src_file, dest_file);
            wc.console_write_green(ARW + "Copying ");
            if(! reverse_pairwise) { wc.console_write_yellow_bold(ft); }
            else                   { wc.console_write_red_bold(ft);    }
            wc.console_write_green(" file" + spacing + ":");
            if(copy_file) { wc.console_write_green_bold(" [  OK ] ");   }
            else          { wc.console_write_red_bold  (" [ ERR ] ");   }
            wc.console_write(src_file + " > " + dest_file);
        }
    }
}


/**
 * @brief Calculate all of the file deltas/synchronisation candidates and assign into their relevant "buckets".
 * @param src_file String specifying root source directory
 * @param dest_file String specifying equivalent destination directory
 * @param ls_only Flag to indicate simply doing a "dry run"; display potential operations then exit
 * @param detailed_ls Flag to switch on detailed listing, namely output WIBBLE_DESC description values as well
 * @param interactive Flag to switch off interactivity, for use with auto_action
 * @param auto_action Integer selector; if interactive flag is false, automatically execute selected action in range 1-3 or exit 
 */
void wibble::WibbleSync::sync_directories(const std::string& src_dir, const std::string& dest_dir, const bool ls_only,
                                          const bool detailed_ls, const bool interactive, const int auto_action)
{
    const bool condensed = detailed_ls ? false : true;
    if(src_dir == dest_dir) { wc.console_write_error_red("Cannot sync a directory with itself! Aborting."); throw wibble::wibble_exit(1); }

    std::vector<file_info> source_files = _parse_directory_tree(src_dir);
    std::vector<file_info> dest_files   = _parse_directory_tree(dest_dir);

    if(! std::filesystem::exists(dest_dir) && dest_dir != "")
    {
        wc.console_write_green(ARW + "Destination directory does not exist. Synchronising new files will create it:\n");
        wc.console_write(dest_dir);
    } 

    std::vector<std::pair<file_info, file_info>> file_matches;        //!< Tuple of pairwise file matches (same filename + relative position in tree)
    std::vector<std::pair<file_info, file_info>> update_matches;      //!< Tuple of pairwise update matches (i.e. datetime of WIBBLE_VERSION stamp later at source)
    std::vector<std::pair<file_info, file_info>> no_update_matches;   //!< Tuple of pairwise no update matches (identical WIBBLE_VERSION stamps)
    std::vector<std::pair<file_info, file_info>> untracked_files;     //!< Tuple of pairwise untracked files (i.e. neither candidate has WIBBLE_VERSION stamp)
    std::vector<std::pair<file_info, file_info>> newer_on_dest_files; //!< Tuple of pairwise files newer at destination (i.e. WIBBLE_VERSION stamp later at dest)
    std::vector<file_info> new_files;

    std::cout << '\n';
    wc.console_write_hz_divider();
    for(auto const& f: source_files)
    {
        if(auto_action == 4)
        {
            wc.console_write_yellow_bold("File info\n");
            wc.console_write_yellow_bold("────────\n");
            wc.console_write_yellow_bold("Time string     : ");  wc.console_write(f.time_string              );
            wc.console_write_yellow_bold("Time stamp      : ");  wc.console_write(std::to_string(f.timestamp));
            wc.console_write_yellow_bold("File path       : ");  wc.console_write(f.file_path                );
            wc.console_write_yellow_bold("Relative path   : ");  wc.console_write(f.rel_path                 );
            wc.console_write_yellow_bold("File description:\n"); wc.console_write(f.file_desc                );
            wc.console_write_yellow_bold("Filename        : ");  wc.console_write(f.file_name                );
            wc.console_write_hz_divider();
        }
        bool found_match = false;
        for(auto const& d: dest_files)
        {
            if(f.rel_path == d.rel_path)
            {
                found_match = true;
                std::pair<file_info, file_info> match{f,d};
                file_matches.push_back(match);
            }
        }
        if(! found_match)
        {
            std::pair<file_info, file_info> match{f,file_info()};
            file_matches.push_back(match);
        }
    }

    // For file listing only
    if(auto_action == 4) { throw wibble::wibble_exit(0); }
    /*
    for(auto const& f: dest_files)
    {
        // debug
        std::cout << "File info\n";
        std::cout << "---------\n";
        std::cout << "Time string     : " << f.time_string << '\n';
        std::cout << "Time stamp      : " << f.timestamp << '\n';
        std::cout << "File path       : " << f.file_path << '\n';
        std::cout << "Relative path   : " << f.rel_path << '\n';
        std::cout << "File description: " << f.file_desc;// << '\n';
        std::cout << "Filename        : " << f.file_name << '\n';
        std::cout << "--------------------------------------------------\n";
    }
    */

    for(auto const& p: file_matches)
    {
        //std::cout << "Match     : " << p.first.file_path << " -> " << p.second.file_path << '\n';
        //std::cout << "Timestring: " << p.first.time_string << " -> " << p.second.time_string << '\n';
        //std::cout << "Timestamps: " << p.first.timestamp << " -> " << p.second.timestamp << '\n';

        // 1. Updated files - already tracked both sides
        if(p.first.timestamp > p.second.timestamp && p.first.timestamp != 0 && p.second.timestamp != 0) 
        { update_matches.push_back(p);      }

        // 2. Files that have been newly staged on source for update - i.e. exist on both sides but timestamp just added
        if(p.first.timestamp != 0 && p.first.file_name != "" && p.second.rel_path == p.first.rel_path && p.second.timestamp == 0)
        { update_matches.push_back(p); }

        // 3. Non-updated files
        if(p.first.timestamp == p.second.timestamp && p.first.timestamp != 0 && p.second.timestamp != 0) 
        { no_update_matches.push_back(p);   }

        // 4. Untracked files
        if(p.first.timestamp == 0 && p.second.timestamp == 0 && p.first.rel_path != "")// && p.second.rel_path != "")
        { untracked_files.push_back(p);     }

        // 5. Newer on destination...
        if(p.second.timestamp > p.first.timestamp && p.first.timestamp != 0 && p.second.timestamp != 0)
        { newer_on_dest_files.push_back(p); }

        // 6. Entirely new files...
        if(p.first.timestamp != 0 && p.first.file_name != "" && p.second.file_name == "")
        { new_files.push_back(p.first); }

            
    }

    wc.console_write_header("UNTRACKED");
    for(auto const& p: untracked_files)     { _print_summary(p.first, 1, condensed); } 
    wc.console_write_yellow(ARW + "" + std::to_string(untracked_files.size()) + " file[s] are currently untracked.\n");
    wc.console_write_hz_divider();
    wc.console_write_header("NO UPDATE");
    for(auto const& p: no_update_matches)   { _print_summary(p.first, 2, condensed); }
    wc.console_write_yellow(ARW + "" + std::to_string(no_update_matches.size()) + " file[s] are fully up to date.\n");
    wc.console_write_hz_divider();
    wc.console_write_header(" NEWER ");
    for(auto const& p: newer_on_dest_files) { _print_summary(p.second, 4, condensed); }
    wc.console_write_yellow(ARW + "" + std::to_string(newer_on_dest_files.size()) + " file[s] are newer at destination.\n");
    wc.console_write_hz_divider();
    wc.console_write_header(" UPDATE");
    for(auto const& p: update_matches)      { _print_summary(p.first, 3, condensed); }
    wc.console_write_yellow(ARW + "" + std::to_string(update_matches.size()) + " file[s] have updates pending.\n");
    wc.console_write_hz_divider();
    wc.console_write_header("NEW FILES");
    for(auto const& p: new_files)           { _print_summary(p, 5, condensed);       }
    wc.console_write_yellow(ARW + "" + std::to_string(new_files.size()) + " file[s] are new/do no exist in destination.\n");
    wc.console_write_hz_divider();

    if(! ls_only)
    {
        std::string resp;
        if(interactive)
        {
            wc.console_write_hz_divider();
            wc.console_write_yellow("1. Synchronise "); wc.console_write_green_bold("updated"); wc.console_write_yellow(" files only.\n");
            wc.console_write_yellow("2. Synchronise "); wc.console_write_green_bold("new & updated"); wc.console_write_yellow(" files.\n");
            wc.console_write_yellow("3. "); wc.console_write_red_bold("Reverse synchronise ");
            wc.console_write_yellow("files that are"); wc.console_write_red_bold(" newer"); wc.console_write_yellow(" at the ");
            wc.console_write_yellow_bold("destination.\n");
            wc.console_write_yellow("q. Abort file operation/quit.\n"); 
            wc.console_write_hz_divider();
    
            resp = wc.console_write_prompt("\nInput operation [1:3] > ", "", "");
            wc.trim(resp);
        }
        else
        {
            resp = "-1";
        }
        std::cout << "\n"; wc.console_write_hz_divider(); std::cout << "\n";

        int action = 0;
        if(resp != "q" && resp != "Q")
        {
            try
            {
                action = std::stoi(resp);
            }
            catch(std::exception& ex)
            {
                wc.console_write_error_red("Invalid selection. Exiting.");
            }
        }
        else { action = -1; }

        if(! interactive) { action = auto_action; }
        
        switch(action)
        {
        case 1:
            wc.console_write_success_green("[1/1] Synchronising/updating tracked files...");
            _synchronise_files(update_matches);
            break;
        case 2:
            wc.console_write_success_green("[1/2] Synchronising/updating tracked files...");
            _synchronise_files(update_matches);
            wc.console_write_success_green("[2/2] Copying new files...");
            _copy_new_files(src_dir, dest_dir, new_files);
            break;
        case 3:
            wc.console_write_success_green("[1/1] Reverse synchronising/Copying newer files present in destination...");
            _synchronise_files(newer_on_dest_files, true);
            break;
        case -1:
            break;
        default:
            wc.console_write_error_red("Invalid option.");
            wc.console_write_yellow_bold("Valid options:\n");
            wc.console_write_yellow("[1] Synchronise/update files.\n");
            wc.console_write_yellow("[2] Synchronise/update files + copy new files.\n");
            wc.console_write_yellow("[3] Reverse synchronise/copy files that are newer at destination.\n");
            break;
        }
    }
}
