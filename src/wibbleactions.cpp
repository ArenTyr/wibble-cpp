/**
 * @file      wibbleactions.cpp
 * @brief     Helper/utility class for some shared functions.
 * @details
 *
 * Common functionality/methods used by both the "bookmark" and "task"
 * features, hence extraction into a common shared library.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibbleactions.hpp"
#include "wibbleexit.hpp"
#include "wibblejotter.hpp"
#include "wibbletopicmanager.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Process an entry in a topic bookmark file into a bookmark entry struct.
 * @param line String with line/record from bookmark file.
 */
wibble::bm wibble::WibbleActions::deserialize_topic_bm(const std::string& line)
{
    bm entry;
    try
    {
        // topic bookmarks have six fields to process/deserialize
        size_t hsh_sep          = line.find(FIELD_DELIM);
        size_t topic_id_sep     = line.find(FIELD_DELIM, hsh_sep + 1);
        size_t topic_name_sep   = line.find(FIELD_DELIM, topic_id_sep + 1);
        size_t data_sep         = line.find(FIELD_DELIM, topic_name_sep + 1);
        size_t cat_sep          = line.find(FIELD_DELIM, data_sep + 1);
        size_t fn_sep           = line.find(FIELD_DELIM, cat_sep + 1);

        // we can include the topic id in the hash
        std::string uniq_hsh    = line.substr(0, topic_id_sep);

        std::string topic_id    = line.substr(hsh_sep + 1, (topic_id_sep - 1 - hsh_sep));
        std::string topic_name  = line.substr(topic_id_sep +1, (topic_name_sep - 1 - topic_id_sep));
        std::string entry_type  = line.substr(topic_name_sep + 1, 1);
        std::string category    = line.substr(data_sep + 1, (cat_sep - 1 - data_sep));
        std::string topic_fn    = line.substr(cat_sep + 1, (fn_sep - 1 - cat_sep));
        std::string title       = line.substr(fn_sep + 1, (line.length() - 1 - fn_sep));

        //std::cout << "DEBUG topic fields:\n";
        //std::cout << "index = " << topic_id_sep   << "  topic_id   = " << topic_id << "\n";
        //std::cout << "index = " << topic_name_sep << "  topic_name = " << topic_name << "\n";
        //std::cout << "index = " << data_sep       << "  entry_type = " << entry_type << "\n";
        //std::cout << "index = " << cat_sep        << "  category   = " << category << "\n";
        //std::cout << "index = " << fn_sep         << " topic_fn   = " << topic_fn << "\n";
        //std::cout << "index = " << fn_sep         << " title      = " << title << "\n";

        // bookmark entry struct has generic field names due to the disparate types of
        // bookmark types its stores
        entry.title  = title;
        entry.field1 = topic_id;
        entry.field2 = topic_name;
        entry.field3 = entry_type;
        entry.field4 = topic_fn;
        entry.field5 = category;
        entry.field6 = uniq_hsh;
    }
    catch(std::out_of_range& ex)
    {
        std::cerr << "Warning: malformed topic bookmark entry" << std::endl;
        return bm();
    }

    return entry;
}

/**
 * @brief Execution wrapper that detects whether bookmark refers to an bookmark script rather than simple command.
 * @param wib_e WibbleExecutor execution object
 * @param cmd String containing simple shell command or path to script file (with EXEC_PREFIX token)
 */
void wibble::WibbleActions::expand_exec(WibbleExecutor& wib_e, const std::string& cmd)
{
    std::string exec_cmd;
    size_t we = cmd.find(EXEC_PREFIX);
    if(we != std::string::npos)
    {
        exec_cmd = wib_e.get_scripts_dir() + BOOKMK_SCR + "/" + cmd.substr(EXEC_PREFIX.length(), std::string::npos);
    }
    else { exec_cmd = cmd; }

    std::system(exec_cmd.c_str());
}

/**
 * @brief Execution wrapper that routes through to execution template processor.
 * @param wib_e WibbleExecutor execution/environment object
 * @param exec_template String containing execution command template
 * @param fn String containing file name to execute command upon
 */
void wibble::WibbleActions::expand_exec(WibbleExecutor& wib_e, const std::string& exec_template, const std::string& fn)
{
    std::string exec_fn;
    size_t we = fn.find(EXEC_PREFIX);
    if(we != std::string::npos)
    {
        exec_fn = wib_e.get_scripts_dir() + BOOKMK_SCR + "/" + fn.substr(EXEC_PREFIX.length(), std::string::npos);
    }
    else { exec_fn = fn; }

    wib_e.run_exec_expansion(exec_template, exec_fn);
}

/**
 * @brief Copy all Task project files into a given Node's data directory
 * @param wc WibbleConsole utility object
 * @param wib_e WibbleExecutor execution/environment object
 * @param dd_path Filesystem path to task data directory/project files
 * @param lf_list Filesystem path to task linked file list, if it exists
 */
bool wibble::WibbleActions::copy_project_files_into_node(WibbleConsole& wc, const pkg_options& OPTS,
                                                         std::string& dd_path, const std::string& lf_list)
{
    WibbleExecutor wib_e{OPTS};
    wc.console_write_header("TO NODE ");
    wc.console_write_yellow("Select the Node you wish to copy the data files into.\n");
    wc.console_write_hz_divider();

    search_filters SEARCH;
    SEARCH.menu_search = true;
    WibbleUtility::menu_search_set_filters(wc, SEARCH);
    WibbleRecord w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH);
    if(w.get_id() == "___NULL___") { wc.console_write_error_red("Invalid record selection."); return false; }

    // ensure std::cout is synced
    std::cout << std::unitbuf;

    wc.console_write_header("CONFIRM");
    w.pretty_print_record_full();
    const std::string resp = wc.console_write_prompt("Copy data into above Node's data directory? (y/n) > ", "", "");
    wc.console_write("\n");
    if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); return false; }

    if(! std::filesystem::exists(std::filesystem::path(dd_path)))
    {
        wc.console_write_error_red("No project files/data present to copy! Aborting.");
        wc.console_write("\n");
        return false;
    }

    // COPY (not move) project data across
    const bool copy_proj_files = wib_e.op_add_data_to_note(w, wib_e.get_main_db(), wib_e.get_tmp_dir() + "/wibble-workfile", false, dd_path, true);
    if(copy_proj_files) { wc.console_write_success_green("Data files successfully copied."); }

    // are there any other external linked files to merge into Node linked file list?
    if(std::filesystem::exists(std::filesystem::path(lf_list)))
    {
        // excise the lines with the ___TASKDIR___ placehoder, since by definition these have now been
        // copied into Node as data, hence no longer "linked files"
        try
        {
            std::stringstream lf_ss = wibble::WibbleIO::read_file_into_stringstream(lf_list);
            std::string line;
            std::string linked_files = "";
            bool do_copy = false;
            while(std::getline(lf_ss, line, '\n'))
            {
                if(line.find(TASKDIR_PLCHDR) == std::string::npos) { linked_files.append(line + '\n'); do_copy = true; }
                else                                               { continue;                                         }
            }
            std::stringstream().swap(lf_ss);

            // only proceed if there are additional external linked files to merge into linked file list
            if(do_copy)
            {
                // ensure path to linked file is present
                const std::string data_path = wib_e._build_note_path(w.get_id(), w.get_type(), w.get_proj(), false, false) + "/.wibble";
                const std::string lf_path = data_path + "/linked_files.wib";
                bool dd_exists = true;
                if(! std::filesystem::exists(std::filesystem::path(data_path))) { dd_exists = wibble::WibbleIO::create_path(data_path);    }
                if(! dd_exists)
                {
                    wc.console_write_error_red("Error creating data directory: " + data_path + ". Not copying linked files.");
                    throw std::invalid_argument("Bad path");
                }

                bool file_op = WibbleIO::write_out_file_append(linked_files, lf_path);
                if(file_op) { wc.console_write_success_green("Linked files successfully propagated."); }
            }
        }
        catch(std::exception& ex)
        {
            std::cerr << "Error whilst merging linked file list: " << ex.what() << std::endl;
            return false;
        }
    }

    return true;

}


/**
 * @brief Copy all Task project files into a Topic as a data entry
 * @param wc WibbleConsole utility object
 * @param wib_e WibbleExecutor execution/environment object
 * @param project_dir string containing the path of the Task project directory
 */
bool wibble::WibbleActions::copy_project_files_into_topic(WibbleConsole &wc, const pkg_options& OPTS, std::string& project_dir)
{
    wc.console_write_header("TO TOPIC");
    wc.console_write_yellow("Select the Topic you wish to copy the data files into.\n");
    wc.console_write_hz_divider();

    // ensure std::cout is synced
    std::cout << std::unitbuf;
    try
    {
        WibbleExecutor wib_e{OPTS};
        WibbleTopics wt{wib_e.get_tmp_dir(), wib_e.get_templates_dir(), wib_e.get_bin_mapping()};
        topic_options topic;
        topic.copy_data_file = project_dir;
        bool topics_exist = wt.accumulate_topic_map(wc, wib_e.get_topics_dir());
        if(! topics_exist) { wc.console_write_error_red("No topics found. Exiting."); return false; }
        WibbleTopicManager wtm{OPTS.exec.bin_mapping};
        wtm.dispatch_data_topic_entry(OPTS, topic, wt);
        return true;
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error copying data files into Topic");
        return false;
    }
}

/**
 * @brief Obtain/serialize a topic entry interactively (to a string/record) for bookmarking/quick access later.
 * @param wc WibbleConsole utility object
 * @param wib_e WibbleExecutor execution/environment object
 */
std::string wibble::WibbleActions::generate_topic_entry_link_interactive(WibbleConsole& wc, WibbleExecutor& wib_e)
{
    // ensure std::cout is synced
    std::cout << std::unitbuf;
    try
    {
        bool txt_not_data = true;
        std::string filter_exp = "";
        std::string grep_exp = "";

        wc.console_write_header("ADD");
        wc.console_write_yellow("1. Add a new "); wc.console_write_yellow_bold("topic"); wc.console_write_yellow(" bookmark.\n");
        wc.console_write_yellow("2. Add a new "); wc.console_write_yellow_bold("topic data"); wc.console_write_yellow(" bookmark.\n");
        std::string resp = wc.console_write_prompt("Select topic type: [1:2] >", "", "");
        if(resp != "1" && resp != "2")
        { wc.console_write_error_red("Invalid selection, please enter either '1' or '2'."); throw wibble::wibble_exit(1); }
        else if(resp == "2") { txt_not_data = false; }

        WibbleTopics wt{wib_e.get_tmp_dir(), wib_e.get_templates_dir(), wib_e.get_bin_mapping()};
        bool topics_exist = wt.accumulate_topic_map(wc, wib_e.get_topics_dir());
        if(! topics_exist) { wc.console_write_error_red("No topics found. Exiting."); throw wibble::wibble_exit(1); }

        filter_exp = wc.console_write_prompt("(Optional) Enter any pre-filter expression for topic entry titles\n>", "", "");
        wc.trim(filter_exp);
        // cannot grep data/binary files
        if(resp == "1")
        {
            grep_exp = wc.console_write_prompt("(Optional) Enter any grep expression for topic entries\n>", "", "");
        }
        wc.trim(grep_exp);

        // interactively obtain the actual topic entry
        WibbleTopicManager wtm{wib_e.get_bin_mapping()};
        topic_def topic = wtm.get_topic_entry_fn(wc, wt, wib_e.get_topics_dir(), wib_e.get_tmp_dir(), filter_exp, grep_exp, txt_not_data);
        std::string stub_fn = topic.bk_filename;
        wibble::WibbleRecord::str_to_uppercase(topic.title);
        // remove base directory if present; this is re-interpolated from environment/config file,
        // so Wibble configuration is portable (i.e. no absolute paths)
        if(stub_fn.find(wib_e.get_topics_dir()) != std::string::npos) { stub_fn = stub_fn.substr(wib_e.get_topics_dir().length() + 1, std::string::npos); }
        wc.trim(stub_fn);
        wc.console_write_header("LINK");
        wc.console_write_yellow_bold("Topic: "); wc.console_write(topic.name);
        wc.console_write_yellow_bold("Entry: "); wc.console_write(stub_fn);
        resp = wc.console_write_prompt("Add this topic entry as a new bookmark? (y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { return ""; }
        else
        {
            std::string title = wc.console_write_prompt("Input title/descriptive name of this topic link > ", "", "");
            wc.trim(title); wibble::WibbleRecord::remove_pipe(title);
            if(title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); return ""; }
            std::string record = wibble::WibbleRecord::generate_entry_hash() + "|" + topic.id + "|" + topic.name;
            if(txt_not_data) { record.append("|E|" + topic.category + "|" + stub_fn + "|" + title + "\n");  }
            else             { record.append("|D|" + topic.category + "|" + stub_fn + "|" + title + "\n");  }

            return record;
        }
    }
    catch(std::exception& ex)
    {
        return "";
    }
}

/**
 * @brief Select and perform various common Jotfile operations.
 * @param wc WibbleConsole utility object
 * @param jotfile_dir String representing root Jotfile directory
 * @param tmp_dir String representing cache/temporary directory
 * @param jotfile_fn String representing direct jotfile filename (used with jotfile_dir)
 */
std::string wibble::WibbleActions::jotfile_action_menu(WibbleConsole& wc, const std::string& jotfile_dir,
                                               const std::string& tmp_dir, const std::string& jotfile_fn)
{
    if(! std::filesystem::exists(jotfile_fn)) { wc.console_write_error_red("Jotfile does not exist: " + jotfile_fn); return ""; }

    wc.console_write("\n\n");
    wc.console_write_header(" ACTION");
    wc.console_write_yellow_bold("01. "); wc.console_write("Add jotfile entry.");
    wc.console_write_yellow_bold("02. "); wc.console_write("Edit jotfile entry.");
    wc.console_write_yellow_bold("03. "); wc.console_write("View jotfile entry.");
    wc.console_write_yellow_bold("04. "); wc.console_write("Delete jotfile entry.");
    wc.console_write_yellow_bold("05. "); wc.console_write("Delete entire jotfile.");
    wc.console_write_hz_divider();
    std::string option = wc.console_write_prompt("Select option > ", "", "");
    try
    {
        std::string inp = "";
        wc.trim(option);
        unsigned long l = 0;
        l = std::stol(option);
        if(l < 1 || l > 13) { throw std::invalid_argument("Out of range"); }

        WibbleJotter wj{wc, jotfile_dir, tmp_dir};
        bool remove_success = false;
        switch(l)
        {
        case 1:  // add entry
            wj.direct_add_jotfile_entry(wc, jotfile_fn);
            break;
        case 2:  // edit entry
            wj.direct_edit_jotfile_entry(wc, jotfile_fn);
            break;
        case 3:  // view entry
            wj.direct_view_jotfile_entry(wc, jotfile_fn);
            break;
        case 4:  // delete entry
            wj.direct_delete_jotfile_entry(wc, jotfile_fn);
            break;
        case 5:  // delete entry
            remove_success = wj.direct_remove_jotfile(wc, jotfile_fn);
            if(remove_success)
            {
                return jotfile_fn;
            }
            break;
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
    }
    return "";
}

/**
 * @brief Select and perform various common Node operations.
 * @param wc WibbleConsole utility object
 * @param w WibbleRecord (Node) record representing root Jotfile directory
 * @param wib_e WibbleExecutor execution/environment object
 * @param archived Flag indicating whether Node record is present in archive
 */
void wibble::WibbleActions::node_action_menu(WibbleConsole& wc, WibbleRecord& w, WibbleExecutor& wib_e, const bool archived)
{
    bool has_data = false;
    bool has_lf   = false;
    // determine whether Node has any data directory/data files, and whether it has any linked files
    if(w.get_dd().find("[ DATA ]") != std::string::npos) { has_data = true; }
    if(w.get_dd().find("[ LF ]")   != std::string::npos) { has_lf = true; }

    wc.console_write("\n\n");
    wc.console_write_header(" ACTION");
    wc.console_write_yellow_bold("01. "); wc.console_write("Dump note contents to console.");
    wc.console_write_yellow_bold("02. "); wc.console_write("View node with view tool.");
    wc.console_write_yellow_bold("03. "); wc.console_write("Edit node with CLI editor.");
    wc.console_write_yellow_bold("04. "); wc.console_write("Edit node with GUI editor.");
    wc.console_write_yellow_bold("05. "); wc.console_write("Run command on node file.");
    if(has_data) {  wc.console_write_yellow_bold("06. "); wc.console_write("List contents of node data directory.");          }
    else         {  wc.console_write_red("06. ");         wc.console_write("[ NO ACTION ] - no data added.");                 }
    if(has_lf)   {  wc.console_write_yellow_bold("07. "); wc.console_write("List/open linked files.");                        }
    else         {  wc.console_write_red("07. ");         wc.console_write("[ NO ACTION ] - no linked files.");               }
    if(has_data) {  wc.console_write_yellow_bold("08. "); wc.console_write("Open node data directory.");                      }
    else         {  wc.console_write_red("08. ");         wc.console_write("[ NO ACTION ] - no data added.");                 }
    if(has_data) {  wc.console_write_yellow_bold("09. "); wc.console_write("Open individual file from node data directory."); }
    else         {  wc.console_write_red("09. ");         wc.console_write("[ NO ACTION ] - no data added.");                 }
    wc.console_write_yellow_bold("10. "); wc.console_write("COPY a file into node data directory.");
    wc.console_write_yellow_bold("11. "); wc.console_write("MOVE a file into node data directory.");
    wc.console_write_yellow_bold("12. "); wc.console_write("Add a LINKED file.");
    wc.console_write_yellow_bold("13. "); wc.console_write("Dump metadata to console.");

    wc.console_write_hz_divider();
    std::string option = wc.console_write_prompt("Select option > ", "", "");
    try
    {
        std::string inp = "";
        wc.trim(option);
        unsigned long l = 0;
        l = std::stol(option);
        if(l < 1 || l > 13) { throw std::invalid_argument("Out of range"); }
        switch(l)
        {
        case 1:  // dump
            wib_e.note_pipe_through_dump_tool(w, archived);
            break;
        case 2:  // view
            wib_e.note_pipe_through_view_tool(w, archived);
            break;
        case 3:  // CLI edit
            wib_e.note_open_with_editor(w.get_id(), w.get_type(), w.get_proj(), false, archived);
            break;
        case 4:  // GUI edit
            wib_e.note_open_with_editor(w.get_id(), w.get_type(), w.get_proj(), true, archived);
            break;
        case 5:  // exec cmd
            inp = wc.console_write_prompt("Input command/command template: ", "", "");
            wc.trim(inp);
            if(inp != "") { wib_e.note_exec_cmd(w, inp, archived);                        }
            else          { wc.console_write_error_red("Empty command input. Aborting."); }
            break;
        case 6:  // ls data directory
            if(has_data) { wib_e.note_dd_ls_view_dump(w, archived, 2); }
            break;
        case 7:  // open linked file
            if(has_lf)   { wib_e.note_open_linked_file(w, archived); }
            break;
        case 8:  // open data directory
            if(has_data) { wib_e.note_dd_ls_view_dump(w, archived, 3, wib_e.get_fm_tool()); }
            break;
        case 9:  // open individual data directory file
            if(has_data)
            {
                wc.console_write_yellow_bold("(Optional) Filter expression file results. Leave blank for everything.\n");
                inp = wc.console_write_prompt("> ", "", "");
                wc.trim(inp);
                wib_e.note_dd_open_individual_file(w, archived, inp);
            }
            break;
        case 10:  // copy file/add to data directory
            wc.console_write_yellow_bold("Supply filename/path to file to COPY into node directory\n");
            inp = wc.console_write_prompt("> ", "", "");
            wc.trim(inp);
            if(! archived) { wib_e.op_add_data_to_note(w, wib_e.get_main_db(), wib_e.get_tmp_dir() + WORKSTUB, archived, inp, true);  }
            else           { wib_e.op_add_data_to_note(w, wib_e.get_archived_db(), wib_e.get_tmp_dir() + WORKSTUB, archived, inp, true);  }
            break;
        case 11:  // move file/add to data directory
            wc.console_write_yellow_bold("Supply filename/path to file to MOVE into node directory\n");
            inp = wc.console_write_prompt("> ", "", "");
            wc.trim(inp);
            if(! archived) { wib_e.op_add_data_to_note(w, wib_e.get_main_db(), wib_e.get_tmp_dir() + WORKSTUB, archived, inp, false);  }
            else           { wib_e.op_add_data_to_note(w, wib_e.get_archived_db(), wib_e.get_tmp_dir() + WORKSTUB, archived, inp, false);  }
            break;
        case 12:  // add a linked file/add to data directory
            wc.console_write_yellow_bold("Supply filename/path of file to add as a LINKED file in node directory\n");
            inp = wc.console_write_prompt("> ", "", "");
            wc.trim(inp);
            if(! archived) { wib_e.op_add_linked_file_to_note(w, wib_e.get_main_db(), wib_e.get_tmp_dir() + WORKSTUB, archived, inp);     }
            else           { wib_e.op_add_linked_file_to_note(w, wib_e.get_archived_db(), wib_e.get_tmp_dir() + WORKSTUB, archived, inp); }
            break;
        case 13:
            w.pretty_print_record_full_nc();
            break;
        default:
            wc.console_write_error_red("Invalid selection. Exiting.");
            break;
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
    }
}

/**
 * @brief Select and perform various common Topic operations.
 * @param wc WibbleConsole utility object
 * @param wib_e WibbleExecutor execution/environment object
 * @param fn Filename of Topic entry
 */
void wibble::WibbleActions::topic_action_menu(WibbleConsole& wc, WibbleExecutor& wib_e, const std::string& fn)
{

    // handle data topic entries separately
    if(fn.find("data_store/") != std::string::npos)
    {
        expand_exec(wib_e, wib_e.get_xdg_open_cmd(), fn);
    }
    else
    {
        wc.console_write("\n");
        wc.console_write_hz_divider();
        wc.console_write_header(" ACTION");
        wc.console_write_yellow_bold("01. "); wc.console_write("Dump topic contents to console.");
        wc.console_write_yellow_bold("02. "); wc.console_write("View topic with view tool.");
        wc.console_write_yellow_bold("03. "); wc.console_write("Edit topic with CLI editor.");
        wc.console_write_yellow_bold("04. "); wc.console_write("Edit topic with GUI editor.");

        wc.console_write_hz_divider();
        std::string option = wc.console_write_prompt("Select option > ", "", "");
        try
        {
            std::string inp = "";
            wc.trim(option);
            unsigned long l = 0;
            l = std::stol(option);
            if(l < 1 || l > 13) { throw std::invalid_argument("Out of range"); }
            switch(l)
            {
            case 1:  // dump
                //expand_exec(wib_e, wib_e.get_dump_tool() + " " + fn);
                expand_exec(wib_e, wib_e.get_dump_tool(), fn);
                break;
            case 2:  // view
                //expand_exec(wib_e, wib_e.get_view_tool() + " " + fn);
                expand_exec(wib_e, wib_e.get_view_tool(), fn);
                break;
            case 3:  // CLI edit
                //expand_exec(wib_e, wib_e.get_cli_editor() + " " + fn);
                expand_exec(wib_e, wib_e.get_cli_editor(), fn);
                break;
            case 4:  // GUI edit
                //expand_exec(wib_e, wib_e.get_gui_editor() + " " + fn);
                expand_exec(wib_e, wib_e.get_gui_editor(), fn);
                break;
            case 5:  // exec cmd
                //FIXME: implement accessor
                inp = wc.console_write_prompt("Input command/command template: ", "", "");
                wc.trim(inp);
                if(inp != "") { std::cout << "not implemented\n"; /* wib_e.note_exec_cmd(w, inp, archived); */ }
                else          { wc.console_write_error_red("Empty command input. Aborting."); }
                break;
            }
        }
        catch(std::exception& ex)
        {
            wc.console_write_error_red("Invalid selection. Exiting.");
        }
    }
}
