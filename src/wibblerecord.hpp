/**
 * @file      wibblerecord.hpp
 * @brief     Represents abstraction of a WibbleRecord (Node) record (header file).
 * @details
 *
 * This class provides the data structure for the main Wibble record/Node
 * entry, together with various getters/setters, and core utility functions
 * for generating unique entry hashes and working with datetimes.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLERECORD_H
#define WIBBLERECORD_H

#include <algorithm>
#include <ctime>
#include <regex>
#include <set>
#include <vector>

#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleRecord
    {
    private:
        WibbleConsole print;

        std::string date_field;
        std::string title_field;
        std::string id_field;
        std::string desc_field;
        std::string type_field;
        std::string proj_field;
        std::string tags_field;
        std::string class_field;
        std::string dd_field;
        std::string kp_field;
        std::string cust_field;
        std::string lids_field;

        bool bin_record;

        void _print_data_flairs(const bool condensed_view) const;
    public:
        WibbleRecord(): date_field{"___NULL___"}, title_field{"___NULL___"}, id_field{"___NULL___"},
                        desc_field{"___NULL___"}, type_field{"___NULL___"}, proj_field{"___NULL___"},
                        tags_field{"___NULL___"}, class_field{"___NULL___"}, dd_field{"___NULL___"},
                        kp_field{"___NULL___"}, cust_field{"___NULL___"}, lids_field{"___NULL___"}, bin_record {false} {};

        WibbleRecord(std::string&& da_f, std::string&& ti_f, std::string&& id_f,
                     std::string&& de_f, std::string&& ty_f, std::string&& pr_f,
                     std::string&& tg_f, std::string&& cl_f, std::string&& dd_f,
                     std::string&& kp_f, std::string&& cu_f, std::string&& li_f,
                     bool bin_r) :
                     date_field{da_f}, title_field{ti_f}, id_field{id_f},
                     desc_field{de_f}, type_field{sanitise_input(ty_f)}, proj_field{sanitise_input(pr_f)},
                     tags_field{tg_f}, class_field{cl_f}, dd_field{dd_f},
                     kp_field{kp_f}, cust_field{cu_f}, lids_field{li_f}, bin_record{bin_r} {};

        std::string const& get_date()  const { return date_field;  };
        std::string const& get_title() const { return title_field; };
        std::string const& get_id()    const { return id_field;    };
        std::string const& get_desc()  const { return desc_field;  };
        std::string const& get_type()  const { return type_field;  };
        std::string const& get_proj()  const { return proj_field;  };
        std::string const& get_tags()  const { return tags_field;  };
        std::string const& get_class() const { return class_field; };
        std::string const& get_dd()    const { return dd_field;    };
        std::string const& get_kp()    const { return kp_field;    };
        std::string const& get_cust()  const { return cust_field;  };
        std::string const& get_lids()  const { return lids_field;  };
        bool        const& get_bin_r() const { return bin_record;  };

        std::string pretty_print_record_full() const;
        std::string pretty_print_record_full(unsigned long counter, unsigned long tot_records) const;
        std::string pretty_print_record_full_nc() const;
        std::string pretty_print_record_condensed() const;
        std::string pretty_print_record_condensed(unsigned long counter, unsigned long tot_records) const;

        // Project / filetype are involved in path construction (filename) -> eliminate whitespace
        void set_date(std::string&& field_value)  { date_field = std::move(field_value);                     };
        void set_title(std::string&& field_value) { title_field = std::move(field_value);                    };
        void set_id(std::string&& field_value)    { id_field = std::move(field_value);                       };
        void set_desc(std::string&& field_value)  { desc_field = std::move(field_value);                     };
        void set_type(std::string&& field_value)  { type_field = std::move(sanitise_input(field_value));     };
        void set_proj(std::string&& field_value)  { proj_field = std::move(sanitise_input(field_value));     };
        void set_tags(std::string&& field_value)  { tags_field = std::move(field_value);                     };
        void set_class(std::string&& field_value) { class_field = std::move(field_value);                    };
        void set_dd(std::string&& field_value)    { dd_field = std::move(field_value);                       };
        void set_kp(std::string&& field_value)    { kp_field = std::move(field_value);                       };
        void set_cust(std::string&& field_value)  { cust_field = std::move(field_value);                     };
        void set_lids(std::string&& field_value)  { lids_field = std::move(field_value);                     };

        static bool keypair_helper(WibbleConsole& wc, std::set<std::string>& keypairs, const std::string& prefill_key = "", const std::string& prefill_val = "");
        static std::string get_set_keypair_field(WibbleConsole& wc, std::string& kp_string);
        static std::vector<std::string> generate_keypair_vector(WibbleConsole& wc, std::string& kp_string);
        
        /**
         * @brief Replace "%" symbol with " " in target string
         * @param field_val input string to mutate
         */
        static std::string const& remove_keypair_delim(std::string& field_val)
        { std::replace(field_val.begin(), field_val.end(), '%', ' '); return field_val; }

        /**
         * @brief Replace blank spaces with "_" in target string
         * @param field_val input string to mutate
         */
        static std::string const& remove_whitespace(std::string& field_val)
        { std::replace(field_val.begin(), field_val.end(), ' ', '_'); return field_val; }

        /**
         * @brief Replace ":" symbol with "_" in target string
         * @param field_val input string to mutate
         */
        static std::string const& remove_colon(std::string& field_val)
        { std::replace(field_val.begin(), field_val.end(), ':', '_'); return field_val; }

        /**
         * @brief Replace "|" symbol with "_" in target string
         * @param field_val input string to mutate
         */
        static std::string const& remove_pipe(std::string& field_val)
        { std::replace(field_val.begin(), field_val.end(), '|', '_'); return field_val; }

        /**
         * @brief Replace "/" or "\" symbol with "_" in target string
         * @param field_val input string to mutate
         */
        static std::string const& remove_slash(std::string& field_val)
        {
            std::replace(field_val.begin(), field_val.end(), '/', '_');
            std::replace(field_val.begin(), field_val.end(), '\\', '_');
            return field_val;
        }

        /**
         * @brief Remove all whitespace, pipe, colon, and slash symbols from an input string.
         * @param field_val input string to mutate
         */
        static std::string const& sanitise_input(std::string& field_val)
        {
            remove_whitespace(field_val);
            remove_pipe(field_val);
            remove_colon(field_val);
            remove_slash(field_val);
            
            return field_val;
        }
        
        /**
         * @brief Uppercase the given input string.
         * @param input the string to uppercase
         */
        static void str_to_uppercase(std::string& input)
        { transform(input.begin(), input.end(), input.begin(), ::toupper); }

        /**
         * @brief Lowercase the given input string.
         * @param input the string to lowercase
         */
        static void str_to_lowercase(std::string& input)
        { transform(input.begin(), input.end(), input.begin(), ::tolower); }

        /**
         * @brief Replace underscores with hyphens
         * @param field_val the string to perform substitution on
         */
        static std::string const& underscore_to_hyphen(std::string& field_val)
        { std::replace(field_val.begin(), field_val.end(), '_', '-'); return field_val; }

        static std::string const serialize_record(const WibbleRecord& record);
        // generate UUIDs (v4)
        static std::string generate_UUIDv4();
        // datetime generators
        static std::string generate_date_rfc5322(const std::string& input_date);
        static std::string generate_date_YMD(const std::string& input_date);
        static std::string generate_date_YYYY(const std::string& input_date);
        static std::string generate_date_MM(const std::string& input_date);
        static std::string generate_date_DD(const std::string& input_date);
        static std::string generate_entry_hash();
        static std::string generate_numbered_month(const int& manual_prx = 0);
        static std::string generate_timestamp(const std::string& input_date);
        static std::time_t get_time_from_YYYY_MM_DD_str(const std::string& datestring,
                                                        const bool ret_1900 = false,
                                                        const bool gen_time = false);

        static std::string get_default_template_str();
    };
}
#endif
