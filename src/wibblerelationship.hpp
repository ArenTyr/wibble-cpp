/**
 * @file      wibblerelationship.hpp
 * @brief     Provides functionality for Node relationships (header file).
 * @details
 *
 * Management class for defining, retrieving, and editing Node relationships.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <set>
#include <vector>
#include "wibbleexecutor.hpp"
#include "wibblerecord.hpp"

namespace wibble
{
    class WibbleRelationship
    {
    private:
        WibbleConsole& wc;
        WibbleExecutor& wib_e;
        const std::string cache_dir;
        inline WibbleRecord _get_record_for_id(const std::string& node_id);
    public:
        struct related_nodes
        {
            std::vector<std::string> parent_ids;
            std::vector<std::string> sibling_ids;
            std::vector<std::string> child_ids;
            related_nodes() {};
        };

        enum relationship_type
        {
            PARENT_OF,
            SIBLING_OF,
            CHILD_OF
        };
        
        bool add_node_relationships(WibbleRecord& record, relationship_type R);
        bool remove_node_relationship(WibbleRecord& src_record, const bool archived);
        static std::string determine_rn_filename(WibbleExecutor& we, const WibbleRecord& record, const bool archived);
        void display_node_relationships(const WibbleRecord& record, const bool archived, const bool extended_listing);
        void relationship_menu(WibbleRecord& record, const bool archived);
        bool remove_all_relationships_for_record(WibbleRecord& record, const bool archived);
        WibbleRelationship(WibbleExecutor& we, WibbleConsole& c):
            wc{c},
            wib_e{we},
            cache_dir{wib_e.get_tmp_dir()} {};
    private:
        void _append_to_rl_list(WibbleRecord& record, const std::string& fn, const std::string& new_rltn, const bool archived);
        void _append_to_rl_list(WibbleRecord& record, const std::string& fn, const std::set<std::string>& new_rltn, const bool archived);
        std::string _define_node_relationship(const std::string& trgt_id, relationship_type R);
        void _check_and_display(const WibbleRecord& w, WibResultList& records);
        void _display_relationship(const related_nodes& nds, const bool expanded);
        void _display_compact_rlt(const related_nodes& nds);
        void _display_selection(const WibbleRecord& w, WibResultList& records);
        void _display_full_rlt(const related_nodes& nds);
        void _display_stats(const related_nodes& nds);
        related_nodes _get_related_nodes(const WibbleRecord& record, const bool archived);
        bool _purge_relationships_for_id(WibbleRecord& record, const std::string& node_id, const bool archived, const std::string prefix, const bool no_rels_left);
        std::size_t _rel_count(const related_nodes& nds);
    };
}
