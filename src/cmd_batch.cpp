// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_batch.cpp
 * @brief     Handle "batch" subcommand.
 * @details
 *
 * This class dispatches the non-interactive "batch" functionality.
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n 
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>  
 * 
 * 
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *  
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr. 
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filesystem>
#include <iostream>

#include "cmd_batch.hpp"
#include "wibblebatchmanager.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Static function to parse/verify a valid batch command has been issued.
 * @param BATCH command line options/parameters supplied via switches to batch command
 */
int wibble::WibbleCmdBatch::check_input(WibbleConsole& wc, batch_options& BATCH)
{

    // 1. Node creation path
    if(BATCH.create_node)
    {
        if(BATCH.node_title == "UNSET" || BATCH.node_filetype == "UNSET")
        {
            wc.console_write_error_red("Node creation error.");
            wc.console_write_red("\nBoth the title and filetype must be set.\n\n");
            wc.console_write_red("--node-title <TEXT> REQUIRED\n");
            wc.console_write_red("--node-filetype <TEXT> REQUIRED\n\n");
            return INVALID_BATCH_NODE_OP;
        }

        if(BATCH.content_file == "" || ! std::filesystem::exists(std::filesystem::path(BATCH.content_file)))
        {
            wc.console_write_error_red("Node creation error.");
            wc.console_write_red("\nThe Node content file must be provided and must exist.\n\n");
            wc.console_write_red("--content-file <FILENAME> REQUIRED\n\n");
            return INVALID_BATCH_NODE_OP;
        }

        return BATCH_CREATE_NODE;
    }
    else if(BATCH.create_task_item)
    {
        if(BATCH.task_title == "UNSET" || BATCH.task_category == "UNSET" || BATCH.task_group == "UNSET")
        {
            wc.console_write_error_red("Task item creation error.");
            wc.console_write_red("\nThe Task item title, Task item category, and \n");
            wc.console_write_red("designated/matched Task group must be specified.\n\n");
            wc.console_write_red("--task-title <TEXT> REQUIRED\n");
            wc.console_write_red("--task-category <TEXT> REQUIRED\n");
            wc.console_write_red("--task-group <TEXT> REQUIRED\n");
            return INVALID_BATCH_TASK_OP;
        }

        if(BATCH.task_priority != NULL_INT_VAL && (BATCH.task_priority < 1 || BATCH.task_priority > 99))
        {
            wc.console_write_error_red("Task item creation error.");
            wc.console_write_red("\nThe Task item priority must be in range 1-99.\n\n");
            return INVALID_BATCH_TASK_OP;

        }
        return BATCH_CREATE_TASK_ITEM;
    }
    // 3. Topic entry creation path
    else if(BATCH.create_topic_entry)
    {
        if(BATCH.topic_entry_title == "UNSET" || BATCH.use_topic == "UNSET")
        {
            wc.console_write_error_red("Topic entry creation error.");
            wc.console_write_red("\nBoth the Topic entry title and the designated Topic must be set.\n\n");
            wc.console_write_red("--topic-entry-title <TEXT> REQUIRED\n");
            wc.console_write_red("--use-topic <TEXT> REQUIRED\n\n");
            return INVALID_BATCH_TOPIC_OP;
        }

        if(BATCH.content_file == "" || ! std::filesystem::exists(std::filesystem::path(BATCH.content_file)))
        {
            wc.console_write_error_red("Topic entry creation error.");
            wc.console_write_red("\nThe Topic entry content file must be provided and must exist.\n\n");
            wc.console_write_red("--content-file <TEXT> REQUIRED\n\n");
            return INVALID_BATCH_TOPIC_OP;
        }

        return BATCH_CREATE_TOPIC_ENTRY;
    }
    // 4. Jotfile entry creation path
    else if(BATCH.create_jotfile_entry)
    {
        if(BATCH.jot_entry_title == "UNSET" || BATCH.use_jotfile == "UNSET")
        {
            wc.console_write_error_red("Jotfile entry creation error.");
            wc.console_write_red("\nBoth the Jotfile entry title and the designated Jotfile must be set.\n\n");
            wc.console_write_red("--jotfile-entry-title <TEXT> REQUIRED\n");
            wc.console_write_red("--use-jotfile <TEXT> REQUIRED\n\n");
            return INVALID_BATCH_JOTFILE_OP;
        }

        if(BATCH.content_file == "" || ! std::filesystem::exists(std::filesystem::path(BATCH.content_file)))
        {
            wc.console_write_error_red("Jotfile entry creation error.");
            wc.console_write_red("\nThe Jotfile entry content file must be provided and must exist.\n\n");
            wc.console_write_red("--content-file <FILENAME> REQUIRED\n\n");
            return INVALID_BATCH_JOTFILE_OP;
        }

        return BATCH_CREATE_JOTFILE_ENTRY;
    }
    // 5. New Task group creation path
    else if(BATCH.create_new_task_group)
    {
        wc.trim(BATCH.task_group);
        if(BATCH.task_group == "UNSET" || BATCH.task_group == "")
        {
            wc.console_write_error_red("Task group creation error.");
            wc.console_write_red("\nThe Task group name must be specified.\n\n");
            wc.console_write_red("--task-group <TEXT> REQUIRED\n");
            return INVALID_BATCH_TASK_OP;
        }

        return BATCH_CREATE_NEW_TASK_GROUP;
    }
    // 6. New Topic creation path
    else if(BATCH.create_new_topic)
    {
        if(BATCH.topic_category == "UNSET" || BATCH.topic_title == "UNSET" || BATCH.topic_filetype == "UNSET" ||
           BATCH.topic_name == "UNSET" || BATCH.topic_description == "UNSET")
        {
            wc.console_write_error_red("New Topic creation error.");
            wc.console_write_red("\nThe Topic category, title, filetype, name, and description must all be set.\n\n");
            wc.console_write_red("--topic-category <TEXT> REQUIRED\n");
            wc.console_write_red("--topic-title <TEXT> REQUIRED\n");
            wc.console_write_red("--topic-filetype <TEXT> REQUIRED\n");
            wc.console_write_red("--topic-name <TEXT> REQUIRED\n");
            wc.console_write_red("--topic-description <TEXT> REQUIRED\n\n");
            return INVALID_BATCH_TOPIC_OP;
        }

        return BATCH_CREATE_NEW_TOPIC;
    }
    // 7. New Jotfile creation path
    else if(BATCH.create_new_jotfile)
    {
        if(BATCH.jot_category == "UNSET" || BATCH.jot_name == "UNSET")
        {
            wc.console_write_error_red("New Jotfile creation error.");
            wc.console_write_red("\nBoth the Jotfile category and title must be set.\n\n");
            wc.console_write_red("--jotfile-category <TEXT> REQUIRED\n");
            wc.console_write_red("--jotfile-name <TEXT> REQUIRED\n\n");
            return INVALID_BATCH_JOTFILE_OP;
        }

        return BATCH_CREATE_NEW_JOTFILE;
    }
    // 8. Add data to Node/Task item/Topic
    else if(BATCH.node_add_data || BATCH.task_add_data || BATCH.topic_add_data)
    {
        if(! BATCH.data_copy && ! BATCH.data_move)
        {
            wc.console_write_error_red("Data operation error.");
            wc.console_write_red("\nAdding data requires that the copy OR move operation is specified:\n\n");
            wc.console_write_red("--data-copy\n");
            wc.console_write_red("--data-move\n\n");
            return INVALID_BATCH_DATA_OP;
        }

        if(BATCH.data_copy && BATCH.data_move)
        {
            wc.console_write_error_red("Data operation error.");
            wc.console_write_red("\nThe copy and move operations are mutually exclusive. Only specify ONE.\n\n");
            return INVALID_BATCH_DATA_OP;
        }

        if(BATCH.data_file == "" || ! std::filesystem::exists(std::filesystem::path(BATCH.data_file)))
        {
            wc.console_write_error_red("Data operation error.");
            wc.console_write_red("\nThe specified data file/directory must be provided and must exist.\n\n");
            wc.console_write_red("--data-file <FILENAME> REQUIRED\n\n");
            return INVALID_BATCH_DATA_OP;
        }

        if(BATCH.task_add_data)
        {
            if(BATCH.task_id == "UNSET" || BATCH.task_id == "") 
            {
                wc.console_write_error_red("Data operation error.");
                wc.console_write_red("\nThe Task entry ID must be specified when adding a data to a Task item.\n\n");
                wc.console_write_red("--task-id <TEXT> REQUIRED\n\n");
                return INVALID_BATCH_DATA_OP;
            }
        }
 
        if(BATCH.topic_add_data)
        {
            if(BATCH.use_topic == "UNSET" || BATCH.use_topic == "")
            {
                wc.console_write_error_red("Data operation error.");
                wc.console_write_red("\nA valid matched Topic must be specified to add data into.\n\n");
                wc.console_write_red("--use-topic <TEXT> REQUIRED\n\n");
                return INVALID_BATCH_DATA_OP;
            }

            if(BATCH.topic_entry_title == "UNSET") 
            {
                wc.console_write_error_red("Data operation error.");
                wc.console_write_red("\nThe Topic entry title must be specified when adding a data to a Topic.\n\n");
                wc.console_write_red("--topic-entry-title <TEXT> REQUIRED\n\n");
                return INVALID_BATCH_DATA_OP;
            }
        }
       
        if(BATCH.node_add_data)  { return BATCH_NODE_ADD_DATA;  }
        if(BATCH.task_add_data)  { return BATCH_TASK_ADD_DATA;  }
        if(BATCH.topic_add_data) { return BATCH_TOPIC_ADD_DATA; }

        return INVALID_BATCH_DATA_OP;
    }
    else // 9. No valid command issued
    {
        wc.console_write_error_red("No operation specified.");
        wc.console_write_red("\nSpecify ONE of the following commands:\n\n");
        wc.console_write_red("--create-node\n");
        wc.console_write_red("--create-jotfile-entry\n");
        wc.console_write_red("--create-task-item\n");
        wc.console_write_red("--create-topic-entry\n");
        wc.console_write_red("--create-new-jotfile\n");
        wc.console_write_red("--create-new-task-group\n");
        wc.console_write_red("--create-new-topic\n");
        wc.console_write_red("--node-add-data\n");
        wc.console_write_red("--task-add-data\n");
        wc.console_write_red("--topic-add-data\n\n");
        return INVALID_OP;
    }
}

/**
 * @brief Static function to process the batch command request.
 * @param OPTS package/general configuration options struct
 * @param BATCH command line options/parameters supplied via switches to batch command
 */
void wibble::WibbleCmdBatch::handle_batch(pkg_options& OPTS, batch_options& BATCH)
{
    WibbleConsole wc;
    int op = -1;

    op = check_input(wc, BATCH);
    bool proceed = false;

    switch(op)
    {
    case BATCH_CREATE_NODE:
        wc.console_write_success_green("Creating Node");
        proceed = true;
        break;

    case BATCH_CREATE_TASK_ITEM:
        wc.console_write_success_green("Creating Task item");
        proceed = true;
        break;

    case BATCH_CREATE_TOPIC_ENTRY:
        wc.console_write_success_green("Creating Topic entry");
        proceed = true;
        break;

    case BATCH_CREATE_JOTFILE_ENTRY:
        wc.console_write_success_green("Creating Jotfile entry");
        proceed = true;
        break;

    case BATCH_CREATE_NEW_TASK_GROUP:
        wc.console_write_success_green("Creating a new Task group");
        proceed = true;
        break;
 
    case BATCH_CREATE_NEW_TOPIC:
        wc.console_write_success_green("Creating a new Topic");
        proceed = true;
        break;
        
    case BATCH_CREATE_NEW_JOTFILE:
        wc.console_write_success_green("Creating a new Jotfile");
        proceed = true;
        break;

    case BATCH_NODE_ADD_DATA:
        wc.console_write_success_green("Adding Data to existing Node");
        proceed = true;
        break;

    case BATCH_TASK_ADD_DATA:
        wc.console_write_success_green("Adding Data to existing Task item");
        proceed = true;
        break;

    case BATCH_TOPIC_ADD_DATA:
        wc.console_write_success_green("Adding Data to existing Topic");
        proceed = true;
        break;

    case INVALID_BATCH_DATA_OP:
        wc.console_write_error_red("Invalid Data operation specified.");
        break;

    case INVALID_BATCH_NODE_OP:
        wc.console_write_error_red("Invalid Node operation specified.");
        break;

    case INVALID_BATCH_TOPIC_OP:
        wc.console_write_error_red("Invalid Topic operation specified.");
        break;

    case INVALID_BATCH_JOTFILE_OP:
        wc.console_write_error_red("Invalid Jotfile operation specified.");
        break;

    case INVALID_OP:
        wc.console_write_error_red("No valid batch operation specified.");
        break;
    }

    if(proceed)
    {
        WibbleBatchManager wbtm;
        wbtm.execute(wc, OPTS, BATCH, op);
    }
}
