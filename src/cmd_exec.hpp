/**
 * @file      cmd_exec.hpp
 * @brief     Handle "exec" subcommand (header file).
 * @details
 *
 * This class handles the "exec" command, which allows the user
 * to execute an arbitrary command on a given Wibble Node file, or
 * batch execution across a set of Wibble Node files. Also provides
 * ability to reference a script contained in Wibble's 'scripts/'
 * subdirectory.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CMD_EXEC_H
#define CMD_EXEC_H

#include "cmd_structs.hpp"
#include "wibbleexecutor.hpp"

namespace wibble
{
    class WibbleCmdExec
    {
    private:
    public:
        static void _run_batch(WibbleConsole& wc, WibbleExecutor& wib_e, search_filters& SEARCH, std::string& exec_cmd);
        static void handle_exec(pkg_options& OPTS, search_filters& SEARCH, exec_options& EXEC);
    };
}
#endif
