/**
 * @file      wibblejanitor.cpp
 * @brief     Provides a simple and clean non-XDG "trash" directory.
 * @details
 *
 * A simple, deliberately non-XDG compliant "trash" directory manager,
 * designed to make it simple to safely delete and restore files, and
 * avoid accidental rm nightmares. Notion is to make its use seamless
 * by adding bash aliases for 'rm', etc.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibblejanitor.hpp"
#include "wibbleexit.hpp"
#include "wibblerecord.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Match against the optimal trash directory, if exists.
 * @param trash_candidate String representing full filepath/filename of file to trash.
 */
std::string wibble::WibbleJanitor::_determine_trash_target(const std::string& trash_candidate)
{
    std::vector<std::string> trash_roots;
    for(auto const& t: trash_directories)
    {
        // find matching trash roots
        if(trash_candidate.find(t.first) == 0) { trash_roots.push_back(t.second); }
    }

    if(trash_roots.size() == 0) { return "___NO_TRASH_DIR___"; }
    else
    {
        std::string trash_target = "";
        std::size_t max_score = 0;
        for(auto const& tt: trash_roots)
        {
            std::size_t score = 0;
            // determine path with greatest character overlap, i.e. "nearest neighbour trash root"
            while(score < tt.length() && score < trash_candidate.length() && (tt.at(score) == trash_candidate.at(score)) ) { ++score; }

            // find best path match/overlap from character by character comparison
            if(score > max_score) { trash_target = tt; max_score = score; }
        }

        // failsafe, should not occur
        if(trash_target == "") {  return "___NO_TRASH_DIR___"; }
        else                   {  return trash_target;         }
    }
}



/**
 * @brief Pretty print some ASCII art of a dustbin, just because :-).
 */
std::string wibble::WibbleJanitor::_display_obliteration_ascii_art()
{
return R"(%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%#*#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%*     :-+*%%%%%%%%%%%%%++++*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%#           .:=+#%%%%%%%-     :=*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%=                 .:=+#%=       :%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%#*=-.                        +%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%#+=:.                 .-+*#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*+-:                  :-+*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#*=-:                 .:=+#%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#*=-.                 .-=*#%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#*=-:                 -%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*+-.            #%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#*=-.     =%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#+=-%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%*------------------------------------------------------=#%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%+:                                                     :*%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%                                                     #%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%.                                                    %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%-      .*%%%*.         =###*:          :+**+.       :%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%+      *%%%%%=        =%%%%%#         .%%%%%#       =%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%#      +%%%%%*        =%%%%%%         -%%%%%#       *%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      =%%%%%#        =%%%%%%         =%%%%%*       #%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%.     -%%%%%%        =%%%%%%         *%%%%%+      .%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%-     .%%%%%%        =%%%%%%         #%%%%%-      -%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%+      %%%%%%.       =%%%%%%         %%%%%%:      +%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%#      #%%%%%-       =%%%%%%        .%%%%%%.      *%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%      *%%%%%=       =%%%%%%        -%%%%%%       %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%:     +%%%%%+       =%%%%%%        =%%%%%*      .%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%-     =%%%%%*       =%%%%%%        +%%%%%+      -%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%+     -%%%%%#       =%%%%%%        #%%%%%=      +%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%#     :%%%%%%       =%%%%%%        %%%%%%:      #%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%     .%%%%%%.      =%%%%%%       .%%%%%%.      %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%:     %%%%%%:      =%%%%%%       :%%%%%%      :%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%=     #%%%%%-      =%%%%%%       -%%%%%#      =%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%+     *%%%%%=      =%%%%%%       +%%%%%*      *%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%#     +%%%%%*      =%%%%%%       *%%%%%=      #%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%     =%%%%%#      =%%%%%%       #%%%%%-     .%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%:    :%%%%%%      =%%%%%%       %%%%%%.     -%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%=    .%%%%%%      =%%%%%%      :%%%%%%      =%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%+     %%%%%%:     =%%%%%%      -%%%%%#      *%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%#     #%%%%%-     =%%%%%%      =%%%%%*      %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%.    *%%%%%=     =%%%%%%      *%%%%%+     .%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%+    +%%%%%+     =%%%%%%      #%%%%%=     =%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%-    +##*=      .*%%%#-      -#%%%#.    :%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%+.                             .      +%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%*-.                             .-+%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%)";
}

/**
 * @brief Parse the Janitor configuration file to determine trash directory locations for filesystems.
 */
bool wibble::WibbleJanitor::_get_setup_defined_trash_dir_for_devices()
{
    try
    {
        std::stringstream trash_ss = WibbleIO::read_file_into_stringstream(janitor_config_file);
        std::string line;
        while(std::getline(trash_ss, line))
        {
            std::size_t dev_root_delim = line.find(FIELD_DELIM);
            if(dev_root_delim == std::string::npos) { wc.console_write_error_red("Error in configuration: " + line); throw wibble::wibble_exit(1); }

            std::string trsh_root = line.substr(0, dev_root_delim);
            std::string trsh_dir  = line.substr(dev_root_delim + 1, (line.length() - 1 - dev_root_delim));
            wc.trim(trsh_root);
            wc.trim(trsh_dir);
            WibbleUtility::remove_trailing_slash(trsh_root);
            WibbleUtility::remove_trailing_slash(trsh_dir);

            if(! std::filesystem::exists(std::filesystem::path(trsh_root)))
            {  wc.console_write_error_red("Trash root does not exist: " + trsh_root); throw wibble::wibble_exit(1); }

            if(! std::filesystem::exists(std::filesystem::path(trsh_dir)))
            {
                bool create_trash_dir = WibbleIO::create_path(std::filesystem::path(trsh_dir));
                if (! create_trash_dir )
                {  wc.console_write_error_red("Unable to create trash directory: " + trsh_dir); throw wibble::wibble_exit(1); }
            }
                
            std::pair<std::string, std::string> trash_d{trsh_root, trsh_dir};
            trash_directories.push_back(trash_d);
        }

        std::sort(trash_directories.begin(), trash_directories.end(), [](trash_pair const& a, trash_pair const& b) { return a.second > b.second; });
        
        for(auto const& t: trash_directories)
        {
            wc.console_write_green(ARW + "Configured trash directory: '" + t.second + "' for device/filesystem root '" + t.first + "'.\n");
        }

        if(trash_directories.size() == 0)
        {
            wc.console_write_yellow_bold("No Janitor trash directories setup. Configure them with '--config'.\n");
        }
        return true;
    }
    catch(std::exception& ex) { return false; }
}

/**
 * @brief Obtain complete list of trashed files current in any one of the trash directories.
 */
std::pair<std::vector<std::string>, std::set<std::string>> wibble::WibbleJanitor::_get_trashed()
{
    std::vector<std::string> del_list;
    std::stringstream obl_ss = WibbleIO::read_file_into_stringstream(janitor_history_file);
    std::string line;
    std::set<std::string> del_buckets;
    while(std::getline(obl_ss, line))
    {
        std::size_t fp_sep = line.find(FIELD_DELIM);
        if(fp_sep == std::string::npos)
        { wc.console_write_error_red("Corrupted history file."); throw std::invalid_argument("Malformed history file."); }
        const std::string f_name = line.substr(fp_sep + 1, line.length() - 1 - fp_sep);
        del_buckets.insert(WibbleIO::get_parent_dir_path(f_name));
        del_list.push_back(f_name);
    }

    std::stringstream().swap(obl_ss);
    return std::pair<std::vector<std::string>, std::set<std::string>>{del_list, del_buckets};
}


/**
 * @brief Wrapper function to call parser to determine trash directory filesystem locations.
 */
void wibble::WibbleJanitor::_obtain_trash_dirs()
{
    bool read_config = true;
    const std::string janitor_dir = WibbleIO::get_parent_dir_path(janitor_config_file);
    if(! std::filesystem::exists(std::filesystem::path(janitor_config_file)))
    {
        bool create_j_dir = WibbleIO::create_path(janitor_dir);
        if(! create_j_dir)
        {
            wc.console_write_error_red("Unable to setup Janitor configuration directory: " + janitor_dir + ". Aborting.");
            throw wibble::wibble_exit(1);
        }
    }
    
    if(! std::filesystem::exists(std::filesystem::path(janitor_config_file)))
    {
        wc.console_write_error_red("Janitor trash directories have not been setup. Please setup first.");
        wc.console_write_yellow(ARW + "Hint: Run 'wibble janitor --config' for guided setup.\n");
        read_config = false;
    }

    if(read_config) { _get_setup_defined_trash_dir_for_devices(); }
}

/**
 * @brief Helper to display currently trashed filenames.
 * @param del_list Vector containing list of filenames
 */
void wibble::WibbleJanitor::_preview_delete(const std::vector<std::string>& del_list)
{
    for(auto const& item: del_list)
    {
        wc.console_write_yellow_bold("[TO DELETE] ");
        wc.console_write_red(item + '\n');
    }
    std::cout << '\n';
}

/**
 * @brief Interactive function to help-user write-out/build the Janitor configuration file.
 */
void wibble::WibbleJanitor::configure()
{

    if(! std::filesystem::exists(std::filesystem::path(janitor_config_file)))
    {
        wc.console_write_yellow(ARW + "No configuration found. A new configuration file will be setup.\n");
    }
    else
    {
        wc.console_write_yellow(ARW + "Existing file found.\n");
        wc.console_write_yellow("Before editing any existing Janitor configuration, you should ensure\n");
        wc.console_write_yellow("the trash directories are empty by running 'wibble janitor -X'.\n\n");
        std::string resp = wc.console_write_prompt("Edit existing Janitor configuration? (y/n)", "", "");

        if(resp == "y" || resp == "Y")
        {
            wc.console_write_yellow_bold("\nNOTE: ");
            wc.console_write_yellow("To remove an existing configuration, simply enter\n");
            wc.console_write_yellow("either the root or storage directory as a blank input.\n\n");
            const std::size_t editcount = trash_directories.size();
            int i = 0;
            for(auto& pair: trash_directories)
            {
                ++i;
                std::string root = pair.first;
                WibbleUtility::remove_trailing_slash(root);
                std::string t_dir = pair.second;
                WibbleUtility::remove_trailing_slash(t_dir);
                t_dir = WibbleIO::get_parent_dir_path(t_dir);
                std::string nr = wc.console_write_prompt("[" + std::to_string(i) + "/" + std::to_string(editcount) + "]  i) Edit trash/filesystem root  : ", "", root); 
                std::string ns = wc.console_write_prompt("[" + std::to_string(i) + "/" + std::to_string(editcount) + "] ii) Edit trash storage directory: ", "", t_dir); 

                WibbleUtility::remove_trailing_slash(nr); WibbleUtility::remove_trailing_slash(ns);
                WibbleRecord::remove_pipe(nr); WibbleRecord::remove_pipe(ns);

                pair.first = nr; pair.second = ns;
                if(pair.first == "" || pair.second == "") { pair.first = ""; pair.second = ""; }
                else                                      { pair.second += TRSH_DNAME;         }
                wc.console_write_hz_divider();
            }

        }
        else { wc.console_write_error_red("User cancelled."); throw wibble::wibble_exit(0); }
    }

    // remove any blanked entries
    for(auto it = trash_directories.begin(); it != trash_directories.end(); )
    {
        if((*it).first == "")
        {
            wc.console_write_red("Removing entry.\n");
            it = trash_directories.erase(it);
        }
        else { ++it; }
    }

    bool keep_adding = true;
    do
    {
        int i = 0;
        wc.console_write_header("CONFIG");
        for(auto const& item: trash_directories)
        {
            std::cout << "────[ "; wc.console_write_grey_bold(std::to_string(++i)); std::cout << " ]────\n";
            wc.console_write_yellow_bold("Trash root             : "); wc.console_write(item.first);
            wc.console_write_yellow_bold("Trash storage directory: "); wc.console_write(item.second);
            wc.console_write_hz_divider();
        }
        wc.console_write_yellow_bold("a. Add new Janitor trash bucket\n");
        wc.console_write_yellow_bold("b. Write out Janitor configuration\n");
        wc.console_write_yellow_bold("c. Cancel and exit\n");
        bool valid_response = false;
        std::string resp = "";
        do
        {
            resp = wc.console_write_prompt("> ", "", "");
            if(resp == "a" || resp == "A")
            {
                wc.console_write_yellow_bold("\nNOTE: ");
                wc.console_write_yellow("Do not enter leaf directory '.janitor' for storage directory.\n");
                wc.console_write_yellow("This will be added automatically. Simply specify containing directory.\n");
                std::cout << '\n';
                std::string nr = wc.console_write_prompt(" i) Set trash/filesystem root  : ", "", ""); 
                std::string ns = wc.console_write_prompt("ii) Set trash storage directory: ", "", ""); 

                WibbleUtility::remove_trailing_slash(nr); WibbleUtility::remove_trailing_slash(ns);
                WibbleRecord::remove_pipe(nr); WibbleRecord::remove_pipe(ns);

                std::pair<std::string, std::string> trash_dir{nr, ns + TRSH_DNAME};
                trash_directories.push_back(trash_dir);
                valid_response = true;
            }
            else if(resp == "b" || resp == "B")
            {
                valid_response = true;
                keep_adding = false;
                std::string of_config = "";
                for(auto const& item: trash_directories)
                {
                    if(item.first == "" || item.second == "") { continue; }
                    of_config.append(item.first + FIELD_DELIM + item.second + '\n');
                }
                bool wo = WibbleIO::write_out_file_overwrite(of_config, janitor_config_file);
                if(wo) { wc.console_write_success_green("New configuration written successfully."); }
                else   { wc.console_write_success_green("Error writing configuration out to disk."); }
            }
            else if(resp == "c" || resp == "C")
            {
                wc.console_write_red(ARW + "User cancelled configuration. Exiting.\n");
                throw wibble::wibble_exit(0);
            }
            else
            {
                wc.console_write_red(ARW + "Invalid option. Please input option a:c\n");
            }
        }while(! valid_response);
    }
    while(keep_adding);

}

/**
 * @brief List all currently trashed files.
 */
void wibble::WibbleJanitor::list_trashed()
{
    std::pair<std::vector<std::string>, std::set<std::string>> results = _get_trashed();
    if(results.first.size() == 0) { wc.console_write_success_green("Trash is currently empty."); }
    else
    {
        wc.console_write_header("LIST ");
        _preview_delete(results.first);

        for(auto const& bkt: results.second)
        {
            wc.console_write_yellow_bold("[BUCKET   ] " + bkt + '\n');
            std::cout << ARW + "Data to delete/disk space to free:";
            wc.console_write_yellow_bold(" [ ");
            std::cout << std::flush;
            std::system(("du -hs " + bkt + " | cut -f 1 | tr -d '\n'").c_str());
            wc.console_write_yellow_bold(" ] \n");
        }
        wc.console_write_hz_heavy_divider();
    }
}

/**
 * @brief 'Obliterate', i.e. permanently delete files and eliminate them from trash bucket/director[y/ies].
 * @param show_graphic boolean flag controlling if the user is boring and wants to hide the glorious trash can ASCII artwork
 */
void wibble::WibbleJanitor::obliterate_files(const bool show_graphic)
{
    if(! std::filesystem::exists(std::filesystem::path(janitor_history_file)))
    {
        wc.console_write_error_red("No files in any trash bucket. Nothing to obliterate.");
    }
    else
    {
        bool exit_state = false;
        try
        {
            std::pair<std::vector<std::string>, std::set<std::string>> results = _get_trashed();

            if(results.first.empty()) { wc.console_write_green(ARW + "Nothing to obliterate, buckets empty.\n"); exit_state = true; throw wibble::wibble_exit(0); }
            wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━ [ OBLITERATE ] ━━━━━━━━━━━━━━━━━━━\n");

            _preview_delete(results.first);

            for(auto const& bkt: results.second)
            {
                wc.console_write_yellow_bold("[BUCKET   ] " + bkt + '\n');
                std::cout << ARW + "Data to delete/disk space to free:";
                wc.console_write_yellow_bold(" [ ");
                std::cout << std::flush;
                std::system(("du -hs " + bkt + " | cut -f 1 | tr -d '\n'").c_str());
                wc.console_write_yellow_bold(" ] \n");
            }

            wc.console_write_header("CONFIRM");
            wc.console_write_red(ARW + "The following trash directories will be PURGED:\n\n");
            for(auto const& item: trash_directories)
            {
                std::string trash_storage_dir = item.second + TRSH_PREFIX;
                WibbleUtility::remove_trailing_slash(trash_storage_dir);
                if(std::filesystem::exists(std::filesystem::path(trash_storage_dir)))
                {
                    wc.console_write_yellow_bold("[PURGE   ] "); wc.console_write_red_bold(trash_storage_dir + '\n');
                }
            }

            std::string resp = wc.console_write_prompt("\nProceed with obliteration? (y/n) > ", "", "");
            if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); exit_state = true; throw wibble::wibble_exit(0); }
            
            if(show_graphic) { wc.console_write_green_bold('\n' + _display_obliteration_ascii_art() + "\n\n"); }
            else             { wc.console_write("");                                                           } // boring option
            // for display purposes only...
            for(auto const& item: results.first)
            {
                wc.console_write_green_bold("[DELETING ] "); wc.console_write_red(item + '\n');
            }
            // in fact, just remove the containing parent directory for each trash directory, in one fell swoop
            for(auto const& item: trash_directories)
            {
                std::string trash_storage_dir = item.second + TRSH_PREFIX;
                WibbleUtility::remove_trailing_slash(trash_storage_dir);
                if(std::filesystem::exists(std::filesystem::path(trash_storage_dir)))
                {
                    // debugging final safety guard
                    //std::string resp = wc.console_write_prompt("DEBUG SAFETY Still go? (y/n) > ", "", "");
                    //if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); exit_state = true; throw wibble::wibble_exit(0); }

                    wc.console_write("");

                    try
                    {
                        wc.console_write_red_bold("[PURGING  ] " + trash_storage_dir + '\n');
                        std::cout << ARW + "Total storage undergoing deletion:";
                        wc.console_write_red_bold(" [ ");
                        std::cout << std::flush;
                        std::system(("du -hs " + trash_storage_dir + " | cut -f 1 | tr -d '\n'").c_str());
                        wc.console_write_red_bold(" ] \n");

                        std::uintmax_t del_result =  std::filesystem::remove_all(trash_storage_dir);
                        wc.console_write("");
                        wc.console_write_success_green("Successfully obliterated " + std::to_string(del_result) + " files/directories.");
                    }
                    catch(std::filesystem::filesystem_error& fs)
                    {
                        wc.console_write_red("Error purging files from trash directory.");
                        std::cout << "Error: " << fs.what() << std::endl;
                        throw wibble::wibble_exit(1);
                    }
                }
            }

            // deletion succeeded, now take care of the history file
            std::string clear = "";
            bool clear_history = WibbleIO::write_out_file_overwrite(clear,janitor_history_file);
            if(clear_history) { wc.console_write_success_green("Successfully purged history."); }
            else              { wc.console_write_error_red("Error purging history.");           }

            wc.console_write_success_green("Janitor emptied all of the trash.");
        }
        catch(std::exception& ex)
        {
            if(! exit_state) { wc.console_write_error_red("Error obliterating files. Manual deletion required."); }
        }
    }
       
}

/**
 * @brief Interactive function to restore a trashed file back into current directory or specified location.
 */
void wibble::WibbleJanitor::restore_file()
{
    std::pair<std::vector<std::string>, std::set<std::string>> results = _get_trashed();
    const std::size_t tot = results.first.size();
    if(tot == 0) { wc.console_write_success_green("Trash is currently empty."); }
    else
    {
        std::size_t index = 0;
        wc.console_write_header("RESTORE");
        for(auto const& item: results.first)
        {
            wc.console_write_padded_selector(++index, tot, wc.set_pretty_width(item, TRSH_DISP_LENGTH) + '\n'); 
        }

        unsigned long choice = wc.console_present_record_selector(tot);
        if(choice != 0)
        {
            const std::string sel_fn = results.first.at(choice - 1);
            const std::string tgt_fn = std::filesystem::path(sel_fn).filename();
            std::string out_fn = sel_fn;
            std::string tgt_dir = "current directory";
            std::string full_fn;
            bool mv_file = false;
            bool restore = true;

            wc.console_write_yellow_bold("Filename: "); wc.console_write(sel_fn + '\n');
            wc.console_write_hz_divider();
            wc.console_write_yellow_bold("\nSelect action:\n\n");
            wc.console_write_yellow("1. Restore into current directory\n");
            wc.console_write_yellow("2. Specify target directory to restore into\n");
            wc.console_write_yellow("3. Cancel\n\n");

            choice = wc.console_present_record_selector(3);

            switch(choice)
            {
            case 1:
                if(std::filesystem::exists(tgt_fn))
                {
                    wc.console_write_red(ARW + "File/directory '" + tgt_fn + "' already present in current directory.\n");
                    wc.console_write_red(ARW + "Specify alternate target location in order to restore.\n\n");
                    wc.console_write_error_red("Invalid target filename. Aborting.");
                    throw wibble::wibble_exit(1);
                }
                else
                {
                    mv_file = WibbleIO::wmove_file(sel_fn, tgt_fn); 
                    if(! mv_file)
                    { wc.console_write_error_red("Error restoring file into current directory. Aborting."); throw wibble::wibble_exit(1); }
                }
                break;
             case 2:
                tgt_dir = wc.console_write_prompt("Input destination directory > ", "", "");
                wc.trim(tgt_dir);
                WibbleUtility::expand_tilde(tgt_dir);
                WibbleUtility::remove_trailing_slash(tgt_dir);

                if(! std::filesystem::exists(std::filesystem::path(tgt_dir)))
                {
                    wc.console_write_error_red("Destination directory does not exist/is not readable. Aborting.");
                    throw wibble::wibble_exit(1);
                }

                full_fn = tgt_dir + "/" + tgt_fn;
                if(std::filesystem::exists(full_fn))
                {
                    wc.console_write_red(ARW + "File/directory '" + tgt_fn + "' already present in target directory.\n");
                    wc.console_write_red(ARW + "Specify alternate target location in order to restore.\n\n");
                    wc.console_write_error_red("Invalid target filename. Aborting.");
                    throw wibble::wibble_exit(1);
                }
                else
                {
                    mv_file = WibbleIO::wmove_file(sel_fn, full_fn);
                    if(! mv_file)
                    { wc.console_write_error_red("Error restoring file into target directory. Aborting."); throw wibble::wibble_exit(1); }
                }
                break;
            case 3:
                throw wibble::wibble_exit(0);
            default:
                wc.console_write_error_red("Invalid selection. Exiting");
                throw wibble::wibble_exit(0);
            }

            restore = WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, sel_fn, janitor_history_file);
            if(restore) { wc.console_write_success_green("File successfully restored into " + tgt_dir + "."); }
            else        { wc.console_write_error_red("Error updating Janitor index.");   }
        }
        else { wc.console_write_error_red("Invalid selection, exiting."); }
    }
}

/**
 * @brief 'Scrub' files, i.e. move targeted files into given trash director[y/ies] and update trash history/index.
 * @param file_list Vector containing list of strings specifying filename/paths to files
 */
void wibble::WibbleJanitor::scrub_files(const std::vector<std::string>& file_list)
{
    try
    {
        trash_list to_delete;
        std::vector<std::string> invalid_file_list;
        std::vector<std::string> no_trash_dir_list;
        for(auto const& del_file: file_list)
        {
            std::string mt_file = del_file;
            WibbleUtility::remove_trailing_slash(mt_file);
            std::filesystem::path fs_item{mt_file};
            if(std::filesystem::exists(fs_item))
            {
                trash_pair trash_item{std::filesystem::absolute(fs_item), _determine_trash_target(std::string(std::filesystem::absolute(fs_item)))};
                if(trash_item.second != "___NO_TRASH_DIR___") { to_delete.push_back(trash_item);        }
                else                                          { no_trash_dir_list.push_back(trash_item.first); }
            }
            else
            {
                //wc.console_write("Skipping non-existent file: " + del_file);
                invalid_file_list.push_back(del_file);
            }
        }

        for(auto const& item: no_trash_dir_list)
        {
            wc.console_write_hz_divider();
            wc.console_write_yellow_bold("[IGNORING] "); wc.console_write_yellow("No viable device trash directory for file: " + item + '\n');
        }

        for(auto const& item: invalid_file_list)
        {
            wc.console_write_hz_divider();
            wc.console_write_red_bold("[INVALID ] "); wc.console_write_red("Invalid/unreadable file: " + item + '\n');
        }

        wc.console_write_hz_divider();
        for(auto const& item: to_delete)
        {
            const std::string disp_file = wc.set_pretty_width(item.first, FILE_DISP_LENGTH);
            const std::string disp_bkt = wc.set_pretty_width(item.second, FILE_DISP_LENGTH);

            wc.console_write_green_bold("[TRASH   ]"); wc.console_write_green(" [ " + disp_file + " ] "); std::cout << "-"; wc.console_write_red(" [ " + disp_bkt + " ]\n");
        }

        if(to_delete.size() == 0) { wc.console_write_error_red("No viable trash candidates supplied.\n"); throw wibble::wibble_exit(1); }
        wc.console_write_hz_divider();

        const bool dry_run = false;
        std::string dlte_history = "";

        std::string resp = wc.console_write_prompt("Proceed with trashing above candidates? (y/n)", "", "");
        if(resp == "y" || resp == "Y")
        {
            const std::string del_hash = WibbleRecord::generate_entry_hash(); 
            for(auto const& item: to_delete)
            {
                const std::string s_name = std::filesystem::path(item.first).filename();
                const std::string p_name = wc.set_pretty_width(item.first, FILE_DISP_LENGTH);
                const std::string p_trgt = wc.set_pretty_width(item.second + TRSH_PREFIX + s_name, FILE_DISP_LENGTH);
                wc.console_write_green_bold("[TRASHING]"); wc.console_write_green(" [ " + p_name + " ] "); std::cout << "-";
                wc.console_write_red(" [ " + p_trgt + " ]\n");
                if(! dry_run)
                {
                    const std::string of_path = item.second + TRSH_PREFIX + del_hash;
                    //wc.console_write("DEBUG: Creating path: " + of_path);
                    const std::string of_name = of_path + "/" + s_name;
                    bool ddir = WibbleIO::create_path(of_path);
                    if(ddir)
                    {
                        //wc.console_write("std::filesystem::rename(" + item.first + ", " + of_name + ");"); 
                        std::filesystem::rename(std::filesystem::path(item.first), std::filesystem::path(of_name)); 
                        std::string hist_entry = item.first + FIELD_DELIM + of_name + '\n';
                        dlte_history.append(hist_entry);
                    }
                    else
                    {
                        wc.console_write_error_red("Unable to create output directory in bucket. Cancelling trash operation.");
                        throw wibble::wibble_exit(1);
                    }
                }
            }

            if(! dry_run)
            {
                bool whistory = WibbleIO::write_out_file_append(dlte_history, janitor_history_file);
                if(whistory) { wc.console_write_success_green("Trash operation successfully logged in Janitor history file."); }
                else         { wc.console_write_error_red("Error logging trash operation to Janitor history file.");           }
            }
                
        }
    
    }
    catch(std::exception& ex) { }
}

/**
 * @brief Outputs usage hints/guide to terminal.
 */
void wibble::WibbleJanitor::show_usage()
{
    wc.console_write_header("USAGE ");
    wc.console_write("1. Janitor needs configuring before it can be used.\n");
    wc.console_write("2. The basic principle is to setup device or filesystem specific trash directories.\n");
    wc.console_write("3. For example:\n\na) If you have your personal files mounted on a filesystem at:\n\n[ /home/user/myfiles ]\n");
    wc.console_write("b) And you have backup files mounted on an external drive at: \n\n[ /media/external ]\n");
    wc.console_write("c) Furthermore you have photos on this external drive in a subdirectory, which you wish");
    wc.console_write("to trash into their own separate directory from the rest, at:\n\n[ /media/external/photos ]\n");
    wc.console_write("Then you want three trash directories (you can have as many as you like):\n");
    wc.console_write("[1] located at /home/user/myfiles/.janitor, i.e. trash root = /home/user/myfiles");
    wc.console_write("[2] located at /media/external/.janitor, i.e. trash root = /media/external");
    wc.console_write("[3] located at /media/external/photos/.janitor, i.e. trash root = /media/external/photos\n");
    wc.console_write("NOTE: '.janitor' is automatically appended to the input trash directory.\n");
    wc.console_write("So the configuration for this example would be:\n");
    wc.console_write("[1] Trash root     : /home/user/myfiles");
    wc.console_write("[1] Trash directory: /home/user/myfiles");
    wc.console_write("[2] Trash root     : /media/external");
    wc.console_write("[2] Trash directory: /media/external");
    wc.console_write("[3] Trash root     : /media/external/photos");
    wc.console_write("[3] Trash directory: /media/external/photos\n");
    wc.console_write("A file/directory trashed from /home/user/myfiles/project/abc.txt would go into [1]");
    wc.console_write("A file/directory trashed from /media/external/backup/some_project would go into [2]");
    wc.console_write("A file/directory trashed from /media/external/photos/hol/hb33.jpg would go into [3]\n");
    wc.console_write("4. The longest/most specific matched trash directory is the one that is always used.");
    wc.console_write("For example, if you have a trash directory located at /home/user/myfiles/projectA/.janitor");
    wc.console_write("And you trash a a file using Janitor at /home/user/myfiles/projectA/foo/bar/file.txt,");
    wc.console_write("it will get matched/assigned to /home/user/myfiles/projectA/.janitor in preference");
    wc.console_write("to /home/user/myfiles/.janitor.\n");
    wc.console_write("5. Using this principle, you can easily leverage custom trash directories");
    wc.console_write("and control how files are trashed across different hard drives, internal drives,");
    wc.console_write("flash drives, etc. You can create as many trash buckets as suits to enforce");
    wc.console_write("simpler granularity and ease of recovery (if and where necessary).\n");
    wc.console_write("6. Regardless of which trash bucket files are trashed into, Janitor manages all");
    wc.console_write("trash directories as a set, allowing you to obliterate (permanently delete) all files in");
    wc.console_write("one hit, or restore an individual file, all from the one menu regardless of underlying");
    wc.console_write("bucket. Finally, for best use, it is recommended to combine it with some bash shell aliases");
    wc.console_write("for maximum convenience.\n");
    wc.console_write("7. If no valid trash directory can be found for the filesystem root, nothing will");
    wc.console_write("be trashed and it will abort with an error.\n");
    wc.console_write("8. Suggested aliases:\n");
    wc.console_write("alias rm='wibble janitor --scrub'");
    wc.console_write("alias del='/usr/bin/rm'");
    wc.console_write("alias empty='wibble janitor --obliterate'");
    wc.console_write("alias trash='wibble janitor --list'\n");
    wc.console_write("Using these, you can safely trash files via Janitor using your rm command as normal,");
    wc.console_write("or type 'del' to actually directly call 'rm' instead. Meanwhile 'empty' will now call");
    wc.console_write("the interactive trash emptying procedure (with confirmation), and 'trash' to just list.");
    wc.console_write("\nFor example, the following command then calls out to Janitor instead of /usr/bin/rm\n");
    wc.console_write("rm foo.txt ~/doc/tmp/*.el ~/Downloads/junk_photos/\n");
    wc.console_write("This would trash file 'foo.txt' in current directory, all *.el files under ~/doc/tmp");
    wc.console_write("and the directory 'junk_photos' under ~/Downloads, all in the one hit, with confirmation.");
}
