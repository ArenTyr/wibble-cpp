/**
 * @file      cmd_delete.cpp
 * @brief     Handle "delete" subcommand.
 * @details
 *
 * This class handles deletion of an existing Wibble Node, with a
 * confirmatory warning. It will also delete the associated Node
 * data directory if it is exists.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "cmd_delete.hpp"
#include "wibblerelationship.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Dispatching logic for the "delete" operation on Wibble nodes.
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 */
void wibble::WibbleCmdDelete::handle_delete(pkg_options& OPTS, search_filters& SEARCH)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);
    WibbleRecord w;

    w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH); 

    std::cout << std::unitbuf;
    
    bool deletion;
    bool data_delete_required = false;
    bool rels_delete_required = false;

    const std::string path_base = OPTS.paths.wibble_store + "/note_data/" + w.get_id().substr(0,2);
    const std::string dd_path = path_base + "/" + w.get_id();

    w.pretty_print_record_full();
    // additional warning if a data directory and all its files will also be deleted
    // use standard unix shell commands to easily calculate data about to be removed
    if(w.get_dd() != "___NULL___")
    {
        if(std::filesystem::exists(std::filesystem::path(dd_path)))
        {
            data_delete_required = true;
            wc.console_write_red_bold("\nWARNING. Note has data directory.\n");
            std::system(("echo -n 'Directory contains '; du -hs \"" + dd_path + "\" | cut -z -f 1; echo -n ' of data.'").c_str());
            wc.console_write_red("\nDeletion operation will remove entire data directory: \n");
            wc.console_write_red(dd_path + '\n');
        }
    }

    // additional warning if a Node has relations that need removing
    if(w.get_lids() != "___NULL___")
    {
        if(std::filesystem::exists(std::filesystem::path(WibbleRelationship::determine_rn_filename(wib_e, w, SEARCH.archive_db))))
        {
            rels_delete_required = true;
            wc.console_write_red_bold("\nWARNING. Note has relationships defined.\n");
            wc.console_write_red("\nDeletion operation will remove relationships from all connected Nodes.\n");
        }
    }
    
    wc.console_write_yellow_bold("\nPermanently delete above record?\n");
    std::string response = wc.console_write_prompt("Type 'delete' to confirm > ", "", "");
    wc.console_write("");
    
    if(wc.trim(response) == "delete")
    {
        bool proceed = true;
        if(rels_delete_required)
        {
            WibbleRelationship wr{wib_e, wc};
            bool rel_removal = wr.remove_all_relationships_for_record(w, SEARCH.archive_db);
            if(rel_removal) { wc.console_write_success_green("Relationships successfully removed.");                           }
            else            { wc.console_write_error_red("Error removing relationships for Node. Aborting."); proceed = false; }
        }
        
        if(proceed)
        {
            if(! SEARCH.archive_db) { deletion = wib_e.op_remove_node(w, false, OPTS.files.main_db, OPTS.paths.tmp_dir);     }
            else                    { deletion = wib_e.op_remove_node(w, true, OPTS.files.archived_db, OPTS.paths.tmp_dir); }

            if(deletion)
            {
                wc.console_write_success_green("Record " + w.get_id() + " successfully removed from database.");
                if(data_delete_required)
                {
                    try
                    {
                        std::filesystem::remove_all(dd_path);
                        if(std::filesystem::is_empty(path_base)) { std::filesystem::remove(path_base); }
                        wc.console_write_success_green("Successfully removed data directory: " + dd_path);
                    }
                    catch(std::filesystem::filesystem_error& ex)
                    {
                        //std::cerr << "Error deleting directory: " << ex.what() << '\n';
                        wc.console_write_error_red("Error deleting note data directory.");
                    }
                }
            }
            else
            { wc.console_write_error_red("Error deleting record: " + w.get_id() + ". Record NOT deleted."); }
        }
    }
    else
    {
        wc.console_write_error_red("User aborted. Record " + w.get_id() + " not deleted.");
    }
}
