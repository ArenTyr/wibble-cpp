#include <doctest/doctest.h>
#include "../wibbleconsole.hpp"
#include <string>
#include <sstream>
#include "../external/rang.hpp"

/*
TEST_CASE("Testing the various console colour output functions")
{
    wibble::WibbleConsole cw;
    std::ostringstream console_output;
    MESSAGE("\n\n0. Checking default writer wrapper");
    console_output << "Hello there!" << std::endl;
    CHECK(console_output.str() == (cw.console_write("Hello there!")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n1. Checking green string");
    console_output << rang::fg::green << "A green string";
    CHECK(console_output.str() == (cw.console_write_green("A green string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n2.Checking green bold string");
    console_output << rang::fg::green << rang::style::bold << "A green bold string";
    CHECK(console_output.str() == (cw.console_write_green_bold("A green bold string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n3.Checking yellow string");
    console_output << rang::fg::yellow << "A yellow string";
    CHECK(console_output.str() == (cw.console_write_yellow("A yellow string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n4.Checking yellow bold string");
    console_output << rang::fg::yellow << rang::style::bold << "A yellow bold string";
    CHECK(console_output.str() == (cw.console_write_yellow_bold("A yellow bold string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n5.Checking red string");
    console_output << rang::fg::red << "A red string";
    CHECK(console_output.str() == (cw.console_write_red("A red string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n6.Checking red bold string");
    console_output << rang::fg::red << rang::style::bold << "A red bold string";
    CHECK(console_output.str() == (cw.console_write_red_bold("A red bold string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n7.Checking success green string");
    console_output << rang::fg::green << rang::style::bold << "✓ A green success string\n";
    CHECK(console_output.str() == (cw.console_write_success_green("A green success string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n8.Checking error red string");
    console_output << rang::fg::red << rang::style::bold << "❌ A red error string\n";
    CHECK(console_output.str() == (cw.console_write_error_red("A red error string")));
    console_output.str(""); console_output.clear();

    MESSAGE("\n\n9.Checking header function");
    CHECK("━━━━━━━━━━━━━━━━━━━━ [    NOTE    ] ━━━━━━━━━━━━━━━━━━━━" == (cw.console_write_header("NOTE")));
    CHECK("━━━━━━━━━━━━━━━━━━━━ [   NOTE    ] ━━━━━━━━━━━━━━━━━━━━" == (cw.console_write_header("NOTE ")));
    MESSAGE("\n\n10.Checking horizontal divider");
    CHECK("───────────────────────────────────────────────────────" == (cw.console_write_hz_divider()));
    MESSAGE("\n\n11. Checking horizontal heavy divider");
    CHECK("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" == (cw.console_write_hz_heavy_divider()));
    MESSAGE("\n\n12.Checking header overflow handling");
    CHECK("━━━━━━━━━━━━━━━━━━━━━━━ [ .... ] ━━━━━━━━━━━━━━━━━━━━━━" == (cw.console_write_header("TOO_LONG_INPUT_HEADER")));
}
*/

TEST_CASE("Checking string trimming/input utility functions")
{
    wibble::WibbleConsole cw;
    MESSAGE("1. Checking ltrim in-place");
    std::string unclean_input = "     [STRING 1]     ";
    cw.ltrim(unclean_input);
    //wibble::WibbleConsole::ltrim(unclean_input);
    MESSAGE("2. Checking rtrim in-place");
    CHECK("[STRING 1]     " == unclean_input);
    cw.rtrim(unclean_input);
    CHECK("[STRING 1]" == unclean_input);
    MESSAGE("3. Checking trim in-place");
    unclean_input = "     [STRING 2]     ";
    cw.trim(unclean_input);
    CHECK("[STRING 2]" == unclean_input);

    unclean_input = "     [STRING 3]     ";
    MESSAGE("4. Checking ltrim copy");
    std::string result = cw.ltrim_copy(unclean_input); 
    CHECK("[STRING 3]     " == result);
    MESSAGE("5. Checking rtrim copy");
    std::string result2 = cw.rtrim_copy(unclean_input); 
    CHECK("     [STRING 3]" == result2);
    MESSAGE("6. Checking trim copy");
    std::string result3 = cw.trim_copy(unclean_input); 
    CHECK("[STRING 3]" == result3);
    MESSAGE("7. Verifying original unaffected string");
    CHECK("     [STRING 3]     " == unclean_input);
}

TEST_CASE("Checking GNU Readline pre-filled prompt")
{
    MESSAGE("Skipping prompt test");
    /*wibble::WibbleConsole wc;
    std::string result = wc.console_write_prompt("Colour prompt plus symbol", ">", "I am lovely");
    std::cout << "Input result: " << result << std::endl;
    result = wc.console_write_prompt("", "$", "ls -al");
    std::cout << "Input result: " << result << std::endl;
    result = wc.console_write_prompt("Colour prompt only. Age:", "", "30");
    std::cout << "Input result: " << result << std::endl; */
}

TEST_CASE("Checking GNU Readline multi-line input then normal prompt afterwards")
{
    MESSAGE("Skipping prompt test");
    /*wibble::WibbleConsole wc;
    std::string result = wc.console_write_prompt_ml("End input with ctrl-D\n", "", "");
    std::cout << "Input result: " << result << std::endl;
    result = wc.console_write_prompt("Normal prompt", ">", ""); */
}
