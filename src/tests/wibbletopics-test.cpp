#include <doctest/doctest.h>
#include "../wibbleconsole.hpp"
#include "../wibbletopics.hpp"
#include <filesystem>
#include <iostream>
#include <fstream>

TEST_CASE("Testing Wibble topic parser / accumulate_topic_vector()")
{
    MESSAGE("Test case: === WIBTOPIC 001 ==="); 
    wibble::WibbleTopics wt{"/tmp", "", ""};
    wibble::WibbleConsole wc;
    MESSAGE("Checking topic parser");
    wt.accumulate_topic_map(wc, "./test-stubs");
    wt.accumulate_topic_map(wc, "/tmp/wibble++-testing/topics");
    wt.list_available_topics(wc);
}

