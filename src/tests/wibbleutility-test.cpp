#include <doctest/doctest.h>
#include "../wibbleutility.hpp"
#include <string>
#include <sstream>
#include "../external/rang.hpp"
#include <cstdlib>

TEST_CASE("Testing expand tilde...")
{
    const char* env_home = std::getenv("HOME");
    std::string comp_string = "~/doc/wibble";
    std::string homedir = std::string(env_home) + "/doc/wibble";
    wibble::WibbleUtility::expand_tilde(comp_string);
    CHECK(homedir == comp_string);
}
