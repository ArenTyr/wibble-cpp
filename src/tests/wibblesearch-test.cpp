#include <iostream>
#include <filesystem>

#include "../external/doctest.h"
#include "../wibblebinlist.hpp"
#include "../wibblesearch.hpp"
#include "../wibbledbqueryinterface.hpp"

TEST_CASE("Testing execute_search_pipeline()")
{
    MESSAGE("Test case: === WIBSEARCH 001 ===");

    // Filter test database by
    // 1. All notes containing "Puppy" in title (should match Puppy Linux)
    // 2. Of these (two), all note(s) containing "Installation" in description 
    wibble::WibbleSearch::search_frame s1, s2;
    wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_TITLE;
    s1.field = s_field;
    s1.query = "Puppy";
    s_field = wibble::SEARCH_FIELD::F_DESC;
    s2.field = s_field;
    s2.query = "Installation";
    const std::string DB_FILE = "./test-stubs/wibble_db_testing.rec";
    const std::string WORKFILE = "./test-stubs/wibblework";
    wibble::WibbleSearch ws(DB_FILE, WORKFILE, wibble::WibBinList().gen_bin_list("UNSET", "test"));
    
    ws.add_search_frame(s1);
    ws.add_search_frame(s2);

    wibble::WibResultList wr = ws.execute_search_pipeline();
    for(auto& i: *wr)
    {
        i.pretty_print_record_full_nc();
    }
    CHECK("Tue, 24 Oct 2023 14:53:26 +0100" == wr->front().get_date());
    CHECK("46023fa6-5d84-4be4-b857-30f4673c78fc" == wr->front().get_id());
    CHECK("Linux: Puppy Linux DOCUMENTATION" == wr->front().get_title());
    CHECK("md" == wr->front().get_type());
    CHECK("Puppy Linux Documentation/Configuration/Installation notes" == wr->front().get_desc());
    CHECK("Computing" == wr->front().get_proj());
    CHECK("#attachment #notelink #documentation #linux" == wr->front().get_tags());
    CHECK("reference" == wr->front().get_class());
    CHECK("46023fa6-5d84-4be4-b857-30f4673c78fc" == wr->front().get_dd());
    CHECK("___NULL___" == wr->front().get_kp());
    CHECK("___NULL___" == wr->front().get_cust());
    if(std::filesystem::exists(std::filesystem::path(WORKFILE)))        { std::filesystem::remove(std::filesystem::path(WORKFILE));       }
    if(std::filesystem::exists(std::filesystem::path(WORKFILE + "2")))  { std::filesystem::remove(std::filesystem::path(WORKFILE + "2")); }
}

TEST_CASE("Testing execute_search_pipeline() 2")
{
    MESSAGE("Test case: === WIBSEARCH 002 ===");

    const std::string DB_FILE = "./test-stubs/wibble_db_testing.rec";
    const std::string WORKFILE = "./test-stubs/wibblework";
    wibble::WibbleSearch ws(DB_FILE, WORKFILE, wibble::WibBinList().gen_bin_list("UNSET", "test"));

    // A deliberately ridiculously inefficient search/bad criteria to exercise search pipeline/FIFO.
    // Filter test database by
    // 1. All notes of filetype "adoc"
    // 2. Of these, only those containing tag "#unix" (2 results)
    // 3. Filter these by project = "Computing" (return same results)
    // 4. Filter these by date containing "Oct" (return same results) 
    // 5. Finally narrow it to one searching by description containing "tmux"
    
    wibble::WibbleSearch::search_frame s1, s2, s3, s4, s5;

    // 1
    wibble::SEARCH_FIELD s_field = wibble::SEARCH_FIELD::F_TYPE;
    s1.field = s_field;
    s1.query = "adoc";

    // 2
    s_field = wibble::SEARCH_FIELD::F_TAGS;
    s2.field = s_field;
    s2.query = "#unix";

    // 3
    s_field = wibble::SEARCH_FIELD::F_PROJ;
    s3.field = s_field;
    s3.query = "Computing";

    // 4
    s_field = wibble::SEARCH_FIELD::F_DATE;
    s4.field = s_field;
    s4.query = "Oct";

    // 5
    s_field = wibble::SEARCH_FIELD::F_DESC;
    s5.field = s_field;
    s5.query = "tmux";

    ws.add_search_frame(s1);
    ws.add_search_frame(s2);
    ws.add_search_frame(s3);
    ws.add_search_frame(s4);
    ws.add_search_frame(s5);

    wibble::WibResultList wr = ws.execute_search_pipeline();
    for(auto& i: *wr)
    {
        i.pretty_print_record_full_nc();
    }
    CHECK("Tue, 24 Oct 2023 14:53:26 +0100" == wr->front().get_date());
    CHECK("88fa4979-3a2b-4a49-a30c-93de18887594" == wr->front().get_id());
    CHECK("Linux: Tmux Documentation keyboard shortcuts" == wr->front().get_title());
    CHECK("adoc" == wr->front().get_type());
    CHECK("Tmux documentation and keyboard shortcuts" == wr->front().get_desc());
    CHECK("Computing" == wr->front().get_proj());
    CHECK("#attachment #tmux #documentation #linux #unix" == wr->front().get_tags());
    CHECK("reference" == wr->front().get_class());
    CHECK("88fa4979-3a2b-4a49-a30c-93de18887594" == wr->front().get_dd());
    CHECK("___NULL___" == wr->front().get_kp());
    CHECK("___NULL___" == wr->front().get_cust());
    if(std::filesystem::exists(std::filesystem::path(WORKFILE)))       { std::filesystem::remove(std::filesystem::path(WORKFILE));       }
    if(std::filesystem::exists(std::filesystem::path(WORKFILE + "2"))) { std::filesystem::remove(std::filesystem::path(WORKFILE + "2")); }
}
