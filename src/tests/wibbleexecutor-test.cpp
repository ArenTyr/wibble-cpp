#include <doctest/doctest.h>
#include "../wibbleconsole.hpp"
#include "../wibbleutility.hpp"

TEST_CASE("Testing _get_binary_tool")
{
    MESSAGE("\n\n0. Testing get_binary_tool");
    MESSAGE("Test case: === WIBUTILITY 001 ===");
    std::string good_bin_mapping = "odt:lowriter, html:xfwrite, jpg:gimp#sxiv";
    std::string bad_bin_mapping = "odt,,,";
    std::string bad_bin_mapping2 = "";

    wibble::WibbleConsole wc;
    std::string lowriter  = wibble::WibbleUtility::get_binary_tool(wc, "odt", good_bin_mapping, true);
    std::string lowriterv = wibble::WibbleUtility::get_binary_tool(wc, "odt", good_bin_mapping, false);
    std::string xfwrite   = wibble::WibbleUtility::get_binary_tool(wc, "html", good_bin_mapping, true);
    std::string xfwritev  = wibble::WibbleUtility::get_binary_tool(wc, "html", good_bin_mapping, false);
    std::string gimp      = wibble::WibbleUtility::get_binary_tool(wc, "jpg", good_bin_mapping, true);
    std::string sxiv      = wibble::WibbleUtility::get_binary_tool(wc, "jpg", good_bin_mapping, false);

    std::string bad1 = wibble::WibbleUtility::get_binary_tool(wc, "foo", good_bin_mapping, false);
    std::string bad2 = wibble::WibbleUtility::get_binary_tool(wc, "", good_bin_mapping, false);
    std::string bad3 = wibble::WibbleUtility::get_binary_tool(wc, "UNSET", good_bin_mapping, false);
    std::string bad4 = wibble::WibbleUtility::get_binary_tool(wc, "foo", good_bin_mapping, false);
    std::string bad5 = wibble::WibbleUtility::get_binary_tool(wc, "odt", bad_bin_mapping, false);
    std::string bad6 = wibble::WibbleUtility::get_binary_tool(wc, "", bad_bin_mapping, false);
    std::string bad7 = wibble::WibbleUtility::get_binary_tool(wc, "foo", good_bin_mapping, false);

    CHECK(lowriter  == "lowriter");
    CHECK(lowriterv == "lowriter");
    CHECK(xfwrite   == "xfwrite");
    CHECK(xfwritev  == "xfwrite");
    CHECK(gimp      == "gimp");
    CHECK(sxiv      == "sxiv");
    CHECK(bad1      == "");
    CHECK(bad2      == "");
    CHECK(bad3      == "");
    CHECK(bad4      == "");
    CHECK(bad5      == "");
    CHECK(bad6      == "");
    CHECK(bad7      == "");
}
