#include <doctest/doctest.h>
#include <ctime>
#include "../wibblesync.hpp"

TEST_CASE("Testing Wibble sync function")
{
    MESSAGE("Test case: === WIBSYNC 001 ===");
    wibble::WibbleSync ws;
    ws.sync_directories("./test-stubs/sync_test/dir_a", "./test-stubs/sync_test/dir_b");
}

TEST_CASE("Testing interactive Wibble sync function")
{
    MESSAGE("Test case: === WIBSYNC 002 ===");
    wibble::WibbleSync ws;
    ws.sync_directories("/tmp/sync_test/dir_a", "/tmp/sync_test/dir_b", false, false, false);
    ws.sync_directories("/tmp/sync_test/dir_a", "/tmp/sync_test/dir_b", false, true, false);
}
