#!/bin/sh
#
# WIBBLE_VERSION: "2024-03-17 15:07:31"
# WIBBLE_DESC_START
# This file exists simply to test the WibbleSync parsing functionality.
# It is a script file that does nothing.
# WIBBLE_DESC_END

echo "I am a WibbleSync test file!"
