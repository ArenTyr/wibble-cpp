#!/bin/sh
# foo4.sh
# WIBBLE_VERSION: "2023-09-24 15:07:30"
# WIBBLE_DESC_START
# This file exists simply to test the WibbleSync parsing functionality.
# It is a script file that does nothing.
# WIBBLE_DESC_END

echo "I am a WibbleSync test file!"
