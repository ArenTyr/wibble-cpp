#include <doctest/doctest.h>
#include <ctime>
#include "../wibblecache.hpp"

TEST_CASE("Testing Wibble cache cleaning")
{
    MESSAGE("Test case: === WIBCACHE 001 ===");
    wibble::WibbleCache::clean_cache("~/.cache/wibble");
}
