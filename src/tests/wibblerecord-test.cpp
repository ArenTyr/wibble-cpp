#include <doctest/doctest.h>
#include "../wibblerecord.hpp"
#include <ctime>
#include <iomanip>
#include <iostream>
#include <memory>
#include <new>
#include <string>
#include <utility>
#include <vector>

TEST_CASE("Testing Wibble record constructor/initialiser list")
{
    MESSAGE("Test case: === WIBRECORD 001 ===");
    wibble::WibbleRecord wb("December 2023",
                            "A note title",
                            "id-abc-123-345",
                            "Some note description",
                            "txt",
                            "Note Project",
                            "#tag1 #tag2 #tag3",
                            "Reference Material",
                            "id-abc-666-666",
                            "priority:A#type:work#",
                            "Some custom field",
                            "S¬id-xyz-333-333 C¬id-abc-123-123", false);
    
    MESSAGE("Checking all parameters have been initialised correctly");
    CHECK("December 2023" == wb.get_date());
    CHECK("A note title" == wb.get_title());
    CHECK("id-abc-123-345" == wb.get_id());
    CHECK("Some note description" == wb.get_desc());
    CHECK("txt" == wb.get_type());
    CHECK("Note_Project" == wb.get_proj());
    CHECK("#tag1 #tag2 #tag3" == wb.get_tags());
    CHECK("Reference Material" == wb.get_class());
    CHECK("id-abc-666-666" == wb.get_dd());
    CHECK("priority:A#type:work#" == wb.get_kp());
    CHECK("Some custom field" == wb.get_cust());
    CHECK("S¬id-xyz-333-333 C¬id-abc-123-123" == wb.get_lids());

    MESSAGE("Checking parameters when initialised via std::move variable/reference");
    std::string note_title = "A NEW note title";
    wibble::WibbleRecord wb2("November 2023",
                             std::move(note_title),
                             "wb2-id-abc-123-345",
                             "Some other note description",
                             "md",
                             "Another Note Project_Test",
                             "#tag4 #tag5 #tag6",
                             "Temporary Material",
                             "wb2-id-abc-666-666",
                             "priority:B#type:fun#",
                             "Some other custom field",
                             "S¬id-ABC-333-333 C¬id-ZZZ-123-123", false);
    CHECK("November 2023" == wb2.get_date());
    CHECK("A NEW note title" == wb2.get_title());
    CHECK("wb2-id-abc-123-345" == wb2.get_id());
    CHECK("Some other note description" == wb2.get_desc());
    CHECK("md" == wb2.get_type());
    CHECK("Another_Note_Project_Test" == wb2.get_proj());
    CHECK("#tag4 #tag5 #tag6" == wb2.get_tags());
    CHECK("Temporary Material" == wb2.get_class());
    CHECK("wb2-id-abc-666-666" == wb2.get_dd());
    CHECK("priority:B#type:fun#" == wb2.get_kp());
    CHECK("Some other custom field" == wb2.get_cust());
    CHECK("S¬id-ABC-333-333 C¬id-ZZZ-123-123" == wb2.get_lids());
    
    MESSAGE("Verifying member data parameter set via std::move unaffected when original string storage is mutated");
    note_title = "original note title has been MUTATED";
    CHECK("A NEW note title" == wb2.get_title());

    MESSAGE("Checking pretty print");
    wb2.pretty_print_record_full();
}

TEST_CASE("Testing Wibble record individual getters/setters")
{
    MESSAGE("Test case: === WIBRECORD 002 ===");
    wibble::WibbleRecord wb;

    MESSAGE("1. Checking get/set date");
    wb.set_date("2023 December 20th");
    CHECK("2023 December 20th" == wb.get_date());

    MESSAGE("2. Checking get/set title");
    wb.set_title("Some note title");
    CHECK("Some note title" == wb.get_title());

    MESSAGE("3. Checking get/set id");
    wb.set_id("0656fa9a-84ce-47c2-9765-cf3ec1539733");
    CHECK("0656fa9a-84ce-47c2-9765-cf3ec1539733" == wb.get_id());

    MESSAGE("4. Checking get/set desc");
    wb.set_desc("Some note description");
    CHECK("Some note description" == wb.get_desc());

    MESSAGE("5. Checking get/set type");
    wb.set_type("txt");
    CHECK("txt" == wb.get_type());
    wb.set_type("type with spaces");
    CHECK("type_with_spaces" == wb.get_type());

    MESSAGE("6. Checking get/set proj");
    wb.set_proj("Some Project");
    CHECK("Some_Project" == wb.get_proj());

    MESSAGE("7. Checking get/set tags");
    wb.set_tags("#tag1 #tag2 #tag3");
    CHECK("#tag1 #tag2 #tag3" == wb.get_tags());

    MESSAGE("8. Checking get/set class");
    wb.set_class("Reference");
    CHECK("Reference" == wb.get_class());

    MESSAGE("9. Checking get/set datadirs");
    wb.set_dd("5556fa9a-84ce-47c2-9765-cf3ec1539733");
    CHECK("5556fa9a-84ce-47c2-9765-cf3ec1539733" == wb.get_dd());

    MESSAGE("10. Checking get/set keypairs");
    wb.set_kp("priority: High#people: Joe#");
    CHECK("priority: High#people: Joe#" == wb.get_kp());

    MESSAGE("11. Checking get/set custom");
    wb.set_cust("A lovely custom field value");
    CHECK("A lovely custom field value" == wb.get_cust());

    MESSAGE("12. Ensuring values are preserved");
    wibble::WibbleRecord wb2;
    wb2.set_date("2024 December 25th");
    CHECK("2023 December 20th" == wb.get_date());
    CHECK("2024 December 25th" == wb2.get_date());
}

TEST_CASE("Check/Profile vector allocation: [1] pre-allocation + array assign")
{
    MESSAGE("Test case: === WIBRECORD 003 ===");
    MESSAGE("Testing vector performance with mass records");
    std::vector<wibble::WibbleRecord> wib_records(10000);
    MESSAGE("Verifying capacity (10000)");
    CHECK(10000 == wib_records.capacity());
    MESSAGE("Creating 10000 fresh records");
    for(unsigned long i = 0; i < 10000; i++)
    {
        wibble::WibbleRecord wb("December 2023",
                                "Note number [" + std::to_string(i+1) + "]: Some note title",
                                "id-abc-123-345",
                                "Some note description",
                                "txt",
                                "Note Project",
                                "#tag1 #tag2 #tag3",
                                "Reference Material",
                                "id-abc-666-666",
                                "priority:A#type:work#",
                                "Some custom field",
                                "S¬id-xyz-333-333 C¬id-abc-123-123", false);
        wib_records[i] = wb;
    }

    // Print small selection
    for(unsigned long i = 0; i < 3; i++)
    {
        wib_records[i].pretty_print_record_full();
    }

    // with colour divider/index
    for(unsigned long i = 0; i < 3; i++)
    {
        wib_records[i].pretty_print_record_full(i, 3);
    }

    // raw no colour full record
    for(unsigned long i = 0; i < 3; i++)
    {
        wib_records[i].pretty_print_record_full_nc();
    }

    for(unsigned long i = 0; i < 10; i++)
    {
        wib_records[i].pretty_print_record_condensed();
    }

    for(unsigned long i = 0; i < 20; i++)
    {
        wib_records[i].pretty_print_record_condensed(i, 20);
    }

    MESSAGE("Checking capacity (not-resized) (10000)");
    std::cout << "vector SIZE: " << wib_records.size() << std::endl;
    std::cout << "vector CAPACITY: " << wib_records.capacity() << std::endl;

    MESSAGE("Checking memory capacity consumed for 10000 records");
    double bytes = (sizeof(decltype(wib_records.back())) * wib_records.capacity());
    std::cout << "Bytes         : " + std::to_string(bytes) << std::endl;
    std::cout << "Kilobytes (KB): " + std::to_string(bytes / (1024)) << std::endl;
    std::cout << "Megabytes (MB): " + std::to_string(bytes / (1024 * 1024)) << std::endl;
}

TEST_CASE("Check/Profile vector allocation: [2] reserve capacity +  emplace_back")
{
    MESSAGE("Test case: === WIBRECORD 004 ===");
    MESSAGE("Testing vector performance with mass records");
    std::vector<wibble::WibbleRecord> wib_records;
    wib_records.reserve(10000);
    MESSAGE("Verifying capacity (10000)");
    CHECK(10000 == wib_records.capacity());
    MESSAGE("Creating 1000 fresh records");
    for(unsigned long i = 0; i < 10000; i++)
    {
        wibble::WibbleRecord wb("December 2023",
                                "Note number [" + std::to_string(i+1) + "]: Some note title",
                                "id-abc-123-345",
                                "Some note description",
                                "txt",
                                "Note Project",
                                "#tag1 #tag2 #tag3",
                                "Reference Material",
                                "id-abc-666-666",
                                "priority:A#type:work#",
                                "Some custom field",
                                "S¬id-xyz-333-333 C¬id-abc-123-123", false);
        wib_records.emplace_back(wb);
    }

    for(unsigned long i = 0; i < 3; i++)
    {
        wib_records[i].pretty_print_record_full();
    }

    for(unsigned long i = 0; i < 10; i++)
    {
        wib_records.at(i).pretty_print_record_condensed();
    }

    MESSAGE("Checking capacity (not-resized) (10000)");
    std::cout << "SIZE: " << wib_records.size() << std::endl;
    std::cout << "CAPACITY: " << wib_records.capacity() << std::endl;

    MESSAGE("Checking memory capacity consumed for 10000 records");
    double bytes = (sizeof(decltype(wib_records.back())) * wib_records.capacity());
    std::cout << "Bytes         : " + std::to_string(bytes) << std::endl;
    std::cout << "Kilobytes (KB): " + std::to_string(bytes / (1024)) << std::endl;
    std::cout << "Megabytes (MB): " + std::to_string(bytes / (1024 * 1024)) << std::endl;
}

TEST_CASE("Check/Profile vector allocation: [3] shared pointer model +  push_back")
{
    MESSAGE("Test case: === WIBRECORD 005 ===");
    MESSAGE("Testing vector performance with mass records using shared pointer model");
    std::vector<std::shared_ptr<wibble::WibbleRecord>> wib_records;
    //wib_records.reserve(10000);
    //MESSAGE("Verifying capacity (10000)");
    //CHECK(10000 == wib_records.capacity());
    MESSAGE("Creating 10000 fresh records");
    for(unsigned long i = 0; i < 10000; i++)
    {
        wib_records.push_back(std::shared_ptr<wibble::WibbleRecord>(new wibble::WibbleRecord("December 2023",
                                                                                             "Note number [" + std::to_string(i+1) + "]: Some note title",
                                                                                             "id-abc-123-345",
                                                                                             "Some note description",
                                                                                             "txt",
                                                                                             "Note Project",
                                                                                             "#tag1 #tag2 #tag3",
                                                                                             "Reference Material",
                                                                                             "id-abc-666-666",
                                                                                             "priority:A#type:work#",
                                                                                             "Some custom field",
                                                                                             "S¬id-xyz-333-333 C¬id-abc-123-123", false)));
    }

    for(unsigned long i = 0; i < 3; i++)
    {
        wib_records[i]->pretty_print_record_full();
    }

    for(unsigned long i = 0; i < 10; i++)
    {
        wib_records.at(i)->pretty_print_record_condensed();
    }

    MESSAGE("Checking capacity (not-resized) (10000) [shared pointer]");
    std::cout << "SIZE: " << wib_records.size() << std::endl;
    std::cout << "CAPACITY: " << wib_records.capacity() << std::endl;

    MESSAGE("Checking memory capacity consumed for 10000 records [shared pointer]");
    double bytes = (sizeof(decltype(wib_records.back())) * wib_records.capacity());
    std::cout << "Bytes         : " + std::to_string(bytes) << std::endl;
    std::cout << "Kilobytes (KB): " + std::to_string(bytes / (1024)) << std::endl;
    std::cout << "Megabytes (MB): " + std::to_string(bytes / (1024 * 1024)) << std::endl;
}

TEST_CASE("Explicit destructor + constructor call on existing object to re-use constructor as full metadata update method")
{
    MESSAGE("Test case: === WIBRECORD 006 ===");
    wibble::WibbleRecord wb("December 2023",
                            "Some note title",
                            "id-abc-123-345",
                            "Some note description",
                            "txt",
                            "Note Project",
                            "#tag1 #tag2 #tag3",
                            "Reference Material",
                            "id-abc-666-666",
                            "priority:A#type:work#",
                            "Some custom field",
                            "S¬id-xyz-333-333 C¬id-abc-123-123", false);
    CHECK("December 2023" == wb.get_date());
    CHECK("txt" == wb.get_type());

    MESSAGE("Verifying placement new as technique for reusing constructor/updating entire object");
    wb.~WibbleRecord();
    new (&wb) wibble::WibbleRecord("January 2024",
                "Some note title",
                "id-abc-123-345",
                "Some note description",
                "md",
                "Note Project",
                "#tag1 #tag2 #tag3",
                "Reference Material",
                "id-abc-666-666",
                "priority:A#type:work#",
                "Some custom field",
                "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    CHECK("January 2024" == wb.get_date());
    CHECK("md" == wb.get_type());
}

TEST_CASE("Testing datestring converter: dates")
{
    MESSAGE("Test case: === WIBRECORD 007 ===");
    std::time_t t1 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("2020-10-15");
    std::time_t t2 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("2020-18-99", true);
    std::time_t t3 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("--", true);

    // debug
    // NOTE: gmtime and localtime return a pointer to a shared static GLOBAL pointer (yuck), so
    // care required in ordering/usage
    std::tm* tm1 = std::gmtime(&t1);
    std::cout << "1. " << tm1->tm_year << ":" << tm1->tm_mon << ":" << tm1->tm_mday << "  --  ";
    std::cout << std::put_time(std::gmtime(&t1), "%c %Z") << std::endl;
    REQUIRE(tm1->tm_year == 120);  // years are stored as: year - 1900
    REQUIRE(tm1->tm_mon  == 9);    // month is 0 indexed
    REQUIRE(tm1->tm_mday == 15);   // ...but day is 1 indexed

    std::tm* tm2 = std::gmtime(&t2);
    std::cout << "2. " << tm2->tm_year << ":" << tm2->tm_mon << ":" << tm2->tm_mday << "  --  ";
    std::cout << std::put_time(std::gmtime(&t2), "%c %Z") << std::endl;
    REQUIRE(tm2->tm_year == 0);
    REQUIRE(tm2->tm_mon  == 0);
    REQUIRE(tm2->tm_mday == 1);

    std::tm* tm3 = std::gmtime(&t3);
    std::cout << "3. " << tm3->tm_year << ":" << tm3->tm_mon << ":" << tm3->tm_mday << "  --  ";
    std::cout << std::put_time(std::gmtime(&t3), "%c %Z") << std::endl;
    REQUIRE(tm3->tm_year == 0);
    REQUIRE(tm3->tm_mon  == 0);
    REQUIRE(tm3->tm_mday == 1);
}

TEST_CASE("Testing datestring converter: datetimes")
{
    // time_t value for Monday 1900-01-01 00:00:01
    const long nineteenhundred = -2208988799;

    MESSAGE("Test case: === WIBRECORD 008 ===");
    std::time_t t1 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("2024-01-17 23:55:17", true, true);
    std::time_t t2 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("2020-10-15 15:37:19", true, true);
    std::time_t t3 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("2020-01-05 99:99:99", true, true);
    std::time_t t4 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("29020-18-99 99:99:99", true, true);
    std::time_t t5 = wibble::WibbleRecord::get_time_from_YYYY_MM_DD_str("--", true, true);
    //std::cout << "t vals: " << t1 << " / " << t2 << " / " << t3 << " / " <<  t4 << " / " << t5 << std::endl;

    REQUIRE(t3 == nineteenhundred);
    REQUIRE(t4 == nineteenhundred);
    REQUIRE(t5 == nineteenhundred);
    
    // debug
    std::tm* tm1 = std::gmtime(&t1);
    std::cout << "1. " << tm1->tm_year << ":" << tm1->tm_mon << ":" << tm1->tm_mday << "  --  ";
    std::cout << std::put_time(std::gmtime(&t1), "%c %Z") << std::endl;
    // remember quirky C tm time structure, stores seconds from epoch (1970) and years from 1900 onward
    REQUIRE(tm1->tm_year == 124); // 2024 - 1900 -> year
    REQUIRE(tm1->tm_mon  == 0);   // 01 - 1      -> month  
    REQUIRE(tm1->tm_mday == 17);  // 17          -> day of month 
    REQUIRE(tm1->tm_hour == 23);
    REQUIRE(tm1->tm_min  == 55);     
    REQUIRE(tm1->tm_sec  == 17);  

    std::tm* tm2 = std::gmtime(&t2);
    std::cout << "2. " << tm2->tm_year << ":" << tm2->tm_mon << ":" << tm2->tm_mday << "  --  ";
    std::cout << std::put_time(std::gmtime(&t2), "%c %Z") << std::endl;
    REQUIRE(tm2->tm_year == 120);  
    REQUIRE(tm2->tm_mon  == 9);
    REQUIRE(tm2->tm_mday == 15);
    REQUIRE(tm2->tm_hour == 15);
    REQUIRE(tm2->tm_min  == 37);     
    REQUIRE(tm2->tm_sec  == 19);  

    std::tm* tm3 = std::gmtime(&t3);
    std::cout << "3. " << tm3->tm_year << ":" << tm3->tm_mon << ":" << tm3->tm_mday << "  --  ";
    std::cout << std::put_time(std::gmtime(&t3), "%c %Z") << std::endl;
    REQUIRE(tm3->tm_year == 0); // 1900 is "error" date, so year = 0
    REQUIRE(tm3->tm_mon  == 0);
    REQUIRE(tm3->tm_mday == 1);
    REQUIRE(tm3->tm_hour == 0);
    REQUIRE(tm3->tm_min  == 0);     
    REQUIRE(tm3->tm_sec  == 1);  
}

TEST_CASE("Testing string replacement operators")
{
    MESSAGE("Test case: === WIBRECORD 009 ===");

    std::string ts = "This string contains spaces, a |, and slashes: / \\";
    wibble::WibbleRecord::remove_whitespace(ts);
    REQUIRE(ts == "This_string_contains_spaces,_a_|,_and_slashes:_/_\\");
    wibble::WibbleRecord::remove_slash(ts);
    REQUIRE(ts == "This_string_contains_spaces,_a_|,_and_slashes:____");
    wibble::WibbleRecord::remove_pipe(ts);
    REQUIRE(ts == "This_string_contains_spaces,_a__,_and_slashes:____");
    wibble::WibbleRecord::remove_colon(ts);
    REQUIRE(ts == "This_string_contains_spaces,_a__,_and_slashes_____");
    std::string ts2 = "This string contains spaces, a |, and slashes: / \\";
    wibble::WibbleRecord::sanitise_input(ts2);
    REQUIRE(ts == ts2);
}
