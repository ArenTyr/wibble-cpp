#include <doctest/doctest.h>
#include "../wibbleio.hpp"
#include "../wibblerecord.hpp"
#include <filesystem>
#include <iostream>
#include <fstream>

TEST_CASE("Testing Wibble I/O directory creation: create_note_path() / create_path()")
{
    MESSAGE("Test case: === WIBIO 001 ==="); 
    wibble::WibbleIO wio;
    std::filesystem::path tmp{std::filesystem::temp_directory_path()};
    const std::string wib_path = std::string(tmp) + "/testpath/test";
    MESSAGE("Checking path creation for notes/ and /note_data");
    // new note
    wio.create_note_path(wib_path, "abcde", "txt", "Project_Foo", true, false);
    // archived note
    wio.create_note_path(wib_path, "abcde", "txt", "Project_Foo", true, true);
    // data directory
    wio.create_note_path(wib_path, "abcde", "", "", false, false);
    REQUIRE(std::filesystem::exists(std::filesystem::path(wib_path + "/notes/txt/Project_Foo/ab")) == true);
    REQUIRE(std::filesystem::exists(std::filesystem::path(wib_path + "/archived_notes/txt/Project_Foo/ab")) == true);
    REQUIRE(std::filesystem::exists(std::filesystem::path(wib_path + "/note_data/ab")) == true);
}

TEST_CASE("Testing Wibble I/O file moving/copying: copy_new_note_file() / copy_file()")
{
    MESSAGE("Test case: === WIBIO 002 ==="); 
    wibble::WibbleIO wio;
    std::filesystem::path tmp{std::filesystem::temp_directory_path()};
    const std::string wib_path = std::string(tmp) + "/testpath/test";
    std::ofstream(wib_path + "/WIBBLEIO-test-file.txt").put('t');
    wio.copy_new_note_file(wib_path, std::string("ab4cd531-6660-41c6-a5bc-666eb4f0f37f"), std::string("txt"), std::string("Project_Foo"), wib_path + "/WIBBLEIO-test-file.txt", false);
    wio.copy_new_note_file(wib_path, std::string("ab4cd531-6660-41c6-a5bc-666eb4f0f37f"), std::string("txt"), std::string("Project_Foo"), wib_path + "/WIBBLEIO-test-file.txt", true);
    REQUIRE(std::filesystem::exists(std::filesystem::path(wib_path + "/notes/txt/Project_Foo/ab/ab4cd531-6660-41c6-a5bc-666eb4f0f37f.txt")) == true);
    REQUIRE(std::filesystem::exists(std::filesystem::path(wib_path + "/archived_notes/txt/Project_Foo/ab/ab4cd531-6660-41c6-a5bc-666eb4f0f37f.txt")) == true);
}

TEST_CASE("Checking check_if_text_file()")
{
    MESSAGE("Test case: === WIBIO 003 ==="); 
    REQUIRE(wibble::WibbleIO::check_if_text_file("./test-stubs/file_checks/plaintext.txt") == true);
    REQUIRE(wibble::WibbleIO::check_if_text_file("./test-stubs/file_checks/binaryfile.png") == false);
}

TEST_CASE("Checking get_path_minus_base_dir()")
{
    MESSAGE("Test case: === WIBIO 004 ==="); 
    std::string res = wibble::WibbleIO::get_path_minus_base_dir("./baz/chips", "./baz/chips/foo/bar");
    res = wibble::WibbleIO::get_path_minus_base_dir("./baz/chips/", "./baz/chips/foo/bar");
    REQUIRE(res == "foo/bar");
}


/*TEST_CASE("Checking get_path_minus_root_dir()")
{
    MESSAGE("Test case: === WIBIO 005 ==="); 
    std::string res = wibble::WibbleIO::get_path_minus_root_dir("./someparentdir/foo/bar");
    REQUIRE(res == "foo/bar");
    res = wibble::WibbleIO::get_path_minus_root_dir("/someparentdir/foo/bar");
    REQUIRE(res == "foo/bar");
    res = wibble::WibbleIO::get_path_minus_root_dir("someparentdir/foo/bar");
    REQUIRE(res == "foo/bar");
    res = wibble::WibbleIO::get_path_minus_root_dir("./fib/baz/foo/bar");
    REQUIRE(res == "baz/foo/bar");
}*/

TEST_CASE("Cleaning up test directories")
{
    MESSAGE("Cleaning up temporary directories === WIBIO CLEANUP ===");
    std::filesystem::path tmp{std::filesystem::temp_directory_path()};
    std::uintmax_t n{std::filesystem::remove_all(tmp / "testpath")};
    std::cout << "Deleted " << n << " files or directories\n";
}

