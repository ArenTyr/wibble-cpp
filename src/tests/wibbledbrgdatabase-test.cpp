#include <doctest/doctest.h>
#include "../wibbledbqueryinterface.hpp"
#include "../wibbledbrgdatabase.hpp"
#include "../wibbledbrgquery.hpp"
#include "../wibbleio.hpp"
#include "../wibblemetaschema.hpp"

#include <cstdio>
#include <iostream>
#include <filesystem>

//FIXME: Tests use test files outside of test tree

TEST_CASE("Testing update_existing_record")
{
    MESSAGE("Test case: === WIBRGDB 001 ===");
    wibble::WibbleRecord wb("December 2023",
                            "Some note title",
                            "id-abc-123-345",
                            "Some note description",
                            "txt",
                            "Note Project",
                            "#tag1 #tag2 #tag3",
                            "Reference Material",
                            "id-abc-666-666",
                            "priority:A#type:work#",
                            "Some custom field",
                            "S¬id-xyz-333-333 C¬id-abc-123-123", false);
    CHECK("December 2023" == wb.get_date());
    CHECK("txt" == wb.get_type());

    MESSAGE("Updating object with update_existing_record()");
    wibble::WibbleRGDatabase w_rg;

    w_rg.update_existing_record(wb,
                                "UPDATED record 2024",
                                "UPDATED Some note title",
                                "UPDATED-id-abc-123-345",
                                "UPDATED Some note description",
                                "md",
                                "UPDATED-Note Project",
                                "#UPDATED #tag1 #tag2 #tag3",
                                "UPDATED Reference Material",
                                "UPDATED-id-abc-666-666",
                                "UPDATED:UPDATED#priority:A#type:work#",
                                "UPDATED Some custom field",
                                "UPDATED-S¬id-xyz-333-333 C¬id-abc-123-123");
    CHECK("UPDATED record 2024" == wb.get_date());
    CHECK("UPDATED Some note title" == wb.get_title());
    CHECK("UPDATED-id-abc-123-345" == wb.get_id());
    CHECK("UPDATED Some note description" == wb.get_desc());
    CHECK("UPDATED-Note_Project" == wb.get_proj());
    CHECK("#UPDATED #tag1 #tag2 #tag3" == wb.get_tags());
    CHECK("UPDATED Reference Material" == wb.get_class());
    CHECK("UPDATED-id-abc-666-666" == wb.get_dd());
    CHECK("UPDATED:UPDATED#priority:A#type:work#" == wb.get_kp());
    CHECK("UPDATED Some custom field" == wb.get_cust());
    CHECK("UPDATED-S¬id-xyz-333-333 C¬id-abc-123-123" == wb.get_lids());
}

TEST_CASE("Checking create record")
{
    MESSAGE("Test case: === WIBRGDB 002 ===");
    MESSAGE("Skipping interactive input test");
    //wibble::WibbleRGDatabase w_rg;
    //w_rg.create_new_record_interactive();
}

TEST_CASE("Checking persist NEW record")
{
    MESSAGE("Test case: === WIBRGDB 003 ===");
    wibble::WibbleRGDatabase w_rg;
    wibble::WibbleRecord wb("December 2023",
                            "Some note title",
                            "id-abc-123-345",
                            "Some note description",
                            "txt",
                            "Note Project",
                            "#tag1 #tag2 #tag3",
                            "Reference Material",
                            "id-abc-666-666",
                            "priority:A#type:work#",
                            "Some custom field",
                            "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    const std::string TEMP_FILE = "./test-stubs/.testing.db_test.rec";
    MESSAGE("Writing out to temporary Wibble database");
    w_rg.persist_record(true, wb, TEMP_FILE, "", "");
    MESSAGE("Cleaning up temporary files");
    if(std::filesystem::exists(std::filesystem::path(TEMP_FILE)))
    { std::filesystem::remove(std::filesystem::path(TEMP_FILE)); }
}

TEST_CASE("Checking persist/update of EXISTING records + DELETION of existing records")
{
    MESSAGE("Test case: === WIBRGDB 004 ===");
    MESSAGE("Writing out multiple records to temporary Wibble database");

    bool result;

    const std::string TEMP_DB        = "./test-stubs/.testing.db_test.rec";
    const std::string TEMP_FILE_BKUP = "./test-stubs/.testing.db_test.rec.bak";
    const std::string TEMP_WORK      = "./test-stubs/.wibble-workfile";

    wibble::WibbleRGDatabase w_rg;
    wibble::WibbleRecord wb1("January 2023 ONE",
                             "Some note title [1]",
                             "id-abc-123-345",
                             "Some note [1] description",
                             "txt",
                             "Note Project",
                             "#tag1 #tag2 #tag3",
                             "Reference Material",
                             "id-abc-666-666",
                             "priority:A#type:work#",
                             "Some custom field",
                             "S¬id-xyz-333-333 C¬id-abc-123-123", false);


    wibble::WibbleRecord wb2("January 2024 TWO",
                             "Some note title [2]",
                             "id-abc-567-789",
                             "Some other note [2] description",
                             "txt",
                             "Note Project",
                             "#tag1 #tag2 #tag3",
                             "Reference Material",
                             "id-abc-666-666",
                             "priority:A#type:work#",
                             "Some custom field",
                             "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    wibble::WibbleRecord wb3("January 2024 THREE",
                             "Some note title [3]",
                             "id-abc-789-1011",
                             "Some other note [3] description",
                             "txt",
                             "Note Project",
                             "#tag1 #tag2 #tag3",
                             "Reference Material",
                             "id-abc-666-666",
                             "priority:A#type:work#",
                             "Some custom field",
                             "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    wibble::WibbleRecord wb4("January 2024 FOUR",
                             "Some note title [4]",
                             "id-666ab-789-1011",
                             "Some other note [4] description",
                             "txt",
                             "Note Project",
                             "#tag1 #tag2 #tag3",
                             "Reference Material",
                             "id-abc-666-666",
                             "priority:A#type:work#",
                             "Some custom field",
                             "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    wibble::WibbleRecord wb5("January 2024 FIVE",
                             "Some note title [5]",
                             "id-abc-999abc-1011",
                             "Some other note [5] description",
                             "txt",
                             "Note Project",
                             "#tag1 #tag2 #tag3",
                             "Reference Material",
                             "id-abc-666-666",
                             "priority:A#type:work#",
                             "Some custom field",
                             "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    // write out the five records as new entries
    result = w_rg.persist_record(true, wb1, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);
    result = w_rg.persist_record(true, wb2, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);
    result = w_rg.persist_record(true, wb3, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);
    result = w_rg.persist_record(true, wb4, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);
    result = w_rg.persist_record(true, wb5, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);

    MESSAGE("Verifying records have been created");
    wibble::WibbleDBQueryInterface* query (new wibble::WibbleDBRGQuery);
    query->active_search_field = wibble::SEARCH_FIELD::F_TITLE;
    query->title_q = "Some note";
    wibble::WibResultList results = w_rg.get_wibble_records(query, TEMP_DB, TEMP_WORK);
    for(auto& result: *results)
    {
        //result.pretty_print_record_condensed();
        result.pretty_print_record_full();
    }

    // now update the middle 2nd record (ensure ID is a match)
    wibble::WibbleRecord wb2_u("January 2024 TWO UPDATED",
                            "Some note title [2] - UPDATED!",
                            "id-abc-567-789",
                            "Some other note [2] description UPDATED",
                            "txt",
                            "Note Project UPDATED",
                            "#tag1 #tag2 #tag3 UPDATED",
                            "Reference Material UPDATED",
                            "id-abc-666-666 UPDATED",
                            "priority:A#type:work#updated:updated#",
                            "Some custom field UPDATED",
                            "S¬id-xyz-333-333 C¬id-abc-123-123 UPDATED", false);

    // now update the middle 4th record (ensure ID is a match)
    wibble::WibbleRecord wb4_u("January 2024 FOUR UPDATED",
                            "Some note title [4] - UPDATED!",
                            "id-666ab-789-1011",
                            "Some other note [4] description UPDATED",
                            "txt",
                            "Note Project UPDATED",
                            "#tag1 #tag2 #tag3 UPDATED",
                            "Reference Material UPDATED",
                            "id-abc-666-666 UPDATED",
                            "priority:A#type:work#updated:updated#",
                            "Some custom field UPDATED",
                            "S¬id-xyz-333-333 C¬id-abc-123-123 UPDATED", false);


    // ensure flag for "create_new" is false
    MESSAGE("Verifying records [2, 4] have been updated");
    result = w_rg.persist_record(false, wb2_u, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);
    result = w_rg.persist_record(false, wb4_u, TEMP_DB, TEMP_WORK, "");
    CHECK(result == true);
    query->title_q = "UPDATED";
    results = w_rg.get_wibble_records(query, TEMP_DB, TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_full();
    }

    MESSAGE("Deleting multiple records [2,3,4] from temporary Wibble database");
    // by record
    result = w_rg.delete_record(wb2_u, TEMP_DB, TEMP_WORK);
    CHECK(result == true);
    result = w_rg.delete_record(wb4_u, TEMP_DB, TEMP_WORK);
    CHECK(result == true);
    // by raw id -- what was note [3]
    result = w_rg.delete_record("id-abc-789-1011", TEMP_DB, TEMP_WORK);
    CHECK(result == true);

    MESSAGE("Verifying delete fails when no singular match");
    result = w_rg.delete_record("id", TEMP_DB, TEMP_WORK);
    CHECK(result == false);

    MESSAGE("Verifying records have been deleted: only [1] and [5] should remain");
    query->title_q = "Some note";
    results = w_rg.get_wibble_records(query, TEMP_DB, TEMP_WORK);
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec");
    for(auto& result: *results)
    {
        result.pretty_print_record_full();
    }

    MESSAGE("Cleaning up temporary files");
    if(std::filesystem::exists(std::filesystem::path(TEMP_DB)))
    { std::filesystem::remove(std::filesystem::path(TEMP_DB));          }

    if(std::filesystem::exists(std::filesystem::path((TEMP_DB + ".new"))))
    { std::filesystem::remove(std::filesystem::path(TEMP_DB + ".new")); }

    if(std::filesystem::exists(std::filesystem::path(TEMP_FILE_BKUP)))
    { std::filesystem::remove(std::filesystem::path(TEMP_FILE_BKUP));   }

    if(std::filesystem::exists(std::filesystem::path(TEMP_WORK)))
    { std::filesystem::remove(std::filesystem::path(TEMP_WORK));        }

    delete query;
}

TEST_CASE("Checking ripgrep output dump all")
{
    MESSAGE("Test case: === WIBRGDB 005 ===");
    MESSAGE("Retrieving mass set of Wibble records");
    const std::string TEMP_WORK = "./test-stubs/.wibble-workfile";
    wibble::WibbleRGDatabase w_rg;
    // condensed view
    //w_rg.get_all_wibble_records("/home/vm/kvm-shared/wibble_db_1M.rec", true);
    //w_rg.get_all_wibble_records("/home/vm/kvm-shared/wibble_db_testing.rec", true, TEMP_WORK);
    //w_rg.get_all_wibble_records("./test-stubs/wibble_db_testing.rec", true, TEMP_WORK);
    w_rg.get_all_wibble_records("./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    MESSAGE("Checking memory capacity Wibble record set");
    double bytes = (sizeof(decltype(w_rg)));
    std::cout << "Bytes         : " + std::to_string(bytes) << std::endl;
    std::cout << "Kilobytes (KB): " + std::to_string(bytes / (1024)) << std::endl;
    std::cout << "Megabytes (MB): " + std::to_string(bytes / (1024 * 1024)) << std::endl;
    if(std::filesystem::exists(std::filesystem::path(TEMP_WORK))) { std::filesystem::remove(std::filesystem::path(TEMP_WORK)); }
}

TEST_CASE("Checking ripgrep queries on each field")
{
    MESSAGE("Test case: === WIBRGDB 006 ===");
    MESSAGE("Retrieving queried set of Wibble records");
    const std::string TEMP_WORK = "./.wibble-workfile";
    wibble::WibbleRGDatabase w_rg;
    //const std::unique_ptr<wibble::WibbleDBQueryInterface> query; // (new wibble::WibbleDBQueryInterface);
    wibble::WibbleDBQueryInterface* query (new wibble::WibbleDBRGQuery);

    MESSAGE("\n1. Querying with DATE = 'Oct'");
    query->active_search_field = wibble::SEARCH_FIELD::F_DATE;
    query->date_q = "Oct";
    //wibble::WibResultList results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    wibble::WibResultList results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n2. Querying with TITLE = 'Puppy'");
    query->active_search_field = wibble::SEARCH_FIELD::F_TITLE;
    query->title_q = "Puppy";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n3. Querying with NOTEID = '9a18'");
    query->active_search_field = wibble::SEARCH_FIELD::F_NOTEID;
    query->id_q = "9a18";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n4. Querying with DESCRIPTION = 'Librewolf'");
    query->active_search_field = wibble::SEARCH_FIELD::F_DESC;
    query->desc_q = "Librewolf";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n5. Querying with Project = 'Computing'");
    query->active_search_field = wibble::SEARCH_FIELD::F_PROJ;
    query->proj_q = "Computing";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n6. Querying with TAGS = '#linux'");
    query->active_search_field = wibble::SEARCH_FIELD::F_TAGS;
    query->tags_q = "#linux";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }


    MESSAGE("\n7. Querying with CLASS = 'Reference'");
    query->active_search_field = wibble::SEARCH_FIELD::F_CLS;
    query->cls_q = "Reference";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n8. Querying with DATADIRS = 'Wibble'");
    query->active_search_field = wibble::SEARCH_FIELD::F_DD;
    query->dd_q = "7c2a2c";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n9. Querying with KEYPAIRS = 'priority'");
    query->active_search_field = wibble::SEARCH_FIELD::F_KP;
    query->kp_q = "priority:important#";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n10. Querying with CUSTOM = 'Wibble'");
    query->active_search_field = wibble::SEARCH_FIELD::F_CUST;
    query->cust_q = "Custom test";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    MESSAGE("\n11. Querying with LinkedIds = 'a8dfde'");
    query->active_search_field = wibble::SEARCH_FIELD::F_LIDS;
    query->lids_q = "a8dfde";
    //results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }

    if(std::filesystem::exists(std::filesystem::path(TEMP_WORK))) { std::filesystem::remove(std::filesystem::path(TEMP_WORK)); }
    delete query;
}

TEST_CASE("Checking schema parser")
{
    MESSAGE("Test case: === WIBRGDB 007 ===");
    MESSAGE("Reading in schema file");
    wibble::WibbleRGDatabase w_rg;
    wibble::WibbleIO wio;
    wibble::metadata_schema ms = w_rg.parse_schema_file(wio.read_in_schema_file("code_snippet", "./test-stubs/schemas"));
    CHECK("<Tech>:" == ms.title);
    CHECK("<Tech>: Some code description" == ms.desc);
    CHECK("java" == ms.type);
    CHECK("Snippet" == ms.proj);
    CHECK("#reference #howto #snippet #<tech>" == ms.tags);
    CHECK("HOWTO" == ms.cls);
    CHECK("Custom text" == ms.cust);
}


TEST_CASE("Testing template substitution function")
{
    MESSAGE("Test case: === WIBRGDB 008 ===");

    MESSAGE("Copying template test file");
    const auto fs_options = std::filesystem::copy_options::overwrite_existing;
    std::filesystem::copy_file("./test-stubs/notes/txt/Note_Project/tt/tt-test.txt.orig", "./test-stubs/notes/txt/Note_Project/tt/tt-test.txt", fs_options);
    wibble::WibbleRecord wb("December 2023",
                            "Some note title",
                            "tt-test",
                            "Some note description",
                            "txt",
                            "Note Project",
                            "#tag1 #tag2 #tag3",
                            "Reference Material",
                            "id-abc-666-666",
                            "priority:A#type:work#",
                            "Some custom field",
                            "S¬id-xyz-333-333 C¬id-abc-123-123", false);

    wibble::WibbleRGDatabase w_rg;
    w_rg.perform_template_substitutions(wb, "./test-stubs");
}

TEST_CASE("Checking select_record() on individual record")
{
    MESSAGE("Test case: === WIBRGDB 009 ===");
    const std::string TEMP_WORK = "./.wibble-workfile";
    wibble::WibbleRGDatabase w_rg;
    //const std::unique_ptr<wibble::WibbleDBQueryInterface> query (new wibble::WibbleDBQueryInterface);
    wibble::WibbleDBQueryInterface* query (new wibble::WibbleDBRGQuery);

    MESSAGE("\n1. Checking against no results/null match");
    query->active_search_field = wibble::SEARCH_FIELD::F_DATE;
    query->date_q = "FOOBAR";
    //wibble::WibResultList results = w_rg.get_wibble_records(query, "/home/vm/kvm-shared/wibble_db_testing.rec", TEMP_WORK);
    wibble::WibResultList results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed();
    }
    // select record number 3 (0 indexed): should trigger exception, with empty record result full of ___NULL___
    wibble::WibbleRecord w = w_rg.select_record(results, 2);
    w.pretty_print_record_full();
    CHECK("___NULL___" == w.get_date());
    CHECK("___NULL___" == w.get_title());
    CHECK("___NULL___" == w.get_id());
    CHECK("___NULL___" == w.get_desc());
    CHECK("___NULL___" == w.get_type());
    CHECK("___NULL___" == w.get_proj());
    CHECK("___NULL___" == w.get_tags());
    CHECK("___NULL___" == w.get_class());
    CHECK("___NULL___" == w.get_dd());
    CHECK("___NULL___" == w.get_kp());
    CHECK("___NULL___" == w.get_cust());
    CHECK("___NULL___" == w.get_lids());

    MESSAGE("\n2. Checking against valid match");
    query->active_search_field = wibble::SEARCH_FIELD::F_PROJ;
    query->proj_q = "Computing";
    results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    w = w_rg.select_record(results, 2);
    w.pretty_print_record_full();
    CHECK("4fc48196-e7ef-4eec-9af0-5619dae1e0e8" == w.get_id());
    delete query;
}


TEST_CASE("Checking WibbleConsole + select_record() on individual record")
{

    MESSAGE("Test case: === WIBRGDB 010 ===");
    const std::string TEMP_WORK = "./.wibble-workfile";
    wibble::WibbleRGDatabase w_rg;
    wibble::WibbleDBQueryInterface* query (new wibble::WibbleDBRGQuery);
    MESSAGE("Querying against all 'Computing' project matches");
    query->active_search_field = wibble::SEARCH_FIELD::F_PROJ;
    query->proj_q = "Computing";
    wibble::WibResultList results = w_rg.get_wibble_records(query, "./test-stubs/wibble_db_testing.rec", TEMP_WORK);
    long tot_results = w_rg.result_size(results);
    long l = 0;
    for(auto& result: *results)
    {
        result.pretty_print_record_condensed(l, tot_results);
        ++l;
    }
    MESSAGE("Presenting input selector");
    MESSAGE("Skipping input selector test");
    /*
    wibble::WibbleConsole wc;
    long response = wc.console_present_record_selector(tot_results);

    std::cout << "Response is: " << response << std::endl;
    if(response != 0)
    {
        wibble::WibbleRecord w = w_rg.select_record(results, response - 1);
        w.pretty_print_record_full();
        CHECK(response <= tot_results);
        CHECK(response > 0);
    }
    */

    delete query;
}
