/**
 * @file      wibbledbrgquery.cpp
 * @brief     Provides the searching/querying architecture for WibbleRecord/Nodes.
 * @details
 *
 * This class encapsulates the generation of querying logic to
 * translate the user's input of field values into a search query
 * that will generate the correct result set (via ripgrep).
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "wibbleconsole.hpp"
#include "wibbledbrgquery.hpp"
#include "wibbledbinterface.hpp"
#include "wibbledbrgdatabase.hpp"

/**
 * @brief Ensure the given search expression is applied to the correct field.
 * @param SEARCH_FIELD field Selected WIB_FIELDS value/field to search/apply expression to
 * @param expression String containing regular expression/search value
 */
bool wibble::WibbleDBRGQuery::set_query_expression_for_field(SEARCH_FIELD field, std::string expression) 
{
    //std::cerr << "Expression is: " << expression << std::endl;
    switch(field)
    {
    case F_DATE:
        date_q = expression;
        wc.console_write_green("Filtering by notes with Date       : "); wc.console_write(expression);
        return true;
    case F_TITLE:
        title_q = expression;
        wc.console_write_green("Filtering by notes with Title      : "); wc.console_write(expression);
        return true;
    case F_NOTEID:
        id_q = expression;
        wc.console_write_green("Filtering by notes with NoteId     : "); wc.console_write(expression);
        return true;
    case F_DESC:
        desc_q = expression;
        wc.console_write_green("Filtering by notes with Description: "); wc.console_write(expression);
        return true;
    case F_TYPE:
        type_q = expression;
        wc.console_write_green("Filtering by notes with Filetype   : "); wc.console_write(expression);
        return true;
    case F_PROJ:
        proj_q = expression;
        wc.console_write_green("Filtering by notes with Project    : "); wc.console_write(expression);
        return true;
    case F_TAGS:
        tags_q = expression;
        wc.console_write_green("Filtering by notes with Tags       : "); wc.console_write(expression);
        return true;
    case F_CLS:
        cls_q = expression;
        wc.console_write_green("Filtering by notes with Class      : "); wc.console_write(expression);
        return true;
    case F_DD:
        dd_q = expression;
        wc.console_write_green("Filtering by notes with DataDirs   : "); wc.console_write(expression);
        return true;
    case F_KP:
        kp_q = expression;
        wc.console_write_green("Filtering by notes with KeyPairs   : "); wc.console_write(expression);
        return true;
    case F_CUST:
        cust_q = expression;
        wc.console_write_green("Filtering by notes with Custom     : "); wc.console_write(expression);
        return true;
    case F_LIDS:
        lids_q = expression;
        wc.console_write_green("Filtering by notes with LinkedIds  : "); wc.console_write(expression);
        return true;
    default:
        std::cerr << "Unknown field requested. Nothing set." << std::endl;
        return false;
    }
}

/**
 * @brief Generate a ripgrep search commmand based on the currently active search field.
 */
std::string wibble::WibbleDBRGQuery::generate_query() const
{
    std::string rg_regexp = "UNSET";
    // no line numbers, no context marker
    std::string rg_query = "rg -N --no-context-separator ";

    // support options to enable exact (non-regexp) searches, and fixed string/literal searches
    std::string glob_pfx = "(.*)+";
    std::string eol_term = "";
    std::string caret = "^";
    // default to case-insensitive searching
    std::string fixed_flag = "-i ";
    if(exact_search)  { glob_pfx = ""; fixed_flag = ""; eol_term = "$"; } 
    // fixed strings implies no use of regexp wildcards (symbols treated literally)
    if(fixed_strings) { fixed_flag = "-F "; glob_pfx = ""; caret = ""; }
    
    // get results, correctly spliced with before/after context: should always total 12 lines (record size)
    switch(active_search_field)
    {
    case F_DATE:
        rg_regexp = fixed_flag + "-B 1 -A 11 '" + caret + "Date: " + glob_pfx       + date_q  + glob_pfx + eol_term + "'";
        break;
    case F_TITLE:
        rg_regexp = fixed_flag + "-B 2 -A 10 '" + caret + "Title: " + glob_pfx      + title_q + glob_pfx + eol_term + "'";
        break;
    case F_NOTEID:
        rg_regexp = fixed_flag + "-B 3 -A 9 '" + caret + "NoteId: " + glob_pfx      + id_q    + glob_pfx + eol_term + "'";
        break;
    case F_DESC:
        rg_regexp = fixed_flag + "-B 4 -A 8 '" + caret + "Description: " + glob_pfx + desc_q  + glob_pfx + eol_term + "'";
        break;
    case F_TYPE:
        rg_regexp = fixed_flag + "-B 5 -A 7 '" + caret + "Filetype: " + glob_pfx    + type_q  + glob_pfx + eol_term + "'";
        break;
    case F_PROJ:
        rg_regexp = fixed_flag + "-B 6 -A 6 '" + caret + "Project: " + glob_pfx     + proj_q  + glob_pfx + eol_term + "'";
        break;
    case F_TAGS:
        rg_regexp = fixed_flag + "-B 7 -A 5 '" + caret + "Tags: " + glob_pfx        + tags_q  + glob_pfx + eol_term + "'";
        break;
    case F_CLS:
        rg_regexp = fixed_flag + "-B 8 -A 4 '" + caret + "Class: " + glob_pfx       + cls_q   + glob_pfx + eol_term + "'";
        break;
    case F_DD:
        rg_regexp = fixed_flag + "-B 9 -A 3 '" + caret + "DataDirs: " + glob_pfx    + dd_q    + glob_pfx + eol_term + "'";
        break;
    case F_KP:
        // FIXME: KeyPairs need more sophisticated filtering
        rg_regexp = fixed_flag + "-B 10 -A 2 '" + caret + "KeyPairs: " + glob_pfx   + kp_q    + glob_pfx + eol_term + "'";
        break;
    case F_CUST:
        rg_regexp = fixed_flag + "-B 11 -A 1 '" + caret + "Custom: " + glob_pfx     + cust_q  + glob_pfx + eol_term + "'";
        break;
    case F_LIDS:
        rg_regexp = fixed_flag + "-B 12 -A 0 '" + caret + "LinkedIds: " + glob_pfx  + lids_q  + glob_pfx + eol_term + "'";
        break;
    default:
        rg_regexp = "'UNKNOWN FIELD'";
    }
           
    return rg_query + rg_regexp;  
};

/**
 * @brief Critical function that determines the starting line index of the current database record.
 * @param wib_db_path String containing filename path to database file
 * @param WORKFILE String to temporary working file (should be in cache/tmp_dir)
 */
long wibble::WibbleDBRGQuery::get_existing_record_line_num(const std::string& wib_db_path, const std::string& WORKFILE) const
{
    std::string rg_cmd = "rg --vimgrep '^NoteId: " + id_q + "' " + wib_db_path + " > " + WORKFILE;
    //std::cerr << "DEBUG: running cmd: " << rg_cmd << std::endl;
    int res = std::system(rg_cmd.c_str());
    if(res == 1) // No results
    {
        return -1;
    }
    else
    {
        std::string line;
        std::stringstream  result_stream;
        result_stream << std::ifstream(WORKFILE).rdbuf();
        int matches = 0;
        long line_result = -3;
        do
        {
            std::string line_num = "";

            try
            {
                // rg --vimgrep matches of form: ./.testing.db_test.rec:16:1:NoteId: foo... 
                std::string tmp_s = line.substr(line.find(":") + 1, std::string::npos);
                line_num = tmp_s.substr(0, tmp_s.find(":"));
                
            }
            catch (std::out_of_range& ex) { line_result = -3; }
            
            try { line_result = std::stol(line_num); ++matches; }
            catch(std::invalid_argument const &ex) { line_result = -3;}

            //std::cerr << "DEBUG: Matches: '" << matches <<  "'" << std::endl;
            //std::cerr << "DEBUG: processing line: '" << line <<  "'" << std::endl;
            //std::cerr << "DEBUG: got note id line_num value: '" << line_num <<  "'" << std::endl;
        }
        while(std::getline(result_stream, line));

        if(matches == 0)
        {
            return -1;
        }
        else if(matches != 1)
        {
            return -2;
        }
        else
        {
            return line_result;
        }
    }
}
