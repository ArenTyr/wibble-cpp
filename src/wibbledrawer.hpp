/**
 * @file      wibbledrawer.hpp
 * @brief     Provides all of the "draawer" functionality (header file).
 * @details
 *
 * This class provides all of the functionality related to drawer
 * management. A 'drawer' is a managed directory intended for transient
 * storage/organisation of files, prior to them either being discarded or
 * properly filed elsewhere.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_DRAWER_H
#define WIBBLE_DRAWER_H

#include <vector>
#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleDrawer
    {
    private:
        const std::string cache_dir;
        const std::string drawer_def = "drawer_index.wib";
        const std::string drawer_index;
        const std::string drawer_root;
        int def_drawer = 0;
        /**
         * @brief Struct that defines the data structure for a "drawer"
         */
        typedef struct drawer
        {
            std::string drawer_id;   //!< UUID of the drawer/directory
            std::string drawer_desc; //!< User's provided drawer "description"
        } drawer;
        std::vector<drawer> chest; //!< A chest of drawers...
        void _copy_success_msg(WibbleConsole& wc, const int mode);
        void _inventory_chest();
    public:
        WibbleDrawer(const std::string& tmp_dir, const std::string& drawers_dir):
        cache_dir{tmp_dir},
        drawer_index{drawers_dir + "/" + drawer_def},
        drawer_root{drawers_dir}
        {
            _inventory_chest();
        };

        bool add_drawer(WibbleConsole& wc);
        void cd_drawer_dir(WibbleConsole& wc, const int drawer_num);
        bool copy_drawer_to_node(WibbleConsole& wc, const pkg_options& OPTS, const int drawer_num);
        bool copy_drawer_to_topic(WibbleConsole& wc, const pkg_options& OPTS, const int drawer_num);
        bool delete_drawer(WibbleConsole& wc, const int drawer_num);
        bool display_chest(WibbleConsole& wc);
        bool push_file(WibbleConsole& wc, const std::string source_file, const int drawer_num, const bool force_mode);
        bool list_drawer(WibbleConsole& wc, const int drawer_num, const bool interactive, const bool tree_mode);
        bool select_drawer(WibbleConsole& wc, const int drawer_num);
    };
}
#endif
