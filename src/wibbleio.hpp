/**
 * @file      wibbleio.hpp
 * @brief     Provides various wrappers and convenience functions around I/O operations (header file).
 * @details
 *
 * Contains various (mostly static) functions related to writing, copying, moving,
 * and appending to files/stringstreams, employing C++17 <filesystem>; also
 * various helper functions regarding the Wibble database and directory operations.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEIO_H
#define WIBBLEIO_H

#include <filesystem>

#include "wibblemetaschema.hpp"

namespace wibble
{
    class WibbleIO
    {
    private:
    public:
        static std::filesystem::path generate_note_dir(const std::string& wib_path, const std::string& note_id, const std::string& note_ft,
                                       const std::string& proj, const bool archived); 
        static std::filesystem::path generate_note_path(const std::string& wib_path, const std::string& note_id, const std::string& note_ft,
                                       const std::string& proj, const bool archived); 
        static bool init_database_file(const std::string& db_header, const std::string& wib_db_path);
        static bool append_to_database_file(const std::string& serialized_record, const std::string& wib_db_path, bool write_header = false);
        static bool check_if_text_file(const std::string& fn);
        static bool delete_as_new_database_file(const std::string& wib_db_path, long line_number_index);
        static bool update_as_new_database_file(const std::string& serialized_record, const std::string& wib_db_path, long line_number_index);
        static bool update_as_new_database_file(const std::string& serialized_record, const std::string& wib_db_path, long line_number_index, bool delete_record);
        static bool _write_out_file(const std::string& file_contents, const std::string& file_dest, bool overwrite);
        static bool write_out_file_append(const std::string& file_contents, const std::string& file_dest);
        static bool write_out_file_overwrite(const std::string& file_contents, const std::string& file_dest);
        static std::string read_in_database_file(const std::string& wib_db_path);
        static std::stringstream read_file_into_stringstream(const std::string &input_file);
        static bool create_note_path(const std::string& wib_path, const std::string& note_id, const std::string& ftype,
                              const std::string& proj, bool note_not_data, bool archived);
        static bool create_path(const std::string& fs_path);
        bool copy_new_note_file(const std::string& wib_path, const std::string& note_id, const std::string& note_ft,
                                const std::string& proj, const std::string& src_file, bool archived) const;
        static std::string get_parent_dir_path(const std::string& fs_path);
        static std::string get_path_minus_base_dir(const std::string& base_path, const std::string& fs_path);
        //static std::string get_path_minus_root_dir(const std::string& fs_path);
        static bool remove_dir_if_empty(const std::string& fs_path);
        //static bool remove_parent_dir_if_empty(const std::string& fs_path);
        static bool wcopy_file(const std::string& src_file, const std::string& dest_file);
        static bool wcopy_file(const std::filesystem::path& src_file, const std::filesystem::path& dest_file);
        static bool wmove_file(const std::filesystem::path& src_file, const std::filesystem::path& dest_file);
        static bool migrate_note_file(bool to_archive, const std::string& wib_path, const std::string& note_id,
                                      const std::string ftype, const std::string proj);
        const std::string read_in_schema_file(const std::string& schema_f_name, const std::string& schemas_dir);
    };
}
#endif
