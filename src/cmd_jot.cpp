// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_jot.cpp
 * @brief     Handle "jot" subcommand.
 * @details
 *
 * This class handles the dispatcher for the Wibble Jotfile
 * functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "cmd_jot.hpp"
#include "wibblejotter.hpp"

/**
 * @brief Dispatching logic for the "jot" command (Jotfile functionality).
 * @param OPTS package/general configuration options struct
 * @param JOT command line options/parameters supplied via switches for "jot"
 */
void wibble::WibbleCmdJot::handle_jot(pkg_options& OPTS, jot_options& JOT)
{
    WibbleConsole wc;
    // are we filtering any entries (title identifier) in the jotfile
    const std::string title_fltr = (JOT.filter_jot_entries != "UNSET") ? JOT.filter_jot_entries : "";
    WibbleJotter wj{wc, OPTS.paths.jot_dir, OPTS.paths.tmp_dir, title_fltr};

    
    if(JOT.create_jotfile)      { wj.create_jotfile(wc); }
    else if(JOT.delete_jotfile) { wj.remove_jotfile(wc); }
    else
    {
        if     (JOT.add_to_jotfile) { wj.append_new_entry(wc); }
        else if(JOT.rename_jotfile)
        {
            wj.rename_jotfile(wc, JOT.jf_num);
        }
        else if(JOT.dump_jotfile)
        {
            // just output entire raw jotfile to console
            WibbleExecutor wib_e{OPTS};
            wj.dump_jotfile(wc, wib_e, JOT.jf_num);
        }
        // int values are initialised at -1
        else if(JOT.edit_jf_entry >= 0)  { wj.edit_jotfile_entry(wc, JOT.jf_num, JOT.edit_jf_entry);  }
        else if(JOT.del_jf_entry >= 0)   { wj.delete_jotfile_entry(wc, JOT.jf_num, JOT.del_jf_entry); }
        else if(JOT.view_jf_entry >= 0)  { wj.view_jotfile_entry(wc, JOT.jf_num, JOT.view_jf_entry);  }
        else
        {
            wc.console_write_success_green("No operation specified. Listing available jotfiles.");
            wj.list_jotfiles(wc);
        }
    }
} 
