/**
 * @file      wibblecache.cpp
 * @brief     Provides cache cleanup/maintenance facility.
 * @details
 *
 * This class exists to purge old/stale files from the cache and
 * prevent temporary files taking up increasing space.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <chrono>

#include "wibblecache.hpp"
#include "wibbleutility.hpp"

// C++ 17 version
//https://stackoverflow.com/questions/61030383/how-to-convert-stdfilesystemfile-time-type-to-time-t
/**
 * @brief Convert a filesystem file update timestamp to a time_t value.
 * @param tp std::chrono::time_point file update/creation timestamp to convert to time_t
 */
template <typename TP>
std::time_t wibble::WibbleCache::to_time_t(TP tp)
{
    auto sctp = std::chrono::time_point_cast<std::chrono::system_clock::duration>
                (tp - TP::clock::now() + std::chrono::system_clock::now());
    return std::chrono::system_clock::to_time_t(sctp);
}

/**
 * @brief Maintain a clean and non-bloated cache/temporary directory.
 * @param cache_dir String with path to Wibble temporary/cache directory keep clean
 */
void wibble::WibbleCache::clean_cache(const std::string& cache_dir)
{
    std::string cdir = cache_dir;
    wibble::WibbleUtility::remove_trailing_slash(cdir);
    wibble::WibbleUtility::expand_tilde(cdir);
    

    if(std::filesystem::exists(std::filesystem::path(cdir)))
    {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        // remove files older than 48 hours
        const std::time_t old_f = std::chrono::system_clock::to_time_t(now - std::chrono::hours(48));

        bool cleanup = false;
        for (auto const& dir_entry : std::filesystem::directory_iterator{cdir})
        {
            if(dir_entry.is_regular_file())
            {
                const std::string cache_file = dir_entry.path();
                const auto creation_t = std::filesystem::last_write_time(cache_file);
                const auto crt_time_t = to_time_t(creation_t);
                const auto res = crt_time_t - old_f;
                if(res < 0)
                {
                    // safety guard in case user has done something really dumb like putting/saving their own files inside the directory,
                    // setting their home directory as cache dir, etc... All wibble files use this file pattern
                    if(cache_file.find(".wib") != std::string::npos || cache_file.find("wibble-workfile") != std::string::npos)
                    {
                        //std::cout << "OLDER than last 48 hours - removing: " << cache_file << '\n';
                        cleanup = true;
                        //std::cout << "calling std::filesystem::remove(" << cache_file << ")\n";
                        std::filesystem::remove(cache_file);
                    }
                    //else
                    //{
                    //    std::cout << "Ignoring older non-Wibble cache file: " << cache_file << '\n';
                    //}
                }
                //else
                //{

                //    if(cache_file.find(".wib") != std::string::npos || cache_file.find("wibble-workfile") != std::string::npos)
                //    {
                //        std::cout << "Retaining recent Wibble file: " << cache_file << '\n';
                //    }
                //    else
                //    {
                //        std::cout << "Ignoring recent non-Wibble cache file: " << cache_file << '\n';
                //    }
                //}
            }
        }

        // Cleanup is likely only necessary if this is first time they've used Wibble in > 48 hours...
        if(cleanup)
        {
            WibbleConsole().console_write_success_green("Periodic cleanup of cache directory: '" + cdir + "' complete.");
        }
    }
}

