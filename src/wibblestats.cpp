/**
 * @file      wibblestats.cpp
 * @brief     Display sundry statistics about Wibble configuration.
 * @details
 *
 * This class not implemented yet.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "wibblestats.hpp"

void wibble::WibbleStats::display_stats()
{
    std::cout << "display_stats() not implemented yet" << std::endl;
    /* wibble-perl stats page:
        Statistics
       ========================================================
        Main note storage                           1.8M
        Archived note storage                       20K
        Note data storage                           304M
       --------------------------------------------------------
        Main note database size                     56K
        Archived note database size                 4.0K
       --------------------------------------------------------
        Number of notes                             150
        Number of archived notes                    1
        Number of data files                        5502
        Number of journal files                     1
        Number of quicknote files                   1
        Number of template definitions              5
        Number of schema definitions                7
       --------------------------------------------------------
        Note base directory      /home/aren/doc/wibble
        Note main database       /home/aren/doc/wibble/db/main.wib.db
        Note archived database   /home/aren/doc/wibble/db/ark.wib.db
       ========================================================
     */
}
