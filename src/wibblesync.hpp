/**
 * @file      wibblesync.hpp
 * @brief     Provides functionality for the "sync" subcommand (header file).
 * @details
 *
 * This class provides the functionality for the "sync" subcommand.
 * This offers a form of semi-automated file versioning based
 * on manual curation/injection of WIBBLE_VERSION time stamps.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLESYNC_H
#define WIBBLESYNC_H

#include <ctime>
#include <vector>

#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleSync
    {
    private:
        const std::string VER_TOKEN        = "WIBBLE_VERSION";
        const std::string DESC_START_TOKEN = "WIBBLE_DESC_START";
        const std::string DESC_END_TOKEN   = "WIBBLE_DESC_END";
        std::time_t _extract_version_timestamp(const std::string& fn);
        WibbleConsole wc;

    public:
        void sync_directories(const std::string& src_dir, const std::string& dest_dir, const bool ls_only = true, const bool detailed_ls = false, 
                              const bool interactive = true, const int auto_action = 0);
        /**
         * @brief Struct providing all relevant file metadata for versioning.
         */
        struct file_info
        {
            time_t      timestamp;   //!< Detected WIBBLE_VERSION timestamp value, if present
            std::string time_string; //!< Detected WIBBLE_VERSION human readable ISO datetime value, if present
            std::string file_path;   //!< Absolute file path of file
            std::string rel_path;    //!< File path relative to synchronisation root directory
            std::string file_desc;   //!< Detected WIBBLE_DESC description value, if present
            std::string file_name;   //!< Filename only of file
            file_info():
                timestamp{0},
                time_string{""},
                file_path{""},
                rel_path{""},
                file_desc{""},
                file_name{""} {}
        };
    private:
        std::vector<file_info> _parse_directory_tree(std::string path);
        void _print_summary(const file_info& f, const int type, const bool condensed = true);
        void _copy_new_files(std::string base_dir, std::string dest_base, std::vector<file_info>& file_list);
        void _synchronise_files(std::vector<std::pair<file_info, file_info>>& file_list, const bool reverse_pairwise = false);
    };
}
#endif
