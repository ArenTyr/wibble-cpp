/**
 * @file      wibbleexecutor.hpp
 * @brief     General execution/worker class (header file).
 * @details
 *
 * General execution/worker class, that stores the current working environment,
 * various std::system() execution wrappers, and other utility
 * functions for working and operating with Wibble Nodes (WibbleRecord).
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_EXECUTOR_H
#define WIBBLE_EXECUTOR_H

#include "cmd_structs.hpp"
#include "wibblebinlist.hpp"
#include "wibbledbrgdatabase.hpp"
#include "wibbleio.hpp"
#include "wibblemetaschema.hpp"
#include "wibblesearch.hpp"

namespace wibble
{
    class WibbleExecutor
    {
    private:
        // environment: 1) databases, 2) directories, 3) programs
        const std::string archived_db;
        const std::string main_db;

        const std::string backup_location;
        const std::string bookmarks_dir;
        const std::string jot_dir;
        const std::string schemas_dir;
        const std::string scripts_dir;
        const std::string tasks_dir;
        const std::string templates_dir;
        const std::string topics_dir;
        const std::string tmp_dir;
        const std::string wibble_db_dir;
        const std::string wibble_store;

        const std::string cli_editor;
        const std::string dump_tool;
        const std::string fm_tool;
        const std::string gui_editor;
        const std::string pager;
        const std::string view_tool;
        const std::string xdg_open_cmd;
        const std::string render_script;
        const std::string bin_mapping;

        const std::string FILE_DELIM = "___FILE___";
        const std::string EXEC_DELIM = "___EXEC___";

        WibbleConsole wc;
        WibbleIO wio;
        WibbleRGDatabase w_rg;

        std::string _check_for_template(const std::string& ft, const std::string& t_mplate_name);
        void _copy_default_template_if_exists(const std::string& note_id, const std::string& ft, const std::string& proj) const;
        const WibbleRecord _initialise_new_note(const metadata_schema& ms, const std::string& t_mplate_ft) const;
        const WibbleRecord _parse_batch_node_input(const batch_options& BATCH);
        std::vector<WibbleSearch::search_frame> _generate_search_frames_for_keypair_string(WibbleConsole& wc, std::string kp_search);
        void _print_node_records(WibResultList& results, const long& tot_results, const bool extended_view);
        bool _setup_note_path(const std::string& note_id, const std::string& ftype, const std::string& proj) const;
        std::string _splice_exec_cmd(const std::size_t start_offset, const std::string& exec_template, const std::string& placeholder, const std::string& sub_val) const;
        bool _archive_note(const WibbleRecord& w,  const std::string& wibble_store, const std::string& main_db_path, const std::string& ark_db_path) const;
        bool _unarchive_note(const WibbleRecord& w,  const std::string& wibble_store, const std::string& main_db_path, const std::string& ark_db_path) const;


    public:
        /**
         * @brief Constructor that takes in the current environment struct (pkg_options).
         */
        WibbleExecutor(pkg_options CLI_ENV, const bool silence = false) :
            archived_db{CLI_ENV.files.archived_db},
            main_db{CLI_ENV.files.main_db},

            backup_location{CLI_ENV.paths.backup_location},
            bookmarks_dir{CLI_ENV.paths.bookmarks_dir},
            jot_dir{CLI_ENV.paths.jot_dir},
            schemas_dir{CLI_ENV.paths.schemas_dir},
            scripts_dir{CLI_ENV.paths.scripts_dir},
            tasks_dir{CLI_ENV.paths.tasks_dir},
            templates_dir{CLI_ENV.paths.templates_dir},
            topics_dir{CLI_ENV.paths.topics_dir},
            tmp_dir{CLI_ENV.paths.tmp_dir},
            wibble_db_dir{CLI_ENV.paths.wibble_db_dir},
            wibble_store{CLI_ENV.paths.wibble_store},

            cli_editor{CLI_ENV.exec.cli_editor},
            dump_tool{CLI_ENV.exec.dump_tool},
            fm_tool{CLI_ENV.exec.fm_tool},
            gui_editor{CLI_ENV.exec.gui_editor},
            pager{CLI_ENV.exec.pager},
            view_tool{CLI_ENV.exec.view_tool},
            xdg_open_cmd{CLI_ENV.exec.xdg_open_cmd},
            render_script{wibble_store + "/render.sh"},
            bin_mapping{CLI_ENV.exec.bin_mapping},
            w_rg{WibBinList().gen_bin_list(CLI_ENV.exec.bin_mapping, CLI_ENV.paths.templates_dir, silence)}

            {};

        std::string get_archived_db() const;
        std::string get_main_db() const;

        std::string get_backup_location() const;
        std::string get_bookmarks_dir() const;
        std::string get_jot_dir() const;
        std::string get_scripts_dir() const;
        std::string get_tasks_dir() const;
        std::string get_templates_dir() const;
        std::string get_topics_dir() const;
        std::string get_tmp_dir() const;
        std::string get_wibble_store() const;

        std::string get_cli_editor() const;
        std::string get_dump_tool() const;
        std::string get_fm_tool() const;
        std::string get_gui_editor() const;
        std::string get_pager() const;
        std::string get_view_tool() const;
        std::string get_xdg_open_cmd() const;
        std::string get_bin_mapping() const;

        // FIXME: some parameters could be rationalised by making better use of class data from constructor
        std::string _build_note_path(const std::string& note_id, const std::string& ftype, const std::string& proj,
                                     const bool& archived, const bool& note_not_data, const std::string& override_path = "") const;
        std::string generate_dd_list(WibbleRecord& w, const bool& archived, const std::string WORKFILE, const std::string& filter = "") const;
        bool create_node_passthrough(WibbleRecord& w, const bool archive_db, const bool update);
        bool create_schema_interactive();
        bool create_template_interactive(const bool create_default);
        bool export_node(const WibbleRecord& w, const std::string& export_path, const bool archived);
        bool op_add_linked_file_to_note(WibbleRecord& w, const std::string& db_path, const std::string& WORKFILE, bool archived, std::string& f_name);
        bool op_add_data_to_note(WibbleRecord& w, const std::string& db_path, const std::string& WORKFILE, bool archived, std::string& f_name, bool copy_not_move, const bool bypass_check = false);
        bool op_create_node_batch(const batch_options& BATCH);
        bool op_create_note_interactive(const bool gui_edit);
        bool op_create_note_using_schema_file(const std::string f_name, const std::string& t_mplate_name, const std::string& from_file, const bool gui_edit);
        bool op_create_note_from_template_file(const std::string f_name, const bool gui_edit);
        bool op_create_note_from_file(const std::string f_name, bool open_afterwards, const std::string& ft = "", bool gui_edit = false);
        bool op_remove_node(const WibbleRecord& w, const bool archived, const std::string& wib_db, const std::string tmp_dir);
        bool archive_note_confirm(const WibbleRecord& w, const bool restore_from_archive, const std::string& wibble_store, const std::string& main_db_path, const std::string& ark_db_path) const;
        bool note_open_with_editor(const std::string& note_id, const std::string& ftype, const std::string& proj, const bool gui_edit, const bool archived);
        bool note_open_linked_file(const WibbleRecord&w, const bool archived);
        bool note_dd_open_individual_file(WibbleRecord& w, const bool archived, const std::string& filter);
        bool note_dd_ls_view_dump(WibbleRecord& w, const bool archived, const int cmd, std::string dir_cmd = "") const;
        bool note_pipe_through_dump_tool(const WibbleRecord&, const bool archived);
        bool note_pipe_through_view_tool(const WibbleRecord& record, const bool archived, const bool render_node = false);
        bool note_exec_cmd(const WibbleRecord& record, const std::string& exec_template, const bool archived) const;
        bool note_exec_arbitrary_cmd(const WibbleRecord& record, const std::string& exec_template, const std::string& exec_cmd, const bool archived) const;

        bool run_exec_expansion(const std::string& exec_template, const std::string note_path) const;
        bool op_update_existing_record(const WibbleRecord& w, const bool use_archive_db) const;
        WibResultList range_select_record_selection(WibResultList& results, const bool extended_view);
        long select_valid_record_or_exit(WibResultList& results, const bool extended_view);
        void _set_template_ft_and_name(const std::string& f_name, std::string& ft, std::string& t_mplate);
        //void menu_search_set_filters(const std::string& wib_db_path, search_filters& SEARCH);
        WibResultList perform_search(search_filters& SEARCH, const std::string& override_db = "");
        WibbleRecord get_record_from_list(WibResultList& list, const unsigned long response);
        void display_configuration();
        long get_tot_results(WibResultList& results);
    };
}
#endif
