/**
 * @file      wibblesearch.cpp
 * @brief     Build and execute a "search" pipeline for retreiving database results.
 * @details
 *
 * This class is responsible for building up a complete search query/
 * search chain. In case of searching multiple fields, this means multiple
 * iterations/"frames"; since this operates on a very rapidly/drastically
 * reducing temporary file each time, in practice search multiple files
 * is extremely fast. Searching logic implies a logical "&&"/AND operation;
 * i.e. the venn diagram representing the set of records that satisfy the
 * criteria input in all active search fields.
 *
 * More sophisticated logical filtering, is, by design, not offered,
 * since in practice it has little day-to-day value, and would considerably
 * increase the complexity of the searching logic.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "wibblesearch.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Add another search "frame", i.e. searching iteration by particular field.
 * @param search_frame Struct containing the search expression/settings, and target field
 */
bool wibble::WibbleSearch::add_search_frame(search_frame search_iteration)
{
    search_queue.push(search_iteration);
    return true;
}

/**
 * @brief Run through the pipeline of search "frames", repeatedly narrowing result set.
 */
wibble::WibResultList wibble::WibbleSearch::execute_search_pipeline()
{
    WibResultList results;
    bool first_iteration = true;
    int iteration = 0;
    while(search_queue.size() > 0)
    {
        if(! search_queue.empty())
        {
            //std::cerr << "DEBUG: search field is: " << search_queue.front().field << std::endl;
            //std::cerr << "DEBUG: search query is: " << search_queue.front().query << std::endl;
            wc.console_write_green(ARW + "[" + std::to_string(iteration + 1) + "] "); 
            query->active_search_field = search_queue.front().field;
            query->set_query_expression_for_field(search_queue.front().field, search_queue.front().query);
            query->set_exact_flag(search_queue.front().exact_search);
            query->set_fixed_flag(search_queue.front().fixed_string);

            search_queue.pop();
            
            if(first_iteration)
            {
                //std::cerr << "DEBUG: on first iteration" << std::endl;
                // results are now in WORKFILE
                results = w_rg.get_wibble_records(query, DB_FILE, WORKFILE);
                first_iteration = false;
                //std::cerr << "DEBUG: cat of WORKFILE: " << std::endl;
                //system(("cat " + WORKFILE).c_str());
            }
            else
            {
                ++iteration;
                //std::cerr << "DEBUG: on subsequent iteration: " << iteration << std::endl;
                // alternate between two temporary files using mod 2 to avoid file handle/locking issues
                if(iteration % 2 == 1) { results = w_rg.get_wibble_records(query, WORKFILE, WORKFILE2); } 
                else                   { results = w_rg.get_wibble_records(query, WORKFILE2, WORKFILE); } 
                //std::cerr << "WORKFILE1:" << '\n';
                //system(("cat " + WORKFILE).c_str());
                //std::cerr << "WORKFILE2:" << '\n';
                //system(("cat " + WORKFILE2).c_str());
            }
        }
    }

    return results;
}
