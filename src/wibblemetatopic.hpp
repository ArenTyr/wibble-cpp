/**
 * @file      wibblemetatopic.hpp
 * @brief     Definition of a Topic definition file/data structer (header file).
 * @details
 *
 * Provides a struct definition for a "Topic".
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_META_TOPIC_H
#define WIBBLE_META_TOPIC_H
#include <string>

namespace wibble
{
    /**
     * @brief Defines a "Topic"; all of the core metadata.
     * 
     * This struct can also provide/handle storing a given Topic entry
     * for all-in-one exchange with other methods/classes, saving
     * having to pass two stucts when the full information pertaining to
     * a Topic entry is needed.
     */
    struct topic_def
    {
        std::string name;     //!< the name of the Topic
        std::string id;       //!< the canonical identifier of the Topic
        std::string title;    //!< the title/short description of Topic
        std::string category; //!< the category/grouping of Topic
        std::string type;     //!< the filetype/extension of the Topic
        std::string desc;     //!< the long multiline description of the Topic
        std::string status;   //!< the status (active or hibernated) of the Topic

        // fields below setup after insert/creation
        std::string gen_YMD_date;  //!< generated ISO YYYY-MM-DD for a Topic entry
        std::string gen_rfc_date;  //!< generated RFC format date for a Topic entry
        std::string gen_timestamp; //!< generated unique timestamp for a Topic entry
        std::string gen_filename;  //!< generated unique filename for a Topic entry
        std::string entry_title;   //!< given title for a Topic entry
        std::string entry_year;    //!< YYYY format year of Topic entry
        std::string entry_fn;      //!< full filename/path to Topic entry file
        std::string uuid_stub;     //!< the UUID stub as part of the Topic entry ID
        // field below used for bookmark facility
        std::string bk_filename;   //!< generated ISO YYYY-MM-DD for a Topic entry
    };
}
#endif
