/**
 * @file      wibbledispatch.cpp
 * @brief     Main CLI routing/dispatching class.
 * @details
 *
 * This class provides all of the fundamental command-line argument/
 * subcommand processing/dispatching, and serves as the effective
 * entry point to all of the Wibble functionality.
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_archive.hpp"
#include "cmd_autofile.hpp"
#include "cmd_backup.hpp"
#include "cmd_batch.hpp"
#include "cmd_bookmark.hpp"
#include "cmd_create.hpp"
#include "cmd_daily.hpp"
#include "cmd_data.hpp"
#include "cmd_delete.hpp"
#include "cmd_drawer.hpp"
#include "cmd_dump.hpp"
#include "cmd_edit.hpp"
#include "cmd_exec.hpp"
#include "cmd_init.hpp"
#include "cmd_janitor.hpp"
#include "cmd_jot.hpp"
#include "cmd_meta.hpp"
#include "cmd_sync.hpp"
#include "cmd_task.hpp"
#include "cmd_topic.hpp"
#include "cmd_transfer.hpp"
#include "cmd_view.hpp"
#include "wibblebinlist.hpp"
#include "wibblecache.hpp"
#include "wibbledispatch.hpp"
#include "wibbleexit.hpp"
#include "wibbleutility.hpp"
#include "wibbleversion.hpp"
#include "wibblesymbols.hpp"

using wibble::cmd_files;
using wibble::cmd_paths;
using wibble::cmd_exec;
using wibble::pkg_options;

using wibble::archive_options;
using wibble::backup_options;
using wibble::batch_options;
using wibble::bookmark_options;
using wibble::config_env;
using wibble::create_options;
using wibble::data_options;
using wibble::daily_options;
using wibble::edit_options;
using wibble::exec_options;
using wibble::init_options;
using wibble::jot_options;
using wibble::metadata_options;
using wibble::sync_options;
using wibble::task_options;
using wibble::topic_options;
using wibble::transfer_options;
using wibble::view_options;

using wibble::search_filters;

namespace wibble
{
    // setup/initialise all of the command line structures
    /**
     * @brief Struct variable for storing the core database file paths.
     */
    cmd_files CMD_FILES = { "UNSET", "UNSET", "UNSET" };
    
    /**
     * @brief Struct variable for storing the core Wibble file paths.
     */
    cmd_paths CMD_PATHS = { "UNSET", "UNSET", "UNSET",
                            "UNSET", "UNSET", "UNSET",
                            "UNSET", "UNSET", "UNSET",
                            "UNSET", "UNSET", "UNSET",
                            "UNSET" };
    
    /**
     * @brief Struct variable for storing the user configured execution programs/choices.
     */
    cmd_exec CMD_EXEC = { "UNSET", "UNSET", "UNSET",
                          "UNSET", "UNSET", "UNSET",
                          "UNSET", "UNSET" };
    
    /**
     * @brief Struct variable for storing all of the key configuration settings.
     */
    config_env CONFIG { false, false, false, false, false,
                        false, false, false, false, false,
                        false, false, false, false, false,
                        false, false, false, false, false,
                        false, false, false };
    
    /**
     * @brief Parent struct variable to package paths + execution + config together.
     */
    pkg_options PKG_OPTS = { CMD_FILES, CMD_PATHS, CMD_EXEC };
    
    /**
     * @brief Struct variable for storing set 'autofile' subcommand command line switches/arguments.
     */
    autofiler_options AUTOFILE_OPT = { "UNSET", "UNSET", "UNSET", "UNSET", "UNSET", "", "",
                                       false, false, false, false, false };

    /**
     * @brief Struct variable for storing set 'archive' subcommand command line switches/arguments.
     */
    archive_options ARCHIVE_OPT = { false };
    
    /**
     * @brief Struct variable for storing set 'bookmark' subcommand command line switches/arguments.
     */
    bookmark_options BOOK_OPT = { false, false, false, false, false,
                                  false, false, false, false, false,
                                  false, false, false, false, false,
                                  false, false, false, false, false,
                                  false, false, false, false, false,
                                  false, false, false, false, false,
                                  false, false, false, false, NULL_INT_VAL,
                                  NULL_INT_VAL, "UNSET", false };
    
    /**
     * @brief Struct variable for storing set 'batch' subcommand command line switches/arguments.
     */
    batch_options BATCH_OPT = { false, false, false, false,
                                false, false, false, false,
                                false, false, false, false, false,
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET", "UNSET", NULL_INT_VAL, "UNSET",
                                "", "", "UNSET" };
    
    /**
     * @brief Struct variable for storing set 'backup' subcommand command line switches/arguments.
     */
    backup_options BACKUP_OPT = { false, false, false, false,
                                  false, false, false };
    
    /**
     * @brief Struct variable for storing set 'create' subcommand command line switches/arguments.
     */
    create_options CREATE_OPT = { "UNSET", "UNSET", "UNSET",
                                  false, false, false, false, false };
    
    /**
     * @brief Struct variable for storing set 'daily' subcommand command line switches/arguments.
     */
    daily_options DAILY_OPT = { false, false, false, false, "UNSET" };
    
    /**
     * @brief Struct variable for storing set 'daily' subcommand command line switches/arguments.
     */
    drawer_options DRAW_OPT = { false, false, false, false, "UNSET", false, false, -1, false, false, false, false };
    
    /**
     * @brief Struct variable for storing set 'data' subcommand command line switches/arguments.
     */
    data_options DATA_OPT = { "UNSET", "UNSET", "UNSET",
                              "UNSET", false, false,
                              false, false, false,
                              false, false };
    
    /**
     * @brief Struct variable for storing set 'edit' subcommand command line switches/arguments.
     */
    edit_options EDIT_OPT = { false };
    
    /**
     * @brief Struct variable for storing set 'exec' subcommand command line switches/arguments.
     */
    exec_options EXEC_OPT = { false, "UNSET", "UNSET" };
    
    /**
     * @brief Struct variable for storing set 'init' subcommand command line switches/arguments.
     */
    init_options INIT_OPT = { "UNSET" };
    
    /**
     * @brief Struct variable for storing set 'jot' subcommand command line switches/arguments.
     */
    janitor_options JANITOR_OPT { {}, false, false, false, false, false, false };

     /**
     * @brief Struct variable for storing set 'jot' subcommand command line switches/arguments.
     */
    jot_options JOT_OPT { false, false, false,
                          0, false, 0,
                          -1, -1, -1,
                          false, "UNSET", "UNSET",
                          "UNSET" };
    
    /**
     * @brief Struct variable for storing set 'metadata' subcommand command line switches/arguments.
     */
    metadata_options META_OPT = { false, false, false,
                                  false, false, false,
                                  false, false, false,
                                  false, false, false,
                                  false, false, false,
                                  false, false, false,
                                  false, false, false,
                                  false, false, false,
                                  false, false };
    
    // note: do not use "UNSET" for SYNC_OPT
    /**
     * @brief Struct variable for storing set 'sync' subcommand command line switches/arguments.
     */
    sync_options SYNC_OPT = { "", "", false, false, false, false, -1 };

    /**
     * @brief Struct variable for storing set 'task' subcommand command line switches/arguments.
     */
    task_options TASK_OPT = { "UNSET", "UNSET", false, false, "UNSET", false, false, false, "UNSET", false };
    
    /**
     * @brief Struct variable for storing set 'topic' subcommand command line switches/arguments.
     */
    topic_options TOPIC_OPT = { false,   false,   false,
                                false,   false,   false,
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", false,   "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", "UNSET", "UNSET",
                                "UNSET", false };
    
    /**
     * @brief Struct variable for storing set search filters for processing a WibbleRecord set/WibResultList (Node results).
     */
    search_filters SEARCH_FILTERS; 

    /**
     * @brief Struct variable for storing set 'transfer' subcommand command line switches/arguments
     */
    transfer_options TRANS_OPT = { false, "UNSET", "UNSET", false, false, false, false, false, "UNSET" }; 

    /**
     * @brief Struct variable for storing set 'view' subcommand command line switches/arguments
     */
    view_options VIEW_OPT = { false };
}

/**
 * @brief Ensure necessary configuration directory exists if not already present.
 * @param wc WibbleConsole utility/console object.
 * @param fs_path String containing filename path to ensure exists/create.
 */
bool wibble::WibbleDispatch::_config_create_dir_if_needed(WibbleConsole& wc, const std::string& fs_path)
{
    if(! std::filesystem::exists(std::filesystem::path(fs_path)))
    {
        wc.console_write_red(ARW + "Core directory " + fs_path + " does not exist, attempting to setup.\n");
        bool created = wibble::WibbleIO::create_path(fs_path);
        if(created) 
        { wc.console_write_success_green("Core directory " + fs_path + " successfully created."); return true; }
        else
        { wc.console_write_error_red("Error creating directory " + fs_path + ". Check configuration/permissions."); return false; }
    }
    else 
    {
        return true;
    }
}

/**
 * @brief Ensure that no "~" remains in a filename path, replaced with expanded environment value.
 * @param PKG Containing parent struct that contains all configuration values.
 */
void wibble::WibbleDispatch::_expand_all_settings(pkg_options& PKG)
{
    // ensure tilde is expanded in all paths
    // 1. cmd_exec
    wibble::WibbleUtility::expand_tilde(PKG.exec.cli_editor);
    wibble::WibbleUtility::expand_tilde(PKG.exec.dump_tool);
    wibble::WibbleUtility::expand_tilde(PKG.exec.fm_tool);
    wibble::WibbleUtility::expand_tilde(PKG.exec.gui_editor);
    wibble::WibbleUtility::expand_tilde(PKG.exec.pager);
    wibble::WibbleUtility::expand_tilde(PKG.exec.view_tool);
    wibble::WibbleUtility::expand_tilde(PKG.exec.xdg_open_cmd);

    //2. cmd_files
    wibble::WibbleUtility::expand_tilde(PKG.files.archived_db);
    wibble::WibbleUtility::expand_tilde(PKG.files.main_db);

    //3. cmd_paths
    wibble::WibbleUtility::expand_tilde(PKG.paths.autofile_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.backup_location);
    wibble::WibbleUtility::expand_tilde(PKG.paths.bookmarks_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.jot_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.schemas_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.scripts_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.tasks_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.templates_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.topics_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.tmp_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.wibble_db_dir);
    wibble::WibbleUtility::expand_tilde(PKG.paths.wibble_store);
}

/**
 * @brief Helper function to guide user to a valid binary mapping configuration string
 * @param wc WibbleConsole general console utility object
 */
void wibble::WibbleDispatch::_gen_bin_conf(WibbleConsole& wc)
{

    wc.console_write_header("BINARY MAP");
    wc.console_write_yellow("The prompts below will help you generate a valid binary mapping\n"); 
    wc.console_write_yellow("configuration key that you can paste into your configuration file.\n\n"); 
    wc.console_write_yellow("1. To FINISH adding mappings, simply enter a "); wc.console_write_yellow_bold("blank ");
    wc.console_write_yellow("input for the extension.\n");
    wc.console_write_yellow("2. The 'view' command (input [3/4]) is optional and can be left blank.\n\n");

    struct bin_entry
    {
        std::string ext;
        std::string main_cmd;
        std::string view_cmd;
        bool bckgd_main;
        bool bckgd_view;
    };

    std::set<std::string> exts;
    std::vector<bin_entry> map_list;
    
    bool finished = false;
    do
    {
        wc.console_write_hz_divider();
        // file extension
        std::string ext   = wc.console_write_prompt("[1/5] Input binary file/type extension                            \n> ", "", "");
        wc.trim(ext);
        if(ext == "") { finished = true; continue; }

        // main program command
        std::string m_cmd = wc.console_write_prompt("[2/5] Input 'create/edit/open' command/program/script             \n> ", "", "");
        wc.trim(m_cmd);

        // main program backgrounded?
        std::string bckgd_m = wc.console_write_prompt("[3/5] Should 'open' execution be backgrounded/is it a GUI program? (y/n) \n> ", "", "");
        while(bckgd_m != "y" && bckgd_m != "Y" && bckgd_m != "n" && bckgd_m != "N")
        { bckgd_m = wc.console_write_prompt("[3/5] Please input either 'y' or 'n' > ", "", ""); wc.trim(bckgd_m); }

        // view program command
        std::string v_cmd = wc.console_write_prompt("[4/5] (Optional) Input 'preview/view' command/program/script      \n> ", "", "");
        wc.trim(v_cmd);
        
        std::string bckgd_v = "";
        // only ask about backgrounding view program if they specified one
        if(v_cmd != "")
        {
            // view program backgrounded?
            bckgd_v = wc.console_write_prompt("[5/5] Should 'view' execution be backgrounded/is it a GUI program? (y/n) \n> ", "", "");
            while(bckgd_v != "y" && bckgd_v != "Y" && bckgd_v != "n" && bckgd_v != "N")
            { bckgd_v = wc.console_write_prompt("[5/5] Please input either 'y' or 'n' > ", "", ""); wc.trim(bckgd_v); }
        }

        if(ext != "" && exts.count(ext) == 0)
        {
            bin_entry mapping;
            exts.insert(ext);
            mapping.ext = ext;
            mapping.main_cmd = m_cmd;
            if(v_cmd != "") { mapping.view_cmd = v_cmd; }              else { mapping.view_cmd = "___NONE___";     }
            if(bckgd_m == "Y" || bckgd_m == "y") { mapping.bckgd_main = true; } else { mapping.bckgd_main = false; }
            if(bckgd_v == "Y" || bckgd_v == "y") { mapping.bckgd_view = true; } else { mapping.bckgd_view = false; }
            map_list.push_back(mapping);

            wc.console_write_hz_divider();
            wc.console_write_green(ARW + "Added extension '" + ext + "'\n");
        }
        else
        {
            if(ext != "") { wc.console_write_red(ARW + "Extension '" + ext + "' has already been defined. Skipping.\n"); }
        }
    }
    while(! finished);

    wc.console_write_hz_divider();
    if(exts.size() > 0)
    {
        wc.console_write_green("\n- Complete.\n");
        wc.console_write_yellow("Paste/append the following key value into your configuration file:\n\n");

        std::string bin_map_ent = "binary_mapping = \"";
        for(auto const& entry: map_list)
        {

            bin_map_ent.append(entry.ext + ":" + entry.main_cmd);
            if(entry.bckgd_main) { bin_map_ent.append(" ___FILE___&");}
            if(entry.view_cmd != "___NONE___")
            {
                bin_map_ent.append("%" + entry.view_cmd);
                if(entry.bckgd_view) { bin_map_ent.append(" ___FILE___&");}
            }
            bin_map_ent.append(", ");
        }
        bin_map_ent = bin_map_ent.substr(0, (bin_map_ent.length() - 2));
        bin_map_ent.append("\"");
        
        wc.console_write_hz_heavy_divider();
        wc.console_write(bin_map_ent);
        wc.console_write_hz_heavy_divider();
    }

}


/**
 * @brief Ensure that all necessary configuration directories exist.
 * @param wc WibbleConsole utility/console object.
 * @param CMD_PATHS Struct containing all configuration subdirectory paths.
 */
bool wibble::WibbleDispatch::_prepare_directories(WibbleConsole& wc, cmd_paths& CMD_PATHS)
{
    // ensure tilde is expanded
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.autofile_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.backup_location);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.bookmarks_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.drawers_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.jot_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.schemas_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.scripts_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.tasks_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.templates_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.topics_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.tmp_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.wibble_db_dir);
    wibble::WibbleUtility::expand_tilde(CMD_PATHS.wibble_store);


    // do/create filesystem stuff IF "valid_config == true"
    // return fail state if unable to create directory
    
    bool valid_config = true;
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.autofile_dir))                      { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.backup_location))                   { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.bookmarks_dir))                     { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.drawers_dir))                       { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.jot_dir))                           { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.schemas_dir))                       { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.scripts_dir))                       { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.tasks_dir))                         { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.templates_dir + "/bin_default"))    { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.topics_dir))                        { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.tmp_dir))                           { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.wibble_db_dir))                     { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.wibble_store))                      { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.wibble_store + "/notes"))           { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.wibble_store + "/archived_notes"))  { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.wibble_store + "/note_data"))       { valid_config = false; }
    if(! _config_create_dir_if_needed(wc, CMD_PATHS.wibble_store + "/janitor"))         { valid_config = false; }
    
    return valid_config;
}

/**
 * @brief Ensure that configuration is sane/environment is correctly operation before proceeding any further.
 */
bool wibble::WibbleDispatch::validate_configuration()
{
    WibbleConsole wc;

    bool valid_config = true;

    // guard -- ensure that no option is empty/has only whitespace
    // is possible to (stupidly) enter blank values in toml configuration file
    // 1/3. core files
    if(! wibble::WibbleUtility::check_string_has_content(CMD_FILES.archived_db))
    { CMD_FILES.archived_db = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_FILES.main_db))
    { CMD_FILES.main_db = "UNSET"; }

    // 2/3. core paths
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.autofile_dir))
    { CMD_PATHS.autofile_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.backup_location))
    { CMD_PATHS.backup_location = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.bookmarks_dir))
    { CMD_PATHS.bookmarks_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.drawers_dir))
    { CMD_PATHS.drawers_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.jot_dir))
    { CMD_PATHS.jot_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.schemas_dir))
    { CMD_PATHS.schemas_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.scripts_dir))
    { CMD_PATHS.scripts_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.tasks_dir))
    { CMD_PATHS.tasks_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.templates_dir))
    { CMD_PATHS.templates_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.tmp_dir))
    { CMD_PATHS.tmp_dir = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_PATHS.wibble_store))
    { CMD_PATHS.wibble_store = "UNSET"; }

    // 3/3. core exec commands
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.cli_editor))
    { CMD_EXEC.cli_editor = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.dump_tool))
    { CMD_EXEC.dump_tool = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.fm_tool))
    { CMD_EXEC.fm_tool = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.gui_editor))
    { CMD_EXEC.gui_editor = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.pager))
    { CMD_EXEC.pager = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.view_tool))
    { CMD_EXEC.view_tool = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.xdg_open_cmd))
    { CMD_EXEC.xdg_open_cmd = "UNSET"; }
    if(! wibble::WibbleUtility::check_string_has_content(CMD_EXEC.bin_mapping))
    { CMD_EXEC.bin_mapping = "UNSET"; }
 
    // group 1
    bool archive_db_set    = true;
    bool main_db_set       = true;
   
    if(CMD_FILES.archived_db             == "UNSET") { archive_db_set    = false; valid_config = false; } // #1  
    if(CMD_FILES.main_db                 == "UNSET") { main_db_set       = false; valid_config = false; } // #2 
    
    // group 2
    bool autofile_dir_set    = true;
    bool backup_location_set = true;
    bool bookmarks_dir_set   = true;
    bool drawers_dir_set     = true;
    bool jot_dir_set         = true;
    bool schemas_dir_set     = true;
    bool scripts_dir_set     = true;
    bool tasks_dir_set       = true;
    bool topics_dir_set      = true;
    bool templates_dir_set   = true;
    bool tmp_dir_set         = true;
    bool wibble_db_dir_set   = true;
    bool wibble_store_set    = true;
 
    if(CMD_PATHS.autofile_dir    == "UNSET") { autofile_dir_set    = false; valid_config = false; } // #3 
    if(CMD_PATHS.backup_location == "UNSET") { backup_location_set = false; valid_config = false; } // #4 
    if(CMD_PATHS.bookmarks_dir   == "UNSET") { bookmarks_dir_set   = false; valid_config = false; } // #5
    if(CMD_PATHS.drawers_dir     == "UNSET") { drawers_dir_set     = false; valid_config = false; } // #6
    if(CMD_PATHS.jot_dir         == "UNSET") { jot_dir_set         = false; valid_config = false; } // #7
    if(CMD_PATHS.schemas_dir     == "UNSET") { schemas_dir_set     = false; valid_config = false; } // #8
    if(CMD_PATHS.scripts_dir     == "UNSET") { scripts_dir_set     = false; valid_config = false; } // #9
    if(CMD_PATHS.tasks_dir       == "UNSET") { tasks_dir_set       = false; valid_config = false; } // #10
    if(CMD_PATHS.templates_dir   == "UNSET") { templates_dir_set   = false; valid_config = false; } // #11
    if(CMD_PATHS.topics_dir      == "UNSET") { topics_dir_set      = false; valid_config = false; } // #12
    if(CMD_PATHS.tmp_dir         == "UNSET") { tmp_dir_set         = false; valid_config = false; } // #13
    if(CMD_PATHS.wibble_db_dir   == "UNSET") { wibble_db_dir_set   = false; valid_config = false; } // #14
    if(CMD_PATHS.wibble_store    == "UNSET") { wibble_store_set    = false; valid_config = false; } // #15

    // group 3
    bool cli_editor_set    = true;
    bool dump_tool_set     = true;
    bool fm_tool_set       = true;
    bool gui_editor_set    = true;
    bool pager_set         = true;
    bool view_tool_set     = true;
    bool xdg_open_cmd_set  = true;
    bool bin_map_set       = true;

    if(CMD_EXEC.cli_editor   == "UNSET") { cli_editor_set   = false; valid_config = false; } // #16 
    if(CMD_EXEC.dump_tool    == "UNSET") { dump_tool_set    = false; valid_config = false; } // #17
    if(CMD_EXEC.fm_tool      == "UNSET") { fm_tool_set      = false; valid_config = false; } // #17
    if(CMD_EXEC.gui_editor   == "UNSET") { gui_editor_set   = false; valid_config = false; } // #19
    if(CMD_EXEC.pager        == "UNSET") { pager_set        = false; valid_config = false; } // #20
    if(CMD_EXEC.view_tool    == "UNSET") { view_tool_set    = false; valid_config = false; } // #21
    if(CMD_EXEC.xdg_open_cmd == "UNSET") { xdg_open_cmd_set = false; valid_config = false; } // #22
    if(CMD_EXEC.bin_mapping  == "UNSET") { bin_map_set      = false;                       } // #23

    if(VERBOSEMODE || ! valid_config)
    {
        // 1. ========== Core files ==========
        wc.console_write_hz_divider();
        wc.console_write_header("CORE ");

        // #1
        if(archive_db_set)    { wc.console_write_green("Archived database file     : " + CMD_FILES.archived_db);             std::cout << '\n'; }
        else                  {   wc.console_write_red("Archived database file     : UNSET");                                std::cout << '\n'; } 

        // #2
        if(main_db_set)       { wc.console_write_green("Main database file         : " + CMD_FILES.main_db);                 std::cout << '\n'; }
        else                  {   wc.console_write_red("Main database file         : UNSET");                                std::cout << '\n'; } 

        // 2. ========== Core paths ==========
        wc.console_write_hz_divider();
        wc.console_write_header("DIRS ");

        // #3
        if(autofile_dir_set)    { wc.console_write_green("Autofile directory         : " + CMD_PATHS.autofile_dir);  std::cout << '\n'; }
        else                    {   wc.console_write_red("Autofile directory         : UNSET");                      std::cout << '\n'; } 

        // #4
        if(backup_location_set) { wc.console_write_green("Backup directory           : " + CMD_PATHS.backup_location); std::cout << '\n'; }
        else                    {   wc.console_write_red("Backup directory           : UNSET");                        std::cout << '\n'; } 

        // #5
        if(bookmarks_dir_set)   { wc.console_write_green("Bookmarks directory        : " + CMD_PATHS.bookmarks_dir);   std::cout << '\n'; }
        else                    {   wc.console_write_red("Bookmarks directory        : UNSET");                        std::cout << '\n'; } 

        std::string df_config_dir = "~/.config/wibble"; 
        WibbleUtility::expand_tilde(df_config_dir);
        wc.console_write_green("Core config directory      : " + df_config_dir); std::cout << '\n';
        wc.console_write_green("Janitor config directory   : " + CMD_PATHS.wibble_store + "/janitor"); std::cout << '\n';

        // #6
        if(drawers_dir_set)     { wc.console_write_green("Drawers directory          : " + CMD_PATHS.drawers_dir);     std::cout << '\n'; }
        else                    {   wc.console_write_red("Drawers directory          : UNSET");                        std::cout << '\n'; }
        
        // #7
        if(jot_dir_set)         { wc.console_write_green("Journal directory          : " + CMD_PATHS.jot_dir);         std::cout << '\n'; }
        else                    {   wc.console_write_red("Journal directory          : UNSET");                        std::cout << '\n'; } 


        wc.console_write_green("Nodes directory (data)     : " + CMD_PATHS.wibble_store + "/note_data");      std::cout << '\n';
        wc.console_write_green("Nodes directory (main)     : " + CMD_PATHS.wibble_store + "/notes");          std::cout << '\n';
        wc.console_write_green("Nodes directory (archived) : " + CMD_PATHS.wibble_store + "/archived_notes"); std::cout << '\n';
        
        // #11
        if(schemas_dir_set)     { wc.console_write_green("Schemas directory          : " + CMD_PATHS.schemas_dir);     std::cout << '\n'; }
        else                    {   wc.console_write_red("Schemas directory          : UNSET");                        std::cout << '\n'; } 

        // #12
        if(scripts_dir_set)     { wc.console_write_green("Scripts directory          : " + CMD_PATHS.scripts_dir);     std::cout << '\n'; }
        else                    {   wc.console_write_red("Scripts directory          : UNSET");                        std::cout << '\n'; } 

        // #13
        if(tasks_dir_set)       { wc.console_write_green("Tasks directory            : " + CMD_PATHS.tasks_dir);       std::cout << '\n'; }
        else                    {   wc.console_write_red("Tasks directory            : UNSET");                        std::cout << '\n'; } 

        // #14
        if(templates_dir_set)   { wc.console_write_green("Templates directory        : " + CMD_PATHS.templates_dir);   std::cout << '\n'; }
        else                    {   wc.console_write_red("Templates directory        : UNSET");                        std::cout << '\n'; } 

        // #15
        if(topics_dir_set)      { wc.console_write_green("Topics directory           : " + CMD_PATHS.topics_dir);      std::cout << '\n'; }
        else                    {   wc.console_write_red("Topics directory           : UNSET");                        std::cout << '\n'; } 

        // #16
        if(tmp_dir_set)         { wc.console_write_green("Temporary/working directory: " + CMD_PATHS.tmp_dir);         std::cout << '\n'; }
        else                    {   wc.console_write_red("Temporary/working directory: UNSET");                        std::cout << '\n'; } 

        // #17
        if(wibble_db_dir_set)   { wc.console_write_green("Wibble database directory  : " + CMD_PATHS.wibble_db_dir);   std::cout << '\n'; }
        else                    {   wc.console_write_red("Wibble database directory  : UNSET");                        std::cout << '\n'; } 

        // #18
        if(wibble_store_set)    { wc.console_write_green("Base Wibble directory      : " + CMD_PATHS.wibble_store);    std::cout << '\n'; }
        else                    {   wc.console_write_red("Base Wibble directory      : UNSET");                        std::cout << '\n'; } 

        // 3. ========== Execution tools ==========
        wc.console_write_hz_divider();
        wc.console_write_header("EXEC ");

        // #19
        if(cli_editor_set)   { wc.console_write_green("Command line editor        : " + CMD_EXEC.cli_editor); std::cout << '\n';   }
        else                 {   wc.console_write_red("Command line editor        : UNSET");                  std::cout << '\n';   } 

        // #20
        if(dump_tool_set)    { wc.console_write_green("Dump/cat tool              : " + CMD_EXEC.dump_tool);  std::cout << '\n';   }
        else                 {   wc.console_write_red("Dump/cat tool              : UNSET");                  std::cout << '\n';   } 

        // #21
        if(fm_tool_set)      { wc.console_write_green("File manager               : " + CMD_EXEC.fm_tool);    std::cout << '\n';   }
        else                 {   wc.console_write_red("File manager               : UNSET");                  std::cout << '\n';   } 

        // #22
        if(gui_editor_set)   { wc.console_write_green("GUI editor                 : " + CMD_EXEC.gui_editor); std::cout << '\n';   }
        else                 {   wc.console_write_red("GUI editor                 : UNSET");                  std::cout << '\n';   } 

        // #23
        if(pager_set)        { wc.console_write_green("Pager                      : " + CMD_EXEC.pager);      std::cout << '\n';   }
        else                 {   wc.console_write_red("Pager                      : UNSET");                  std::cout << '\n';   } 

        // #24
        if(view_tool_set)    { wc.console_write_green("View tool                  : " + CMD_EXEC.view_tool);  std::cout << '\n';   }
        else                 {   wc.console_write_red("View tool                  : UNSET");                  std::cout << '\n';   } 

        // #25
        if(xdg_open_cmd_set) { wc.console_write_green("XDG open cmd               : " + CMD_EXEC.xdg_open_cmd); std::cout << '\n'; }
        else                 {   wc.console_write_red("XDG open cmd               : UNSET");                    std::cout << '\n'; } 

        // #26
        if(bin_map_set)      { wc.console_write_green("Binary file mapping        : " + CMD_EXEC.bin_mapping); std::cout << '\n';  }
        else                 {   wc.console_write_red("Binary file mapping        : UNSET");                    std::cout << '\n'; } 

        wc.console_write_hz_divider();
    }

    // exit function if something is not correctly set
    if(! valid_config) { return false; }

    // Nothing is "UNSET", so proceed with checking files/directories
    return _prepare_directories(wc, CMD_PATHS);
}

/**
 * @brief Utility method for checking settings during development.
 */
void wibble::WibbleDispatch::debug_config()
{
    //  --- BEGIN: DEBUG configuration options ---
    std::cout << "\nVERIFYING CONFIGURATION OPTIONS" << std::endl;
    std::cout << "-------------------------------" << std::endl;
    std::cout << "======= CMD_FILES =======" << std::endl;
    std::cout << CMD_FILES.archived_db << std::endl;
    std::cout << CMD_FILES.main_db << std::endl;

    std::cout << "======= CMD_PATHS =======" << std::endl;
    std::cout << CMD_PATHS.autofile_dir << std::endl;
    std::cout << CMD_PATHS.backup_location << std::endl;
    std::cout << CMD_PATHS.bookmarks_dir << std::endl;
    std::cout << CMD_PATHS.drawers_dir << std::endl;
    std::cout << CMD_PATHS.jot_dir << std::endl;
    std::cout << CMD_PATHS.schemas_dir << std::endl;
    std::cout << CMD_PATHS.scripts_dir << std::endl;
    std::cout << CMD_PATHS.tasks_dir << std::endl;
    std::cout << CMD_PATHS.templates_dir << std::endl;
    std::cout << CMD_PATHS.topics_dir << std::endl;
    std::cout << CMD_PATHS.tmp_dir << std::endl;
    std::cout << CMD_PATHS.wibble_db_dir << std::endl;
    std::cout << CMD_PATHS.wibble_store << std::endl;
    
    std::cout << "======= CMD_EXEC =======" << std::endl;
    std::cout << CMD_EXEC.cli_editor << std::endl;
    std::cout << CMD_EXEC.dump_tool << std::endl;
    std::cout << CMD_EXEC.fm_tool << std::endl;
    std::cout << CMD_EXEC.gui_editor << std::endl;
    std::cout << CMD_EXEC.pager << std::endl;
    std::cout << CMD_EXEC.view_tool << std::endl;
    std::cout << CMD_EXEC.xdg_open_cmd << std::endl;
    std::cout << CMD_EXEC.bin_mapping << std::endl;
    //  --- END: DEBUG configuration options ---
}

/**
 * @brief Setup/ensure relevant search switches/command line options are present.
 * @param cmd Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::_configure_generic_search_switches(CLI::App& cmd)
{

    cmd.add_option("--content",        SEARCH_FILTERS.content_srch, "Search full Node content");
    cmd.add_option("-D,--date",        SEARCH_FILTERS.date, "Search by Date field");
    cmd.add_option("-t,--title",       SEARCH_FILTERS.title, "Search by Title field");
    cmd.add_option("-i,--id",          SEARCH_FILTERS.id, "Search by NoteId field");
    cmd.add_option("-d,--description", SEARCH_FILTERS.desc, "Search by Description field");
    cmd.add_option("-y,--type",        SEARCH_FILTERS.type, "Search by Filetype field");
    cmd.add_option("-p,--project",     SEARCH_FILTERS.proj, "Search by Project field");
    cmd.add_option("-a,--tags",        SEARCH_FILTERS.tags, "Search by Tags field");
    cmd.add_option("-c,--class",       SEARCH_FILTERS.cls, "Search by Class field");
    cmd.add_option("-R,--data-dir",    SEARCH_FILTERS.dd, "Search by DataDir field");
    cmd.add_option("-k,--keypairs",    SEARCH_FILTERS.kp, "Search by KeyPairs field");
    cmd.add_option("-s,--custom",      SEARCH_FILTERS.cust, "Search by Custom field");
    cmd.add_option("-l,--linkedids",   SEARCH_FILTERS.lids, "Search by LinkedIds field");

    cmd.add_flag("-E,--exact",         SEARCH_FILTERS.exact, "Switch on exact (non-substring) match mode");
    cmd.add_flag("-@,--fixed-strings", SEARCH_FILTERS.fixed_strings, "Switch on fixed strings (non-regexp) match mode");
    cmd.add_flag("-K,--archive",       SEARCH_FILTERS.archive_db, "Search using archive database instead");
    cmd.add_flag("-X,--extended",      SEARCH_FILTERS.extended_view, "Switch on extended listing mode");
    cmd.add_flag("-1,--everything",    SEARCH_FILTERS.everything, "Bypass all search filters and dump everything");
    cmd.add_flag("--single",           SEARCH_FILTERS.single_only, "Quiet mode/only accept singular match");

    cmd.add_flag("--menu",             SEARCH_FILTERS.menu_search, "Enable interactive menu search");
}

/**
 * @brief Setup 'archive' subcommand switches.
 * @param archive Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_archive_options(CLI::App& archive)
{
    //std::cout << "DEBUG: Setting up options: " << archive.get_name() << std::endl;
    archive.add_flag("--restore", ARCHIVE_OPT.restore_from_archive, "Restore from archive rather than archive note");
    _configure_generic_search_switches(archive);
}

/**
 * @brief Setup 'autofile' subcommand switches.
 * @param autofile Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_autofile_options(CLI::App& autofile)
{
    //std::cout << "DEBUG: Setting up options: " << autofile.get_name() << std::endl;
    autofile.add_flag("-A,--add", AUTOFILE_OPT.add_category, "Add autofile category");
    autofile.add_flag("-E,--edit", AUTOFILE_OPT.edit_categories, "Edit or remove autofile categories");
    autofile.add_option("-C,--copy", AUTOFILE_OPT.copy_data, "Autofile file/directory via COPY")->excludes("--add");
    autofile.add_option("-M,--move", AUTOFILE_OPT.move_data, "Autofile file/directory via MOVE")->excludes("--add","--copy");
    autofile.add_option("-D,--custom-date", AUTOFILE_OPT.custom_date, "Autofile using custom YYYY-MM-DD datestring instead");
    autofile.add_flag("-T,--dry-run", AUTOFILE_OPT.dry_run, "Dry run/only display proposed file operation");
    autofile.add_flag("-N,--no-prompt", AUTOFILE_OPT.no_prompt, "No prompt/confirmation, just perform operation")->excludes("--dry-run");
    autofile.add_flag("-O,--open", AUTOFILE_OPT.open_file_action, "Open/Perform file action")->excludes("--add","--copy","--dry-run");
    autofile.add_option("-F,--filter-filename", AUTOFILE_OPT.filter_fn, "Filter filenames by substring");
    autofile.add_option("-R,--filter-desc", AUTOFILE_OPT.filter_desc, "Filter description by substring");
    autofile.add_option("-G,--set-category", AUTOFILE_OPT.set_category, "Pre-set the autofile category");
    autofile.add_option("-I,--set-desc", AUTOFILE_OPT.set_desc, "Pre-set the autofile description");
}

/**
 * @brief Setup 'backup' subcommand switches.
 * @param backup Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_backup_options(CLI::App& backup)
{
    //std::cout << "DEBUG: Setting up options: " << backup.get_name() << std::endl;
    backup.add_flag("-L,--list",           BACKUP_OPT.list_backup, "List completed backups");
    backup.add_flag("-F,--full-backup",    BACKUP_OPT.full_backup, "Perform full backup (including all attachment data)")
           ->excludes("--list");
    backup.add_flag("-x,--xz-compression", BACKUP_OPT.xz_compression, "Use xz/LZMA compression (default)")
           ->excludes("--list");
    backup.add_flag("-l,--lz-compression", BACKUP_OPT.lz_compression, "Use tarlz multimember compression")
           ->excludes("--list", "--xz-compression");
    backup.add_flag("-g,--gz-compression", BACKUP_OPT.gz_compression, "Use gzip compression")
           ->excludes("--list", "--xz-compression", "--lz-compression");
    backup.add_flag("-7,--7z-compression", BACKUP_OPT._7z_compression, "Create a 7z archive instead of compressed tar archive")
           ->excludes("--list", "--xz-compression", "--lz-compression", "--gz-compression");
    backup.add_flag("-z,--zip-compression", BACKUP_OPT.zip_compression, "Create a zip archive instead of compressed tar archive")
           ->excludes("--list", "--xz-compression", "--lz-compression", "--gz-compression", "--7z-compression");

}

/**
 * @brief Setup 'batch' subcommand switches.
 * @param batch Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_batch_options(CLI::App& batch)
{
    //std::cout << "DEBUG: Setting up options: " << batch.get_name() << std::endl;
    // 1. command/control flags
    batch.add_flag("--create-node",            BATCH_OPT.create_node,                "Create a new Node");
    batch.add_flag("--create-new-topic",       BATCH_OPT.create_new_topic,           "Create a new Topic")
        ->excludes("--create-node");
    batch.add_flag("--create-new-jotfile",     BATCH_OPT.create_new_jotfile,         "Create a new Jotfile")
        ->excludes("--create-node", "--create-new-topic");
    batch.add_flag("--create-jotfile-entry",   BATCH_OPT.create_jotfile_entry,       "Create a new Jotfile entry")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile");
    batch.add_flag("--create-task-item",       BATCH_OPT.create_task_item,           "Create a new Task item")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile", "--create-jotfile-entry");
    batch.add_flag("--create-new-task-group",  BATCH_OPT.create_new_task_group,     "Create a new Task group")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile", "--create-jotfile-entry", "--create-task-item");
    batch.add_flag("--create-topic-entry",     BATCH_OPT.create_topic_entry,         "Create a new Topic entry")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile", "--create-jotfile-entry", "--create-task-item",
                   "--create-new-task-group");
    batch.add_flag("--node-add-data",          BATCH_OPT.node_add_data,              "Add data to an existing Node")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile", "--create-jotfile-entry", "--create-task-item",
                   "--create-new-task-group", "--create-topic-entry");
    batch.add_flag("--task-add-data",         BATCH_OPT.task_add_data,             "Add data to an existing Topic")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile", "--create-jotfile-entry", "--create-task-item",
                   "--create-new-task-group", "--create-topic-entry", "--node-add-data");
    batch.add_flag("--topic-add-data",         BATCH_OPT.topic_add_data,             "Add data to an existing Topic")
        ->excludes("--create-node", "--create-new-topic", "--create-new-jotfile", "--create-jotfile-entry", "--create-task-item",
                   "--create-new-task-group", "--create-topic-entry", "--node-add-data", "--task-add-data");

    batch.add_flag("--data-copy",              BATCH_OPT.data_copy,                  "Add data via COPY operation");
    batch.add_flag("--data-move",              BATCH_OPT.data_move,                  "Add data via MOVE operation");

    // 2. node parameters
    batch.add_flag("--node-archived",          BATCH_OPT.node_archived,             "Search in the archived database (data operation only)");
    batch.add_option("--node-id",              BATCH_OPT.node_id,                   "Specify the Node ID (data operation only)");
    batch.add_option("--node-date",            BATCH_OPT.node_date,                 "Specify the Node Date (data operation only)");
    
    batch.add_option("--node-title",           BATCH_OPT.node_title,                "Specify the Node Title");
    batch.add_option("--node-filetype",        BATCH_OPT.node_filetype,             "Specify the Node Filetype");
    batch.add_option("--node-description",     BATCH_OPT.node_description,          "Specify the Node Description");
    batch.add_option("--node-project",         BATCH_OPT.node_project,              "Specify the Node Project");
    batch.add_option("--node-tags",            BATCH_OPT.node_tags,                 "Specify the Node Tags");
    batch.add_option("--node-class",           BATCH_OPT.node_class,                "Specify the Node Class");
    batch.add_option("--node-keypairs",        BATCH_OPT.node_keypairs,             "Specify the Node KeyPairs");
    batch.add_option("--node-custom",          BATCH_OPT.node_custom,               "Specify the Node Custom");

    // 3. jotfile entry parameters
    batch.add_option("--jotfile-entry-title",  BATCH_OPT.jot_entry_title,         "Specify the Jotfile entry title");
    batch.add_option("--use-jotfile",          BATCH_OPT.use_jotfile,             "Specify the Jotfile to add to");

    // 4. topic entry parameters
    batch.add_option("--topic-entry-title",    BATCH_OPT.topic_entry_title,         "Specify the Topic entry title");
    batch.add_option("--topic-set-date",       BATCH_OPT.topic_entry_custom_date,   "Specify the Topic entry custom date (optional)");
    batch.add_option("--use-topic",            BATCH_OPT.use_topic,                 "Specify the Topic to add to");

    // 5. creating new jotfile parameters
    batch.add_option("--jotfile-category",     BATCH_OPT.jot_category,              "Specify the new Jotfile category");
    batch.add_option("--jotfile-name",         BATCH_OPT.jot_name,                  "Specify the new Jotfile name");

    // 6. creating new topic parameters
    batch.add_option("--topic-category",       BATCH_OPT.topic_category,           "Specify the new Topic category");
    batch.add_option("--topic-title",          BATCH_OPT.topic_title,              "Specify the new Topic title");
    batch.add_option("--topic-filetype",       BATCH_OPT.topic_filetype,           "Specify the new Topic filetype");
    batch.add_option("--topic-name",           BATCH_OPT.topic_name,               "Specify the new Topic name");
    batch.add_option("--topic-description",    BATCH_OPT.topic_description,        "Specify the new Topic description");

    // 7. creating new task parameters
    batch.add_option("--task-id",              BATCH_OPT.task_id,                  "Specify the Task item ID (adding data only)");
    batch.add_option("--task-title",           BATCH_OPT.task_title,               "Specify the Task item title");
    batch.add_option("--task-category",        BATCH_OPT.task_category,            "Specify the Task item category");
    batch.add_option("--task-due-date",        BATCH_OPT.task_due_date,            "Specify the Task item due date");
    batch.add_option("--task-priority",        BATCH_OPT.task_priority,            "Specify the Task item priority");
    batch.add_option("--task-group",           BATCH_OPT.task_group,               "Specify the Task item group");

    // 8. data parameters
    batch.add_option("--content-file",         BATCH_OPT.content_file,             "Specify the path to the file to use for content");
    batch.add_option("--data-file",            BATCH_OPT.data_file,                "Specify the path to the file/directory for data operation");

    // 9. optional log file
    batch.add_option("--log-file",             BATCH_OPT.logfile,                   "Specify the path to an optional logfile to write out");
}

/**
 * @brief Setup 'bookmark' subcommand switches.
 * @param bookmark Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_bookmark_options(CLI::App& bookmark)
{
    //std::cout << "DEBUG: Setting up options: " << bookmark.get_name() << std::endl;
    bookmark.add_flag("-V,--select-all-bm",            BOOK_OPT.select_all,                "Select any item from all bookmark types");

    bookmark.add_flag("-A,--add-new-node-bm",          BOOK_OPT.add_new_node_bm,           "Add a node bookmark");
    bookmark.add_flag("-T,--add-new-topic-bm",         BOOK_OPT.add_new_topic_bm,          "Add a topic entry bookmark");
    bookmark.add_flag("-J,--add-new-jotfile-bm",       BOOK_OPT.add_new_jotfile_bm,        "Add a jotfile bookmark");
    bookmark.add_flag("-K,--add-new-task-bm",          BOOK_OPT.add_new_task_bm,           "Add a task bookmark");
    bookmark.add_flag("-W,--add-new-wibble-cmd-bm",    BOOK_OPT.add_new_wibble_cmd_bm,     "Add a Wibble command bookmark");
    bookmark.add_flag("-X,--add-new-general-cmd-bm",   BOOK_OPT.add_new_general_cmd_bm,    "Add a general command bookmark");
                 
    bookmark.add_flag("-1,--select-node-bm",           BOOK_OPT.select_node_bm,            "Select a node bookmark");
    bookmark.add_flag("-2,--select-topic-bm",          BOOK_OPT.select_topic_bm,           "Select a topic bookmark");
    bookmark.add_flag("-3,--select-jotfile-bm",        BOOK_OPT.select_jotfile_bm,         "Select a jotfile bookmark");
    bookmark.add_flag("-4,--select-task-bm",           BOOK_OPT.select_task_bm,            "Select a task bookmark");
    bookmark.add_flag("-5,--select-wibble-cmd-bm",     BOOK_OPT.select_wibble_cmd_bm,      "Select/Execute a Wibble command bookmark");
    bookmark.add_flag("-6,--select-wibble-general-bm", BOOK_OPT.select_general_cmd_bm,     "Select/Execute a general command bookmark");
                 
    bookmark.add_flag("--delete-node-bm",              BOOK_OPT.delete_node_bm,            "Delete a node bookmark");
    bookmark.add_flag("--delete-task-bm",              BOOK_OPT.delete_task_bm,            "Delete a task bookmark");
    bookmark.add_flag("--delete-topic-bm",             BOOK_OPT.delete_topic_bm,           "Delete a topic bookmark");
    bookmark.add_flag("--delete-jotfile-bm",           BOOK_OPT.delete_jotfile_bm,         "Delete a jotfile bookmark");
    bookmark.add_flag("--delete-wibble-cmd-bm",        BOOK_OPT.delete_wibble_cmd_bm,      "Delete a Wibble command bookmark");
    bookmark.add_flag("--delete-general-bm",           BOOK_OPT.delete_general_cmd_bm,     "Delete a general command bookmark");

    bookmark.add_flag("--edit-node-bm",                BOOK_OPT.edit_node_bm,            "Edit a node bookmark");
    bookmark.add_flag("--edit-task-bm",                BOOK_OPT.edit_task_bm,            "Edit a task bookmark");
    bookmark.add_flag("--edit-topic-bm",               BOOK_OPT.edit_topic_bm,           "Edit a topic bookmark");
    bookmark.add_flag("--edit-jotfile-bm",             BOOK_OPT.edit_jotfile_bm,         "Edit a jotfile bookmark");
    bookmark.add_flag("--edit-wibble-cmd-bm",          BOOK_OPT.edit_wibble_cmd_bm,      "Edit a Wibble command bookmark");
    bookmark.add_flag("--edit-general-bm",             BOOK_OPT.edit_general_cmd_bm,     "Edit a general command bookmark");

    bookmark.add_option("-C,--custom-category",        BOOK_OPT.cust_exec_bm_category,     "Select/Execute a given numbered custom category.");
    bookmark.add_flag("-U,--add-new-custom-bm",        BOOK_OPT.cust_exec_category_add_bm, "Add a custom category bookmark")
            ->needs("--custom-category");
    bookmark.add_flag("-Y,--edit-new-custom-bm",       BOOK_OPT.cust_exec_category_edit_bm, "Edit a custom category bookmark")
            ->needs("--custom-category");
    bookmark.add_flag("-Z,--delete-custom-bm",         BOOK_OPT.cust_exec_category_del_bm, "Delete a custom category bookmark")
            ->needs("--custom-category");

    bookmark.add_flag("--act-custom-category",         BOOK_OPT.act_cust_exec_category,    "Reactive a custom category that has been hibernated");
    bookmark.add_flag("--add-custom-category",         BOOK_OPT.add_cust_exec_category,    "Add a new custom category");
    bookmark.add_flag("--hib-custom-category",         BOOK_OPT.hib_cust_exec_category,    "Hibernate (hide) a custom category");
    bookmark.add_flag("--ren-custom-category",         BOOK_OPT.ren_cust_exec_category,    "Rename an existing custom category");
    bookmark.add_flag("--rm-custom-category",          BOOK_OPT.rm_cust_exec_category,     "Permanently remove a custom category and all its commands");

    bookmark.add_option("-N,--select-entry",           BOOK_OPT.num,                       "Automatically select a numbered entry within a given category");
    bookmark.add_option("-?,--exec-args",              BOOK_OPT.exec_args,                 "Supply optional arguments to bookmarked command/script");
    bookmark.add_flag("-S,--silence",                  BOOK_OPT.silence,                   "Suppress all Wibble++ output");
}

/**
 * @brief Setup/ensure presence of all core/critical configuration switches; these are typically set via a config file.
 * @param config Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_config_options(CLI::App& config)
{
    //std::cout << "DEBUG: Setting up options: " << config.get_name() << std::endl;
    config.add_flag("--config-file-archived-db", CONFIG.config_archived_db , "Archived database location");
    config.add_flag("--config-file-main-db", CONFIG.config_main_db , "Main database location");

    config.add_flag("--config-dir-autofile-dir", CONFIG.config_autofile_dir , "Autofile directory");
    config.add_flag("--config-dir-backup-location", CONFIG.config_backup_location , "Backup location");
    config.add_flag("--config-dir-bookmarks-dir", CONFIG.config_bookmarks_dir , "Bookmarks directory");
    config.add_flag("--config-dir-jot-dir", CONFIG.config_jot_dir , "Journal directory");
    config.add_flag("--config-dir-schemas-dir", CONFIG.config_schemas_dir , "Schemas directory");
    config.add_flag("--config-dir-scripts-dir", CONFIG.config_scripts_dir , "Scripts directory");
    config.add_flag("--config-dir-tasks-dir", CONFIG.config_tasks_dir , "Tasks directory");
    config.add_flag("--config-dir-templates-dir", CONFIG.config_templates_dir , "Templates directory");
    config.add_flag("--config-dir-topics-dir", CONFIG.config_topics_dir , "Topics directory");
    config.add_flag("--config-dir-tmp-dir", CONFIG.config_tmp_dir , "Temporary/work directory");
    config.add_flag("--config-dir-wibble-db-dir", CONFIG.config_wibble_db_dir , "Wibble database directory");
    config.add_flag("--config-dir-wibble-store", CONFIG.config_wibble_store , "Main Wibble directory");

    config.add_flag("--config-exec-cli-editor", CONFIG.config_cli_editor , "CLI editor command");
    config.add_flag("--config-exec-dump-tool", CONFIG.config_dump_tool , "Dump tool command");
    config.add_flag("--config-exec-fm-tool", CONFIG.config_fm_tool , "File manager command");
    config.add_flag("--config-exec-gui-editor", CONFIG.config_gui_editor , "GUI editor command");
    config.add_flag("--config-exec-pager", CONFIG.config_pager , "Pager command");
    config.add_flag("--config-exec-view-tool", CONFIG.config_view_tool , "View tool command");
    config.add_flag("--config-exec-xdg-open", CONFIG.config_xdg_open_cmd , "XDG open/file association launcher command");
    config.add_flag("--config-bin-mapping", CONFIG.config_bin_mapping , "Binary file/Node pairwise mapping string");
    config.add_flag("--gen-bin-mapping", CONFIG.gen_bin_mapping , "Guided prompt to binary parwise mapping string");
}

/**
 * @brief Display current configuration/set config environment.
 * @param wc WibbleConsole utility/console object.
 */
void wibble::WibbleDispatch::show_config_env(WibbleConsole& wc)
{
    bool show_all = true;

    if(CONFIG.config_archived_db)             { std::cout << PKG_OPTS.files.archived_db             << " "; show_all = false; }
    if(CONFIG.config_main_db)                 { std::cout << PKG_OPTS.files.main_db                 << " "; show_all = false; }

    if(CONFIG.config_autofile_dir)            { std::cout << PKG_OPTS.paths.autofile_dir            << " "; show_all = false; }
    if(CONFIG.config_backup_location)         { std::cout << PKG_OPTS.paths.backup_location         << " "; show_all = false; }
    if(CONFIG.config_bookmarks_dir)           { std::cout << PKG_OPTS.paths.bookmarks_dir           << " "; show_all = false; }
    if(CONFIG.config_jot_dir)                 { std::cout << PKG_OPTS.paths.jot_dir                 << " "; show_all = false; }
    if(CONFIG.config_schemas_dir)             { std::cout << PKG_OPTS.paths.schemas_dir             << " "; show_all = false; }
    if(CONFIG.config_scripts_dir)             { std::cout << PKG_OPTS.paths.scripts_dir             << " "; show_all = false; }
    if(CONFIG.config_tasks_dir)               { std::cout << PKG_OPTS.paths.tasks_dir               << " "; show_all = false; }
    if(CONFIG.config_templates_dir)           { std::cout << PKG_OPTS.paths.templates_dir           << " "; show_all = false; }
    if(CONFIG.config_topics_dir)              { std::cout << PKG_OPTS.paths.topics_dir              << " "; show_all = false; }
    if(CONFIG.config_tmp_dir)                 { std::cout << PKG_OPTS.paths.tmp_dir                 << " "; show_all = false; }
    if(CONFIG.config_wibble_db_dir)           { std::cout << PKG_OPTS.paths.wibble_db_dir           << " "; show_all = false; }
    if(CONFIG.config_wibble_store)            { std::cout << PKG_OPTS.paths.wibble_store            << " "; show_all = false; }

    if(CONFIG.config_cli_editor)              { std::cout << PKG_OPTS.exec.cli_editor               << " "; show_all = false; }
    if(CONFIG.config_dump_tool)               { std::cout << PKG_OPTS.exec.dump_tool                << " "; show_all = false; }
    if(CONFIG.config_fm_tool)                 { std::cout << PKG_OPTS.exec.fm_tool                  << " "; show_all = false; }
    if(CONFIG.config_gui_editor)              { std::cout << PKG_OPTS.exec.gui_editor               << " "; show_all = false; }
    if(CONFIG.config_pager)                   { std::cout << PKG_OPTS.exec.pager                    << " "; show_all = false; }
    if(CONFIG.config_view_tool)               { std::cout << PKG_OPTS.exec.view_tool                << " "; show_all = false; }
    if(CONFIG.config_xdg_open_cmd)            { std::cout << PKG_OPTS.exec.xdg_open_cmd             << " "; show_all = false; }
    if(CONFIG.config_bin_mapping)             { std::cout << PKG_OPTS.exec.bin_mapping              << " "; show_all = false; }
 
    if(show_all) { VERBOSEMODE = true; validate_configuration(); wc.console_write_success_green("Configuration displayed."); }
}

/**
 * @brief Setup 'create' subcommand switches.
 * @param create Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_create_options(CLI::App& create)
{
    //std::cout << "DEBUG: Setting up options: " << create.get_name() << std::endl;
    create.add_flag("-c,--create-schema", CREATE_OPT.create_schema, "Create a metadata schema");
    create.add_flag("-C,--create-template", CREATE_OPT.create_template, "Create/edit a template file");
    create.add_flag("-D,--create-default-template", CREATE_OPT.create_default_template, "Create/edit the default template file for a filetype")
           ->excludes("--create-schema")->excludes("--create-template");
    create.add_option("-F,--from-file", CREATE_OPT.opt_from_file, "Create note with existing file content")->check(CLI::ExistingFile);
    create.add_option("-S,--schema", CREATE_OPT.opt_schema, "Create note using metadata schema")
           ->excludes("--create-schema")->excludes("--create-template", "--create-default-template"); 
    create.add_option("-T,--template", CREATE_OPT.opt_t_mplate, "Create note with templated content")
           ->excludes("--create-schema")->excludes("--from-file", "--create-template", "--create-default-template");
    create.add_flag("-L,--list", CREATE_OPT.opt_list, "List defined schemas and templates templated content")
           ->excludes("--create-schema")->excludes("--from-file", "--create-template", "--create-default-template", "--schema", "--template");
    create.add_flag("-G,--gui-editor", CREATE_OPT.use_gui_editor, "Open note after creation with GUI editor"); 
}

/**
 * @brief Setup 'daily' subcommand switches.
 * @param daily Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_daily_options(CLI::App& daily)
{
    daily.add_flag("-A,--add", DAILY_OPT.add, "Add a new daily script to the set");
    daily.add_flag("-E,--edit", DAILY_OPT.edit, "Edit an existing daily script");
    daily.add_flag("-D,--delete", DAILY_OPT.del, "Delete an existing daily script");
    daily.add_flag("-L,--ls",  DAILY_OPT.ls, "List exiting daily scripts");
    daily.add_option("-X,--exec",  DAILY_OPT.exec_ds, "Execute a single daily script or execute all scripts")->expected(0,1);
}

/**
 * @brief Setup 'data' subcommand switches.
 * @param data Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_data_options(CLI::App& data)
{
    //std::cout << "DEBUG: Setting up options: " << data.get_name() << std::endl;
    data.add_option("-C,--copy-file", DATA_OPT.add_by_copy, "COPY file or directory to to note data directory")->expected(0,1);
    data.add_option("-M,--move-file", DATA_OPT.add_by_move, "MOVE file or directory to to note data directory")
         ->excludes("--copy-file")->expected(0,1);
    data.add_option("-A,--add-link", DATA_OPT.add_linked_file, "Add a linked file or directory to to note data directory")
         ->excludes("--copy-file", "--move-file")->expected(0,1);
    data.add_flag("-L,--ls-dd", DATA_OPT.ls_dd, "List contents of data directory")
         ->excludes("--move-file", "--copy-file", "--add-link");
    data.add_flag("-O,--open-dd", DATA_OPT.open_dd, "Open data directory using file manager")
         ->excludes("--ls-dd", "--move-file", "--copy-file", "--add-link");
    data.add_flag("-T,--tree-dd", DATA_OPT.tree_dd, "Display tree contents of data directory")
         ->excludes("--open-dd", "--ls-dd", "--move-file", "--copy-file", "--add-link");
    data.add_flag("-F,--open-link", DATA_OPT.open_linked_file, "Open a linked file")
         ->excludes("--open-dd", "--ls-dd", "--move-file", "--copy-file", "--add-link", "--tree-dd");
    data.add_flag("-Z,--open-dd-file", DATA_OPT.open_dd_file, "Open an individual file from data directory")
         ->excludes("--ls-dd", "--move-file", "--copy-file", "--add-link", "--open-link");
    data.add_option("-?,--filter-dd-file", DATA_OPT.open_dd_file_filter, "Filter data directory individual file results")
         ->excludes("--ls-dd", "--move-file", "--copy-file", "--add-link", "--open-link")->needs("--open-dd-file");
    data.add_flag("--cd", DATA_OPT.cd_dd, "Change into/enter Node data directory");
    data.add_flag("--cd-node", DATA_OPT.cd_node, "Change into/enter Node source/text directory");
    _configure_generic_search_switches(data);
}

/**
 * @brief Setup 'delete' subcommand switches.
 * @param dlte Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_delete_options(CLI::App& dlte)
{
    //std::cout << "DEBUG: Setting up options: " << dlte.get_name() << std::endl;
    _configure_generic_search_switches(dlte);
}

/**
 * @brief Setup 'edit' subcommand switches.
 * @param dlte Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_edit_options(CLI::App& edit)
{
    //std::cout << "DEBUG: Setting up options: " << edit.get_name() << std::endl;
    _configure_generic_search_switches(edit);
    edit.add_flag("-G,--gui-editor", EDIT_OPT.use_gui_editor, "Open note after creation with GUI editor"); 
}

/**
 * @brief Setup 'init' subcommand switches.
 * @param init Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_init_options(CLI::App& init)
{
    init.add_option("-C,--custom-config", INIT_OPT.custom_init_file, "Save/generate configuration into custom filename");
}

/**
 * @brief Setup 'janitor' subcommand switches.
 * @param janitor Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_janitor_options(CLI::App& janitor)
{
    //std::cout << "DEBUG: Setting up options: " << janitor.get_name() << std::endl;
    janitor.add_option("-S,--scrub", JANITOR_OPT.scrub, "Scrub (move to trash) files or directories");
    janitor.add_flag("-L,--list", JANITOR_OPT.list, "List currently trashed files")->excludes("--scrub");
    janitor.add_flag("-R,--restore", JANITOR_OPT.restore, "Restore file from trash")->excludes("--scrub", "--list");
    janitor.add_flag("-X,--obliterate", JANITOR_OPT.obliterate, "Permanently delete all trashed files")->excludes("--scrub", "--list", "--restore");
    janitor.add_flag("-U,--usage", JANITOR_OPT.usage, "Show usage tips/guidance")->excludes("--scrub", "--list", "--restore", "--obliterate");
    janitor.add_flag("-C,--config", JANITOR_OPT.config, "Setup/create configuration")->excludes("--scrub", "--list", "--restore", "--obliterate");
    janitor.add_flag("-B,--boring", JANITOR_OPT.boring, "Be boring and hide the ASCII trash artwork")->needs("--obliterate");
}

/**
 * @brief Setup 'jot' subcommand switches.
 * @param jot Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_jot_options(CLI::App& jot)
{
    //std::cout << "DEBUG: Setting up options: " << jot.get_name() << std::endl;
    jot.add_flag("-C,--create", JOT_OPT.create_jotfile, "Create a new jotfile");
    jot.add_flag("-X,--remove-jotfile", JOT_OPT.delete_jotfile, "Remove an entire jotfile");
    jot.add_flag("-A,--add", JOT_OPT.add_to_jotfile, "Add an entry to a jotfile");
    jot.add_option("-D,--delete", JOT_OPT.del_jf_entry, "Delete a jotfile entry")
        ->excludes("--add")->expected(0,1);
    jot.add_option("-U,--view", JOT_OPT.view_jf_entry, "View a jotfile entry")
        ->excludes("--add", "--delete")->expected(0,1);
    jot.add_flag("-Z,--dump-all", JOT_OPT.dump_jotfile, "Dump an entire jotfile to console")
        ->excludes("--add", "--delete", "--view");
    jot.add_flag("--rename-jotfile", JOT_OPT.rename_jotfile, "Rename a jotfile")
        ->excludes("--add", "--delete", "--view", "--dump-all");
    jot.add_option("-E,--edit", JOT_OPT.edit_jf_entry, "Edit a jotfile entry")
        ->excludes("--add", "--delete", "--view", "--dump-all")->expected(0,1);
    jot.add_option("-R,--raw-edit", JOT_OPT.raw_edit, "Edit raw jotfile using editor")
        ->excludes("--add", "--delete", "--view", "--dump-all", "--edit")->expected(0,1);
    jot.add_option("-B,--batch-exec", JOT_OPT.batch, "Run shell command on all jotfile entries")
        ->excludes("--add", "--delete", "--view", "--dump-all", "--edit", "--raw-edit")->expected(0,1);
    jot.add_option("-F,--filter", JOT_OPT.filter_jot_entries, "Filter jotfile entries titles using ripgrep")
        ->excludes("--dump-all", "--raw-edit")->expected(0,1);
    jot.add_option("-P,--ripgrep", JOT_OPT.grep_jotfile, "Filter/grep jotfile file content using ripgrep")
        ->excludes("--dump-all", "--raw-edit")->expected(0,1);
    jot.add_option("-N,--jotfile-num", JOT_OPT.jf_num, "Directly select jotfile number N");
}

/**
 * @brief Setup 'sync' subcommand switches.
 * @param sync Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_sync_options(CLI::App& sync)
{
    sync.add_option("-S,--source-dir", SYNC_OPT.source_dir, "Set the synchronisation source directory");
    sync.add_option("-D,--dest-dir", SYNC_OPT.dest_dir, "Set the synchronisation dest directory");
    sync.add_flag("-M,--detailed", SYNC_OPT.detailed, "Show detailed/extended information");
    sync.add_flag("-L,--list", SYNC_OPT.tree_dir, "List current file status in source dir")->needs("--source-dir");
    sync.add_flag("-I,--status", SYNC_OPT.status, "Show synchronisation status/proposals")->needs("--source-dir", "--dest-dir");
    sync.add_flag("-X,--synchronise", SYNC_OPT.synchronise, "Execute synchronisation")->needs("--source-dir", "--dest-dir");
    sync.add_option("-A,--auto", SYNC_OPT.action, "Automatically execute action. 1 = synchronise, 2 = synchronise + new, 3 = copy destination newer")
         ->needs("--synchronise");
}

/**
 * @brief Setup 'task' subcommand switches.
 * @param task Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_task_options(CLI::App& tasks)
{
    //std::cout << "DEBUG: Setting up options: " << tasks.get_name() << std::endl;
    tasks.add_option("-A,--add", TASK_OPT.add_task, "Add a new task")->expected(0,1);
    tasks.add_flag("-H,--history", TASK_OPT.show_history, "Dump task history");
    tasks.add_option("-M,--all", TASK_OPT.mark_task, "Mark, edit, view, and manipulate all tasks by category")->expected(0,1);
    tasks.add_flag("-D,--dump-groups", TASK_OPT.dump_group_names, "Dump group names")->excludes("--add", "--all");
    tasks.add_option("-L,--list", TASK_OPT.list_tasks, "List pending tasks. Optionally filter by category substring")->excludes("--add", "--all", "--dump-groups")->expected(0,1);
    tasks.add_flag("-X,--completed", TASK_OPT.list_completed, "Show completed tasks")->excludes("--add", "--all", "--list", "--dump-groups");
    tasks.add_flag("-E,--everything", TASK_OPT.show_everything, "Show everything: all completed, all open tasks")->excludes("--add", "--all", "--list", "--completed", "--dump-groups");
    tasks.add_flag("-G,--group", TASK_OPT.view_tasks_for_group, "Mark, edit, view, and manipulate tasks by group")->excludes("--add", "--all", "--list",
                                                                                                                             "--completed", "--everything", "--dump-groups");
    tasks.add_flag("-C,--closed", TASK_OPT.closed_task_groups, "Edit, view and manipulate archived tasks by group")->excludes("--add", "--all", "--list",
                                                                                                                              "--group", "--completed", "--everything", "--dump-groups");
    tasks.add_option("-T,--title", TASK_OPT.task_title_filter, "Filter task titles by substring match.")->expected(0,1);
}

/**
 * @brief Setup 'topic' subcommand switches.
 * @param topic Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_topic_options(CLI::App& topics)
{
    //std::cout << "DEBUG: Setting up options: " << topics.get_name() << std::endl;
    // # 1 General topic options / creation / listing /description of topics
    topics.add_flag("--expunge", TOPIC_OPT.expunge_topic, "Expunge/Permanently remove an entire Topic");
    topics.add_flag("--hibernate", TOPIC_OPT.hibernate_topic, "Hibernate (hide) a topic");
    topics.add_flag("--reactivate", TOPIC_OPT.reactivate_topic, "Reactivate a hibernated topic")->excludes("--hibernate");

    topics.add_flag("-R,--create",     TOPIC_OPT.create_topic, "Create new topic");
    topics.add_flag("-L,--list",     TOPIC_OPT.list_topics, "List available topics")->excludes("--create");
    topics.add_flag("--describe", TOPIC_OPT.describe_topics, "Describe available topics")->excludes("--create", "--list");
    topics.add_option("-A,--add",    TOPIC_OPT.add_entry_to_topic, "Add/create new topic entry")->excludes("--create", "--describe", "--list")
           ->expected(0,1);
    topics.add_option("--from-file", TOPIC_OPT.add_from_file, "Create new topic entry from an existing file")->needs("--add");
    topics.add_option("--from-template", TOPIC_OPT.add_with_template, "Create new topic entry from an existing file")->needs("--add")->excludes("--from-file");

    topics.add_option("--custom-date", TOPIC_OPT.override_date, "Specify a custom date (YYYY-MM-DD) instead of using current datetime")
           ->needs("--add");

    // # 2 Topic entry options / creation / editing / filtering of topics
    topics.add_option("-E,--edit",   TOPIC_OPT.edit_topic_entry, "Edit existing topic entry with CLI editor")
           ->excludes("--create")->excludes("--describe", "--list", "--add")->expected(0,1);
    topics.add_option("-G,--gui-editor", TOPIC_OPT.gui_edit_topic_entry, "Use GUI editor instead of CLI editor")
           ->excludes("--describe", "--list")->expected(0,1);
    topics.add_option("-D,--delete", TOPIC_OPT.delete_entry_from_topic, "Delete entry from topic")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor")->expected(0,1);
    topics.add_option("-U,--dump", TOPIC_OPT.dump_topic_entry, "Dump/cat topic entry")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->expected(0,1);

    topics.add_option("-O,--configure", TOPIC_OPT.configure_existing_topic, "Configure/edit existing topic")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump")->expected(0,1);

    topics.add_option("-V,--view", TOPIC_OPT.view_topic_entry, "View topic entry")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure")->expected(0,1);
    topics.add_flag("-B,--batch-exec", TOPIC_OPT.batch_exec, "Execute specified command/script on batch set of topic entries (CAUTION)");
    topics.add_option("-^,--exec-cmd", TOPIC_OPT.exec_cmd_on_topic_entries, "Execute a specified command pattern on topic entry")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view")->expected(0,1);
    topics.add_option("-?,--exec-script", TOPIC_OPT.exec_script_cmd_on_topic_entries, "Run script command on topic entries")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--exec-cmd")->expected(0,1);
    topics.add_option("-W,--exec-topic", TOPIC_OPT.exec_topic_for_cmd, "Specify topic for command")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--exec-script")->expected(0,1)->needs("--exec-cmd");
    topics.add_option("-%,--exec-script-topic", TOPIC_OPT.exec_topic_for_script_cmd, "Specify topic for script")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--exec-topic")->expected(0,1)->needs("--exec-script");

    topics.add_option("-F,--filter", TOPIC_OPT.filter_topic_entry, "Filter topic entry titles using ripgrep")
           ->excludes("--create", "--list", "--describe", "--add");

    topics.add_option("-P,--ripgrep", TOPIC_OPT.grep_topic_entry, "Filter/grep topic entries file content using ripgrep")
           ->excludes("--create", "--list", "--describe", "--add");

    // # 3 Topic data options / adding / removing / opening of data files
    topics.add_option("-C,--copy-data-file", TOPIC_OPT.copy_data_file, "COPY a file/directory to data storage of topic")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep");

    topics.add_option("-M,--move-data-file", TOPIC_OPT.move_data_file, "MOVE a file/directory to data storage of topic")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file");

    topics.add_option("-Y,--open-data-file", TOPIC_OPT.open_data_file, "List/open a file/directory from data storage of topic")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
           ->excludes("--move-data-file")->expected(0,1);

    topics.add_option("-Z,--delete-data-file", TOPIC_OPT.delete_data_file, "Delete item from data storage of topic")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
           ->excludes("--move-data-file", "--open-data-file")->expected(0,1);

    topics.add_option("-S,--set-data-topic", TOPIC_OPT.set_data_topic, "Explicitly specify topic for data operation")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep");

    topics.add_option("-I,--inventory-topic", TOPIC_OPT.inventory_topic, "List/inventory topic contents (notes + data)")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
           ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter")->expected(0,1);

    topics.add_option("--default-template", TOPIC_OPT.create_edit_default_template, "Create or edit default topic template")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
           ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter")->expected(0,1);

    topics.add_option("--migrate-topic-entry", TOPIC_OPT.migrate_topic, "Migrate/move a topic from one topic to another")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
           ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter", "--default-template")->expected(0,1);

    topics.add_option("--migrate-data-entry", TOPIC_OPT.migrate_data_topic, "Migrate/move a topic data attachment from one topic to another")
           ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
           ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
           ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter", "--default-template")
           ->excludes("--migrate-topic-entry")->expected(0,1);

    topics.add_option("--cd-data", TOPIC_OPT.cd_data_dir, "cd into the Topic data entry directory")
        ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
        ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
        ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter", "--default-template")
        ->excludes("--migrate-topic-entry", "--migrate-data-entry")->expected(0,1);

    topics.add_option("--alter-entry-title", TOPIC_OPT.alter_ent_title, "Alter/adjust the title for a Topic entry")
        ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
        ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
        ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter", "--default-template")
        ->excludes("--migrate-topic-entry", "--migrate-data-entry", "--cd-data")->expected(0,1);

    topics.add_option("--alter-data-title", TOPIC_OPT.alter_dat_title, "Alter/adjust the title for a Topic data entry")
        ->excludes("--create", "--describe", "--list", "--add", "--edit", "--gui-editor", "--delete")
        ->excludes("--dump", "--configure", "--view", "--ripgrep", "--copy-data-file")
        ->excludes("--move-data-file", "--delete-data-file", "--set-data-topic", "--filter", "--default-template")
        ->excludes("--migrate-topic-entry", "--migrate-data-entry", "--cd-data", "--alter-entry-title")->expected(0,1);

    topics.add_option("--specify-title", TOPIC_OPT.specify_title, "Specify title for topic [or data] entry. Implies non-interactivity/no open afterwards.")
           ->needs("--add");

    topics.add_option("--custom-data-date", TOPIC_OPT.override_data_date, "Specify a custom date for data attachment (YYYY-MM-DD) instead of using current datetime")
           ->excludes("--add");


    topics.add_flag("-r,--render", TOPIC_OPT.render, "Pass Topic entry through render script");
}


/**
 * @brief Setup 'transfer' subcommand switches.
 * @param task Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_transfer_options(CLI::App& transfer)
{
    //std::cout << "DEBUG: Setting up options: " << transfer.get_name() << std::endl;
    transfer.add_flag("--create-bundle", TRANS_OPT.create_bundle, "Create export bundle from contents of export directory");
    transfer.add_flag("--import-bundle", TRANS_OPT.import_bundle, "Import data from wibb bundle file or directory")->excludes("--create-bundle");
    transfer.add_option("--dl-bundle", TRANS_OPT.dl_bundle, "Download and import data from Wibb bundle via URL");
    transfer.add_option("--dl-bundle-sha256", TRANS_OPT.dl_bundle_sha256, "Manually specify Wibb bundle SHA256 URL");
    transfer.add_flag("--export-bin-map", TRANS_OPT.export_bin_map, "Export binary mappings into target export path");//->excludes("--import-nodes");
    transfer.add_flag("--export-nodes", TRANS_OPT.export_nodes, "Export Nodes into target export path");//->excludes("--import-nodes");
    transfer.add_flag("--export-task-group", TRANS_OPT.export_task_group, "Export Task group into target export pathh");//->excludes("--import-task-group");
    transfer.add_flag("--export-topic", TRANS_OPT.export_topic, "Export Topic into target export path");//->excludes("--import-topic");
    transfer.add_option("--path", TRANS_OPT.dir_path, "Path to import/export directory or wibb bundle archive");
}


/**
 * @brief Setup 'dump' subcommand switches.
 * @param dump Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_dump_options(CLI::App& dump)
{
    //std::cout << "DEBUG: Setting up options: " << dump.get_name() << std::endl;
    _configure_generic_search_switches(dump);
}

/**
 * @brief Setup 'drawer' subcommand switches.
 * @param dump Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_drawer_options(CLI::App& drawer)
{
    //std::cout << "DEBUG: Setting up options: " << drawer.get_name() << std::endl;
    drawer.add_flag("-A,--add", DRAW_OPT.add_drawer, "Add/setup a new drawer");
    drawer.add_option("-P,--push", DRAW_OPT.push_to_drawer, "Push file(s) into drawer")->expected(0,1);
    drawer.add_flag("--cd", DRAW_OPT.cd_drawer, "Change/cd into drawer directory")->excludes("--push");
    drawer.add_flag("-D,--delete", DRAW_OPT.delete_drawer, "Delete an entire drawer")->excludes("--cd", "--push");
    drawer.add_flag("-L,--list", DRAW_OPT.list_drawer, "List contents of a drawer")->excludes("--delete", "--cd", "--push");
    drawer.add_flag("-S,--select", DRAW_OPT.select_drawer, "Select active drawer")->excludes("--list", "--delete", "--cd", "--push");
    drawer.add_flag("-B,--browse", DRAW_OPT.browse_drawer, "Browse drawer list and select to view")->excludes("--list", "--delete", "--cd", "--push", "--select");
    drawer.add_flag("--copy-to-node", DRAW_OPT.copy_to_node, "Copy entire drawer contents to Node data directory")->excludes("--list", "--delete", "--cd", "--push", "--select", "--browse");
    drawer.add_flag("--copy-to-topic", DRAW_OPT.copy_to_topic, "Copy entire drawer contents as Topic data entry")->excludes("--list", "--delete", "--cd", "--push", "--select", "--browse", "--copy-to-node");
    drawer.add_option("-N,--num", DRAW_OPT.num_drawer, "Override active drawer");
    drawer.add_flag("-F, --force", DRAW_OPT.force_mode, "Bypass confirmation when pushing to drawer")->needs("--push");
    drawer.add_flag("-T, --tree", DRAW_OPT.tree_mode, "Display directory tree rather than use ls");
}

/**
 * @brief Setup 'exec' subcommand switches.
 * @param exec Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_exec_options(CLI::App& exec)
{
    //std::cout << "DEBUG: Setting up options: " << exec.get_name() << std::endl;
    exec.add_option("-^,--exec-cmd", EXEC_OPT.exec_cmd, "Execute specified command on note")->expected(0,1);
    exec.add_option("-?,--exec-script", EXEC_OPT.exec_script, "Execute named script from scripts directory on note")->excludes("--exec-cmd")->expected(0,1);
    exec.add_flag("-B,--batch-exec", EXEC_OPT.batch_exec, "Execute specified command/script on batch set of notes (CAUTION)");
    _configure_generic_search_switches(exec);
}

/**
 * @brief Setup 'metadata' subcommand switches.
 * @param metadata Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_meta_options(CLI::App& meta)
{
    //std::cout << "DEBUG: Setting up options: " << meta.get_name() << std::endl;
    // switch processing/validation logic is in WibbleCmdMetadata class
    meta.add_flag("-U,--update-metadata", META_OPT.update_mt, "Interactively update the metadata on a note");
    meta.add_flag("-P,--display-metadata", META_OPT.print_mt, "Display the full metadata for a note");
    meta.add_flag("--md-date", META_OPT.get_mt_date, "Return the note Date metadata");
    meta.add_flag("--md-title", META_OPT.get_mt_title, "Return the note Title metadata");
    meta.add_flag("--md-id", META_OPT.get_mt_id, "Return the NoteId metadata");
    meta.add_flag("--md-desc", META_OPT.get_mt_desc, "Return the Description metadata");
    meta.add_flag("--md-filetype", META_OPT.get_mt_ft, "Return the note Filetype metadata");
    meta.add_flag("--md-project", META_OPT.get_mt_proj, "Return the note Project metadata");
    meta.add_flag("--md-tags", META_OPT.get_mt_tags, "Return the note Tags metadata");
    meta.add_flag("--md-class", META_OPT.get_mt_cls, "Return the note Class metadata");
    meta.add_flag("--md-datadirs", META_OPT.get_mt_dd, "Return the note DataDirs metadata");
    meta.add_flag("--md-keypairs", META_OPT.get_mt_kp, "Return the note KeyPairs metadata");
    meta.add_flag("--md-linkedids", META_OPT.get_mt_lids, "Return the note LinkedIds metadata");
    meta.add_flag("--rel-add-parent", META_OPT.rel_as_parent, "Define a new Node relationship as parent");
    meta.add_flag("--rel-add-sibling", META_OPT.rel_as_sibling, "Define a new Node relationship as sibling");
    meta.add_flag("--rel-add-child", META_OPT.rel_as_child, "Define a new Node relationship as child");
    meta.add_flag("-M,--rel-interactive", META_OPT.rel_interactive, "Display the main Node relationship menu");
    meta.add_flag("--rel-delete", META_OPT.rel_delete, "Delete an existing Node relationship interactively");
    meta.add_flag("--rel-delete-all", META_OPT.rel_delete_all, "Delete all relationships for an existing Node interactively");
    meta.add_flag("-S,--show", META_OPT.show_rels, "Display defined Node relationships (expanded)");
    meta.add_flag("--showc", META_OPT.show_rels_c, "Display defined Node relationships (compact)");
    meta.add_flag("--cd", META_OPT.cd_dd, "Change into/enter Node data directory");
    meta.add_flag("--cd-node", META_OPT.cd_node, "Change into/enter Node source/text directory");
    meta.add_flag("--path-data", META_OPT.path_dd, "Output Node data directory/path");
    meta.add_flag("--path-node", META_OPT.path_node, "Output Node file location/path");
    _configure_generic_search_switches(meta);
}

/**
 * @brief Setup 'view' subcommand switches.
 * @param view Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::setup_view_options(CLI::App& view)
{
    //std::cout << "DEBUG: Setting up options: " << view.get_name() << std::endl;
    _configure_generic_search_switches(view);
    view.add_flag("-r,--render", VIEW_OPT.render, "Pass Node through render script");
}

// main routing
/**
 * @brief Core entry point for processing a given subcommand.
 * @param cmd Reference to instance of CLI::App subcommand object to work on
 */
void wibble::WibbleDispatch::handle_cmd(CLI::App& cmd)
{
    WibbleConsole wc;
    //std::cout << "DEBUG: Working with app subcommand: " << cmd.get_name() << std::endl;
    //for(auto *opt : cmd.get_options()) { std::cout << "Option: " << opt->get_name() << std::endl; }
    //std::cout << "DEBUG: Using main DB: " << PKG_OPTS.files.main_db << std::endl;

    // CRITICAL: ensure all "~" symbols are removed/expanded
    _expand_all_settings(PKG_OPTS);

    //wibble::WibBinList().set_display();
    
    // ensure configuration directories exist, UNLESS it is a first run bootstrap
    if(cmd.get_name() != "bootstrap")    { _prepare_directories(wc, CMD_PATHS); }

    // keep the cache maintained - delete files older than 48 hours
    wibble::WibbleCache::clean_cache(PKG_OPTS.paths.tmp_dir);

    // use interactive menu to setup search filters, if applicable
    if(SEARCH_FILTERS.menu_search)        { WibbleUtility::menu_search_set_filters(wc, SEARCH_FILTERS);              }
    
    if     (cmd.get_name() == "archive")  { WibbleCmdArchive::handle_archive(PKG_OPTS, SEARCH_FILTERS, ARCHIVE_OPT); }
    if     (cmd.get_name() == "autofile") { WibbleCmdAutofile::handle_autofile(PKG_OPTS, AUTOFILE_OPT);              }
    else if(cmd.get_name() == "backup")   { WibbleCmdBackup::handle_backup(PKG_OPTS, BACKUP_OPT);                    }
    else if(cmd.get_name() == "batch")    { WibbleCmdBatch::handle_batch(PKG_OPTS, BATCH_OPT);                       }
    else if(cmd.get_name() == "bookmark") { WibbleCmdBookmark::handle_bookmark(PKG_OPTS, BOOK_OPT);                  }
    else if(cmd.get_name() == "config")   { if(! CONFIG.gen_bin_mapping) { show_config_env(wc); } else { _gen_bin_conf(wc); } }
    else if(cmd.get_name() == "create")   { WibbleCmdCreate::handle_create(PKG_OPTS, CREATE_OPT);                    }
    else if(cmd.get_name() == "daily")    { WibbleCmdDaily::handle_daily(PKG_OPTS, DAILY_OPT);                       }
    else if(cmd.get_name() == "data")     { WibbleCmdData::handle_data(PKG_OPTS, SEARCH_FILTERS, DATA_OPT);          }
    else if(cmd.get_name() == "delete")   { WibbleCmdDelete::handle_delete(PKG_OPTS, SEARCH_FILTERS);                }
    else if(cmd.get_name() == "drawer")   { WibbleCmdDrawer::handle_drawer(PKG_OPTS, DRAW_OPT);                      }
    else if(cmd.get_name() == "dump")     { WibbleCmdDump::handle_dump(PKG_OPTS, SEARCH_FILTERS);                    }
    else if(cmd.get_name() == "edit")     { WibbleCmdEdit::handle_edit(PKG_OPTS, SEARCH_FILTERS, EDIT_OPT);          }
    else if(cmd.get_name() == "exec")     { WibbleCmdExec::handle_exec(PKG_OPTS, SEARCH_FILTERS, EXEC_OPT);          }
    else if(cmd.get_name() == "init" || cmd.get_name() == "bootstrap")
    {
        WibbleCmdInit wi;
        std::string DEFAULT_CONFIG_DIR = "~/.config/wibble";
        wibble::WibbleUtility::expand_tilde(DEFAULT_CONFIG_DIR);
        _config_create_dir_if_needed(wc, DEFAULT_CONFIG_DIR);
        wi.init_config(INIT_OPT);
            
    }
    else if(cmd.get_name() == "jot")      { WibbleCmdJot::handle_jot(PKG_OPTS, JOT_OPT);                             }
    else if(cmd.get_name() == "janitor")  { WibbleCmdJanitor::handle_janitor(PKG_OPTS, JANITOR_OPT);                 }
    else if(cmd.get_name() == "metadata") { WibbleCmdMetadata().handle_metadata(PKG_OPTS, SEARCH_FILTERS, META_OPT); }
    else if(cmd.get_name() == "sync")     { WibbleCmdSync::handle_sync(SYNC_OPT);                                    }
    else if(cmd.get_name() == "task")     { WibbleCmdTask::handle_task(PKG_OPTS, TASK_OPT);                          }
    else if(cmd.get_name() == "topic")    { WibbleCmdTopic::handle_topic(PKG_OPTS, TOPIC_OPT);                       }
    else if(cmd.get_name() == "transfer") { WibbleCmdTransfer::handle_transfer(PKG_OPTS, SEARCH_FILTERS, TRANS_OPT); }
    else if(cmd.get_name() == "version")
    {
        std::cout << WibbleVersion::get_build_info() << "\n";
        std::cout << WibbleVersion::get_version() << "\n";
    }
    else if(cmd.get_name() == "view")     { WibbleCmdView::handle_view(PKG_OPTS, SEARCH_FILTERS, VIEW_OPT);          }
}


/**
 * @brief Immediate entry point from main; process initial command argument.
 * @param argc Command line argument count
 * @param argv Command line argument list
 */
int wibble::WibbleDispatch::dispatch_cli(int argc, char** argv)
{
    std::string default_path = "~/.config/wibble";
    wibble::WibbleUtility::expand_tilde(default_path);
    CLI::App app{"Wibble++ CLI"};
    // ensure external directory helper value is reset very early on
    //wibble::WibbleConsole::init_output();
    wibble::WibbleUtility::cd_helper();

    if(argc == 1 && ! (std::filesystem::exists(std::filesystem::path(default_path + "/wibble.toml"))))
    {
        std::cout << "No configuration file found at '~/.config/wibble/wibble.toml'.\n\n";
        std::cout << "1. Run the command 'wibble++ bootstrap' to setup a brand new configuration.\n";
        std::cout << "2. Alternatively, specify the --config option and provide the path to a custom configuration file.\n"; 
    }
    // bootstrap function: need a way to skip checking/CLI validation for config file, in order to CREATE a config file from scratch
    else if(argc == 2 && argv[1] && std::string(argv[1]) == "bootstrap")//strcmp(argv[1], "bootstrap") == 0)
    {
        auto cmd_init = app.add_subcommand("bootstrap", "Setup a fresh Wibble repository");                  // #10  
        setup_init_options(*cmd_init);        // #10
        CLI11_PARSE(app, argc, argv);
        handle_cmd(*cmd_init);
    }
    else
    {
        bool show_bootstrap = true;
        // hide/disable bootstrap option if a default configuration file already exists/they're already setup
        if(std::filesystem::exists(std::filesystem::path(default_path + "/wibble.toml"))) { show_bootstrap = false; }

        app.set_config("--config",
                       "wibble.toml",
                       "Specify wibble.toml configuration file",
                       true)->transform(CLI::FileOnDefaultPath("./tests/test-stubs", false)) // for test configuration
                            ->transform(CLI::FileOnDefaultPath(default_path, false)); 
        
        // setup global options
        // 1. Core files
        app.add_option("--archived_db",     CMD_FILES.archived_db,     "Specify Wibble archive database")->required()->group("Core configuration");
        app.add_option("--main_db",         CMD_FILES.main_db, "Specify Wibble main database")->required()->group("Core configuration");

        // 2. Core paths
        app.add_option("--autofile_dir",    CMD_PATHS.autofile_dir, "Specify Wibble backup location")->required()->group("Core configuration");
        app.add_option("--backup_location", CMD_PATHS.backup_location, "Specify Wibble backup location")->required()->group("Core configuration");
        app.add_option("--bookmarks_dir",   CMD_PATHS.bookmarks_dir, "Specify Wibble bookmarks directory")->required()->group("Core configuration");
        app.add_option("--drawers_dir",     CMD_PATHS.drawers_dir, "Specify Wibble drawers directory")->required()->group("Core configuration");
        app.add_option("--jot_dir",         CMD_PATHS.jot_dir, "Specify Wibble journal directory")->required()->group("Core configuration");
        app.add_option("--schemas_dir",     CMD_PATHS.schemas_dir, "Specify Wibble schemas directory")->required()->group("Core configuration");
        app.add_option("--scripts_dir",     CMD_PATHS.scripts_dir, "Specify Wibble scripts directory")->required()->group("Core configuration");
        app.add_option("--tasks_dir",       CMD_PATHS.tasks_dir, "Specify Wibble tasks directory")->required()->group("Core configuration");
        app.add_option("--templates_dir",   CMD_PATHS.templates_dir, "Specify Wibble templates directory")->required()->group("Core configuration");
        app.add_option("--topics_dir",      CMD_PATHS.topics_dir, "Specify Wibble topics directory")->required()->group("Core configuration");
        app.add_option("--tmp_dir",         CMD_PATHS.tmp_dir, "Specify Wibble working/temporary directory")->required()->group("Core configuration");
        app.add_option("--wibble_db_dir",   CMD_PATHS.wibble_db_dir, "Specify Wibble database directory")->required()->group("Core configuration");
        app.add_option("--wibble_store",    CMD_PATHS.wibble_store, "Specify main Wibble storage directory")->required()->group("Core configuration");

        // 3. Core tools/applications
        app.add_option("--cli_editor",   CMD_EXEC.cli_editor, "Specify Wibble CLI editor")->required()->group("Core configuration");
        app.add_option("--dump_tool",    CMD_EXEC.dump_tool, "Specify Wibble dump/cat tool")->required()->group("Core configuration");
        app.add_option("--fm_tool",      CMD_EXEC.fm_tool, "Specify Wibble file manager")->required()->group("Core configuration");
        app.add_option("--gui_editor",   CMD_EXEC.gui_editor, "Specify Wibble GUI editor")->required()->group("Core configuration");
        app.add_option("--pager",        CMD_EXEC.pager, "Specify Wibble pager")->required()->group("Core configuration");
        app.add_option("--view_tool",    CMD_EXEC.view_tool, "Specify Wibble viewing/read-only tool")->required()->group("Core configuration");
        app.add_option("--xdg_open_cmd", CMD_EXEC.xdg_open_cmd, "Specify XDG open command")->required()->group("Core configuration");

        // optional binary mapping
        app.add_option("--binary_mapping", CMD_EXEC.bin_mapping, "Specify binary file/association mapping list (advanced)")->group("Core configuration");

        // 4. Global flags
        bool no_colour = false;
        bool verbose_mode = false;
        bool ascii_chars = false;
        app.add_flag("-A,--ascii", ascii_chars, "Use simple/no-extended characters");
        app.add_flag("-N,--no_colour", no_colour, "Disable all colour output to terminal");
        app.add_flag("-V,--verbose", verbose_mode, "Toggle verbose mode");

        CLI::App* cmd_bootstrap = nullptr;
        // Setup sub-commands
        auto cmd_archive   = app.add_subcommand("archive", "Archive or un-archive Node");                    // #1
        auto cmd_autofile  = app.add_subcommand("autofile", "Automatically file away files and data");       // #2
        auto cmd_backup    = app.add_subcommand("backup", "Backup note store & data");                       // #3  
        if(show_bootstrap) { cmd_bootstrap = app.add_subcommand("bootstrap", "Prepare/setup an initial Wibble configuration"); } // #4   
        auto cmd_batch     = app.add_subcommand("batch", "Perform operations non-interactively");            // #5 
        auto cmd_bookmark  = app.add_subcommand("bookmark", "Bookmark an existing Node, Topic, Jotfile, or command"); // #6 
        auto cmd_config    = app.add_subcommand("config", "Display/validate configuration settings");        // #7 
        auto cmd_create    = app.add_subcommand("create", "Create a new Node");                              // #8 
        auto cmd_daily     = app.add_subcommand("daily", "Create or run all daily scripts");                 // #9  
        auto cmd_data      = app.add_subcommand("data", "Add, view, and work with Node data & attachments"); // #10   
        auto cmd_delete    = app.add_subcommand("delete", "Delete an existing Node & any associated data");  // #11   
        auto cmd_drawer    = app.add_subcommand("drawer", "Temporarily store files for later organisation"); // #12   
        auto cmd_dump      = app.add_subcommand("dump", "Dump an existing Node");                            // #13   
        auto cmd_edit      = app.add_subcommand("edit", "Edit an existing Node");                            // #14   
        auto cmd_exec      = app.add_subcommand("exec", "Execute an arbitrary command template on a Node");  // #15    
        auto cmd_init      = app.add_subcommand("init", "Generate a Wibble configuration file");             // #16    
        auto cmd_janitor   = app.add_subcommand("janitor", "Safely and sanely delete & restore files");      // #    
        auto cmd_jot       = app.add_subcommand("jot", "Quickly edit/work with a Jotfile");                  // #17    
        auto cmd_metadata  = app.add_subcommand("metadata", "Update or display Node metadata");              // #18     
        //auto cmd_server    = app.add_subcommand("server", "Start up a web server/socket for Web UI");        // #19     
        auto cmd_sync      = app.add_subcommand("sync", "Synchronise files based on manual timestamps");     // #20     
        auto cmd_task      = app.add_subcommand("task", "Task and Project management");                      // #21     
        auto cmd_topic     = app.add_subcommand("topic", "Topic notes management");                          // #22     
        auto cmd_transfer  = app.add_subcommand("transfer", "Import/export material between Wibble++ configs"); // #23     
        //auto cmd_tool      = app.add_subcommand("tool", "Helper utilities for working with notes");          //      
        auto cmd_version   = app.add_subcommand("version", "Show Wibble++ build/version information");       // #24     
        auto cmd_view      = app.add_subcommand("view", "View an existing Node");                            // #25     

        // Setup command options
        setup_archive_options(*cmd_archive);   // #1
        setup_autofile_options(*cmd_autofile); // #2
        setup_backup_options(*cmd_backup);     // #3
        setup_batch_options(*cmd_batch);       // #5
        setup_bookmark_options(*cmd_bookmark); // #6
        setup_config_options(*cmd_config);     // #7
        setup_create_options(*cmd_create);     // #8
        setup_daily_options(*cmd_daily);       // #9
        setup_data_options(*cmd_data);         // #10
        setup_delete_options(*cmd_delete);     // #11
        setup_drawer_options(*cmd_drawer);     // #12
        setup_dump_options(*cmd_dump);         // #13
        setup_edit_options(*cmd_edit);         // #14
        setup_exec_options(*cmd_exec);         // #15
        setup_init_options(*cmd_init);         // #16
        setup_janitor_options(*cmd_janitor);   // #
        setup_jot_options(*cmd_jot);           // #17
        setup_meta_options(*cmd_metadata);     // #18
        //setup_server_options(*cmd_server);   // #19
        setup_sync_options(*cmd_sync);         // #20
        setup_task_options(*cmd_task);         // #21
        setup_topic_options(*cmd_topic);       // #22
        setup_transfer_options(*cmd_transfer); // #23
        setup_view_options(*cmd_view);         // #25

        // Allow at most ONE subcommand per invocation
        // No further checking logic required
        app.require_subcommand(1);
        // Parse/setup all of the command line options & values
        CLI11_PARSE(app, argc, argv);
        
        // set currently live/used configuration file
        CMD_FILES.live_config_file = std::filesystem::absolute(std::filesystem::path(app["--config"]->as<std::string>()));

        // "ascii" switch to remove non-standard characters for use with fixed/limited fonts etc.
        if(ascii_chars)  { ARW = "[-] "; SUC = "[*] "; ERR = "[x] "; TCK = "*"; CRS = "_"; }
        // global switch to toggle all colour on/off for terminal output
        if(no_colour)    { ENABLECOLOUR = false; }
        if(verbose_mode) { VERBOSEMODE = true;   }

        if(! validate_configuration())
        {
            std::cout << "Errors in configuration.\n" << std::endl;
            std::cout << "1. First run/brand new configuration: please run 'wibble init' for a guided automatic configuration." << std::endl;
            std::cout << "2. For existing configurations: please correct the errors/UNSET values shown." << std::endl;
            std::cout << "3. For alternate configurations: please specify a correct configuration file using the --config option." << std::endl;
            throw wibble::wibble_exit(0);
        }

        // Dispatch logic
             if(app.got_subcommand("archive"))  handle_cmd(*cmd_archive);
        else if(app.got_subcommand("autofile")) handle_cmd(*cmd_autofile);
        else if(app.got_subcommand("backup"))   handle_cmd(*cmd_backup);
        else if(show_bootstrap && cmd_bootstrap)
        { if(app.got_subcommand("bootstrap"))   handle_cmd(*cmd_bootstrap); }
        else if(app.got_subcommand("batch"))    handle_cmd(*cmd_batch);
        else if(app.got_subcommand("bookmark")) handle_cmd(*cmd_bookmark);
        else if(app.got_subcommand("config"))   handle_cmd(*cmd_config);
        else if(app.got_subcommand("create"))   handle_cmd(*cmd_create);
        else if(app.got_subcommand("daily"))    handle_cmd(*cmd_daily);
        else if(app.got_subcommand("data"))     handle_cmd(*cmd_data);
        else if(app.got_subcommand("delete"))   handle_cmd(*cmd_delete);
        else if(app.got_subcommand("drawer"))   handle_cmd(*cmd_drawer);
        else if(app.got_subcommand("dump"))     handle_cmd(*cmd_dump);
        else if(app.got_subcommand("edit"))     handle_cmd(*cmd_edit);
        else if(app.got_subcommand("exec"))     handle_cmd(*cmd_exec);
        else if(app.got_subcommand("init"))     handle_cmd(*cmd_init);
        else if(app.got_subcommand("janitor"))  handle_cmd(*cmd_janitor);
        else if(app.got_subcommand("jot"))      handle_cmd(*cmd_jot);
        else if(app.got_subcommand("metadata")) handle_cmd(*cmd_metadata);
             //else if(app.got_subcommand("search"))   handle_cmd(*cmd_search);
        //else if(app.got_subcommand("server"))   handle_cmd(*cmd_server);
        else if(app.got_subcommand("sync"))     handle_cmd(*cmd_sync);
        else if(app.got_subcommand("task"))     handle_cmd(*cmd_task);
             //else if(app.got_subcommand("tool"))     handle_cmd(*cmd_tool);
        else if(app.got_subcommand("topic"))    handle_cmd(*cmd_topic);
        else if(app.got_subcommand("transfer")) handle_cmd(*cmd_transfer);
        else if(app.got_subcommand("view"))     handle_cmd(*cmd_view);
        else if(app.got_subcommand("version"))  handle_cmd(*cmd_version);

        //debug_config();
    }
    return(EXIT_SUCCESS);
};
