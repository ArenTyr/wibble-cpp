/**
 * @file      cmd_data.cpp
 * @brief     Handle "data" subcommand.
 * @details
 *
 * This class handles all of the dispatching logic for the "data"
 * subcommand which allows copying/moving, listing, opening, and linking
 * of files/directories into the associated "data directory" for a
 * Wibble Node.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_data.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Dispatching logic for the "data" operation on Wibble nodes.
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 * @param DATA command line options/parameters supplied via switches for "data"
 */
void wibble::WibbleCmdData::handle_data(pkg_options& OPTS, search_filters& SEARCH, data_options& DATA)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);
    WibbleRecord w;

    // retrieve the record interactively
    w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH);

    if(DATA.cd_dd || DATA.cd_node) // enter directory
    {
        const bool txt_not_data = DATA.cd_node ? true : false;
        WibbleUtility::cd_record(wib_e, w, SEARCH.archive_db, txt_not_data);
    }
    else if(DATA.add_by_copy != "UNSET") // copy file/dir into data directory
    {
        if(DATA.add_by_copy == "") { wibble::WibbleUtility::fix_file_param_get_input(wc, DATA.add_by_copy); }

        bool copy_result = false;
        if(! SEARCH.archive_db)
        {
            copy_result = wib_e.op_add_data_to_note(w, OPTS.files.main_db,
                                                    OPTS.paths.tmp_dir + "/wibble-workfile", false, DATA.add_by_copy, true);
        }
        else
        {
            copy_result = wib_e.op_add_data_to_note(w, OPTS.files.archived_db,
                                                    OPTS.paths.tmp_dir + "/wibble-workfile", true, DATA.add_by_copy, true);
        }

        if(copy_result) { wc.console_write_success_green("Data successfully copied into note data directory for note: " + w.get_id()); }
        else            { wc.console_write_error_red("Data was NOT successfully copied into note data directory for note: " + w.get_id());     }
    }
    else if(DATA.add_by_move != "UNSET") // move file/dir into data directory
    {
        if(DATA.add_by_move == "") { wibble::WibbleUtility::fix_file_param_get_input(wc, DATA.add_by_move); }

        bool move_result = false;
        if(! SEARCH.archive_db)
        {
            move_result = wib_e.op_add_data_to_note(w, OPTS.files.main_db,
                                                    OPTS.paths.tmp_dir + "/wibble-workfile", false, DATA.add_by_move, false);
        }
        else
        {
            move_result = wib_e.op_add_data_to_note(w, OPTS.files.archived_db,
                                                    OPTS.paths.tmp_dir + "/wibble-workfile", true, DATA.add_by_move, false);
        }

        if(move_result) { wc.console_write_success_green("Data successfully moved into note data directory for note: " + w.get_id()); }
        else            { wc.console_write_error_red("Data was NOT successfully moved into note data directory for note: " + w.get_id());     }

    }
    else if(DATA.add_linked_file != "UNSET") // "link"/associate an existing file/dir to Node
    {
        if(DATA.add_linked_file == "") { wibble::WibbleUtility::fix_file_param_get_input(wc, DATA.add_linked_file); }

        if(DATA.add_linked_file == "") { wc.console_write_error_red("File must exist and/or filename cannot be blank. Aborting."); }
        else
        {
            bool link_result;

            if(! SEARCH.archive_db)
            { link_result = wib_e.op_add_linked_file_to_note(w, OPTS.files.main_db, OPTS.paths.tmp_dir + "/wibble-workfile", false, DATA.add_linked_file);    }
            else
            { link_result = wib_e.op_add_linked_file_to_note(w, OPTS.files.archived_db, OPTS.paths.tmp_dir + "/wibble-workfile", true, DATA.add_linked_file); }

            if(link_result) { wc.console_write_success_green("Linked file successfully associated with note."); }
            else            { wc.console_write_error_red("Error associating linked file to note.");           }
        }
    }
    else if(DATA.open_linked_file) // open a linked file (i.e. an associated file that is not physically located in the data directory)
    {
        wib_e.note_open_linked_file(w, SEARCH.archive_db);
    }
    else if(DATA.open_dd) // open the data directory using file manager
    {
        wib_e.note_dd_ls_view_dump(w, SEARCH.archive_db, 3, OPTS.exec.fm_tool);
    }
    else if(DATA.open_dd_file) // open an individual data directoy file directly, with optional filename/path filter
    {
        const std::string dd_fltr = (DATA.open_dd_file_filter != "UNSET") ? DATA.open_dd_file_filter : "";
        wib_e.note_dd_open_individual_file(w, SEARCH.archive_db, dd_fltr);
    }
    else if(DATA.tree_dd) // run UNIX/GNU "tree" command on data directory
    {
        wib_e.note_dd_ls_view_dump(w, SEARCH.archive_db, 2);
    }
    else // default to running ls
    {
        wib_e.note_dd_ls_view_dump(w, SEARCH.archive_db, 1);
    }
}
