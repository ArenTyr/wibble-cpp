/**
 * @file      wibblebatchmanager.hpp
 * @brief     Class to execute all batch functionality (header file).
 * @details
 *
 * Provides the "batch" command functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEBATCHMANAGER_H
#define WIBBLEBATCHMANAGER_H

#include "wibbleexecutor.hpp"
#include "cmd_batch.hpp"

namespace wibble
{
    class WibbleBatchManager
    {
    private:
        std::string logfile;
        void        _append_to_logfile(WibbleConsole& wc, const std::string& result, const batch_options& BATCH) const;
        std::string _batch_create_node(WibbleConsole& wc, const pkg_options& OPTS, const batch_options& BATCH) const;
        std::string _batch_create_task_group(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const;
        std::string _batch_create_task_item(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const;
        std::string _batch_create_topic_entry(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const;
        std::string _batch_create_jotfile_entry(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const;
        std::string _batch_create_new_topic(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH);
        std::string _batch_create_new_jotfile(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH);
        std::string _batch_node_add_data(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH);
        std::string _batch_task_add_data(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH);
        std::string _batch_topic_add_data(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH);
        std::string _gen_result_timestamp(const bool success) const;
    public:
        void execute(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH, const int action);
    };
}
#endif
