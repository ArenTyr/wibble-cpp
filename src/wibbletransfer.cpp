/**
 * @file      wibbletransfer.cpp
 * @brief     Class to execute all batch functionality.
 * @details
 *
 * Provides the "transfer" command functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibbletransfer.hpp"
#include "wibbleexit.hpp"
#include "wibblejotter.hpp"
#include "wibbletask.hpp"
#include "wibbletopicmanager.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Add entry to the index/inventory of export items
 * @param wc WibbleConsole general console utility object
 * @param path destination root directory for exported content
 */
bool wibble::WibbleTransfer::_add_bundle_entry(const std::string& path, const std::string& type,
                                               const std::string& desc, const std::string& id)
{
    const std::string& bundle_file = path + "/wibble_bundle/bundle.wib";
    const std::string& entry_line = type + "|" + desc + "|" + id + '\n';
    return WibbleIO::write_out_file_append(entry_line, bundle_file);
    /*
   > cat /tmp/export/wibble_bundle/bundle.wib
     TOPIC|wdir|WDIR|TESTING_WDIR_TOPIC_20240616132455
     BIN_MAP|wdir|Binary mapping|mm:freeplane ___FILE___ > /dev/null 2>&1 &, wdir:wdir%wdir ___FILE___ view
     NODE|txt|foo|719e51d3-d58c-4a88-94b6-53718d69cb40
     NODE|wdir|foobar|00fbbfdf-1494-4e57-a032-4cb83e4a9487
     BIN_MAP|wdir|Binary mapping|mm:freeplane ___FILE___ > /dev/null 2>&1 &, wdir:wdir%wdir ___FILE___ view
     TASK_GROUP|Tasks|foo group [ 2024-06-16 13:33:04 ]|20240616133304_9634c0f8

     fields: type | extension (if relevant) | description/name | id/data
     */
}

/**
 * @brief Add everything to import set
 */
void wibble::WibbleTransfer::_add_all_to_set()
{
    _add_nodes_to_set();
    _add_tgs_to_set();
    _add_topics_to_set();
    _add_bin_maps_to_set();
}

/**
 * @brief Add Nodes to import set
 */
void wibble::WibbleTransfer::_add_nodes_to_set() { payload.node_count = node_count; }

/**
 * @brief Add Task Groups to import set
 */
void wibble::WibbleTransfer::_add_tgs_to_set() { payload.tg_count = tg_count; }

/**
 * @brief Add Topics to import set
 */
void wibble::WibbleTransfer::_add_topics_to_set() { payload.topic_count = topic_count; }

/**
 * @brief Add Binary Mappings to import set
 */
void wibble::WibbleTransfer::_add_bin_maps_to_set() { payload.bm_count = bm_count; }


/**
 * @brief Display current import set
 * @param wc WibbleConsole general console utility object
 * @param path destination root directory for exported content
 */
void wibble::WibbleTransfer::_display_import_set(WibbleConsole& wc)
{
    wc.console_write_yellow_bold("Nodes to import:           ");
    payload.node_count == 0 ? wc.console_write(std::to_string(payload.node_count)) : wc.console_write_green(std::to_string(payload.node_count) + "\n");
    wc.console_write_yellow_bold("Task Groups to import:     ");
    payload.tg_count == 0 ? wc.console_write(std::to_string(payload.tg_count)) : wc.console_write_green(std::to_string(payload.tg_count) + "\n");
    wc.console_write_yellow_bold("Topics to import:          ");
    payload.topic_count == 0 ? wc.console_write(std::to_string(payload.topic_count)) : wc.console_write_green(std::to_string(payload.topic_count) + "\n");
    wc.console_write_yellow_bold("Binary Mappings to import: ");
    payload.bm_count == 0 ? wc.console_write(std::to_string(payload.bm_count)) : wc.console_write_green(std::to_string(payload.bm_count) + "\n");
    wc.console_write_hz_divider();
}


/**
 * @brief Map a transfer item string into the x_item_type enum
 * @param token string containing value to map
 */
wibble::WibbleTransfer::x_item_type wibble::WibbleTransfer::_map_x_type(const std::string& token) const
{
    if     (token == "NODE")       { return WibbleTransfer::x_item_type::NODE;       }
    else if(token == "TASK_GROUP") { return WibbleTransfer::x_item_type::TASK_GROUP; }
    else if(token == "TOPIC")      { return WibbleTransfer::x_item_type::TOPIC;      }
    else if(token == "BIN_MAP")    { return WibbleTransfer::x_item_type::BIN_MAP;    }
    else                           { return WibbleTransfer::x_item_type::ERR;        }
}

/**
 * @brief Parse the bundle definition file to determine contents/populate vector
 * @param wc WibbleConsole general console utility object
 * @param bundle_def path to defined bundle.wib file
 */
bool wibble::WibbleTransfer::_parse_bundle_file(WibbleConsole& wc, const std::string& bundle_def)
{
    if(! std::filesystem::exists(std::filesystem::path(bundle_def)))
    {
        std::cout << "bundle_def is '" << bundle_def << "' \n";
        wc.console_write_error_red("Unable to open/read bundle file. Aborting.");
        return false;
    }

    try
    {
        std::stringstream input_ss{WibbleIO::read_file_into_stringstream(bundle_def)};
        std::string line;
        while(std::getline(input_ss, line, '\n'))
        {
            auto type_sep = line.find("|");
            auto ext_sep  = line.find("|", type_sep + 1);
            auto desc_sep = line.find("|", ext_sep + 1);
            //auto id_sep   = line.find("|", desc_sep + 1);

            //std::cout << "DEBUG: " << type_sep << " , " << ext_sep << " , " << desc_sep << " , " << /*id_sep << */'\n';
            if(type_sep == std::string::npos || ext_sep == std::string::npos || desc_sep == std::string::npos)// || id_sep == std::string::npos)
            {
                wc.console_write_error_red("Malformed bundle file");
                throw std::invalid_argument("Malfomed line");
            }
            const std::string type = line.substr(0, type_sep);
            const std::string ext  = line.substr(type_sep + 1, ext_sep - type_sep - 1);
            const std::string desc = line.substr(ext_sep + 1, desc_sep - ext_sep - 1);
            const std::string id   = line.substr(desc_sep + 1, line.length() - desc_sep - 1);

            xfer_item item{_map_x_type(type), ext, desc, id};
            xfer_bundle.push_back(item);
        }

        // debugging
        //for(auto const& item: xfer_bundle)
        //{
        //    std::cout << "---";
        //    std::cout << "Type: " << _render_x_type(item.type) << '\n';
        //    std::cout << "Ext : " << item.ext << '\n';
        //    std::cout << "Desc: " << item.desc << '\n';
        //    std::cout << "Id  : " << item.id << '\n';
        //}
        return true;
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error parsing bundle file. Aborting.");
        return false;
    }
}

/**
 * @brief Ensure filesystem export path is sane
 * @param wc WibbleConsole general console utility object
 * @param path destination root directory for exported content
 */
void wibble::WibbleTransfer::_prep_path(WibbleConsole& wc, std::string& path)
{
    WibbleUtility::expand_tilde(path);
    WibbleUtility::remove_trailing_slash(path);
    wc.console_write_green(ARW + "Defined export location: " + path + "\n");
}

/**
 * @brief Display the current contents of an exported or decompressed bundle
 * @param wc WibbleConsole general console utility object
 */
void wibble::WibbleTransfer::_pretty_print_bundle_contents(WibbleConsole& wc)
{
    wc.console_write("");
    wc.console_write_green_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
    // 1. Nodes
    wc.console_write_header("NODES");
    auto node_count = 0;
    for(auto const& item: xfer_bundle)
    {
        if(item.type == WibbleTransfer::x_item_type::NODE)
        {
            wc.console_write_yellow_bold("Filetype  : ");
            wc.console_write(item.ext);
            wc.console_write_yellow_bold("Title     : ");
            wc.console_write(item.desc);
            wc.console_write_yellow_bold("Id        : ");
            wc.console_write(item.id);
            wc.console_write_hz_divider();
            ++node_count;
        }
    }
    if(node_count == 0) { wc.console_write(ARW + "No Nodes present in bundle.");                                      }
    else                { wc.console_write_green(ARW + std::to_string(node_count) + " Node(s) present in bundle.\n"); }
    wc.console_write_hz_heavy_divider();
    wc.console_write("");

    // 2. Task Groups
    wc.console_write_header("TK GROUPS");
    auto tg_count = 0;
    for(auto const& item: xfer_bundle)
    {
        if(item.type == WibbleTransfer::x_item_type::TASK_GROUP)
        {
            wc.console_write_yellow_bold("Group name: ");
            wc.console_write(item.desc);
            wc.console_write_yellow_bold("Group Id  : ");
            wc.console_write(item.id);
            wc.console_write_hz_divider();
            ++tg_count;
        }
    }
    if(tg_count == 0) { wc.console_write(ARW + "No Task Groups present in bundle.");                                      }
    else              { wc.console_write_green(ARW + std::to_string(tg_count) + " Task Group(s) present in bundle.\n"); }
    wc.console_write_hz_heavy_divider();
    wc.console_write("");

    // 3. Nodes
    wc.console_write_header("TOPICS ");
    auto topic_count = 0;
    for(auto const& item: xfer_bundle)
    {
        if(item.type == WibbleTransfer::x_item_type::TOPIC)
        {
            wc.console_write_yellow_bold("Filetype  : ");
            wc.console_write(item.ext);
            wc.console_write_yellow_bold("Topic Id  : ");
            wc.console_write(item.id);
            wc.console_write_hz_divider();
            ++topic_count;
        }
    }
    if(topic_count == 0) { wc.console_write(ARW + "No Topics present in bundle.");                                      }
    else                 { wc.console_write_green(ARW + std::to_string(topic_count) + " Topic(s) present in bundle.\n"); }
    wc.console_write_hz_heavy_divider();
    wc.console_write("");

    // 4. Binary Mappings
    wc.console_write_header("BIN MAP");
    auto bin_count = 0;
    std::set<std::string> bin_type;
    for(auto const& item: xfer_bundle) { if(item.type == WibbleTransfer::x_item_type::BIN_MAP) { bin_type.insert(item.ext); } }
    for(auto const& item: bin_type)
    {
        wc.console_write_yellow_bold("Filetype  : ");
        wc.console_write(item);
        wc.console_write_hz_divider();
        ++bin_count;
    }
    if(topic_count == 0) { wc.console_write(ARW + "No Binary Mappings present in bundle.");                                      }
    else                 { wc.console_write_green(ARW + std::to_string(bin_count) + " distinct Binary Mapping(s) present in bundle.\n"); }
    wc.console_write_hz_heavy_divider();
    wc.console_write("");

    wc.console_write_success_green("Bundle data:");
    wc.console_write("");
    wc.console_write_green_bold(ARW + std::to_string(node_count) + " Node(s)\n");
    wc.console_write_green_bold(ARW + std::to_string(tg_count) + " Task Group(s)\n");
    wc.console_write_green_bold(ARW + std::to_string(topic_count) + " Topic(s)\n");
    wc.console_write_green_bold(ARW + std::to_string(bin_count) + " Binary Type mapping(s).\n");
    wc.console_write_green_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
}

/**
 * @brief Set the counts of the bundle content
 * @param wc WibbleConsole general console utility object
 */
void wibble::WibbleTransfer::_set_bundle_counts()
{
    for(auto const& item: xfer_bundle)
    {
        if(item.type == WibbleTransfer::x_item_type::NODE)       { node_count++;  }
        if(item.type == WibbleTransfer::x_item_type::TASK_GROUP) { tg_count++;    }
        if(item.type == WibbleTransfer::x_item_type::TOPIC)      { topic_count++; }
        if(item.type == WibbleTransfer::x_item_type::BIN_MAP)    { bm_count++;    }
    }
}

/**
 * @brief Map a transfer item type to the string serialization
 * @param type enumerated transfer type
 */
std::string wibble::WibbleTransfer::_render_x_type(const WibbleTransfer::x_item_type& type) const
{
    switch(type)
    {
    case WibbleTransfer::x_item_type::NODE:
        return "NODE";
    case WibbleTransfer::x_item_type::TASK_GROUP:
        return "TASK_GROUP";
    case WibbleTransfer::x_item_type::TOPIC:
        return "TOPIC";
    case WibbleTransfer::x_item_type::BIN_MAP:
        return "BIN_MAP";
    case WibbleTransfer::x_item_type::ERR:
    default:
        return "ERROR";
    }
}

/**
 * @brief Generate a Wibble bundle (tar.xz file) containing exported content
 * @param wc WibbleConsole general console utility object
 * @param path destination root directory for exported content
 */
bool wibble::WibbleTransfer::create_bundle(WibbleConsole& wc, const std::string& path)
{
    if(_parse_bundle_file(wc, path + "/wibble_bundle/bundle.wib"))
    {
        _pretty_print_bundle_contents(wc);
        wc.console_write("");
        const std::string resp = wc.console_write_prompt("Create bundle from above data? (y/n) > ", "", "");
        if(resp != "Y" && resp != "y") { wc.console_write_error_red("User aborted."); return false; }

        const std::string ark_name = "wibble_bundle_" + WibbleRecord::generate_timestamp("") + ".wibb";
        const std::string cmd = "cd " + path + " && tar -cvJf " + ark_name + " wibble_bundle/ && sha256sum "
                                + ark_name + " > " + ark_name + ".sha256";
        wc.console_write_hz_heavy_divider();
        auto exec_result = std::system(cmd.c_str());
        wc.console_write_hz_heavy_divider();
        if(exec_result == 0)
        {
            wc.console_write("");
            wc.console_write_success_green("Bundle archive + hash successfully generated under " + path);
            wc.console_write_green(ARW + "Bundle archive : "); wc.console_write(path + "/" + ark_name);
            wc.console_write_green(ARW + "Bundle checksum: ");  wc.console_write(path + "/" + ark_name + ".sha256");
            return true;
        }
        else
        {
            wc.console_write_error_red("Error generating bundle archive. Check output.");
            return false;
        }
    }
    else { return false; }
}


/**
 * @brief Exported a binary mapping into bundle definition
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param path destination root directory for exported content
 */
bool wibble::WibbleTransfer::export_binary_mapping(WibbleConsole& wc, pkg_options& OPTS, const std::string& path)
{
    WibbleExecutor wib_e{OPTS};
    std::string export_path = path;
    _prep_path(wc, export_path);
    const auto bin_map = WibbleUtility::export_binary_tool_mapping(wc, wib_e.get_bin_mapping());

    wc.console_write_header("BIN MAP");
    for(auto i = 1UL; i <= bin_map.size(); i++)
    {
        wc.console_write_yellow_bold(std::to_string(i));
        wc.console_write(". " + bin_map.at(i).first);
    }

    wc.console_write_yellow_bold("\nPlease indicate the mappings you wish to export.");
    wc.console_write_yellow_bold("\nInvalid entries will be ignored.");
    wc.console_write_yellow_bold("\nInput multiple entries separated with a comma (e.g. '1,3').\n\n");
    auto bin_selection = wc.console_write_prompt("> ", "", "");
    wc.trim(bin_selection);
    if(bin_selection == "")
    {
        wc.console_write_error_red("Empty input. Aborting.");
        return false;
    }

    std::set<int> mapping_choices;
    std::string token;
    std::stringstream input_map{bin_selection};
    {
        while(std::getline(input_map, token, ','))
        {
            wc.trim(token);
            try
            {
                const auto map_num = std::stoi(token);
                mapping_choices.insert(map_num);
            }
            catch(const std::invalid_argument& err) {}
        }
    }

    std::stringstream().swap(input_map);

    if(mapping_choices.empty())
    {
        wc.console_write_error_red("Empty/no mappings selected. Aborting.");
        return false;
    }

    wc.console_write_yellow_bold("\nSelected mappings:\n\n");
    std::set<int> valid_entries;
    for(auto const& entry: mapping_choices)
    {
        if(bin_map.count(entry) > 0)
        {
            wc.console_write(std::to_string(entry) + ". " + bin_map.at(entry).first);
            valid_entries.insert(entry);
        }
    }

    if(valid_entries.empty())
    {
        wc.console_write("<EMPTY>\n");
        wc.console_write_error_red("No valid mappings selected. Aborting.");
        return false;
    }

    wc.console_write_yellow_bold("\nProceed with exporting selected binary mappings?\n");
    wc.console_write_yellow("\n(Note: This will overwrite any previous binary mapping\nexport definition into this bundle).\n\n");
    auto proceed_prompt = wc.console_write_prompt("(y/n) > ", "", "");
    wc.trim(proceed_prompt);
    if(proceed_prompt != "y" && proceed_prompt != "Y")
    {
        wc.console_write_error_red("User cancelled.");
        return false;
    }

    const auto bin_template_dir = export_path + "/wibble_bundle/bin_default";
    // ensure paths are prepared
    if(! WibbleIO::create_path(bin_template_dir))
    {
        wc.console_write_error_red("I/O error creating output directory.");
        return false;
    }

    try
    {
        std::string binmap_bundle_lines = "";
        std::string map_string = "";
        for(const auto& entry: valid_entries)
        {
            binmap_bundle_lines.append("BIN_MAP|" + bin_map.at(entry).first + "|Binary file mapping for '" +
                                            bin_map.at(entry).first + "' filetype|No unique ID\n");
            // copy binary template if it exists (should do)
            if(std::filesystem::exists(std::filesystem::path(OPTS.paths.wibble_store + "/bin_default/default" + bin_map.at(entry).first)))
            {
                auto cp_template = WibbleIO::wcopy_file(OPTS.paths.wibble_store + "/bin_default/default" + bin_map.at(entry).first,
                                                        export_path + "/wibble_bundle/bin_default" + bin_map.at(entry).first);
                if(cp_template)
                {
                    wc.console_write_yellow(ARW + "Default binary template copied for filetype '" + bin_map.at(entry).first + "'");
                }
            }
            if(bin_map.at(entry).second.second == NONE_IDENT)
            {
                map_string.append(bin_map.at(entry).first + ":" + bin_map.at(entry).second.first + ", ");
            }
            else
            {
                map_string.append(bin_map.at(entry).first + ":");
                map_string.append(bin_map.at(entry).second.first + "%");
                map_string.append(bin_map.at(entry).second.second + ", ");
            }
        }
        map_string = map_string.substr(0, map_string.length() - 2);// + "\"";
        wc.console_write_green("\n" + ARW + "Exported binary mapping:\n\n" + "binary_mapping = \"" + map_string + "\"\n\n");
        std::ofstream(export_path + "/wibble_bundle/bin_map.wib").write(map_string.c_str(), map_string.length());
        wc.console_write_success_green("Binary mapping exported into bundle.");

        // clear out any pre-existing BIN_MAP entries
        wc.silence_output();
        WibbleUtility::remove_or_update_line_from_file(OPTS.paths.tmp_dir, wc, "BIN_MAP|", export_path + "/wibble_bundle/bundle.wib", true, "");
        wc.enable_output();
        return WibbleIO::write_out_file_append(binmap_bundle_lines, export_path + "/wibble_bundle/bundle.wib");
    }
    catch(const std::exception& err)
    {
        wc.console_write_error_red("Error exporting binary definition.");
        return false;
    }
}


/**
 * @brief Export an individual specific Node, or selection of Nodes (Node(s) + all data)
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param SEARCH search parameters to identify candidate Nodes
 * @param path destination root directory for exported content
 * @param archived flag controlling whether to look in archived database for Node(s)
 */
bool wibble::WibbleTransfer::export_nodes(WibbleConsole& wc, pkg_options& OPTS, search_filters& SEARCH, const std::string& path, const bool archived_node)
{
    WibbleExecutor wib_e{OPTS};
    std::string export_path = path;
    _prep_path(wc, export_path);

    WibResultList records = WibbleUtility::get_records_multi_interactive(wc, wib_e, SEARCH);
    if(records->empty()){ wc.console_write(""); wc.console_write_error_red("Invalid Node selection."); return false; }
    else
    {
        unsigned long tot = records->size();
        unsigned long counter = 0;
        for(auto const& r: *records)
        {
            r.pretty_print_record_condensed(counter, tot);
            ++counter;
        }
        wc.console_write("");
        const std::string resp = wc.console_write_prompt("Export the above " + std::to_string(tot) + " Node(s)? (y/n) > ", "", "");
        wc.console_write("");
        if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); return false; }
        else
        {
            for(auto const& r: *records)
            {
                wc.console_write_hz_divider();
                if(wib_e.export_node(r, export_path, archived_node))
                {
                    wc.silence_output();
                    _add_bundle_entry(path, "NODE|" + r.get_type(), r.get_title(), r.get_id());
                    if(r.get_bin_r())
                    {
                        _add_bundle_entry(path, "BIN_MAP|" + r.get_type(), "Binary mapping", OPTS.exec.bin_mapping);
                    }
                    wc.enable_output();
                }
            }
        }
        return true;
    }
}


/**
 * @brief Export an entire Task group (tasks + extended data)
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param SEARCH search parameters to identify candidate Nodes
 * @param path destination root directory for exported content
 */
bool wibble::WibbleTransfer::export_task_group(WibbleConsole& wc, pkg_options& OPTS, const std::string& path)
{
    // 1. Copy task group index file across
    // 2. Collect task list files from task group file
    // 3. Build output content for selected tasks into output task lists
    // 4. Prepare output directory structure
    // 5. Output/serialize these group tasks (only) to relevant task list files
    // 6. Copy any relevant task data directories where they exist

    const bool suppress = true;
    WibbleTask wtsk{OPTS.paths.tasks_dir, OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping, suppress};
    wc.console_write_header("EXPORT");
    wc.console_write("");
    WibbleTask::task_group_set tgs = wtsk.export_task_group();

    auto open_tasks = tgs.tasks.first.size();
    auto closed_tasks = tgs.tasks.second.size();

    if((open_tasks + closed_tasks) == 0)
    { wc.console_write_error_red("Task group '" + tgs.tg.group_name + "' is empty. Nothing to export."); return false; }

    wc.console_write_yellow_bold("Task Group Name: ");
    wc.console_write(tgs.tg.group_name);
    wc.console_write_yellow_bold("Task Group ID  : ");
    wc.console_write(tgs.tg.group_id);
    wc.console_write_yellow_bold("Open Tasks     : ");
    wc.console_write(std::to_string(open_tasks));
    wc.console_write_yellow_bold("Closed Tasks   : ");
    wc.console_write(std::to_string(closed_tasks));

    wc.console_write("");
    const std::string resp = wc.console_write_prompt("Export above Task Group ? (y/n) > ", "", "");
    if(resp != "Y" && resp != "y") { wc.console_write_error_red("User cancelled."); return false; }

    // 1. Ensure the main task group index file exists, then copy
    const std::string tgYYYY = tgs.tg.group_id.substr(0,4);
    const std::string task_group_src_fl = OPTS.paths.tasks_dir + "/open_tasks/task_groups/" + tgYYYY + "/" + tgs.tg.fn;
    const std::string task_group_exp_dir = path + "/wibble_bundle/tasks/open_tasks/task_groups/" + tgYYYY + "/";
    const std::string task_group_exp_fl = task_group_exp_dir + tgs.tg.fn;
    wc.console_write_yellow(ARW + "[VERIFY] Inspecting for Task group file             : " + task_group_src_fl + "\n");
    wc.console_write_yellow(ARW + "[VERIFY] Check no previous Task group file exists at: " + task_group_exp_fl + "\n");
    wc.console_write_hz_divider();

    if(! std::filesystem::exists(task_group_src_fl))
    {
        wc.console_write_error_red("Task group file unreadable/does not exit. Aborting.");
        return false;
    }

    if(std::filesystem::exists(task_group_exp_fl))
    {
        wc.console_write_success_green("Task group has already been exported into target directory.");
        return true;
    }

    // 1/6. Copy index file
    if(! (WibbleIO::create_path(task_group_exp_dir) && WibbleIO::wcopy_file(task_group_src_fl, task_group_exp_fl)))
    { wc.console_write_error_red("Error copying task group index. Aborting."); return false; }

    // 2/6. Generate list of output category "task_lists" files
    std::set<std::string> of_files;
    std::set<std::string> task_extended_dir_list;
    std::set<std::string> task_data_par_dirs;
    for(auto const& item: tgs.tasks.first)
    {
        const std::string idYYYY = item.task_id.substr(0,4);
        of_files.insert(idYYYY + "_" + item.category);
        task_extended_dir_list.insert("task_data/" + idYYYY + "/" + item.task_id);
        task_data_par_dirs.insert("task_data/" + idYYYY);
    }
    for(auto const& item: tgs.tasks.second)
    {
        const std::string idYYYY = item.task_id.substr(0,4);
        of_files.insert(idYYYY + "_" + item.category);
        task_extended_dir_list.insert("task_data/" + idYYYY + "/" + item.task_id);
        task_data_par_dirs.insert("task_data/" + idYYYY);
    }

    // 3/6. Build output content of each task_lists file from stored task node content
    // We only want to excise the task items that pertain to the group under export
    std::map<std::string, std::string> task_lists_open;
    std::map<std::string, std::string> task_lists_closed;
    for(auto const& cat: of_files)
    {
        for(auto const& item: tgs.tasks.first)
        {
            if((item.task_id.substr(0,4) + "_" + item.category) == cat)
            {
                task_lists_open[cat] += wtsk.serialize_task_entry(item);
            }
        }
        for(auto const& item: tgs.tasks.second)
        {
            if((item.task_id.substr(0,4) + "_" + item.category) == cat)
            {
                task_lists_closed[cat] += wtsk.serialize_task_entry(item);
            }
        }
    }

    // 4/6. Now write out the content
    // 4a) Open tasks
    for(auto const& [cat, f_content]: task_lists_open)
    {
        const std::string dir_path = path + "/wibble_bundle/tasks/open_tasks/task_lists/" + cat.substr(0, 4) + "/";
        const std::string of_file = dir_path + cat + ".wib";
        if(! (WibbleIO::create_path(dir_path) && WibbleIO::write_out_file_append(f_content, of_file)))
        { wc.console_write_error_red("Error copying task list file. Aborting."); return false; }
    }

    // 4b) Open tasks
    for(auto const& [cat, f_content]: task_lists_closed)
    {
        const std::string dir_path = path + "/wibble_bundle/tasks/closed_tasks/task_lists/" + cat.substr(0, 4) + "/";
        const std::string of_file = dir_path + cat + ".wib";
        if(! (WibbleIO::create_path(dir_path) && WibbleIO::write_out_file_append(f_content, of_file)))
        { wc.console_write_error_red("Error copying task list file. Aborting."); return false; }
    }

    // 5/6. Ensure all parent data dirs exist (in case of multi-year)
    for(auto const& item: task_data_par_dirs)
    {
        if(!(WibbleIO::create_path(path + "/wibble_bundle/tasks/" + item)))
        { wc.console_write_error_red("Error preparing output directory.");  return false; }
    }

    // 6/6. Copy existing output task_data directories to destination
    for(auto const& item: task_extended_dir_list)
    {
        const std::string dir_copy = OPTS.paths.tasks_dir + "/" + item;
        if(std::filesystem::exists(std::filesystem::path(dir_copy)))
        {
            if(!(WibbleIO::wcopy_file(OPTS.paths.tasks_dir + "/" + item, path + "/wibble_bundle/tasks/" + item)))
            { wc.console_write_error_red("Error preparing output directory.");  return false; }

        }
    }

    wc.silence_output();
    //_add_bundle_entry(path, "TASK_GROUP|Tasks", tgs.tg.group_name, tgs.tg.group_id);
    _add_bundle_entry(path, "TASK_GROUP|Tasks", tgs.tg.fn, tgs.tg.group_id);
    wc.enable_output();
    wc.console_write_hz_divider();
    wc.console_write_success_green("Task group '" + tgs.tg.group_name + "' exported successfully.");
    return true;
}

/**
 * @brief Export an entire Topic (entries + data)
 * @param OPTS package/general configuration options struct
 * @param SEARCH search parameters to identify candidate Nodes
 * @param path destination root directory for exported content
 */
bool wibble::WibbleTransfer::export_topic(WibbleConsole& wc, pkg_options& OPTS, const std::string& path)
{
   std::string export_path = path;
   _prep_path(wc, export_path);
   WibbleTopics wt{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
   wt.accumulate_topic_map(wc, OPTS.paths.topics_dir);
   WibbleTopicManager wtm{OPTS.exec.bin_mapping};
   topic_def topic = wtm.set_ensure_topic_or_exit(wc, wt);

   const std::string resp = wc.console_write_prompt("Export Topic '" + topic.name + "' with ID '" + topic.id + "' (y/n) > ", "", "");
   if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); return false; }
   wc.console_write_hz_divider();

   // check paths
   const std::string topic_index_par = OPTS.paths.topics_dir + "/topic_indexes";
   const std::string topic_def_par = OPTS.paths.topics_dir + "/topic_defs";

   const std::string data_store_par = OPTS.paths.topics_dir + "/data_store/" + topic.category;
   const std::string norm_store_par = OPTS.paths.topics_dir + "/store/" + topic.category;

   const std::string data_store_path = data_store_par + "/" + topic.id;
   const std::string data_store_indx = topic_index_par + "/" + topic.id + ".data.wib";

   const std::string norm_store_path = norm_store_par + "/" + topic.id;
   const std::string norm_store_indx = topic_index_par + "/" + topic.id + ".wib";

   const std::string topic_def_file = topic_def_par + "/" + topic.category + "_" + topic.name + ".wib";

   // optional default template file; may or may not exist
   const std::string topic_template_file = OPTS.paths.templates_dir + "/" + topic.id;

   wc.console_write_yellow(ARW + "[VERIFY] Inspecting for Topic definition file: " + topic_def_file + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Inspecting for Topic data store at  : " + data_store_path + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Inspecting for Topic data index at  : " + data_store_indx + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Inspecting for Topic entries path   : " + norm_store_path + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Inspecting for Topic entries index  : " + norm_store_indx + "\n");

   // abort export if not Topic definition file present
   if(! std::filesystem::exists(std::filesystem::path(topic_def_file)))
   { wc.console_write_error_red("Topic definition file missing. Aborting"); return false; }

   bool copy_data = false;
   bool copy_entries = false;

   // check data path + index
   if(std::filesystem::exists(std::filesystem::path(data_store_path)) && std::filesystem::exists(std::filesystem::path(data_store_indx)) )
   {
       copy_data = true;
   }

   // check normal path + index
   if(std::filesystem::exists(std::filesystem::path(norm_store_path)) && std::filesystem::exists(std::filesystem::path(norm_store_indx)) )
   {
       copy_entries = true;
   }

   //check that it hasn't already been exported
   const std::string topic_base_export_path = export_path + "/wibble_bundle/topics";
   const std::string topic_base_index_path  = topic_base_export_path + "/topic_indexes";
   const std::string topic_base_def_path  = topic_base_export_path + "/topic_defs";
   const std::string topic_data_export_par  = topic_base_export_path + "/data_store/" + topic.category;
   const std::string topic_norm_export_par  = topic_base_export_path + "/store/" + topic.category;

   const std::string topic_data_export_path = topic_data_export_par + "/" + topic.id;
   const std::string topic_norm_export_path = topic_norm_export_par + "/" + topic.id;

   const std::string topic_data_export_indx = topic_base_index_path + "/" + topic.id + ".data.wib";
   const std::string topic_norm_export_indx = topic_base_index_path + "/" + topic.id + ".wib";

   const std::string topic_export_def_file  =  topic_base_def_path + "/" + topic.category + "_" + topic.name + ".wib";

   const std::string topic_export_template_path = export_path + "/wibble_bundle/templates";
   const std::string topic_export_template_file = topic_export_template_path + "/" + topic.id;

   wc.console_write_hz_divider();
   wc.console_write_yellow(ARW + "[VERIFY] Check no previous data path exists at at      : " + topic_data_export_path + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Check no previous entries path exists at at   : " + topic_norm_export_path + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Check no previous data index exists at at     : " + topic_data_export_indx + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Check no previous entries index exists at at  : " + topic_norm_export_indx + "\n");
   wc.console_write_yellow(ARW + "[VERIFY] Check no previous definition file exists at at: " + topic_norm_export_indx + "\n");
   wc.console_write_hz_divider();
   if(std::filesystem::exists(std::filesystem::path(topic_data_export_path)) ||
      std::filesystem::exists(std::filesystem::path(topic_norm_export_path)) ||
      std::filesystem::exists(std::filesystem::path(topic_data_export_indx)) ||
      std::filesystem::exists(std::filesystem::path(topic_norm_export_indx)) ||
      std::filesystem::exists(std::filesystem::path(topic_export_def_file)))

   {
       wc.console_write("");
       wc.console_write_success_green("Topic with ID " + topic.id + " already exported into destination.");
       return false;
   }

   if(copy_data)    { wc.console_write_green(ARW + "[EXPORT] Topic has valid data files + index.\n"); }
   if(copy_entries) { wc.console_write_green(ARW + "[EXPORT] Topic has valid entries + index.\n");    }

   bool valid_data_copy = false;
   bool valid_ent_copy = false;
   bool setup_index_path = true;
   bool setup_def_file = true;

   // if nothing to do...
   if(! copy_data && ! copy_entries) { wc.console_write_error_red("Empty Topic. Nothing to export."); return false; }
   else
   {
        bool valid_def_path = true;
       // 1. Ensure topic definition is copied
       if(! std::filesystem::exists(std::filesystem::path(topic_base_def_path)))
       {
           //std::cout << "Creating dir: " << topic_base_def_path << "\n";
           valid_def_path = WibbleIO::create_path(topic_export_template_path);
           valid_def_path = WibbleIO::create_path(topic_base_def_path);
       }

       if(valid_def_path)
       {
           //std::cout << "Copying definition file  " << topic_def_file << " TO " << topic_export_def_file << "\n";
           setup_def_file = WibbleIO::wcopy_file(topic_def_file, topic_export_def_file);
           // copy default Topic template, if it exists
           if(std::filesystem::exists(std::filesystem::path(topic_template_file)))
           {
                WibbleIO::wcopy_file(topic_template_file, topic_export_template_file);
           }
       }

       // 2. Ensure index path exists
       if(! std::filesystem::exists(std::filesystem::path(topic_base_index_path)))
       {
           //std::cout << "Creating dir: " << topic_base_index_path << "\n";
           setup_index_path = WibbleIO::create_path(topic_base_index_path);
       }
   }

   if(! setup_def_file || ! setup_index_path) { wc.console_write_error_red("Unable to setup output files. Aborting."); return false; }

   if(copy_data)
   {
       if(! std::filesystem::exists(std::filesystem::path(topic_base_index_path)))
       {
           //std::cout << "Creating dir: " << topic_base_index_path << "\n";
           setup_index_path = WibbleIO::create_path(topic_base_index_path);
       }
       if(setup_index_path)
       {
           //std::cout << "Creating dir: " << topic_data_export_par << "\n";
           bool create_data_path = WibbleIO::create_path(topic_data_export_par);
           bool copy_data_dir = false;
           bool copy_data_idx = false;
           if(create_data_path)
           {
               //std::cout << "Copying data " << data_store_path << " TO " << topic_data_export_path << "\n";
               copy_data_dir = WibbleIO::wcopy_file(data_store_path, topic_data_export_path);
           }

           if(copy_data_dir)
           {
               //std::cout << "Copying data index " << data_store_indx << " TO " << topic_data_export_indx << "\n";
               copy_data_idx = WibbleIO::wcopy_file(data_store_indx, topic_data_export_indx);
           }

           if(copy_data_dir && copy_data_idx)
           {
               wc.console_write_green(ARW + "[EXPORT] Topic data successfully exported.\n");
               valid_data_copy = true;
           }
       }
   }
   else { valid_data_copy = true; } // no data needing copying


   if(copy_entries)
   {
       bool setup_index_path = true;
       if(! std::filesystem::exists(std::filesystem::path(topic_base_index_path)))
       {
           //std::cout << "Creating dir: " << topic_base_index_path << "\n";
           setup_index_path = WibbleIO::create_path(topic_base_index_path);
       }
       if(setup_index_path)
       {
           //std::cout << "Creating dir: " << topic_norm_export_par << "\n";
           bool create_ent_path = WibbleIO::create_path(topic_norm_export_par);
           bool copy_ent_dir = false;
           bool copy_ent_idx = false;
           if(create_ent_path)
           {
               //std::cout << "Copying " << norm_store_path << " TO " << topic_norm_export_path << "\n";
               copy_ent_dir = WibbleIO::wcopy_file(norm_store_path, topic_norm_export_path);
           }

           if(copy_ent_dir)
           {
               //std::cout << "Copying entries index " << norm_store_indx << " TO " << topic_norm_export_indx << "\n";
               copy_ent_idx = WibbleIO::wcopy_file(norm_store_indx, topic_norm_export_indx);
           }

           if(copy_ent_dir && copy_ent_idx)
           {
               wc.console_write_green(ARW + "[EXPORT] Topic data successfully exported.\n");
               valid_ent_copy = true;
               wc.silence_output();
               _add_bundle_entry(path, "TOPIC|" + topic.type, topic.category + ":" + topic.name, topic.id);
               if(WibbleUtility::get_binary_tool(wc, topic.type, OPTS.exec.bin_mapping, true) != "")
               {
                   _add_bundle_entry(path, "BIN_MAP|" + topic.type, "Binary file mapping for '" + topic.type + "' filetype", OPTS.exec.bin_mapping);
               }
               wc.enable_output();
           }
       }
   }
   else { valid_ent_copy = true; } // no entries needing copying

   wc.console_write_hz_divider();
   wc.console_write("");
   if(valid_data_copy && valid_ent_copy) { wc.console_write_success_green("Topic successfully exported."); return true; }
   else                                  { wc.console_write_success_green("Error exporting Topic."); return false;      }
}

/**
 * @brief Interactively import material from a bundle
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param path destination root directory for exported content
 */
bool wibble::WibbleTransfer::import_bundle(WibbleConsole& wc, pkg_options& OPTS, const std::string& bundle_file_or_path)
{
    wc.console_write("\n---\n");
    bool ready = false;
    bool decompress_wibb = true;
    std::string validation_status = "ERR";
    std::string bundle_import_type = "Wibble++ bundle archive (.wibb)";
    const std::string extract_dir = "/.WIB_extract";
    std::string bundle_file;
    std::string par_path;
    std::string bundle_dir = "UNSET";

    if(std::filesystem::is_directory(std::filesystem::path(bundle_file_or_path)))
    {
        if(! std::filesystem::exists(std::filesystem::path(bundle_file_or_path + "/wibble_bundle/")))
        {
            wc.console_write_error_red("No bundle directory 'wibble_bundle/' present in source.");
            wc.console_write_red("Supplied path: " + bundle_file_or_path + "\n");
            return false;
        }
        else
        {
            bundle_import_type = "Wibble++ bundle import directory";
            decompress_wibb = false;
            par_path = bundle_file_or_path;
            bundle_dir = bundle_file_or_path + "/wibble_bundle";
            bundle_file = bundle_file_or_path + "/wibble_bundle/bundle.wib";
            validation_status = "SKIPPED - directory import";
        }
    }

    if(std::filesystem::is_regular_file(std::filesystem::path(bundle_file_or_path)))
    {
        if(std::filesystem::path(bundle_file_or_path).extension() != ".wibb")
        {
            wc.console_write_error_red("Invalid bundle file '" + bundle_file_or_path + "'.");
            wc.console_write_red("Supplied file: " + bundle_file_or_path + "\n");
            return false;
        }
        else
        {
            par_path = WibbleIO::get_parent_dir_path(bundle_file_or_path);
            if(! WibbleIO::create_path(par_path + extract_dir))
            { wc.console_write_error_red("Error creating temporary directory."); return false; }
            const std::string cmd = "tar -Jvxf " + bundle_file_or_path + " --directory " + par_path + extract_dir;
            wc.console_write_yellow(ARW + "Extracting bundle contents...\n");
            wc.console_write("\n---\n");
            if(std::system(cmd.c_str()) != 0) { return false; }
            bundle_dir = par_path + extract_dir + "/wibble_bundle";
            bundle_file = bundle_dir + "/bundle.wib";
            wc.console_write("\n---\n");
        }
    }

    if(decompress_wibb && std::filesystem::exists(std::filesystem::path(bundle_file_or_path + ".sha256")))
    {
        wc.console_write_yellow(ARW + "Checksum file found. Verifying bundle...\n");
        wc.console_write("\n---\n");
        const std::string cmd = "cd " + par_path + " && sha256sum --check " +
                                std::string(std::filesystem::path(bundle_file_or_path).filename()) + ".sha256";
        if(std::system(cmd.c_str()) == 0) { validation_status = "OK"; }
        wc.console_write("\n---\n");
    }
    else
    {
        validation_status = "MISS";
    }

    //bool parsed_bundle = false;
    if(! _parse_bundle_file(wc, bundle_file)) { return false; }
    _set_bundle_counts();

    bool import_success = false;
    while(! ready)
    {
        wc.console_write_header("IMPORT ");
        wc.console_write_yellow("Wibble++ root: " + OPTS.paths.wibble_store + "\n");
        wc.console_write_yellow("Bundle import: " + bundle_file_or_path + "\n");
        wc.console_write_yellow("Bundle type  : " + bundle_import_type + "\n");
        wc.console_write_yellow("Bundle check : ");
        if(validation_status == "ERR")
        {
            wc.console_write_red_bold("CRITICAL - Bad checksum\n");
        }
        else if(validation_status == "OK")
        {
            wc.console_write_green_bold("PASSED\n");
        }
        else if(validation_status == "MISS")
        {
            wc.console_write_red("Skipped - no checksum file\n");
        }
        else
        {
            wc.console_write_yellow(validation_status + "\n");
        }

        wc.console_write_header("SET");
        _display_import_set(wc);
        wc.console_write("");
        wc.console_write_yellow("1) List bundle contents\n");
        wc.console_write_yellow("2) Add ENTIRE bundle to import set\n");
        wc.console_write_yellow("3) Add all Nodes to import set\n");
        wc.console_write_yellow("4) Add all Task Groups to import set\n");
        wc.console_write_yellow("5) Add all Topics to import set\n");
        wc.console_write_yellow("6) Add all Binary Mappings to import set\n");
        wc.console_write_yellow("7) Proceed to import mode selection\n");
        wc.console_write_yellow("8) Quit\n");
        wc.console_write("");

        std::string resp = wc.console_write_prompt("Select option [1-8] > ", "", "");
        wc.trim(resp);
        wc.console_write("");

        if(resp == "1")
        {
            _pretty_print_bundle_contents(wc);
            wc.console_write("");
        }
        if(resp == "2") { _add_all_to_set();                                                    }
        if(resp == "3") { _add_nodes_to_set();                                                  }
        if(resp == "4") { _add_tgs_to_set();                                                    }
        if(resp == "5") { _add_topics_to_set();                                                 }
        if(resp == "6") { _add_bin_maps_to_set();                                               }
        if(resp == "7") { ready = true; import_success = _import_confirm(wc, OPTS, bundle_dir); }
        if(resp == "8") { ready = true;                                                         }
    }

    return import_success;
}

/**
 * @brief Confirm import of designated material from bundle
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param bundle_dir path to the extracted bundle contents
 */
bool wibble::WibbleTransfer::_import_confirm(WibbleConsole& wc, pkg_options& OPTS, const std::string& bundle_dir)
{
    if(payload.node_count == 0 && payload.tg_count == 0 && payload.topic_count == 0 && payload.bm_count == 0)
    {
        wc.console_write("");
        wc.console_write_error_red("Empty import set specified. Nothing to import.");
        return false;
    }

    wc.console_write("");
    wc.console_write_yellow("- "); wc.console_write_green(std::to_string(payload.node_count) + " Node[s]\n");
    wc.console_write_yellow("- "); wc.console_write_green(std::to_string(payload.tg_count) + " Task Group[s]\n");
    wc.console_write_yellow("- "); wc.console_write_green(std::to_string(payload.topic_count) + " Topic[s]\n");
    wc.console_write_yellow("- "); wc.console_write_green(std::to_string(payload.bm_count) + " Binary Mapping[s]\n");
    wc.console_write("");


    wc.console_write_header("STRATEGY ");
    wc.console_write_yellow("1) NEW ONLY. Collisions: only import entirely NEW items.\n");
    wc.console_write_yellow("2) OVERWRITE. Collisions: Delete originals/overwrite.\n");
    wc.console_write_yellow("3) Abort\n");
    wc.console_write("");
    wc.console_write_yellow("NOTE: Option (1) is strongly recommended.\n\n");
    std::string resp = wc.console_write_prompt("Select option [1-3] > ", "", "");
    wc.trim(resp);
    wc.console_write("");

    if(resp == "1") { return _do_import(wc, OPTS, false, bundle_dir); }
    else if(resp == "2")
    {
        wc.console_write_red_bold("                  !!! WARNING !!!\n");
        wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
        wc.console_write_red("Any Nodes, Topics, Task Groups, or Binary Mapping templates\n");
        wc.console_write_red("that ALREADY exist with IDENTICAL IDs/filenames will be deleted\n");
        wc.console_write_red("and REPLACED with the content of the corresponding ID/name\n");
        wc.console_write_red("of the item in this bundle. This includes deletion of any\n");
        wc.console_write_red("data directories/attachments/etc for collided IDs present\n");
        wc.console_write_red("in your current Wibble++ store. Only use this option if\n");
        wc.console_write_red("everything in the imported bundle is canonically NEWER than\n");
        wc.console_write_red("in the existing Wibble++ store, and you wish to overwrite all\n");
        wc.console_write_red("existing content that COLLIDES. (Note that this will NOT effect/\n");
        wc.console_write_red("touch NON-collided content, which will remain as before.)\n");
        wc.console_write("");
        wc.console_write_red("It is strongly recommended you perform a full Wibble++ backup\n");
        wc.console_write_red("BEFORE proceeding with this option to ensure no data is lost.\n");
        wc.console_write("");
        wc.console_write_red("If you are simply trying to add NEW content to your Wibble++\n");
        wc.console_write_red("store, you should generally select option (1) to merge NEW ONLY.\n");
        wc.console_write_red("Content present in the bundle that already exists in the store\n");
        wc.console_write_red("will be safely skipped/ignored.\n");
        wc.console_write("");
        wc.console_write_red("This guarantees that no existing content in your store is\n");
        wc.console_write_red("overwritten with content from the bundle. Use this OVERWRITE\n");
        wc.console_write_red("option if syncing content where the bundle contains newer/updated\n");
        wc.console_write_red("versions of any collided content (should any collisions exist\n");
        wc.console_write_red("and actually occur).\n");
        wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
        wc.console_write("");
        resp = wc.console_write_prompt("Input 'CONFIRM' to proceed > ", "", "");
        wc.trim(resp);
        wc.console_write("");
        if(resp != "CONFIRM")  { wc.console_write_error_red("User cancelled."); return false; }
        else                   { return _do_import(wc, OPTS, true, bundle_dir);               }
    }
    else { wc.console_write_error_red("User cancelled."); return false; }
}

/**
 * @brief Check and copy Node data directory if present/exists
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param bundle_dir path to the extracted bundle contents
 * @param wib_e Execution utility object
 * @param imp_record WibbleRecord whose data we are importing
 * @param data_source path to the data source containing directory
 * @param data_dest_dir path to the data destinationg parent directory
 * @param data_dest_file path to the data destination file/directory
 */
void wibble::WibbleTransfer::propagate_data_if_exists(WibbleConsole &wc, pkg_options &OPTS, const std::string &bundle_dir,
                                                      WibbleExecutor &wib_e, WibbleRecord &imp_record, std::filesystem::path data_source,
                                                      std::filesystem::path data_dest_dir, std::filesystem::path data_dest_file)
{
    // archived parameter has no consequence for data directory/data files
    data_source = wib_e._build_note_path(imp_record.get_id(), imp_record.get_type(), imp_record.get_proj(), false, false, bundle_dir + "/nodes");
    data_dest_file = wib_e._build_note_path(imp_record.get_id(), imp_record.get_type(), imp_record.get_proj(), false, false, OPTS.paths.wibble_store);
    data_dest_dir =  WibbleIO::get_parent_dir_path(std::string(data_dest_file));
    // IF the node has a data directory, copy that too
    if (std::filesystem::exists(data_source))
    {
        wc.console_write_green(ARW + "Node has data directory, importing.\n");

        if (!WibbleIO::create_path(std::string(data_dest_dir)))
        {
            wc.console_write_error_red("Unable to create output data directory.");
            throw std::invalid_argument("Bad insert");
        }
        if (!WibbleIO::wcopy_file(data_source, data_dest_file))
        {
            wc.console_write_error_red("Unable to copy output Node data directory.");
            throw std::invalid_argument("Bad insert");
        }
        wc.console_write_success_green("Node [main]    : " + imp_record.get_id() + " data directory ADDED.");
    }
}

/**
 * @brief Import/merge the contents of a given Topic
 * @param wc WibbleConsole general console utility object
 * @param bundle_dir path to the extracted bundle contents
 * @param overwrite boolean flag controlling whether to overwrite existing Topic entries
 * @param topic_name canonical topic identifyinger name
 * @param OPTS package/general configuration options struct
 * @param data_topic boolean flag indicating whether we are dealing with a data rather than textual Topic
 * @param topic_destination WibbleTopics object representing the destination/target Topic
 * @param topic_source WibbleTopics object representing the source/import Topic
 */
bool wibble::WibbleTransfer::_process_topic(WibbleConsole &wc, const std::string& bundle_dir, const bool overwrite,
                                            const std::string& topic_name, const pkg_options& OPTS, const bool data_topic,
                                            WibbleTopics& topic_destination, WibbleTopics& topic_source)
{
    auto const dest_topic_def   = topic_destination.get_topic_for_name(topic_name);
    auto const source_topic_def = topic_source.get_topic_for_name(topic_name);
    auto const dest_topic_id    = dest_topic_def.id;
    auto const source_topic_id  = source_topic_def.id;
    auto const data_stub        = data_topic == true ? ".data" : "";
    auto const topic_store      = data_topic == true ? "/data_store/" : "/store/";
    auto const topic_stub       = "/topic_indexes/";
    auto const source_topic_fn  = bundle_dir + "/topics" + topic_stub + source_topic_id + data_stub + ".wib";
    auto const dest_topic_fn    = OPTS.paths.topics_dir + topic_stub + dest_topic_id + data_stub + ".wib";
    auto const source_topic_tpl = bundle_dir + "/templates/" + source_topic_id;
    auto const dest_topic_tpl   = OPTS.paths.templates_dir + "/" + source_topic_id;

    if(! (std::filesystem::exists(source_topic_fn) && std::filesystem::exists(dest_topic_fn)))
    {
        // Only show error message for non-data topic, as some Topics will not have any data
        // index if user has not added any data files. Non-data Topics should always have index file
        if(! data_topic)
        {
            wc.console_write_error_red("Unable to parse source or destination Topic index file. Verify Topic has entries.\n");
            return false;
        }
    }

    if(data_topic && ! std::filesystem::exists(source_topic_fn))
    {
        // no data index to import; is fine
        wc.console_write_yellow(ARW + "No data index found/no data attachments. Skipping.\n");
        return true;
    }

    // copy default Topic template file if it now exists and conditions are met
    if(std::filesystem::exists(std::filesystem::path(source_topic_tpl)))
    {
        if(! std::filesystem::exists(std::filesystem::path(dest_topic_tpl)))
        {
            WibbleIO::wcopy_file(source_topic_tpl, dest_topic_tpl);
            wc.console_write_success_green("Default Topic template successfully copied.");
        }
        else if(overwrite)
        {
            WibbleIO::wcopy_file(source_topic_tpl, dest_topic_tpl);
            wc.console_write_success_green("Default Topic template successfully copied.");
        }

    }

    std::stringstream source_topic_ss{wibble::WibbleIO::read_file_into_stringstream(source_topic_fn)};
    std::stringstream dest_topic_ss{wibble::WibbleIO::read_file_into_stringstream(dest_topic_fn)};
    std::string line_source;
    std::string line_dest;

    std::ofstream new_index;
    auto const of_index_fn = dest_topic_fn + ".new";
    new_index.open(of_index_fn, std::ios::out | std::ios::trunc);
    // Pass 1: go through original/destination Topic index, line-by-line,
    // keeping existing unique entries, and overwriting collisions if enabled
    auto ignored_entries = 0;
    auto overwritten_entries = 0;
    while(std::getline(dest_topic_ss, line_dest, '\n'))
    {
        auto sep_index = line_dest.find("|");
        auto search_key = line_dest.substr(0, sep_index);
        // check for key's existence
        if(search_key != "")
        {
            while(std::getline(source_topic_ss, line_source, '\n'))
            {
                if(line_source.find(search_key) != std::string::npos)
                {
                    if(overwrite)
                    {
                        auto src_path = bundle_dir + "/topics" + topic_store + source_topic_def.category
                                                   + "/" + source_topic_def.id + "/" + search_key;
                        auto dest_path = OPTS.paths.topics_dir + topic_store + dest_topic_def.category + "/" + dest_topic_def.id + "/" + search_key;
                        WibbleIO::wcopy_file(src_path, dest_path);
                        new_index << line_source << '\n';
                        ++overwritten_entries;
                        wc.console_write_success_green("Existing Topic entry with ID: '" + search_key + "' OVERWRITTEN into Topic " + dest_topic_def.id);
                    }
                    else
                    {
                        wc.console_write_yellow(ARW + "Skipping existing Topic entry with ID: '" + search_key + "'. (no overwrite)\n");
                        new_index << line_dest + '\n';
                        ++ignored_entries;
                    }
                }
            }
            // clear the eofbit and failbit (if set)
            source_topic_ss.clear();
            source_topic_ss.seekg(0);
        }
        else
        {
            // in case of a broken/null key, do nothing/keep existing line
            new_index << line_dest + '\n';
        }
    }

    // clear the eofbit and failbit (if set)
    dest_topic_ss.clear();
    dest_topic_ss.seekg(0);

    // Pass 2: Go through new source/import Topic, only copying all entirely NEW lines versus original index
    auto new_entries = 0;
    while(std::getline(source_topic_ss, line_source, '\n'))
    {
        bool new_entry = true;
        auto sep_index = line_source.find("|");
        auto search_key = line_source.substr(0, sep_index);
        while(std::getline(dest_topic_ss, line_dest, '\n'))
        {
            if(search_key != "" && line_dest.find(search_key) != std::string::npos)
            {
                new_entry = false;
            }
        }

        if(new_entry)
        {
            // found an entirely new entry, so add it + copy files
            new_index << line_source << '\n';
            auto src_path = bundle_dir + "/topics" + topic_store + source_topic_def.category
                                       + "/" + source_topic_def.id + "/" + search_key;
            auto dest_path = OPTS.paths.topics_dir + topic_store + dest_topic_def.category + "/" + dest_topic_def.id + "/" + search_key;
            wc.console_write_success_green("New Topic entry with ID: '" + search_key + "' IMPORTED into Topic " + dest_topic_def.id);
            WibbleIO::wcopy_file(src_path, dest_path);
            ++new_entries;
        }

        dest_topic_ss.clear();
        dest_topic_ss.seekg(0);
    }

    // tidy up resources
    std::stringstream().swap(dest_topic_ss);
    std::stringstream().swap(source_topic_ss);
    new_index.close();

    if(overwritten_entries == 0 && new_entries == 0)
    {
        wc.console_write_yellow_bold(SUC + "No new Topic entries added/overwritten. Nothing to do.\n");
        if(std::filesystem::exists(of_index_fn)) { std::filesystem::remove(of_index_fn); }
        return true;
    }

    // Overwrite with new index file
    WibbleUtility::create_backup_and_overwrite_file(OPTS.paths.tmp_dir, of_index_fn, dest_topic_fn);
    if(! data_topic)
    {
        wc.console_write_success_green("Topic regular entries for " + source_topic_def.id + " successfully merged into '" + dest_topic_def.id + "'");
    }
    else
    {
        wc.console_write_success_green("Topic data entries for " + source_topic_def.id + " successfully merged into '" + dest_topic_def.id + "'");
    }
    wc.console_write_green("Ignored (no overwrite): " + std::to_string(ignored_entries) + '\n');
    wc.console_write_green("Overwritten           : " + std::to_string(overwritten_entries) + '\n');
    wc.console_write_green("New (added)           : " + std::to_string(new_entries) + '\n');

    return true;
}


//bool wibble::WibbleTransfer::_import_topic(WibbleConsole &wc, pkg_options &OPTS, const std::vector<std::string>& topic_list,
bool wibble::WibbleTransfer::_import_topics(WibbleConsole &wc, pkg_options &OPTS, const bool overwrite,
                                            const std::string &bundle_dir)
{
    // Build up our topic maps
    wc.silence_output();
    WibbleTopics topic_destination{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
    WibbleTopics topic_source{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
    topic_destination.accumulate_topic_map(wc, OPTS.paths.topics_dir, false);
    topic_source.accumulate_topic_map(wc, bundle_dir + "/topics", false);
    wc.enable_output();
    std::vector<std::string> source_topic_names = topic_source.obtain_topic_list();
    std::vector<std::string> dest_topic_names   = topic_destination.obtain_topic_list();
    // Now iterate over the set of topic names, which must be distinct (even though they retain unique IDs)
    for(auto const& i_topic: source_topic_names)
    {
        bool new_topic = true;
        for(auto const& d_topic: dest_topic_names)
        {
            if(d_topic == i_topic)
            {
                new_topic = false;
                wc.console_write_yellow(ARW + "Topic '" + i_topic + "' already exists.\n");
                if(overwrite)
                {
                    wc.console_write_yellow_bold(ARW + "Overwrite mode enabled. Topic '" + i_topic + "' will have any entry collisions OVERWRITTEN.\n");
                }
            }
        }

        if(new_topic)
        {
            wc.console_write_green(ARW + "Importing NEW Topic '" + i_topic + "'.\n");
            // Brand new topic, so easiest to deal with:
            //
            //   i). Copy topic definition + index files
            //  ii). Copy all topic entries
            // iii). Copy all topic data files (if any exist)
            // 1. Obtain the given topic from unique name
            auto source_topic_def = topic_source.get_topic_for_name(i_topic);
            auto source_topic_id = source_topic_def.id;
            // 2. Assign paths
            auto const source_topic_template_file      = bundle_dir + "/templates/" + source_topic_id;
            auto const source_topic_definition         = bundle_dir + "/topics/topic_defs/" + source_topic_def.category + "_" + source_topic_def.name + ".wib";
            auto const source_topic_index_norm_fn      = bundle_dir + "/topics/topic_indexes/" + source_topic_id + ".wib";
            auto const source_topic_index_data_fn      = bundle_dir + "/topics/topic_indexes/" + source_topic_id + ".data.wib";
            auto const source_topic_norm_entries_dir   = bundle_dir + "/topics/store/" + source_topic_def.category + "/" + source_topic_id;
            auto const source_topic_data_entries_dir   = bundle_dir + "/topics/data_store/" + source_topic_def.category + "/" + source_topic_id;
            auto const dest_topic_template_file        = OPTS.paths.templates_dir + "/" + source_topic_id;
            auto const dest_topic_definition           = OPTS.paths.topics_dir + "/topic_defs/" + source_topic_def.category + "_" + source_topic_def.name + ".wib";
            auto const dest_topic_index_norm_fn        = OPTS.paths.topics_dir + "/topic_indexes/" + source_topic_id + ".wib";
            auto const dest_topic_index_data_fn        = OPTS.paths.topics_dir + "/topic_indexes/" + source_topic_id + ".data.wib";
            auto const dest_topic_norm_entries_par_dir = OPTS.paths.topics_dir + "/store/" + source_topic_def.category;
            auto const dest_topic_data_entries_par_dir = OPTS.paths.topics_dir + "/data_store/" + source_topic_def.category;
            auto const dest_topic_norm_entries_dir     = dest_topic_norm_entries_par_dir + "/" + source_topic_id;
            auto const dest_topic_data_entries_dir     = dest_topic_data_entries_par_dir + "/" + source_topic_id;
            auto copy_data = true;

            // 3. Begin the copy process
            if(! std::filesystem::exists(source_topic_definition))
            {
                wc.console_write_error_red("Malformed or empty topic. No definition file found.");
                return false;
            }

            if(! std::filesystem::exists(source_topic_index_norm_fn))
            {
                wc.console_write_error_red("Malformed or empty topic. No index file found.");
                return false;
            }

            if(! std::filesystem::exists(source_topic_index_data_fn) || ! std::filesystem::exists(source_topic_data_entries_dir))
            {
                wc.console_write_yellow(ARW + "Topic has no data entries/data index. No data directory to copy.\n");
                copy_data = false;
            }

            // 4. Ensure paths exist. Some of these should not need creating,
            //    but no harm in guaranteeing they all exist
            auto def_dir    = WibbleIO::create_path(OPTS.paths.topics_dir + "/topic_defs");
            auto index_dir  = WibbleIO::create_path(OPTS.paths.topics_dir + "/topic_indexes");
            auto norm_ready = WibbleIO::create_path(dest_topic_norm_entries_par_dir);
            auto data_ready = WibbleIO::create_path(dest_topic_data_entries_par_dir);

            if(! (def_dir && index_dir && norm_ready && data_ready))
            {
                wc.console_write_error_red(ERR + "Error preparing output topic directories.");
            }

            if(std::filesystem::exists(std::filesystem::path(source_topic_template_file)))
            {
                wc.console_write_yellow(ARW + "Default Topic template found.\n");
                if(WibbleIO::wcopy_file(source_topic_template_file, dest_topic_template_file))
                {
                    wc.console_write_green(ARW + "[IMPORT] Topic default template found and imported.\n");
                }
                else
                {
                    wc.console_write_error_red("Warning: Error copying Topic default template.");
                }
            }

            if(! WibbleIO::wcopy_file(source_topic_definition, dest_topic_definition))
            {
                wc.console_write_error_red("Error copying Topic definition file. Aborting.");
                return false;
            }
            wc.console_write_green(ARW + "[IMPORT] Topic definition imported.\n");

            if(! WibbleIO::wcopy_file(source_topic_index_norm_fn, dest_topic_index_norm_fn))
            {
                wc.console_write_error_red("Error copying Topic index file. Aborting.");
                return false;
            }
            wc.console_write_green(ARW + "[IMPORT] Topic index imported.\n");

            if(! WibbleIO::wcopy_file(source_topic_norm_entries_dir, dest_topic_norm_entries_dir))
            {
                wc.console_write_error_red("Error copying Topic entries directory. Aborting.");
                return false;
            }
            wc.console_write_green(ARW + "[IMPORT] Topic entries imported.\n");

            // 5. If there are data entries, we can simply copy the entire data directory since this is a
            // new topic
            if(copy_data)
            {
                if(! WibbleIO::wcopy_file(source_topic_index_data_fn, dest_topic_index_data_fn))
                {
                    wc.console_write_error_red("Error copying Topic data index file. Aborting.");
                    return false;
                }
                wc.console_write_green(ARW + "[IMPORT] Topic data index imported.\n");

                if(! WibbleIO::wcopy_file(source_topic_data_entries_dir, dest_topic_data_entries_dir))
                {
                    wc.console_write_error_red("Error copying Topic data storage directory. Aborting.");
                    return false;
                }
                wc.console_write_green(ARW + "[IMPORT] Topic data entries imported.\n");
            }

            wc.console_write_success_green("New Topic with ID " + source_topic_id + " imported successfully.");
        }
        else
        {
            // We need to merge into an existing topic, so more complex path...
            auto valid_process_reg  = false;
            auto valid_process_data = false;
            // 1. Process regular index files, merge based on mode
            wc.console_write_green(ARW + "[1/2]. Processing regular indices + textual entries.\n");
            valid_process_reg = _process_topic(wc, bundle_dir, overwrite, i_topic, OPTS, false, topic_destination, topic_source);
            // 2. Process data index files, merge based on mode
            wc.console_write_green(ARW + "[2/2]. Processing data indices + data attachements/files.\n");
            valid_process_data = _process_topic(wc, bundle_dir, overwrite, i_topic, OPTS, true, topic_destination, topic_source);

            // Abort process if something went wrong
            if(! valid_process_reg || ! valid_process_data)
            {
                std::cout << "process_reg " << valid_process_reg << " process_data << " << valid_process_data << '\n';
                return false;
            }
        }
    }

    // If we got here, all iterations across all topics requiring import in bundle succeeded
    return true;
}


/**
 * @brief Import a Node from bundle into Wibble++ store/repository
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param overwrite flag controlling whether to overwrite the original Node
 * @param bundle_dir string containing path to root bundle directory to import from
 */
bool wibble::WibbleTransfer::_import_node(WibbleConsole& wc, pkg_options& OPTS, const xfer_item& item, const bool overwrite, const std::string& bundle_dir)
{
    wc.silence_output();
    search_filters SEARCH_main;
    search_filters SEARCH_ark;
    SEARCH_main.exact = true;
    SEARCH_main.id = item.id;
    WibbleExecutor wib_e{OPTS};
    SEARCH_ark = SEARCH_main;
    SEARCH_ark.archive_db = true;
    try
    {
        // Need to inspect both databases
        const WibResultList chk_main = wib_e.perform_search(SEARCH_main);
        const WibResultList chk_ark = wib_e.perform_search(SEARCH_ark);

        // Logic:
        // 1. If does not exist in either main or archive database, is NEW
        // 2. If exists in main database, is an UPDATE to main database item (EXS_MAIN)
        // 3. If exists in archive database, is an UPDATE to archive database item (EXS_ARK)
        // 4. (Entirely new items will never go straight into archive)
        enum IMPORT_OP
            {
                NEW_MAIN,
                EXS_MAIN,
                EXS_ARK,
                NONE
            };

        IMPORT_OP action{NONE};

        if(chk_main->empty() && chk_ark->empty()) { action = IMPORT_OP::NEW_MAIN; }
        else if(chk_main->size() == 1)            { action = IMPORT_OP::EXS_MAIN; }
        else if(chk_ark->size() == 1 )            { action = IMPORT_OP::EXS_ARK;  }

        search_filters SEARCH_import;
        SEARCH_import.exact = true;
        SEARCH_import.id = item.id;

        WibResultList imp_listing = wib_e.perform_search(SEARCH_import, bundle_dir + "/nodes/nodes.wib");
        WibbleRecord imp_record = imp_listing->at(0);

        if(imp_record.get_id() == "___NULL___") { wc.console_write_error_red("Bad Node import."); throw std::invalid_argument("Bad record."); }

        bool update_db = false;
        std::filesystem::path node_source{WibbleIO::generate_note_path(bundle_dir + "/nodes", imp_record.get_id(), imp_record.get_type(), imp_record.get_proj(), false)};
        std::filesystem::path node_dest_dir;
        std::filesystem::path node_dest_file;

        std::filesystem::path data_source;
        std::filesystem::path data_dest_dir;
        std::filesystem::path data_dest_file;

        wc.enable_output();
        switch(action)
        {
        case NEW_MAIN:
            // Update main database, with NEW Node
            update_db = wib_e.create_node_passthrough(imp_record, false, false);
            if(! update_db)
            { wc.console_write_error_red("Unable to insert Node into main database."); throw std::invalid_argument("Bad insert"); }
            node_dest_file = WibbleIO::generate_note_path(OPTS.paths.wibble_store, imp_record.get_id(), imp_record.get_type(), imp_record.get_proj(), false);
            node_dest_dir = WibbleIO::get_parent_dir_path(std::string(node_dest_file));
            if(! WibbleIO::create_path(std::string(node_dest_dir)))
            { wc.console_write_error_red("Unable to create output directory."); throw std::invalid_argument("Bad insert"); }
            if(! WibbleIO::wcopy_file(node_source, node_dest_file))
            { wc.console_write_error_red("Unable to copy output Node."); throw std::invalid_argument("Bad insert"); }
            wc.console_write_success_green("Node [main]    : " + imp_record.get_id() + " ADDED.");
            propagate_data_if_exists(wc, OPTS, bundle_dir, wib_e, imp_record, data_source, data_dest_dir, data_dest_file);
            return true;
        case EXS_MAIN:
            // Update main database, with EXISTING Node
            if(overwrite)
            {
                update_db = wib_e.create_node_passthrough(imp_record, false, true);
                if(! update_db)
                { wc.console_write_error_red("Unable to update existing Node in main database."); throw std::invalid_argument("Bad insert"); }
                node_dest_file = WibbleIO::generate_note_path(OPTS.paths.wibble_store, imp_record.get_id(), imp_record.get_type(), imp_record.get_proj(), false);
                if(! WibbleIO::wcopy_file(node_source, node_dest_file))
                { wc.console_write_error_red("Unable to copy output Node."); throw std::invalid_argument("Bad insert"); }
                wc.console_write_success_green("Node [main]    : " + imp_record.get_id() + " OVERWRITTEN.");
                propagate_data_if_exists(wc, OPTS, bundle_dir, wib_e, imp_record, data_source, data_dest_dir, data_dest_file);
                return true;
            }
            else
            {
                wc.console_write_yellow_bold(ARW + "Node [main]: " + imp_record.get_id() + " SKIPPED (already exists).\n");
                return true;
            }
        case EXS_ARK:
            // Update archived database, with EXISTING Node
            // Note: path of NEW node -> archived would never exist/make any sense to exist
            if(overwrite)
            {
                update_db = wib_e.create_node_passthrough(imp_record, true, true);
                if(! update_db)
                { wc.console_write_error_red("Unable to update existing Node in archived database."); throw std::invalid_argument("Bad insert"); }
                node_dest_file = WibbleIO::generate_note_path(OPTS.paths.wibble_store, imp_record.get_id(), imp_record.get_type(), imp_record.get_proj(), true);
                if(! WibbleIO::wcopy_file(node_source, node_dest_file))
                { wc.console_write_error_red("Unable to copy output Node."); throw std::invalid_argument("Bad insert"); }
                wc.console_write_success_green("Node [archived]: " + imp_record.get_id() + " OVERWRITTEN.");
                propagate_data_if_exists(wc, OPTS, bundle_dir, wib_e, imp_record, data_source, data_dest_dir, data_dest_file);
                return true;
            }
            else
            {
                wc.console_write_yellow_bold(ARW + "Node: " + imp_record.get_id() + " SKIPPED (already exists).\n");
                return true;
            }
        default:
            wc.console_write_error_red("No action");
            break;
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error importing Node.");
        return false;
    }
    return true;
}

/**
 * @brief Import/merge a binary mapping definition set
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param overwrite flag controlling whether to overwrite the original Node
 * @param bundle_dir string containing path to root bundle directory to import from
 */
bool wibble::WibbleTransfer::_import_bin_map(WibbleConsole& wc, pkg_options& OPTS, const bool overwrite, const std::string& bundle_dir)
{
    WibbleExecutor wib_e{OPTS};
    const auto bin_map    = WibbleUtility::export_binary_tool_mapping(wc, wib_e.get_bin_mapping());
    const std::string& bin_map_def = bundle_dir + "/bin_map.wib";
    if(! std::filesystem::exists(bin_map_def))
    {
        wc.console_write_error_red("Unable to open binary mapping import definition. Aborting.");
        return false;
    }

    std::map<std::string, std::pair<std::string, std::string>> o_bin_map;
    for(const auto& [key, value]: bin_map)
    {
        o_bin_map[value.first] = value.second;
    }

    std::stringstream import_ss{WibbleIO::read_file_into_stringstream(bin_map_def)};
    const auto& import_def = import_ss.str();
    std::stringstream().swap(import_ss);
    const auto import_map = WibbleUtility::export_binary_tool_mapping(wc, import_def);
    for(const auto& [import_index, import_entry]: import_map)
    {
        bool already_exists = false;
        for(const auto& [original_index, original_entry]: bin_map)
        {
            if(original_entry.first == import_entry.first)
            {
                if(overwrite)
                {
                    wc.console_write_yellow(ARW + "Binary file mapping '" + original_entry.first + "' already exists. Will be OVERWRITTEN.\n");
                }
                else
                {
                    wc.console_write_yellow(ARW + "Binary file mapping '" + original_entry.first + "' already exists. Skipping.\n");
                }
                already_exists = true;
            }
        }

        // if an entirely new entry, OR we are overwriting an existing entry, update the map
        if(! already_exists || (already_exists && overwrite))
        {
            o_bin_map[import_entry.first] = import_entry.second;
        }
    }

    // build binary mapping config file entry
    std::string map_string = "binary_mapping          = \"";
    for(const auto& [key, value]: o_bin_map)
    {

        if(value.second == NONE_IDENT)
        {
            map_string.append(key + ":" + value.first + ", ");
        }
        else
        {
            map_string.append(key + ":");
            map_string.append(value.first + "%");
            map_string.append(value.second + ", ");
        }
    }
    map_string = map_string.substr(0, map_string.length() - 2) + "\"";

    // ensure target path exists
    bool ensure_path =  WibbleIO::create_path(OPTS.paths.wibble_store + "/templates/bin_default");

    // ensure to remove all binary mapping lines for a clean config
    bool clean_config = WibbleUtility::remove_or_update_line_from_file(OPTS.paths.tmp_dir, wc,
                                                   "binary_mapping", OPTS.files.live_config_file,
                                                   true, "");
    // FIXME: should revert to previous config if any sort of error as additional
    // safety operation
    if(ensure_path && clean_config)
    {
        if(std::filesystem::exists(std::filesystem::path(bundle_dir + "/bin_default")))
        {
            for(auto const& entry: std::filesystem::directory_iterator{std::filesystem::path(bundle_dir + "/bin_default")})
            {
                if(entry.is_regular_file())
                {
                    // copy template file across
                    if( ! std::filesystem::exists(OPTS.paths.wibble_store + "/templates/bin_default/" + std::string(entry.path().filename()))
                       || overwrite)
                    {
                        auto result = WibbleIO::wcopy_file(entry.path(), std::filesystem::path(OPTS.paths.wibble_store + "/templates/bin_default/" + std::string(entry.path().filename())));
                        if(! result)
                        {
                            wc.console_write_error_red("Error copying template file: " + std::string(entry.path()));
                        }
                        wc.console_write_yellow(ARW + "Binary mapping template file imported: " + std::string(entry.path().filename()) + "\n");
                    }
                }
            }
        }
        return WibbleIO::write_out_file_append(map_string + "\n", OPTS.files.live_config_file);
    }
    else
    {
        return false;
    }
}

/**
 * @brief Import a given Task Group
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param bundle_dir string containing path to root bundle directory to import from
 * @param overwrite flag controlling whether to overwrite the original item(s)
 */
bool wibble::WibbleTransfer::_import_task_group(WibbleConsole& wc, pkg_options& OPTS,
                                                const xfer_item& item, const bool overwrite,
                                                const std::string& bundle_dir)
{
    const auto source_tg_def        = bundle_dir + "/tasks/open_tasks/task_groups/" + item.id.substr(0, 4) + "/" + item.desc;
    const auto target_tg_def        = OPTS.paths.tasks_dir + "/open_tasks/task_groups/" + item.id.substr(0, 4) + "/" + item.desc;
    const auto task_list_open_dir   = bundle_dir + "/tasks/open_tasks/task_lists/" +item.id.substr(0, 4);
    const auto task_list_closed_dir = bundle_dir + "/tasks/closed_tasks/task_lists/" +item.id.substr(0, 4);
    const auto task_data_base_dir   = bundle_dir + "/tasks/task_data/" + item.id.substr(0, 4);
    const auto target_data_base_dir = OPTS.paths.tasks_dir + "/task_data/" + item.id.substr(0, 4);
    const auto target_tl_open_dir   = OPTS.paths.tasks_dir + "/open_tasks/task_lists/" + item.id.substr(0, 4);
    const auto target_tl_closed_dir = OPTS.paths.tasks_dir + "/closed_tasks/task_lists/" + item.id.substr(0, 4);

    if(! std::filesystem::exists(source_tg_def))
    {
        wc.console_write_error_red("Unable to read source Task Group file. Aborting.");
        return false;
    }

    // 1. Merge or copy the Task Group definition file across
    // 2. For each open task list category:
    //    a) If it does not exist, straight copy;
    //    b) If it does exist, perform merge of unique lines/tasks
    // 3. Repeat process for closed task list(s)
    // 4. Copy any task data directories. If overwrite is enabled,
    //    remove/replace with new variant

    // Task Group already exists, so merge
    if(std::filesystem::exists(target_tg_def))
    {
        WibbleUtility::merge_index_file_entries(OPTS.paths.tmp_dir, wc, target_tg_def, source_tg_def, overwrite);
    } // Else copy anew
    else
    {
        // ensure path exists, should not be necessary, but housekeeping
        WibbleIO::create_path(WibbleIO::get_parent_dir_path(target_tg_def));
        bool copy_tl = WibbleIO::wcopy_file(source_tg_def, target_tg_def);
        copy_tl == true ? wc.console_write_green(ARW + "Task Group " + std::string(std::filesystem::path(source_tg_def).filename()) + " copied.\n")
                        : wc.console_write_error_red("Error copying Task Group file.");
    }
    // handle open tasks
    if(std::filesystem::exists(task_list_open_dir))
    {
        for(const auto& fp: std::filesystem::directory_iterator(task_list_open_dir))
        {
            const auto entry_fn = std::filesystem::path(fp.path()).filename();
            const auto target_fn = target_tl_open_dir + "/" + std::string(entry_fn);

           if(std::filesystem::exists(target_fn))
           {
               wc.console_write_yellow(ARW + "Open Task List exists, merging.\n");
               WibbleUtility::merge_index_file_entries(OPTS.paths.tmp_dir, wc, target_fn, fp.path(), overwrite);
           }
           else
           {
               WibbleIO::create_path(WibbleIO::get_parent_dir_path(target_fn));
               bool copy_tl = WibbleIO::wcopy_file(std::string(fp.path()), target_fn);
               copy_tl == true ? wc.console_write_green(ARW + "Open Task List " + std::string(entry_fn) + " copied.\n")
                               : wc.console_write_error_red("Error copying Open Task List file.");
           }
        }
    }

    // handle closed tasks
    if(std::filesystem::exists(task_list_closed_dir))
    {
        for(const auto& fp: std::filesystem::directory_iterator(task_list_closed_dir))
        {
            const auto entry_fn = std::filesystem::path(fp.path()).filename();
            const auto target_fn = target_tl_closed_dir + "/" + std::string(entry_fn);


            if(std::filesystem::exists(target_fn))
            {
                wc.console_write_yellow(ARW + "Closed Task List exists, merging.\n");
                WibbleUtility::merge_index_file_entries(OPTS.paths.tmp_dir, wc, target_fn, fp.path(), overwrite);
            }
            else
            {
                WibbleIO::create_path(WibbleIO::get_parent_dir_path(target_fn));
                bool copy_tl = WibbleIO::wcopy_file(std::string(fp.path()), target_fn);
                copy_tl == true ? wc.console_write_green(ARW + "Closed Task List " + std::string(entry_fn) + " copied.\n")
                                : wc.console_write_error_red("Error copying Closed Task List file.");
            }
        }
    }

    // handle task data
    if(std::filesystem::exists(task_data_base_dir))
    {
        for(const auto& fp: std::filesystem::directory_iterator(task_data_base_dir))
        {
            const auto entry_fn = std::filesystem::path(fp.path()).filename();
            const auto target_fn = target_data_base_dir + "/" + std::string(entry_fn);

            if(std::filesystem::exists(target_fn) && ! overwrite)
            {
                wc.console_write_yellow(ARW + "Task data exists, overwrite mode disabled. Skipping.\n");
            }
            else
            {
                auto dest_path = WibbleIO::get_parent_dir_path(target_fn);
                if(overwrite && std::filesystem::exists(target_fn))
                {
                    wc.console_write_green(ARW + "Overwrite mode enabled. Forcefully re-copying Task Data directory.\n");
                    // purge existing directory; hence why overwrite can be dangerous
                    std::filesystem::remove_all(target_fn);
                }
                WibbleIO::create_path(dest_path);
                bool copy_data = WibbleIO::wcopy_file(std::string(fp.path()), target_fn);
                copy_data == true ? wc.console_write_green(ARW + "Task Data directory " + std::string(entry_fn) + " copied.\n")
                                  : wc.console_write_error_red("Error copying Task Data directory.");
            }
        }
    }

    wc.console_write_success_green("Task Group successfully imported.");
    return true;
}

/**
 * @brief Perform the actual import of designated material from bundle
 * @param wc WibbleConsole general console utility object
 * @param OPTS package/general configuration options struct
 * @param overwrite flag controlling whether to overwrite the original item(s)
 */
bool wibble::WibbleTransfer::_do_import(WibbleConsole& wc, pkg_options& OPTS, const bool overwrite, const std::string& bundle_dir)
{
    bool success = true;
    bool process_topics = false;
    bool process_bm     = false;

    for(auto const& item: xfer_bundle)
    {
        switch(item.type)
        {

        case WibbleTransfer::x_item_type::NODE:
            if(payload.node_count > 0 && ! _import_node(wc, OPTS, item, overwrite, bundle_dir)) { success = false; }
            break;

        case WibbleTransfer::x_item_type::TASK_GROUP:
            if(payload.tg_count > 0 && ! _import_task_group(wc, OPTS, item, overwrite, bundle_dir)) { success = false; }
            break;

        case WibbleTransfer::x_item_type::TOPIC:
            // only parse/accummulate topic maps once
            if(payload.topic_count > 0) { process_topics = true; } //topic_list.push_back(item.desc); }
            break;

        case WibbleTransfer::x_item_type::BIN_MAP:
            if(payload.bm_count > 0) { process_bm = true; }
            break;

        default:
            break;
        }
    }

    if(process_topics)
    {
        // can now do the import on the basis of selected topics
        if(! _import_topics(wc, OPTS, overwrite, bundle_dir)) { success = false; }
    }

    if(process_bm)
    {
        if(! _import_bin_map(wc, OPTS, overwrite, bundle_dir)) { success = false; }
    }
    return success;
}

