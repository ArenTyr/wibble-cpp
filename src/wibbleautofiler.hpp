/**
 * @file      wibbleautofiler.hpp
 * @brief     Provides "autofile" functionality (header file).
 * @details
 *
 * This class provides all of the 'autofiling' functionality that Wibble
 * offers.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_AUTOFILER_H
#define WIBBLE_AUTOFILER_H

#include "wibbleexecutor.hpp"

namespace wibble
{
    class WibbleAutofiler
    {
    private:
        WibbleConsole wc;
        WibbleExecutor wib_e;
        const std::string AF_CAT_FILE = "/autofile_categories.wib";
        const std::string AF_INDEX_FILE = "/autofile_index.wib";
        const std::string af_base_dir;
        const std::string cache_dir;
        const std::string SEP = "/";
        bool _append_file_to_filing_list(const std::string& fn, const std::string& desc);
        void _confirm_file_operation(const std::string& src, const std::string& dest, const std::string& desc, const int& mode, const bool no_prompt);
        void _display_file_listing(std::vector<std::pair<std::string, std::string>>& file_listing);
        void _display_file_options(std::pair<std::string, std::string>& file_selection);
        void _execute_autofile(const std::string& src_file, const std::string& dest_file, const std::string& description, const int& mode);
        std::string _get_set_description(const std::string& s_desc = "");
        std::string _get_set_filing_category(const std::string& s_category = "");
        std::vector<std::pair<std::string, std::string>> _read_in_autofiles(const std::string& filter_fn = "", const std::string& filter_desc = "");
        std::vector<std::string> _read_in_categories();
        std::string _generate_dest_dir(const std::string& category, const std::string& manual_YMD = "");
        void _update_category_file(const std::set<std:: string>& cat_map);
        void _update_index_or_exit(const std::string& dest_file, const std::string& description);

        // add in function to automatically search/filter/open/<do something> to file list

    public:
        void add_new_category();
        void edit_categories();
        void just_list(const std::string& filter_fn, const std::string& filter_desc);
        void open_file_action(const std::string& filter_fn, const std::string& filter_desc);
        void perform_autofile(const std::string& fn, const int& mode, const bool dry_run, const std::string& custom_date,
                              const bool no_prompt, const std::string& s_category = "", const std::string& s_desc = "");
        void null_operation();
        WibbleAutofiler(const pkg_options& OPTS, const std::string& autofile_base_dir, const std::string& cache_dir):
            wib_e{OPTS},
            af_base_dir{autofile_base_dir},
            cache_dir{cache_dir}
             {} ;
    };
}
#endif
