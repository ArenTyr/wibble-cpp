/**
 * @file      wibbletopics.hpp
 * @brief     Main dispatcher/interface for the topic functionality (header file).
 * @details
 *
 * This class generates a bash shell command which executes to
 * generate a compressed archive of all of the user's Wibble data.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLETOPICS_H
#define WIBBLETOPICS_H

#include <map>
#include <vector>

#include "wibbleconsole.hpp"
#include "wibblemetatopic.hpp"

namespace wibble
{
    class WibbleTopics
    {
    private:
        const int TP_FIELD_WIDTH = 30;
        const std::string cache_dir; 
        const std::string templates_dir; 
        const std::string bin_mapping; 
        bool hibernated_topics = false;
        std::vector<topic_def> topic_desc;
        std::map<std::string, std::string> topic_map;

        /**
         * @brief Struct for containing all Topic matches.
         * @param entries mapping of index to filename (for interactive selection)
         * @param entry_list vector of all entry titles to iterate over for display
         */
        struct topic_result_set
        {
            std::map<unsigned long, std::string> entries;
            std::vector<std::string> entry_list;
        };
        
        void _build_result_set(const std::string& input_file, topic_result_set& rs, unsigned long& tot_entries);
        int _calc_padding_width(unsigned long tot_records) const;
        topic_def _generate_blank_topic_def();
        const std::string _gen_topic_definition(const std::string& name, const std::string& id, const std::string& title,
                                                const std::string& category, const std::string& type, const std::string& desc) const;
        const std::string _get_topic_placeholder_replacement(const std::string& TOKEN, const topic_def& topic) const;
        std::stringstream _get_topic_stringstream_from_file(const std::string& topic_file); 
        topic_def _parse_topic_file(std::stringstream& topic_ss);
        bool _topic_def_integrity_check(topic_def& topic);
    public:
        /**
         * @brief Struct for containing ripgrep Topic entry matches.
         * @param fn Filename of the Topic entry
         * @param title Topic entry title
         * @param count Match frequency/number of ripgrep matches for entry
         */
        struct entry_match
        {
            std::string fn;
            std::string title;
            unsigned long count;
        };

        /**
         * @brief Struct for containing ripgrep matches
         * @param result_list vector of matches from topic_result_set as result of ripgrep filtering
         * @param rs struct with raw result set built up after parsing Topic index file
         */
        struct rg_matches
        {
            std::vector<entry_match> result_list;
            topic_result_set rs;
        };

        bool accumulate_topic_map(WibbleConsole& wc, const std::string& topic_base_dir, const bool get_hibernated = false);
        bool batch_construct_new_topic(WibbleConsole& wc, const std::string& topic_base_dir, std::string& name, std::string& category,
                                       const std::string& title, std::string& type, std::string& desc);
        const std::string get_default_topic_template_str() const;
        topic_def get_topic_for_name(const std::string& topic_name);
        std::string add_topic_entry(WibbleConsole& wc, topic_def& topic, const std::string& topic_base_dir,
                                    const std::string& override_date);
        std::string add_topic_entry(WibbleConsole& wc, topic_def& topic, const std::string& topic_base_dir,
                                    const std::string& override_date, const std::string& title);
        bool add_topic_data_entry(WibbleConsole& wc, const topic_def& topic, const std::string& topic_base_dir,
                                  const std::string& data_file, const bool move_file, const std::string& override_date);
        bool add_topic_data_entry(WibbleConsole& wc, const topic_def& topic, const std::string& topic_base_dir,
                                  const std::string& data_file, const bool move_file, const std::string& override_date,
                                  const std::string& title, const bool trim_prefix, const bool bypass_move_check = false);
        bool create_new_topic(WibbleConsole& wc, const std::string& topic_base_dir);
        void describe_topic_definitions(WibbleConsole& wc);
        void execute_command_on_batch_set_build_trs(WibbleConsole& wc, const std::string& topic_base_dir,
                                          const topic_def& topic, const std::string& index_file, std::string& exec_cmd,
                                          const bool txt_not_data);
        void execute_command_on_batch_set(WibbleConsole& wc, const std::string& topic_base_dir,
                                          const topic_def& topic, std::string& exec_cmd,
                                          const bool txt_not_data, const topic_result_set& trs);

        void expunge_topic(WibbleConsole& wc, const std::string& topic_base_dir);
        bool flip_active_status(const bool active, const std::string& topic_fn);
        bool get_hibernated_status() const;
        std::string get_topic_fn_from_rg_vimgrep(WibbleConsole& wc, const std::string& topic_index,
                                                 const std::string& topic_entry_dir, const std::string RG_WORKFILE);
        bool inventory_topic(const WibbleConsole& wc, const std::string& topic_base_dir, const topic_def& topic);
        std::string list_available_topics(WibbleConsole& wc, bool selector = false);
        std::vector<std::string> obtain_topic_list();
        void print_topic_definition(WibbleConsole&wc, const topic_def& topic);
        bool remove_topic_entry(WibbleConsole& wc, const topic_def& topic, const std::string& topic_base_dir,
                                const std::string& entry_key, bool txt_entry);
        bool rename_topic_entry(WibbleConsole& wc, const std::string& cache_dir, const std::string& topic_index_file, const std::string& topic_entry, const bool txt_not_data);
        rg_matches rg_vimgrep_matches(WibbleConsole& wc, const std::string& RG_WORKFILE, const std::string& topic_index);
        std::string select_topic_entry_from_list(WibbleConsole& wc, const std::string& topic_base_dir, const std::string& topic_index,
                                                 topic_def& topic, const bool stub_only = false);
        bool sort_index_file(const std::string& index_file);
        bool topic_template_substitutions(const std::string& topic_fn, const topic_def& topic);

        WibbleTopics(const std::string& c_dir, const std::string& t_mplate_dir, const std::string& b_map):
            cache_dir{c_dir},
            templates_dir{t_mplate_dir},
            bin_mapping{b_map} 
            {};
            
    };
}
#endif
