/**
 * @file      wibblebatchmanager.cpp
 * @brief     Class to execute all batch functionality.
 * @details
 *
 * Provides the "batch" command functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibblebatchmanager.hpp"
#include "wibbleexit.hpp"
#include "wibblejotter.hpp"
#include "wibbletask.hpp"
#include "wibbletopicmanager.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Optionally append to a designated logging file.
 * @param wc general console utility object
 * @param result String indicating success/error result of batch operation
 * @param BATCH batch_options struct with batch command line parameters
 */
void wibble::WibbleBatchManager::_append_to_logfile(WibbleConsole& wc, const std::string& result, const batch_options& BATCH) const
{
    try
    {
        //--------------------------------------------------------------------------------
        //=============== [ SUCCESS ] [ Thu, 16 May 2024 13:37:16 +0100 ]  ===============
        //================================================================================
        //--------------------------------------------------------------------------------

        const std::string light_div = "--------------------------------------------------------------------------------\n";
        const std::string heavy_div = "================================================================================\n";
        const std::string title_div = "===============";
        const std::string opera_div = "----------------------- ( Operation ) ------------------------------------------\n";
        const std::string noded_div = "----------------------- ( Node Parameters ) ------------------------------------\n";
        const std::string taski_div = "----------------------- ( Task Parameters ) ------------------------------------\n";
        const std::string topic_div = "----------------------- ( Topic Parameters ) -----------------------------------\n";
        const std::string jotfl_div = "----------------------- ( Jotfile Parameters ) ---------------------------------\n";
        const std::string topin_div = "----------------------- ( Create New Topic Parameters ) ------------------------\n";
        const std::string jotfn_div = "----------------------- ( Create New Jotfile Parameters ) ----------------------\n";
        const std::string datap_div = "----------------------- ( Data Parameters ) ------------------------------------\n";
        std::string log_entry = light_div + title_div + " " + result + " " + title_div + "\n";
        log_entry.append(opera_div);
        log_entry.append("> Create Node          : "); BATCH.create_node                        ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Create Task Item     : "); BATCH.create_task_item                   ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Create Topic Entry   : "); BATCH.create_topic_entry                 ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Create Jotfile Entry : "); BATCH.create_jotfile_entry               ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Create New Task Group: "); BATCH.create_new_task_group              ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Create New Topic     : "); BATCH.create_new_topic                   ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Create New Jotfile   : "); BATCH.create_new_jotfile                 ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Add Node data        : "); BATCH.node_add_data                      ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Add Topic data       : "); BATCH.topic_add_data                     ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append(noded_div);
        log_entry.append("> Node ID              : "); BATCH.node_id == "UNSET"                 ? log_entry.append("\n")     : log_entry.append(BATCH.node_id + "\n");
        log_entry.append("> Node Date            : "); BATCH.node_date == "UNSET"               ? log_entry.append("\n")     : log_entry.append(BATCH.node_date + "\n");
        log_entry.append("> Node Title           : "); BATCH.node_title == "UNSET"              ? log_entry.append("\n")     : log_entry.append(BATCH.node_title + "\n");
        log_entry.append("> Node Filetype        : "); BATCH.node_filetype == "UNSET"           ? log_entry.append("\n")     : log_entry.append(BATCH.node_filetype + "\n");
        log_entry.append("> Node Description     : "); BATCH.node_description == "UNSET"        ? log_entry.append("\n")     : log_entry.append(BATCH.node_description + "\n");
        log_entry.append("> Node Project         : "); BATCH.node_project == "UNSET"            ? log_entry.append("\n")     : log_entry.append(BATCH.node_project + "\n");
        log_entry.append("> Node Tags            : "); BATCH.node_tags == "UNSET"               ? log_entry.append("\n")     : log_entry.append(BATCH.node_tags + "\n");
        log_entry.append("> Node Class           : "); BATCH.node_class == "UNSET"              ? log_entry.append("\n")     : log_entry.append(BATCH.node_class + "\n");
        log_entry.append("> Node Keypairs        : "); BATCH.node_keypairs == "UNSET"           ? log_entry.append("\n")     : log_entry.append(BATCH.node_keypairs + "\n");
        log_entry.append("> Node Custom          : "); BATCH.node_custom == "UNSET"             ? log_entry.append("\n")     : log_entry.append(BATCH.node_custom + "\n");
        log_entry.append(taski_div);
        log_entry.append("> Task ID              : "); BATCH.task_id == "UNSET"                 ? log_entry.append("\n")     : log_entry.append(BATCH.task_id + "\n");
        log_entry.append("> Task Title           : "); BATCH.task_title == "UNSET"              ? log_entry.append("\n")     : log_entry.append(BATCH.task_title + "\n");
        log_entry.append("> Task Category        : "); BATCH.task_category == "UNSET"           ? log_entry.append("\n")     : log_entry.append(BATCH.task_category + "\n");
        log_entry.append("> Task Due Date        : "); BATCH.task_category == "UNSET"           ? log_entry.append("\n")     : log_entry.append(BATCH.task_due_date + "\n");
        log_entry.append("> Task Priority        : "); BATCH.task_priority == NULL_INT_VAL      ? log_entry.append("\n")     : log_entry.append(std::to_string(BATCH.task_priority) + "\n");
        log_entry.append("> Task Group Name      : "); BATCH.task_group == "UNSET"              ? log_entry.append("\n")     : log_entry.append(BATCH.task_group + "\n");
        log_entry.append(topic_div);
        log_entry.append("> Topic Entry Title    : "); BATCH.topic_entry_title == "UNSET"       ? log_entry.append("\n")     : log_entry.append(BATCH.topic_entry_title + "\n");
        log_entry.append("> Topic Custom Date    : "); BATCH.topic_entry_custom_date == "UNSET" ? log_entry.append("\n")     : log_entry.append(BATCH.topic_entry_custom_date + "\n");
        log_entry.append("> Topic Selected       : "); BATCH.use_topic == "UNSET"               ? log_entry.append("\n")     : log_entry.append(BATCH.use_topic + "\n");
        log_entry.append(jotfl_div);
        log_entry.append("> Jotfile Entry Title  : "); BATCH.jot_entry_title == "UNSET"         ? log_entry.append("\n")     : log_entry.append(BATCH.jot_entry_title + "\n");
        log_entry.append("> Jotfile Selected     : "); BATCH.use_jotfile == "UNSET"             ? log_entry.append("\n")     : log_entry.append(BATCH.use_jotfile + "\n");
        log_entry.append(topin_div);
        log_entry.append("> New Topic Category   : "); BATCH.topic_category == "UNSET"          ? log_entry.append("\n")     : log_entry.append(BATCH.topic_category + "\n");
        log_entry.append("> New Topic Name       : "); BATCH.topic_name == "UNSET"              ? log_entry.append("\n")     : log_entry.append(BATCH.topic_name + "\n");
        log_entry.append("> New Topic Title      : "); BATCH.topic_title == "UNSET"             ? log_entry.append("\n")     : log_entry.append(BATCH.topic_title + "\n");
        log_entry.append("> New Topic Filetype   : "); BATCH.topic_filetype == "UNSET"          ? log_entry.append("\n")     : log_entry.append(BATCH.topic_filetype + "\n");
        log_entry.append("> New Topic Description: "); BATCH.topic_description == "UNSET"       ? log_entry.append("\n")     : log_entry.append(BATCH.topic_description + "\n");
        log_entry.append(jotfn_div);
        log_entry.append("> New Jotfile Name     : "); BATCH.jot_name == "UNSET"                ? log_entry.append("\n")     : log_entry.append(BATCH.jot_name + "\n");
        log_entry.append("> New Jotfile Category : "); BATCH.jot_category == "UNSET"            ? log_entry.append("\n")     : log_entry.append(BATCH.jot_category + "\n");
        log_entry.append(datap_div);
        log_entry.append("> Content File (path)  : "); BATCH.content_file == ""                 ? log_entry.append("\n")     : log_entry.append(BATCH.content_file + "\n");
        log_entry.append("> Data File (path)     : "); BATCH.data_file == ""                    ? log_entry.append("\n")     : log_entry.append(BATCH.data_file + "\n");
        log_entry.append("> Data Operation COPY  : "); BATCH.data_copy                          ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Data Operation MOVE  : "); BATCH.data_move                          ? log_entry.append("TRUE\n") : log_entry.append("FALSE\n");
        log_entry.append("> Log File (path)      : "); log_entry.append(BATCH.logfile + "\n");
        log_entry.append(heavy_div);

        WibbleIO::write_out_file_append(log_entry, BATCH.logfile);
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error appending to logfile: " + std::string(ex.what()));
    }
}

/**
 * @brief Automatically create a new Node.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_node(WibbleConsole& wc, const pkg_options& OPTS, const batch_options& BATCH) const
{
    wc.silence_output();
    WibbleExecutor wib_e{OPTS};
    const bool success = wib_e.op_create_node_batch(BATCH);
    wc.enable_output();
    return _gen_result_timestamp(success);
}


/**
 * @brief Automatically create a new Task group.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_task_group(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const
{
    wc.silence_output();
    WibbleTask wtsk{OPTS.paths.tasks_dir, OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping, true};
    WibbleTask::task_group tg = wtsk.add_new_group(BATCH.task_group);
    wc.enable_output();
    wc.console_write_success_green("Added task group: '" + tg.group_name + "'. Task group ID: " + tg.group_id + ".");
    return _gen_result_timestamp(true);
}

/**
 * @brief Automatically create a new Task item within an existing Task group.
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_task_item(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const
{
    if(BATCH.task_due_date == "UNSET") { BATCH.task_due_date = ""; }

    wc.silence_output();
    WibbleTask wtsk{OPTS.paths.tasks_dir, OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping, true};
    const std::string add_task_item = wtsk.batch_add_task_item(BATCH.task_title, BATCH.task_category, BATCH.task_due_date, BATCH.task_priority, BATCH.task_group);
    wc.enable_output();
    if(add_task_item == "INVALID_GROUP")
    {
        wc.console_write_error_red("No such task group '" + BATCH.task_group + "' exists. Valid groups names listed above.");
        return _gen_result_timestamp(false);
    }
    else if(add_task_item == "INVALID_TASK")
    {
        wc.console_write_error_red("Error adding task '" + BATCH.task_group + "' exists.");
        return _gen_result_timestamp(false);
    }
    else
    {
        wc.console_write_success_green("Added task with ID: " + add_task_item);
        return _gen_result_timestamp(true);
    }
}

/**
 * @brief Automatically add an entry to an existing Topic.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_topic_entry(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const
{
    try
    {
        wc.silence_output();
        // The Topic functionality already has de facto "non-interactive" capability, simply exploit this
        topic_options TOPIC;
        TOPIC.add_from_file = BATCH.content_file;
        TOPIC.add_with_template = "UNSET";
        TOPIC.add_entry_to_topic = BATCH.use_topic;
        TOPIC.specify_title = BATCH.topic_entry_title;
        TOPIC.override_date = BATCH.topic_entry_custom_date;

        WibbleTopics wt{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
        WibbleTopicManager wtm{OPTS.exec.bin_mapping};
        wt.accumulate_topic_map(wc, OPTS.paths.topics_dir);
        wtm.add_new_topic_entry(OPTS, TOPIC, wt);
        wc.enable_output();
        return _gen_result_timestamp(true);
    }
    catch(const wibble::wibble_exit& we)
    {
        wc.enable_output();
        wc.console_write_error_red("Error adding entry to Topic '" + BATCH.use_topic + "'. Ensure the specified Topic exists.");
        return _gen_result_timestamp(false);
    }
}

/**
 * @brief Automatically add an entry to an existing Jotfile.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_jotfile_entry(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH) const
{
    wc.silence_output();
    WibbleJotter wj{wc, OPTS.paths.jot_dir, OPTS.paths.tmp_dir, ""};
    const bool jf_res = wj.batch_add_new_entry(wc, BATCH.use_jotfile, BATCH.jot_entry_title, BATCH.content_file);
    wc.enable_output();
    return _gen_result_timestamp(jf_res);
}

/**
 * @brief Automatically create a new Topic.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_new_topic(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH)
{
    wc.silence_output();
    WibbleTopics wt{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
    const bool topic_result = wt.batch_construct_new_topic(wc, OPTS.paths.topics_dir, BATCH.topic_name, BATCH.topic_category,
                                                           BATCH.topic_title, BATCH.topic_filetype, BATCH.topic_description);
    wc.enable_output();
    return _gen_result_timestamp(topic_result);
}

/**
 * @brief Automatically create a new Jotfile.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_create_new_jotfile(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH)
{
    wc.silence_output();
    WibbleJotter wj{wc, OPTS.paths.jot_dir, OPTS.paths.tmp_dir, ""};
    const bool jf_res = wj.create_jotfile(wc, BATCH.jot_category, BATCH.jot_name);
    wc.enable_output();
    return _gen_result_timestamp(jf_res);
}

/**
 * @brief Automatically add data to an existing Node.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_node_add_data(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH)
{
    bool null_search = true;
    search_filters SEARCH;
    // enforce selection of one unique record only
    SEARCH.single_only = true;
    // are we looking in archive database?
    SEARCH.archive_db = BATCH.node_archived;
    if(BATCH.node_id          != "UNSET") { SEARCH.id    = wc.trim(BATCH.node_id);          null_search = false; } 
    if(BATCH.node_date        != "UNSET") { SEARCH.date  = wc.trim(BATCH.node_date);        null_search = false; } 
    if(BATCH.node_title       != "UNSET") { SEARCH.title = wc.trim(BATCH.node_title);       null_search = false; } 
    if(BATCH.node_filetype    != "UNSET") { SEARCH.type  = wc.trim(BATCH.node_filetype);    null_search = false; } 
    if(BATCH.node_description != "UNSET") { SEARCH.desc  = wc.trim(BATCH.node_description); null_search = false; } 
    if(BATCH.node_project     != "UNSET") { SEARCH.proj  = wc.trim(BATCH.node_project);     null_search = false; } 
    if(BATCH.node_tags        != "UNSET") { SEARCH.tags  = wc.trim(BATCH.node_tags);        null_search = false; } 
    if(BATCH.node_class       != "UNSET") { SEARCH.cls   = wc.trim(BATCH.node_class);       null_search = false; } 
    if(BATCH.node_keypairs    != "UNSET") { SEARCH.kp    = wc.trim(BATCH.node_keypairs);    null_search = false; } 
    if(BATCH.node_custom      != "UNSET") { SEARCH.cust  = wc.trim(BATCH.node_custom);      null_search = false; } 
  
    if(null_search) { wc.console_write_error_red("No Node search parameters specified. Aborting."); return _gen_result_timestamp(false); }
    try
    {
        wc.silence_output();
        WibbleExecutor wib_e{OPTS};
        // single_only flag ensures non-interactivity
        WibbleRecord w = WibbleUtility::get_record_interactive(wc, wib_e, SEARCH);

        const bool copy_not_move = BATCH.data_copy ? true : false;
        bool added = false;
        // final parameter bypasses the move file warning/check
        if(! SEARCH.archive_db)  { added = wib_e.op_add_data_to_note(w, OPTS.files.main_db, OPTS.paths.tmp_dir + "/wibble-workfile", false, BATCH.data_file, copy_not_move, true);    }
        else                     { added = wib_e.op_add_data_to_note(w, OPTS.files.archived_db, OPTS.paths.tmp_dir + "/wibble-workfile", true, BATCH.data_file, copy_not_move, true); }

        wc.enable_output();
        if(added && copy_not_move)   { wc.console_write_success_green("Data successfully COPIED into Node with ID " + w.get_id() + "."); }
        if(added && ! copy_not_move) { wc.console_write_success_green("Data successfully MOVED into Node with ID " + w.get_id() + ".");  }
        return _gen_result_timestamp(added);
    }
    catch(const wibble::wibble_exit& we)
    {
        wc.enable_output();
        wc.console_write_error_red("Node search filters must result in ONE singular Node match.");
        return _gen_result_timestamp(false);
    }
}

/**
 * @brief Automatically add data to an existing Task item.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_task_add_data(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH)
{
    wc.silence_output();
    WibbleTask wtsk{OPTS.paths.tasks_dir, OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping, true};
    const std::string action = BATCH.data_copy ? "b" : "c";
    const bool add_data = wtsk.batch_add_task_data(BATCH.task_id, BATCH.data_file, action);
    wc.enable_output();
    if(add_data && action == "b") { wc.console_write_success_green("Data successsfully COPIED into task item '" + BATCH.task_id + "'."); }
    if(add_data && action == "c") { wc.console_write_success_green("Data successsfully MOVED into task item '" + BATCH.task_id + "'.");  }
    return _gen_result_timestamp(add_data);
}

/**
 * @brief Automatically add data to an existing Topic.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 */
std::string wibble::WibbleBatchManager::_batch_topic_add_data(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH)
{
    try
    {
        wc.silence_output();
        WibbleTopics wt{OPTS.paths.tmp_dir, OPTS.paths.templates_dir, OPTS.exec.bin_mapping};
        wt.accumulate_topic_map(wc, OPTS.paths.topics_dir);
        topic_def tgt_topic = wt.get_topic_for_name(BATCH.use_topic);
        const std::string cust_date = (BATCH.topic_entry_custom_date == "UNSET") ? "" : BATCH.topic_entry_custom_date; 

        if(tgt_topic.id == "___NULL___") 
        { 
            wc.console_write_error_red("No valid Topic found for '" + BATCH.use_topic + "'. Aborting.", false);
            wc.enable_output();
            return _gen_result_timestamp(false); 
        }
        const bool result = wt.add_topic_data_entry(wc, tgt_topic, OPTS.paths.topics_dir, BATCH.data_file, BATCH.data_move, cust_date, BATCH.topic_entry_title, false, true);
        const bool copy_not_move = BATCH.data_copy ? true : false;
        wc.enable_output();
        if(result && copy_not_move)   { wc.console_write_success_green("Data successfully COPIED into Topic with ID " + tgt_topic.id + "."); } 
        if(result && ! copy_not_move) { wc.console_write_success_green("Data successfully MOVED into Topic with ID " + tgt_topic.id + ".");  } 
        return _gen_result_timestamp(result);
    }
    catch(const wibble::wibble_exit& we)
    {
        wc.enable_output();
        return _gen_result_timestamp(false);
    }
}

/**
 * @brief Generate a standardised string/logging output result.
 * @param success Flag indicating whether batch operation completed successfully
 */
std::string wibble::WibbleBatchManager::_gen_result_timestamp(const bool success) const
{
    const std::string evt_dt = WibbleRecord::generate_date_rfc5322("");
    if(success) { return "[ SUCCESS ] [ " + evt_dt + " ] "; }
    else        { return "[ FAILURE ] [ " + evt_dt + " ] "; }
}

/**
 * @brief Main dispatching function for batch operations.
 * @param wc general console utility object
 * @param OPTS pkg_options environment settings/configuration struct
 * @param BATCH batch_options struct with batch command line parameters
 * @param ACTION enumeration with selected branch to execute
 */
void wibble::WibbleBatchManager::execute(WibbleConsole& wc, pkg_options& OPTS, batch_options& BATCH, const int action)
{
    std::string result = "";
    switch(action)
    {
    case BATCH_CREATE_NODE:
        result = _batch_create_node(wc, OPTS, BATCH);
        break;

    case BATCH_CREATE_TASK_ITEM:
        result = _batch_create_task_item(wc, OPTS, BATCH);
        break;

    case BATCH_CREATE_TOPIC_ENTRY:
        result = _batch_create_topic_entry(wc, OPTS, BATCH);
        break;

    case BATCH_CREATE_JOTFILE_ENTRY:
        result = _batch_create_jotfile_entry(wc, OPTS, BATCH);
        break;

    case BATCH_CREATE_NEW_TASK_GROUP:
        result = _batch_create_task_group(wc, OPTS, BATCH);
        break;

    case BATCH_CREATE_NEW_TOPIC:
        result = _batch_create_new_topic(wc, OPTS, BATCH);
        break;
        
    case BATCH_CREATE_NEW_JOTFILE:
        result = _batch_create_new_jotfile(wc, OPTS, BATCH);
        break;

    case BATCH_NODE_ADD_DATA:
        result = _batch_node_add_data(wc, OPTS, BATCH);
        break;

    case BATCH_TASK_ADD_DATA:
        result = _batch_task_add_data(wc, OPTS, BATCH);
        break;

    case BATCH_TOPIC_ADD_DATA:
        result = _batch_topic_add_data(wc, OPTS, BATCH);
        break;
    }

    if(result.find("[ SUCCESS ]") != std::string::npos) { wc.console_write_success_green(result); }
    else                                                { wc.console_write_error_red(result); }

    if(BATCH.logfile != "UNSET")
    {
        _append_to_logfile(wc, result, BATCH);
    }
}
