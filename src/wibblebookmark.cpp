// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      wibblebookmark.cpp
 * @brief     Main class with all bookmark operations.
 * @details
 *
 * This class contains all of the "bookmark" functionality: the
 * ability to quickly store and retrieve frequently accessed
 * Wibble Nodes, Topic entries, Jotfiles, and Task entries.
 *
 * Futhermore it also offers the capability to "bookmark"/alias any 
 * Wibble command, or any arbritrary shell command. Such shell commands
 * can either be simple single-line/small commands, or instead can be an
 * entire custom script (sh, perl, python, whatever).
 *
 * Finally, the facility exists to create any number of custom
 * categories/"groups", each of which can contain any such selection
 * of defined command bookmarks. This provides the facility to build up
 * a very convenient dynamic "menu" of preferred commands, suitably organised,
 * thus saving the user from the burden of endless shell aliases,
 * remembering what/where particular shell commands or scripts do/exist,
 * or having to continually consult man pages and documentation to remember
 * particularly necessary switches or commands.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibbleactions.hpp"
#include "wibblebookmark.hpp"
#include "wibbleexecutor.hpp"
#include "wibbleexit.hpp"
#include "wibblejotter.hpp"
#include "wibbletask.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

typedef wibble::WibbleBookmark::cat_bm_entry cbm;

/**
 * @brief Method to retrieve and display custom [command] bookmark categories.
 * @param c_sel direct selector/bypass to automatically select a category non-interactively
 * @param hib_categories switch to work with hibernated rather than active categories
 */
cbm wibble::WibbleBookmark::_get_display_custom_categories(const int& c_sel, const bool hib_categories)
{
    wc.console_write_header(" CATEGORY");
    wc.console_write_hz_divider();
    std::map<int, cbm> sel_map;
    std::size_t index = 0;
    std::vector<wbe> cust_categories = (! hib_categories) ? _get_all_cust_category_ids() :
                                                            _get_all_hib_cust_category_ids(); 
    std::vector<std::string> names = _reduce_to_names(cust_categories);
    std::size_t num_names = names.size();
    if(names.size() == 0)
    {
        if(hib_categories) { wc.console_write_error_red("No hibernated custom categories. Exiting.");  throw wibble::wibble_exit(0); }
        else               { wc.console_write_error_red("No active/live custom categories. Exiting."); throw wibble::wibble_exit(0); }
    }

    for(auto const& entry: names)
    {
        cbm bm_entry; 
        bm_entry.short_name = entry;
        for(auto const& item: cust_categories)
        {
            if(item.cust_cat_name == entry)
            {
                // full name with id
                bm_entry.full_id = item.field1;
                break;
            }
        }
        sel_map[++index] = bm_entry;
        wc.console_write_padded_selector(index, num_names, entry); wc.console_write("");
    }
    wc.console_write_hz_divider();
    wc.console_write("");
    std::string resp;
    if(c_sel == -1) { resp = wc.console_write_prompt("Select category > ", "", ""); }
    else            { resp = "DIRECT";                                              }
    try
    {

        wc.console_write("");
        const unsigned int choice = (resp == "DIRECT") ? c_sel : std::stoi(resp);
        if(resp == "DIRECT")
        {
            wc.console_write_green(ARW + "Attempting direct selection of custom category: [" + std::to_string(c_sel) + "]");
            wc.console_write("");
        }
        if(choice < 1 || choice > num_names)
        { std::invalid_argument("Out of range"); }
        return sel_map[choice];
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        throw wibble::wibble_exit(1); 
    }
    
}

/**
 * @brief Generate a vector of bookmarks containing links to Wibble Nodes.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_node_bm()
{
    return _process_bm_file(1);
}

/**
 * @brief Generate a vector of bookmarks containing links to Wibble Topic entries.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_topic_bm()
{
    return _process_bm_file(2);
}

/**
 * @brief Generate a vector of bookmarks containing links to Wibble Jotfile entries.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_jotfile_bm()
{
    return _process_bm_file(3);
}

/**
 * @brief Generate a vector of bookmarks containing links to Wibble Task entries.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_task_bm()
{
    return _process_bm_file(4);
}

/**
 * @brief Generate a vector of bookmarks containing links to Wibble execution commands.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_wib_exec_bm()
{
    return _process_bm_file(5);
}

/**
 * @brief Generate a vector of bookmarks containing links to general shell execution commands.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_gen_exec_bm()
{
    return _process_bm_file(6);
}

/**
 * @brief Generate a vector of active custom category ids (employ bookmark struct).
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_cust_category_ids()
{
    return _process_bm_file(8);
}

/**
 * @brief Generate a vector of hibernated custom category ids (employ bookmark struct).
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_hib_cust_category_ids()
{
    return _process_bm_file(9);
}

/**
 * @brief Use a std::set to present a canonical de-duplicated vector of custom category names/categories.
 * @param cat_names vector of category names found
 */
std::vector<std::string> wibble::WibbleBookmark::_reduce_to_names(const std::vector<wbe>& cat_names)
{
    std::set<std::string> unique_names;
    std::vector<std::string> name_map;
    // use a set to remove duplicate category names
    for(auto const& entry: cat_names)    { unique_names.insert(entry.cust_cat_name); }
    for(auto const& name: unique_names)  { name_map.push_back(name);                 }
        
    std::sort(name_map.begin(), name_map.end());
    return name_map;
}

/**
 * @brief Get all custom category names/category list.
 */
std::vector<std::string> wibble::WibbleBookmark::_get_all_cust_category_names()
{
    std::vector<wbe> cat_names = _get_all_cust_category_ids();
    return _reduce_to_names(cat_names);
}

/**
 * @brief Get all custom command execution bookmarks for a given category name.
 * @param cat_name unique identifying category name 
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_cust_exec_bm_for_cat(const std::string& cat_name)
{
    std::vector<wbe> cat_entries;
    for(auto const& entry: bookmarks[CUST_CAT_MAP]) { if(entry.cust_cat_name == cat_name) { cat_entries.push_back(entry); } }
    return cat_entries;
}

/**
 * @brief Gather all custom command bookmarks from all custom category files.
 */
std::vector<wbe> wibble::WibbleBookmark::_get_all_cust_exec_bm()
{
    std::vector<wbe> all_cust_bm;
    try
    {
        for(auto const& dir_entry : std::filesystem::directory_iterator{wib_e.get_bookmarks_dir()})
        {
            // custom category bm files have form <id>_CUST_name.wib
            if(dir_entry.is_regular_file() && std::string(std::filesystem::path(dir_entry).filename()).find(CUST_PREFIX) != std::string::npos)
            {
                const std::string full_cust_fn = wib_e.get_bookmarks_dir() + "/" + std::string(std::filesystem::path(dir_entry).filename());
                const std::vector<wbe> entries = _process_bm_file(7, full_cust_fn);

                // copy into master vector
                for(auto const& entry: entries) { all_cust_bm.push_back(entry); }
            }
        }
        
        return all_cust_bm;
    }
    catch(std::exception& ex)
    {
        return std::vector<wbe>();
    }
}

/**
 * @brief Interactive menu for selecting of type of new bookmark.
 */
void wibble::WibbleBookmark::_add_new_bm_menu()
{
    wc.console_write_header("ADD");
    _anum("A"); std::cout << " ┃ "; wc.console_write_yellow("Add a new "); 
    wc.console_write_yellow_bold("node"); wc.console_write_yellow(" bookmark\n");
    _anum("B"); std::cout << " ┃ "; wc.console_write_yellow("Add a new ");
    wc.console_write_yellow_bold("topic"); wc.console_write_yellow(" bookmark\n");
    _anum("C"); std::cout << " ┃ "; wc.console_write_yellow("Add a new ");
    wc.console_write_yellow_bold("jotfile"); wc.console_write_yellow(" bookmark\n");
    _anum("D"); std::cout << " ┃ "; wc.console_write_yellow("Add a new ");
    wc.console_write_yellow_bold("task"); wc.console_write_yellow(" bookmark\n");
    _anum("E"); std::cout << " ┃ "; wc.console_write_yellow("Add a new ");
    wc.console_write_yellow_bold("Wibble command"); wc.console_write_yellow(" bookmark\n");
    _anum("F"); std::cout << " ┃ "; wc.console_write_yellow("Add a new ");
    wc.console_write_yellow_bold("General command"); wc.console_write_yellow(" bookmark\n");
    _anum("G"); std::cout << " ┃ "; wc.console_write_yellow("Add a new ");
    wc.console_write_yellow_bold("Custom category command"); wc.console_write_yellow(" bookmark\n");
    std::string choice = wc.console_write_prompt("\nSelect option > ", "", "");

    wc.trim(choice);
    int choice_num = 0;
    if(choice == "a" || choice == "A") { choice_num = 1; }
    if(choice == "b" || choice == "B") { choice_num = 2; }
    if(choice == "c" || choice == "C") { choice_num = 3; }
    if(choice == "d" || choice == "D") { choice_num = 4; }
    if(choice == "e" || choice == "E") { choice_num = 5; }
    if(choice == "f" || choice == "F") { choice_num = 6; }
    if(choice == "g" || choice == "G") { choice_num = 7; }
    try
    {
        if(choice_num >= 1 || choice_num <= 7)
        {
            _dispatch_add_bm_action(choice_num);
        }
        else
        {
            wc.console_write_error_red("Invalid selection, exiting.");
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection, exiting.");
    }
}

/**
 * @brief Interactively add a new custom bookmark category/containing group.
 */
bool wibble::WibbleBookmark::_add_new_custom_category()
{
    try
    {
        std::string cat_name;
        bool valid_entry = false;
        const int MAXTRIES = 5;
        int tries = 0;
        do
        {
            ++tries;
            cat_name = wc.console_write_prompt("Name of new category (max 100 characters) > ", "", ""); 
            cat_name = cat_name.substr(0, 100);
            wc.trim(cat_name);
            wibble::WibbleRecord::remove_slash(cat_name);
            bool unique = _check_cat_name_is_unique(cat_name);
            if(cat_name == "") { wc.console_write_error_red("Category name cannot be blank.");                                                       }
            else if(! unique)  { wc.console_write_error_red("Custom category name '" + cat_name + "' already exists. Please enter a unique name.");  }
            else { valid_entry = true; }
        } while(tries < MAXTRIES && valid_entry == false);

        if(valid_entry == false)
        {
            wc.console_write_error_red("Maximum attempts exceeded. Aborting.");
            throw std::invalid_argument("Invalid category name");
        }

        const std::string ts_hsh = wibble::WibbleRecord::generate_timestamp("");
        // A = active flag (not hibernated)
        const std::string record = cat_name + "|A|" + ts_hsh + "\n";
        return wibble::WibbleIO::write_out_file_append(record, wib_e.get_bookmarks_dir() + "/"
                                                           + CUST_CAT_MAP + ".wib");           
    }
    catch(std::exception& ex)
    {
        return false;
    }
}

/**
 * @brief Add a new command bookmark - either general command or command within a custom category.
 * @param custom_entry struct with the optional custom category to assign to
 */
bool wibble::WibbleBookmark::_add_cmd_bm(const cbm& custom_entry)
{
    try
    {
        wc.console_write_header("ADD");
        wc.console_write_yellow("1. Add a new "); wc.console_write_yellow_bold("simple/single line"); wc.console_write_yellow(" command bookmark.\n");
        wc.console_write_yellow("2. Add a new "); wc.console_write_yellow_bold("complex/multi-line/script"); wc.console_write_yellow(" bookmark.\n");
        std::string resp = wc.console_write_prompt("Select option [1:2] > ", "", "");
        if(resp != "1" && resp != "2") { wc.console_write_error_red("Invalid selection. Exiting.");  throw wibble::wibble_exit(1); }
        std::string cmd;
        const std::string ts_hsh = wibble::WibbleRecord::generate_timestamp("");
        const std::string script_dir = wib_e.get_scripts_dir() + BOOKMK_SCR;
        const std::string script_fn  = script_dir + "/" + ts_hsh + "_bm_script.sh";
        const std::string script_stub  = ts_hsh + "_bm_script.sh";
        const std::string shebang = "#!/bin/sh\necho 'Null command: Edit this script'\n";
        bool edit_script = false;

        if(resp == "1")
        {
            cmd = wc.console_write_prompt("Input shell command > ", "", "");
            wc.trim(cmd);
            wc.console_write_hz_divider();
            wc.console_write_green_bold(cmd + "\n");
            wc.console_write_hz_divider();
            resp = wc.console_write_prompt("Add this shell command as a new bookmark? (y/n) > ", "", "");
            if(resp != "y" && resp != "Y") { return false; }
        }
        else
        {
            bool bm_script_dir = wibble::WibbleIO::create_path(script_dir);
            if(! bm_script_dir) { wc.console_write_error_red("Error creating output directory. Aborting."); throw wibble::wibble_exit(1); }

            std::ofstream(script_fn).write(shebang.c_str(), shebang.length());

            // use alias to allow generated scripts to always be relative to configuration settings (for portability)
            cmd = EXEC_PREFIX + script_stub;
            //cmd = script_fn;
            std::system(("chmod u+x " + script_fn).c_str());
            edit_script = true;
        }

        std::string title = wc.console_write_prompt("Input title/descriptive name of this bookmark > ", "", ""); 
        wc.trim(title); wibble::WibbleRecord::remove_pipe(title);
        if(title == "")
        {
            wc.console_write_error_red("Title cannot be blank. Aborting.");
            // cleanup if needed
            if(std::filesystem::exists(std::filesystem::path(script_fn)))
            {
                std::filesystem::remove(std::filesystem::path(script_fn));
            }
            return false;
        }
        std::string record = "";
        // put the title first in case the command itself contains pipes (|)

        bool wor = false;
        if(custom_entry.short_name == "___NULL___") // no custom category name = general command bookmark
        {
            record = _generate_entry_hsh() + "|" + title + "|" + cmd + "\n";
            wor = wibble::WibbleIO::write_out_file_append(record, gen_exec_bm_file);
        }
        else
        {
            record = _generate_entry_hsh() + "|" + custom_entry.short_name + "|" + title + "|" + cmd + "\n";
            wor = wibble::WibbleIO::write_out_file_append(record, wib_e.get_bookmarks_dir() + "/"
                                                           + custom_entry.full_id + ".wib");
        }

        if(wor && edit_script)
        {
            _edit_bm_script(script_fn);
        }

        return wor;
    }
    catch(std::range_error& re)
    {
        return false;
    }

}

/**
 * @brief Add a new custom command bookmark.
 */
bool wibble::WibbleBookmark::_add_new_custom_bm()
{
    cbm cust = _get_display_custom_categories();
    return _add_cmd_bm(cust);
}

/**
 * @brief Add a new general command bookmark. 
 */
bool wibble::WibbleBookmark::_add_new_general_cmd_bm()
{
    cbm blank;
    return _add_cmd_bm(blank);
}

/**
 * @brief Add a new Wibble Jotfile bookmark.
 */
bool wibble::WibbleBookmark::_add_new_jotfile_bm()
{
    WibbleJotter wj{wc, b_OPTS.paths.jot_dir, b_OPTS.paths.tmp_dir};
    const std::string jf = wj.get_jotfile(wc, true) + '\n';

    if(jf == "" || jf.find(FIELD_DELIM) == std::string::npos) { return false; }
    else
    {
        std::string jf_name = jf.substr(jf.find(FIELD_DELIM) + 1, jf.length() - jf.find(FIELD_DELIM));
        wc.console_write_green_bold("Jotfile: " + jf_name + "\n");
        wc.console_write_hz_divider();
        std::string resp = wc.console_write_prompt("\nAdd this jotfile as a new bookmark? (y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { return false; }
        else
        {
            return wibble::WibbleIO::write_out_file_append(jf, jotfile_bm_file);           
        }
    }
}

/**
 * @brief Add a new Wibble Node bookmark. 
 */
bool wibble::WibbleBookmark::_add_new_node_bm()
{
    search_filters s; // menu search, add bookmark
    wibble::WibbleUtility::menu_search_set_filters(wc, s);
    WibbleRecord w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, s);

    // ensure stream flushing for input (after potential mass record retrieval with sync_with_stdio = false)
    std::cout << std::unitbuf;
    if(w.get_id() == "___NULL___") { return false; }
    else
    {
        w.pretty_print_record_full();
        std::cout << std::endl;
        std::string resp = wc.console_write_prompt("Add this record as a new bookmark? (y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { return false; }
        else
        {
            std::string title = wc.console_write_prompt("Input title/descriptive name of this bookmark > ", "", ""); 
            wc.trim(title); wibble::WibbleRecord::remove_pipe(title);
            if(title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); return false; }
            std::string record = "";
            if(! s.archive_db) { record = _generate_entry_hsh() + "|" + w.get_id() + "|M|" + title + "\n"; }
            else               { record = _generate_entry_hsh() + "|" + w.get_id() + "|A|" + title + "\n"; }

            return wibble::WibbleIO::write_out_file_append(record, node_bm_file);           
        }
    }
}

/**
 * @brief Add a new Wibble Task entry bookmark.
 */
bool wibble::WibbleBookmark::_add_new_task_bm()
{
    WibbleTask wtsk{wib_e.get_tasks_dir(), wib_e.get_tmp_dir(), wib_e.get_templates_dir(), wib_e.get_bin_mapping()};
    WibbleTask::task_entry e = wtsk.pick_task_entry();
    if(e.task_id == "___NULL___") { wc.console_write_error_red("No valid task selected. No bookmark added."); return false; }
    wtsk.print_task_detailed(e);
    std::string resp = wc.console_write_prompt("Add above task as a bookmark? (y/n) > ", "", "");
    if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); return false; }

    const std::string record = e.task_id + FIELD_DELIM + e.category + FIELD_DELIM + e.title + '\n';
    return wibble::WibbleIO::write_out_file_append(record, task_bm_file);
}

/**
 * @brief Add a new Wibble Topic entry bookmark. 
 */
bool wibble::WibbleBookmark::_add_new_topic_bm()
{
    const std::string topic_record = wibble::WibbleActions().generate_topic_entry_link_interactive(wc, wib_e);
    if(topic_record == "") { return false; }
    else
    {
        return wibble::WibbleIO::write_out_file_append(topic_record, topic_bm_file);
    }          
        
}

/**
 * @brief Add a new Wibble command bookmark.
 */
bool wibble::WibbleBookmark::_add_new_wibble_cmd_bm()
{
    try
    {
        std::string cmd = wc.console_write_prompt("Input Wibble subcommand > ", "wibble", "");
        wc.trim(cmd);
        // get rid of 'wibble' if they've unnecessarily re-input it
        if(cmd.find("wibble") == 0) { cmd = cmd.substr(7, std::string::npos); }
        wc.console_write_hz_divider();
        wc.console_write_green_bold("wibble " + cmd + "\n");
        wc.console_write_hz_divider();
        std::string resp = wc.console_write_prompt("Add this Wibble command as a new bookmark? (y/n) > ", "", "");
        if(resp != "y" && resp != "Y") { return false; }
        else
        {
            std::string title = wc.console_write_prompt("Input title/descriptive name of this bookmark > ", "", ""); 
            wc.trim(title); wibble::WibbleRecord::remove_pipe(title);
            if(title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); return false; }
            std::string record = "";
            // put the title first in case the command itself contains pipes (|)
            record = _generate_entry_hsh() + "|" + title + "|" + cmd + "\n";

            return wibble::WibbleIO::write_out_file_append(record, wib_exec_bm_file);           
        }
    }
    catch(std::range_error& re)
    {
        return false;
    }
}

/**
 * @brief Helper function to easily print grey letter on blue background for menu.
 */
void wibble::WibbleBookmark::_anum(const std::string& ltr)
{
    wc.console_write_grey_on_blue("[" + ltr + "]");
}

/**
 * @brief Helper function to ensure any newly created custom category has a unique non-duplicated name.
 */
bool wibble::WibbleBookmark::_check_cat_name_is_unique(const std::string& cat_name)
{
    for(auto const& entry: _get_all_cust_category_names()) { if(cat_name == entry) { return false; } }
    return true;
}

/**
 * @brief Interactive confirmation before deletion of any command bookmark. 
 * @param exec_cmd String representing either executable command or name of executable script
 * @param title Title of command bookmark
 * @param custom_entry Flag to indicate whether bookmark is within a custom category or general command bookmark
 */
std::string wibble::WibbleBookmark::_check_delete_script_warning(const std::string& exec_cmd, const std::string& title, const bool custom_entry)
{
    std::string exec_scr = "";
    if(exec_cmd.find(EXEC_PREFIX) != std::string::npos)
    {
        exec_scr = wib_e.get_scripts_dir() + BOOKMK_SCR + "/" + exec_cmd.substr(EXEC_PREFIX.length(), std::string::npos);
        wc.console_write_red_bold("WARNING. This bookmark has an associated script:\n\n");
        wc.console_write_red(exec_scr + "\n");
        wc.console_write_red_bold("\nRemoving this bookmark will permanently delete this script.\n");
    }
    wc.console_write_hz_divider();
    if(! custom_entry) { wc.console_write_red_bold("[ General Command Bookmark pending deletion ]\n"); }
    else               { wc.console_write_red_bold("[ Custom Command Bookmark pending deletion ]\n"); }
    wc.console_write_red(ARW + "'" + title + "'\n");
    if(exec_scr != "")
    {
        wc.console_write_red_bold("[ Associated script pending deletion ]\n");
        wc.console_write_red(ARW + "'" + exec_scr + "'\n");
        wc.console_write_hz_divider();
        wc.console_write_red("\nThe bookmark and script file for above command entry will be removed.\n");
    }
    else
    {
        wc.console_write_hz_divider();
        wc.console_write_red("\nThe bookmark for above command entry will be removed.\n");
    }
    wc.console_write_red_bold("Input 'delete' to confirm.\n");
    return exec_scr;
}

/**
 * @brief Helper wrapper for pretty-printing category numbers.
 * @param entry_num Integer number to present of category in menu
 */
void wibble::WibbleBookmark::_cnum(int entry_num)
{
    wc.console_write_padded_selector(entry_num, 9, " ");
}

/**
 * @brief Deletion routine for removing custom category command bookmarks.
 * @param entry Bookmark struct containing bookmark information
 * @param cbm_def Custom bookmark structure definition
 */
bool wibble::WibbleBookmark::_delete_existing_custom_cmd_bm(wbe& entry, cbm& cbm_def)
{
    std::string sf = _check_delete_script_warning(entry.field1, entry.title, true);
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        //bool index_delete = _remove_line_from_index(wibble::WibbleBookmark::bookmark_type::CUST_EXEC_BM, entry, cbm_def.full_id);
        bool index_delete = _remove_line_from_index(entry, cbm_def.full_id);
        // remove any attached script file
        if(sf != "" && index_delete && std::filesystem::exists(std::filesystem::path(sf)))
        {
            bool sf_del = std::filesystem::remove(std::filesystem::path(sf));
            if(sf_del) { wc.console_write_success_green("Script file '" + sf + "' deleted."); }
        }
        return index_delete;
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
}

/**
 * @brief Deletion routine for removing Wibble Jotfile bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_delete_existing_jotfile_bm(wbe& entry)
{
    wc.console_write_yellow("\nNOTE: The underlying jotfile will NOT be affected.\nThis simply removes the bookmark to it.\n\n");
    wc.console_write_hz_divider();
    wc.console_write_red_bold("[ Jotfile Bookmark pending deletion ]\n");
    wc.console_write_red(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above jotfile will be removed.\n");
    wc.console_write_red_bold("Input 'delete' to confirm.\n");
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        return _remove_line_from_index(entry);
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
}

/**
 * @brief Deletion routine for removing Wibble Node bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_delete_existing_node_bm(wbe& entry)
{
    wc.console_write_yellow("\nNOTE: The underlying node will NOT be affected.\nThis simply removes the bookmark to it.\n\n");
    wc.console_write_hz_divider();
    wc.console_write_red_bold("[ Node Bookmark pending deletion ]\n");
    wc.console_write_red(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above node will be removed.\n");
    wc.console_write_red_bold("Input 'delete' to confirm.\n");
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        return _remove_line_from_index(entry);
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
}

/**
 * @brief Deletion routine for removing Wibble Task bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_delete_existing_task_bm(bookmark_entry& entry)
{
    wc.console_write_yellow("\nNOTE: The underlying task will NOT be affected.\nThis simply removes the bookmark to it.\n\n");
    wc.console_write_hz_divider();
    wc.console_write_red_bold("[ Task Bookmark pending deletion ]\n");
    wc.console_write_red(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above task will be removed.\n");
    wc.console_write_red_bold("Input 'delete' to confirm.\n");
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        return _remove_line_from_index(entry);
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
}

/**
 * @brief Deletion routine for removing Wibble Topic entry bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_delete_existing_topic_bm(wbe& entry)
{
    wc.console_write_yellow("\nNOTE: The underlying topic entry will NOT be affected.\nThis simply removes the bookmark to it.\n\n");
    wc.console_write_hz_divider();
    wc.console_write_red_bold("[ Topic Bookmark pending deletion ]\n");
    wc.console_write_red(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above topic entry will be removed.\n");
    wc.console_write_red_bold("Input 'delete' to confirm.\n");
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        return _remove_line_from_index(entry);
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
}

/**
 * @brief Deletion routine for removing general command bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_delete_existing_general_cmd_bm(wbe& entry)
{
    std::string sf = _check_delete_script_warning(entry.field1, entry.title, false);
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        bool index_delete = _remove_line_from_index(entry);
        // remove any attached script file
        if(sf != "" && index_delete && std::filesystem::exists(std::filesystem::path(sf)))
        {
            bool sf_del = std::filesystem::remove(std::filesystem::path(sf));
            if(sf_del) { wc.console_write_success_green("Script file '" + sf + "' deleted."); }
        }
        return index_delete;
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
}

/**
 * @brief Deletion routine for removing Wibble command bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_delete_existing_wibble_cmd_bm(wbe& entry)
{
    wc.console_write_hz_divider();
    wc.console_write_red_bold("[ Wibble Command Bookmark pending deletion ]\n");
    wc.console_write_red(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above Wibble command will be removed.\n");
    wc.console_write_red_bold("Input 'delete' to confirm.\n");
    std::string confirm = wc.console_write_prompt("> ", "", "");
    if(confirm == "delete")
    {
        return _remove_line_from_index(entry);
    }
    else
    {
        wc.console_write_error_red("Deletion cancelled.");
        return false;
    }
   
}

/**
 * @brief Switching logic for interactively adding a new bookmark.
 * @param choice Integer representing menu selection/bookmark type
 */
void wibble::WibbleBookmark::_dispatch_add_bm_action(const int& choice)
{
    bool add_result = false;
    switch(choice)
    {
    case 1:
        add_result = _add_new_node_bm();
        if(add_result) { wc.console_write_success_green("New node bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding node bookmark.");                     }
        break;
    case 2:
        add_result = _add_new_topic_bm();
        if(add_result) { wc.console_write_success_green("New topic bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding topic bookmark.");                     }
        break;
    case 3: //jotfile bm
        add_result = _add_new_jotfile_bm();
        if(add_result) { wc.console_write_success_green("New jotfile bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding jotfile bookmark.");                     }
        break;
    case 4: //task bm
        add_result = _add_new_task_bm();
        if(add_result) { wc.console_write_success_green("New task bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding task bookmark.");                     }
        break;
    case 5: //cmd bm
        add_result = _add_new_wibble_cmd_bm();
        if(add_result) { wc.console_write_success_green("New Wibble command bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding Wibble command bookmark.");                     }
        break;
    case 6: //general cmd bm
        add_result = _add_new_general_cmd_bm();
        if(add_result) { wc.console_write_success_green("New shell command bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding shell command bookmark.");                     }
        break;
    case 7: //custom cmd bm
        add_result = _add_new_custom_bm();
        if(add_result) { wc.console_write_success_green("New shell command bookmark added successfully. Now present in menu."); }
        else           { wc.console_write_error_red("User cancelled/Error adding shell command bookmark.");                     }
        break;

    }
}

/**
 * @brief Main switching logic for interactive menu.
 * @param choice Integer representing menu selection/operation
 */
void wibble::WibbleBookmark::_dispatch_bm_action(const int& choice)
{
    switch(choice)
    {
    case 1:
        _add_new_bm_menu();
        break;
    case 2:
        _edit_bm_menu();
        break;
    case 3:
        _delete_bm_menu();
        break;
    case 4:
        _add_new_custom_category();
        break;
    case 5:
        cust_ren_category();
        break;
    case 6:
        _remove_custom_category();
        break;
    case 7:
        cust_category_activate();
        break;
    case 8:
        cust_category_hibernate();
        break;
    default:
        wc.console_write_error_red("Invalid choice. Exiting");
    }

}

/**
 * @brief Main switching logic for interactive bookmark removal menu.
 */
void wibble::WibbleBookmark::_delete_bm_menu()
{
    wc.console_write_header("DELETE");
    _anum("A"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing "); 
    wc.console_write_yellow_bold("node"); wc.console_write_yellow(" bookmark\n");
    _anum("B"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing ");
    wc.console_write_yellow_bold("topic"); wc.console_write_yellow(" bookmark\n");
    _anum("C"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing ");
    wc.console_write_yellow_bold("jotfile"); wc.console_write_yellow(" bookmark\n");
    _anum("D"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing ");
    wc.console_write_yellow_bold("task"); wc.console_write_yellow(" bookmark\n");
    _anum("E"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing ");
    wc.console_write_yellow_bold("Wibble command"); wc.console_write_yellow(" bookmark\n");
    _anum("F"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing ");
    wc.console_write_yellow_bold("General command"); wc.console_write_yellow(" bookmark\n");
    _anum("G"); std::cout << " ┃ "; wc.console_write_yellow("Delete an existing ");
    wc.console_write_yellow_bold("Custom category command"); wc.console_write_yellow(" bookmark\n");
    std::string choice = wc.console_write_prompt("\nSelect option > ", "", "");

    wc.trim(choice);
    int choice_num = 0;
    if(choice == "a" || choice == "A") { choice_num = 1; }
    if(choice == "b" || choice == "B") { choice_num = 2; }
    if(choice == "c" || choice == "C") { choice_num = 3; }
    if(choice == "d" || choice == "D") { choice_num = 4; }
    if(choice == "e" || choice == "E") { choice_num = 5; }
    if(choice == "f" || choice == "F") { choice_num = 6; }
    if(choice == "g" || choice == "G") { choice_num = 7; }
    try
    {
        if(choice_num >= 1 || choice_num <= 7)
        {
            _dispatch_delete_bm_action(choice_num);
        }
        else
        {
            wc.console_write_error_red("Invalid selection, exiting.");
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection, exiting.");
    }
}

/**
 * @brief Main switching logic for interactive selection of a bookmark.
 * @param index Integer representing menu selection/bookmark type
 */
void wibble::WibbleBookmark::_dispatch_category(const int& index)
{
    switch(index)
    {
    case 1:
        select_node_bm();
        break;
    case 2:
        select_topic_bm();
        break;
    case 3:
        select_jotfile_bm();
        break;
    case 4:
        select_task_bm();
        break;
    case 5:
        select_wib_exec_bm();
        break;
    case 6:
        select_gen_exec_bm();
        break;
    case 7:
        select_cust_exec_bm();
        break;
    case 8:
        _select_bm_edit_menu();
        break;
    default:
        wc.console_write_error_red("Invalid choice. Exiting");
    }
}

/**
 * @brief Dispatcher for deletion of a particular type of bookmark.
 * @param choice Integer representing selected candidate/bookmark type
 */
void wibble::WibbleBookmark::_dispatch_delete_bm_action(const int& choice)
{
    bool delete_result = false;
    switch(choice)
    {
    case 1:
        delete_result = select_node_bm(2);
        if(delete_result) { wc.console_write_success_green("Node bookmark deleted successfully. Removed from menu."); }
        else           { wc.console_write_error_red("User cancelled/Error deleting node bookmark.");                  }
        break;
    case 2:
        delete_result = select_topic_bm(2);
        if(delete_result) { wc.console_write_success_green("Topic bookmark deleted successfully. Removed from menu."); }
        else           { wc.console_write_error_red("User cancelled/Error deleting topic bookmark.");                  }
        break;
    case 3: //jotfile bm
        delete_result = select_jotfile_bm(2);
        if(delete_result) { wc.console_write_success_green("Jotfile bookmark deleted successfully. Removed from menu."); }
        else              { wc.console_write_error_red("User cancelled/Error deleting jotfile bookmark.");               }
        break;
    case 4: //task bm
        delete_result = select_task_bm(2);
        if(delete_result) { wc.console_write_success_green("Task bookmark deleted successfully. Removed from menu."); }
        else           { wc.console_write_error_red("User cancelled/Error deleting task bookmark.");                  }
        break;
    case 5: //wibble cmd bm
        delete_result = select_wib_exec_bm(2);
        if(delete_result) { wc.console_write_success_green("Wibble command bookmark deleted successfully. Removed from menu."); }
        else           { wc.console_write_error_red("User cancelled/Error deleting Wibble command bookmark.");                  }
        break;
    case 6: //general cmd bm
        delete_result = select_gen_exec_bm(2);
        if(delete_result) { wc.console_write_success_green("Shell command bookmark deleted successfully. Removed from menu."); }
        else           { wc.console_write_error_red("User cancelled/Error deleting shell command bookmark.");                  }
        break;
    case 7: //custom cmd bm
        delete_result = select_cust_exec_bm(2);
        if(delete_result) { wc.console_write_success_green("Shell command bookmark deleted successfully. Removed from menu."); }
        else           { wc.console_write_error_red("User cancelled/Error deleting shell command bookmark.");                  }
        break;
    case 8: //custom cmd bm
        delete_result = _remove_custom_category();
        if(delete_result) { wc.console_write_success_green("Custom category removed from menu.");         }
        else              { wc.console_write_error_red("User cancelled/Error removing custom category."); }

    }
}

/**
 * @brief Dispatcher for editing of a particular type of bookmark.
 * @param choice Integer representing selected candidate/bookmark type
 */
void wibble::WibbleBookmark::_dispatch_edit_bm_action(const int& choice)
{
    bool edit_result = false;
    switch(choice)
    {
    case 1:
        edit_result = select_node_bm(3);
        if(edit_result) { wc.console_write_success_green("Node bookmark edited successfully.");                }
        else            { wc.console_write_error_red("User cancelled/Error editing node bookmark.");           }
        break;
    case 2:
        edit_result = select_topic_bm(3);
        if(edit_result) { wc.console_write_success_green("Topic bookmark edited successfully.");               }
        else            { wc.console_write_error_red("User cancelled/Error editing topic bookmark.");          }
        break;
    case 3: //jotfile bm
        edit_result = select_jotfile_bm(3);
        if(edit_result) { wc.console_write_success_green("Jotfile bookmark edited successfully.");             }
        else            { wc.console_write_error_red("User cancelled/Error editing jotfile bookmark.");        }
        break;
    case 4: //task bm
        edit_result = select_task_bm(3);
        if(edit_result) { wc.console_write_success_green("Task bookmark edited successfully.");                }
        else            { wc.console_write_error_red("User cancelled/Error editing task bookmark.");           }
        break;
    case 5: //wibble cmd bm
        edit_result = select_wib_exec_bm(3);
        if(edit_result) { wc.console_write_success_green("Wibble command bookmark edited successfully.");      }
        else            { wc.console_write_error_red("User cancelled/Error editing Wibble command bookmark."); }
        break;
    case 6: //general cmd bm
        edit_result = select_gen_exec_bm(3);
        if(edit_result) { wc.console_write_success_green("Shell command bookmark edited successfully.");       }
        else            { wc.console_write_error_red("User cancelled/Error editing shell command bookmark.");  }
        break;
    case 7: //custom cmd bm
        edit_result = select_cust_exec_bm(3);
        if(edit_result) { wc.console_write_success_green("Shell command bookmark edited successfully.");       }
        else            { wc.console_write_error_red("User cancelled/Error editing shell command bookmark.");  }
        break;
    }
}

/**
 * @brief Interactive menu for selection of type of bookmark to edit.
 */
void wibble::WibbleBookmark::_edit_bm_menu()
{
    wc.console_write_header("EDIT");
    _anum("A"); std::cout << " ┃ "; wc.console_write_yellow("Edit a "); 
    wc.console_write_yellow_bold("node"); wc.console_write_yellow(" bookmark\n");
    _anum("B"); std::cout << " ┃ "; wc.console_write_yellow("Edit a ");
    wc.console_write_yellow_bold("topic"); wc.console_write_yellow(" bookmark\n");
    _anum("C"); std::cout << " ┃ "; wc.console_write_yellow("Edit a ");
    wc.console_write_yellow_bold("jotfile"); wc.console_write_yellow(" bookmark\n");
    _anum("D"); std::cout << " ┃ "; wc.console_write_yellow("Edit a ");
    wc.console_write_yellow_bold("task"); wc.console_write_yellow(" bookmark\n");
    _anum("E"); std::cout << " ┃ "; wc.console_write_yellow("Edit a ");
    wc.console_write_yellow_bold("Wibble command"); wc.console_write_yellow(" bookmark\n");
    _anum("F"); std::cout << " ┃ "; wc.console_write_yellow("Edit a ");
    wc.console_write_yellow_bold("General command"); wc.console_write_yellow(" bookmark\n");
    _anum("G"); std::cout << " ┃ "; wc.console_write_yellow("Edit a ");
    wc.console_write_yellow_bold("Custom category command"); wc.console_write_yellow(" bookmark\n");
    std::string choice = wc.console_write_prompt("\nSelect option > ", "", "");

    wc.trim(choice);
    int choice_num = 0;
    if(choice == "a" || choice == "A") { choice_num = 1; }
    if(choice == "b" || choice == "B") { choice_num = 2; }
    if(choice == "c" || choice == "C") { choice_num = 3; }
    if(choice == "d" || choice == "D") { choice_num = 4; }
    if(choice == "e" || choice == "E") { choice_num = 5; }
    if(choice == "f" || choice == "F") { choice_num = 6; }
    if(choice == "g" || choice == "G") { choice_num = 7; }
    try
    {
        if(choice_num >= 1 || choice_num <= 7)
        {
            _dispatch_edit_bm_action(choice_num);
        }
        else
        {
            wc.console_write_error_red("Invalid selection, exiting.");
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection, exiting.");
    }
}

/**
 * @brief Method for editing an existing command bookmark.
 * @param cmd_field Shell command/bookmark command, or filename of script if it contains EXEC_PREFIX
 */
std::string wibble::WibbleBookmark::_edit_cmd_wrapper(const std::string& cmd_field)
{
    std::string cmd;
    std::size_t ep = cmd_field.find(EXEC_PREFIX);
    if(ep != std::string::npos)
    {
        cmd = cmd_field;
        std::string resp = wc.console_write_prompt("Bookmark uses a command script. Edit? (y/n) > ", "", "");
        wc.trim(resp);
        if(resp == "y" || resp == "Y")
        {
            const std::string script_stb = cmd_field.substr(ep + EXEC_PREFIX.length(), cmd_field.length() - 1 - ep);
            const std::string script_fn = wib_e.get_scripts_dir() + BOOKMK_SCR + "/" + script_stb;
            //std::cout << "DEBUG: script_fn = " << script_fn << std::endl;
            _edit_bm_script(script_fn);
        }
        else
        { return "NO_EDIT"; }
    }
    else
    {
        wc.console_write_yellow_bold("Adjust command.\n");
        cmd = wc.console_write_prompt("> ", "", cmd_field);
        wc.trim(cmd);
    }

    return cmd;
}

/**
 * @brief Wrapper method for editing an existing command bookmark.
 * @param entry Bookmark struct containing bookmark information
 * @param cbm_def Custom bookmark structure definition
 */
bool wibble::WibbleBookmark::_edit_existing_custom_cmd_bm(wbe& entry, cbm& cbm_def)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit Custom Command Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above custom command will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wibble::WibbleRecord::remove_pipe(edit);
    wc.trim(edit);

    const bool direct_cmd = (entry.field1.find(EXEC_PREFIX) == std::string::npos) ? true : false;
    std::string cmd = _edit_cmd_wrapper(entry.field1);

    if(edit == "" || cmd == "")
    {
        wc.console_write_error_red("New title and/or command cannot be blank.");
        return false;
    }
    else if(edit == entry.title && (cmd == entry.field1 || cmd == "NO_EDIT")) // no need to update bookmark entry itself
    {
        if(direct_cmd || cmd == "NO_EDIT") { wc.console_write_red(ARW + "No adjustment made.\n");   return false; }
        else                               { wc.console_write_green(ARW + "Script file edited.\n"); return true;  }
    }   
    else
    {
        if(cmd == "NO_EDIT") { cmd = entry.field1; }

        const std::string record_line = entry.field2 + FIELD_DELIM + entry.cust_cat_name + FIELD_DELIM + edit + FIELD_DELIM + cmd; 
        const std::string cust_file = wib_e.get_bookmarks_dir() + "/" + cbm_def.full_id + ".wib";

        // function can update as well as remove
        return _remove_line_from_file(entry.field2, cust_file, record_line);
    }
}

/**
 * @brief Wrapper method for editing an existing Jotfile bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_edit_existing_jotfile_bm(wbe& entry)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit Jotfile Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above jotfile will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wc.trim(edit);
    if(edit == "")
    {
        wc.console_write_error_red("New title cannot be blank.");
        return false;
    }
    else if(edit == entry.title)
    {
        wc.console_write_red(ARW + "No adjustment made.\n");
        return false;
    }
    else
    {
        const std::string record_line = entry.field1 + FIELD_DELIM + edit; 

        // function can update as well as remove
        return _remove_line_from_file(entry.field1, jotfile_bm_file, record_line);
    }
}

/**
 * @brief Wrapper method for editing an existing Node bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_edit_existing_node_bm(wbe& entry)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit Node Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above node will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wc.trim(edit);
    if(edit == "")
    {
        wc.console_write_error_red("New title cannot be blank.");
        return false;
    }
    else if(edit == entry.title)
    {
        wc.console_write_red(ARW + "No adjustment made.\n");
        return false;
    }
    else
    {
        std::string record_line = entry.field2 + FIELD_DELIM + entry.field1; 
        if(entry.archived_node) { record_line.append("|A|" + edit); }
        else                    { record_line.append("|M|" + edit); }

        // function can update as well as remove
        return _remove_line_from_file(entry.field2, node_bm_file, record_line);
    }
}

/**
 * @brief Wrapper method for editing an existing Task bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_edit_existing_task_bm(wbe& entry)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit Task Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above task will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wc.trim(edit);
    if(edit == "")
    {
        wc.console_write_error_red("New title cannot be blank.");
        return false;
    }
    else if(edit == entry.title)
    {
        wc.console_write_red(ARW + "No adjustment made.\n");
        return false;
    }
    else
    {
        // field1 is task id
        std::string record_line = entry.field1 + FIELD_DELIM + entry.field2 + FIELD_DELIM + edit; 

        // function can update as well as remove
        return _remove_line_from_file(entry.field1, task_bm_file, record_line);
    }
}

/**
 * @brief Wrapper method for editing an existing Topic bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_edit_existing_topic_bm(wbe& entry)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit Topic Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above topic will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wc.trim(edit);
    if(edit == "")
    {
        wc.console_write_error_red("New title cannot be blank.");
        return false;
    }
    else if(edit == entry.title)
    {
        wc.console_write_red(ARW + "No adjustment made.\n");
        return false;
    }
    else
    {
        // record structure:
        // <uniq hsh> | <topic id> |  <E> or <D> | <category> | <path releative to topic base dir> | <title>
        // field6 includes topic id. E/D: whether it is a text or data topic bookmark
        std::string record_line = entry.field6 + FIELD_DELIM + entry.field2 + FIELD_DELIM +
                                  entry.field3 + FIELD_DELIM + entry.field5 + FIELD_DELIM +
                                  entry.field4 + FIELD_DELIM + edit;
                                   
        // function can update as well as remove
        return _remove_line_from_file(entry.field6, topic_bm_file, record_line);
    }
}

/**
 * @brief Wrapper method for editing an existing general command bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_edit_existing_general_cmd_bm(wbe& entry)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit General Command Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above node will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wibble::WibbleRecord::remove_pipe(edit);
    wc.trim(edit);

    const bool direct_cmd = (entry.field1.find(EXEC_PREFIX) == std::string::npos) ? true : false;
    std::string cmd = _edit_cmd_wrapper(entry.field1);

    if(edit == "" || cmd == "")
    {
        wc.console_write_error_red("New title and/or command cannot be blank.");
        return false;
    }
    else if(edit == entry.title && (cmd == entry.field1 || cmd == "NO_EDIT")) // no need to update bookmark entry itself
    {
        if(direct_cmd || cmd == "NO_EDIT") { wc.console_write_red(ARW + "No adjustment made.\n");   return false; }
        else                               { wc.console_write_green(ARW + "Script file edited.\n"); return true;  }
    }
    else
    {
        if(cmd == "NO_EDIT") { cmd = entry.field1; }
        const std::string record_line = entry.field2 + FIELD_DELIM + edit + FIELD_DELIM + cmd; 

        // function can update as well as remove
        return _remove_line_from_file(entry.field2, gen_exec_bm_file, record_line);
    }
}
    
/**
 * @brief Wrapper method for editing an existing Wibble command bookmark.
 * @param entry Bookmark struct containing bookmark information
 */
bool wibble::WibbleBookmark::_edit_existing_wibble_cmd_bm(wbe& entry)
{
    wc.console_write_header("EDIT");
    wc.console_write_yellow_bold("[ Edit Wibble Command Bookmark ]\n");
    wc.console_write_yellow(ARW + "'" + entry.title + "'\n");
    wc.console_write_hz_divider();
    wc.console_write_red("\nThe bookmark for above node will be adjusted.\n");
    wc.console_write_yellow_bold("Input replacement title.\n");
    std::string edit = wc.console_write_prompt("> ", "", entry.title);
    wc.trim(edit);
    wibble::WibbleRecord::remove_pipe(edit);
    wc.console_write_yellow_bold("Adjust Wibble command.\n");
    std::string cmd = wc.console_write_prompt("> ", "", entry.field1);
    wc.trim(cmd);
    // get rid of 'wibble' if they're re-input it
    if(cmd.find("wibble") == 0) { cmd = cmd.substr(7, std::string::npos); }
    if(edit == "" || cmd == "")
    {
        wc.console_write_error_red("New title and/or command cannot be blank.");
        return false;
    }
    else if(edit == entry.title && cmd == entry.field1)
    {
        wc.console_write_red(ARW + "No adjustment made.\n");
        return false;
    }
    else
    {
        const std::string record_line = entry.field2 + FIELD_DELIM + edit + FIELD_DELIM + cmd; 

        // function can update as well as remove
        return _remove_line_from_file(entry.field2, wib_exec_bm_file, record_line);
    }
}

/**
 * @brief Wrapper method for editing an existing command bookmark that is a script rather than simple command.
 * @param script_fn Filename of script file for bookmark
 */
void wibble::WibbleBookmark::_edit_bm_script(const std::string& script_fn)
{
    wc.console_write_hz_divider();
    wc.console_write_yellow("1. Edit new script/command with "); wc.console_write_yellow_bold("CLI"); wc.console_write_yellow(" editor.\n");
    wc.console_write_yellow("2. Edit new script/command with "); wc.console_write_yellow_bold("GUI"); wc.console_write_yellow(" editor.\n");
    wc.console_write_hz_divider();
    std::string option = wc.console_write_prompt("Select option > ", "", "");

    if(option == "1")      { wib_e.run_exec_expansion(wib_e.get_cli_editor(), script_fn); }
    else                   { wib_e.run_exec_expansion(wib_e.get_gui_editor(), script_fn); }
}

/**
 * @brief Wrapper to always ensure every bookmark entry has a guaranteed singular and unique record hash/identifier.
 */
std::string wibble::WibbleBookmark::_generate_entry_hsh()
{
    return wibble::WibbleRecord::generate_entry_hash();
}

/**
 * @brief Wrapper to enforce consist mapping to canonical bookmark file used to store given type of bookmarks.
 * @param choice Integer mapping of bookmark type
 */
std::string wibble::WibbleBookmark::index_to_lookup_name(const int& choice)
{
    switch(choice)
    {
    case 1: // node bookmarks
        return BM_CAT_1;
        break;
    case 2: // topic bookmarks
        return BM_CAT_2;
        break;
    case 3: // jotfile bookmarks
        return BM_CAT_3;
        break;
    case 4: // task bookmarks
        return BM_CAT_4;
        break;
    case 5: // wibble command bookmarks
        return BM_CAT_5;
        break;
    case 6: // general command bookmarks
        return BM_CAT_6;
        break;
    case 7: // custom category command bookmarks
        return CUST_CAT_MAP;
    default: // invalid type/default error handler
        return "ERROR";
    } 
}

/**
 * @brief Main function that parses record line from bookmark file into a bookmark struct. 
 * @param line String representing the bookmark record/entry in file
 * @param type Integer representing the bookmark type to process/parse
 */
wbe wibble::WibbleBookmark::_pack_entry_from_line(const std::string& line, const int& type)
{
    wbe entry;
    try
    {
        if(type == 1) // node bookmarks
        {
            // first field is unique hash 
            size_t hsh_sep         = line.find(FIELD_DELIM);
            size_t node_id_sep     = line.find(FIELD_DELIM, hsh_sep + 1);
            size_t ark_status_sep  = line.find(FIELD_DELIM, node_id_sep + 1);

            std::string uniq_hsh   = line.substr(0, hsh_sep);
            std::string node_id    = line.substr(hsh_sep + 1, (node_id_sep - 1 - hsh_sep));
            std::string ark_status = line.substr(node_id_sep + 1, 1);
            std::string title      = line.substr(ark_status_sep + 1, line.length() - ark_status_sep);

            entry.field1 = node_id;
            entry.field2 = uniq_hsh;
            if(ark_status == "A") { entry.archived_node = true; } // false in default constructor
            entry.title = title;
            entry.type = WibbleBookmark::bookmark_type::NODE_BM;
            //std::cout << "DEBUG: added node hsh: " << entry.field2 << std::endl;
        }
        if(type == 2) // topic bookmarks
        {
            //format to parse:
            //<uniq hsh>
            //|TESTING_20240128193211|TESTING|D|general|store/general/TESTING_20240128193211/2024/20240128234159_e7d4c8b7_rsync-copy-20240128-171501.log|Rsync log

            //<uniq hsh> | <topic id> |  <E> or <D> | <category> | <path releative to topic base dir> | <title>

            // replace entry with value from external function; shared with 'task' functionality
            entry = wibble::WibbleActions().deserialize_topic_bm(line);
            entry.type = WibbleBookmark::bookmark_type::TOPIC_BM;
        }
        if(type == 3) // jotfile bookmarks
        {
            size_t title_sep = line.find(FIELD_DELIM);
            std::string jotfile_fn    = line.substr(0, title_sep);
            std::string jotfile_title = line.substr(title_sep + 1, (line.length() - title_sep));
            entry.field1 = jotfile_fn; 
            entry.title  = jotfile_title;
            entry.type   = WibbleBookmark::bookmark_type::JF_BM;
        }
        if(type == 4) // task bookmarks
        {
            size_t hsh_sep       = line.find(FIELD_DELIM);
            size_t title_sep     = line.find(FIELD_DELIM, hsh_sep + 1);
            std::string task_id  = line.substr(0, hsh_sep);
            std::string task_cat = line.substr(hsh_sep + 1, (title_sep - 1 - hsh_sep));
            std::string title    = line.substr(title_sep + 1, (line.length() - title_sep));
            entry.field1 = task_id; 
            entry.field2 = task_cat;
            entry.title  = title;
            entry.type   = WibbleBookmark::bookmark_type::TASK_BM;
        }
        if(type == 5 || type == 6) // wibble & general exec bookmarks
        {
            size_t hsh_sep       = line.find(FIELD_DELIM);
            size_t title_sep     = line.find(FIELD_DELIM, hsh_sep + 1);
            std::string uniq_hsh = line.substr(0, hsh_sep);
            std::string title    = line.substr(hsh_sep + 1, (title_sep - 1 - hsh_sep));
            std::string exec_cmd = line.substr(title_sep + 1, line.length() - title_sep);
            entry.field1 = exec_cmd;
            entry.field2 = uniq_hsh; 
            entry.title  = title;
            if(type == 5)      { entry.type = WibbleBookmark::bookmark_type::WEXEC_BM;     }
            else if(type == 6) { entry.type = WibbleBookmark::bookmark_type::GENEXEC_BM;   }
            //std::cout << "DEBUG: added exec hsh: " << entry.field2 << std::endl;
        }
        if(type == 7) // custom exec bookmarks
        {
            size_t hsh_sep       = line.find(FIELD_DELIM);
            size_t cat_sep       = line.find(FIELD_DELIM, hsh_sep + 1);
            size_t title_sep     = line.find(FIELD_DELIM, cat_sep + 1);
            //std::string exec_cmd = line.substr(exec_sep + 1, line.length() - exec_sep);
            //std::string title    = line.substr(title_sep + 1, (exec_sep - title_sep));

            std::string uniq_hsh = line.substr(0, hsh_sep);
            std::string title    = line.substr(cat_sep + 1, (title_sep - cat_sep - 1));
            std::string exec_cmd = line.substr(title_sep + 1, line.length() - title_sep);

            entry.cust_cat_name = line.substr(hsh_sep + 1, (cat_sep - 1 - hsh_sep));
            entry.title = title;
            entry.field1 = exec_cmd;
            entry.field2 = uniq_hsh; 

            entry.type = WibbleBookmark::bookmark_type::CUST_EXEC_BM;
            //std::cout << "DEBUG: added cust exec hsh: " << entry.field2 << std::endl;
        }
        if(type == 8) // for category map only
        {
            size_t cat_sep       = line.find(FIELD_DELIM);
            size_t active_sep    = line.find(FIELD_DELIM, cat_sep + 1);
            std::string cat_name = line.substr(0, cat_sep);
            std::string active_s = line.substr(cat_sep + 1, 1);
            std::string cat_id = line.substr(active_sep + 1, line.length() - active_sep);
            
            entry.field1 = cat_id + CUST_PREFIX + cat_name;
            entry.field2 = cat_id;
            entry.cust_cat_name = cat_name;
            entry.type = (active_s == "A") ? WibbleBookmark::bookmark_type::CUST_EXEC_BM :
                                             WibbleBookmark::bookmark_type::HIB_CUST_EXEC_BM;
            //std::cout << "DEBUG: added custom category name: " << cat_name << std::endl;
            //std::cout << "DEBUG: added custom category id name: " << entry.field1 << std::endl;
            //std::cout << "DEBUG: added custom category id: " << entry.field2 << std::endl;
            //std::cout << "DEBUG: hibernated status: " << active_s << std::endl;
        }
    }
    catch(std::range_error& re)
    {
        wc.console_write("Bad index line: " + line + "\n");
    }

    return entry;
}

/**
 * @brief Helper wrapper to get user menu selection option or direct bypass.
 * @param def_sel Integer value used for either direct selection or prompt for user input
 */
unsigned long wibble::WibbleBookmark::_prompt_entry(const int& def_sel)
{
    if(def_sel == -1)
    {
        std::string resp = wc.console_write_prompt("Select entry > ", "", "");
        return std::stol(resp);
    }
    else
    {
        wc.console_write_green(ARW + "Attempting direct selection of bookmark entry: [" + std::to_string(def_sel) + "]");
        wc.console_write("\n");
        return def_sel;
    }
}

/**
 * @brief Wrapper to read bookmark file in, then parse line by line into bookmark entries.
 * @param bm_type Integer value mapping type of bookmark file to read in
 * @param cust_cat_file Optional string representing category map file with canonical category [file]names
 */
std::vector<wbe> wibble::WibbleBookmark::_process_bm_file(const int bm_type, const std::string& cust_cat_file)
{
    std::vector<wbe> entries_wbe;
    try
    {
        //std::cout << "DEBUG: reading in node bookmarks from file: " << node_m_bm_file << std::endl;
        std::stringstream bm_ss;
        switch(bm_type)
        {
        case 1:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(node_bm_file);
            break;
        case 2:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(topic_bm_file);
            break;
        case 3:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(jotfile_bm_file);
            break;
        case 4:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(task_bm_file);
            break;
        case 5:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(wib_exec_bm_file);
            break;
        case 6:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(gen_exec_bm_file);
            break;
        case 7:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(cust_cat_file);
            break;
        case 8: // for custom category names only
        case 9:
            bm_ss = wibble::WibbleIO::read_file_into_stringstream(cat_map_file);
            break;
            
        }
        
        std::string line;
        while(std::getline(bm_ss, line, '\n')) 
        {
            if(line.find(FIELD_DELIM) != std::string::npos)
            {
                wbe entry;
                switch(bm_type)
                {
                case 1:
                    entry = _pack_entry_from_line(line, 1);
                    if(entry.type == bookmark_type::NODE_BM)  { entries_wbe.push_back(entry); }
                    break;
                case 2:
                    entry = _pack_entry_from_line(line, 2);
                    if(entry.type == bookmark_type::TOPIC_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 3:
                    entry = _pack_entry_from_line(line, 3);
                    if(entry.type == bookmark_type::JF_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 4:
                    entry = _pack_entry_from_line(line, 4);
                    if(entry.type == bookmark_type::TASK_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 5:
                    entry = _pack_entry_from_line(line, 5);
                    if(entry.type == bookmark_type::WEXEC_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 6:
                    entry = _pack_entry_from_line(line, 6);
                    if(entry.type == bookmark_type::GENEXEC_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 7:
                    entry = _pack_entry_from_line(line, 7);
                    if(entry.type == bookmark_type::CUST_EXEC_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 8:
                    entry = _pack_entry_from_line(line, 8);
                    if(entry.type == bookmark_type::CUST_EXEC_BM) { entries_wbe.push_back(entry); } 
                    break;
                case 9: // same read as case 8, diferent bookmark type result
                    entry = _pack_entry_from_line(line, 8);
                    if(entry.type == bookmark_type::HIB_CUST_EXEC_BM) { entries_wbe.push_back(entry); } 
                    break;
                }
            }
        }

    }
    catch(std::exception& ex) { } // can just return empty vector on error case
    return entries_wbe;
}

/**
 * @brief Dispatch out to WibbleActions for working with Node entry bookmarks (shared with WibbleTask).
 * @param w WibbleRecord (Wibble Node) record
 * @param archived Flag indicating whether Node is from archived database
 */
void wibble::WibbleBookmark::_dispatch_node_action_menu(WibbleRecord& w, const bool archived)
{
    wibble::WibbleActions().node_action_menu(wc, w, wib_e, archived);
}

/**
 * @brief Dispatch out to WibbleActions for working with Topic entry bookmarks (shared with WibbleTask)
 * @param fn String with filename of topic entry
 */
void wibble::WibbleBookmark::_dispatch_topic_action_menu(const std::string& fn)
{
    wibble::WibbleActions().topic_action_menu(wc, wib_e, fn);
}

/**
 * @brief Main parent method that retrieves/reads in all available bookmarks.
 */
void wibble::WibbleBookmark::_pack_all_bm_data()
{
    std::vector<bookmark_entry> node_bm = _get_all_node_bm();
    std::vector<bookmark_entry> topic_bm = _get_all_topic_bm();
    std::vector<bookmark_entry> jotfile_bm = _get_all_jotfile_bm();
    std::vector<bookmark_entry> task_bm = _get_all_task_bm();
    std::vector<bookmark_entry> wib_exec_bm = _get_all_wib_exec_bm();
    std::vector<bookmark_entry> gen_exec_bm = _get_all_gen_exec_bm();
    std::vector<bookmark_entry> custom_bm = _get_all_cust_exec_bm();

    // pack the map
    bookmarks[BM_CAT_1]      = node_bm;
    bookmarks[BM_CAT_2]      = topic_bm;
    bookmarks[BM_CAT_3]      = jotfile_bm;
    bookmarks[BM_CAT_4]      = task_bm;
    bookmarks[BM_CAT_5]      = wib_exec_bm;
    bookmarks[BM_CAT_6]      = gen_exec_bm;
    bookmarks[CUST_CAT_MAP]  = custom_bm;
              
    std::vector<std::string> cust_categories_names = _get_all_cust_category_names();
    for(std::string& category: cust_categories_names)
    {
        bookmarks["0" + CUST_PREFIX + category] = _get_all_cust_exec_bm_for_cat(category);
    }

    if(exec_args != "")
    {
        wc.console_write_yellow(ARW + "Execution arguments supplied: ");
        wc.console_write_yellow_bold(exec_args + "\n");
    }
}

/**
 * @brief Interactive method for removal of a custom bookmark category.
 */
bool wibble::WibbleBookmark::_remove_custom_category()
{
    try
    {
        std::cout << "\n";
        cbm cust = _get_display_custom_categories();
        std::vector<wbe> category = _get_all_cust_exec_bm_for_cat(cust.short_name);
        if(! category.empty())
        {
            wc.console_write_error_red("Unable to remove a non-empty category.");
            wc.console_write_error_red("Delete all of the bookmarks in the category first.");
            wc.console_write_red(ARW + "Custom category " + cust.short_name + " has " + std::to_string(category.size()) + " bookmark entry/entries.\n");
            return false;
        }
        else
        {
            // do removal
            // FIXME: should replace file copying/backup with function in wibble::WibbleUtility::create_backup_and_overwrite_file
            bool del_custom = false;

            bookmark_entry bm;
            bm.type = wibble::WibbleBookmark::bookmark_type::UNDEF;
            std::string id_name = cust.full_id.substr(0, cust.full_id.find("_"));
            bm.field1 = id_name;
            bool index_delete = _remove_line_from_index(bm);
            if(index_delete)
            {
                std::string cust_fn = wib_e.get_bookmarks_dir() + "/" + cust.full_id + ".wib";
                // remove backup file if it exists
                std::string bak_name = wib_e.get_bookmarks_dir() + "/" + id_name + "_BAKC_" + cust.short_name + ".wib.bak";
                if(std::filesystem::exists(std::filesystem::path(bak_name))) { std::filesystem::remove(std::filesystem::path(bak_name)); }
                if(std::filesystem::exists(std::filesystem::path(cust_fn)))
                {
                    del_custom = std::filesystem::remove(std::filesystem::path(cust_fn));
                    if(del_custom) { wc.console_write_success_green("Custom category '" + cust.short_name + "' removed successfully."); }
                }
                return del_custom;
            }
            else { return false; }
        }
    }
    catch(std::exception& ex)
    {
        return false;
    }
}

/**
 * @brief Wrapper method enabling removal (or update) of entry/line from given bookmark file.
 * @param unique_line_match String containing the unique hash/identifier to find deletion candidate(s)
 * @param index_fn String containing the filename of the bookmark file to parse
 * @param update_line Optional string enabling function to also replace/update rather than simply remove line 
 */
bool wibble::WibbleBookmark::_remove_line_from_file(const std::string& unique_line_match, const std::string& index_fn, const std::string& update_line)
{
    return wibble::WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, unique_line_match, index_fn, true, update_line);
}

/**
 * @brief Dispatcher method enabling removal (or update) of entry/line from given bookmark file.
 * @param entry Bookmark struct containing bookmark information
 * @param cust_index_file Optional string with filename of custom category when removing a custom command bookmark
 */
bool wibble::WibbleBookmark::_remove_line_from_index(bookmark_entry& entry, const std::string& cust_index_file)
{
    try
    {
        switch(entry.type)
        {
        case NODE_BM:
            return _remove_line_from_file(entry.field2, node_bm_file);
        case TOPIC_BM:
            return _remove_line_from_file(entry.field6, topic_bm_file);
        case JF_BM:
            return _remove_line_from_file(entry.field1, jotfile_bm_file);
        case TASK_BM:
            return _remove_line_from_file(entry.field1, task_bm_file);
        case WEXEC_BM:
            return _remove_line_from_file(entry.field2, wib_exec_bm_file);
        case GENEXEC_BM:
            return _remove_line_from_file(entry.field2, gen_exec_bm_file);
        case CUST_EXEC_BM:
            return _remove_line_from_file(entry.field2, wib_e.get_bookmarks_dir() + "/" + cust_index_file + ".wib");
        case UNDEF: // category delete
            return _remove_line_from_file(entry.field1, cat_map_file);
        default:
            return false;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error: " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Method to rework a custom category bookmark file if the user renames an existing custom category.
 * @param cat_file String with filename of index for bookmarks for a give custom category 
 * @param existing_sn String with current identifier/unique custom category (id) name
 * @param new_sn String with new/replacement identifier/unique custom category (id) name
 */
bool wibble::WibbleBookmark::_rename_cust_category_entries(const std::string& cat_file, const std::string& existing_sn, const std::string& new_sn)
{
    try
    {
        std::stringstream cat_ss = wibble::WibbleIO::read_file_into_stringstream(cat_file);
        std::string line;

        std::ofstream new_cat_f;
        new_cat_f.open(cat_file + ".new", std::ios::out | std::ios::trunc);
        while(getline(cat_ss, line, '\n'))
        {
            if(line.find(existing_sn) == std::string::npos) { throw std::invalid_argument("Invalid category map file. Aborting rename remapping."); }
            size_t hsh_sep       = line.find(FIELD_DELIM);
            size_t cat_sep       = line.find(FIELD_DELIM, hsh_sep + 1);
            size_t title_sep     = line.find(FIELD_DELIM, cat_sep + 1);

            const std::string uniq_hsh = line.substr(0, hsh_sep);
            const std::string title    = line.substr(cat_sep + 1, (title_sep - cat_sep - 1));
            const std::string exec_cmd = line.substr(title_sep + 1, line.length() - title_sep);

            const std::string upd_line = uniq_hsh + FIELD_DELIM + new_sn + FIELD_DELIM + title + FIELD_DELIM + exec_cmd + '\n';
            new_cat_f << upd_line;
        }

        std::stringstream().swap(cat_ss);
        new_cat_f.close();

        return wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, cat_file + ".new", cat_file);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _rename_cust_category_entries(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Main entry point (for interactive use) - presents top-level bookmark menu.
 */
void wibble::WibbleBookmark::_render_menu()
{
    wc.console_write_header("BOOKMARKS");
    wc.console_write_hz_divider();
    int entry_num = 0;
    _cnum(++entry_num);
    wc.console_write_yellow("Wibble Record (Node) Bookmarks\n");
    wc.console_write_green(ARW + "" + std::to_string(bookmarks[BM_CAT_1].size()) + " entries.\n");
    wc.console_write_hz_divider();
    _cnum(++entry_num);
    wc.console_write_yellow("Topic Bookmarks\n");
    wc.console_write_green(ARW + "" + std::to_string(bookmarks[BM_CAT_2].size()) + " entries.\n");
    wc.console_write_hz_divider();
    _cnum(++entry_num);
    wc.console_write_yellow("Jotfile Bookmarks\n");
    wc.console_write_green(ARW + "" + std::to_string(bookmarks[BM_CAT_3].size()) + " entries.\n");
    wc.console_write_hz_divider();
    _cnum(++entry_num);
    wc.console_write_yellow("Task Bookmarks\n");
    wc.console_write_green(ARW + "" + std::to_string(bookmarks[BM_CAT_4].size()) + " entries.\n");
    wc.console_write_hz_divider();
    _cnum(++entry_num);
    wc.console_write_yellow("Wibble Command Bookmarks\n");
    wc.console_write_green(ARW + "" + std::to_string(bookmarks[BM_CAT_5].size()) + " entries.\n");
    wc.console_write_hz_divider();
    _cnum(++entry_num);
    wc.console_write_yellow("General Command Bookmarks\n");
    wc.console_write_green(ARW + "" + std::to_string(bookmarks[BM_CAT_6].size()) + " entries.\n");
    wc.console_write_hz_divider();

    _cnum(++entry_num);
    wc.console_write_yellow("Custom Category Command Bookmarks");

    for(auto const& [cat_name, cust_bm]: bookmarks)
    {
        // all custom categories contain custom prefix in category key/name
        if(cat_name.find(CUST_PREFIX) != std::string::npos)
        {
            wc.console_write_yellow_bold("\n   •  " + cat_name.substr(CUST_PREFIX.length() + 1, std::string::npos));
            wc.console_write_green(" - " + std::to_string(cust_bm.size()) + " entries.");
        }
    }
    wc.console_write("");
    wc.console_write_hz_divider();
    _cnum(++entry_num);
    wc.console_write_yellow("Bookmark Editing Actions\n");
    wc.console_write_green_bold(ARW + "Create, edit, or delete bookmarks & categories.\n");
    wc.console_write_hz_divider();
    wc.console_write_hz_heavy_divider();

    bool valid_choice = true;
    int cat_num = -1;
    std::string choice = wc.console_write_prompt("\nInput option > ", "", "");
    try
    {
        wc.trim(choice);
        cat_num = std::stoi(choice);
        if(cat_num > entry_num || cat_num <= 0)  { valid_choice = false; }
    }
    catch(std::exception& ex)
    {
        valid_choice = false;
    }

    if(valid_choice)
    {
        // dispatch to bookmark category/type
        _dispatch_category(cat_num);
    }
    else
    { wc.console_write_error_red("Invalid menu choice. Valid selection: [1] - [" + std::to_string(entry_num) + "]. Aborting."); }
}

/**
 * @brief Update an existing line in a bookmark file (e.g. hibernating a custom category).
 * @param orig_line Line to look for/update
 * @param repl_line Replacement line to insert
 * @param update_file Output filename for the newly updated file
 */
bool wibble::WibbleBookmark::_update_bookmark_line(const std::string& orig_line, const std::string& repl_line, const std::string& update_file)
{
    // FIXME: This entire method can probably be eliminated and just use _remove_line_from_file instead
    try
    {
        std::stringstream bmf_ss = wibble::WibbleIO::read_file_into_stringstream(update_file);
        std::string line;
        std::string output = "";
        bool do_write = false;
        while(getline(bmf_ss, line, '\n'))
        {
            //std::cout << "Comparing:\n" << "1. Search line: '" << orig_line << "'\n"; 
            //                 std::cout  << "2. Curr line: '" << line << "'\n"; 
            if(line.find(orig_line) == std::string::npos) { output.append(line + '\n'); }
            else
            {
                output.append(repl_line + '\n');
                do_write = true;
            }
        }

        std::stringstream().swap(bmf_ss);

        if(do_write)
        {
            const std::string new_bm_fn = update_file + ".new";
            bool new_bm_file = wibble::WibbleIO::write_out_file_overwrite(output, new_bm_fn);
            if(new_bm_file)
            {
                bool result = wibble::WibbleUtility::create_backup_and_overwrite_file(cache_dir, new_bm_fn, update_file);
                return result;
            }
            else { wc.console_write_error_red("Error updating bookmark file/settings. Aborting."); return false; }
        }
        else
        {
            wc.console_write_green(ARW + "Nothing to update. Skipping write out.\n");
            return false;
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _update_bookmark_line(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Interactive menu for selecting main bookmark operation: adding, deleting, creating...
 */
void wibble::WibbleBookmark::_select_bm_edit_menu()
{
    std::cout << "\n";
    wc.console_write_header("ACTIONS");
    wc.console_write_hz_divider();
    _anum("A"); std::cout << " ┃ "; wc.console_write_yellow_bold("Add");        wc.console_write_yellow(" a new bookmark\n");
    _anum("B"); std::cout << " ┃ "; wc.console_write_yellow_bold("Edit");       wc.console_write_yellow(" an existing bookmark\n");
    _anum("C"); std::cout << " ┃ "; wc.console_write_yellow_bold("Delete");     wc.console_write_yellow(" an existing bookmark\n");
    _anum("D"); std::cout << " ┃ "; wc.console_write_yellow_bold("Create");     wc.console_write_yellow(" a new custom category\n");
    _anum("E"); std::cout << " ┃ "; wc.console_write_yellow_bold("Rename");     wc.console_write_yellow(" an existing custom category\n");
    _anum("F"); std::cout << " ┃ "; wc.console_write_yellow_bold("Remove");     wc.console_write_yellow(" an existing custom category\n");
    _anum("G"); std::cout << " ┃ "; wc.console_write_yellow_bold("Reactivate"); wc.console_write_yellow(" an existing hibernated custom category\n");
    _anum("H"); std::cout << " ┃ "; wc.console_write_yellow_bold("Hibernate");  wc.console_write_yellow(" an existing custom category\n");
    wc.console_write_hz_divider();
    std::string choice = wc.console_write_prompt("\nSelect option > ", "", "");

    wc.trim(choice);
    int choice_num = 0;
    if(choice == "a" || choice == "A") { choice_num = 1; }
    if(choice == "b" || choice == "B") { choice_num = 2; }
    if(choice == "c" || choice == "C") { choice_num = 3; }
    if(choice == "d" || choice == "D") { choice_num = 4; }
    if(choice == "e" || choice == "E") { choice_num = 5; }
    if(choice == "f" || choice == "F") { choice_num = 6; }
    if(choice == "g" || choice == "G") { choice_num = 7; }
    if(choice == "h" || choice == "H") { choice_num = 8; }
    try
    {
        if(choice_num >= 1 && choice_num <= 8)
        {
            _dispatch_bm_action(choice_num);
        }
        else
        {
            wc.console_write_error_red("Invalid selection, exiting.");
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection, exiting.");
    }


}

/**
 * @brief Public accessor for adding a new Wibble Node bookmark.
 */
void wibble::WibbleBookmark::add_new_node_bm()
{
    _dispatch_add_bm_action(1);
}

/**
 * @brief Public accessor for adding a new Wibble Topic entry bookmark.
 */
void wibble::WibbleBookmark::add_topic_bm()
{
    _dispatch_add_bm_action(2);
}

/**
 * @brief Public accessor for adding a new Wibble Jotfile bookmark.
 */
void wibble::WibbleBookmark::add_jotfile_bm()
{
    _dispatch_add_bm_action(3);
}

/**
 * @brief Public accessor for adding a new Wibble Task entry bookmark.
 */
void wibble::WibbleBookmark::add_task_bm()
{
    _dispatch_add_bm_action(4);
}

/**
 * @brief Public accessor for adding a new Wibble command bookmark.
 */
void wibble::WibbleBookmark::add_wib_exec_bm()
{
    _dispatch_add_bm_action(5);
}

/**
 * @brief Public accessor for adding a new general command bookmark.
 */
void wibble::WibbleBookmark::add_gen_exec_bm()
{
    _dispatch_add_bm_action(6);
}

/**
 * @brief Public accessor for displaying list of existing Wibble Node bookmarks.
 */
void wibble::WibbleBookmark::display_node_bm()
{
    wc.console_write("");
    wc.console_write_hz_divider();
    wc.console_write("");
    wc.console_write_header("NODE BM");
    size_t index = 0;
    size_t tot_records;
    tot_records = bookmarks[BM_CAT_1].size();
    for(auto const& bm: bookmarks[BM_CAT_1])
    {
        ++index;
        wc.console_write_padded_selector(index, tot_records, "");
        if(bm.archived_node) {   wc.console_write_red("A"); }
        else                 { wc.console_write_green("M"); }
        std::cout << " ┃ ";
        //wc.console_write_yellow_bold("(" + bm.node_bm_id.substr(0,5) + ")");
        wc.console_write_yellow_bold("(" + bm.field1.substr(0,5) + ")");
        wc.console_write(" ┃ " + bm.title);
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Public accessor for displaying list of existing Wibble Task entry bookmarks.
 */
void wibble::WibbleBookmark::display_task_bm()
{
    wc.console_write("");
    wc.console_write_hz_divider();
    wc.console_write_header("TASK BM");
    size_t index = 0;
    size_t tot_records;
    tot_records = bookmarks[BM_CAT_4].size();
    for(auto const& bm: bookmarks[BM_CAT_4])
    {
        ++index;
        wc.console_write_padded_selector(index, tot_records, "");
        wc.console_write_green_bold(bm.field2); 
        wc.console_write(" ┃ " + bm.title);
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Public accessor for displaying list of existing Wibble Jotfile bookmarks.
 */
void wibble::WibbleBookmark::display_jotfile_bm()
{
    wc.console_write("");
    wc.console_write_hz_divider();
    wc.console_write_header("JOT BM");
    size_t index = 0;
    size_t tot_records;
    tot_records = bookmarks[BM_CAT_3].size();
    unsigned int maxlength = 0;
    // for pretty printing, work out spacing
    for(auto const& bm: bookmarks[BM_CAT_3])
    {
        if(std::string(std::filesystem::path(bm.field1).filename()).length() > maxlength)
        { maxlength = std::string(std::filesystem::path(bm.field1).filename()).length(); } 
    }

    for(auto const& bm: bookmarks[BM_CAT_3])
    {
        ++index;
        std::string raw_name = std::string(std::filesystem::path(bm.field1).filename());
        int fn_length = raw_name.length();
        // safety guard, should always be true unless user has deliberately broken things by
        // by manually renaming a jotfile
        if(fn_length > 5) 
        {
            std::string spacer = "";
            int padcount = maxlength - fn_length;
            if(padcount > 0) { spacer.insert(0, padcount, ' '); }
            wc.console_write_padded_selector(index, tot_records, "");
            std::string pretty_n = raw_name.substr(0, raw_name.length() - 4);
            std::cout << pretty_n << spacer << " ┃ ";
            try
            {
                std::size_t sep = bm.title.find(":");
                wc.console_write_yellow(bm.title.substr(0, sep + 1));
                wc.console_write_green_bold(bm.title.substr(sep + 1, bm.title.length() - sep - 1) + "\n");
            }
            catch(std::exception& ex) {}
        }
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Public accessor for displaying list of existing Wibble Topic entry bookmarks.
 */
void wibble::WibbleBookmark::display_topic_bm()
{
    wc.console_write("");
    wc.console_write_hz_divider();
    wc.console_write_header(" TOPIC BM");
    size_t index = 0;
    size_t tot_records;
    tot_records = bookmarks[BM_CAT_2].size();
    for(auto const& bm: bookmarks[BM_CAT_2])
    {
        ++index;
        wc.console_write_padded_selector(index, tot_records, "");
        wc.console_write_green_bold(bm.field5 + ":" + bm.field2); 
        std::cout << " ┃ ";
        if(bm.field3 == "E") { wc.console_write_green(bm.field3);  }
        else                 { wc.console_write_yellow(bm.field3); }
        wc.console_write(" ┃ " + bm.title);
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Public accessor for displaying list of existing custom command bookmarks.
 * @param cat_name Unique custom category identifier/name
 * @param category Vector of bookmarks from custom category
 */
void wibble::WibbleBookmark::display_custom_bm(const std::string& cat_name, const std::vector<wbe>& category)
{
    wc.console_write("");
    wc.console_write_header("CUSTOM BM");
    wc.console_write_yellow_bold("Category: "); wc.console_write(cat_name);
    wc.console_write_hz_divider();
    size_t index = 0;
    size_t tot_records = category.size();
    for(auto const& bm: category)
    {
        ++index;
        wc.console_write_padded_selector(index, tot_records, "");
        wc.console_write(bm.title);
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Public accessor for displaying list of existing general command bookmarks.
 */
void wibble::WibbleBookmark::display_gen_exec_bm()
{
    wc.console_write("");
    wc.console_write_hz_divider();
    wc.console_write("");
    wc.console_write_header("SHELL BM ");
    size_t index = 0;
    size_t tot_records;
    tot_records = bookmarks[BM_CAT_6].size();
    for(auto const& bm: bookmarks[BM_CAT_6])
    {
        ++index;
        wc.console_write_padded_selector(index, tot_records, "");
        wc.console_write(" ┃ " + bm.title);
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Public accessor for displaying list of Wibble command bookmarks.
 */
void wibble::WibbleBookmark::display_wib_exec_bm()
{
    wc.console_write("");
    wc.console_write_hz_divider();
    wc.console_write("");
    wc.console_write_header("WIBBLE BM");
    size_t index = 0;
    size_t tot_records;
    tot_records = bookmarks[BM_CAT_5].size();
    for(auto const& bm: bookmarks[BM_CAT_5])
    {
        ++index;
        wc.console_write_padded_selector(index, tot_records, "");
        wc.console_write(" ┃ " + bm.title);
    }
    wc.console_write_hz_divider();
    wc.console_write("");
}

/**
 * @brief Retrieve (or delete or edit) a designated Wibble Node bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param d_sel Optional integer flag to determine whether to automatically directly select or prompt for user input
 */
bool wibble::WibbleBookmark::select_node_bm(const int& op, const int& d_sel)
{
    try
    {
        if(bookmarks[BM_CAT_1].size() == 0)
        { wc.console_write_error_red("No node bookmarks yet present. Add one."); return true; }
        display_node_bm();
        unsigned long sel = _prompt_entry(d_sel);
        if(sel < 1 || sel > bookmarks[BM_CAT_1].size()) { throw std::invalid_argument("Out of range"); }
        --sel; // vector is 0 indexed 
        search_filters node_search;
        node_search.exact = true;
        node_search.id = bookmarks[BM_CAT_1][sel].field1;
        node_search.archive_db = bookmarks[BM_CAT_1][sel].archived_node;
        WibResultList res = wib_e.perform_search(node_search);
        WibbleRecord w = wib_e.get_record_from_list(res, 1);
        if(w.get_id() != "___NULL___")
        { 
            w.pretty_print_record_full();
            // default is add, op == 1
            if(op == 1)      { _dispatch_node_action_menu(w, node_search.archive_db); }
            else if(op == 2) { return _delete_existing_node_bm(bookmarks[BM_CAT_1][sel]); }
            else if(op == 3) { return _edit_existing_node_bm(bookmarks[BM_CAT_1][sel]);   }
        }
        else
        {
            wc.console_write_error_red("Node not found. Remove exiting bookmark?");
            std::string resp = wc.console_write_prompt("Purge bookmark? (y/n) > ", "", "");
            if(resp == "y" || resp == "Y")
            {
                // field2 contains individual bookmark unique hash
                // FIXME: could use _remove_line_from_index instead?
                bool remove = _remove_line_from_file(bookmarks[BM_CAT_1][sel].field2, node_bm_file);
                if(remove) { wc.console_write_success_green("Bookmark removed successfully."); }
                else       { wc.console_write_error_red("Error removing bookmark.");           }
            }
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        return false;
    }

    return true;
}

/**
 * @brief Retrieve (or delete or edit) a designated Wibble Topic entry bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param d_sel Optional integer flag to determine whether to automatically directly select or prompt for user input
 */
bool wibble::WibbleBookmark::select_topic_bm(const int& op, const int& d_sel)
{
    try
    {
        if(bookmarks[BM_CAT_2].size() == 0)
        { wc.console_write_error_red("No topic bookmarks yet present. Add one."); return true; }
        display_topic_bm();
        unsigned long sel = _prompt_entry(d_sel);
        if(sel < 1 || sel > bookmarks[BM_CAT_2].size()) { throw std::invalid_argument("Out of range"); }
        --sel; // vector is 0 indexed 

        // default is add, op == 1
        if(op == 1)
        {
            const std::string fn = wib_e.get_topics_dir() + "/" + bookmarks[BM_CAT_2][sel].field4;

            // offer to remove if non existent topic entry (e.g. user deleted topic entry)
            if(! std::filesystem::exists(std::filesystem::path(fn)))
            {
                wc.console_write_error_red("Topic entry/file not found. Remove exiting bookmark?");
                std::string resp = wc.console_write_prompt("Purge bookmark? (y/n) > ", "", "");
                if(resp == "y" || resp == "Y")
                {
                    // field6 contains individual topic bookmark unique hash
                    bool remove = _remove_line_from_file(bookmarks[BM_CAT_2][sel].field6, topic_bm_file);
                    if(remove) { wc.console_write_success_green("Bookmark removed successfully."); }
                    else       { wc.console_write_error_red("Error removing bookmark.");           }
                }
            }
            else
            {
                // determine whether it is a text or data node
                if(bookmarks[BM_CAT_2][sel].field3 == "E")
                {
                    _dispatch_topic_action_menu(fn);
                }
                else // type == "D"
                {
                    wibble::WibbleActions().expand_exec(wib_e, wib_e.get_xdg_open_cmd() + " " + fn);
                }
            }
        }
        else if(op == 2) { return _delete_existing_topic_bm(bookmarks[BM_CAT_2][sel]); }
        else if(op == 3) { return _edit_existing_topic_bm(bookmarks[BM_CAT_2][sel]);   }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        return false;
    }
    return true;
}

/**
 * @brief Retrieve (or delete or edit) a designated Wibble Jotfile bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param d_sel Optional integer flag to determine whether to automatically directly select or prompt for user input
 */
bool wibble::WibbleBookmark::select_jotfile_bm(const int& op, const int& d_sel)
{
    try
    {
        if(bookmarks[BM_CAT_3].size() == 0)
        { wc.console_write_error_red("No jotfile bookmarks yet present. Add one."); return true; }
        display_jotfile_bm();
        unsigned long sel = _prompt_entry(d_sel);
        if(sel < 1 || sel > bookmarks[BM_CAT_3].size()) { throw std::invalid_argument("Out of range"); }
        --sel; // vector is 0 indexed 
        const std::string jotfile_fn  = wib_e.get_jot_dir() + "/" + bookmarks[BM_CAT_3][sel].field1;  

        // offer to remove if non existent jotfile (e.g. user deleted jotfile)
        if(! std::filesystem::exists(std::filesystem::path(jotfile_fn)))
        {
            wc.console_write_error_red("Jotfile not found. Remove exiting bookmark?");
            std::string resp = wc.console_write_prompt("Purge bookmark? (y/n) > ", "", "");
            if(resp == "y" || resp == "Y")
            {
                // field1 contains jotfile unique filename
                bool remove = _remove_line_from_file(bookmarks[BM_CAT_3][sel].field1, jotfile_bm_file);
                if(remove) { wc.console_write_success_green("Bookmark removed successfully."); }
                else       { wc.console_write_error_red("Error removing bookmark.");           }
            }
        }
        else
        {
            if(op == 1)
            {
                wibble::WibbleActions().jotfile_action_menu(wc, b_OPTS.paths.jot_dir, b_OPTS.paths.tmp_dir, jotfile_fn); 
            }
            else if(op == 2) { return _delete_existing_jotfile_bm(bookmarks[BM_CAT_3][sel]); }
            else if(op == 3) { return _edit_existing_jotfile_bm(bookmarks[BM_CAT_3][sel]);   }
        }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        return false;
    }
    return true;
}

/**
 * @brief Retrieve (or delete or edit) a designated Wibble Task entry bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param d_sel Optional integer flag to determine whether to automatically directly select or prompt for user input
 */
bool wibble::WibbleBookmark::select_task_bm(const int& op, const int& d_sel)
{
    try
    {
        if(bookmarks[BM_CAT_4].size() == 0)
        { wc.console_write_error_red("No task bookmarks yet present. Add one."); return true; }
        display_task_bm();
        unsigned long sel = _prompt_entry(d_sel);
        if(sel < 1 || sel > bookmarks[BM_CAT_4].size()) { throw std::invalid_argument("Out of range"); }
        --sel; // vector is 0 indexed 
        std::string task_id  = bookmarks[BM_CAT_4][sel].field1;  
        std::string category = bookmarks[BM_CAT_4][sel].field2;  
        if(op == 1)
        {
            WibbleTask wtsk{wib_e.get_tasks_dir(), wib_e.get_tmp_dir(), wib_e.get_templates_dir(), wib_e.get_bin_mapping()};
            bool task_sel = wtsk.select_task_for_id(task_id, category, b_OPTS);
            if(! task_sel)
            {
                wc.console_write_error_red("Task not found. Remove exiting bookmark?");
                std::string resp = wc.console_write_prompt("Purge bookmark? (y/n) > ", "", "");
                if(resp == "y" || resp == "Y")
                {
                    // field1 contains individual task id/bookmark hash
                    bool remove = _remove_line_from_file(bookmarks[BM_CAT_4][sel].field1, task_bm_file);
                    if(remove) { wc.console_write_success_green("Bookmark removed successfully."); return true; }
                    else       { wc.console_write_error_red("Error removing bookmark.");           return false;}
                }
                else { return true; }
            }
            else { return true; }
        }
        else if(op == 2) { return _delete_existing_task_bm(bookmarks[BM_CAT_4][sel]); }
        else if(op == 3) { return _edit_existing_task_bm(bookmarks[BM_CAT_4][sel]);   }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        return false;
    }
    return true;
}

/**
 * @brief Retrieve (or delete or edit) a designated Wibble command bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param d_sel Optional integer flag to determine whether to automatically directly select or prompt for user input
 */
bool wibble::WibbleBookmark::select_wib_exec_bm(const int& op, const int& d_sel)
{
    try
    {
        if(bookmarks[BM_CAT_5].size() == 0)
        { wc.console_write_error_red("No Wibble command bookmarks yet present. Add one."); return true; }
        display_wib_exec_bm();
        unsigned long sel = _prompt_entry(d_sel);
        if(sel < 1 || sel > bookmarks[BM_CAT_5].size()) { throw std::invalid_argument("Out of range"); }
        --sel; // vector is 0 indexed 
        std::string entry = bookmarks[BM_CAT_5][sel].field1;  
        //std::cout << "DEBUG: EXEC CMD: " << bookmarks[BM_CAT_5][sel].field1 << " " << exec_args << std::endl;
        if(op == 1) { std::system(("wibble++ " + bookmarks[BM_CAT_5][sel].field1 + " " + exec_args).c_str()); }
        else if(op == 2) { return _delete_existing_wibble_cmd_bm(bookmarks[BM_CAT_5][sel]);          }
        else if(op == 3) { return _edit_existing_wibble_cmd_bm(bookmarks[BM_CAT_5][sel]);            }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        return false;
    }
    return true;
}

/**
 * @brief Retrieve (or delete or edit) a designated general command bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param d_sel Optional integer flag to determine whether to automatically directly select or prompt for user input
 */
bool wibble::WibbleBookmark::select_gen_exec_bm(const int& op, const int& d_sel)
{
    try
    {
        if(bookmarks[BM_CAT_6].size() == 0)
        { wc.console_write_error_red("No General command bookmarks yet present. Add one."); return true; }
        display_gen_exec_bm();
        unsigned long sel = _prompt_entry(d_sel);
        if(sel < 1 || sel > bookmarks[BM_CAT_6].size()) { throw std::invalid_argument("Out of range"); }
        --sel; // vector is 0 indexed 
        std::string entry = bookmarks[BM_CAT_6][sel].field1;  
        if(op == 1)      { wibble::WibbleActions().expand_exec(wib_e, bookmarks[BM_CAT_6][sel].field1 + " " + exec_args); }
        else if(op == 2) { return _delete_existing_general_cmd_bm(bookmarks[BM_CAT_6][sel]);                              }
        else if(op == 3) { return _edit_existing_general_cmd_bm(bookmarks[BM_CAT_6][sel]);                                }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid selection. Exiting.");
        return false;
    }
    return true;
}

/**
 * @brief Retrieve (or delete or edit) a designated custom command bookmark.
 * @param op Integer flag to indicate whether to open, delete, or edit given bookmark
 * @param c_sel Optional integer flag for category to determine whether to automatically directly select or prompt for user input
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
bool wibble::WibbleBookmark::select_cust_exec_bm(const int& op, const int& c_sel, const int& d_sel)
{
    try
    {
        //std::cout << "DEBUG: c_sel = " << c_sel << std::endl;
        wc.console_write("");
        cbm cust = _get_display_custom_categories(c_sel);
        std::vector<wbe> category = _get_all_cust_exec_bm_for_cat(cust.short_name);
        if(! category.empty())
        {
            display_custom_bm(cust.short_name, category);
            unsigned long sel = _prompt_entry(d_sel);
            if(sel < 1 || sel > category.size()) { throw std::invalid_argument("Out of range"); }
            --sel; // vector is 0 indexed 
            std::string entry = category[sel].field1;  
            if(op == 1)      { wibble::WibbleActions().expand_exec(wib_e, category[sel].field1 + " " + exec_args); }
            else if(op == 2) { return _delete_existing_custom_cmd_bm(category[sel], cust);                                   }
            else if(op == 3) { return _edit_existing_custom_cmd_bm(category[sel], cust);                                     }
        }
        else { wc.console_write_error_red("No bookmarks in category. Add some."); return false; }
    }
    catch(std::exception& ex) { return false; }

    return true;
}

/**
 * @brief Public accessor to delete a designated general command bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::delete_gen_exec_bm(const int& d_sel)
{
    select_gen_exec_bm(2, d_sel);
}

/**
 * @brief Public accessor to delete a designated Wibble Jotfile bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::delete_jotfile_bm(const int& d_sel)
{
    select_jotfile_bm(2, d_sel);
}

/**
 * @brief Public accessor to delete a designated Wibble Node bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::delete_node_bm(const int& d_sel)
{
    select_node_bm(2, d_sel);
}

/**
 * @brief Public accessor to delete a designated Wibble Task entry bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::delete_task_bm(const int& d_sel)
{
    select_task_bm(2, d_sel);
}

/**
 * @brief Public accessor to delete a designated Wibble Topic entry bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::delete_topic_bm(const int& d_sel)
{
    select_topic_bm(2, d_sel);
}

/**
 * @brief Public accessor to delete a designated Wibble command bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::delete_wib_exec_bm(const int& d_sel)
{
    select_wib_exec_bm(2, d_sel);
}

/**
 * @brief Public accessor to edit a designated general command bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::edit_gen_exec_bm(const int& d_sel)
{
    select_gen_exec_bm(3, d_sel);
}

/**
 * @brief Public accessor to edit a designated Wibble Jotfile bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::edit_jotfile_bm(const int& d_sel)
{
    select_jotfile_bm(3, d_sel);
}

/**
 * @brief Public accessor to edit a designated Wibble Node bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::edit_node_bm(const int& d_sel)
{
    select_node_bm(3, d_sel);
}

/**
 * @brief Public accessor to edit a designated Wibble Task entry bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::edit_task_bm(const int& d_sel)
{
    select_task_bm(3, d_sel);
}

/**
 * @brief Public accessor to edit a designated Wibble Topic entry bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::edit_topic_bm(const int& d_sel)
{
    select_topic_bm(3, d_sel);
}

/**
 * @brief Public accessor to edit a designated Wibble command bookmark.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::edit_wib_exec_bm(const int& d_sel)
{
    select_wib_exec_bm(3, d_sel);
}

/**
 * @brief Public accessor to add a new custom category/group for command bookmarks.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_add_category()
{
    _add_new_custom_category();
}

/**
 * @brief Public accessor to remove an existing custom category/group for command bookmarks.
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_rm_category()
{
    _remove_custom_category();
}

/**
 * @brief Public accessor to add a new command bookmark to an existing custom category/group.
 * @param c_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_category_add_bm(const int& c_sel)
{
    _add_cmd_bm(_get_display_custom_categories(c_sel));
}

/**
 * @brief Public accessor to remove a command bookmark from an existing custom category/group.
 * @param c_sel Optional integer flag for category to determine whether to automatically directly select given entry or prompt for user input
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_category_del_bm(const int& c_sel, const int& d_sel)
{
    select_cust_exec_bm(2, c_sel, d_sel);
}

/**
 * @brief Public accessor to edit an existing command bookmark in a custom category/group.
 * @param c_sel Optional integer flag for category to determine whether to automatically directly select given entry or prompt for user input
 * @param d_sel Optional integer flag to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_category_edit_bm(const int& c_sel, const int& d_sel)
{
    select_cust_exec_bm(3, c_sel, d_sel);
}

/**
 * @brief Public accessor to activate a previously hibernated custom category/group.
 * @param c_sel Optional integer flag for category to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_category_activate(const int& c_sel)
{
    cbm cat =_get_display_custom_categories(c_sel, true);
    if(cat.full_id == "___NULL___") { wc.console_write_error_red("Invalid category selection. Exiting."); throw wibble::wibble_exit(1); }
    wc.console_write_header("ACTIVATE");
    wc.console_write_green("Category name: "); wc.console_write(cat.short_name);
    wc.console_write_green("Category id  : "); wc.console_write(cat.full_id);
    wc.console_write_hz_divider();
    std::string resp = wc.console_write_prompt("Activate above custom category? Category will be re-enabled.\n(y/n) > ", "", "");
    if(resp == "y" || resp == "Y")
    {
        std::size_t id_delim = cat.full_id.find("_");
        if(id_delim != std::string::npos)
        {
            const std::string ct_id = cat.full_id.substr(0, id_delim);
            const std::string org_e = cat.short_name + "|H|" + ct_id;
            const std::string new_e = cat.short_name + "|A|" + ct_id;
            bool hib_result = _update_bookmark_line(org_e, new_e, cat_map_file);
            if(hib_result)
            {
                wc.console_write_success_green("Custom category '"
                                               + cat.short_name + "' successfully reactivated. Now present in menu.");
            }
            else
            {
                wc.console_write_error_red("Error reactivating custom category '" + cat.short_name + "'.");
            }
        }
        else { wc.console_write_error_red("Malformed custom category file. Aborting."); }
    }
    else { wc.console_write_error_red("User cancelled."); }
}

/**
 * @brief Public accessor to hibernate a currently active custom category/group.
 * @param c_sel Optional integer flag for category to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_category_hibernate(const int& c_sel)
{
    cbm cat =_get_display_custom_categories(c_sel);
    if(cat.full_id == "___NULL___") { wc.console_write_error_red("Invalid category selection. Exiting."); throw wibble::wibble_exit(1); }
    wc.console_write_header("HIBERNATE");
    wc.console_write_green("Category name: "); wc.console_write(cat.short_name);
    wc.console_write_green("Category id  : "); wc.console_write(cat.full_id);
    wc.console_write_hz_divider();
    std::string resp = wc.console_write_prompt("Hibernate above custom category? Category will be hidden.\n(y/n) > ", "", "");
    if(resp == "y" || resp == "Y")
    {
        std::size_t id_delim = cat.full_id.find("_");
        if(id_delim != std::string::npos)
        {
            const std::string ct_id = cat.full_id.substr(0, id_delim);
            const std::string org_e = cat.short_name + "|A|" + ct_id;
            const std::string new_e = cat.short_name + "|H|" + ct_id;
            bool hib_result = _update_bookmark_line(org_e, new_e, cat_map_file);
            if(hib_result)
            {
                wc.console_write_success_green("Custom category '"
                                               + cat.short_name + "' successfully hibernated. Reactivate it to make it visible again.");
            }
            else
            {
                wc.console_write_error_red("Error hibernating custom category '" + cat.short_name + "'.");
            }
        }
        else { wc.console_write_error_red("Malformed custom category file. Aborting."); }
    }
    else { wc.console_write_error_red("User cancelled."); }

}

/**
 * @brief Public accessor to rename a currently active custom category/group.
 * @param c_sel Optional integer flag for category to determine whether to automatically directly select given entry or prompt for user input
 */
void wibble::WibbleBookmark::cust_ren_category(const int& c_sel)
{
    cbm cat =_get_display_custom_categories(c_sel);
    if(cat.full_id == "___NULL___") { wc.console_write_error_red("Invalid category selection. Exiting."); throw wibble::wibble_exit(1); }
    wc.console_write_header("RENAME");
    wc.console_write_yellow_bold("Existing category name: "); wc.console_write(cat.short_name);
    std::string cat_n = wc.console_write_prompt("Input new name > ", "", cat.short_name);
    wc.trim(cat_n);
    wibble::WibbleRecord::remove_pipe(cat_n);
    wibble::WibbleRecord::remove_slash(cat_n);
    wc.console_write_yellow_bold("New category name     : "); wc.console_write(cat_n);
    std::string resp = wc.console_write_prompt("Rename custom category? (y/n) > ", "", "");
    if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled. Exiting."); }
    else
    {
        const std::string existing_name = wib_e.get_bookmarks_dir() + "/" + cat.full_id + ".wib";
        //std::cout << "Existing name = " << existing_name << std::endl;
        std::size_t id_sep = cat.full_id.find("_");
        if(std::filesystem::exists(std::filesystem::path(existing_name)) && id_sep != std::string::npos)
        {
            const std::string cat_id = cat.full_id.substr(0, id_sep);

            const std::string new_name = wib_e.get_bookmarks_dir() + "/" + cat_id + CUST_PREFIX + cat_n + ".wib";
            //std::cout << "New name = " << new_name << std::endl;
            bool rename_cat = wibble::WibbleIO::wmove_file(existing_name, new_name);
            if(rename_cat)
            {
                bool update_idx = _rename_cust_category_entries(new_name, cat.short_name, cat_n);
                if(update_idx)
                {
                    const std::string replc_r = cat_n + "|A|" + cat_id;
                    bool do_update = _remove_line_from_file(cat.short_name, cat_map_file, replc_r);
                    if(do_update) { wc.console_write_success_green("Custom category successfully renamed."); }
                    else          { wc.console_write_error_red("Error renaming custom category.");           }
                }
                else { wc.console_write_error_red("Error updating custom category entries."); }
            }
            else { wc.console_write_error_red("Error renamining custom category file."); }
        }
    }
}

/**
 * @brief Public accessor to display the main menu.
 */
void wibble::WibbleBookmark::select_bm_menu()
{
    _render_menu();
}
