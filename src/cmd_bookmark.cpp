/**
 * @file      cmd_bookmark.cpp
 * @brief     Handle "bookmark" subcommand.
 * @details
 *
 * This class handles the initial dispatching logic for the various
 * bookmark commands. Whilst nearly every bookmark operation can
 * be performed interactively, command line switches exist to rapidly
 * perform/access most bookmark operations directly.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_bookmark.hpp"
#include "wibblebookmark.hpp"
#include "wibbleexit.hpp"
#include "wibblesymbols.hpp"
 
void wibble::WibbleCmdBookmark::handle_bookmark(pkg_options& OPTS, bookmark_options& BOOK)
{
    
    WibbleConsole wc;

    bookmark_env BE;
    int modes = 0;
    // NULL_INT_VAL is placeholder initialisation value
    const int ENTRY_NUM = BOOK.num != NULL_INT_VAL ? BOOK.num : 0;
    // any environment/shell arguments for command
    const std::string exec_args = BOOK.exec_args == "UNSET" ? "" : BOOK.exec_args; 

    if(ENTRY_NUM < 0)
    {
        wc.console_write_error_red("Invalid selection number. Please enter a value greater than 0.");
        throw wibble::wibble_exit(1);
    }

    // switch on silent mode if requested
    if(BOOK.silence) { wc.silence_output(); }
    
    // setup paths/constants to files - standard categories
    BE.env_node_bm_file         = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(1) + ".wib"; 
    BE.env_topic_bm_file        = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(2) + ".wib";
    BE.env_jotfile_bm_file      = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(3) + ".wib";
    BE.env_task_bm_file         = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(4) + ".wib";
    BE.env_wib_exec_bm_file     = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(5) + ".wib";
    BE.env_gen_exec_bm_file     = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(6) + ".wib";
    BE.env_cat_map_file         = OPTS.paths.bookmarks_dir + "/" + wibble::WibbleBookmark::index_to_lookup_name(7) + ".wib";

    // custom category handling
    BE.env_cust_exec_bm_cat      = BOOK.cust_exec_bm_category;
    BE.env_cust_exec_cat_add_bm  = BOOK.cust_exec_category_add_bm;
    BE.env_cust_exec_cat_del_bm  = BOOK.cust_exec_category_del_bm;
    BE.env_cust_exec_cat_edit_bm = BOOK.cust_exec_category_edit_bm;
    BE.env_cust_exec_cat_sel_bm  = BOOK.cust_exec_category_sel_bm;
    BE.env_rm_cust_exec_cat      = BOOK.rm_cust_exec_category;
    BE.env_add_cust_exec_cat     = BOOK.add_cust_exec_category;

    WibbleBookmark wb{BE, OPTS, exec_args};

    enum bookmark_op
    {
        // add operations
        ADD_NODE_BM,
        ADD_TOPIC_BM,
        ADD_JOTFILE_BM,
        ADD_TASK_BM,
        ADD_WIB_EXEC_BM,
        ADD_GEN_EXEC_BM,

        // select operations
        SELECT_MENU,
        SELECT_NODE_BM,
        SELECT_TOPIC_BM,
        SELECT_JOTFILE_BM,
        SELECT_TASK_BM,
        SELECT_WIB_EXEC_BM,
        SELECT_GEN_EXEC_BM,

        // delete operations
        DELETE_NODE_BM,
        DELETE_TOPIC_BM,
        DELETE_JOTFILE_BM,
        DELETE_TASK_BM,
        DELETE_WIB_EXEC_BM,
        DELETE_GEN_EXEC_BM,

        // delete operations
        EDIT_NODE_BM,
        EDIT_TOPIC_BM,
        EDIT_JOTFILE_BM,
        EDIT_TASK_BM,
        EDIT_WIB_EXEC_BM,
        EDIT_GEN_EXEC_BM,

        // custom category operations
        CUST_ADD_CATEGORY,
        CUST_REN_CATEGORY,
        CUST_RM_CATEGORY,
        CUST_CATEGORY_ADD_BM,
        CUST_CATEGORY_DEL_BM,
        CUST_CATEGORY_EDIT_BM,
        CUST_CATEGORY_SEL_BM,

        CUST_CATEGORY_ACTIVATE,
        CUST_CATEGORY_HIBERNATE,

        ERR_MULT_MODES
    };

    bookmark_op run_bm;

    // select menu
    if(BOOK.select_all)                { ++modes; run_bm = SELECT_MENU;        }

    // standard categories - #1. Add
    if(BOOK.add_new_node_bm)           { ++modes; run_bm = ADD_NODE_BM;        }
    if(BOOK.add_new_topic_bm)          { ++modes; run_bm = ADD_TOPIC_BM;       }
    if(BOOK.add_new_jotfile_bm)        { ++modes; run_bm = ADD_JOTFILE_BM;     }
    if(BOOK.add_new_task_bm)           { ++modes; run_bm = ADD_TASK_BM;        }
    if(BOOK.add_new_wibble_cmd_bm)     { ++modes; run_bm = ADD_WIB_EXEC_BM;    }
    if(BOOK.add_new_general_cmd_bm)    { ++modes; run_bm = ADD_GEN_EXEC_BM;    }

    // standard categories - #2. Select
    if(BOOK.select_node_bm)            { ++modes; run_bm = SELECT_NODE_BM;     }
    if(BOOK.select_topic_bm)           { ++modes; run_bm = SELECT_TOPIC_BM;    }
    if(BOOK.select_jotfile_bm)         { ++modes; run_bm = SELECT_JOTFILE_BM;  }
    if(BOOK.select_task_bm)            { ++modes; run_bm = SELECT_TASK_BM;     }
    if(BOOK.select_wibble_cmd_bm)      { ++modes; run_bm = SELECT_WIB_EXEC_BM; }
    if(BOOK.select_general_cmd_bm)     { ++modes; run_bm = SELECT_GEN_EXEC_BM; }

    // standard categories - #3. Delete
    if(BOOK.delete_node_bm)            { ++modes; run_bm = DELETE_NODE_BM;     }
    if(BOOK.delete_topic_bm)           { ++modes; run_bm = DELETE_TOPIC_BM;    }
    if(BOOK.delete_jotfile_bm)         { ++modes; run_bm = DELETE_JOTFILE_BM;  }
    if(BOOK.delete_task_bm)            { ++modes; run_bm = DELETE_TASK_BM;     }
    if(BOOK.delete_wibble_cmd_bm)      { ++modes; run_bm = DELETE_WIB_EXEC_BM; }
    if(BOOK.delete_general_cmd_bm)     { ++modes; run_bm = DELETE_GEN_EXEC_BM; }

    // standard categories - #4. Edit
    if(BOOK.edit_node_bm)              { ++modes; run_bm = EDIT_NODE_BM;       }
    if(BOOK.edit_topic_bm)             { ++modes; run_bm = EDIT_TOPIC_BM;      }
    if(BOOK.edit_jotfile_bm)           { ++modes; run_bm = EDIT_JOTFILE_BM;    }
    if(BOOK.edit_task_bm)              { ++modes; run_bm = EDIT_TASK_BM;       }
    if(BOOK.edit_wibble_cmd_bm)        { ++modes; run_bm = EDIT_WIB_EXEC_BM;   }
    if(BOOK.edit_general_cmd_bm)       { ++modes; run_bm = EDIT_GEN_EXEC_BM;   }

    int cat = -1;
    // custom category operations
    if(BOOK.cust_exec_bm_category != NULL_INT_VAL)
    {
        ++modes;
        cat = BOOK.cust_exec_bm_category;
        run_bm = CUST_CATEGORY_SEL_BM;
        // These switches NEED category number specified by cust_exec_bm_category
        if(BOOK.cust_exec_category_add_bm)  { run_bm = CUST_CATEGORY_ADD_BM;  }
        if(BOOK.cust_exec_category_edit_bm) { run_bm = CUST_CATEGORY_EDIT_BM; }
        if(BOOK.cust_exec_category_del_bm)  { run_bm = CUST_CATEGORY_DEL_BM;  }
    }

    // activation and hibernation of custom categories
    if(BOOK.act_cust_exec_category) { ++modes; run_bm = CUST_CATEGORY_ACTIVATE;  }
    if(BOOK.hib_cust_exec_category) { ++modes; run_bm = CUST_CATEGORY_HIBERNATE; }

    // removal of entire category
    if(BOOK.rm_cust_exec_category)  { ++modes; run_bm = CUST_RM_CATEGORY;        }

    // rename of custom category
    if(BOOK.ren_cust_exec_category) { ++modes; run_bm = CUST_REN_CATEGORY;       }

    // default to select menu. Protect against multiple operations
    if(modes == 0)     { run_bm = SELECT_MENU; }
    else if(modes > 1) { run_bm = ERR_MULT_MODES; }

    // for direct selection of entries
    const int num = (BOOK.num != NULL_INT_VAL) ? BOOK.num : -1;

    // dispatcher for the many bookmark operations
    switch(run_bm)
    {
        // add operations
    case ADD_GEN_EXEC_BM:
        wb.add_gen_exec_bm();
        break;
    case ADD_JOTFILE_BM:
        wb.add_jotfile_bm();
        break;
    case ADD_NODE_BM:
        wb.add_new_node_bm();
        break;
    case ADD_TASK_BM:
        wb.add_task_bm();
        break;
    case ADD_TOPIC_BM:
        wb.add_topic_bm();
        break;
    case ADD_WIB_EXEC_BM:
        wb.add_wib_exec_bm();
        break;

        // select operations
    case SELECT_GEN_EXEC_BM:
        wb.select_gen_exec_bm(1, num);
        break;
    case SELECT_JOTFILE_BM:
        wb.select_jotfile_bm(1, num);
        break;
    case SELECT_NODE_BM:
        wb.select_node_bm(1, num);
        break;
    case SELECT_TASK_BM:
        wb.select_task_bm(1, num);
        break;
    case SELECT_TOPIC_BM:
        wb.select_topic_bm(1, num);
        break;
    case SELECT_WIB_EXEC_BM:
        wb.select_wib_exec_bm(1, num);
        break;

        // delete operations
    case DELETE_GEN_EXEC_BM:
        wb.delete_gen_exec_bm(num);
        break;
    case DELETE_JOTFILE_BM:
        wb.delete_jotfile_bm(num);
        break;
    case DELETE_NODE_BM:
        wb.delete_node_bm(num);
        break;
    case DELETE_TOPIC_BM:
        wb.delete_topic_bm(num);
        break;
    case DELETE_TASK_BM:
        wb.delete_task_bm(num);
        break;
    case DELETE_WIB_EXEC_BM:
        wb.delete_wib_exec_bm(num);
        break;

        // delete operations
    case EDIT_GEN_EXEC_BM:
        wb.edit_gen_exec_bm(num);
        break;
    case EDIT_JOTFILE_BM:
        wb.edit_jotfile_bm(num);
        break;
    case EDIT_NODE_BM:
        wb.edit_node_bm(num);
        break;
    case EDIT_TOPIC_BM:
        wb.edit_topic_bm(num);
        break;
    case EDIT_TASK_BM:
        wb.edit_task_bm(num);
        break;
    case EDIT_WIB_EXEC_BM:
        wb.edit_wib_exec_bm(num);
        break;
        
        // custom category operations
    case CUST_ADD_CATEGORY:
        wb.cust_add_category();
        break;
    case CUST_REN_CATEGORY:
        wb.cust_ren_category(cat);
        break;
    case CUST_RM_CATEGORY:
        wb.cust_rm_category();
        break;
    case CUST_CATEGORY_ADD_BM:
        wb.cust_category_add_bm(cat);
        break;
    case CUST_CATEGORY_DEL_BM:
        wb.cust_category_del_bm(cat, num);
        break;
    case CUST_CATEGORY_EDIT_BM:
        wb.cust_category_edit_bm(cat, num);
        break;
    case CUST_CATEGORY_SEL_BM:
        wb.select_cust_exec_bm(1, cat, num);
        break;

    case CUST_CATEGORY_ACTIVATE:
        wb.cust_category_activate(cat);
        break;
    case CUST_CATEGORY_HIBERNATE:
        wb.cust_category_hibernate(cat);
        break;

    case ERR_MULT_MODES:
        wc.console_write_error_red("Multiple operations/flags selected.");
        wc.console_write_red("Please select only one option out of 'add', 'select', or 'delete' for corresponding type.\n");
        wc.console_write_red("For custom category operations, please also specify the number of the custom category if working non-interactively.\n");
        break;
        // main menu
    default:
    case SELECT_MENU:
        wb.select_bm_menu();
        break;
    }
} 
