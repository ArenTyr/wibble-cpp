/**
 * @file      cmd_autofile.hpp
 * @brief     Dispatcher class for autofiling feature (header file).
 * @details
 *
 * This class handles the command line switching for dispatching
 * the autofiling functionality/features within wibbleautofiler.cpp
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CMD_AUTOFILE_H
#define CMD_AUTOFILE_H

#include "cmd_structs.hpp"

namespace wibble
{
    class WibbleCmdAutofile
    {
    private:
    public:
        static void handle_autofile(pkg_options& OPTS, autofiler_options& AUTOF);
    };
}
#endif
