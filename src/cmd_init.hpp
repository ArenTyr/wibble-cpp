/**
 * @file      cmd_init.hpp
 * @brief     Handle "init" subcommand (header file).
 * @details
 *
 * Critical command used to setup the core Wibble++ environment, and
 * assist the user in creating an initial configuration file (TOML format),
 * saving them from writing it from scratch. Also ensures presence of
 * ripgrep, which is a mandatory tool that Wibble++ needs in order to
 * operate.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEINIT_H
#define WIBBLEINIT_H

#include "cmd_structs.hpp"
#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleCmdInit
    {
    public:
        struct init_settings
        {
            std::string ark_cmd;
            std::string autofile_dir;
            std::string backup_wibble_dir;
            std::string main_wibble_dir;
            std::string cli_editor;
            std::string dump_cmd;
            std::string fm_cmd;
            std::string gui_editor;
            std::string pager_cmd;
            std::string view_cmd;
        };
     private:
        std::string _set_output_filename(WibbleConsole& wc, init_options opts);
        void _check_for_ripgrep_cmd(WibbleConsole& wc);
        void _clean_response(WibbleConsole& wc, std::string& answer);
        init_settings _get_user_options(WibbleConsole& wc);
        void _print_confirm_config(WibbleConsole& wc, init_settings& answers);
        std::string _serialize_user_opts(init_settings& user_opts);
    public:
        void init_config(init_options opts);
    };
}
#endif
