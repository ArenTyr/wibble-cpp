// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_exec.cpp
 * @brief     Handle "exec" subcommand.
 * @details
 *
 * This class handles the "exec" command, which allows the user
 * to execute an arbitrary command on a given Wibble Node file, or
 * batch execution across a set of Wibble Node files. Also provides
 * ability to reference a script contained in Wibble's 'scripts/'
 * subdirectory.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_exec.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Run a 'batch' command across a set/multiple Wibble Node records.
 * @param wc Reference to WibbleConsole console object
 * @param SEARCH search filters/fields to generate result set
 * @param exec_cmd The actual command to run upon each Wibble Node file
 */
void wibble::WibbleCmdExec::_run_batch(WibbleConsole& wc, WibbleExecutor& wib_e, search_filters& SEARCH, std::string& exec_cmd)
{
    WibResultList wl;
    std::string fn_list = "";
    // generate a vector of WibbleRecords
    wl = wibble::WibbleUtility::get_record_set(wib_e, SEARCH);
    for(auto const& record: *wl)
    {
        // iterate over and extract the destination filename path for each Node
        fn_list.append(wib_e._build_note_path(record.get_id(), record.get_type(),
                                              record.get_proj(), SEARCH.archive_db, true) + " ");
    }

    // build and run command
    wibble::WibbleUtility::build_batch_exec_cmd(exec_cmd, fn_list);
    wibble::WibbleUtility::prompt_confirm_run_batch_exec(wc, exec_cmd);
}

/**
 * @brief Dispatching logic for the "exec" operation on Wibble nodes.
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 * @param EXEC command line options/parameters supplied via switches for "exec"
 */
void wibble::WibbleCmdExec::handle_exec(pkg_options& OPTS, search_filters& SEARCH, exec_options& EXEC)
{
    WibbleConsole wc;
    WibbleExecutor wib_e(OPTS);
    WibbleRecord w;
    bool batch_exec = false;
    bool run_std_cmd = false;
    bool run_scr_cmd = false;

    // check whether we're running a batch operation or not
    if(EXEC.batch_exec) { batch_exec = true; }

    // check whether we're using specified command or something within scripts/ subdir inside main Wibble directory
    if(EXEC.exec_cmd != "UNSET")         { run_std_cmd = true; }
    else if(EXEC.exec_script != "UNSET") { run_scr_cmd = true; }

    // get the command to run if they haven't already supplied it, check script file exists if they're referencing
    // something in scripts/ subdir
    bool do_run = wibble::WibbleUtility::check_prompt_set_exec_cmds_scripts(wc, OPTS.paths.scripts_dir,
                                                                            run_std_cmd, run_scr_cmd, EXEC.exec_cmd, EXEC.exec_script);
    
    // batch execution across multiple records
    if(batch_exec && do_run)
    {
        if(run_std_cmd) { _run_batch(wc, wib_e, SEARCH, EXEC.exec_cmd);    }
        if(run_scr_cmd) { _run_batch(wc, wib_e, SEARCH, EXEC.exec_script);      } 

        if(! run_std_cmd && ! run_scr_cmd)
        { wc.console_write_error_red("Batch operation requested but no script or command supplied!"); }
    }
    else if(do_run) // singular execution across one node
    {
        w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH); 

        if(EXEC.exec_cmd == "UNSET" && EXEC.exec_script == "UNSET")
        {
            run_std_cmd = true;
            EXEC.exec_cmd = wc.console_write_prompt("Input execution command/command template: ", "", "");
            wc.trim(EXEC.exec_cmd);
            if(EXEC.exec_cmd == "") { run_std_cmd = false; wc.console_write_error_red("Command cannot be empty."); }
        }

        if(run_scr_cmd)
        {
            bool exec_result = wib_e.note_exec_cmd(w, EXEC.exec_script, SEARCH.archive_db);
            if(! exec_result) { wc.console_write_error_red("Error executing specified command on note."); }
        }

        if(run_std_cmd)
        {
            bool exec_result = wib_e.note_exec_cmd(w, EXEC.exec_cmd, SEARCH.archive_db);
            if(! exec_result) { wc.console_write_error_red("Error executing specified command on note."); }
        }
    }
    else
    {
        wc.console_write_error_red("No execution operation specified.");
    }
} 
