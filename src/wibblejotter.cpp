/**
 * @file      wibblejotter.cpp
 * @brief     Provides the Jotfile functionality ("jot").
 * @details
 *
 * This class provides the functionality related to the single-file
 * sectioned "Jotfile" operations: essentially a single file that can
 * be parsed and worked on in sections for quickly retrieving, adding,
 * or removing quick notes to, simply from the console, no editor needed.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include "wibbleexit.hpp"
#include "wibblejotter.hpp"
#include "wibblesymbols.hpp"

namespace wibble
{
    // some handy aliases
    using jtf = WibbleJotter::jotfile;
    using jfe = std::vector<WibbleJotter::jf_entry>;
    using jfr = WibbleJotter::jf_entry;
}

/**
 * @brief Parse the special filename structure of a Jotfile into its fields.
 * @param fn String representing filename/path to Jotfile.
 */
wibble::jtf wibble::WibbleJotter::_decompose_fn_to_jotfile(const std::string& fn) const
{
    jtf jf;
    try
    {
        unsigned long cat_delim = fn.find("_"); 
        unsigned long index = std::stol(fn.substr(0, cat_delim));
        unsigned long name_delim = fn.find("_", cat_delim + 1);
        std::string category = fn.substr(cat_delim + 1, (name_delim - 1 - cat_delim));
        // -5 because we want to remove the ".wib" file suffix
        std::string name = fn.substr(name_delim + 1, (fn.length() - name_delim - 5));

        //std::cout << "DEBUG: got indices: " << cat_delim << " | " << name_delim << std::endl;
        //std::cout << "DEBUG: got values: " << index << " | " << category <<  " | " << name << std::endl;

        jf.index = index;
        jf.category = category;
        jf.name = name;
    }
    catch(std::range_error& ex)
    {
        std::cerr << "Bad jotfile filename: " << ex.what() << std::endl;
        jf.index = 0;
        jf.category = "";
        jf.name = "";
    }

    return jf;
}

/**
 * @brief Identify the lowest numeric gap/index for the autonumbering of new Jotfiles.
 * @param jotfiles Vector of currently existing Jotfiles
 */
unsigned long wibble::WibbleJotter::_find_numeric_gap(const std::vector<jtf>& jotfiles) const
{
    unsigned long counter = 0;
    for(auto const& entry: jotfiles)
    {
            ++counter;
            if(counter < entry.index) { break; }
    }

    if(counter == 1 && jotfiles.size() > 1) { return 1;           }
    else if(counter == jotfiles.size())     { return counter + 1; }
    else                                    { return counter;     }
}

/**
 * @brief Main function to read all existing Jotfiles in upon object construction.
 * @param wc WibbleConsole utility object
 * @param jot_base_dir Root Jotfile directory 
 */
void wibble::WibbleJotter::_gather_jotfiles(WibbleConsole& wc, const std::string& jot_base_dir) 
{
    try
    {
        const std::filesystem::path jotfile_base_dir{jot_base_dir};
        wc.console_write_green(ARW + "Processing jotfile definitions under: " + std::string(jotfile_base_dir) + '\n');
        if(std::filesystem::exists(jotfile_base_dir))
        {
            for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{jotfile_base_dir})
            {
                //std::cerr << "Parsing: " << dir_entry.path() << '\n';
                if(dir_entry.is_regular_file())
                {
                    jtf jf = _decompose_fn_to_jotfile(std::string(std::filesystem::path(dir_entry).filename())); 
                    if(jf.index > 0 && jf.name != "" && jf.category != "")
                    {
                        jf.fn = std::string(std::filesystem::path(dir_entry).filename());
                        wc.console_write_green(ARW + "Found defined jotfile. Index: ");
                        wc.console_write_yellow(std::to_string(jf.index));
                        wc.console_write_green(". Name: ");
                        wc.console_write_yellow(jf.name);
                        wc.console_write_green(". Category: ");
                        wc.console_write_yellow(jf.category);
                        wc.console_write_green(".\n");
                        jotfiles.push_back(jf);
                    }
                }
                else { std::cout << "Jotfile ERROR" << std::endl; }
            }

            // sort jotfiles by filename index using inline lambda so they appear in numbered order
            std::sort(jotfiles.begin(), jotfiles.end(),
                      [](jotfile const& a, jotfile const& b) { return a.index < b.index; });

        }
        else
        {
            wc.console_write_green(ARW + "No jotfile definitions found. Please create a jotfile.\n");
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error processing jotfiles: " << ex.what() << std::endl;
    }
}

/**
 * @brief Function that actually does the hard work of extracting out the section of the file corresponding to the entry.
 * @param entry The Jotfile entry metadata used to extract the content from the Jotfile.
 * @param jf_fn Full filename/path to underlying Jotfile
 */
std::string wibble::WibbleJotter::_get_jotfile_entry(const jf_entry& entry, const std::string& jf_fn) const
{
    std::string content = "";
    std::stringstream jf = wibble::WibbleIO::read_file_into_stringstream(jf_fn);
    try
    {
        std::string line;
        unsigned long line_num = 0;
        bool append_to_entry = false;
        while(std::getline(jf, line, '\n'))
        {
            ++line_num;
            if(line_num < entry.starts_at)  { continue;                }
            if(line_num == entry.starts_at) { append_to_entry = true;  }
            if(line_num > entry.ends_at)    { break; }
            if(append_to_entry) { content.append(line + "\n"); }
        }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _get_jotfile_entry() " << ex.what() << std::endl;
        return "";
    }

    return content;
}

/**
 * @brief Reads the underlying Jotfile (text file), and parses/extracts out the vector of entries.
 * @param wc WibbleConsole utility object
 * @param entry The Jotfile entry metadata used to extract the content from the Jotfile.
 * @param jf_fn Full filename/path to underlying Jotfile
 * @param do_delete Flag to hide "[ Add new entry ]" if we're presenting entry list for purposes of deleting an entry
 */
wibble::jfe wibble::WibbleJotter::_parse_jotfile_into_entries(WibbleConsole& wc, const std::string& jf_fn, const bool do_delete) const
{
    jfe entries;
    try
    {
        std::stringstream jf = wibble::WibbleIO::read_file_into_stringstream(jf_fn);
        std::string line;
        unsigned long line_num = 0;
        unsigned long entry_count = 0;
        bool in_entry = false;
        std::string jfe_date;

        const std::string DATE_OPEN  = "[>->->->->------------ [ ";

        // can hardcode the constants for efficiency; line format is always identical
        const int DATE_START = 25;
        const int DATE_LENGTH = 19;
        const int TITLE_DELIM = 3;
        const int TITLE_MAX_LENGTH = 64;
        const int LINE_DIFF = 2;
        
        bool set_date_line = true;
        while(std::getline(jf, line, '\n'))
        {
            ++line_num;
            // chomp up/throw away lines until we reach the next date opening line
            if(! in_entry && line.find(DATE_OPEN) == std::string::npos) { continue; }
            else
            {
                in_entry = true;
                if(set_date_line) { jfe_date = line.substr(DATE_START, DATE_LENGTH); set_date_line = false; }
            }

            if(in_entry)
            {
                // title line
                if(line.substr(0,TITLE_DELIM) == "[= ")
                {
                    
                    jf_entry entry;
                    entry.date = jfe_date;
                    entry.title = line.substr(TITLE_DELIM, TITLE_MAX_LENGTH);
                    entry.title = wc.trim(entry.title);
                    entry.starts_at = line_num + LINE_DIFF;

                    // try to set the previous entry's ending line number
                    if(line_num >= 2 && entry_count > 0)
                    {
                        try
                        {
                            entries.at(entry_count - 1).ends_at = line_num - LINE_DIFF;
                        }
                        catch(std::out_of_range const& ex) { std::cerr << "Ignoring bad index " << entry_count << " into vector " << std::endl; }
                    }

                    entries.push_back(entry);
                    // now increment the entry index
                    ++entry_count;
                    // std::cout << "DEBUG: added entry. Entry count: " << entry_count << std::endl;
                    in_entry = false;
                    set_date_line = true;
                }
            }
                
        }

        // final entry ends at end of the file
        if(entries.size() != 0)
        { 
            if(entries.at(entries.size() - 1).ends_at == 0) { entries.at(entries.size() - 1).ends_at = line_num; }
        }
        // clean up
        std::stringstream().swap(jf);

        // add our placeholder for adding new entries
        if(! do_delete) // don't show if user is in deletion process
        {
            jf_entry entry;
            entry.date = NEW_DATE_PLACHDR;
            entry.title = "[ Add new entry ]";
            entry.starts_at = 0;
            entry.ends_at = 0;

            entries.push_back(entry);
        }
       
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in _parse_jotfile_into_entries(): " << ex.what() << std::endl;
    }

    // filter titles case insensitively
    if(filter_titles != "")
    {

        jfe f_entries;
        std::string s_expr = filter_titles;
        wc.trim(s_expr);
        wibble::WibbleRecord::str_to_lowercase(s_expr);

        wc.console_write_green_bold(ARW + "Filtering");
        wc.console_write_green(" jotfile titles against '");
        wc.console_write_green_bold(s_expr);
        wc.console_write_green("'.\n\n");

        for(auto const& e: entries)
        {
            std::string c_title = e.title;
            wibble::WibbleRecord::str_to_lowercase(c_title);
            if(c_title.find(s_expr) != std::string::npos) { f_entries.push_back(e); }
        }

        return f_entries;
    }
    else { return entries; }
}

/**
 * @brief Create a brand new Jotfile.
 * @param wc WibbleConsole utility object
 */
bool wibble::WibbleJotter::create_jotfile(WibbleConsole& wc, const std::string& d_category, const std::string& d_name) const
{
    std::string category = (d_category != "") ? d_category : wc.console_write_prompt("Enter category *:", "", "general");
    std::string name     = (d_name != "")     ? d_name     : wc.console_write_prompt("Enter name     *:", "", "");

    wc.trim(category); wc.trim(name);

    wibble::WibbleRecord::sanitise_input(category);
    wibble::WibbleRecord::underscore_to_hyphen(category);
    wibble::WibbleRecord::str_to_lowercase(category);

    wibble::WibbleRecord::sanitise_input(name);
    wibble::WibbleRecord::str_to_uppercase(name);

    if(category == "" || name == "")
    {
        wc.console_write_error_red("Neither category or name can be blank. Aborting.", false);
    }
    else
    {
        bool proceed = true;
        for(auto const& item: jotfiles) { if(category == item.category && name == item.name) { proceed = false; } }

        if(proceed)
        {
            unsigned long index = _find_numeric_gap(jotfiles);
            std::string index_num = std::to_string(index); index_num.insert(0, 8U - std::min(std::string::size_type(8), index_num.length()), '0');
            std::string jf_header = "# " + index_num + " JOTFILE: " + name + "\n";
            std::string dest_fn = jotfile_base_dir + "/" + index_num + "_" + category + "_" + name + ".wib";
            const bool jf_res = WibbleIO::write_out_file_append(jf_header, dest_fn);
            if(jf_res) { wc.console_write_success_green("New Jotfile '" + category + ":" + name + "' successfully created.", false); }
            else       { wc.console_write_error_red("Error writing output file.", false);                                          }
            return jf_res;
        }
        else { wc.console_write_error_red("A jotfile named " + category + ":" + name + " already exists. Aborting.", false); }
    }

    return false;
} 

/**
 * @brief List currently existing Jotfiles.
 * @param wc WibbleConsole utility object
 * @param selector Flag controlling whether to user input selector/prompt
 */
void wibble::WibbleJotter::list_jotfiles(WibbleConsole& wc, const bool selector) const
{
    //std::cout << "DEBUG: selector is = " << selector << std::endl;
    wc.console_write_header("JOTFILES ");
    wc.console_write_hz_divider();
    std::size_t index = 0;
    int add_selector = 0;
    if(selector) { add_selector = 1; }
    for(auto const& e: jotfiles)
    {
        ++index;
        wc.console_write_padded_selector(index, jotfiles.size() + add_selector, "");
        wc.console_write_yellow(e.category); std::cout << ":";
        wc.console_write_green_bold(e.name + "\n");
    }

    if(selector)
    {
        ++index;
        wc.console_write_padded_selector(index, jotfiles.size() + add_selector, "");
        wc.console_write_yellow_bold("[ Add new jotfile ]\n");
    }
    wc.console_write_hz_divider();
}

/**
 * @brief Interactively select a Jotfile from list for working with.
 * @param wc WibbleConsole utility object
 * @param rel_path Flag controlling whether to output filename in relative (to Jotfile base dir) or absolute format
 */
const std::string wibble::WibbleJotter::get_jotfile(WibbleConsole& wc, const bool rel_path) const
{
    if(jotfiles.size() == 0)
    {
        wc.console_write_error_red("No Jotfiles defined. Add one first.");
        return "";
    }
    list_jotfiles(wc, true);
    unsigned long ans = wc.console_present_record_selector(jotfiles.size());
    if(ans != 0)
    {
        const std::string jf_rel = jotfiles.at(ans - 1).fn + DELIM + jotfiles.at(ans -1).category + ":" + jotfiles.at(ans - 1).name;
        if(! rel_path) { return jotfile_base_dir + "/" + jf_rel; }
        else           { return jf_rel;                          }
    }
    else { return ""; }
}

/**
 * @brief Get confirmation before permanent removal of an entire Jotfile.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full filename/path to underlying Jotfile
 */
bool wibble::WibbleJotter::_do_delete_confirm(WibbleConsole& wc, const std::string& jotfile_fn) const
{
    const std::string short_name = std::filesystem::path(jotfile_fn).filename();
    if(std::filesystem::exists(std::filesystem::path(jotfile_fn)))
    {
            wc.console_write_red_bold("Pending deletion: " + short_name + "\n\n");
            wc.console_write_red("The above file will be permanently deleted.\n");
            wc.console_write_red("Input 'delete' to confirm.\n");
            wc.console_write_hz_divider();
            std::string resp = wc.console_write_prompt("Confirm > ", "", "");
            if(resp == "delete")
            {
                bool do_delete = std::filesystem::remove(std::filesystem::path(jotfile_fn));
                if(do_delete) { wc.console_write_success_green("Jotfile successfully removed."); return true; }
                else          { wc.console_write_success_green("Error removing jotfile.");                    }
            }
            else
            {
                wc.console_write_error_red("User cancelled.");
            }
    }
    else
    {
        wc.console_write_error_red("Jotfile does not exist, cannot remove.");
    }

    return false;
}

/**
 * @brief Remove given Jotfile when explicitly specified by filename.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full filename/path to underlying Jotfile
 */
bool wibble::WibbleJotter::direct_remove_jotfile(WibbleConsole& wc, const std::string& jotfile_fn)
{
    return _do_delete_confirm(wc, jotfile_fn);
}

/**
 * @brief Wrapper function to rename a given Jotfile. 
 * @param wc WibbleConsole utility object
 * @param preselect_jf Index to optionally preselect a Jotfile rather than present selector
 */
void wibble::WibbleJotter::rename_jotfile(WibbleConsole& wc, const unsigned long& preselect_jf)
{
    _jotfile_operation(wc, 5, "", preselect_jf);
}

/**
 * @brief Interactively remove a given Jotfile. 
 * @param wc WibbleConsole utility object
 */
void wibble::WibbleJotter::remove_jotfile(WibbleConsole& wc) 
{
    unsigned long j = select_jotfile(wc);
    if(j > 0)
    {
        const std::string jf_filename = jotfile_base_dir + "/" + jotfiles.at(j - 1).fn;
        if(std::filesystem::exists(std::filesystem::path(jf_filename)))
        {
            _do_delete_confirm(wc, jf_filename);
        }
        else
        {
            wc.console_write_error_red("Invalid jotfile selection. Aborting.");
        }
    }
    else
    {
        wc.console_write_error_red("Invalid jotfile selection. Aborting.");
    }
}

/**
 * @brief Function to rename an existing Jotfile. 
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to rename
 */
void wibble::WibbleJotter::_rename_existing_jotfile(WibbleConsole& wc, const std::string& jotfile_fn)
{
    if(! std::filesystem::exists(std::filesystem::path(jotfile_fn)))
    {
        wc.console_write_error_red("Invalid/unreadable jotfile: '" + jotfile_fn + "'.");
    }
    else
    {
        WibbleJotter::jotfile jtf = _decompose_fn_to_jotfile(std::filesystem::path(jotfile_fn).filename());
        wc.console_write_header("RENAME");
    
        std::string category = wc.console_write_prompt("Enter category *:", "", jtf.category);
        std::string name     = wc.console_write_prompt("Enter name     *:", "", jtf.name);

        wc.trim(category); wc.trim(name);

        wibble::WibbleRecord::sanitise_input(category);
        wibble::WibbleRecord::underscore_to_hyphen(category);
        wibble::WibbleRecord::str_to_lowercase(category);

        wibble::WibbleRecord::sanitise_input(name);
        wibble::WibbleRecord::str_to_uppercase(name);

        if(category == "" || name == "")
        {
            wc.console_write_error_red("Neither category or name can be blank. Aborting.");
        }
        else
        {
            // 0 pad number (as string) to given length
            std::string index_num = std::to_string(jtf.index); index_num.insert(0, 8U - std::min(std::string::size_type(8), index_num.length()), '0');
            std::string dest_fn = jotfile_base_dir + "/" + index_num + "_" + category + "_" + name + ".wib";

            if(std::filesystem::exists(std::filesystem::path(dest_fn)))
            { wc.console_write_error_red("A destination jotfile with that name already exists. Aborting."); }
            else
            {
                bool rename_op = wibble::WibbleIO::wmove_file(jotfile_fn, dest_fn);
                if(rename_op) { wc.console_write_success_green("Jotfile renamed successfully."); }
                else          { wc.console_write_error_red("Jotfile renamed successfully.");     }
            }
        }
    }
}

/**
 * @brief Wrapper to interactively select an existing (or add a new) Jotfile.
 * @param wc WibbleConsole utility object
 */
unsigned long wibble::WibbleJotter::select_jotfile(WibbleConsole& wc)
{
    list_jotfiles(wc, true);
    auto sel = wc.console_present_record_selector(jotfiles.size() + 1);
    if(sel == 0 && jotfiles.size() == 0)
    {
         std::string resp = wc.console_write_prompt("No jotfiles present. Create one? (y/n) > ", "", "");
         if(resp != "y" && resp != "Y") { wc.console_write_error_red("User cancelled."); throw wibble::wibble_exit(0); }
         else                           { sel = 1; }
    }
    //std::cout << "DEBUG: sel is = " << sel << " size() is: " << jotfiles.size() << std::endl;
    if((sel - 1) == jotfiles.size() && sel >= 1) // last entry, i.e. "Add new". sel is unsigned, so ensure >= 1
    {
        create_jotfile(wc);
        // remember to empty vector first 
        jotfiles.clear();
        // reparse everything for a fresh new list
        _gather_jotfiles(wc, jotfile_base_dir);
        return select_jotfile(wc);
    } 
    else { return sel; }
}

/**
 * @brief Adds a new entry to a given Jotfile.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to add new entry to
 */
void wibble::WibbleJotter::_add_new_entry(WibbleConsole& wc, const std::string& jotfile_fn) const
{
    const std::string header = JF_ENTRY_PREFIX + wibble::WibbleRecord::generate_date_YMD("") + JF_ENTRY_POSTFIX;
    wc.console_write("\n");
    wc.console_write_hz_divider();
    std::string entry_title = wc.console_write_prompt("Entry title: ", "", ""); wc.trim(entry_title);
    if(entry_title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); throw wibble::wibble_exit(1); }
    std::string entry_txt = wc.console_write_prompt_ml("Input entry text. Press Ctrl-D when done to add the entry.\n\n", "", "");
    if(entry_title.length() >= HEADER_WIDTH) { entry_title = entry_title.substr(0, HEADER_WIDTH); }
    std::string padding = "";
    padding.insert(0, (HEADER_WIDTH - entry_title.length()), ' ');
    //std::cout << "Padding size is: " << padding.length() << "\n";
        
    std::string new_entry = header + TIT_OPEN + entry_title + padding + TIT_CLOSE + JF_ENTRY_TERM + entry_txt;

    wc.console_write_hz_divider();
    WibbleIO::write_out_file_append(new_entry, jotfile_fn);
}

/**
 * @brief Adds a new entry to a given Jotfile (non-interactive, batch).
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to add new entry to
 * @param entry_title Title for Jotfile entry
 * @param entry_text Content of Jotfile entry
 */
bool wibble::WibbleJotter::batch_add_new_entry(WibbleConsole& wc, const std::string& jotfile_name, std::string& entry_title, const std::string& entry_text) const
{
    const std::string header = JF_ENTRY_PREFIX + wibble::WibbleRecord::generate_date_YMD("") + JF_ENTRY_POSTFIX;
    if(entry_title.length() >= HEADER_WIDTH) { entry_title = entry_title.substr(0, HEADER_WIDTH); }
    std::string padding = "";
    padding.insert(0, (HEADER_WIDTH - entry_title.length()), ' ');
    std::stringstream in_ss;
    std::string content;
    try
    {
        in_ss = WibbleIO::read_file_into_stringstream(entry_text); 
        content = in_ss.str();
        std::stringstream().swap(in_ss);
    }
    catch(std::exception& ex) { wc.console_write_error_red("Error reading input file '" + jotfile_name + "'", false); return false; }
    std::string new_entry = header + TIT_OPEN + entry_title + padding + TIT_CLOSE + JF_ENTRY_TERM + content;
    auto spl = jotfile_name.find(":");
    std::string out_jf = "";
    if(spl != std::string::npos)
    {
        const std::string cat  = jotfile_name.substr(0, spl);
        const std::string name = jotfile_name.substr(spl + 1, jotfile_name.length() - spl - 1);
        for(auto const& item: jotfiles)
        {
            if(cat == item.category && name == item.name) { out_jf = jotfile_base_dir + "/" + item.fn; }
        }

        if(out_jf != "") { return WibbleIO::write_out_file_append(new_entry, out_jf);                                                  }
        else             { wc.console_write_error_red("No Jotfile matches specification '" + jotfile_name + "'", false); return false; }
    }
    else
    {
        wc.console_write_error_red("Invalid Jotfile name: '" + jotfile_name + "'", false); 
        return false;
    }
}

/**
 * @brief Select a single Jotfile entry. 
 * @param entries Vector of Jotfile entries
 * @param wc WibbleConsole utility object
 * @param preselect Index value of entry to potentially pre-select (non-interactive)
 */
wibble::jfr wibble::WibbleJotter::select_entry(jfe& entries, WibbleConsole& wc, unsigned long& preselect) const
{
    try
    {
        if(preselect != 0)
        {
            // ensure preselection only works once
            unsigned long ent_n = preselect;
            preselect = 0;
            return entries.at(ent_n - 1);
        }
        else
        {
            unsigned long tot_entries = entries.size();
            unsigned long index = 0;
            for(auto const& entry: entries)
            {
                ++index;
                wc.console_write_padded_selector(index, tot_entries, entry.date + " ┃ " + entry.title + "\n");
            }

            unsigned long sel = wc.console_present_record_selector(tot_entries);

            if(sel == 0 || sel > entries.size()) { throw std::invalid_argument("Bad index"); }
            return entries.at(sel - 1);
        }
    }
    catch(std::exception const& ex)
    {
        wc.console_write_error_red("Invalid entry selection.");
        return jf_entry(); 
    }
}

/**
 * @brief Warn upon failure to create backup copy of Jotfile (i.e. when doing deletion of entries)
 * @param wc WibbleConsole utility object
 */
void wibble::WibbleJotter::_cache_warning(WibbleConsole& wc) const
{
    wc.console_write_error_red("Warning: Unable to create a backup copy of the jotfile.");
    wc.console_write_error_red("Check your cache/temporary directory settings."); 
}

/**
 * @brief Either display, delete, or edit an existing Jotfile entry.
 * @param wc WibbleConsole utility object
 * @param int Integer flag controlling whether to display, delete, or edit
 * @param jf Jotfile entry (metadata indices) to obtain from underlying Jotfile/text file
 * @param jf_fn Full path/filename to Jotfile to corresponding to entry
 */
void wibble::WibbleJotter::display_delete_or_edit_entry(WibbleConsole& wc, int mode,
                                                        const jf_entry& jf, const std::string& jf_fn) 
{
    if(jf.date != "" && jf.starts_at != 0)
    {
        std::string jf_content = _get_jotfile_entry(jf, jf_fn);

        if(mode == 1)      // display/dump
        {
            wc.console_write_success_green("Displaying Jotfile entry."); 
            std::cout << jf_content << std::endl;
        }
        else if(mode == 2) // update/edit
        {
            wc.console_write("\n");
            wc.console_write_hz_divider();
            std::string title = wc.console_write_prompt("Edit title\n", "", jf.title);
            wc.trim(title);
            if(title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); throw wibble::wibble_exit(1); }
            std::string content = wc.console_write_prompt_ml("Edit entry (Ctrl-D when done)\n", "", jf_content);
            //std::cout << std::endl; std::cout << "\n\n\nResult:\n\n\n" << content << std::endl;
            wc.trim(content);
            wc.console_write_hz_divider();
            bool update_result = _write_out_replacement_file(jf_fn, jf, title, content); 
            if(update_result)
            {
                std::string bak_file =  cache_dir + "/" + std::string(std::filesystem::path(jf_fn).filename()) + ".bak";
                bool create_backup = wibble::WibbleIO::wcopy_file(jf_fn, bak_file);
                bool overwrite_with_new = wibble::WibbleIO::wmove_file(jf_fn + ".new", jf_fn);
                if(! create_backup) { _cache_warning(wc); }
                if(overwrite_with_new) { wc.console_write_success_green("Jotfile entry successfully updated."); }
            }
        }
        else if(mode == 3) //delete
        {
            bool update_result = _write_out_replacement_file(jf_fn, jf, "", "", true); 
            if(update_result)
            {
                std::string bak_file =  cache_dir + "/" + std::string(std::filesystem::path(jf_fn).filename()) + ".bak";
                bool create_backup = wibble::WibbleIO::wcopy_file(jf_fn, bak_file);
                bool overwrite_with_new = wibble::WibbleIO::wmove_file(jf_fn + ".new", jf_fn);
                if(! create_backup) { _cache_warning(wc); }
                if(overwrite_with_new) { wc.console_write_success_green("Jotfile entry successfully deleted."); }
            }
        }
    }
}

/**
 * @brief Write out a replacement/new Jotfile (i.e. when editing or deleting an entry).
 * @param orig_jf Full path/filename to Jotfile
 * @param jf_entry Jotfile entry to update 
 * @param new_title String containing replacement entry title
 * @param new_content String containing replacement entry content
 * @param expurge_entry Flag controlling whether to remove/expurge an entry
 */
bool wibble::WibbleJotter::_write_out_replacement_file(const std::string& orig_jf,
                                                       const jf_entry& entry_to_update,
                                                       const std::string& new_title,
                                                       const std::string& new_content,
                                                       bool expurge_entry) const
{
    try
    {
        if(entry_to_update.ends_at <= entry_to_update.starts_at || entry_to_update.starts_at <= 2)
        {
            std::cerr << "Bad update entry" << std::endl;
            return false;
        }

        std::stringstream jf_ss = wibble::WibbleIO::read_file_into_stringstream(orig_jf);
        std::ofstream new_jf;
        new_jf.open(orig_jf + ".new", std::ios::out | std::ios::trunc);
        std::string line;
        unsigned long line_num = 0;
        const int FIELD_WIDTH = 2;
        while(std::getline(jf_ss, line, '\n'))
        {
            ++line_num;

            // === deletion ===
            // override state machine below for case where we want to expurge entry
            if(expurge_entry)
            {
                if(line_num < entry_to_update.starts_at - FIELD_WIDTH - 1)
                { new_jf << line + '\n'; continue; }
                else if(line_num <= entry_to_update.ends_at)
                { continue; }
                else
                { expurge_entry = false; }
            }
                    

            // === ...otherwise, update the entry... ===
                
            // starts_at points to the first line AFTER the header
            if(line_num < entry_to_update.starts_at - FIELD_WIDTH)
            { new_jf << line + '\n'; continue; }

            if(line_num == entry_to_update.starts_at)
            {

                std::string title = new_title;
                if(new_title.length() >= HEADER_WIDTH) { title = new_title.substr(0, HEADER_WIDTH); }
                std::string padding = "";
                padding.insert(0, (HEADER_WIDTH - new_title.length()), ' ');
                std::string rep_title = TIT_OPEN + title + padding + TIT_CLOSE;
                new_jf << rep_title;
                new_jf << JF_ENTRY_TERM;
                new_jf << new_content;
                new_jf << "\n\n";
                continue;
            }

            if(line_num <= entry_to_update.ends_at ) { continue; }

            if(line_num > entry_to_update.ends_at)
            { new_jf << line + '\n'; continue; }
        }

        std::stringstream().swap(jf_ss);
        new_jf.close();

        return true;
    }
    catch(std::exception const& ex)
    {
        return false;
    }
}

/**
 * @brief Dispatcher wrapper for the various Jotfile operations.
 * @param wc WibbleConsole utility object
 * @param op Integer flag to select between view, edit/add, delete entries; or rename of Jotfile
 * @param direct_jf String with override to supply filename non-interactively
 * @param preselect_jf Index value to optionally pre-select a given Jotfile
 * @param preselect_entry Index value to optionally pre-select a given Jotfile entry
 */
void wibble::WibbleJotter::_jotfile_operation(WibbleConsole& wc, const int& op, const std::string& direct_jf,
                                              const unsigned long& preselect_jf, const unsigned long& preselect_entry)
{

    switch(op)
    {
    case 1:
        wc.console_write_green_bold(ARW + "View"); wc.console_write_green(" an entry.\n");
        break;
    case 2:
        wc.console_write_green_bold(ARW + "Edit/Add"); wc.console_write_green(" an entry.\n");
        break;
    case 3:
        wc.console_write_red_bold(ARW + "Delete"); wc.console_write_green(" an entry.\n");
        break;
    case 4:
        wc.console_write_green_bold(ARW + "Add"); wc.console_write_green(" a new entry.\n");
        break;
    case 5:
        wc.console_write_green_bold(ARW + "Rename"); wc.console_write_green(" existing jotfile.\n");
        break;
    default:
        wc.console_write_error_red("Invalid selection. Exiting");
    }

    //std::cout << "DEBUG: direct_jf = " << direct_jf << std::endl;
    if(preselect_jf > 0)
    {
        wc.console_write_green_bold(ARW + "Preselecting");
        wc.console_write_green(" jotfile ");
        wc.console_write_green_bold("number " + std::to_string(preselect_jf) + ".\n");
    }
    if(preselect_entry > 0)
    {
        wc.console_write_green_bold(ARW + "Preselecting");
        wc.console_write_green(" entry ");
        wc.console_write_green_bold("number " + std::to_string(preselect_entry) + ".\n");
    }

    unsigned long j = 1;
    // get jotfile filename if not already supplied
    if(direct_jf == "" && preselect_jf == 0) { j = select_jotfile(wc); }
    else if(preselect_jf >= 1)               { j = preselect_jf;       }

    // second conditinal in below if logic handles edge case of adding
    // a task specific Jotfile via Tasks when no global Jotfiles have been created
    if( (j > 0 && j <= jotfiles.size()) || (jotfiles.size() == 0 && direct_jf != ""))
    {
        const std::string jf_filename = (direct_jf == "") ? jotfile_base_dir + "/" + jotfiles.at(j - 1).fn : direct_jf;

        if(preselect_jf > 0)
        {
            wc.console_write_green_bold(ARW + "Preselected");
            wc.console_write_green(" jotfile: ");
            wc.console_write_green_bold(jotfiles.at(j - 1).name);
            wc.console_write_green(". Category: ");
            wc.console_write_green_bold(jotfiles.at(j - 1).category);
            wc.console_write_green(".\n");
        }

        if(op == 4)
        {
            _add_new_entry(wc, jf_filename);
        }
        else if(op == 5)
        {
            _rename_existing_jotfile(wc, jf_filename);
        }
        else
        {
            // don't show add option if doing entry deletion - makes no sense
            jfe entries = (op != 3) ? _parse_jotfile_into_entries(wc, jf_filename) : _parse_jotfile_into_entries(wc, jf_filename, true);
            jf_entry choice;

            bool finished_adding = false;
            unsigned long sel_entry_num = preselect_entry;
            do
            {
                choice = select_entry(entries, wc, sel_entry_num);

                if(choice.date == NEW_DATE_PLACHDR)
                {
                    direct_add_jotfile_entry(wc, jf_filename);
                    entries = _parse_jotfile_into_entries(wc, jf_filename);
                }
                else if(choice.date == "" || choice.title == "" || choice.starts_at == 0 || choice.ends_at == 0)
                {
                    //wc.console_write_error_red("Invalid entry selected. Aborting.");
                    throw wibble::wibble_exit(1);
                }
                else { finished_adding = true; }
            }
            while (! finished_adding);
            
            if(op == 3)
            {
                wc.console_write_hz_divider();
                wc.console_write_red_bold("Entry pending deletion\n\n");
                wc.console_write_red_bold("Date : "); wc.console_write_red(choice.date + "\n");
                wc.console_write_red_bold("Title: "); wc.console_write_red(choice.title + "\n");
                wc.console_write_hz_divider();
                const std::string resp = wc.console_write_prompt("Input 'delete' to permanently delete above entry\n> ", "", "");
                if(resp != "delete") { wc.console_write_error_red("Deletion cancelled."); throw wibble::wibble_exit(1); }
            }
            display_delete_or_edit_entry(wc, op, choice, jf_filename);
        }
    }
    else
    { wc.console_write_error_red("Invalid selection, exiting."); }
        
}

/**
 * @brief Append a new entry to an existing Jotfile (public accessor).
 * @param wc WibbleConsole utility object
 */
void wibble::WibbleJotter::append_new_entry(WibbleConsole& wc)
{
    _jotfile_operation(wc, 4);
}

/**
 * @brief Append a new entry to an existing Jotfile (public accessor), filename already provided.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to add new entry to
 */
void wibble::WibbleJotter::direct_add_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn)
{
    _jotfile_operation(wc, 4, jotfile_fn);
}

/**
 * @brief Remove an existing entry from a Jotfile (public accessor), filename already provided.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to remove entry from
 */
void wibble::WibbleJotter::direct_delete_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn) 
{
    _jotfile_operation(wc, 3, jotfile_fn);
}

/**
 * @brief Edit an existing entry from a Jotfile (public accessor), filename already provided.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to edit entry in 
 */
void wibble::WibbleJotter::direct_edit_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn) 
{
    _jotfile_operation(wc, 2, jotfile_fn);
}

/**
 * @brief View/dump an existing entry from a Jotfile (public accessor), filename already provided.
 * @param wc WibbleConsole utility object
 * @param jotfile_fn Full path/filename to Jotfile to view entry in 
 */
void wibble::WibbleJotter::direct_view_jotfile_entry(WibbleConsole& wc, const std::string& jotfile_fn) 
{
    _jotfile_operation(wc, 1, jotfile_fn);
}

/**
 * @brief Dump/cat entire raw Jotfile to console.
 * @param wc WibbleConsole utility object
 * @param wib_e WibbleExecutor exeuction/environment object
 * @param preselect_jf Index to preselect an existing Jotfile, non-interactive selection
 */
void wibble::WibbleJotter::dump_jotfile(WibbleConsole& wc, WibbleExecutor& wib_e, const unsigned long& preselect_jf)
{
    try
    {
        unsigned long j = (preselect_jf == 0) ? select_jotfile(wc) : preselect_jf;
        const std::string jf_filename = jotfile_base_dir + "/" + jotfiles.at(j - 1).fn;
        wib_e.run_exec_expansion(wib_e.get_dump_tool(), jf_filename);
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Invalid jotfile selection. Exiting.");
    }
}

/**
 * @brief Edit an existing entry from a Jotfile (public accessor).
 * @param wc WibbleConsole utility object
 * @param preselect_jf Index to preselect an existing Jotfile, non-interactive selection
 * @param preselect_entry Index to preselect an existing Jotfile entry, non-interactive selection
 */
void wibble::WibbleJotter::edit_jotfile_entry(WibbleConsole& wc, const unsigned long& preselect_jf, const unsigned long& preselect_entry) 
{
    _jotfile_operation(wc, 2, "", preselect_jf, preselect_entry);
}

/**
 * @brief View an existing entry from a Jotfile (public accessor).
 * @param wc WibbleConsole utility object
 * @param preselect_jf Index to preselect an existing Jotfile, non-interactive selection
 * @param preselect_entry Index to preselect an existing Jotfile entry, non-interactive selection
 */
void wibble::WibbleJotter::view_jotfile_entry(WibbleConsole& wc, const unsigned long& preselect_jf, const unsigned long& preselect_entry) 
{
    _jotfile_operation(wc, 1, "", preselect_jf, preselect_entry);
}

/**
 * @brief Delete an existing entry from a Jotfile (public accessor).
 * @param wc WibbleConsole utility object
 * @param preselect_jf Index to preselect an existing Jotfile, non-interactive selection
 * @param preselect_entry Index to preselect an existing Jotfile entry, non-interactive selection
 */
void wibble::WibbleJotter::delete_jotfile_entry(WibbleConsole& wc, const unsigned long& preselect_jf, const unsigned long& preselect_entry) 
{
    _jotfile_operation(wc, 3, "", preselect_jf, preselect_entry);
}
