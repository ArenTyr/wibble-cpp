/**
 * @file      cmd_meta.hpp
 * @brief     Handle "meta" subcommand (header file).
 * @details
 *
 * Either display or edit/update metadata for a given Wibble Node.
 * Individual keys can be output or entire set of metadata
 * simply displayed.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CMD_META_H
#define CMD_META_H

#include "cmd_structs.hpp"
#include "wibbleexecutor.hpp"

namespace wibble
{
    class WibbleCmdMetadata
    {
    private:
        int _check_arg_count(metadata_options& META);
        void _relationship_operation(pkg_options& OPTS, WibbleConsole& print, WibbleRecord& w, WibbleExecutor& wib_e);
        void _update_record(pkg_options& OPTS, WibbleConsole& print, WibbleRecord& w, WibbleExecutor& wib_e, const bool archived_db);
        void _print_field(WibbleRecord& w, metadata_options& META);
    public:
        void handle_metadata(pkg_options& OPTS, search_filters& SEARCH, metadata_options& META);
    };
}
#endif
