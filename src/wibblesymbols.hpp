/**
 * @file      wibblesymbols.hpp
 * @brief     Various standard namespace global symbols used program-wide (header file).
 * @details
 *
 * This class generates a bash shell command which executes to
 * generate a compressed archive of all of the user's Wibble data.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_SYMBOLS_H
#define WIBBLE_SYMBOLS_H

#include <string>

// common/global symbols
namespace wibble
{
    const std::string BOOKMK_SCR = "/bookmark_scripts";
    const std::string EXEC_PREFIX = "WIB_EXEC:";
    const std::string FIELD_DELIM = "|";
    const std::string WORKSTUB = "/wibble-workfile";

    const std::string COLON_DELIM = ":";
    const std::string KP_DELIM = "%";
    const std::string NONE_IDENT = "___NONE___";
    const std::string REC_IDENT = "[ RELATIONSHIPS ]";
    const std::string REL_NODES = "/related_nodes.wib";
    const std::string TASKDIR_PLCHDR    = "___TASKDIR___";
    const std::string WIB_HID_DIR = "/.wibble";

    // time_t value for Monday 1900-01-01 00:00:01
    const long NULLDATETIME = -2208988799;

    const int NULL_INT_VAL = -999999;

    // Global needed to deal with int(*fn)() function pointer 
    inline std::string PROMPT_TXT = "UNSET_PROMPT > ";
    inline bool ENABLECOLOUR = true;

    // global toggle to switch on/off verbose mode
    inline bool VERBOSEMODE = false;

    // global toggleabout arrow indicator
    inline std::string ARW = "[→] ";
    inline std::string SUC = "[✓] ";
    inline std::string ERR = "[х] ";
    inline std::string TCK = "✓";
    inline std::string CRS = "х";

    // for pretty-printing lists
    inline int calc_padding_width(unsigned long tot_records)
    { return std::to_string(tot_records).length(); }
}
#endif
