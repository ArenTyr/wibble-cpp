/**
 * @file      wibbleconsole.cpp
 * @brief     Provides various functionality for ouput to terminal.
 * @details
 *
 * Provides colourised output to the terminal courtesy of the rang
 * library, plus various utility functions for getting and cleaning
 * user input.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <iomanip>
#include <limits>
#include <readline/readline.h>

#include "external/rang.hpp"

#include "wibbleconsole.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Initialise/enable/start static bool used by for multline input with GNU Readline.
 */
void wibble::WibbleConsole::init_ml_input()
{
    ml_input_chomp = true;
}

/**
 * @brief Disable/end static bool used by for multline input with GNU Readline.
 */
void wibble::WibbleConsole::end_ml_input()
{
    ml_input_chomp = false;
}

/**
 * @brief Check whether we're still getting multiline user input.
 */
bool wibble::WibbleConsole::get_ml_input()
{
    return ml_input_chomp;
}

/**
 * @brief Used by function pointer to pre-fill text for GNU Readline prompt.
 */
int wibble::WibbleConsole::pre_prompt_insert()
{
    rl_insert_text(PROMPT_TXT.c_str());
    rl_redisplay();
    return 0;
}

/**
 * @brief Override/rebind enter key to add a newline rather than end input GNU Realine (multiline).
 * @param count Unused parameter, required for Readline library method signature
 * @param key Unused parameter, required for Readline library method signature
 */
int wibble::WibbleConsole::append_rl_ml_input(int count, int key)
{
   (void)count; (void)key;
   rl_on_new_line();
   return 0;
}

/**
 * @brief Terminate multiline input; triggered by Ctrl-D keysequence.
 * @param count Unused parameter, required for Readline library method signature
 * @param key Unused parameter, required for Readline library method signature
 */
int wibble::WibbleConsole::end_rl_ml_input(int count, int key)
{
   (void)count; (void)key;
   end_ml_input();
   rl_done = 1;
   return 0;
}

/**
 * @brief Prepare/setup a GNU Realine prompt with multiple lines of entry.
 * @param colour_txt String with prefilled/colour text
 * @param prompt String with uncoloured prompt text
 * @param default_txt String with prefilled default text on prompt line, user editable
 * @param default_txt Reference to string to populate/build with resultant display prompt
 */
void wibble::WibbleConsole::_setup_rl_prompt(std::string& colour_txt, const std::string& prompt, std::string& default_txt, std::string& disp_prompt) const
{
    if(prompt != "") { disp_prompt = prompt + " "; }
    if(colour_txt != "")
    {
        if(ENABLECOLOUR) { std::cout << rang::fgB::yellow << rang::style::bold << colour_txt << rang::style::reset; }
        else             { std::cout << colour_txt; }
        // enforce one blank space, prevents the user from being able to
        // backspace over and make the left-hand prompt disappear entirely
        disp_prompt = " " + disp_prompt;
    }
        
    PROMPT_TXT = default_txt;
    int (*ppi_fn)() { &pre_prompt_insert };
    if(ppi_fn) { rl_pre_input_hook = ppi_fn; }
}

/**
 * @brief Get user input via a GNU Readline prompt.
 * @param colour_txt String with prefilled/colour text
 * @param prompt String with uncoloured prompt text
 * @param default_txt String with prefilled default text on prompt line, user editable
 */
std::string wibble::WibbleConsole::console_write_prompt(std::string colour_txt, const std::string& prompt, std::string default_txt) const
{
    std::string disp_prompt;
    _setup_rl_prompt(colour_txt, prompt, default_txt, disp_prompt);

    char* inpt = readline(disp_prompt.c_str());
    std::string result = std::string(inpt);

    std::free(inpt);
    colour_txt = "";
    default_txt = "";
    return result;
}

/**
 * @brief Get user input via a GNU Readline prompt, multiline variant termined by Ctrl-D.
 * @param colour_txt String with prefilled/colour text
 * @param prompt String with uncoloured prompt text
 * @param default_txt String with prefilled default text on prompt line, user editable
 */
std::string wibble::WibbleConsole::console_write_prompt_ml(std::string colour_txt, const std::string& prompt, std::string default_txt) 
{
    std::string disp_prompt;
    _setup_rl_prompt(colour_txt, prompt, default_txt, disp_prompt);
    
    rl_bind_key('\n', append_rl_ml_input);
    rl_bind_keyseq("\\C-d", end_rl_ml_input);

    std::string result = "";

    init_ml_input();
    char* inpt = nullptr;
    size_t counter = 0;
    bool clear_prefill = true;
    long ref_count = 0;
    while(get_ml_input())
    {
        try
        {
            ++ref_count; 
            inpt = readline(("[" + std::to_string(++counter) + "] " + disp_prompt).c_str());
            if(clear_prefill) { PROMPT_TXT = ""; clear_prefill = false; }
            
            if(inpt) { result.append(std::string(inpt) + "\n"); }
            else     { end_ml_input(); }

            std::free(inpt);
            inpt = nullptr;
            --ref_count; 
        }
        catch (std::exception& ex) { std::cout << "Exception in ml input: " << ex.what() << std::endl; end_ml_input(); }
    }

    if(ref_count > 0) { if(inpt) { std::free(inpt); inpt = nullptr; } }
    return result;
}

/**
 * @brief std::cout wrapper with newline
 */
void wibble::WibbleConsole::console_write(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else        { std::cout << msg << '\n'; }
}

/**
 * @brief Helper function write green coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_green(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fg::green << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write bold and green coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_green_bold(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::green << rang::style::bold << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write bold and grey coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_grey_bold(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::gray << rang::style::bold << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write yellow coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_yellow(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::yellow << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write bold and yellow coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_yellow_bold(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::yellow << rang::style::bold << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write red coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_red(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::red << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write bold and red coloured text to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_red_bold(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::red << rang::style::bold << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write bold grey text on blue background to console.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_grey_on_blue(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::gray << rang::style::bold << rang::bg::blue << msg << rang::style::reset; }
    else                  { std::cout << msg; }
}

/**
 * @brief Helper function write a standard numerical input selector (blue on grey) with pretty-printed spacing.
 * @param index size_t Value with current counter/number to display 
 * @param record_count size_t Total number of records, used for calculating padding width
 * @param non_numeric Optional string to override display of number for use with a lettered selector
 */
void wibble::WibbleConsole::console_write_padded_selector(const size_t& index, const size_t& record_count, const std::string& text, const std::string& non_numeric, const bool allow_silence)
{

    if(silence && allow_silence) { }
    else if(ENABLECOLOUR)
    {
        std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
        if(non_numeric == "") { std::cout << std::setfill('0') << std::setw(calc_padding_width(record_count)); }
        else                  { std::cout << std::setfill(' ') << std::setw(calc_padding_width(record_count)); }
        if(non_numeric == "") { std::cout << index << "]" << rang::style::reset << " ┃ "; }
        else                  { std::cout << non_numeric << "]" << rang::style::reset << " ┃ "; }
        
        std::cout << text;
    }
    else
    {
        if(non_numeric == "") { std::cout << "[" << std::setfill('0') << std::setw(calc_padding_width(record_count)); }
        else                  { std::cout << "[" << std::setfill(' ') << std::setw(calc_padding_width(record_count)); }
        if(non_numeric == "") { std::cout << index << "]" << " ┃ "; }
        else                  { std::cout << non_numeric << "]" << " ┃ "; }

        std::cout << text;
    }
}

/**
 * @brief Write out a standard "success" message in bold green with a tickmark.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_success_green(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::green << rang::style::bold << SUC + msg << rang::style::reset << '\n'; }
    else                  { std::cout << SUC + msg + '\n'; }
}

/**
 * @brief Write out a standard "error" message in bold red with a error mark.
 * @param msg Reference to string to output to console.
 */
void wibble::WibbleConsole::console_write_error_red(const std::string &msg, const bool allow_silence) const
{
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR) { std::cout << rang::fgB::red << rang::style::bold << ERR << msg << rang::style::reset << '\n'; }
    else                  { std::cout << ERR + msg + '\n'; }
}

/**
 * @brief Write out a standard "header" section in bold yellow to a normalised width.
 * @param msg Reference to string to with short header text (< 10 chars)
 */
void wibble::WibbleConsole::console_write_header(const std::string& hding, const bool allow_silence) const
{
    // Note: make odd length words "balanced" by passing parameter through with extra " " on end
    //std::string header_open = "=======================================================";
    
    std::string full_header = "";
    const int MAXWIDTH = 10;
    if(hding.length() > MAXWIDTH)
    {
        full_header = "━━━━━━━━━━━━━━━━━━━━━━━ [ .... ] ━━━━━━━━━━━━━━━━━━━━━━";
    }
    else
    {
        // ensure all headers end up all the same total width
        const int padcount = (MAXWIDTH - hding.length()) / 2;
        std::string header_open = "━━━━━━━━━━━━━━━━━━━━ [ ";
        header_open.append(padcount + (MAXWIDTH % 2), ' ');
        std::string header_close = "";
        header_close.append(padcount, ' ');
        header_close.append   (" ] ━━━━━━━━━━━━━━━━━━━━");
        full_header = header_open + hding + header_close;
    }
    
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR)
    { std::cout << rang::fgB::yellow << rang::style::bold << full_header << rang::style::reset << '\n'; }
    else { std::cout << full_header << '\n'; }
}

/**
 * @brief Write out a standard  yellow unicode divider/ruled line to a normalised width.
 */
void wibble::WibbleConsole::console_write_hz_divider(const bool allow_silence) const
{
    std::string hl_line = "───────────────────────────────────────────────────────";
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR)
    { std::cout << rang::fgB::yellow << rang::style::bold << hl_line << rang::style::reset << '\n'; }
    else { std::cout << hl_line << '\n'; }
}

/**
 * @brief Write out a standard  yellow unicode heavy divider/ruled line to a normalised width.
 */
void wibble::WibbleConsole::console_write_hz_heavy_divider(const bool allow_silence) const
{
    std::string heavy_hl_line = "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━";
    if(silence && allow_silence) { }
    else if(ENABLECOLOUR)
    { std::cout << rang::fgB::yellow << rang::style::bold << heavy_hl_line << rang::style::reset << '\n'; }
    else { std::cout << heavy_hl_line << '\n'; }
}


/**
 * @brief Present a standardised input prompt for user selection of record option.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
unsigned long wibble::WibbleConsole::console_present_record_selector(unsigned long total_results) const
{
    if(total_results == 0) { return 0; }

    if(ENABLECOLOUR)
    { std::cout << rang::fgB::yellow << rang::style::bold << "\nSelect choice [1:" << total_results << "] > " << rang::style::reset; }
    else { std::cout << "\nSelect choice [1:" << total_results << "] > "; }

    unsigned long answer = 0;
    try
    {
        std::string input;
        std::cin >> input;
        answer = std::atol(input.c_str());
        if(answer > total_results)  { answer = 0; }
    }
    catch(std::exception &ex)
    {
        answer = 0;
    }

    // use 0 as the error value
    std::cout << '\n';
    return answer;
}

/**
 * @brief Present a standardised input prompt for user selection of record option.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::set<int> wibble::WibbleConsole::console_present_range_record_selector(unsigned long total_results)
{
    std::set<int> selection;
    if(total_results == 0) { return selection; }

    try
    {
        std::string input = console_write_prompt("Multi-select items [1:" + std::to_string(total_results) + "] > ", "", "");
        console_write("");
        // throw away whitespace characters, resize container/string pointer
        input.erase(std::remove_if(input.begin(), input.end(), [](unsigned char x) { return std::isspace(x); }), input.end());
        if(input == "*") { selection.insert(NULL_INT_VAL); return selection; }

        // state machine parser for ',' and '-' symbols
        std::stringstream input_ss{input};
        std::string token;
        while(std::getline(input_ss, token, ','))
        {
            auto hyph_sep = token.find("-"); 
            if(hyph_sep != std::string::npos) // range present
            {
                // try splitting into LHS (lower) / RHS (upper)
                std::string lhs = token.substr(0, hyph_sep);
                std::string rhs = token.substr(hyph_sep + 1, token.length() - hyph_sep - 1);
                // use ulong to avoid range overflows
                unsigned long lhs_l = std::atol(lhs.c_str());
                unsigned long rhs_l = std::atol(rhs.c_str());

                // check within sane limits and that lhs < rhs
                // limit result input parsing to less than 2 billion...  (2,147,483,647)
                if((lhs_l != 0 && lhs_l <= total_results && lhs_l < std::numeric_limits<int>::max()) &&
                   (rhs_l != 0 && rhs_l <= total_results && rhs_l < std::numeric_limits<int>::max()) &&
                   (lhs_l < rhs_l))
                {
                    int l_index = std::atoi(lhs.c_str());
                    int r_index = std::atoi(rhs.c_str());
                    while(l_index <= r_index) { selection.insert(l_index); ++l_index; }
                }
                else
                {
                    if(lhs_l >= rhs_l)         { console_write_error_red("Invalid range specified: '" + lhs + "-" + rhs + "'"); }
                    if(lhs_l >= total_results) { console_write_error_red("Input exceeds available records: '" + lhs + "'");     }
                    if(rhs_l >= total_results) { console_write_error_red("Input exceeds available records: '" + rhs + "'");     }
                    throw std::invalid_argument("Bad input");
                }
            }
            else // just a value
            {
                unsigned long answer = std::atol(token.c_str());
                if(answer != 0 && answer <= total_results && answer < std::numeric_limits<int>::max())
                {
                    int r_index = std::atoi(token.c_str());
                    selection.insert(r_index);
                }
                else
                {
                    if(answer >= total_results) { console_write_error_red("Input exceeds available records: '" + std::to_string(answer) + "'"); }
                    throw std::invalid_argument("Bad input");
                }
            }
        }
    }
    catch(std::exception &ex)
    {
        // return empty vector in error case
        return std::set<int>();
    }

    return selection;
}


// most efficient way to elimiate whitspace, C++ style
// https://stackoverflow.com/questions/1798112/removing-leading-and-trailing-spaces-from-a-string
// const char t default = " \t\n\r\f\v"
// trim from left
/**
 * @brief In place removal of empty/whitespace in string from left-hand side.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::string& wibble::WibbleConsole::ltrim(std::string& s, const char* t)
{
    s.erase(0, s.find_first_not_of(t));
    return s;
}

/**
 * @brief In place removal of empty/whitespace in string from right-hand side.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::string& wibble::WibbleConsole::rtrim(std::string& s, const char* t)
{
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}

// trim from left & right
/**
 * @brief In place removal of empty/whitespace in string from both sides.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::string& wibble::WibbleConsole::trim(std::string& s, const char* t)
{
    return ltrim(rtrim(s, t), t);
}

// as above, copying versions
/**
 * @brief Copying removal of empty/whitespace in string from left-hand side.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::string wibble::WibbleConsole::ltrim_copy(std::string s, const char* t)
{
    return ltrim(s, t);
}

/**
 * @brief Copying removal of empty/whitespace in string from right-hand side.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::string wibble::WibbleConsole::rtrim_copy(std::string s, const char* t)
{
    return rtrim(s, t);
}

/**
 * @brief Copying removal of empty/whitespace in string from both sides.
 * @param total_results unsigned long/total results in order to calculate pretty-printed padding width
 */
std::string wibble::WibbleConsole::trim_copy(std::string s, const char* t)
{
    return trim(s, t);
}

/**
 * @brief Standardised width/truncation of displayed strings so listings are vertically/column aligned.
 * @param target String to convert to pretty-printed/truncated display string
 * @param Size in characters of new target string
 */
std::string wibble::WibbleConsole::set_pretty_width(const std::string& target, const std::size_t target_width)
{
    std::string pretty;
    if(target.length() > target_width + 3)
    {
        pretty = "..." + target.substr(target.length() - (target_width), target.length());
    }
    else
    {
        std::string padding = ""; padding.insert(0, (target_width - target.length()) + 3, ' '); 
        pretty = target + padding; 
    }
    return pretty;
}

//void wibble::WibbleConsole::init_output()    { wibble::WibbleConsole::silence = false; }
void wibble::WibbleConsole::enable_output()  { silence = false; }
void wibble::WibbleConsole::silence_output() { silence = true;  }
    
