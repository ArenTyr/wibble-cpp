/**
 * @file      wibbledbrgdatabase.cpp
 * @brief     Provides all of the core plain-text "database" functionality.
 * @details
 *
 * Provides all of the features related to the plain-text database
 * behind all of the Wibble "Nodes", with functionality for searching, persisting,
 * deleting and editing of records; associating and attaching and working with the related
 * Node data directory and all the files found within it.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <array>
#include <fstream>

#include "wibbledbrgdatabase.hpp"
#include "wibbleexit.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Insert a recutils compatible header, in case user should wish to search/run recutils on the database file.
 *
 * Example: recsel -t Wibble -e 'Title = "Some title"' 
 * 
 */
const std::string wibble::WibbleRGDatabase::_rgdb_header()
{
    std::string header;
    header.append("# -*- mode: rec -*-\n");
    header.append("# --- [BEGIN] RECUTILS COMPATIBLE HEADER ---\n");
    header.append("%rec: Wibble\n");
    header.append("%key: NoteId\n");
    header.append("%type: NoteId uuid\n");
    header.append("%type: Date date\n");
    header.append("%auto: NoteId Date\n");
    header.append("%sort: Date\n");
    header.append("%mandatory: Title Filetype\n");
    header.append("# --- [END] RECUTILS COMPATIBLE HEADER ---\n");
    header.append("# --- WIBBLE DATABASE STARTS HERE ---\n\n");

    return header;
}


/**
 * @brief Output red warning on occurrence of a database field integrity failure/corruption.
 * @param wc general console utility object
 * @param record_index number with index/offset into current record
 * @param line string with current line read in from database file
 * @param line_num long value with line number in database file
 */
void wibble::WibbleRGDatabase::_database_field_error(WibbleConsole& wc, const long& record_index, const std::string& line, const unsigned long line_num) const
{
    wc.console_write_error_red("Malformed record at index " + std::to_string(record_index));
    wc.console_write_red(ARW + "Bad record line: " + line + "\n");
    wc.console_write_red(ARW + "Located at line number: "); wc.console_write_yellow(std::to_string(line_num) + "\n");
    // records are consistent format (size and field ordering), we exploit this during verification/error output
    switch(record_index)
    {
    case 0:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.date + "' field\n");
        break;
    case 1:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.title + "' field\n");
        break;
    case 2:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.noteid + "' field\n");
        break;
    case 3:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.desc + "' field\n");
        break;
    case 4:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.type + "' field\n");
        break;
    case 5:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.proj + "' field\n");
        break;
    case 6:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.tags + "' field\n");
        break;
    case 7:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.cls + "' field\n");
        break;
    case 8:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.dd + "' field\n");
        break;
    case 9:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.kp + "' field\n");
        break;
    case 10:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.cust + "' field\n");
        break;
    case 11:
        wc.console_write_red(ARW + "Expected '" + FIELD_DEF.lids + "' field\n");
        break;
    }

}

/**
 * @brief Convert the serialized record/read in the record into a vector of WibbleRecords from input vector of arrays
 * @param rg_results input vector of fixed length arrays to transfer into WibbleRecord object
 */
wibble::WibResultList wibble::WibbleRGDatabase::deserialize_wibble_record(const std::vector<std::array<std::string, 13>>& rg_results) const
{
    // std::unique_ptr vector list 
    WibResultList results (new std::vector<wibble::WibbleRecord>); 

    for(auto wib_record: rg_results)
    {
        // check if filetype is registered by user as a custom binary type
        const bool bin_type = (bin_map.count(wib_record[4]) == 0) ? false : true; 
        // directly insert the new record
        results->emplace_back(wibble::WibbleRecord(std::move(wib_record[0]),
                                                   std::move(wib_record[1]),
                                                   std::move(wib_record[2]),
                                                   std::move(wib_record[3]),
                                                   std::move(wib_record[4]),
                                                   std::move(wib_record[5]),
                                                   std::move(wib_record[6]),
                                                   std::move(wib_record[7]),
                                                   std::move(wib_record[8]),
                                                   std::move(wib_record[9]),
                                                   std::move(wib_record[10]),
                                                   std::move(wib_record[11]),
                                                   bin_type));
    }
    return results;
}

/**
 * @brief Core/critical function for retrieving database records. 
 *
 * Generate a pointer to a heap allocated vector of WibbleRecords,
 * selecting a subset of records based on the database query.
 *
 * Leverage deliberate standardisation of database records together with
 * ripgrep context separator per field, provides perfectly formed record
 * output, together with integrity checking. Database format should be
 * both strict and rigidly enforced with strong integrity checks at every
 * stage to guarantee database remains forever clean or easily fixible.
 * 
 * Offset mapping:
 *
 * rg "^Date: " /tmp/foo.rec -N -B 1 -A 11 --no-context-separator  
 * rg "^Title: " /tmp/foo.rec -N -B 2 -A 10 --no-context-separator  
 * rg "^NoteId: " /tmp/foo.rec -N -B 3 -A 9 --no-context-separator  
 * rg "^Description: " /tmp/foo.rec -N -B 4 -A 8 --no-context-separator  
 * rg "^Filetype: " /tmp/foo.rec -N -B 5 -A 7 --no-context-separator  
 * rg "^Project: " /tmp/foo.rec -N -B 6 -A 6 --no-context-separator  
 * rg "^Tags: " /tmp/foo.rec -N -B 7 -A 5 --no-context-separator  
 * rg "^DataDirs: " /tmp/foo.rec -N -B 8 -A 4 --no-context-separator  
 * rg "^KeyPairs: " /tmp/foo.rec -N -B 9 -A 3 --no-context-separator  
 * rg "^Custom: " /tmp/foo.rec -N -B 10 -A 2 --no-context-separator  
 * rg "^LinkedIds: " /tmp/foo.rec -N -B 11 -A 1 --no-context-separator  
 *
 * For deletion/updating, use -n for line number indices in file.
 *
 * @param filter_expression database query/set of fields with patterns to match against with mutliple iterations
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
wibble::WibResultList wibble::WibbleRGDatabase::select_database_records(const wibble::WibbleDBRGQuery& filter_expression,
                                                                        const std::string& wib_db_path, const std::string& WORKFILE) const
{

    wibble::WibbleConsole wc;
    const int RECORD_LENGTH = 13;                   //!< WibbleRecord format is deliberately fixed/constrained for performance and standardisation
    const int END_RECORD_INDEX = RECORD_LENGTH - 1; 
    std::string line;
    std::fstream fs;
    std::stringstream result_stream;

    std::vector<std::array<std::string, RECORD_LENGTH>> rg_results;
    bool found_db_start = false;
    bool search_result_set = false;

    // This parses entire database into records (i.e. "--everything")
    if(filter_expression.get_all_flag())
    {
        // no need for ripgrep, just read entire database file directly in
        result_stream << std::ifstream(wib_db_path).rdbuf();
    }
    else
    {
        // partial/search result set, so set flag
        search_result_set = true;

        if(filter_expression.get_exact_flag())
        { wc.console_write_green(ARW + "EXACT & case sensitive matching is "); wc.console_write_yellow("enabled.\n"); }

        if(filter_expression.get_fixed_flag())
        { wc.console_write_green(ARW + "FIXED string (also implies EXACT + non-regexp) matching is "); wc.console_write_yellow("enabled.\n"); }
        
        // generate the search results into a temporary file
        // this line generates the ripgrep expression to run on the database file
        const std::string query_expr = filter_expression.generate_query() + " " + wib_db_path + " > " + WORKFILE;
        //std::cerr << "DEBUG: query_expr = " << query_expr << std::endl;
        int res = std::system(query_expr.c_str());
        if(res == 1) // no matches
        {
            wc.console_write_error_red("No matches found.");
            return deserialize_wibble_record(rg_results);
        }
        else
        {
            // add newline character to end of ripgrep results to indicate termination of last record
            std::ofstream rg_res(WORKFILE, std::ios::app);
            rg_res.put('\n');
            rg_res.close();
            if(bool(rg_res.rdstate() & rg_res.failbit) || bool(rg_res.rdstate() & rg_res.badbit))
            { wc.console_write_error_red("WARNING: Error appending newline to ripgrep results file."); }
            //std::cout << "DEBUG: reading file in " << std::endl;
            // now read generated ripgrep results/matches into stringstream
            result_stream << std::ifstream(WORKFILE).rdbuf();
        }
    }
    
    std::array<std::string, RECORD_LENGTH> result;
    int record_index = 0;
    unsigned long line_num = 0;
    unsigned long valid_records = 0;
    bool clean_db_file = true;
    bool header_delim_check = true;
    std::string last_line;

    // no db header on search results/filter results
    if(search_result_set)
    {
        //std::cerr << "in search_result_set_check" << std::endl;
        found_db_start = true;
        // don't attempt to verify header delimiter on search result set (obviously not present)
        header_delim_check = false;
    }
 
    wc.console_write_green(ARW + "Verifying database integrity...\n");

    while(std::getline(result_stream, line)) 
    {
        ++line_num;
        
        // skip over recutils compatible header - full database dump --everything only
        if(! found_db_start)
        {
            if(line == "# --- WIBBLE DATABASE STARTS HERE ---")
            {
                found_db_start = true;
                //process_first_record = true;
            }
            continue;
        }

        // blank lines
        if(line.length() == 0)
        {
            //std::cout << "DEBUG: Pushing record, index is: " << record_index << std::endl; 
            // all correctly formed records end at index 12
            // add fully generated record into vector
            if(record_index == END_RECORD_INDEX)     { rg_results.push_back(result); ++valid_records; }
            else if(record_index > END_RECORD_INDEX)
            {
                std::string msg = "WARNING. Potential database corruption.\n";
                msg.append(ARW + "Malformed/excess record detected at line: " + std::to_string(line_num) + "\n");
                msg.append(ARW + "Problem line: '" + line + "'");
                wc.console_write_error_red(msg);
                clean_db_file = false;
            }
            else if(record_index > 0)
            {
                std::string msg = "WARNING. Potential database corruption.\n";
                msg.append(ARW + "Malformed/incomplete record detected at/before line: " + std::to_string(line_num) + "\n");
                msg.append(ARW + "Problem line: '" + line + "'");
                wc.console_write_error_red(msg);
                clean_db_file = false;
            }

            record_index = 0; 
        }
        else
        {
            if(record_index > RECORD_LENGTH)
            { clean_db_file = false; std::cerr << "ERROR: Warning, over record length" << std::endl; }
            else
            {
                try
                {
                    bool clean_record = true;
                    // verify, match, and assign field values into array of strings in correct index
                    switch(record_index)
                    {
                    case 0:
                        if(line.find(FIELD_DEF.date) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_DATE, std::string::npos);   // 1. Date:
                        break;
                    case 1:
                        if(line.find(FIELD_DEF.title) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_TITLE, std::string::npos);  // 2. Title:
                        break;
                    case 2:
                        if(line.find(FIELD_DEF.noteid) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_NOTEID, std::string::npos); // 3. NoteId:
                        break;
                    case 3:
                        if(line.find(FIELD_DEF.desc) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_DESC, std::string::npos);   // 4. Description:
                        break;
                    case 4:
                        if(line.find(FIELD_DEF.type) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_TYPE, std::string::npos);   // 5. Filetype:
                        break;
                    case 5:
                        if(line.find(FIELD_DEF.proj) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_PROJ, std::string::npos);   // 6. Project:
                        break;
                    case 6:
                        if(line.find(FIELD_DEF.tags) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_TAGS, std::string::npos);   // 7. Tags:
                        break;
                    case 7:
                        if(line.find(FIELD_DEF.cls) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_CLS, std::string::npos);    // 8. Class:
                        break;
                    case 8:
                        if(line.find(FIELD_DEF.dd) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_DD, std::string::npos);     // 9. DataDirs:
                        break;
                    case 9:
                        if(line.find(FIELD_DEF.kp) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_KP, std::string::npos);     // 10. KeyPairs:
                        break;
                    case 10:
                        if(line.find(FIELD_DEF.cust) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_CUST, std::string::npos);   // 11. Custom:
                        break;
                    case 11:
                        if(line.find(FIELD_DEF.lids) == std::string::npos) { clean_record = false; clean_db_file = false; }
                        result[record_index] = line.substr(WibbleDBQueryInterface::W_LIDS, std::string::npos);   // 12. LinkedIds:
                        break;
                    default:
                        break;
                    }

                    if(! clean_record)
                    {
                        // immediately warn user regarding corruption/unexpected field;
                        // prevents propagation/continued work with unclean file
                        _database_field_error(wc, record_index, line, line_num);
                    }
                }
                catch(std::exception& err)
                {
                    result[record_index] = "___MALFORMED ENTRY___: Check DB integrity";
                    clean_db_file = false; 
                }
            }
            ++record_index;
        }

        // get last record
        if(! result_stream.good())
        {
            if(record_index > RECORD_LENGTH)
            {
                //std::cout << "DEBUG: Warning, over record length" << std::endl;
                std::string msg = "WARNING. Potential database corruption.\n";
                msg.append(ARW + "Malformed/incomplete record detected at line: " + std::to_string(line_num) + "\n");
                msg.append(ARW + "Problem line: '" + line + "'");
                wc.console_write_error_red(msg);
                clean_db_file = false;
            }
            else { result[record_index] = line; }
        }
    }

    if(record_index == END_RECORD_INDEX)
    {
        //std::cout << "DEBUG: Pushing final record, index is: " << record_index << std::endl; 
        rg_results.push_back(result);
        ++valid_records;
    }
    else if(record_index != 0)
    {
        std::string msg = "WARNING. Potential database corruption.\n";
        msg.append(ARW + "Unexpected end of file record detected at line: " + std::to_string(line_num) + "\n");
        msg.append(ARW + "Expected index 0 (blank line) but instead got trailing content.");
        wc.console_write_error_red(msg);
        clean_db_file = false;
    }

    if(header_delim_check && ! found_db_start)
    {
        wc.console_write_error_red("Database header delimiter missing. Please fix. Aborting.");
        wc.console_write_red(ARW + "Expected delimiter PRIOR to first record:\n\n");
        wc.console_write("# --- WIBBLE DATABASE STARTS HERE ---\n");
        clean_db_file = false;
    }

    if(clean_db_file)
    {
        wc.console_write_success_green("Clean database file, passed integrity check.");
        if(search_result_set) { wc.console_write_success_green(std::to_string(valid_records) + " record(s) present in filtered results."); }
        else                  { wc.console_write_success_green(std::to_string(valid_records) + " record(s) present in database."); }
    }
    else              { wc.console_write_error_red("Database file '" + wib_db_path + "' is corrupted. Please fix. Aborting.");
                        throw wibble::wibble_exit(1); }
        
    return deserialize_wibble_record(rg_results);
}

/**
 * @brief Wrapper function to retrieve entire set of database records.
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
wibble::WibResultList wibble::WibbleRGDatabase::get_all_wibble_records(const std::string& wib_db_path, const std::string& WORKFILE) const
{
    wibble::WibbleDBRGQuery query;
    query.set_all_flag(true);
    return select_database_records(query, wib_db_path, WORKFILE);
}

/**
 * @brief Wrapper function to get set of records based on content of WibbleDBRGQuery object.
 * @param query_exp WibbleDBQueryInterface query object with expression(s) to generate final result set
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
wibble::WibResultList wibble::WibbleRGDatabase::get_wibble_records(const wibble::WibbleDBQueryInterface* query_exp,
                                                                   const std::string& wib_db_path, const std::string& WORKFILE) const
{
    const wibble::WibbleDBRGQuery* w_query = static_cast<const wibble::WibbleDBRGQuery*>(query_exp);
    return select_database_records(*w_query, wib_db_path, WORKFILE);
}

/**
 * @brief Parse in a metadata schema file, enabling pre-population of interactive prompts when creating new records.
 * @param schema_definition string with path to schema definition file to read in
 */
const wibble::metadata_schema wibble::WibbleRGDatabase::parse_schema_file(const std::string& schema_definition) const
{
    // 1. read in file
    // 2. iterate / split fields
    // 3. assign + return struct
    metadata_schema ms = generate_blank_schema();
    
    try
    {
        const std::string DELIM = "|";
        std::stringstream ss(schema_definition);
        std::string line;
        WibbleConsole wc;
        while(std::getline(ss, line, '\n')) 
        {
            try
            {
                //std::cerr << "DEBUG: schema line: '" << line << "'" << std::endl;
                if(line[0] == '#') { continue; }
                std::string schema_key = line.substr(0, line.find(DELIM));
                std::string schema_val = line.substr(line.find(DELIM) + 1, std::string::npos);
                wc.trim(schema_key);
                wc.trim(schema_val);

                // these are all the user-editable fields; i.e. Date, NoteID, LinkedIds and DataDirs are automatically managed
                if     (schema_key == "TITLE")    { ms.title = schema_val; }
                else if(schema_key == "DESC")     { ms.desc = schema_val;  }
                else if(schema_key == "FILETYPE") { ms.type = schema_val;  }
                else if(schema_key == "PROJECT")  { ms.proj = schema_val;  }
                else if(schema_key == "TAGS")     { ms.tags = schema_val;  }
                else if(schema_key == "CLASS")    { ms.cls = schema_val;   }
                else if(schema_key == "KEYPAIRS") { ms.kp = schema_val;    }
                else if(schema_key == "CUSTOM")   { ms.cust = schema_val;  }
            }
            catch(std::out_of_range& ex)
            {
                std::cerr << "DEBUG: malformed schema line: '" << line << "'" << std::endl;
            }
            catch(std::exception& ex)
            {
                std::cerr << "DEBUG: general error: '" << ex.what() << "'" << std::endl;
            }
        }
        
        //std::cerr << "DEBUG: schema string: '" << schema_definition << "'" << std::endl; 
        return ms;
    }
    catch(const std::exception& err)
    {
        std::cerr << "Error parsing schema definition: " << err.what() << std::endl;
        return ms;
    }
}

/**
 * @brief Default constructor/return a fresh/blank schema.
 * @param schema_definition string with path to schema definition file to read in
 */
const wibble::metadata_schema wibble::WibbleRGDatabase::generate_blank_schema() const
{
    metadata_schema ms;
    ms.title = "";
    ms.desc = "";
    ms.type = "";
    ms.proj = "";
    ms.tags = "";
    ms.cls = "";
    ms.kp = "";
    ms.cust = "";
    return ms;
}

/**
 * @brief Interactively create a new record with set of guided prompts to user.
 * @param schema metadata_scheam struct, if non-blank, with values to pre-populate in (editable) fields
 * @param t_mplate_ft optional pre-set filetype to bypass interactive prompt (i.e. when using a template file) 
 */
wibble::WibbleRecord wibble::WibbleRGDatabase::create_new_record_interactive(const metadata_schema schema, const std::string& t_mplate_ft) const
{
    const int MAXTRIES = 5;
    int tries = 0;

    WibbleConsole wc;
    std::string title, type, desc, proj, tags, cls, kp, cust;

    wc.console_write_header("CREATE");
    do { title = wc.console_write_prompt("Title     *: ", "", schema.title); wc.trim(title); ++tries; }
    while (title == "" && tries < MAXTRIES);

    if(title == "")
    {
        wc.console_write_error_red("Maximum attempts reached. Title field cannot be blank.");
        throw wibble::wibble_exit(1);
    }

    tries = 0;

    // entirely bypass filetype prompt if using a schema with filetype set, or if using a template, which always has a defined filetype
    if(schema.type != "" && WibbleUtility::check_string_has_content(schema.type)) { type = schema.type;   }
    else if(t_mplate_ft != "") { type = t_mplate_ft; }
    else
    {
        do
        {
            type = wc.console_write_prompt("Filetype  *: ", "", schema.type);
            WibbleRecord::sanitise_input(type);
            wc.trim(type);
            ++tries;
        }
        while (type == "" && tries < MAXTRIES);
    }

    if(type == "")
    {
        wc.console_write_error_red("Maximum attempts reached. Filetype field cannot be blank.");
        throw wibble::wibble_exit(1);
    }

    desc = wc.console_write_prompt("Description: ", "", schema.desc);
    proj = wc.console_write_prompt("Project    : ", "", schema.proj);
    tags = wc.console_write_prompt("Tags       : ", "", schema.tags);
    cls  = wc.console_write_prompt("Class      : ", "", schema.cls); 
    cust = wc.console_write_prompt("Custom     : ", "", schema.cust);
    std::string kp_string = schema.kp;
    kp   = WibbleRecord::get_set_keypair_field(wc, kp_string); 
    wc.console_write_hz_heavy_divider();
    
    // remove any whitespace surrounding input
    wc.trim(desc);
    wc.trim(proj);
    wc.trim(tags);
    wc.trim(cls);
    wc.trim(kp);
    wc.trim(cust);

    // always default to 'General' for Project if not defined
    if(desc == "") { desc = "___NULL___"; }
    if(proj == "") { proj = "General";    }
    if(tags == "") { tags = "___NULL___"; }
    if(cls == "" ) { cls = "___NULL___";  }
    if(kp == ""  ) { kp = "___NULL___";   }
    if(cust == "") { cust = "___NULL___"; }

    // ensure filetype/project fields don't have abherrant characters in,
    // since these are used in construction of final output path of Node file
    WibbleRecord::sanitise_input(type);
    WibbleRecord::sanitise_input(proj);

    std::string gen_date = WibbleRecord::generate_date_rfc5322("");
    std::string gen_id = WibbleRecord::generate_UUIDv4();

    wc.console_write("\n");
    wc.console_write_header("CONFIRM");
    wc.console_write_yellow_bold("Date       : "); wc.console_write(gen_date); 
    wc.console_write_yellow_bold("Title      : "); wc.console_write(title); 
    wc.console_write_yellow_bold("NoteId     : "); wc.console_write(gen_id); 
    wc.console_write_yellow_bold("Filetype   : "); wc.console_write(type); 
    wc.console_write_yellow_bold("Description: "); wc.console_write(desc); 
    wc.console_write_yellow_bold("Project    : "); wc.console_write(proj); 
    wc.console_write_yellow_bold("Tags       : "); wc.console_write(tags); 
    wc.console_write_yellow_bold("Class      : "); wc.console_write(cls); 
    wc.console_write_yellow_bold("DataDirs   : "); wc.console_write("___NULL___"); 
    wc.console_write_yellow_bold("KeyPairs   : "); wc.console_write(kp); 
    wc.console_write_yellow_bold("Custom     : "); wc.console_write(cust); 
    wc.console_write_yellow_bold("LinkedIds  : "); wc.console_write("___NULL___"); 
    wc.console_write_hz_heavy_divider();
    
    std::cout << std::endl;
    tries = 0;
    std::string ans;
    bool valid_input = false;
    do
    {
        ++tries;
        wc.console_write_green_bold("Create record? (y/n) > ");
        std::getline(std::cin, ans);
        if(ans != "y" && ans != "Y" && ans != "n" && ans != "N")
        {
            valid_input = false;
            if(tries <= MAXTRIES)
            {
                wc.console_write_error_red("[" + std::to_string(tries) + "/" + std::to_string(MAXTRIES) +
                                       "]. Please 'y' to confirm or 'n' to cancel.");
            }
        }
        else { valid_input = true; }
    }
    while(tries <= MAXTRIES && valid_input == false);

    if(ans != "y" && ans != "Y")
    {
        wc.console_write_error_red("User aborted.");
        throw wibble::wibble_exit(1);
    }
    
    const bool bin_type = (bin_map.count(type) == 0) ? false : true; 
    // return the built record, throw away temporaries
    return WibbleRecord (std::move(gen_date),
                         std::move(title),
                         std::move(gen_id),
                         std::move(desc),
                         std::move(type),
                         std::move(proj),
                         std::move(tags),
                         std::move(cls),
                         std::move("___NULL___"),
                         std::move(kp),
                         std::move(cust),
                         std::move("___NULL___"),
                         bin_type);
}

/**
 * @brief Update existing Node/WibbleRecord record in-place/overwrite in memory.
 * @param w WibbleRecord object to update
 * @param da_f rvalue string with new value of 'Date' field
 * @param ti_f rvalue string with new value of 'Title' field
 * @param id_f rvalue string with new value of 'NoteId' field
 * @param de_f rvalue string with new value of 'Description' field
 * @param ty_f rvalue string with new value of 'Filetype' field
 * @param pr_f rvalue string with new value of 'Project' field
 * @param tg_f rvalue string with new value of 'Tags' field
 * @param cl_f rvalue string with new value of 'Class' field
 * @param dd_f rvalue string with new value of 'DataDirs' field
 * @param kp_f rvalue string with new value of 'KeyPairs' field
 * @param cu_f rvalue string with new value of 'Custom' field
 * @param li_f rvalue string with new value of 'LinkedIds' field
 */
bool wibble::WibbleRGDatabase::update_existing_record(WibbleRecord& w,
                            std::string&& da_f, std::string&& ti_f, std::string&& id_f,
                            std::string&& de_f, std::string&& ty_f, std::string&& pr_f,
                            std::string&& tg_f, std::string&& cl_f, std::string&& dd_f,
                            std::string&& kp_f, std::string&& cu_f, std::string&& li_f) const
{
    try
    {
        w.~WibbleRecord();

        const bool bin_type = (bin_map.count(ty_f) == 0) ? false : true; 
        new (&w) wibble::WibbleRecord(std::move(da_f),
                                      std::move(ti_f),
                                      std::move(id_f),
                                      std::move(de_f),
                                      std::move(ty_f),
                                      std::move(pr_f),
                                      std::move(tg_f),
                                      std::move(cl_f),
                                      std::move(dd_f),
                                      std::move(kp_f),
                                      std::move(cu_f),
                                      std::move(li_f),
                                      bin_type);
        return true;
    }
    catch(const std::bad_alloc& e)
    {
        std::cerr << "update_existing_record(): Problem updating object" << std::endl;
        return false;
    }
}

/**
 * @brief Wrapper function to call underlying method to eliminate a record from database file.
 * @param wib_db_path full path to database file
 * @param line_match line number corresponding to first line/start of record to eliminate
 */
bool wibble::WibbleRGDatabase::_perform_delete(const std::string& wib_db_path, long& line_match) const
{
    if(_validate_result(line_match))
    {
        //std::cerr << "Found line: " << line_match << " proceeding with delete!" << std::endl;
        return wibble::WibbleIO::delete_as_new_database_file(wib_db_path, line_match);
    }
    else
    {
        //std::cout << "No record found for update." << std::endl;
        return false;
    }
}

/**
 * @brief Overloaded public accessor function to call underlying method to eliminate a record from database file.
 * @param note_id unique identifer for WibbleRecord/Node to delete
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
bool wibble::WibbleRGDatabase::delete_record(const std::string& note_id, const std::string& wib_db_path, const std::string& WORKFILE) const
{
    long line_match = _get_line_number_for_record(note_id, wib_db_path, WORKFILE);
    return _perform_delete(wib_db_path, line_match);
}

/**
 * @brief Overloaded public accessor function to call underlying method to eliminate a record from database file.
 * @param w WibbleRecord object to update
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
bool wibble::WibbleRGDatabase::delete_record(const wibble::WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const
{
    long line_match = _get_line_number_for_record(record, wib_db_path, WORKFILE);
    return _perform_delete(wib_db_path, line_match);
}

/**
 * @brief Template substitution helper; get value to insert for placholder.
 * @param TOKEN string with token to match
 * @param w WibbleRecord object to update
 */
const std::string wibble::WibbleRGDatabase::get_placeholder_replacement(const std::string& TOKEN, const WibbleRecord& record) const
{
    if     (TOKEN == "___DATE___")        { return record.get_date();  }
    else if(TOKEN == "___NOTEID___")      { return record.get_id();    }
    else if(TOKEN == "___TITLE___")       { return record.get_title(); }
    else if(TOKEN == "___DESCRIPTION___") { return record.get_desc();  }
    else if(TOKEN == "___FILETYPE___")    { return record.get_type();  }
    else if(TOKEN == "___PROJECT___")     { return record.get_proj();  }
    else if(TOKEN == "___TAGS___")        { return record.get_tags();  }
    else if(TOKEN == "___CLASS___")       { return record.get_class(); }
    else if(TOKEN == "___CUSTOM___")      { return record.get_cust();  }
    // Unknown token, leave as-is
    else                                  { return TOKEN;              }
}

/**
 * @brief For a given template file, replace placeholders with Node/WibbleRecord specific metadata.
 * @param record WibbleRecord object to use for metadata retrieval
 * @param wibble_store string with path to configured root Wibble directory
 */
bool wibble::WibbleRGDatabase::perform_template_substitutions(const WibbleRecord& record, const std::string& wibble_store) const
{
    if(wibble_store == "")
    {
        //std::cerr << "DEBUG: Null wibble_store, Skipping template substitutions." <<  std::endl;
        return true;
    }

    const std::string input_filename = wibble_store + "/notes" + "/" + record.get_type() + "/" + record.get_proj() + "/" +
                                       record.get_id().substr(0,2) + "/" + record.get_id() + "." + record.get_type();
    std::string output_file = "";
    bool need_to_replace_file = false;

    try
    {
        std::stringstream ss(WibbleIO::read_file_into_stringstream(input_filename));
        std::string line;
        const std::string TOKEN_DELIM = "___";
        while(std::getline(ss, line, '\n')) 
        {
            std::string substituted_line = line;
            try
            {
                std::size_t start_index = 0;
                std::size_t end_index = 0;
                std::string token_to_replace = "";
                const std::size_t TOKEN_OFFSET = 3;
                std::size_t TOKEN_START;
                std::size_t TOKEN_END;
                std::size_t TOKEN_LENGTH;
                //std::string TOKEN_REPLACEMENT = "[M]";

                // inner loop needed for case where multiple replacement tokens need
                // greedily replacing on same line
                while(start_index != std::string::npos)
                {
                    start_index = line.find(TOKEN_DELIM, start_index);
                    // immediately move onto next line if we've got no matches/end-of-line
                    if(start_index == std::string::npos) { break; }

                    end_index = line.find(TOKEN_DELIM, start_index + 1);

                    TOKEN_START = start_index;
                    TOKEN_END = end_index + TOKEN_OFFSET;
                    TOKEN_LENGTH = TOKEN_END - TOKEN_START;

                    // extract the placeholder
                    std::string TOKEN = line.substr(TOKEN_START, TOKEN_LENGTH);
                    // clear/remove the placeholder
                    line.replace(TOKEN_START, TOKEN_LENGTH, "");
                    // now insert the replacement record value where the placeholder token started
                    line.insert(TOKEN_START, get_placeholder_replacement(TOKEN, record));
                    // beware WRAPAROUND of std::string::npos / size_t value
                    if(end_index == std::string::npos ||
                       end_index >= (std::string::npos - TOKEN_OFFSET))
                    { start_index = std::string::npos; }
                    else
                    { start_index = end_index + TOKEN_OFFSET; }
                    need_to_replace_file = true;
                }
            }
            catch(std::out_of_range &ex) { }

            // output the new file
            output_file.append(line + '\n');
        }

        std::stringstream().swap(ss);
    }
    catch(std::exception& ex)
    {
        std::cerr << "DEBUG: general error: '" << ex.what() << "'" << std::endl;
    }
    //std::cerr << "DEBUG: output file:\n" << output_file << "\n";

    // only apply a replacement file if some substitutions were performed...
    if(need_to_replace_file)
    {
        // args: file contents, filename/destination file
        if(wibble::WibbleIO::write_out_file_overwrite(output_file, input_filename)) { return true; }
        else { return false; }
    }

    return true;
}

/**
 * @brief Overload wrapper to persist (i.e. permanently save) a given Node/WibbleRecord to plain-text database file.
 * @param create_new flag indicating whether to simply append a new record or update an existing record
 * @param record WibbleRecord object to use for metadata retrieval
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 * @param wibble_store string with path to configured root Wibble directory
 */
bool wibble::WibbleRGDatabase::persist_record(bool create_new, const wibble::WibbleRecord& record, const std::string& wib_db_path,
                                              const std::string& WORKFILE, const std::string& wibble_store) const
{

    if(create_new)
    {
        perform_template_substitutions(record, wibble_store);
        return persist_new_record(record, wib_db_path);
    }
    else
    {
        if(WORKFILE == "")
        {
            std::cerr << "Empty temporary file specified! Aborting." << std::endl;
            return false;
        }
        return persist_existing_record(record, wib_db_path, WORKFILE);
    }
    return true;
}


/**
 * @brief Public accessor wrapper to export Nodes to new database file
 * @param record WibbleRecord object to use for metadata retrieval
 * @param wib_db_path full path to database file
 */
bool wibble::WibbleRGDatabase::export_record(const WibbleRecord& record, const std::string& wib_db_path) const
{ return persist_new_record(record, wib_db_path); }

/**
 * @brief Persist (i.e. permanently save) a given Node/WibbleRecord to plain-text database file.
 * @param record WibbleRecord object to use for metadata retrieval
 * @param wib_db_path full path to database file
 */
bool wibble::WibbleRGDatabase::persist_new_record(const WibbleRecord& record, const std::string& wib_db_path) const
{
    bool valid_db = true;
    if(! std::filesystem::exists(std::filesystem::path(wib_db_path))) 
    { 
        valid_db = WibbleIO::init_database_file(_rgdb_header(), wib_db_path); 
    }
    if(! valid_db) { return false; } 
    return WibbleIO::append_to_database_file(WibbleRecord::serialize_record(record), wib_db_path);
}

/**
 * @brief Helper function to validate that proposed deletion of a WibbleRecord/Node is viable.
 * @param line_match index line/flag value to verify
 */
bool wibble::WibbleRGDatabase::_validate_result(long line_match) const
{
    bool valid_line = false;
    switch(line_match)
    {
    case -1:
        std::cerr << "DELETION ERROR: No matches!" << std::endl;
        break;
    case -2:
        std::cerr << "DELETION ERROR: Multiple matches!" << std::endl;
        break;
    case -3:
        std::cerr << "DELETION ERROR: Bad conversion/parsing error!" << std::endl;
        break;
    default:
        if(line_match > 0) { valid_line = true;  }
        else               { valid_line = false; }
        break;
    }

    return valid_line;
}

/**
 * @brief Overloaded helper function to identify line number that a record starts at, given its unique NoteId value/hash.
 * @param note_id unique identifer for WibbleRecord/Node to obtain starting line for
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
long wibble::WibbleRGDatabase::_get_line_number_for_record(const std::string& note_id, const std::string& wib_db_path, const std::string& WORKFILE) const
{
    wibble::WibbleDBRGQuery w_query;
    w_query.id_q = note_id;
    return w_query.get_existing_record_line_num(wib_db_path, WORKFILE);
}

/**
 * @brief Overloaded helper function to identify line number that a record starts at, given its unique NoteId value/hash.
 * @param record WibbleRecord object to obtain starting line for
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
long wibble::WibbleRGDatabase::_get_line_number_for_record(const WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const
{
    wibble::WibbleDBRGQuery w_query;
    w_query.id_q = record.get_id();
    return w_query.get_existing_record_line_num(wib_db_path, WORKFILE);
}

/**
 * @brief Permanently update an existing WibbleRecord/Node.
 * @param record WibbleRecord object to obtain starting line for
 * @param wib_db_path full path to database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 */
bool wibble::WibbleRGDatabase::persist_existing_record(const wibble::WibbleRecord& record, const std::string& wib_db_path, const std::string& WORKFILE) const
{
    long line_match = _get_line_number_for_record(record, wib_db_path, WORKFILE);
    if(_validate_result(line_match))
    {
        //std::cerr << "Found line: " << line_match << " proceeding with update!" << std::endl;
        wibble::WibbleIO::update_as_new_database_file(wibble::WibbleRecord::serialize_record(record), wib_db_path, line_match);
    }
    else
    {
        std::cerr << "No record found for update." << std::endl;
    }
    return true;
}

/**
 * @brief Obtain number of record matches for given query.
 * @param w Pointer to vector of WibbleRecord's that match search criteria
 */
unsigned long wibble::WibbleRGDatabase::result_size(WibResultList& w)
{
    try { return w->size(); }
    catch(std::exception& ex)
    {
        std::cerr << "Error determining size of result set: " << ex.what() << std::endl;
        return 0;
    }
}

/**
 * @brief Obtain a selected record from result set by index.
 * @param w Pointer to vector of WibbleRecord's that match search criteria
 * @param s index with record to retrieve
 */
wibble::WibbleRecord wibble::WibbleRGDatabase::select_record(WibResultList& w, unsigned long s)
{
    try { return w->at(s); }
    catch(std::exception& ex)
    {
        std::cerr << "Error retrieving record: " << ex.what() << std::endl;
        return wibble::WibbleRecord();
    }
}

/**
 * @brief Migrate an existing Node/WibbleRecord to/from main archive to archived database.
 * @param record WibbleRecord object to use for metadata retrieval
 * @param wibble_store string with path to configured root Wibble directory
 * @param main_wib_db_path full path to main database file
 * @param ark_wib_db_path full path to archived database file
 * @param WORKFILE temporary workfile to use for ripgrep intermediate output/results
 * @param from_main_to_archive flag indicating direction of migration (main -> archived, or archived -> main)
 */
bool wibble::WibbleRGDatabase::migrate_record(const wibble::WibbleRecord& record, const std::string& wibble_store, const std::string& main_wib_db_path, const std::string& ark_wib_db_path, const std::string& WORKFILE, bool from_main_to_archive) const
{

    // ensure correct "direction": main -> archive, or archive -> main
    // 1. moving record from main to archive
    if(from_main_to_archive)
    {
        //std::cerr << "DEBUG: 1a. persisting record to archive database: " << ark_wib_db_path << std::endl;
        bool deleted = false;
        bool migrated = false;
        bool archived = persist_new_record(record, ark_wib_db_path);
        // only delete record and move note file if persist succeeded
        if(archived)
        {
            //std::cerr << "DEBUG: 2a. deleting record from main database: " << main_wib_db_path << std::endl;
            deleted = delete_record(record, main_wib_db_path, WORKFILE);
            if(deleted) { migrated = wibble::WibbleIO::migrate_note_file(true, wibble_store, record.get_id(), record.get_type(), record.get_proj()); }
        }

        return migrated;
    }
    else // 2. unarchiving from archive back to main
    {
        //std::cerr << "DEBUG: 1b. persisting record to main database: " << main_wib_db_path << std::endl;
        bool deleted = false;
        bool migrated = false;
        bool unarchived = persist_new_record(record, main_wib_db_path);
        // only delete record if persist succeeded
        if(unarchived)
        {
            //std::cerr << "DEBUG: 2b. deleting record from archive database: " << ark_wib_db_path << std::endl;
            deleted = delete_record(record, ark_wib_db_path, WORKFILE);
            if(deleted) { migrated = wibble::WibbleIO::migrate_note_file(false, wibble_store, record.get_id(), record.get_type(), record.get_proj()); }
        }
        
        return migrated;
    }
}
