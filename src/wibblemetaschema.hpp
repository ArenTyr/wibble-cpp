/**
 * @file      wibblemetaschema.hpp
 * @brief     Provides struct for "metadata schema".
 * @details
 *
 * A metadata schema allows pre-population of Node/note metadata fields,
 * editable, but acting as a prompt/suggestion to help aid conformity and
 * conistency when adding new Nodes/notes.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_META_SCHEMA_H
#define WIBBLE_META_SCHEMA_H

#include <string>

namespace wibble
{
    /**
     * @brief Struct providing strings to pre-populate each field via ReadLine prompt.
     */
    struct metadata_schema
    {
        std::string title; //!< 'Title' metadata field
        std::string desc;  //!< 'Description' metadata field
        std::string type;  //!< 'Filetype' metadata field
        std::string proj;  //!< 'Project' metadata field
        std::string tags;  //!< 'Tags' metadata field
        std::string cls;   //!< 'Class' metadata field
        std::string kp;    //!< 'KeyPairs' metadata field
        std::string cust;  //!< 'Custom' metadata field
    };
}
#endif
