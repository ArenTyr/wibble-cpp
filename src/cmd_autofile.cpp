// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_autofile.cpp
 * @brief     Dispatcher class for autofiling feature.
 * @details
 *
 * This class handles the command line switching for dispatching
 * the autofiling functionality/features within wibbleautofiler.cpp
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_autofile.hpp"
#include "wibbleautofiler.hpp"

/**
 * @brief Dispatching logic for the "autofile" feature/command
 * @param OPTS package/general configuration options struct
 * @param AUTOF autofile command line options/switches
 */
void wibble::WibbleCmdAutofile::handle_autofile(pkg_options& OPTS, autofiler_options& AUTOF)
{
    WibbleAutofiler wa{OPTS, OPTS.paths.autofile_dir, OPTS.paths.tmp_dir};

    // check whether filename filter, description filter, or a custom date override has been specified
    const std::string custom_date = (AUTOF.custom_date == "UNSET") ? "" : AUTOF.custom_date;
    const std::string filter_f    = (AUTOF.filter_fn == "UNSET") ? ""   : AUTOF.filter_fn;
    const std::string filter_d    = (AUTOF.filter_desc == "UNSET") ? "" : AUTOF.filter_desc;

    if     (AUTOF.add_category)         { wa.add_new_category();                                                                                                    }
    else if(AUTOF.edit_categories)      { wa.edit_categories();                                                                                                     }
    else if(AUTOF.copy_data != "UNSET") { wa.perform_autofile(AUTOF.copy_data, 1, AUTOF.dry_run, custom_date, AUTOF.no_prompt, AUTOF.set_category, AUTOF.set_desc); }
    else if(AUTOF.move_data != "UNSET") { wa.perform_autofile(AUTOF.move_data, 2, AUTOF.dry_run, custom_date, AUTOF.no_prompt, AUTOF.set_category, AUTOF.set_desc); }
    else if(AUTOF.open_file_action)     { wa.open_file_action(filter_f, filter_d);                                                                                  }
    else                                { wa.just_list(filter_f, filter_d);                                                                                         } 
    //else { wa.null_operation(); } // could show stats? 
}
