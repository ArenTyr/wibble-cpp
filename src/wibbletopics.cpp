/**
 * @file      wibbletopics.cpp
 * @brief     Main dispatcher/interface for the topic functionality.
 * @details
 *
 * This class generates a bash shell command which executes to
 * generate a compressed archive of all of the user's Wibble data.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iomanip>
#include <sstream>

#include "external/rang.hpp"

#include "wibbleexit.hpp"
#include "wibbletopics.hpp"
#include "wibbleutility.hpp"
#include "wibblesymbols.hpp"

/**
 * @brief Buld the primary result set/listing of Topic entries for a given Topic file.
 * @param input_file string with filename/path to Topic index to read in
 * @param rs result set structure to mutate in place
 * @param tot_entries index to mutate in place
 */
void wibble::WibbleTopics::_build_result_set(const std::string& input_file, topic_result_set& rs, unsigned long& tot_entries)
{
        std::stringstream input_ss = wibble::WibbleIO::read_file_into_stringstream(input_file);
        const std::string DELIM = "|";
        std::string line;
        std::string fn, title;
        tot_entries = 0;
        // iterate over file line by line, yielding mapping of index to filename, and list of titles in same order/index
        while(std::getline(input_ss, line, '\n'))
        {
            ++tot_entries;
            fn = line.substr(0, line.find(DELIM));
            title = line.substr(line.find(DELIM) + 1, std::string::npos);
            rs.entries[tot_entries] = fn;
            rs.entry_list.push_back(title);
        }

        //close up stringstream
        std::stringstream().swap(input_ss);
}


/**
 * @brief Non-interactive method to create an entirely fresh/new Topic definition.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 */
bool wibble::WibbleTopics::batch_construct_new_topic(WibbleConsole& wc, const std::string& topic_base_dir, std::string& name, std::string& category,
                                                     const std::string& title, std::string& type, std::string& desc)
{
    if(desc == "") { desc  = "No description entered.\n"; }

    name = name.substr(0, TP_FIELD_WIDTH);         wibble::WibbleRecord::sanitise_input(name);
    category = category.substr(0, TP_FIELD_WIDTH); wibble::WibbleRecord::sanitise_input(category);

    WibbleRecord::str_to_uppercase(name);
    WibbleRecord::str_to_lowercase(category);

    // ensure they're not trying to clobber an existing topic definition
    if(std::filesystem::exists(std::filesystem::path(topic_base_dir + "/topic_defs/" + category + "_" + name + ".wib")))
    {
        wc.console_write_error_red("A topic with name '" +  name + "' in category '" + category +
                                   "' already exists. Please input a different name/alias.", false);
        return false;
    }

    std::string id_stub = "_" + wibble::WibbleRecord::generate_timestamp("");
    std::string id = name + id_stub;

    const std::string topic_path     = topic_base_dir + "/store/" + category + "/" + id;
    const std::string topic_def_path = topic_base_dir + "/topic_defs";

    bool create_topic_path = WibbleIO::create_path(topic_path);
    bool create_topic_def_path = WibbleIO::create_path(topic_def_path);

    // write out the topic definition file; format is easily human readable
    if(create_topic_path && create_topic_def_path)
    {
        const std::string topic_output_file = _gen_topic_definition(name, id, title, category, type, desc);
        bool write_topic = WibbleIO::write_out_file_overwrite(topic_output_file, topic_def_path + "/" + category + "_" + name + ".wib");

        if(write_topic) { wc.console_write_success_green("New Topic with ID " + id + " created successfully.", false);     }
        else            { wc.console_write_error_red("Error writing Topic output file.", false);                           }

        return write_topic;
    }
    else
    {
        wc.console_write_error_red("Error creating Topic output path.", false);
        return false;
    }
}


/**
 * @brief Interactive method to create an entirely fresh/new Topic definition.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 */
bool wibble::WibbleTopics::create_new_topic(WibbleConsole& wc, const std::string& topic_base_dir)
{
    const int MAXTRIES = 3;
    wc.console_write_header("NEW TOPIC");
    std::string name, title, category, type, desc;
    int tries = 0;

    do
    {
        name = wc.console_write_prompt("Name/short alias (max chars = 30) *: ", "", ""); wc.trim(name); ++tries;
    }
    while (name == "" && tries < MAXTRIES);

    if(name == "")
    {
        wc.console_write_error_red("Maximum input attempts reached. Topic name is required. Aborting.");
        return false;
    }

    tries = 0;

    do { title = wc.console_write_prompt("Title                             *: ", "",  ""); wc.trim(title); ++tries;   }
    while (title == "" && tries < MAXTRIES);

    if(title == "")
    {
        wc.console_write_error_red("Maximum input attempts reached. Topic title is required. Aborting.");
        return false;
    }

    tries = 0;

    do { category = wc.console_write_prompt("Category         (max chars = 30) *: ", "", "general"); wc.trim(category); ++tries; }
    while (category == "" && tries < MAXTRIES);

    if(category == "")
    {
        wc.console_write_error_red("Maximum input attempts reached. Topic category is required. Aborting.");
        return false;
    }

    tries = 0;

    do { type = wc.console_write_prompt("Filetype                          *: ", "", ""); wc.trim(type); ++tries;   }
    while (type == "" && tries < MAXTRIES);

    if(type == "")
    {
        wc.console_write_error_red("Maximum input attempts reached. Topic filetype is required. Aborting.");
        return false;
    }

    tries = 0;
    do { desc = wc.console_write_prompt_ml("Description (multi-line, Ctrl-D)  *:\n", "", ""); wc.trim(desc); ++tries;   }
    while (desc == "" && tries < MAXTRIES);

    if(desc == "")
    {
        desc  = "No description entered.\n";
    }

    name = name.substr(0, TP_FIELD_WIDTH);         wibble::WibbleRecord::sanitise_input(name);
    category = category.substr(0, TP_FIELD_WIDTH); wibble::WibbleRecord::sanitise_input(category);

    // standardised opinionated topic format/presentation:
    // 1. The topic "name" is always in capitals
    // 2. The topic "category" is always lowercase
    // 3. Spaces are replaced with underscores
    // yielding, e.g notes:QUANTUM_PHYSICS_101, work:MEETINGS, work:AGENDA, work:PROJECT_X
    wibble::WibbleRecord::str_to_uppercase(name);
    wibble::WibbleRecord::str_to_lowercase(category);

    // ensure they're not trying to clobber an existing topic definition
    bool valid_input = false;
    do
    {
        if(std::filesystem::exists(std::filesystem::path(topic_base_dir + "/topic_defs/" + category + "_" + name + ".wib")))
        {
            wc.console_write_error_red("A topic with name '" +  name + "' in category '" + category + "' already exists. Please input a different name/alias.");
            name = "";
            --tries;
        }
        else { valid_input = true; break; }

        tries = 0;
        do
        {
            name = wc.console_write_prompt("Name/short alias (max chars = 30) *: ", "", ""); wc.trim(name); ++tries;
        }
        while (name == "" && tries < MAXTRIES);

        name = name.substr(0, TP_FIELD_WIDTH); name = wibble::WibbleRecord::sanitise_input(name);
        wibble::WibbleRecord::str_to_uppercase(name);
    }
    while(valid_input == false);

    std::string id_stub = "_" + wibble::WibbleRecord::generate_timestamp("");
    std::string id = name + id_stub;

    wc.console_write("\n");
    wc.console_write_header("TOPIC");
    wc.console_write_yellow_bold("Name       : "); wc.console_write(name);
    wc.console_write_yellow_bold("ID         : "); wc.console_write(id);
    wc.console_write_yellow_bold("Title      : "); wc.console_write(title);
    wc.console_write_yellow_bold("Category   : "); wc.console_write(category);
    wc.console_write_yellow_bold("Filetype   : "); wc.console_write(type);
    wc.console_write_yellow_bold("Description: "); wc.console_write("\n" + desc);
    wc.console_write_hz_divider();
    std::string confirm = wc.console_write_prompt("\nCreate this new topic? (y/n) > ", "", "");

    if(confirm != "y" && confirm != "Y")
    {
        wc.console_write_error_red("Topic creation aborted.");
        return false;
    }

    std::string topic_path     = topic_base_dir + "/store/" + category + "/" + id;
    std::string topic_def_path = topic_base_dir + "/topic_defs";

    bool create_topic_path = WibbleIO::create_path(topic_path);
    bool create_topic_def_path = WibbleIO::create_path(topic_def_path);

    // write out the topic definition file; format is easily human readable
    if(create_topic_path && create_topic_def_path)
    {

        const std::string topic_output_file = _gen_topic_definition(name, id, title, category, type, desc);
        bool write_topic = WibbleIO::write_out_file_overwrite(topic_output_file, topic_def_path + "/" + category + "_" + name + ".wib");

        if(write_topic) { wc.console_write_success_green("New topic created successfully."); }
        else            { wc.console_write_error_red("Error creating new topic."); }

        return write_topic;
    }
    else
    {
        return false;
    }
}

/**
 * @brief Wrapper function to run a batch command across a result set/multiple Topic entries.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param index_file Topic index file to parse to build initial mapping/Topic entry list
 * @param exec_cmd Shell command/command template to execute across final result set
 * @param txt_not_data flag indicating whether text or data to alter search path
 */
void wibble::WibbleTopics::execute_command_on_batch_set_build_trs(WibbleConsole& wc, const std::string& topic_base_dir, const topic_def& topic, const std::string& index_file, std::string& exec_cmd, const bool txt_not_data)
{
    topic_result_set trs;
    unsigned long tot_entries = 0;
    _build_result_set(index_file, trs, tot_entries);
    return execute_command_on_batch_set(wc, topic_base_dir, topic, exec_cmd, txt_not_data, trs);
}

/**
 * @brief Function to run a batch command across a result set/multiple Topic entries.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param exec_cmd Shell command/command template to execute across final result set
 * @param txt_not_data flag indicating whether text or data to alter search path
 * @param trs struct with all defined topic filenames of matches
 */
void wibble::WibbleTopics::execute_command_on_batch_set(WibbleConsole& wc, const std::string& topic_base_dir, const topic_def& topic, std::string& exec_cmd, const bool txt_not_data, const topic_result_set& trs)
{
    if(exec_cmd == "")
    {
        exec_cmd = wc.console_write_prompt("Input BATCH execution command/command template: ", "", "");
        wc.trim(exec_cmd);
    }

    if(exec_cmd != "")
    {
        std::string fn_list = "";
        for(const auto& [key, value]: trs.entries)
        {
            if(txt_not_data) { fn_list.append(topic_base_dir + "/store/"      + topic.category + "/" + topic.id + "/" + value + " "); }
            else             { fn_list.append(topic_base_dir + "/data_store/" + topic.category + "/" + topic.id + "/" + value + " "); }
        }
        wibble::WibbleUtility::build_batch_exec_cmd(exec_cmd, fn_list);
        wibble::WibbleUtility::prompt_confirm_run_batch_exec(wc, exec_cmd);
    }
    else { wc.console_write_error_red("Command cannot be empty. Aborting."); }
}

/**
 * @brief Generate a Topic descriptor/metadata file content
 * @param name String with name of topic
 * @param id String with id of topic
 * @param title String with title of topic
 * @param category String with category of topic
 * @param type String with type of topic
 * @param desc String with description of topic
 */
const std::string wibble::WibbleTopics::_gen_topic_definition(const std::string& name, const std::string& id, const std::string& title,
                                                const std::string& category, const std::string& type, const std::string& desc) const
{
        std::string topic_output_file = "# Wibble topic generated at " + wibble::WibbleRecord::generate_date_YMD("") + '\n';
        topic_output_file.append("NAME     | " + name + '\n');
        topic_output_file.append("TOPIC_ID | " + id + '\n');
        topic_output_file.append("TITLE    | " + title + '\n');
        topic_output_file.append("CATEGORY | " + category + '\n');
        topic_output_file.append("FILETYPE | " + type + '\n');
        topic_output_file.append("STATUS   | live\n");
        topic_output_file.append("DESC\n");
        topic_output_file.append("==========\n\n");
        topic_output_file.append(desc);

        return topic_output_file;
}



/**
 * @brief Validate integrity of topic definition.
 * @param topic struct containing Topic metadata/information for selected Topic
 */
bool wibble::WibbleTopics::_topic_def_integrity_check(topic_def& topic)
{
    if(topic.id       == "___NULL___") { return false; }
    if(topic.title    == "___NULL___") { return false; }
    if(topic.category == "___NULL___") { return false; }
    if(topic.type     == "___NULL___") { return false; }
    if(topic.desc     == "___NULL___") { return false; }

    return true;
}

/**
 * @brief Constructor to initialise a blank/fresh Topic definition.
 */
wibble::topic_def wibble::WibbleTopics::_generate_blank_topic_def()
{
    topic_def topic;
    topic.name     = "___NULL___";
    topic.id       = "___NULL___";
    topic.title    = "___NULL___";
    topic.category = "___NULL___";
    topic.type     = "___NULL___";
    topic.status   = "___NULL___";
    topic.desc     = "___NULL___";

    return topic;
}

/**
 * @brief Convenience function for ensuring vertically aligned display/columns.
 * @param tot_records Total count of records in order to determine number of digits/spacing required
 */
int wibble::WibbleTopics::_calc_padding_width(unsigned long tot_records) const
{
    return std::to_string(tot_records).length();
}

/**
 * @brief Generate an 'inventory', i.e. summary list of Topic entries (tet + data) for a given Topic.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 * @param topic struct containing Topic metadata/information for selected Topic
 */
bool wibble::WibbleTopics::inventory_topic(const WibbleConsole& wc, const std::string& topic_base_dir, const topic_def& topic)
{
    try
    {
        const std::string& topic_store = topic_base_dir + "/store/" + topic.category + "/" + topic.id;
        const std::string& topic_data = topic_base_dir + "/data_store/" + topic.category + "/" + topic.id;
        const std::string& topic_index = topic_base_dir + "/topic_indexes/" + topic.id + ".wib";
        const std::string& topic_data_index = topic_base_dir + "/topic_indexes/" + topic.id + ".data.wib";

        // determine which index files exist
        bool t_txt_index_exists =  std::filesystem::exists(std::filesystem::path(topic_index));
        bool t_dat_index_exists =  std::filesystem::exists(std::filesystem::path(topic_data_index));

        const std::string DELIM = "|";
        std::string line;
        std::string title;
        unsigned long tot_entries = 0;
        std::vector<std::string> entry_list;

        bool inventory_entries = false; bool inventory_data_entries = false;

        std::stringstream topic_inv;
        bool do_run = true;
        std::string storage_cmd = "";

        // wrapper function that allows us to potentially iterate over both files
        // if Topic has both text and data entries
        while(inventory_entries == false || inventory_data_entries == false)
        {

            if(! t_txt_index_exists) { inventory_entries = true; }
            if(inventory_entries == false)
            {
                topic_inv = wibble::WibbleIO::read_file_into_stringstream(topic_index);
                storage_cmd = "du -hs " + topic_store + " | cut -f 1";
                inventory_entries = true;
                wc.console_write_success_green("Listing topic entries for topic ID: " + topic.id);
                wc.console_write_header("ENTRIES");
            }
            else
            {
                if(t_dat_index_exists)
                {
                    topic_inv = wibble::WibbleIO::read_file_into_stringstream(topic_data_index);
                    storage_cmd = "du -hs " + topic_data + " | cut -f 1";
                    wc.console_write_success_green("Listing topic data entries for topic ID: " + topic.id);
                    wc.console_write_header("DATA ");
                }
                else
                {
                    do_run = false;
                }
                inventory_data_entries = true;
            }

            if(do_run)
            {
                while(std::getline(topic_inv, line, '\n'))
                {
                    ++tot_entries;
                    title = line.substr(line.find(DELIM) + 1, std::string::npos);
                    entry_list.push_back(title);
                }

                // close up stringstream
                std::stringstream().swap(topic_inv);
                unsigned long counter = 0;

                if(ENABLECOLOUR)
                {
                    for(const auto& entry: entry_list)
                    {
                        std::cout << rang::fgB::yellow << rang::style::bold << "[";
                        std::cout << std::setfill('0') << std::setw(_calc_padding_width(tot_entries));
                        std::cout << ++counter << "]" << rang::style::reset << " ┃ ";
                        std::cout << entry << '\n';
                    }
                }
                else
                {
                    for(const auto& entry: entry_list)
                    {
                        std::cout << "[" << std::setfill('0') << std::setw(_calc_padding_width(tot_entries));
                        std::cout << ++counter << "]" << " ┃ ";
                        std::cout << entry << '\n';
                    }
                }

                wc.console_write_hz_divider();
                std::cout << rang::fgB::yellow << rang::style::bold << "Storage used: " << rang::style::reset << std::flush;
                std::system(storage_cmd.c_str()); std::cout << '\n';
                counter = 0;
                entry_list.clear();
            }
        }

        return true;
    }
    catch(std::exception& ex)
    {
        std::cerr << "error in inventory_topic(): " << ex.what() << '\n';
        return false;
    }
}

/**
 * @brief Interactive function for selecting a Topic entry.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 * @param topic_index Topic index file to parse to build initial mapping/Topic entry list
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param stub_only flag to control whether function returns full filename path or simply path relative to Topic root directory
 */
std::string wibble::WibbleTopics::select_topic_entry_from_list(WibbleConsole& wc, const std::string& topic_base_dir, const std::string& topic_index, topic_def& topic, const bool stub_only)
{
    if(topic_index == "") { return ""; }
    try
    {
        topic_result_set trs;
        unsigned long tot_entries;

        // populate the topic_result_set struct
        _build_result_set(topic_index, trs, tot_entries);

        std::size_t counter = 0;
        std::size_t count = trs.entry_list.size();
        for(const auto& entry: trs.entry_list) { wc.console_write_padded_selector(++counter, count, entry + '\n'); }

        unsigned long selection = 1;
        if(tot_entries > 1)        { selection = wc.console_present_record_selector(tot_entries);                    }
        else if (tot_entries == 1) { wc.console_write_success_green("Singular candidate, automatically selecting."); }
        else                       { wc.console_write_error_red("Empty topic, add some entries!");                   }

        std::string topic_fn = "";
        if(trs.entries[selection] != "")
        {
            if(stub_only) { topic_fn = trs.entries[selection] + "|" + trs.entry_list[selection - 1]; }
            else          { topic_fn = topic_base_dir + "/store/" + topic.category + "/" + topic.id + "/" + trs.entries[selection]; }
        }
        else
        {
            wc.console_write_error_red("Invalid selection. Exiting.");
        }
        return topic_fn;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in select_topic_entry_from_list() " << ex.what() << std::endl;
        return "";
    }
}

/**
 * @brief Parse a given Topic definition file.
 * @param topic_ss stringstream pointing at Topic definition file
 */
wibble::topic_def wibble::WibbleTopics::_parse_topic_file(std::stringstream& topic_ss)
{
    topic_def topic = _generate_blank_topic_def();
    try
    {
        const std::string DELIM = "|";
        std::string line;
        WibbleConsole wc;
        bool desc_delim = false;
        bool chomp_desc = false;
        bool desc_fl = true;
        while(std::getline(topic_ss, line, '\n'))
        {
            try
            {
                if(line[0] == '#') { continue; }
                if(chomp_desc)
                {
                    if(desc_fl) { topic.desc = line + '\n'; desc_fl = false; }
                    else        { topic.desc.append(line + '\n');            }
                }
                else if(desc_delim)
                {
                    // description separator... description is multiline, anything to the end of the file
                    if(line == "==========")
                    {
                        chomp_desc = true;
                        continue;
                    }
                    else { continue; }
                }
                else
                {
                    //std::cerr << "DEBUG: topic line: '" << line << "'" << std::endl;
                    std::string topic_key = line.substr(0, line.find(DELIM));
                    std::string topic_val = line.substr(line.find(DELIM) + 1, std::string::npos);
                    wc.trim(topic_key);
                    wc.trim(topic_val);

                    // match key to value
                    if     (topic_key == "NAME")      { topic.name     = topic_val; }
                    else if(topic_key == "TOPIC_ID")  { topic.id       = topic_val; }
                    else if(topic_key == "TITLE")     { topic.title    = topic_val; }
                    else if(topic_key == "CATEGORY")  { topic.category = topic_val; }
                    else if(topic_key == "FILETYPE")  { topic.type     = topic_val; }
                    else if(topic_key == "STATUS")    { topic.status   = topic_val; }
                    else if(topic_key == "DESC")      { desc_delim     = true;      }
                }
            }
            catch(std::out_of_range& ex)
            {
                std::cerr << "DEBUG: malformed topic line: '" << line << "'" << std::endl;
            }
            catch(std::exception& ex)
            {
                std::cerr << "DEBUG: general error: '" << ex.what() << "'" << std::endl;
            }
        }

        return topic;
    }
    catch(const std::exception& err)
    {
        std::cerr << "Error parsing topic definition: " << err.what() << std::endl;
        return topic;
    }
}


/**
 * @brief Convenience wrapper for getting Topic stringstream from Topic file.
 * @param topic_file string with filename/path to topic file to read in
 */
std::stringstream wibble::WibbleTopics::_get_topic_stringstream_from_file(const std::string& topic_file)
{
    return wibble::WibbleIO::read_file_into_stringstream(topic_file);
}

/**
 * @brief Read in the set of Topic definition files to determine the available Topics.
 * @param wc general console utility object
 * @param topic_base_dir configured root Topic directory
 * @param get_hibernated flag to control whether to read in 'hibernated' (i.e.  hidden/retired) Topic definitions
 */
bool wibble::WibbleTopics::accumulate_topic_map(WibbleConsole& wc, const std::string& topic_base_dir, const bool get_hibernated)
{
    try
    {
        const std::string the_path = topic_base_dir + "/topic_defs";
        const std::filesystem::path the_topic_defs{the_path};
        //std::cerr << "Topic directory is: " << the_topic_defs << std::endl;
        wc.console_write_green(ARW + "Processing topic definitions under: " + the_path + '\n');
        if(std::filesystem::exists(std::filesystem::path(the_topic_defs)))
        {
            for (auto const& dir_entry : std::filesystem::directory_iterator{the_topic_defs})
            {
                if(dir_entry.is_regular_file())
                {
                    //std::cerr << "Parsing: " << dir_entry.path() << '\n';
                    std::stringstream top_ss = _get_topic_stringstream_from_file(dir_entry.path());
                    topic_def topic = _parse_topic_file(top_ss);
                    if(_topic_def_integrity_check(topic))
                    {
                        // do not add/show archived topics
                        if(topic.status == "live" && get_hibernated == false)
                        {
                            topic_desc.push_back(topic);
                            topic_map[topic.category + ":" + topic.name] = topic.id;
                            wc.console_write_green(ARW + "Found live topic: " + topic.category + ":" + topic.name + '\n');
                        }
                        else if(topic.status == "hibernated") // ...unless user has specifically requested to do so
                        {
                            wc.console_write_yellow(ARW + "Found hibernated topic: " + topic.category + ":" + topic.name + '\n');
                            hibernated_topics = true;
                            if(get_hibernated) // for reactivating a topic
                            {
                                topic_desc.push_back(topic);
                                topic_map[topic.category + ":" + topic.name] = topic.id;
                            }
                        }
                    }
                    else { std::cout << "Topic ERROR" << std::endl; }
                }
            }
        }
        else
        {
            wc.console_write_green(ARW + "No topic definitions found. Please create a topic.\n");
            return false;
        }

        return true;
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error processing topic: " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Display all defined Topics/extended metadata.
 * @param wc general console utility object
 */
void wibble::WibbleTopics::describe_topic_definitions(WibbleConsole& wc)
{
    for(const auto& entry: topic_desc)
    {
        print_topic_definition(wc, entry);
    }
}

/**
 * @brief Permanently remove an entire Topic and all associated files.
 * @param wc general console utility object
 * @param topic_base_dir path to root topic directory for environment
 */
void wibble::WibbleTopics::expunge_topic(WibbleConsole& wc, const std::string& topic_base_dir)
{
    const std::string topic_to_expunge = list_available_topics(wc, true);
    const std::string topic_id = topic_map[topic_to_expunge];

    const topic_def rm_topic = get_topic_for_name(topic_to_expunge);
    wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━ [  WARNING  ] ━━━━━━━━━━━━━━━━━━━━\n");
    wc.console_write_red_bold(" EXPUNGING THE TOPIC WILL REMOVE ALL ITS ENTRIES & DATA\n");
    wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n\n");
    wc.console_write_red_bold("The following Topic entries will ALL BE PERMANENTLY REMOVED:\n\n");
    inventory_topic(wc, topic_base_dir, rm_topic);

    const std::string topic_def_file       = topic_base_dir + "/topic_defs/" + rm_topic.category + "_" + rm_topic.name + ".wib";
    const std::string topic_index_txt      = topic_base_dir + "/topic_indexes/" + topic_id + ".wib";
    const std::string topic_index_data     = topic_base_dir + "/topic_indexes/" + topic_id + ".data.wib";
    const std::string topic_store_dir_txt  = topic_base_dir + "/store/" + rm_topic.category + "/" + topic_id;
    const std::string topic_store_dir_data = topic_base_dir + "/data_store/" + rm_topic.category + "/" + topic_id;

    bool del_def       = false;
    bool del_index_txt = false;
    bool del_index_dat = false;
    bool del_store_txt = false;
    bool del_store_dat = false;

    if(std::filesystem::exists(std::filesystem::path(topic_def_file)))       { del_def       = true; }
    if(std::filesystem::exists(std::filesystem::path(topic_index_txt)))      { del_index_txt = true; }
    if(std::filesystem::exists(std::filesystem::path(topic_index_data)))     { del_index_dat = true; }
    if(std::filesystem::exists(std::filesystem::path(topic_store_dir_txt)))  { del_store_txt = true; }
    if(std::filesystem::exists(std::filesystem::path(topic_store_dir_data))) { del_store_dat = true; }

    wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━ [  DELETION  ] ━━━━━━━━━━━━━━━━━━━\n\n");
    wc.console_write_red_bold("The following files will be permanently deleted:\n\n");
    if(del_def)       { wc.console_write_red(" " + topic_def_file + '\n');       }
    if(del_index_txt) { wc.console_write_red(" " + topic_index_txt + '\n');      }
    if(del_index_dat) { wc.console_write_red(" " + topic_index_data + '\n');     }
    wc.console_write_red_bold("\nThe following directories will be permanently deleted:\n\n");
    if(del_store_txt) { wc.console_write_red(" " + topic_store_dir_txt + '\n');  }
    if(del_store_dat) { wc.console_write_red(" " + topic_store_dir_data + '\n'); }

    wc.console_write_yellow_bold("\nTo proceed with deletion, input the full topic ID and press enter.\n\n");
    std::string resp = wc.console_write_prompt("Input '" + topic_id + "'\n> ", "", "");
    wc.trim(resp);

    if(resp != topic_id) { wc.console_write_error_red("Topic expunge operation cancelled."); }
    else // proceed, danger zone...
    {
        // first the single files
        if(del_def)       { std::filesystem::remove(topic_def_file);           }
        if(del_index_txt) { std::filesystem::remove(topic_index_txt);          }
        if(del_index_dat) { std::filesystem::remove(topic_index_data);         }
        // now the directories
        if(del_store_txt) { std::filesystem::remove_all(topic_store_dir_txt);  }
        if(del_store_dat) { std::filesystem::remove_all(topic_store_dir_data); }
        wc.console_write_success_green("Topic ID " + topic_id + " successfully expunged.");
    }
}


/**
 * @brief Display detailed metadata regarding a defined Topic.
 * @param wc general console utility object
 * @param topic struct containing Topic metadata/information for selected Topic
 */
void wibble::WibbleTopics::print_topic_definition(WibbleConsole& wc, const topic_def& topic)
{
    wc.console_write_header("TOPIC");
    wc.console_write_yellow_bold("Name       : "); wc.console_write(topic.name);
    wc.console_write_yellow_bold("Id         : "); wc.console_write(topic.id);
    wc.console_write_yellow_bold("Title      : "); wc.console_write(topic.title);
    wc.console_write_yellow_bold("Category   : "); wc.console_write(topic.category);
    wc.console_write_yellow_bold("Filetype   : "); wc.console_write(topic.type);
    wc.console_write_yellow_bold("Description:\n"); wc.console_write(topic.desc);
    wc.console_write_hz_divider();
}

/**
 * @brief Present list of all available Topics, with optional selector.
 * @param wc general console utility object
 * @param selector flag controlling whether to display select instead of simply listing only
 */
std::string wibble::WibbleTopics::list_available_topics(WibbleConsole& wc, bool selector)
{
    wc.console_write_green(ARW + "Listing available topics (default category = 'general'):\n\n");
    const int PADDING_WIDTH = 2 * TP_FIELD_WIDTH;
    int counter = 0;
    std::map<unsigned long, std::string> topic_selector;
    for(const auto& [key, value]: topic_map)
    {
        if(selector)
        {
            if(ENABLECOLOUR)
            {
                std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
                std::cout << std::setfill('0') << std::setw(_calc_padding_width(topic_map.size()));
                std::cout << ++counter << "]" << rang::style::reset << " ┃ ";
            }
            else
            {
                std::cout << "[" << std::setfill('0') << std::setw(_calc_padding_width(topic_map.size()));
                std::cout << ++counter << "]" << " ┃ ";
            }

            topic_selector[counter] = key;
        }
        wc.console_write_green_bold(key + std::string(PADDING_WIDTH - key.length(), ' '));  wc.console_write(" [" + value + "]");
        //std::cout << "Topic name: " << key << ". Topic id: " << value << '\n';
    }

    if(selector)
    {
        if(topic_map.size() == 0) { return ""; }
        unsigned long selection = wc.console_present_record_selector(topic_map.size());
        return topic_selector[selection];
    }
    else
    {
        return "";
    }
}

/**
 * @brief Obtain listing of defined topic names
 */
std::vector<std::string> wibble::WibbleTopics::obtain_topic_list()
{
    std::vector<std::string> topic_list;
    for(const auto& [key, value]: topic_map) { topic_list.push_back(key); }
    return topic_list;
}

/**
 * @brief Overloaded function for adding a "data" Topic entry (i.e. file attachment) to a defined Topic.
 * @param wc general console utility object
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param topic_base_dir configured root Topic directory
 * @param data_file string with filename/path to "data" file to add to Topic; can be any type of file, e.g. tar.xz, .mp4
 * @param move_only flag indicating whether file should be added via a copy or move operation
 * @param override_date optional string for YYYY-MM-DD ISO date to use custom rather than current date for entry
 */
bool wibble::WibbleTopics::add_topic_data_entry(WibbleConsole& wc, const topic_def& topic, const std::string& topic_base_dir,
                                                const std::string& data_file, const bool move_file, const std::string& override_date)
{
    return add_topic_data_entry(wc, topic, topic_base_dir, data_file, move_file, override_date, "", false, false);
}

/**
 * @brief Overloaded function for adding a "data" Topic entry (i.e. file attachment) to a defined Topic.
 * @param wc general console utility object
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param topic_base_dir configured root Topic directory
 * @param data_file string with filename/path to "data" file to add to Topic; can be any type of file, e.g. tar.xz, .mp4
 * @param move_only flag indicating whether file should be added via a copy or move operation
 * @param override_date optional string for YYYY-MM-DD ISO date to use custom rather than current date for entry
 * @param title string with descriptive title to use for data entry
 * @param trim_prefix flag indicating whether output filename should be truncated to avoid ever-growing expansion with repeated migration
 */
bool wibble::WibbleTopics::add_topic_data_entry(WibbleConsole& wc, const topic_def& topic, const std::string& topic_base_dir,
                                                const std::string& data_file, const bool move_file, const std::string& override_date,
                                                const std::string& title, const bool trim_prefix, const bool bypass_move_check)
{
    const int PREFIX_OFFSET = 24;
    std::string entry_title = title;
    if(entry_title == "") { entry_title = wc.console_write_prompt("Data entry title*: ", "",  ""); wc.trim(entry_title);              }

    if(entry_title == "") { wc.console_write_error_red("Data title cannot be blank. Aborting.", false); throw wibble::wibble_exit(1); }

    std::string data_fn = data_file;
    const std::string data_basename = std::filesystem::path(data_fn).filename();
    wibble::WibbleUtility::expand_tilde(data_fn);
    if(! std::filesystem::exists(std::filesystem::path(data_fn))) { wc.console_write_error_red("Non-existent file: " + data_fn + ". Aborting.", false); return false; }

    const std::string entry_time = wibble::WibbleRecord::generate_date_YMD(override_date);
    const std::string entry_year = wibble::WibbleRecord::generate_date_YYYY(override_date);
    const std::string timestamp = wibble::WibbleRecord::generate_timestamp(override_date);
    std::string output_fn;
    if(! trim_prefix)
    { output_fn = timestamp + "_" + wibble::WibbleRecord::generate_UUIDv4().substr(0,8) + "_" + data_basename; }
    else if(data_basename.length() > PREFIX_OFFSET) // safety, ensure existing filename is at least as long as the prefix...
    {
        // prevent filename continually expanding when migrating
        // a data item from topic to topic, by trimming old prefix - edge case;
        // repeated migration of a data topic entry is likely to be an infrequent occurrence
        output_fn = timestamp + "_" + wibble::WibbleRecord::generate_UUIDv4().substr(0,8) + "_" +
                  data_basename.substr(PREFIX_OFFSET, std::string::npos);
    }
    wibble::WibbleRecord::remove_whitespace(output_fn);

    // setup paths
    const std::string output_base_dir = topic_base_dir + "/data_store/" + topic.category + "/" + topic.id + "/" + entry_year;
    const std::string index_base_dir = topic_base_dir + "/topic_indexes";
    const std::string index_output_fn = index_base_dir + "/" + topic.id + ".data" + ".wib";
    try
    {
        WibbleIO wio;
        bool index_dir  = wio.create_path(index_base_dir);
        bool output_dir = wio.create_path(output_base_dir);
        if(index_dir && output_dir)
        {

            const std::string final_copy_path = output_base_dir + "/" + output_fn;
            if(move_file) { WibbleUtility::confirm_file_move_or_exit(wc, data_fn, final_copy_path, bypass_move_check); }
            bool copy_data_file = wio.wcopy_file(data_fn, final_copy_path);
            // constrain displayed filename as part of entry display for nice vertical consistency/alignment
            const std::string data_index_entry = entry_year + "/" + output_fn + "|[" + entry_time + "]"
                + " - (..." + output_fn.substr(output_fn.length() - 22, std::string::npos) + ")" + " | " + entry_title + '\n';

            if(copy_data_file)
            {
                wc.console_write_green("Data file added at: " + final_copy_path + '\n');
                if(move_file)
                {
                    std::filesystem::remove_all(data_file);
                    wc.console_write_success_green("Data file MOVED into topic.");
                }
                else
                {
                    wc.console_write_success_green("Data file COPIED into topic.");
                }

                bool write_index_entry = wio.write_out_file_append(data_index_entry, index_output_fn);

                if(write_index_entry)
                {
                    if(override_date != "") // added a custom date entry, re-sort entire index
                    {
                        wio.wcopy_file(index_output_fn, index_output_fn + ".bak");
                        bool sorted = sort_index_file(index_output_fn);
                        if(sorted) { wc.console_write_success_green("Custom date entry specified, index re-sorted successfully.");            }
                        else       { wc.console_write_error_red("An error occurred sorting the index for the new custom date entry.", false); }
                    }

                    wc.console_write_success_green("Topic data index entry added successfully.");
                    return true;
                }
                else
                {
                    wc.console_write_error_red("Error updating topic data index.", false);
                    return false;
                }
            }
            else
            {
                wc.console_write_error_red("Error copying data file to new destination filename.", false);
                return false;
            }
        }
        else
        {
            wc.console_write_error_red("Error constructing necessary output paths/directories.", false);
            return false;
        }
    }
    catch(std::filesystem::filesystem_error& fs)
    {
        std::cerr << "Error copying file: " << fs.what() << std::endl;
        return false;
    }
}

/**
 * @brief Check whether private flag for working with hibernated rather than active Topics has been set.
 */
bool wibble::WibbleTopics::get_hibernated_status() const
{
    return hibernated_topics;
}

/**
 * @brief Run ripgrep in vimgrep mode to determine matches/counts of given substring search term against content of Topic entries/files.
 * @param wc general console utility object
 * @param RG_WORKFILE string containing path/filename to temporary ripgrep result list after parsing Topic index list
 * @param topic_index Topic index file to parse to build initial mapping/Topic entry list
 */
wibble::WibbleTopics::rg_matches wibble::WibbleTopics::rg_vimgrep_matches(WibbleConsole& wc, const std::string& RG_WORKFILE, const std::string& topic_index)
{
    rg_matches rg;
    std::map<std::string, unsigned long> file_matches;
    std::stringstream rg_results = _get_topic_stringstream_from_file(RG_WORKFILE);
    std::string line;
    // build up a summary count of number of matches per filename, i.e 1-to-N; can present weighted/sorted
    // result list, ordered by frequency of matches/likely relevance to search term
    while(std::getline(rg_results, line, '\n'))
    {
        unsigned long spl_index = line.find(COLON_DELIM);
        if(spl_index != std::string::npos)
        {
            std::string fn = line.substr(0, spl_index);
            if(file_matches.count(fn) == 1)
            {
                file_matches[fn] = ++file_matches[fn];
            }
            else
            {
                file_matches[fn] = 1;
            }
        }
    }

    //close up stringstream
    std::stringstream().swap(rg_results);

    // compare ripgrep matches with the contents of the topic index
    std::stringstream topic_results = _get_topic_stringstream_from_file(topic_index);
    const std::string PIPE_DELIM = "|";

    unsigned long tot_entries = 0;

    while(std::getline(topic_results, line, '\n'))
    {
        unsigned long spl_index = line.find(PIPE_DELIM);
        std::string fn = line.substr(0, spl_index);
        std::string title = line.substr(spl_index + 1, std::string::npos);
        wc.trim(fn);
        wc.trim(title);

        // found a filename match/topic entry match
        if(file_matches.count(fn) == 1)
        {
            entry_match entry;
            entry.fn = fn;
            entry.title = title;
            entry.count = file_matches[fn];

            // additional compatibility structure is employed for batch operations on ripgrep results
            ++tot_entries;
            rg.rs.entries[tot_entries] = fn;
            rg.rs.entry_list.push_back(title);

            rg.result_list.push_back(entry);
        }
    }

    std::stringstream().swap(topic_results);
    return rg;
}

/**
 * @brief Obtain a mapping from the ripgrep result set back to a Topic entry filename.
 * @param wc general console utility object
 * @param topic_index Topic index file to parse to build initial mapping/Topic entry list
 * @param topic_entry_dir configured root Topic directory
 * @param RG_WORKFILE string containing path/filename to temporary ripgrep result list after parsing Topic index list
 */
std::string wibble::WibbleTopics::get_topic_fn_from_rg_vimgrep(WibbleConsole& wc, const std::string& topic_index,
                                                               const std::string& topic_entry_dir, const std::string RG_WORKFILE)
{

    try
    {
        // only care about the result_list component of rg_matches structure here
        std::vector<entry_match> result_list = rg_vimgrep_matches(wc, RG_WORKFILE, topic_index).result_list;

        unsigned long counter = 0;
        long selection = 0;
        if(ENABLECOLOUR)
        {
            // FIXME: could replace with padded selector code?
            for(const entry_match& entry: result_list)
            {
                std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
                std::cout << std::setfill('0') << std::setw(_calc_padding_width(result_list.size()));
                std::cout << ++counter << "]" << rang::style::reset << " ┃ Matches: ";
                std::cout << rang::bg::gray << rang::fgB::red << rang::style::bold << " " << entry.count << " " << rang::style::reset << " ┃ ";
                std::cout << entry.title << '\n';
            }
        }
        else
        {
            for(const entry_match& entry: result_list)
            {
                std::cout << "["; std::cout << std::setfill('0') << std::setw(_calc_padding_width(result_list.size()));
                std::cout << ++counter << "]" << " ┃ Matches: " << entry.count << " ┃ " << entry.title << '\n';
            }
        }

        long tot_entries = result_list.size();
        if(tot_entries > 1)        { selection = wc.console_present_record_selector(tot_entries); --selection; /* NB: vector 0 indexed */ }
        else if (tot_entries == 1) { wc.console_write_success_green("Singular candidate, automatically selecting.");                      }
        else                       { wc.console_write_error_red("No results. Exiting."); selection = -1;                                  }

        // can eliminate the temporary ripgrep file now
        if(std::filesystem::exists(std::filesystem::path(RG_WORKFILE))) { std::filesystem::remove(std::filesystem::path(RG_WORKFILE));    }

        if(selection < tot_entries && selection >= 0)
        { return topic_entry_dir + "/" + result_list[selection].fn; }
        else { return ""; }
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in get_topic_fn_from_rg_vimgrep(): " << ex.what() << std::endl;
        return "";
    }
}

/**
 * @brief Remove/delete a Topic entry.
 * @param wc general console utility object
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param topic_base_dir configured root Topic directory
 * @param entry_key string with unique entry hash/unambiguous line match
 * @param txt_entry flag controlling whether it is text or 'data' entry, for correct path generation
 */
bool wibble::WibbleTopics::remove_topic_entry(WibbleConsole& wc, const topic_def& topic, const std::string& topic_base_dir,
                                                  const std::string& entry_key, bool txt_entry)
{
    try
    {
        std::string index_fn;
        std::string entry_fn;

        if(txt_entry) { index_fn = topic_base_dir + "/topic_indexes/" + topic.id + ".wib";     }
        else          { index_fn = topic_base_dir + "/topic_indexes/" + topic.id + ".data.wib"; }
        if(txt_entry) { entry_fn = topic_base_dir + "/store/" + topic.category + "/" + topic.id + "/" + entry_key; }
        else          { entry_fn = topic_base_dir + "/data_store/" + topic.category + "/" + topic.id + "/" + entry_key; }

        wc.trim(index_fn);
        wc.trim(entry_fn);

        bool index_remove = WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, entry_key, index_fn);
        if(index_remove) { wc.console_write_success_green("Updated index written successfully."); }

        bool del_error = false;
        if(std::filesystem::exists(std::filesystem::path{entry_fn}))
        {
            bool remove_success = std::filesystem::remove(std::filesystem::path{entry_fn});
            if(remove_success) { wc.console_write_success_green("File " + entry_fn + " removed successfully."); }
            else               { del_error = true;                                                              }
        }
        else                   { del_error = true;                                                              }

        if(del_error)
        {
            wc.console_write_error_red("Unable to remove file: '" + entry_fn + "'.");
            wc.console_write_red(ARW + "Manual deletion required.\n");
        }

        return true;
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error removing entry from topic.");
        std::cerr << "Error deleting entry: " << ex.what() << std::endl;
        return false;
    }

}

/**
 * @brief Renames a Topic entry title in the index (text entry or data attachment)
 * @param wc general console utility object
 * @param cache_dir temporary working directory for index file update operation
 * @param topic_base_dir configured root Topic directory
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param txt_not_data flag indicating whether it is a text or data index to update
 */
bool wibble::WibbleTopics::rename_topic_entry(WibbleConsole& wc, const std::string& cache_dir, const std::string& topic_index_file, const std::string& topic_entry, const bool txt_not_data)
{
    try
    {
        auto sep_index = topic_entry.find("|");
        auto ts_index = topic_entry.find("]", sep_index + 1);
        auto data_sep_index = sep_index;
        if(! txt_not_data) { data_sep_index = topic_entry.find("|", sep_index + 1); }
        if(sep_index == std::string::npos || data_sep_index == std::string::npos || ts_index == std::string::npos )
        { throw std::invalid_argument("Malformed entry"); }

        const std::string search_key = topic_entry.substr(0, sep_index);
        std::string title = "";
        std::string prefix = "";

        // format of entries for text or data are slightly different
        if(txt_not_data)
        {
            prefix = topic_entry.substr(sep_index + 1, ts_index - sep_index) + " ";
            title = topic_entry.substr(ts_index + 2, topic_entry.length() - ts_index - 1);
        }
        else
        {
            prefix =  topic_entry.substr(sep_index + 1, data_sep_index - sep_index - 1) + "| ";
            title  = topic_entry.substr(data_sep_index + 2, topic_entry.length() - data_sep_index - 2);
        }
        const std::string new_title = wc.console_write_prompt("Alter title: ", "", title);

        const std::string new_index_line = search_key + "|" + prefix + new_title;
        const bool update_index = WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, search_key, topic_index_file, false, new_index_line);
        if(update_index) { wc.console_write_success_green("Title on entry successfully updated."); return true;  }
        else             { throw::std::invalid_argument("Bad update");                                           }
    }
    catch(std::exception& ex)
    {
        wc.console_write_error_red("Error updating title for entry in index.");
        return false;
    }
    return true;
}



/**
 * @brief Override for public wrapper/accessor to add a new Topic entry.
 * @param wc general console utility object
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param topic_base_dir configured root Topic directory
 * @param override_date optional string for YYYY-MM-DD ISO date to use custom rather than current date for entry
 */
std::string wibble::WibbleTopics::add_topic_entry(WibbleConsole& wc, topic_def& topic, const std::string& topic_base_dir,
                                                  const std::string& override_date)
{
    return add_topic_entry(wc, topic, topic_base_dir, override_date, "");
}

/**
 * @brief Public wrapper/accessor to add a new Topic entry.
 * @param wc general console utility object
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param topic_base_dir configured root Topic directory
 * @param override_date optional string for YYYY-MM-DD ISO date to use custom rather than current date for entry
 * @param title optional string for title, can yield non-interactive use when combined with custom date and --from-file
 */
std::string wibble::WibbleTopics::add_topic_entry(WibbleConsole& wc, topic_def& topic, const std::string& topic_base_dir,
                                                  const std::string& override_date, const std::string& title)
{
    std::string entry_title;
    if(title == "")
    {
        entry_title = wc.console_write_prompt("Entry title*: ", "",  ""); wc.trim(entry_title);
    }
    else { entry_title = title; }

    if(entry_title == "") { wc.console_write_error_red("Title cannot be blank. Aborting."); exit(1); }

    WibbleIO wio;

    // allow facility to specify explicit entry date (ISO YYYY-MM-DD format)
    const std::string entry_year = wibble::WibbleRecord::generate_date_YYYY(override_date);
    const std::string entry_time = wibble::WibbleRecord::generate_date_YMD(override_date);
    const std::string timestamp = wibble::WibbleRecord::generate_timestamp(override_date);
    const std::string rfc5322date = wibble::WibbleRecord::generate_date_rfc5322(override_date);

    const std::string uuid_stub = wibble::WibbleRecord::generate_UUIDv4().substr(0,8);
    const std::string topic_path = topic_base_dir + "/store/" + topic.category + "/" + topic.id + "/" + entry_year;

    topic.entry_title = entry_title;
    topic.entry_year = entry_year;

    bool create_topic_path = wio.create_path(topic_path);

    if(create_topic_path)
    {
        // store time/date information back into topic struct for use with template substitutions later
        topic.gen_YMD_date = entry_time;
        topic.gen_timestamp = timestamp;
        topic.gen_rfc_date = rfc5322date;
        topic.uuid_stub = uuid_stub;

        const std::string topic_output_filename  = timestamp + "_" + uuid_stub +
                                                         "_" + topic.name + "." + topic.type;
        const std::string topic_entry_filename = topic_path + "/" + topic_output_filename;

        topic.entry_fn = topic_entry_filename;

        const std::string bin_cmd = WibbleUtility::get_binary_tool(wc, topic.type, bin_mapping, false);
        bool write_topic_entry = false;
        if(bin_cmd != "") { write_topic_entry = WibbleIO::wcopy_file(templates_dir + "/bin_default/default." + topic.type, topic_entry_filename); }
        else              { write_topic_entry = wio.write_out_file_overwrite(rfc5322date, topic_entry_filename);                                  }
        if(write_topic_entry)
        {
            const std::string topic_index_path = topic_base_dir + "/topic_indexes";
            bool create_index_path = wio.create_path(topic_index_path);
            if(create_index_path)
            {
                const std::string index_entry = entry_year + "/" + topic_output_filename + "|[" + entry_time + "] " + entry_title + '\n';
                const std::string index_fn = topic_index_path + "/" + topic.id + ".wib";
                bool write_index_entry = wio.write_out_file_append(index_entry, index_fn);
                if(write_index_entry)
                {
                    if(override_date != "") // added a custom date entry, therefore re-sort entire index on disk
                    {
                        wio.wcopy_file(index_fn, index_fn + ".bak");
                        bool sorted = sort_index_file(index_fn);
                        if(sorted) { wc.console_write_success_green("Custom date entry specified, index re-sorted successfully."); }
                        else       { wc.console_write_success_green("An error occurred sorting the index for the new custom date entry."); }
                    }

                    wc.console_write_success_green("Topic entry added successfully.");
                    return topic_entry_filename;
                }
                else
                {
                    wc.console_write_error_red("Error. Unable to add entry to topic index: " +  index_fn);
                    return "";
                }
            }
            else
            {
                wc.console_write_error_red("Error. Unable to create output path for topic index: " +  topic_index_path);
                return "";
            }
        }
        else
        {
            wc.console_write_error_red("Error. Unable to add topic entry to file: " +  topic_output_filename);
            return "";
        }
    }
    else
    {
            wc.console_write_error_red("Error. Unable to create topic storage path: " +  topic_path);
            return "";
    }
}

/**
 * @brief Flip a Topic definition between active and hibernated status.
 * @param active flag indicating whether Topic is currently active (therefore hibernate), or hibernated (therefore re-activate)
 * @param topic_fn string with filename to Topic definition file
 */
bool wibble::WibbleTopics::flip_active_status(const bool active, const std::string& topic_fn)
{
    try
    {
        std::stringstream topic_def_ss = _get_topic_stringstream_from_file(topic_fn);
        std::ofstream new_def_file;
        new_def_file.open(topic_fn + ".new", std::ios::out | std::ios::trunc);
        std::string line;

        while(std::getline(topic_def_ss, line, '\n'))
        {
            if(line.find("STATUS   |") == std::string::npos) { new_def_file << line + '\n'; continue; }
            else
            {
                if(active) { new_def_file << "STATUS   | live\n"; }
                else       { new_def_file << "STATUS   | hibernated\n"; }
            }
        }

        std::stringstream().swap(topic_def_ss);
        new_def_file.close();

        return WibbleUtility::create_backup_and_overwrite_file(cache_dir, topic_fn + ".new", topic_fn);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error in flip_active_status(): " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Find/map an existing Topic to a given Topic name/key.
 * @param topic_name string with unique name/key for Topic definition to match
 */
wibble::topic_def wibble::WibbleTopics::get_topic_for_name(const std::string& topic_name)
{
    topic_def result = _generate_blank_topic_def();
    for(const topic_def& entry: topic_desc)
    {
        if((entry.category + ":" + entry.name) == topic_name)
        {
            result = entry;
            break;
        }
    }

    return result;
}

/**
 * @brief Replace templated placeholders with Topic entry values, for use with entries created using a template.
 * @param TOKEN string with placeholder/key to match
 * @param topic struct containing Topic metadata/information for selected Topic
 */
const std::string wibble::WibbleTopics::_get_topic_placeholder_replacement(const std::string& TOKEN, const topic_def& topic) const
{
    if     (TOKEN == "___YMD_DATE___")           { return topic.gen_YMD_date;  }
    else if(TOKEN == "___TIMESTAMP___")          { return topic.gen_timestamp; }
    else if(TOKEN == "___RFC_DATE___")           { return topic.gen_rfc_date;  }
    else if(TOKEN == "___ENTRY_TITLE___")        { return topic.entry_title;   }
    else if(TOKEN == "___ENTRY_YEAR___")         { return topic.entry_year;    }
    else if(TOKEN == "___ENTRY_FN___")           { return topic.entry_fn;      }
    else if(TOKEN == "___TOPIC_NAME___")         { return topic.name;          }
    else if(TOKEN == "___TOPIC_ID___")           { return topic.id;            }
    else if(TOKEN == "___TOPIC_TITLE___")        { return topic.title;         }
    else if(TOKEN == "___TOPIC_CATEGORY___")     { return topic.category;      }
    else if(TOKEN == "___TOPIC_TYPE___")         { return topic.type;          }
    else if(TOKEN == "___TOPIC_DESCRIPTION___")  { return topic.desc;          }
    else if(TOKEN == "___UUID_STUB___")          { return topic.uuid_stub;     }
    // Unknown token, leave as-is
    else                                         { return TOKEN;               }
}



/**
 * @brief Apply a set of substitutions/expansions in a new Topic entry created using a custom template.
 * @param topic struct containing Topic metadata/information for selected Topic
 * @param topic_fn string with filename of new Topic entry to do replacements within
 */
bool wibble::WibbleTopics::topic_template_substitutions(const std::string& topic_fn, const topic_def& topic)
{
    if(topic_fn == "" || topic.name == "")
    {
        //std::cerr << "DEBUG: Blank/invalid topic filename or topic definition." <<  std::endl;
        return false;
    }

    std::string output_file = "";
    bool need_to_replace_file = false;

    try
    {
        std::stringstream ss(WibbleIO::read_file_into_stringstream(topic_fn));
        std::string line;
        const std::string TOKEN_DELIM = "___";
        while(std::getline(ss, line, '\n'))
        {
            std::string substituted_line = line;
            try
            {
                std::size_t start_index = 0;
                std::size_t end_index = 0;
                std::string token_to_replace = "";
                const std::size_t TOKEN_OFFSET = 3;
                std::size_t TOKEN_START;
                std::size_t TOKEN_END;
                std::size_t TOKEN_LENGTH;

                // inner loop needed for case where multiple replacement tokens need
                // greedily replacing on same line
                while(start_index != std::string::npos)
                {
                    //std::cerr << "DEBUG: line is: " << line << std::endl;
                    //std::cerr << "DEBUG: start_index / end_index is: " << start_index << " / " << end_index << std::endl;
                    start_index = line.find(TOKEN_DELIM, start_index);
                    // immediately move onto next line if we've got no matches/end-of-line
                    if(start_index == std::string::npos) { break; }

                    end_index = line.find(TOKEN_DELIM, start_index + 1);

                    TOKEN_START = start_index;
                    TOKEN_END = end_index + TOKEN_OFFSET;
                    TOKEN_LENGTH = TOKEN_END - TOKEN_START;

                    // extract the placeholder
                    std::string TOKEN = line.substr(TOKEN_START, TOKEN_LENGTH);
                    // clear/remove the placeholder
                    line.replace(TOKEN_START, TOKEN_LENGTH, "");
                    // now insert the replacement record value where the placeholder token started
                    line.insert(TOKEN_START, _get_topic_placeholder_replacement(TOKEN, topic));
                    // beware WRAPAROUND of std::string::npos / size_t value
                    if(end_index == std::string::npos ||
                       end_index >= (std::string::npos - TOKEN_OFFSET))
                    { start_index = std::string::npos; }
                    else
                    { start_index = end_index + TOKEN_OFFSET; }
                    need_to_replace_file = true;
                }
            }
            catch(std::out_of_range &ex) { }

            // output the new file
            output_file.append(line + '\n');
        }

        std::stringstream().swap(ss);
    }
    catch(std::exception& ex)
    {
        std::cerr << "DEBUG: general error: '" << ex.what() << "'" << std::endl;
    }
    //std::cerr << "DEBUG: output file:\n" << output_file << "\n";

    // only apply a replacement file if some substitutions were performed...
    if(need_to_replace_file)
    {
        // args: file contents, filename/destination file
        if(wibble::WibbleIO::write_out_file_overwrite(output_file, topic_fn)) { return true; }
        else { return false; }
    }

    return true;
}

/**
 * @brief [Re-]sort the Topic index file; this is needed if an entry is added with a custom date
 * @param index_file string with filename to index file to update
 */
bool wibble::WibbleTopics::sort_index_file(const std::string& index_file)
{
    try
    {
        std::vector<std::string> index_lines;
        std::stringstream index_ss = _get_topic_stringstream_from_file(index_file);
        std::string line;
        while(std::getline(index_ss, line, '\n'))
        {
            index_lines.push_back(line);
        }

        //close up stringstream
        std::stringstream().swap(index_ss);

        // sort the entries -- numberical date stamps should guarantee ordering
        std::sort(index_lines.begin(), index_lines.end());

        std::ofstream new_index;
        new_index.open(index_file + ".new", std::ios::out | std::ios::trunc);
        for(auto& line: index_lines) { new_index << line + '\n'; }
        new_index.close();

        // overwrite with new sorted index and clean up
        return WibbleUtility::create_backup_and_overwrite_file(cache_dir, index_file + ".new", index_file);
    }
    catch(std::exception& ex)
    {
        std::cerr << "Error sorting index: " << ex.what() << std::endl;
        return false;
    }
}

/**
 * @brief Generate content of default initial template file; provide all fields for user to edit down as they wish.
 */
const std::string wibble::WibbleTopics::get_default_topic_template_str() const
{
    std::string dft = "Edit the contents of default template to suit!\n";
    dft.append("\n");
    dft.append("All possible field expansions/field placeholders are listed.\n");
    dft.append("\n");
    dft.append("Timestamps\n");
    dft.append("=================\n");
    dft.append("\n");
    dft.append("YMD date is: ___YMD_DATE___\n");
    dft.append("Timestamp is: ___TIMESTAMP___\n");
    dft.append("RFC date is: ___RFC_DATE___\n");
    dft.append("\n");
    dft.append("Entry information\n");
    dft.append("=================\n");
    dft.append("\n");
    dft.append("Entry title is: ___ENTRY_TITLE___\n");
    dft.append("Entry year is: ___ENTRY_YEAR___\n");
    dft.append("Entry filename is: ___ENTRY_FN___\n");
    dft.append("\n");
    dft.append("Topic information\n");
    dft.append("=================\n");
    dft.append("\n");
    dft.append("Topic name is: ___TOPIC_NAME___\n");
    dft.append("Topic ID is: ___TOPIC_ID___\n");
    dft.append("Topic title is: ___TOPIC_TITLE___\n");
    dft.append("Topic category is: ___TOPIC_CATEGORY___\n");
    dft.append("Topic type is: ___TOPIC_TYPE___\n");
    dft.append("Topic description is: ___TOPIC_DESCRIPTION___\n");

    return dft;
}
