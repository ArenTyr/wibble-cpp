/**
 * @file      main.cpp
 * @brief     Main entry point for Wibble++.
 * @details
 *
 * Execution entry point, with key signal handling/exception trapping,
 * typically during explicit custom wibble_exit() calls.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <signal.h>

#include "wibbledispatch.hpp"
#include "wibbleexit.hpp"

// compile-time helper for checking compile-time types
// https://stackoverflow.com/questions/17818009/how-can-i-determine-the-actual-type-of-an-auto-variable
template<class T>unsigned char identify(T&& v) {return v;}

// more for introspection/experiments rather than use
static void fpehandler(const int fpe)
{
    std::cerr << "FPE: " << fpe << std::endl;
    throw std::invalid_argument("Floating point exception");
}

/**
 * @brief Handle generated signals, but in particular user generated CTRL-C termination requests.
 * @param signum Integer representing signal type; mostly interested in/expect to receive '2'
 */
static void sighandler(const int signum)
{
    if(signum == 2)
    {
        std::cout << '\n' << '\n'; wibble::WibbleConsole().console_write_yellow_bold("Exit: ");
        std::cout << "User termination request received (CTRL-C). Quitting." << std::endl;
    }
    else
    {
        std::cout << "Fatal error. Aborting with signal number: " << signum << std::endl;
    }

    throw wibble::wibble_exit(1);
}

/**
 * @brief Classical entry point for execution.
 * @param argc Integer representing command line argument count
 * @param argv pointer to pointer, i.e. array of character strings (arrays) for each argument
 */
int main(int argc, char** argv)
{
    //feenableexcept(FE_ALL_EXCEPT);
   try
   {
       // Explicitly handle CTRL-C etc.
       signal(SIGABRT, &sighandler);
       signal(SIGTERM, &sighandler);
       signal(SIGINT,  &sighandler);
       signal(SIGFPE,  &fpehandler);

       // WibbleDispatch is where all of the command-line argument
       // logic and processing happens
       wibble::WibbleDispatch run;
       return run.dispatch_cli(argc, argv);
   }
   catch(const wibble::wibble_exit& we)
   {
       std::cout << "Wibble++ exiting.\n";
       if(we.value == 0) { return EXIT_SUCCESS; }
       else              { return EXIT_FAILURE; }
   }
   catch(const std::exception& err)
   {
       std::cout << "Wibble++ trapped an unexpected error: " << err.what() << std::endl;
       return(EXIT_FAILURE);
   }
}
