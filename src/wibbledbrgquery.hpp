/**
 * @file      wibbledbrgquery.hpp
 * @brief     Provides the searching/querying architecture for WibbleRecord/Nodes (header file).
 * @details
 *
 * This class encapsulates the generation of querying logic to
 * translate the user's input of field values into a search query
 * that will generate the correct result set (via ripgrep).
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_DBRGQUERY_H
#define WIBBLE_DBRGQUERY_H

#include "wibbledbqueryinterface.hpp"
#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleDBRGQuery : public WibbleDBQueryInterface
    {
    public:
        WibbleDBRGQuery()
        {
            date_q = "___NULL___";
            title_q = "___NULL___";
            id_q = "___NULL___";
            desc_q = "___NULL___";
            proj_q = "___NULL___";
            tags_q = "___NULL___";
            cls_q = "___NULL___";
            dd_q = "___NULL___";
            kp_q = "___NULL___";
            cust_q = "___NULL___";
            lids_q = "___NULL___";

            dump_all = false;
            exact_search = false;
            fixed_strings = false;
        };

        /**
         * @brief Core field mapping for search fields. 
         */
        struct WIB_FIELD_DEF
        {
            std::string date;   //!<  1. Searches 'Date:' field  
            std::string title;  //!<  2. Searches 'Title:' field  
            std::string noteid; //!<  3. Searches 'NoteId:' field  
            std::string desc;   //!<  4. Searches 'Description:' field  
            std::string type;   //!<  5. Searches 'Filetype:' field  
            std::string proj;   //!<  6. Searches 'Project:' field  
            std::string tags;   //!<  7. Searches 'Tags:' field  
            std::string cls;    //!<  8. Searches 'Class:' field  
            std::string dd;     //!<  9. Searches 'DataDirs:' field  
            std::string kp;     //!< 10. Searches 'KeyPairs:' field  
            std::string cust;   //!< 11. Searches 'Custom:' field  
            std::string lids;   //!< 12. Searches 'LinkedIds:' field
            WIB_FIELD_DEF(): date  {"Date: "       },
                             title {"Title: "      },
                             noteid{"NoteId: "     },
                             desc  {"Description: "},
                             type  {"Filetype: "   },
                             proj  {"Project: "    },
                             tags  {"Tags: "       },
                             cls   {"Class: "      },
                             dd    {"DataDirs: "   },
                             kp    {"KeyPairs: "   },
                             cust  {"Custom: "     },
                             lids  {"LinkedIds: "} {};
        };

        WibbleConsole wc;

        bool set_query_expression_for_field(SEARCH_FIELD field, std::string expression) override;
        std::string generate_query() const override;
        long get_existing_record_line_num(const std::string& wib_db_path, const std::string& WORKFILE) const override;
        bool get_all_flag() const { return dump_all; }
        void set_all_flag(bool dump_all_flag) { dump_all = dump_all_flag; }
        bool get_exact_flag() const { return exact_search; }
        void set_exact_flag(bool exact_search_flag) { exact_search = exact_search_flag; }
        bool get_fixed_flag() const { return fixed_strings; }
        void set_fixed_flag(bool fixed_strings_flag) { fixed_strings = fixed_strings_flag; }
    };

    static WibbleDBRGQuery::WIB_FIELD_DEF FIELD_DEF;
}
#endif
