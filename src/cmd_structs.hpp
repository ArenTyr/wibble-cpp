/**
 * @file      cmd_structs.hpp
 * @brief     Struct definitions for all subcommands (header file).
 * @details
 *
 * This file contains all of the structs that define all of the relevant
 * command line options for each subcommand.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CMD_STRUCTS_HPP
#define CMD_STRUCTS_HPP
#include <string>
#include <vector>

// Use include guard to avoid redefinition of these global structs

// === Global command line options ===
namespace wibble
{
    /**
     * @brief Structure representing configuration file settings/environment
     */
    typedef struct config_env
    {
        bool config_archived_db;     //!< flag indicating setting of location of archived database file *.db.wib
        bool config_main_db;         //!< flag indicating setting of location of main database file *.db.wib

        bool config_autofile_dir;    //!< flag indicating setting of autofile root directory
        bool config_backup_location; //!< flag indicating setting of backup root directory
        bool config_bookmarks_dir;   //!< flag indicating setting of bookmark root directory
        bool config_jot_dir;         //!< flag indicating setting of jotfile root directory
        bool config_schemas_dir;     //!< flag indicating setting of schemas root directory
        bool config_scripts_dir;     //!< flag indicating setting of scripts root directory
        bool config_tasks_dir;       //!< flag indicating setting of tasks root directory
        bool config_templates_dir;   //!< flag indicating setting of templates root directory
        bool config_topics_dir;      //!< flag indicating setting of topics root directory
        bool config_tmp_dir;         //!< flag indicating setting of cache/temporary directory
        bool config_wibble_db_dir;   //!< flag indicating setting of Wibble database directory
        bool config_wibble_store;    //!< flag indicating setting of main Wibble root directory

        bool config_cli_editor;      //!< flag indicating setting of main/default command line editor
        bool config_dump_tool;       //!< flag indicating setting of dump (cat) program
        bool config_fm_tool;         //!< flag indicating setting of file manager program
        bool config_gui_editor;      //!< flag indicating setting of GUI/secondary editor
        bool config_pager;           //!< flag indicating setting of paging tool (e.g. less, more, most)
        bool config_view_tool;       //!< flag indicating setting of view tool (e.g. gxmessage, batcat)
        bool config_xdg_open_cmd;    //!< flag indicating setting of xdg-open command (e.g. xdg-open)
        bool config_bin_mapping;     //!< flag indicating setting of binary file mapping configuration
        bool gen_bin_mapping;        //!< flag to trigger assistance method for setting up binary file mapping
    } config_env;

    /**
     * @brief Structure representing database file locations
     */
    typedef struct cmd_files
    {
        std::string archived_db;      //!< location of archived database file *.db.wib
        std::string main_db;          //!< location of main database file *.db.wib
        std::string live_config_file; //!< currently active configuration file 
    } cmd_files;

    /**
     * @brief Structure representing core Wibble storage paths
     */
    typedef struct cmd_paths
    {
        std::string autofile_dir;    //!< autofile root directory
        std::string backup_location; //!< backup root directory
        std::string bookmarks_dir;   //!< bookmark root directory
        std::string drawers_dir;     //!< drawers root directory
        std::string jot_dir;         //!< jotfile root directory
        std::string schemas_dir;     //!< schemas root directory
        std::string scripts_dir;     //!< scripts root directory
        std::string tasks_dir;       //!< tasks root directory
        std::string templates_dir;   //!< templates root directory
        std::string topics_dir;      //!< topics root directory
        std::string tmp_dir;         //!< cache/temporary directory
        std::string wibble_db_dir;   //!< Wibble database directory
        std::string wibble_store;    //!< main Wibble root/containing directory
    } cmd_paths;

    /**
     * @brief Structure representing user preferences for programs/executables
     */
    typedef struct cmd_exec
    {
        std::string cli_editor;   //!< main/default command line editor
        std::string dump_tool;    //!< dump (cat) program
        std::string fm_tool;      //!< file manager program
        std::string gui_editor;   //!< GUI/secondary editor
        std::string pager;        //!< paging tool (e.g. less, more, most)
        std::string view_tool;    //!< view tool (e.g. gxmessage, batcat)
        std::string xdg_open_cmd; //!< xdg-open command (e.g. xdg-open)
        std::string bin_mapping;  //!< mapping list for files of binary type
    } cmd_exec;

    /**
     * @brief Containing parent structure to aggregate files, paths, and programs
     */
    typedef struct pkg_options
    {
        struct cmd_files& files; //!< reference to struct pointing at core files
        struct cmd_paths& paths; //!< reference to struct pointing at core directories
        struct cmd_exec& exec;   //!< reference to struct pointing at core programs executable
    } pkg_options;

    /**
     * @brief Structure for autofile command line options/flags
     */
    typedef struct autofiler_options
    {
        std::string copy_data;    //!< autofile data by copying source file/directory
        std::string custom_date;  //!< autofile data with a custom date override
        std::string move_data;    //!< autofile data by moving source file/directory
        std::string filter_fn;    //!< filter displayed results matching filename substring
        std::string filter_desc;  //!< filter displayed results matching description substring
        std::string set_category; //!< pre-set/specify the autofile category
        std::string set_desc;     //!< pre-set/specify the autofile description
        bool add_category;        //!< add a new autofile category
        bool edit_categories;     //!< add a new autofile category
        bool dry_run;             //!< show proposed operation only
        bool open_file_action;    //!< select autofile item and show options menu
        bool no_prompt;           //!< non-interactive/no warning/automatically perform action
    } autofiler_options;

    /**
     * @brief Structure for bookmark command line options/flags
     */
    typedef struct bookmark_options
    {
        bool add_new_node_bm;            //!< directly add new node bookmark
        bool add_new_topic_bm;           //!< directly add new topic bookmark
        bool add_new_jotfile_bm;         //!< directly add new jotfile bookmark
        bool add_new_task_bm;            //!< directly add new task bookmark
        bool add_new_wibble_cmd_bm;      //!< directly add new Wibble command bookmark
        bool add_new_general_cmd_bm;     //!< directly add new general command bookmark

        bool select_all;                 //!< default action, show menu

        bool select_node_bm;             //!< directly select a node bookmark
        bool select_topic_bm;            //!< directly select a topic bookmark
        bool select_jotfile_bm;          //!< directly select a jotfile bookmark
        bool select_task_bm;             //!< directly select a task bookmark
        bool select_wibble_cmd_bm;       //!< directly select a Wibble command bookmark
        bool select_general_cmd_bm;      //!< directly select a general command bookmark

        bool delete_node_bm;             //!< directly delete a node bookmark
        bool delete_topic_bm;            //!< directly delete a topic bookmark
        bool delete_jotfile_bm;          //!< directly delete a jotfile bookmark
        bool delete_task_bm;             //!< directly delete a task bookmark
        bool delete_wibble_cmd_bm;       //!< directly delete a Wibble command bookmark
        bool delete_general_cmd_bm;      //!< directly delete a general command bookmark

        bool edit_node_bm;               //!< directly edit a node bookmark
        bool edit_topic_bm;              //!< directly edit a topic bookmark
        bool edit_jotfile_bm;            //!< directly edit a jotfile bookmark
        bool edit_task_bm;               //!< directly edit a task bookmark
        bool edit_wibble_cmd_bm;         //!< directly edit a Wibble command bookmark
        bool edit_general_cmd_bm;        //!< directly edit a general bookmark

        bool cust_exec_category_add_bm;  //!< add a new command bookmark to a custom category
        bool cust_exec_category_del_bm;  //!< delete an existing command bookmark from a custom category
        bool cust_exec_category_edit_bm; //!< edit an existing  command bookmark in a custom category
        bool cust_exec_category_sel_bm;  //!< select an existing command bookmar from a custom category

        bool act_cust_exec_category;     //!< activate a hibernated custom category
        bool add_cust_exec_category;     //!< add a new custom category
        bool hib_cust_exec_category;     //!< hibernate an existing active custom category
        bool ren_cust_exec_category;     //!< rename an existing custom category
        bool rm_cust_exec_category;      //!< remove an entire custom category

        int  cust_exec_bm_category;      //!< directly select custom category N
        int  num;                        //!< directly select bookmark entry N
        std::string exec_args;           //!< optionally supply arguments to selected bookmark command
        bool silence;                    //!< silence all output (useful for direct execution)
    } bookmark_options;

    /**
     * @brief Structure for containing search values/settings for Node metadata
     */
    typedef struct search_filters
    {
        // core metadata fields
        std::string date;          //!< 'Date'  metadata field
        std::string title;         //!< 'Title'  metadata field
        std::string id;            //!< 'NodeId'  metadata field
        std::string desc;          //!< 'Description'  metadata field
        std::string type;          //!< 'Type'  metadata field
        std::string proj;          //!< 'Project'  metadata field
        std::string tags;          //!< 'Tags'  metadata field
        std::string cls;           //!< 'Class'  metadata field
        std::string dd;            //!< 'DataDirs'  metadata field
        std::string kp;            //!< 'KeyPairs'  metadata field
        std::string cust;          //!< 'Custom'  metadata field
        std::string lids;          //!< 'LinkedIds'  metadata field
        std::string content_srch;  //!<  Full text content search

        // other search options
        bool archive_db;           //!< flag controlling whether to search archived database instead
        bool exact;                //!< flag controlling whether to perform exact case sensitive search
        bool extended_view;        //!< flag controlling whether to display extended record view/all fields
        bool everything;           //!< flag controlling whether to simply dump all records
        bool fixed_strings;        //!< flag controlling whether to run ripgrep in "fixed strings"/literal mode
        bool menu_search;          //!< flag controlling whether to perform interactive menu search
        bool single_only;          //!< flag indicating whether only a single match is acceptable
        // default constructor with sane initialisation values
        search_filters():
            date{""},
            title{""},
            id{""},
            desc{""},
            type{""},
            proj{""},
            tags{""},
            cls{""},
            dd{""},
            kp{""},
            cust{""},
            lids{""},
            content_srch{""},
            archive_db{false},
            exact{false},
            extended_view{false},
            everything{false},
            fixed_strings{false},
            menu_search{false},
            single_only{false} {};
    } search_filters;

    // === Subcommand options ===
    /**
     * @brief Structure containing "archive" command line options
     */
    typedef struct archive_options
    {
        bool restore_from_archive; //!< flag controlling whether we're restoring rather than archiving
    } archive_options;

    /**
     * @brief Structure containing "backup" command line options
     */
    typedef struct backup_options
    {
        bool list_backup;      //!< flag controlling whether to simply list existing backups
        bool full_backup;      //!< flag controlling whether to do full backup (i.e. entire data directory as well)
        bool xz_compression;   //!< flag indicating to use xz compression (default) 
        bool lz_compression;   //!< flag indicating to use tarlz compression
        bool gz_compression;   //!< flag indicating to use gz compression
        bool _7z_compression;  //!< flag indicating to use 7z compression
        bool zip_compression;  //!< flag indicating to use zip compression
    } backup_options;


    /**
     * @brief Structure containing "batch" command line options
     */
    typedef struct batch_options
    {
        bool create_node;                    //!< flag controlling whether to create a Node
        bool create_new_topic;               //!< flag controlling whether to setup a new Topic
        bool create_new_jotfile;             //!< flag controlling whether to setup a new Topic
        bool create_jotfile_entry;           //!< flag controlling whether to create a Topic entry
        bool create_task_item;               //!< flag controlling whether to create a Task item
        bool create_new_task_group;          //!< flag controlling whether to create a fresh Task group
        bool create_topic_entry;             //!< flag controlling whether to create a Topic entry
        bool node_add_data;                  //!< flag controlling whether to add a Node data attachment
        bool task_add_data;                  //!< flag controlling whether to add a Task item data attachment
        bool topic_add_data;                 //!< flag controlling whether to add a Topic data attachment
        bool data_copy;                      //!< flag controlling whether to add data via copy
        bool data_move;                      //!< flag controlling whether to add data via move

        // 1. Creating/finding Nodes
        bool node_archived;                  //!< boolean override if Node is in archived database (finding/adding data only)
        std::string node_id;                 //!< value for Node id (finding/adding data only) 
        std::string node_date;               //!< value for Node date (finding/adding data only) 
        std::string node_title;              //!< value for Node title
        std::string node_filetype;           //!< value for Node filetype
        std::string node_description;        //!< value for Node description
        std::string node_project;            //!< value for Node project
        std::string node_tags;               //!< value for Node tags
        std::string node_class;              //!< value for Node class
        std::string node_keypairs;           //!< value for Node keypairs
        std::string node_custom;             //!< value for Node custom

        // 2. Creating a Topic entry
        std::string topic_entry_title;       //!< value for Topic entry title
        std::string topic_entry_custom_date; //!< value for Topic entry custom date (optional)
        std::string use_topic;               //!< specify Topic to use

        // 3. Creating a Jotfile entry 
        std::string jot_entry_title;         //!< value for Jotfile entry title
        std::string use_jotfile;             //!< specify Jotfile to use

        // 4. Creating a new Topic 
        std::string topic_category;          //!< value for Topic category
        std::string topic_title;             //!< value for Topic title/name
        std::string topic_filetype;          //!< value for Topic filetype
        std::string topic_name;              //!< value for Topic name
        std::string topic_description;       //!< value for Topic description

        // 5. Creating a new Jotfile 
        std::string jot_category;            //!< value for Jotfile category
        std::string jot_name;                //!< value for Jotfile name

        // 6. Creating a new Task item
        std::string task_id;                 //!< value for Task ID (finding/adding data only)
        std::string task_title;              //!< value for Task title
        std::string task_category;           //!< value for Task category
        std::string task_due_date;           //!< value for Task due date
        int         task_priority;           //!< value for Task priority
        std::string task_group;              //!< value for Task group name

        std::string content_file;            //!< file that has the relevant Node/Topic entry/Jotfile entry content
        std::string data_file;               //!< path to file/directory to use for data operation
        std::string logfile;                 //!< optional logfile to store history of operations

    } batch_options;

    /**
     * @brief Structure containing "create" command line options
     */
    typedef struct create_options
    {
        std::string opt_from_file;     //!< string indicating to create Node from existing file content
        std::string opt_schema;        //!< string indicating to create Node with metadata stubbed from schema file
        std::string opt_t_mplate;      //!< string indicating to create Node from existing template file
 
        bool create_schema;            //!< flag controlling whether to create a new Node metadata schema file 
        bool create_template;          //!< flag controlling whether to create a new Node template file 
        bool create_default_template;  //!< flag controlling whether to create a filetype default template file 
        bool opt_list;                 //!< flag controlling whether to simply list defined templates and schemas
        bool use_gui_editor;           //!< flag controlling whether to use GUI editor upon opening new Node file
    } create_options;

    /**
     * @brief Structure containing "add" command line options
     */
    typedef struct daily_options
    {
        bool add;            //!< flag to add a new daily script file 
        bool edit;           //!< flag to edit an existing daily script file 
        bool ls;             //!< flag to list current existing daily script files 
        bool del;            //!< flag to delete an existing daily script file 
        std::string exec_ds; //!< string to indicate execution of an existing daily script file (or all)
    } daily_options;

    /**
     * @brief Structure for containing "data" command line options
     */
    typedef struct data_options
    {
        std::string add_by_copy;         //!< add data by copying file/directory
        std::string add_by_move;         //!< add data by moving file/directory
        std::string add_linked_file;     //!< add data by linking to a file/directory
        std::string open_dd_file_filter; //!< filter data directory files by substring match
        bool ls_dd;                      //!< list contents of data directory
        bool open_dd;                    //!< open data directory in file manager
        bool open_dd_file;               //!< open data directory file via xdg-open
        bool open_linked_file;           //!< open linked file via xdg-open
        bool tree_dd;                    //!< run 'tree' command on data directory
        bool cd_node;                    //!< flag to change shell directory into Node text/source directory
        bool cd_dd;                      //!< flag to change shell directory into Node data directory
    } data_options;

    /**
     * @brief Structure representing "drawer" command line options
     */
    typedef struct drawer_options
    {
        bool add_drawer;
        bool browse_drawer;
        bool cd_drawer;
        bool delete_drawer;
        std::string push_to_drawer;
        bool list_drawer;
        bool select_drawer;
        int num_drawer;
        bool force_mode;
        bool tree_mode;
        bool copy_to_node;
        bool copy_to_topic;
    } drawer_options;
    
    /**
     * @brief Structure for containing "edit" command line options
     */
    typedef struct edit_options
    {
        bool use_gui_editor; //!< flag controlling whether to open Wibble Node using GUI editor
    } edit_options;

    /**
     * @brief Structure for containing "exec" command line options
     */
    typedef struct exec_options
    {
        bool batch_exec;         //!< flag controlling whether to perform a batch operation on record set
        std::string exec_cmd;    //!< string containing execution/shell command to perform
        std::string exec_script; //!< string containing execution script to run
    } exec_options;

    /**
     * @brief Structure for containing "init" command line options
     */
    typedef struct init_options
    {
        std::string custom_init_file; //!< string with custom filename/path to write out to
    } init_options;

    /**
     * @brief Structure for containing "janitor" command line options
     */
    typedef struct janitor_options
    {
        std::vector<std::string> scrub;  //!< list of any filesl to remove/move file into device trash directory
        bool obliterate;                 //!< flag to permanently remove all trashed files
        bool restore;                    //!< flag to restore given file/directory into current directory
        bool list;                       //!< flag to list all currently trashed files
        bool usage;                      //!< flag to provide guidance as to best usage
        bool config;                     //!< flag to enable setup/configuration mode
        bool boring;                     //!< flag to enable hiding of trash ascii artwork
    } janitor_options;


    /**
     * @brief Structure for containing "jot" command line options
     */
    typedef struct jot_options
    {
        bool create_jotfile;            //!< flag to create a new jotfile
        bool delete_jotfile;            //!< flag to delete an existing jotfile
        bool rename_jotfile;            //!< flag to rename an existing jotfile
        bool add_to_jotfile;            //!< flag to add an entry to a jotfile
        bool dump_jotfile;              //!< flag to dump entire jotfile to console
        long jf_num;                    //!< directly select jotfile entry N
        long del_jf_entry;              //!< delete/directly delete a jotfile entry
        long edit_jf_entry;             //!< edit/directly edit a jotfile entry
        long view_jf_entry;             //!< view/directly view a jotfile entry
        bool raw_edit;                  //!< flag to directly edit raw jotfile
        std::string batch;              //!< string with shell command to run on jotfile entries
        std::string filter_jot_entries; //!< string to substring filter jotfile entries by title
        std::string grep_jotfile;       //!< string to filter/grep jotfile content to match entries
    } jot_options;

    /**
     * @brief Structure for containing "meta" command line options
     */
    typedef struct metadata_options
    {
        bool update_mt;       //!< flag to update existing metadata values
        bool print_mt;        //!< flag to display existing metadata values

        bool get_mt_date;     //!< flag to get specific metadata field: date 
        bool get_mt_title;    //!< flag to get specific metadata field: title
        bool get_mt_id;       //!< flag to get specific metadata field: node ID

        bool get_mt_desc;     //!< flag to get specific metadata field: description 
        bool get_mt_ft;       //!< flag to get specific metadata field: filetype
        bool get_mt_proj;     //!< flag to get specific metadata field: project

        bool get_mt_tags;     //!< flag to get specific metadata field: tags
        bool get_mt_cls;      //!< flag to get specific metadata field: class
        bool get_mt_dd;       //!< flag to get specific metadata field: data directory

        bool get_mt_kp;       //!< flag to get specific metadata field: keypairs
        bool get_mt_cust;     //!< flag to get specific metadata field: custom
        bool get_mt_lids;     //!< flag to get specific metadata field: linked IDs

        bool rel_as_parent;   //!< flag to define relationship as parent
        bool rel_as_sibling;  //!< flag to define relationship as sibling
        bool rel_as_child;    //!< flag to define relationship as child
        bool rel_interactive; //!< flag to define relationship as child
        bool rel_delete;      //!< flag to expurge a relationship
        bool rel_delete_all;  //!< flag to expurge all relationships for Node
        bool show_rels;       //!< flag to display relationships (expanded) 
        bool show_rels_c;     //!< flag to display relationships (condensed) 
        bool cd_node;         //!< flag to change shell directory into Node text/source directory
        bool cd_dd;           //!< flag to change shell directory into Node data directory
        bool path_node;       //!< flag to output path to Node text/source file
        bool path_dd;         //!< flag to output path to Node data directory
    } metadata_options;

    /**
     * @brief Structure representing "sync" command line options
     */
    typedef struct sync_options
    {
        std::string source_dir;  //!< set synchronisation source directory HELLO
        std::string dest_dir;    //!< set synchronisation destination directory
        bool detailed;           //!< show details/text input within WIBBLE_DESC
        bool tree_dir;           //!< show detailed listing of file status in source_dir
        bool status;             //!< display synchronisation proposals, no execute
        bool synchronise;        //!< synchronisation directories, interactive options
        int action;              //!< automatic bypass/selector for non-interactive use

    } sync_options;

    /**
     * @brief Structure representing "task" command line options
     */
    typedef struct task_options
    {
        std::string add_task;          //!< flag to trigger adding a new task
        std::string mark_task;         //!< flag to trigger marking/completion of existing task 
        bool closed_task_groups;       //!< flag to trigger display of closed task groups 
        bool dump_group_names;         //!< flag to trigger display of task group names
        std::string list_tasks;        //!< string to trigger display of all current open tasks of from a particular category
        bool list_completed;           //!< flag to trigger display of all completed tasks 
        bool show_everything;          //!< flag to trigger display of everything; all open tasks, all closed tasks
        bool show_history;             //!< flag to trigger display of task operations/completion history
        std::string task_title_filter; //!< string to filter tasks by a given title substring
        bool view_tasks_for_group;     //!< flag to trigger display of tasks by particular task group 
    } task_options;

    /**
     * @brief Structure representing "topic" command line options
     */
    typedef struct topic_options
    {
        bool create_topic;                            //!< flag to control creation of a new Topic 
        bool list_topics;                             //!< flag to control listing of existing topics (default action) 
        bool describe_topics;                         //!< flag to control detailed description of an existing Topic 
        bool hibernate_topic;                         //!< flag to control hibernation of an existing Topic 
        bool reactivate_topic;                        //!< flag to control reactivation of a hibernated Topic 
        bool expunge_topic;                           //!< flag to control permanent removal of entire Topic
 
        std::string add_entry_to_topic;               //!< string/flag to trigger adding a new Topic entry 
        std::string add_from_file;                    //!< string/flag to trigger adding a new Topic entry using existing file content 
        std::string add_with_template;                //!< string/flag to trigger adding a new Topic entry using the topic local template

        std::string configure_existing_topic;         //!< string/flag to edit an existing defined Topic 
        std::string copy_data_file;                   //!< string/flag to add a data file to a Topic via copying
        std::string create_edit_default_template;     //!< string/flag to edit/create a default Topic local template 

        std::string delete_data_file;                 //!< string/flag to delete an existing data file from a Topic
        std::string delete_entry_from_topic;          //!< string/flag to delete an existing Topic entry 
        std::string dump_topic_entry;                 //!< string/flag to dump an existing Topic entry to console (non-data) 

        std::string edit_topic_entry;                 //!< string/flag to edit an existing Topic entry 
        bool batch_exec;                              //!< flag to indicate whether a shell command is to be run en masse as a batch operation
        std::string exec_cmd_on_topic_entries;        //!< string indicating shell command to execute across topic entries
        std::string exec_script_cmd_on_topic_entries; //!< string indicating script command to run across topic entries/files

        std::string exec_topic_for_cmd;               //!< string to indicate Topic to use for shell command 
        std::string exec_topic_for_script_cmd;        //!< string to indicate Topic to use for script command 
        std::string filter_topic_entry;               //!< string to filter Topic entry titles

        std::string grep_topic_entry;                 //!< string to filter Topic entries by ripgrep across their content
        std::string gui_edit_topic_entry;             //!< string/flag to indicate use of GUI editor instead of terminal editor
        std::string inventory_topic;                  //!< string/flag to indicate summary display/inventory of all Topic entries for a given Topic 

        std::string move_data_file;                   //!< string/flag to add a data file to a Topic via moving
        std::string open_data_file;                   //!< string/flag to open an existing data file within a given Topic

        std::string override_date;                    //!< string to indicate use of a custom date[-time] (rather than now) when adding a new Topic entry
        std::string override_data_date;               //!< string to indicate use of a custom date[-time] (rather than now) when adding a new Topic data entry
        std::string set_data_topic;                   //!< string to explicitly specify Topic for data operation, non-interactive

        std::string specify_title;                    //!< string to specify Topic entry title and bypass prompt
        std::string view_topic_entry;                 //!< string/flag to view a given Topic entry (non-data)
        std::string migrate_topic;                    //!< string/flag to migrate a given Topic entry from one Topic to another
 
        std::string migrate_data_topic;               //!< string/flag to migrate a given Topic data entry from one Topic to another
        std::string cd_data_dir;                      //!< string/flag to trigger cd into Topic directory after using cd helper alias
        std::string alter_ent_title;                  //!< string/flag to adjust the title for a Topic entry
        std::string alter_dat_title;                  //!< string/flag to adjust the title for a Topic data entry

        bool render;                            //!< flag to pass Topic entry to render script
    } topic_options;

    /**
     * @brief Structure representing "transfer" command line options
     */
    typedef struct transfer_options
    {
        bool create_bundle;
        std::string dl_bundle;
        std::string dl_bundle_sha256;
        bool import_bundle;
        bool export_bin_map;
        bool export_nodes;
        bool export_task_group;
        bool export_topic;
        std::string dir_path;
    } transfer_options;

    /**
     * @brief Structure representing "view" command line options
     */
    typedef struct view_options
    {
        bool render;
    } view_options;

}
#endif
