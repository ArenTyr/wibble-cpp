/**
 * @file      wibbledispatch.hpp
 * @brief     Main CLI routing/dispatching class (header file).
 * @details
 *
 * This class provides all of the fundamental command-line argument/
 * subcommand processing/dispatching, and serves as the effective
 * entry point to all of the Wibble functionality.
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEDISPATCH_H
#define WIBBLEDISPATCH_H

#include "external/CLI11.hpp"

#include "cmd_structs.hpp"
#include "wibbleconsole.hpp"


namespace wibble
{
    class WibbleDispatch
    {
    private:
        void _configure_generic_search_switches(CLI::App& cmd);
        bool _config_create_dir_if_needed(WibbleConsole& wc, const std::string& fs_path);
        void _expand_all_settings(pkg_options& PKG);
        void _gen_bin_conf(WibbleConsole& wc);
        bool _prepare_directories(WibbleConsole& wc, cmd_paths& CMD_PATHS);
        void setup_archive_options(CLI::App& archive);
        void setup_autofile_options(CLI::App& autofile);
        void setup_batch_options(CLI::App& batch);
        void setup_backup_options(CLI::App& backup);
        void setup_bookmark_options(CLI::App& bookmark);
        void setup_config_options(CLI::App& config);
        void setup_create_options(CLI::App& create);
        void setup_daily_options(CLI::App& daily);
        void setup_data_options(CLI::App& data);
        void setup_delete_options(CLI::App& dlte);
        void setup_drawer_options(CLI::App& drawer);
        void setup_dump_options(CLI::App& dump);
        void setup_edit_options(CLI::App& edit);
        void setup_exec_options(CLI::App& exec);
        void setup_init_options(CLI::App& init);
        void setup_janitor_options(CLI::App& janitor);
        void setup_jot_options(CLI::App& jot);
        void setup_meta_options(CLI::App& meta);
        void setup_sync_options(CLI::App& sync);
        void setup_task_options(CLI::App& tasks);
        void setup_topic_options(CLI::App& topics);
        void setup_transfer_options(CLI::App& transfer);
        void setup_view_options(CLI::App& view);
        void show_config_env(WibbleConsole& wc);
        void handle_cmd(CLI::App& cmd);
        void debug_config();
        bool validate_configuration();

    public:
        int dispatch_cli(int argc, char** argv);
    };
}
#endif
