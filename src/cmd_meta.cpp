// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_meta.cpp
 * @brief     Handle "meta" subcommand.
 * @details
 *
 * Either display or edit/update metadata for a given Wibble Node.
 * Individual keys can be output or entire set of metadata
 * simply displayed.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_meta.hpp"
#include "wibbleexit.hpp"
#include "wibblerelationship.hpp"
#include "wibblesymbols.hpp"
#include "wibbleutility.hpp"


int wibble::WibbleCmdMetadata::_check_arg_count(metadata_options& META)
{
    int args = 0;
    if(META.print_mt)        { ++args; }
    if(META.update_mt)       { ++args; }
    if(META.get_mt_date  ||
       META.get_mt_title ||
       META.get_mt_id    ||
       META.get_mt_desc  ||
       META.get_mt_ft    ||
       META.get_mt_proj  ||
       META.get_mt_tags  ||
       META.get_mt_cls   ||
       META.get_mt_dd    ||
       META.get_mt_kp    ||
       META.get_mt_lids)     { ++args; }
    if(META.rel_as_parent)   { ++args; }
    if(META.rel_as_sibling)  { ++args; }
    if(META.rel_as_child)    { ++args; }
    if(META.rel_interactive) { ++args; }
    if(META.show_rels)       { ++args; }
    if(META.show_rels_c)     { ++args; }
    if(META.cd_node)         { ++args; }
    if(META.cd_dd)           { ++args; }

    return args;
}


/**
 * @brief Update the metadata for an existing Wibble Node record.
 * @param OPTS Package/general configuration options struct
 * @param print Reference to WibbleConsole utility object
 * @param w The selected WibbleRecord (Node) to operate upon
 * @param wib_e Reference to WibbleExecutor execution wrapper object 
 * @param archived_db Flag to indicate whether we're working with an 'archived' Node
 */
void wibble::WibbleCmdMetadata::_update_record(pkg_options& OPTS, WibbleConsole& print, WibbleRecord& w, WibbleExecutor& wib_e, const bool archived_db)
{
    const int MAXTRIES = 3;
    int tries = 0;
    // metadata fields are prefilled with existing values, allowing user to easily edit them to suit
    std::string pf_desc, pf_tags, pf_cls, pf_kp, pf_cust;
    std::string title, type, proj;
    bool type_changed = false;
    bool proj_changed = false;

    // needed due to side-effect of performance enhancement of sync_with_stdio(false) when dumping all records
    std::cout << std::unitbuf;
    print.console_write_header("INPUT");
    
    // title, filetype and project are never allowed to be ___NULL___, so we do not need to check
    if(w.get_desc()  == "___NULL___")  { pf_desc = ""; } else { pf_desc = w.get_desc(); }
    if(w.get_tags()  == "___NULL___")  { pf_tags = ""; } else { pf_tags = w.get_tags(); }
    if(w.get_class() == "___NULL___")  { pf_cls  = ""; } else { pf_cls = w.get_class(); }
    if(w.get_kp()    == "___NULL___")  { pf_kp   = ""; } else { pf_kp = w.get_kp();     }
    if(w.get_cust()  == "___NULL___")  { pf_cust = ""; } else { pf_cust = w.get_cust(); }

    do { title = print.console_write_prompt("Title      : ", "", w.get_title()); print.trim(title); ++tries; } // 2
    while (title == "" && tries < MAXTRIES);

    if(title == "")
    {
        print.console_write_error_red("Maximum attempts reached. Title field cannot be blank.");
        throw wibble::wibble_exit(1);
    }   
    tries = 0;
  
    std::string desc   = print.console_write_prompt("Description: ", "", pf_desc);       print.trim(desc);  // 4  

    do { type = print.console_write_prompt("Type       : ", "", w.get_type());  print.trim(type); ++tries; } // 5  
    while (type == "" && tries < MAXTRIES);

    if(type == "")
    {
        print.console_write_error_red("Maximum attempts reached. Filetype field cannot be blank.");
        throw wibble::wibble_exit(1);
    }
    tries = 0;

    do { proj   = print.console_write_prompt("Proj       : ", "", w.get_proj());  print.trim(proj); ++tries; } // 6  
    while (proj == "" && tries < MAXTRIES);
    if(proj == "")
    {
        print.console_write_error_red("Maximum attempts reached. Project field cannot be blank.");
        throw wibble::wibble_exit(1);
    }
    tries = 0;

    std::string tags   = print.console_write_prompt("Tags       : ", "", pf_tags);       print.trim(tags);  // 7  
    std::string cls    = print.console_write_prompt("Class      : ", "", pf_cls);        print.trim(cls);   // 8  
    std::string cust   = print.console_write_prompt("Custom     : ", "", pf_cust);       print.trim(cust);  //11  
    std::string kp     = WibbleRecord::get_set_keypair_field(print, pf_kp); // 10

    print.console_write_hz_heavy_divider();
    print.console_write("\n");
    print.console_write_hz_divider();

    // these fields are not user-editable by design
    //std::string date   = print.console_write_prompt("Date       : ", "", w.get_date()); print.trim(date);  // 1  
    //std::string id     = print.console_write_prompt("NoteId     : ", "", w.get_date()); print.trim(id);    // 3  
    //std::string dd     = print.console_write_prompt("DataDirs   : ", "", w.get_date()); print.trim(dd);    // 9  
    //std::string lids   = print.console_write_prompt("LinkedIds  : ", "", w.get_date()); print.trim(lids);  //12  
    //if(date == ""){ date  = w.get_date();  } // 1   
    //if(id == "")  { date = w.get_id();     } // 3
    //if(dd == "")  { date = w.get_ddd();    } // 9
    //if(lids == ""){ date = w.get_lids();   } // 12

    // if they haven't input a new value, reset it to to the existing value
    if(desc == "")  { desc  = w.get_desc();  } // 4
    if(type == "")  { type  = w.get_type();  } // 5  
    if(tags == "")  { tags  = w.get_tags();  } // 7  
    if(cls == "")   { cls   = w.get_class(); } // 8  
    if(kp == "")    { kp    = w.get_kp();    } // 10  
    if(cust == "")  { cust  = w.get_cust();  } // 11  

    // change in filetype/filename extension will necessitate moving/renaming underlying file; warn
    if(type != w.get_type())
    {
        print.console_write_red_bold("Warning. "); print.console_write_yellow("Filetype ");
        print.console_write_red("has changed from '" + w.get_type() + "' to '" + type + "'.\n");
        print.console_write_red("Be aware this will cause the underlying file to be renamed/moved.\n");
        print.console_write_green("Database will be updated accordingly.\n");
        print.console_write_hz_divider();
        type_changed = true;
    }

    // change in 'Project' will necessitate moving file to new path in underlying file tree; warn
    if(proj != w.get_proj())
    {
        print.console_write_red_bold("Warning. "); print.console_write_yellow("Project ");
        print.console_write_red("has changed from '" + w.get_proj() + "' to '" + proj + "'.\n");
        print.console_write_red("Be aware this will cause the underlying file location to be renamed/moved.\n");
        print.console_write_green("Database will be updated accordingly.\n");
        print.console_write_hz_divider();
        proj_changed = true;
    }

    print.console_write("\n");

    if(ENABLECOLOUR)
    {
        print.console_write_header("UPDATE");
        print.console_write_yellow_bold("Date       │ "); print.console_write(w.get_date());
        print.console_write_yellow_bold("Note ID    │ "); print.console_write(w.get_id());
        print.console_write_yellow_bold("Title      │ "); print.console_write(title);
        print.console_write_yellow_bold("Filetype   │ "); print.console_write(type);
        print.console_write_yellow_bold("Description│ "); print.console_write(desc);
        print.console_write_yellow_bold("Project    │ "); print.console_write(proj);
        print.console_write_yellow_bold("Tags       │ "); print.console_write(tags);
        print.console_write_yellow_bold("Class      │ "); print.console_write(cls);
        print.console_write_yellow_bold("DataDirs   │ "); print.console_write(w.get_dd());
        print.console_write_yellow_bold("KeyPairs   │ "); print.console_write(kp);
        print.console_write_yellow_bold("Custom     │ "); print.console_write(cust);
        print.console_write_yellow_bold("LinkedIds  │ "); print.console_write(w.get_lids());
        print.console_write_hz_heavy_divider();

    }
    else
    {
        std::cout << "━━━━━━━━━━━━━━━━━━━━━ [ UPDATE ] ━━━━━━━━━━━━━━━━━━━━━━" << '\n';
        std::cout << "Date       │ " << w.get_date() << "\n";
        std::cout << "Note ID    │ " << w.get_id()   << "\n";
        std::cout << "Title      │ " << title        << "\n";
        std::cout << "Filetype   │ " << type         << "\n";
        std::cout << "Description│ " << desc         << "\n";
        std::cout << "Project    │ " << proj         << "\n";
        std::cout << "Tags       │ " << tags         << "\n";
        std::cout << "Class      │ " << cls          << "\n";
        std::cout << "DataDirs   │ " << w.get_dd()   << "\n";
        std::cout << "KeyPairs   │ " << kp           << "\n";
        std::cout << "Custom     │ " << cust         << "\n";
        std::cout << "LinkedIds  │ " << w.get_lids() << "\n";
        std::cout << "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━" << '\n';

    }

    std::string proceed = print.console_write_prompt("Proceed with update? (y/n) > ", "", "");
    print.trim(proceed);   
    if(proceed == "")
    {
        print.console_write_red(ARW + "Please input a response to confirm.\n");
        //give them one more chance in case they fat fingered Enter twice on last input
        proceed = print.console_write_prompt("Proceed with update? (y/n) > ", "", "");
    }

    if(proceed != "y" && proceed != "Y") { print.console_write_error_red("User cancelled update"); }
    else
    {
        std::string date = w.get_date();
        std::string id   = w.get_id();
        std::string dd   = w.get_dd();
        std::string lids = w.get_lids();
        // do update
        WibbleRecord wr_new;
        wr_new.set_date(std::move(date));
        wr_new.set_title(std::move(title));
        wr_new.set_id(std::move(id));
        wr_new.set_desc(std::move(desc));
        wr_new.set_type(std::move(type));
        wr_new.set_proj(std::move(proj));
        wr_new.set_tags(std::move(tags));
        wr_new.set_class(std::move(cls));
        wr_new.set_dd(std::move(dd));
        wr_new.set_kp(std::move(kp));
        wr_new.set_cust(std::move(cust));
        wr_new.set_lids(std::move(lids));

        bool update_success = wib_e.op_update_existing_record(wr_new, archived_db);
        if(update_success)
        {
            print.console_write_success_green("Record metadata updated successfully.");

            if(type_changed || proj_changed) // a file rename/move is required
            {
                std::string old_fn = wib_e._build_note_path(w.get_id(), w.get_type(), w.get_proj(), archived_db, true);
                std::string new_fn = wib_e._build_note_path(wr_new.get_id(), wr_new.get_type(), wr_new.get_proj(), archived_db, true);

                // ensure directories exist
               bool new_path = wibble::WibbleIO::create_note_path(OPTS.paths.wibble_store, wr_new.get_id(),
                                                                  wr_new.get_type(), wr_new.get_proj(), true, archived_db);
               if(new_path)
               {
                   bool note_move = wibble::WibbleIO::wmove_file(old_fn, new_fn);
                   if(note_move)
                   {
                       print.console_write_yellow(ARW + "Old filename: " + old_fn + "\n");
                       print.console_write_green(ARW + "New filename: " + new_fn + "\n");

                       // housekeeping, get rid of empty directories after note migration
                       std::string note_containing_dir = wibble::WibbleIO::get_parent_dir_path(old_fn);
                       //print.console_write_green(ARW + "Parent path is: " + note_containing_dir + "\n");
                       bool remove_empty_parent = wibble::WibbleIO::remove_dir_if_empty(note_containing_dir);
                       std::string parent_container_path = wibble::WibbleIO::get_parent_dir_path(note_containing_dir);
                       if(remove_empty_parent)
                       {
                           print.console_write_green(ARW + "Empty parent path removed: " + note_containing_dir + "\n");
                           remove_empty_parent = wibble::WibbleIO::remove_dir_if_empty(parent_container_path);
                           if(remove_empty_parent)
                           { print.console_write_green(ARW + "Empty project directory removed: " + parent_container_path + "\n"); }
                       }
                       print.console_write_success_green("Note file successfully moved/renamed.");
                   }
                   else { print.console_write_error_red("Error moving note file."); }
               }
               else { print.console_write_error_red("Error creating new note directory."); }
            } 
        }
        else { print.console_write_error_red("Error updating record metadata."); }
    }
}

/**
 * @brief Output a given metadata field to console (no newline), useful for scripting purposes.
 * @param w The selected WibbleRecord (Node) to operate upon
 * @param META struct containing "meta" command line options
 */
void wibble::WibbleCmdMetadata::_print_field(WibbleRecord& w, metadata_options& META)
{
    // user can request multiple fields as they wish
    if(META.get_mt_date)  { std::cout << w.get_date() << '\n';  } 
    if(META.get_mt_title) { std::cout << w.get_title() << '\n'; }
    if(META.get_mt_id)    { std::cout << w.get_id() << '\n';    }
    if(META.get_mt_desc)  { std::cout << w.get_desc() << '\n';  }
    if(META.get_mt_ft)    { std::cout << w.get_type() << '\n';  }
    if(META.get_mt_proj)  { std::cout << w.get_proj() << '\n';  }
    if(META.get_mt_tags)  { std::cout << w.get_tags() << '\n';  }
    if(META.get_mt_cls)   { std::cout << w.get_class() << '\n'; }
    if(META.get_mt_dd)    { std::cout << w.get_dd() << '\n';    }
    if(META.get_mt_kp)    { std::cout << w.get_kp() << '\n';    }
    if(META.get_mt_cust)  { std::cout << w.get_cust() << '\n';  }
    if(META.get_mt_lids)  { std::cout << w.get_lids() << '\n';  }
}


/**
 * @brief Dispatching logic for the "meta" operation on Wibble nodes.
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 * @param META command line options/parameters supplied via switches for "meta"
 */
void wibble::WibbleCmdMetadata::handle_metadata(pkg_options& OPTS, search_filters& SEARCH, metadata_options& META)
{
    WibbleConsole wc; WibbleExecutor wib_e(OPTS); WibbleRecord w;
    if(SEARCH.single_only) { wc.silence_output(); }
    wc.console_write_header("METADATA"); 
    const int arg_count = _check_arg_count(META);
    if(arg_count > 1)
    {
        wc.console_write_error_red("Multiple conflicting options selected.");
        wc.console_write_yellow_bold("\nPlease only select one out of:\n\n");
        wc.console_write_yellow("'--update-metadata'\n");
        wc.console_write_yellow("'--display-metadata'\n");
        wc.console_write_yellow("'--rel-as-parent'\n");
        wc.console_write_yellow("'--rel-as-sibling'\n");
        wc.console_write_yellow("'--rel-as-child'\n");
        wc.console_write_yellow("'--rel-interactive'\n\n");
        wc.console_write_yellow("'--show'\n\n");
        wc.console_write_yellow("'--show-compact'\n\n");
        wc.console_write_yellow("'--cd'\n\n");
        wc.console_write_yellow("'--cd-node'\n\n");
        wc.console_write_yellow_bold("OR one or many of the following metadata fields to display:\n\n");
        wc.console_write_yellow("'--md-date'\n");
        wc.console_write_yellow("'--md-title'\n");
        wc.console_write_yellow("'--md-id'\n");
        wc.console_write_yellow("'--md-desc'\n");
        wc.console_write_yellow("'--md-filetype'\n");
        wc.console_write_yellow("'--md-project'\n");
        wc.console_write_yellow("'--md-tags'\n");
        wc.console_write_yellow("'--md-class'\n");
        wc.console_write_yellow("'--md-datadirs'\n");
        wc.console_write_yellow("'--md-keypairs'\n");
        wc.console_write_yellow("'--md-linkedids'\n");

        throw wibble::wibble_exit(1);

    }
    w = wibble::WibbleUtility::get_record_interactive(wc, wib_e, SEARCH); 

    
    if(META.cd_dd || META.cd_node) // for changing directory
    {
        const bool txt_not_data = META.cd_node ? true : false;
        WibbleUtility::cd_record(wib_e, w, SEARCH.archive_db, txt_not_data);
    }
    if(META.path_dd || META.path_node) // for listing directory
    {
        const bool txt_not_data = META.path_node ? true : false;
        std::string dir = wib_e._build_note_path(w.get_id(), w.get_type(), w.get_proj(), SEARCH.archive_db, txt_not_data);
        wc.enable_output();
        wc.console_write(dir);
    }
    else if(META.update_mt) // are we updating?
    {
        wc.enable_output();
        _update_record(OPTS, wc, w, wib_e, SEARCH.archive_db);
    }
    else if(META.get_mt_date  ||
            META.get_mt_title ||
            META.get_mt_id    ||
            META.get_mt_desc  ||
            META.get_mt_ft    ||
            META.get_mt_proj  ||
            META.get_mt_tags  ||
            META.get_mt_cls   ||
            META.get_mt_dd    ||
            META.get_mt_kp    ||
            META.get_mt_lids)  // displaying a single field...
    {
        wc.enable_output();
        _print_field(w, META);
    }
    else if(META.rel_as_parent   ||
            META.rel_as_sibling  ||
            META.rel_as_child    ||
            META.rel_interactive ||
            META.rel_delete      ||
            META.rel_delete_all  ||
            META.show_rels       || 
            META.show_rels_c) // defining/displaying a relationship
    {
        wc.enable_output();
        WibbleRelationship wr{wib_e, wc};

        if     (META.rel_interactive) { wr.relationship_menu(w, SEARCH.archive_db);                                      }
        else if(META.rel_as_parent)   { wr.add_node_relationships(w, WibbleRelationship::relationship_type::PARENT_OF);  }
        else if(META.rel_as_sibling)  { wr.add_node_relationships(w, WibbleRelationship::relationship_type::SIBLING_OF); }
        else if(META.rel_as_child)    { wr.add_node_relationships(w, WibbleRelationship::relationship_type::CHILD_OF);   }
        else if(META.rel_delete)      { wr.remove_node_relationship(w, SEARCH.archive_db);                               }
        else if(META.rel_delete_all)  { wr.remove_all_relationships_for_record(w, SEARCH.archive_db);                    }
        else if(META.show_rels)       { wr.display_node_relationships(w, SEARCH.archive_db, true);                       }
        else if(META.show_rels_c)     { wr.display_node_relationships(w, SEARCH.archive_db, false);                      }
        
    }
    else // ...or printing the entire metadata to console
    {
        wc.enable_output();
        if(ENABLECOLOUR) { w.pretty_print_record_full();    }
        else             { w.pretty_print_record_full_nc(); }
    }
} 
