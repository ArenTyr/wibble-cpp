/**
 * @file      wibblejanitor.hpp
 * @brief     Provides a simple and clean non-XDG "trash" directory (header file).
 * @details
 *
 * A simple, deliberately non-XDG compliant "trash" directory manager,
 * designed to make it simple to safely delete and restore files, and
 * avoid accidental rm nightmares. Notion is to make its use seamless
 * by adding bash aliases for 'rm', etc.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_JANITOR_H
#define WIBBLE_JANITOR_H

#include <set>
#include <vector>

#include "wibbleconsole.hpp"
#include "wibbleio.hpp"

namespace wibble
{
    class WibbleJanitor
    {
    private:
        const std::size_t FILE_DISP_LENGTH = 40;
        const std::size_t TRSH_DISP_LENGTH = 80;
        const std::string TRSH_PREFIX = "/trash/";
        const std::string TRSH_DNAME  = "/.janitor"; 
        WibbleConsole wc;
        const std::string cache_dir;
        const std::string janitor_config_file;
        const std::string janitor_history_file;
        std::vector<std::pair<std::string, std::string>> trash_directories; //!< Mapping of device/trash root : filename within hashed trash sub-directory
        std::string _determine_trash_target(const std::string& trash_candidate);
        std::string _display_obliteration_ascii_art();
        bool _ensure_setup_trash_dirs();
        bool _get_setup_defined_trash_dir_for_devices();
        std::pair<std::vector<std::string>, std::set<std::string>> _get_trashed();
        void _obtain_trash_dirs();
        void _preview_delete(const std::vector<std::string>& del_list);
        void _restore_selected_file(const std::string& trsh_file, const std::string& dest_dir);

    public:
        WibbleJanitor(const std::string& cdir, const std::string& config_file, const std::string& history_file):
            cache_dir{cdir},
            janitor_config_file{config_file},
            janitor_history_file{history_file}
        {
            _obtain_trash_dirs();
        };
        void configure();
        void list_trashed();
        void obliterate_files(const bool show_graphic);
        void restore_file();
        void scrub_files(const std::vector<std::string>& file_list);
        void show_usage();
    };

    using trash_pair = std::pair<std::string, std::string>;
    using trash_list = std::vector<std::pair<std::string, std::string>>;
}
#endif
