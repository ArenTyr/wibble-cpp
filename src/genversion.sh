#!/bin/sh

BUILD_DATE=$(date -R)
BUILD_INFO=$(date +"%Y%m%d%H%M%S")

 sed "s/BUILD_DATE = \"\"/BUILD_DATE = \"$BUILD_DATE\"/
      s/CURRENT_VERSION = \"\"/CURRENT_VERSION = \"$BUILD_INFO\"/" wibbleversion.cpp.stub > wibbleversion.cpp

