/**
 * @file      cmd_batch.hpp
 * @brief     Handle "batch" subcommand (header file).
 * @details
 *
 * This class dispatches the non-interactive "batch" functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CMD_BATCH_H
#define CMD_BATCH_H

#include "cmd_structs.hpp"
#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleCmdBatch
    {
    private:
        static int check_input(WibbleConsole& wc, batch_options& BATCH);
        // setup switching logic/state mapping
        /**
         * @brief Batch mode enumeration; setup switching logic.
         */

    public:
        static void handle_batch(pkg_options& OPTS, batch_options& BATCH);
    };

    enum batch_op
    {
        BATCH_CREATE_NODE,           //!< Create a new Node record
        BATCH_CREATE_TASK_ITEM,      //!< Create a new Task item
        BATCH_CREATE_TOPIC_ENTRY,    //!< Create a new Topic entry
        BATCH_CREATE_JOTFILE_ENTRY,  //!< Create a new Jotfile entry

        BATCH_CREATE_NEW_TASK_GROUP, //!< Create a new Task group
        BATCH_CREATE_NEW_TOPIC,      //!< Create a new Topic 
        BATCH_CREATE_NEW_JOTFILE,    //!< Create a new Jotfile

        BATCH_NODE_ADD_DATA,         //!< Add data to existing Node
        BATCH_TASK_ADD_DATA,         //!< Add data to existing Task item
        BATCH_TOPIC_ADD_DATA,        //!< Add data to existing Topic 

        INVALID_BATCH_DATA_OP,       //!< Invalid/incomplete Data request
        INVALID_BATCH_NODE_OP,       //!< Invalid/incomplete Node request
        INVALID_BATCH_TASK_OP,       //!< Invalid/incomplete Task request
        INVALID_BATCH_TOPIC_OP,      //!< Invalid/incomplete Topic request
        INVALID_BATCH_JOTFILE_OP,    //!< Invalid/incomplete Jotfile request
        INVALID_OP                   //!< General batch error/incomplete request
    };
}
#endif
