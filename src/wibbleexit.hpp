/**
 * @file      wibbleexit.hpp
 * @brief     Custom exit routine to properly terminate exeuction (header file).
 * @details
 *
 * This class provides a own custom exit routine to properly terminate
 * and exit from main() with proper resource cleanup.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_EXIT_H
#define WIBBLE_EXIT_H
#include <string>
#include <iostream>

namespace wibble
{
    /**
     * @brief Custom exit exception type, for correct program termination.
     *
     * Allow proper stack unwinding/resource freeing rather than just using
     * "bad habit" C-style exit(1) etc. with potential stack resource leakage.
     * Ensures destructors get called properly.
     */
    struct wibble_exit: public std::exception
    {
        int value;
        wibble_exit(int value): value{value} {}
    };

    /**
     * @brief For debugging purposes, manual 'breakpoint'; immediately halt execution and output any debug string.
     * @param msg String with any values/information/state we wish to dump to screen to aid debuggin.
     */
    static void STOP_DEBUG(const std::string& msg)
    {
        std::cerr << "STOP_DEBUG:\n---\n" << msg << "\n---\n\n";
        throw wibble_exit(0);
    }

    /**
     * @brief Function for ensuring 'STOP_DEBUG' always gets linked
     * 
     * A "pointless" function pointer/struct purely to retain above function for
     * debugging purposes, and to suppress any compiler warnings about it
     * being unused - since it is only explicitly used when there
     * is something to debug!
     */
    struct debugging_unused_retain_linkage
    {
        typedef void (*empty_stop_debug)(const std::string&);
        empty_stop_debug esd = &wibble::STOP_DEBUG;
    };
}

#endif
