/**
 * @file      cmd_janitor.cpp
 * @brief     Dispatcher for "janitor" functionality.
 * @details
 *
 * Dispatcher class for handling subcommand jantior for deleting
 * and restoring files on the filesystem.
 * 
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "cmd_janitor.hpp"
#include "wibblejanitor.hpp"

/**
 * @brief Dispatching logic for the "janitor" functionality
 * @param OPTS package/general configuration options struct
 */
void wibble::WibbleCmdJanitor::handle_janitor(pkg_options& OPTS, janitor_options& JAN)
{
    const std::string jfcache = OPTS.paths.tmp_dir;
    const std::string jfconfig = OPTS.paths.wibble_store  + "/janitor/janitor_config.wib";
    const std::string jfhistory = OPTS.paths.wibble_store + "/janitor/janitor_history.wib";
    WibbleJanitor wj{jfcache, jfconfig, jfhistory};

    const bool show_graphic = ( ! JAN.boring) ? true : false;
    
    if(JAN.list)                  { wj.list_trashed();                  }
    else if(JAN.obliterate)       { wj.obliterate_files(show_graphic);  }
    else if(JAN.restore)          { wj.restore_file();                  }
    else if(JAN.scrub.size() > 0) { wj.scrub_files(JAN.scrub);          }
    else if(JAN.usage)            { wj.show_usage();                    }
    else if(JAN.config)           { wj.configure();                     }
    // otherwise just shows configured directories
} 
