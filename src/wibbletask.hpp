/**
 * @file      wibbletask.hpp
 * @brief     Provides all of the "task" functionality (header file).
 * @details
 *
 * This class provides all of the functionality related to task
 * and project management. 'Tasks' can be added, put into groups,
 * and associated with Nodes, Topics, Jotfiles. They can be
 * 'extended' into a 'project' with detailed description,
 * task-specific Jotfile, and custom project files added/stored
 * with the task entry. Priority, due date and category can be set
 * and edited; theese criteria can be used for filtering and finding
 * relevant tasks. Finally they can completed, cloned, and reactivated.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_TASK_H
#define WIBBLE_TASK_H

#include <fstream>
#include <set>
#include "wibblebookmark.hpp"

namespace wibble
{
    using bm = wibble::WibbleBookmark::bookmark_entry;
    class WibbleTask
    {
    public:

        /**
         * @brief Struct that defines data model pertaining to an individual "task" entry.
         */
        typedef struct task_entry
        {
            std::string fn;       //!< filename of task file entry occurs in
            bool extended;        //!< extended flag to indicate whether task has additional data
            std::string task_id;  //!< guaranteed unique identifier hash for task
            std::string due_date; //!< string representation of due date in YYYY-MM-DD or ___NONE___
            std::string category; //!< category task is labelled with (i.e. a "tag")
            int priority;         //!< priority, integer in range 0-99 (0 = min, 99 = max)
            std::string title;    //!< task title/single-line content of task
            bool completed;       //!< flag indicated completion status
            task_entry():
                fn{"___NULL___"},
                extended{false},
                task_id{"___NULL___"},
                due_date{"___NULL___"},
                category{"general"},
                priority{0},
                title{"___NULL___"},
                completed{false} {};
        } task_entry;

        /**
         * @brief Struct that defines data model pertaining to a task group; all individual task entries exist in at most one group.
         */
        typedef struct task_group
        {
            std::string fn;         //!< filename of the task group file
            std::string group_name; //!< human readble name/description of the task group
            std::string group_id;   //!< unique identifying id/hash of task group
            int com_tasks;          //!< integer representing count of completed tasks
            int tot_tasks;          //!< integer representing count of total/added tasks (open tasks = tot_tasks - com_tasks)
            std::vector<std::pair<std::string, std::string>> nodes;
            task_group():
                fn{"___NULL___"},
                group_name{"___NULL___"},
                group_id{"___NULL___"},
                com_tasks{0},
                tot_tasks{0} {};
        } task_group;

        typedef struct task_group_set
        {
            task_group tg;
            std::pair<std::vector<WibbleTask::task_entry>, std::vector<WibbleTask::task_entry>> tasks; 
        } task_group_set;

    private:
        // various critical constants
        const std::string DF_GROUP          = "General (default group) [ ";
        const std::string DF_GROUP_ID_STUB  = "0000000001_default";
        const std::string DF_GROUP_FN_STUB  = "0000000001_default.wib";
        const std::string DELIM             = "|";
        const std::string CLOSED_GROUP_PATH = "/closed_tasks/task_groups/";
        const std::string CLOSED_TASK_PATH  = "/closed_tasks/task_lists/";
        const std::string OPEN_TASK_PATH    = "/open_tasks/task_lists/";
        const std::string OPEN_GROUP_PATH   = "/open_tasks/task_groups/";
        const std::string TASK_DATA_PATH    = "/task_data/";
        const std::string TASK_HISTORY      = "task_history.wib";
        const std::string WIB_LINKED        = "linked_files/";

        std::size_t TASK_DISPLAY_WIDTH = 60;

        unsigned int tot_completed_tasks;

        WibbleConsole wc;

        const std::string tasks_base_dir;
        const std::string bin_mapping;
        const std::string cache_dir;
        const std::string templates_dir;

        std::map<std::string, std::vector<task_entry>> closed_tasks; //!< map of task group : task entries for all closed/completed tasks
        std::map<std::string, std::vector<task_entry>> open_tasks;   //!< map of task group : task entries for all open/todo tasks
        std::map<std::string, task_group> task_groups;               //!< map of group ID : task group definition for all open groups
        std::map<std::string, task_group> task_groups_archived;      //!< map of group ID :  task group definition for all closed/finished groups


        void _accumulate_open_tasks(const bool silent = false);
        void _accumulate_closed_tasks(const bool silent = false);
        void _accumulate_tasks(const bool o_tasks, const bool silent = false);
        void _accumulate_task_groups(const bool silent = false);
        void _accumulate_closed_task_groups(const bool silent = false);

        bool _add_edit_inline_task_details(task_entry& entry);
        bool _add_project_files(task_entry& entry);
        bool _add_task_to_group(const task_group& tg, const std::string& task_id, const std::string& category) const;
        bool _adjust_task_properties(task_entry& entry, const bool closed_group);

        bool _associate_jotfile_with_task(task_entry& entry, const pkg_options& OPTS);
        bool _associate_linked_file_with_task(task_entry& entry, const std::string& data_file = "", const std::string& data_action = "");
        bool _associate_node_with_task(task_entry& entry, const pkg_options& OPTS);
        bool _associate_topic_with_task(task_entry& entry, const pkg_options& OPTS);

        std::set<std::string> _build_category_list();
        void _build_display_tasks_in_group(unsigned long& index, const std::string& group_name, std::map<std::size_t, task_entry>& task_num_map, std::vector<task_entry>& tasks, const bool show_completed, const bool selector, const std::size_t all_results);

        bool _copy_project_into_topic(const pkg_options& OPTS, std::string& dd_path);
        bool _copy_project_into_node(const pkg_options& OPTS, std::string& dd_path, const std::string& lf_list);
        void _close_task_group(const task_group& tg, const bool reopen = false);

        bool _delete_task(const task_entry& entry, const bool closed_group);
        void _display_group_completion_stats(unsigned long tot_com, unsigned long tot_open);
        task_entry _dispatch_task_group_command(const std::string& r_ans, const task_group& selected_group, const bool closed_group);
        void _duplicate_task_group(const task_group& tg);

        bool _excise_entry_from_open_task_file(const std::string& task_id, const std::string& taskfile_fn);
        std::string _extract_created_date(const std::string& task_id) const;

        const std::string _find_groupfile_for_task(const std::string& task_id, const std::string& group_dir);

        void _gather_tasks(const bool silent = false);
        const std::string _get_closed_group_dir(const std::string& year) const;
        const std::string _get_closed_task_dir(const std::string& year) const;
        const std::string _get_closed_taskfile_fn(const std::string& category, const std::string& year) const;
        int _get_completed_tasks() const;
        const std::string _get_desc_file_for_task(const std::string& task_id) const;
        const std::string _get_data_dir_for_task(const std::string& task_id) const;
        void _get_jotfile_perform_action(const task_entry& entry, const pkg_options& OPTS);
        const std::string _get_proj_data_dir_for_task(const std::string& task_id) const;
        const std::string _get_proj_index_for_task(const std::string& task_id) const;
        const std::string _get_open_group_dir(const std::string& year) const;
        const std::string _get_open_task_dir(const std::string& year) const;
        const std::string _get_open_taskfile_fn(const std::string& category, const std::string& year) const;
        const std::string _get_task_data_lf_fn(const std::string& task_id) const;
        const std::string _get_task_data_jf_fn(const std::string& task_id) const;
        const std::string _get_task_data_nodes_fn(const std::string& task_id) const;
        const std::string _get_task_data_topic_fn(const std::string& task_id) const;
        const std::string _get_task_specific_jf_fn(const std::string& task_id) const;
        const std::string _get_valid_datetime_str_or_NONE(const std::string& due_date) const;
        bm _get_topic_record_from_task_list(const task_entry& entry);
        WibbleRecord _get_node_record_from_task_list(const task_entry& entry, WibbleExecutor& wib_e);
        const std::string _get_topic_fn_from_task_list(const task_entry& entry, WibbleExecutor& wib_e);
        std::pair<std::vector<task_entry>, std::vector<task_entry>> _get_tasks_for_id_list(const std::vector<std::pair<std::string, std::string>>& ids);
        task_group _get_task_group_for_name(const std::string& t_group_name);

        task_entry _list_open_tasks(const std::string& cat_fltr = "", const std::string& title_fltr = "", const bool selector = false, const bool c_tasks = false);
        task_entry _list_completed_tasks(const bool selector = false);
        std::string _list_project_files(const std::string& task_id);
        task_entry _list_tasks_in_group(const bool selector, const task_group& selected_group, const bool closed_group = false); 

        std::string _mark_entry_in_group_header(const std::string& header_line, const bool decrement = false, const bool dec_completed = false);
        void _mark_task_done(const std::string& cat_fltr, const std::string& title_fltr, const pkg_options& OPTS);
        bool _mark_task_as_done(const task_entry& entry);
        bool _move_task_to_closed_from_taskfile(const task_entry& entry);

        task_entry _parse_task_file_for_single_id(const std::string& task_id, const std::string& category);
        bool _perform_project_file_action(const std::string& task_id, const std::string& pf, const pkg_options& OPTS);
        std::string _present_category_list();
        std::string _present_group_task_command(const unsigned long tot_results, const bool closed_group, const bool show_sort = true);
        void _present_task_action_menu(task_entry& entry, const pkg_options& OPTS, const bool closed_group);
        void _print_task_condensed(const task_entry& entry, const bool completed = false, const int padding = 0, const bool show_cat = true);
        void _present_lf_extension_menu(const task_entry& entry, const pkg_options& OPTS);
        void _present_task_extension_menu(const task_entry& entry, const pkg_options& OPTS);
        void _present_task_node_action_menu(const task_entry& entry, const pkg_options& OPTS);
        void _present_task_topic_action_menu(const task_entry& entry, const pkg_options& OPTS);
        std::vector<task_entry> _parse_task_file_with_filter(const std::string& fn, const std::set<std::string>& id_list, const bool& mark_completed);
        std::vector<task_entry> _parse_task_file(std::stringstream& task_ss, const std::string& fn, const std::set<std::string>& id_list, const bool& mark_completed);
        task_group _parse_group_file(std::stringstream& group_ss);

        bool _remove_task_from_taskfile(const std::string& task_id, const std::string& taskfile, const bool group_header_line = false, const bool completed_task = false);
        bool _remove_task_from_groupfile(const std::string& task_id, const bool completed, const std::string& taskfile);
        void _reopen_task_group(const task_group& tg);

        task_group _select_group_from_list(const std::string& preselect_group = "", const bool ls_only = false, const bool closed_groups = false);
        bool _set_extended_task_attribute(task_entry& task);
        task_group _set_default_group();

        bool _update_task_in_taskfile(const task_entry& task, const std::string& taskfile);
        bool _update_task_in_groupfile(const task_entry& task, const std::string& groupfile);
        bool _update_task_details(const task_entry& task, const bool closed_group, const std::string& existing_category);

        bool _write_history(const std::string& id, const std::string& category, const int& priority, const std::string& title);
        bool _write_history(const task_entry& entry, int op, const std::string& pre_text = "");
        bool _write_history(const std::vector<task_entry>& entry, int op, const std::string& pre_text = "");

        bool _update_existing_task_entry(const task_entry& entry);
        bool _update_task_to_closed_in_group_file(const std::string& task_id);

        task_entry _validate_and_add_task(const std::string& title, std::string& category, const std::string& due_date,
                                    const int priority, const task_group& group);
    public:
        void dump_group_names();
        task_group add_new_group(const std::string& auto_group_name = "");
        task_entry add_new_task(const std::string& task_title, const std::string& prefill_category = "");
        std::string batch_add_task_item(const std::string& title, std::string& category, const std::string& due_date,
                                 const int priority, const std::string& t_group_name);
        bool batch_add_task_data(const std::string task_id, const std::string& data_file, const std::string& data_action);
        void complete_task(const std::string& cat_fltr, const std::string& title_fltr, const pkg_options& OPTS);
        void display_history(const pkg_options& OPTS);
        void list_completed();
        void list_tasks(const std::string& cat_fltr, const std::string& title_fltr);
        void list_tasks_in_group(const pkg_options& OPTS);
        void list_tasks_in_closed_group(const pkg_options& OPTS);
        void show_everything();
        task_entry pick_task_entry();
        void print_task_detailed(const task_entry& entry) const;
        bool select_task_for_id(const std::string& task_id, const std::string& category, const pkg_options& OPTS);
        std::string serialize_task_entry(const task_entry& task);
        task_group_set export_task_group();
        WibbleTask(const std::string& tk_base_dir, const std::string& tmp_dir,
                   const std::string& t_dir, const std::string& b_map,
                   const bool silent_mode = false):
            tot_completed_tasks{0},
            tasks_base_dir{tk_base_dir},
            bin_mapping{b_map},
            cache_dir{tmp_dir},
            templates_dir{t_dir}
        {
            _gather_tasks(silent_mode);
        };
    };
}
#endif
