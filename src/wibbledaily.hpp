/**
 * @file      wibbledaily.hpp
 * @brief     Provides functionality for setting up numbered "daily" scripts (header file).
 * @details
 *
 * This class provides functionality to allow the user to setup convenient
 * scripts, automatically incremented/numbered, for typical routine jobs.
 * Rather like a semi-manual crontab. Individual or all scripts can be run;
 * designed to be used for recurrent reminders, or deliberately triggering
 * perodic events to terminal etc.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLEDAILY_H
#define WIBBLEDAILY_H
#include <vector>

#include "cmd_structs.hpp"
#include "wibbleconsole.hpp"

namespace wibble
{
    class WibbleDaily
    {
    private:
        bool _create_daily_dir_if_needed(WibbleConsole& wc, const std::string& scripts_dir);
        std::vector<std::string> _get_scripts(const std::string& daily_dir);
        unsigned long _find_numeric_gap(std::vector<std::string>& scripts);
        void _open_script_with_editor_prompt(WibbleConsole& wc, const std::string& sf, const std::string& padded,
                                            const std::string& cli, const std::string& gui);

    public:
        void add_daily(pkg_options& OPTS, WibbleConsole& wc);
        void edit_or_del_daily(pkg_options& OPTS, WibbleConsole& wc, const bool do_delete);
        std::vector<std::string> ls_daily(pkg_options& OPTS, WibbleConsole& wc);
        void exec_script(pkg_options& OPTS, WibbleConsole& wc, const std::string& exec_num);
    };
}
#endif
