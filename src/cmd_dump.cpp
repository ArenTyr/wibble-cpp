/**
 * @file      cmd_dump.cpp
 * @brief     Handle "dump" subcommand.
 * @details
 *
 * This very simple class is a wrapper around the user's preferred
 * console output tool for quickly displaying note content on the console.
 * Typically this would be 'cat', but other options such as 'batcat'
 * could be used if so desired...
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_dump.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Dispatching logic for the "dump" operation on Wibble nodes.
 * @param OPTS package/general configuration options struct
 * @param SEARCH search filters/fields to generate result set
 */
void wibble::WibbleCmdDump::handle_dump(pkg_options& OPTS, search_filters& SEARCH)
{
    WibbleConsole wc; WibbleExecutor wib_e(OPTS, SEARCH.single_only);
    WibResultList records = WibbleUtility::get_records_multi_interactive(wc, wib_e, SEARCH);
    for(auto const& r: *records)
    {
        bool dump_result = wib_e.note_pipe_through_dump_tool(r, SEARCH.archive_db);
        if(! dump_result) { wc.console_write_error_red("Error dumping note to terminal."); }
    }
}
