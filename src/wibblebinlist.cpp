/**
 * @file      wibblebinlist.cpp
 * @brief     Decomposes the user binary mapping configuration for support of binary files.
 * @details
 *
 * This class parses the users "binary_mapping" field to yield the defined
 * user custom binary file associations/pairings: given extension with
 * given editing/opening tool and optional viewer. Also ensures that user
 * has setup a default stub/prototype/template file to use as a "blank"
 * for new copies.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "wibblebinlist.hpp"
#include "wibbleconsole.hpp"
#include "wibbleexit.hpp"
#include "wibblesymbols.hpp"

#include <filesystem>
#include <sstream>
#include <vector>

// static to ensure only set/run once since data does not change
std::set<std::string> wibble::WibBinList::bin_set = std::set<std::string>();
bool wibble::WibBinList::do_setup = false;

/**
 * @brief Method to determine set of known/defined custom binary file extensions
 * @param bin_mapping configuration key/string with binary file associations/commands
 * @param t_plate_dir location of template directory, to check user has setup necessary stub file
 */
std::set<std::string> wibble::WibBinList::gen_bin_list(const std::string& bin_mapping, const std::string& t_plate_dir, const bool silence_mode)
{
    // static bool/data ensures logic below only run once and once only
    if(! do_setup)
    {
        WibbleConsole wc;
        if(silence_mode) { wc.silence_output(); }
        // nothing to do, no user custom binary associations
        if(bin_mapping == "UNSET") { do_setup = true; return bin_set; }
        else
        {
            // split the command comma delimited list
            try
            {
                std::vector<std::string> mapping_list;
                std::stringstream bin_ss{bin_mapping};
                std::string token;
                while(std::getline(bin_ss, token, ','))
                {
                    mapping_list.push_back(token);
                }
                for(auto const& prg: mapping_list)
                {
                    std::size_t ext_delim = prg.find(":");
                    if(ext_delim == std::string::npos) { wc.console_write_red(ARW + "Bad mapping: " + prg + ". Skipping\n"); continue; }
                    std::string exten = prg.substr(0, ext_delim);
                    wc.trim(exten);
                    bin_set.insert(exten);
                }
                std::stringstream().swap(bin_ss);
            }
            catch(std::exception& ex)
            {
                wc.console_write_error_red("Invalid binary mapping configuration: " + bin_mapping + "\n");
            }

            if(bin_set.size() > 0) { wc.console_write_yellow(ARW + "Registered binary types: "); }
            for(auto const& itm: bin_set)
            {
                if(! std::filesystem::exists(t_plate_dir + "/bin_default/default." + itm))
                {
                    wc.console_write("");
                    wc.console_write("");
                    wc.console_write_error_red("- Binary type '" + itm + "' registered but no default template defined/found.");
                    wc.console_write_error_red("- Please setup/create the default file for this type at the following location:\n\n");
                    wc.console_write_yellow(t_plate_dir + "/bin_default/default." + itm + "\n\n");
                    throw wibble::wibble_exit(1);
                }
                wc.console_write_yellow_bold(itm + " ");
            }
            if(bin_set.size() > 0) { wc.console_write(""); }

            do_setup = true;
            return bin_set;
        }
    }
    else
    {
        return bin_set;
    }
}
