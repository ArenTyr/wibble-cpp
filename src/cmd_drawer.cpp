// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_drawer.cpp
 * @brief     Handle "drawer" subcommand.
 * @details
 *
 * This class dispatches out the relevant logic for managing the various
 * file operations for dealing with transient files via drawers
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cmd_drawer.hpp"
#include "wibbledrawer.hpp"
#include "wibbleutility.hpp"

/**
 * @brief Static function to dispatch out to the various drawer operations
 * @param OPTS package/general configuration options struct
 * @param DRAW command line options/parameters supplied via switches to drawer command
 */
void wibble::WibbleCmdDrawer::handle_drawer(pkg_options& OPTS, drawer_options& DRAW)
{
    WibbleConsole wc;

    WibbleDrawer wd{OPTS.paths.tmp_dir, OPTS.paths.drawers_dir};

    if(DRAW.add_drawer)
    {
        if(wd.add_drawer(wc))
        {
            wc.console_write_success_green("New drawer added.");
        }
    }
    else if(DRAW.copy_to_node)
    {
        if(wd.select_drawer(wc, DRAW.num_drawer))
        {
            wd.copy_drawer_to_node(wc, OPTS, DRAW.num_drawer);
        }
        else
        {
            wc.console_write_error_red("Aborting. Nothing copied.");
        }
    }
    else if(DRAW.copy_to_topic)
    {
        if(wd.select_drawer(wc, DRAW.num_drawer))
        {
            wd.copy_drawer_to_topic(wc, OPTS, DRAW.num_drawer);
        }
        else
        {
            wc.console_write_error_red("Aborting. Nothing copied.");
        }
    }
    else if(DRAW.cd_drawer)
    {
        wd.cd_drawer_dir(wc, DRAW.num_drawer);
    }
    else if(DRAW.delete_drawer)
    {
        wd.delete_drawer(wc, DRAW.num_drawer);
    }
    else if(DRAW.push_to_drawer != "UNSET")
    {
        wd.push_file(wc, DRAW.push_to_drawer, DRAW.num_drawer, DRAW.force_mode);
    }
    else if(DRAW.tree_mode && ! DRAW.list_drawer && ! DRAW.browse_drawer)
    {
        wc.console_write_error_red("Tree option must be used in conjunction with either '--list' or '--browse'.");
    }
    else if(DRAW.list_drawer)
    {
        if(DRAW.tree_mode) { wd.list_drawer(wc, DRAW.num_drawer, false, true);  }
        else               { wd.list_drawer(wc, DRAW.num_drawer, false, false); }
    }
    else if(DRAW.browse_drawer)
    {
        if(DRAW.tree_mode) { wd.list_drawer(wc, DRAW.num_drawer, true, true);  }
        else               { wd.list_drawer(wc, DRAW.num_drawer, true, false); }
    }
    else if(DRAW.select_drawer)
    {
        wd.select_drawer(wc, DRAW.num_drawer);
    }
    else
    {
        wd.display_chest(wc);
    }
}
