/**
 * @file      wibbletopicmanager.hpp
 * @brief     Various algorithms and functions providing Topics backend (header file).
 * @details
 *
 * This class does all of the work providing the Topic functionality and actions.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLETOPICMANAGER_H
#define WIBBLETOPICMANAGER_H

#include "wibbleexecutor.hpp"
#include "wibbletopics.hpp"

namespace wibble
{
    class WibbleTopicManager
    {
    private:
        const std::string bin_mapping;
        std::string _build_topic_entry_full_filename(WibbleConsole& wc, const std::string& topic_base_dir,
                                                     topic_def& topic, const std::string& filename,
                                                     const bool txt_not_data);
        bool _check_null_topic(topic_def& topic);
        void _check_use_template(WibbleConsole& wc, WibbleTopics& wt, topic_def& topic,
                                 const std::string t_mplate_path, const std::string& topic_entry_fn);
        void _check_use_default_template(WibbleConsole& wc, WibbleTopics& wt, topic_def& topic,
                                         const std::string& template_dir, const std::string& topic_entry_fn);
        void _confirm_proceed_migration_or_exit(WibbleConsole&wc, const std::string& existing_fn,
                                        const std::string& src_topic_name, const std::string& dest_topic_name);
        bool _exec_ripgrep_for_index(const std::string& topic_entry_dir, const std::string& tmp_dir,
                                     const std::string& grep_str);

        void _do_hibernation_action(pkg_options& OPTS, WibbleTopics& wt, const bool do_hibernate);
        void _expand_topic_name(std::string& topic_name);
        void _filetype_migration_confirm_or_exit(WibbleConsole& wc, const std::string& src_topic_type, const std::string& dest_topic_type);
        std::string _filter_topic_tmp_index(WibbleConsole& wc, const std::string& topic_base_dir,
                                            const std::string tmp_dir, const std::string topic_id,
                                            const std::string& filter_expression, const bool txt_not_data);
        WibbleTopics::rg_matches _generate_ripgrep_result_index(WibbleConsole& wc, WibbleTopics& wt,
                                                                const std::string& topic_base_dir,
                                                                const std::string& tmp_dir, const std::string& topic_index,
                                                                topic_def& topic, const std::string& grep_str);
        std::string _get_template_path_or_exit(WibbleConsole& wc, WibbleExecutor& wib_e, const std::string& t_mplate_ref,
                                               const std::string& t_mplate_dir);
        topic_def _get_topic_or_exit(WibbleConsole& wc, WibbleTopics& wt, std::string& topic_name);
        void _op_on_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleConsole& wc,
                                WibbleExecutor& wib_e, WibbleTopics& wt, topic_def& topic,
                                const std::string& topic_name);
        void _open_or_delete_data_entry(const pkg_options& OPTS, topic_options& TOPIC, WibbleConsole& wc,
                                        WibbleExecutor& wib_e, WibbleTopics& wt, topic_def& topic);
        void _preview_confirm_migrate_entry(WibbleConsole& wc, const std::string& existing_fn,
                                            const std::string& existing_title, std::string& set_data_title, const bool txt_not_data);
        bool _remove_entry_post_migration(WibbleConsole& wc, WibbleTopics& wt, topic_def& topic, const std::string topic_base_dir,
                                          const std::string& existing_fn, const std::string& src_fn, const std::string& new_fn,
                                          const bool txt_not_data, const bool data_entry_insert_result);

        void _rename_topic_entry_wrapper(pkg_options& OPTS, topic_options& TOPIC, WibbleConsole& wc,
                                         WibbleTopics& wt, topic_def& topic, const std::string& topic_name);
        std::string _ripgrep_topic_content_get_entry(WibbleConsole& wc, WibbleTopics& wt,
                                                     const std::string& topic_base_dir, const std::string& tmp_dir,
                                                     const std::string& topic_index, topic_def& topic);
        void _run_exec_on_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleExecutor& wib_e, const std::string& topic_fn,
                                      const std::string& bin_edit, const std::string& bin_view);
        std::string _set_topic_entry_operation_or_dump(topic_options& TOPIC, WibbleConsole& wc, WibbleTopics& wt);
        void _topic_entry_delete(WibbleConsole& wc, WibbleTopics& wt, const std::string topic_base_dir, const std::string& topic_index, topic_def& topic);
    public:
        void add_new_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt);
        void topic_default_template(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt);
        void dispatch_data_topic_entry(const pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt);
        void migrate_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt);
        void process_topic_entry(pkg_options& OPTS, topic_options& TOPIC, WibbleTopics& wt);
        void hibernate_a_topic(pkg_options& OPTS, WibbleTopics& wt);
        void reactivate_hibernated_topic(pkg_options& OPTS, WibbleTopics& wt);
        // accessor for use with bookmarks facility
        topic_def get_topic_entry_fn(WibbleConsole& wc, WibbleTopics& wt, const std::string& topic_base_dir, const std::string& tmp_dir,
                                       const std::string& filter_exp, const std::string& grep_exp, const bool txt_not_data);

        topic_def set_ensure_topic_or_exit(WibbleConsole& wc, WibbleTopics& wt);
        WibbleTopicManager(const std::string& b_map):
            bin_mapping{b_map} {};
    };

}
#endif
