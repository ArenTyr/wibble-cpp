// -*- mode:c++; coding:utf-8; -*-
/**
 * @file      cmd_sync.cpp
 * @brief     Handle "sync" subcommand.
 * @details
 *
 * This handles the dispatcher for the "sync" functionality.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "cmd_sync.hpp"
#include "wibblerecord.hpp"
#include "wibblesync.hpp"

/**
 * @brief Dispatching logic for the "sync" functionality
 * @param SYNC command line options/parameters supplied via switches
 */
void wibble::WibbleCmdSync::handle_sync(sync_options& SYNC)
{
    WibbleSync ws;
    WibbleConsole wc;
    wc.trim(SYNC.source_dir);
    wc.trim(SYNC.dest_dir);

    if(SYNC.tree_dir) // show detailed status of all files in source_dir
    {
        ws.sync_directories(SYNC.source_dir, SYNC.dest_dir, true, false, false, 4);
    }
    else if(SYNC.status) // show sync proposals from source_dir -> dest_dir, but do not do anything
    {
        ws.sync_directories(SYNC.source_dir, SYNC.dest_dir, true, SYNC.detailed);
    }
    else if(SYNC.synchronise) // present interactive menu to execute sync from source_dir -> dest_dir
    {
        if(SYNC.action != -1) // automatic bypass, non-interactive
        {
            ws.sync_directories(SYNC.source_dir, SYNC.dest_dir, false, SYNC.detailed, false, SYNC.action);
        }
        else
        {
            ws.sync_directories(SYNC.source_dir, SYNC.dest_dir, false, SYNC.detailed);
        }
    }
    else // default action: generate a timestamp and description marker and dump to console
    {
        wc.console_write_success_green("No operation specified, generating timestamp/synchronisation markers.");
        wc.console_write_hz_divider();
        std::string genstamp = "WIBBLE_VERSION: \"" + wibble::WibbleRecord::generate_date_YMD("") + "\"\n";
        genstamp.append("WIBBLE_DESC_START\n  Some description\nWIBBLE_DESC_END\n");
        std::cout << genstamp;
    }
} 
