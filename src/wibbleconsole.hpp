/**
 * @file      wibbleconsole.hpp
 * @brief     Provides various functionality for ouput to terminal (header file).
 * @details
 *
 * Provides colourised output to the terminal courtesy of the rang
 * library, plus various utility functions for getting and cleaning
 * user input.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WIBBLE_CONSOLE_H
#define WIBBLE_CONSOLE_H

#include <string>
#include <set>

namespace wibble
{
    static bool silence = false;
    class WibbleConsole
    {
    private:
    public:
        static int pre_prompt_insert();
        inline static int append_rl_ml_input(int count, int key);
        inline static int end_rl_ml_input(int count, int key);
        inline static void init_ml_input();
        inline static void end_ml_input();
        inline static bool get_ml_input();

        void _setup_rl_prompt(std::string& colour_txt, const std::string& prompt, std::string& default_txt, std::string& disp_prompt) const;
        std::string console_write_prompt_ml(std::string colour_txt, const std::string& prompt, std::string default_txt);
        std::string console_write_prompt(std::string colour_txt, const std::string& prompt, std::string default_txt) const;
        void console_write(const std::string& msg, const bool allow_silence = true) const;
        void console_write_header(const std::string& hding, const bool allow_silence = true) const;

        void console_write_padded_selector(const size_t& index, const size_t& record_count, const std::string& text,
                                           const std::string& non_numeric = "", const bool allow_silence = true);
        void console_write_grey_on_blue(const std::string &msg, const bool allow_silence = true) const;
        void console_write_green(const std::string &msg, const bool allow_silence = true) const;
        void console_write_green_bold(const std::string &msg, const bool allow_silence = true) const;
        void console_write_grey_bold(const std::string &msg, const bool allow_silence = true) const;
        void console_write_yellow(const std::string &msg, const bool allow_silence = true) const;
        void console_write_yellow_bold(const std::string &msg, const bool allow_silence = true) const;
        void console_write_red(const std::string &msg, const bool allow_silence = true) const;
        void console_write_red_bold(const std::string &msg, const bool allow_silence = true) const;
        void console_write_success_green(const std::string &msg, const bool allow_silence = true) const;
        void console_write_error_red(const std::string &msg, const bool allow_silence = true) const;
        void console_write_hz_divider(const bool allow_silence = true) const;
        void console_write_hz_heavy_divider(const bool allow_silence = true) const;
        unsigned long console_present_record_selector(unsigned long total_results) const;
        std::set<int> console_present_range_record_selector(unsigned long total_results);
        
        std::string& ltrim(std::string& s, const char* t = " \t\n\r\f\v");
        std::string& rtrim(std::string& s, const char* t = " \t\n\r\f\v");
        std::string& trim(std::string& s, const char* t = " \t\n\r\f\v");
        std::string ltrim_copy(std::string s, const char* t = " \t\n\r\f\v");
        std::string rtrim_copy(std::string s, const char* t= " \t\n\r\f\v");
        std::string trim_copy(std::string s, const char* t= " \t\n\r\f\v");

        std::string set_pretty_width(const std::string& target, const std::size_t target_width);
        bool check_silence() { return silence; }
        void silence_output();
        void enable_output();
    };

    // silence incorrect compiler warnings about it being unused
    [[maybe_unused]] static bool ml_input_chomp;
}
#endif
