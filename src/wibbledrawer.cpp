/**
 * @file      wibbledrawer.cpp
 * @brief     Provides all of the "drawer" functionality.
 * @details
 *
 * This class provides all of the functionality related to drawer
 * management. A 'drawer' is a managed directory intended for transient
 * storage/organisation of files, prior to them either being discarded or
 * properly filed elsewhere.
 *
 * @author
 * Aren Tyr <aren.t.developer.l7b06@8shield.net> \n
 * <a href="https://www.codeberg.org/ArenTyr">Aren Tyr (Codeberg)</a>
 *
 * @copyright
 *
 *  This software is provided entirely "as-is" with no warranty.
 *  "Do what thou wilt". Your data; your responsibility.
 *
 * <a href="https://codeberg.org/ArenTyr/wibble-cpp">Wibble++ homepage (Codeberg)</a>
 *
 * @license
 *
 *  Copyright (C) 2024 Noctilucent Development / Aren Tyr.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

 #include "external/rang.hpp"
 #include "wibbleactions.hpp"
 #include "wibbledrawer.hpp"
 #include "wibbleio.hpp"
 #include "wibblerecord.hpp"
 #include "wibblesymbols.hpp"
 #include "wibbleutility.hpp"
 #include <iostream>
 #include <fstream>

/**
* @brief Show success copy message
* @param wc General WibbleConsole utility object
* @param mode Flag controlling whether it was a Node or Topic that recieved the data
*/
void wibble::WibbleDrawer::_copy_success_msg(WibbleConsole& wc, const int mode)
{
    wc.console_write("");
    wc.console_write_header("COPIED");
    auto dest_type = "Node";
    if(mode != 0)
    {
        dest_type = "Topic";
    }
    wc.console_write_green("Drawer files copied/propagated into ");
    wc.console_write_green(dest_type);
    wc.console_write_green(" data directory.\n");
    wc.console_write_green("\nThe files now exist in two locations.\n");
    wc.console_write_green("\nAfter verifying all are present/correct within the ");
    wc.console_write_green(dest_type);
    wc.console_write_green(",\n");
    wc.console_write_green("consider deleting the entire drawer to permanently remove all \n");
    wc.console_write_green("the now duplicated files.\n");
    wc.console_write_hz_divider();
}

/**
* @brief Copy entire contents of drawer into a Topic data entry
* @param wc General WibbleConsole utility object
* @param OPTS Main general configuration data structure
*/
bool wibble::WibbleDrawer::copy_drawer_to_node(WibbleConsole& wc, const pkg_options& OPTS, const int drawer_num)
{
    // Protect against signed/unsigned overflow with AND clause
    if(drawer_num > 0 && (unsigned long)drawer_num > chest.size())
    {
        wc.console_write_error_red("Invalid drawer number specified. Aborting");
        return false;
    }

    unsigned long drawer_sel;
    if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
    {
        drawer_sel = drawer_num - 1;
    }
    else
    {
        drawer_sel = def_drawer;
    }


    auto drawer_dir = drawer_root + "/" + chest.at(drawer_sel).drawer_id;
    auto result = WibbleActions().copy_project_files_into_node(wc, OPTS, drawer_dir, "___NONE___");
    if(result)
    {
        _copy_success_msg(wc, 0);
    }
    return result;
}

/**
* @brief Copy entire contents of drawer into a Topic data entry
* @param wc General WibbleConsole utility object
* @param OPTS Main general configuration data structure
*/
bool wibble::WibbleDrawer::copy_drawer_to_topic(WibbleConsole& wc, const pkg_options& OPTS, const int drawer_num)
{
    // Protect against signed/unsigned overflow with AND clause
    if(drawer_num > 0 && (unsigned long)drawer_num > chest.size())
    {
        wc.console_write_error_red("Invalid drawer number specified. Aborting");
        return false;
    }

    unsigned long drawer_sel;
    if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
    {
        drawer_sel = drawer_num - 1;
    }
    else
    {
        drawer_sel = def_drawer;
    }

    auto drawer_dir = drawer_root + "/" + chest.at(drawer_sel).drawer_id;
    auto result = WibbleActions().copy_project_files_into_topic(wc, OPTS, drawer_dir);
    if(result)
    {
        _copy_success_msg(wc, 1);
    }
    return result;
}


/**
* @brief Push (move) a file/directory into the currently selected/active drawer.
* @param wc General WibbleConsole utility object
* @param src_file String with filename of source file to move into drawer
* @param drawer_num (Optional) Integer with specific override to push to another drawer
* @param force_mode Flag indicating whether to bypass all prompts/warnings
*/
bool wibble::WibbleDrawer::push_file(WibbleConsole& wc, std::string src_file, const int drawer_num, const bool force_mode)
{
    if(chest.empty())
    {
       wc.console_write_error_red("Empty chest! Add at least one drawer to it to add files.");
       return false;
    }

    // Protect against signed/unsigned overflow with AND clause
    if(drawer_num > 0 && (unsigned long)drawer_num > chest.size())
    {
        wc.console_write_error_red("Invalid drawer number specified. Aborting");
        return false;
    }

    unsigned long drawer_sel;
    if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
    {
        drawer_sel = drawer_num - 1;
    }
    else
    {
        drawer_sel = def_drawer;
    }

    WibbleUtility::remove_trailing_slash(src_file);
    WibbleUtility::expand_tilde(src_file);
    if(! std::filesystem::exists(src_file))
    {
        wc.console_write_error_red("Unknown/non-existent file: '" + src_file + "'.");
        return false;
    }
    else
    {
        const auto dest_file = drawer_root + "/" + chest.at(drawer_sel).drawer_id + "/" +
                               std::string(std::filesystem::path(src_file).filename());

        if(std::filesystem::exists(dest_file) && ! force_mode)
        {
           wc.console_write_red_bold(ARW + "Destination file already exists:\n\n");
           wc.console_write(dest_file);
           wc.console_write_red_bold("\nProceed with overwrite/merge?\n\n");
           std::string answer = wc.console_write_prompt("(y/n) > ", "", "");
           wc.trim(answer);
           if(answer != "y" && answer != "Y")
           {
               wc.console_write_error_red("User cancelled/aborted.");
               return false;
           }
        }
        wc.console_write("");
        wc.console_write_header("PUSH ");
        wc.console_write_yellow_bold("\nSelected drawer\n\n");
        wc.console_write_yellow_bold(ARW + "Drawer ID         : ");
        wc.console_write(chest.at(drawer_sel).drawer_id);
        wc.console_write_yellow_bold(ARW + "Drawer Description: ");
        wc.console_write(chest.at(drawer_sel).drawer_desc);
        if(! force_mode)
        {
            WibbleUtility::confirm_file_move_or_exit(wc, src_file, dest_file, false);
        }
        WibbleIO::wmove_file(std::filesystem::path(src_file), std::filesystem::path(dest_file));
    }
    wc.console_write_hz_divider();
    wc.console_write_success_green("Successfully pushed to drawer '" + chest.at(drawer_sel).drawer_id + "'.");
    return true;
}

/**
* @brief Change into the directory of the given drawer. Requires cd helper/shell aliases in place
* @param wc General WibbleConsole utility object
* @param drawer_num (Optional) Integer with specific override to select another drawer
*/
void wibble::WibbleDrawer::cd_drawer_dir(WibbleConsole& wc, const int drawer_num)
{
    if(! chest.empty())
    {
        unsigned long drawer_sel;
        if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
        {
            drawer_sel = drawer_num - 1;
        }
        else
        {
            drawer_sel = def_drawer;
        }

        if(drawer_num > 0 && (unsigned long)drawer_num > chest.size())
        {
            wc.console_write_error_red("Invalid drawer number specified. Aborting");
        }
        else
        {
            const auto dest_dir = drawer_root + "/" + chest.at(drawer_sel).drawer_id;
            wc.console_write_success_green("Entering drawer directory '" + dest_dir + "'.");
            WibbleUtility::cd_helper(dest_dir);
        }
    }
    else
    {
        wc.console_write_error_red("Empty chest, nowhere to 'cd' into. Aborting");
    }
}

/**
* @brief Entirely delete a given drawer
* @param wc General WibbleConsole utility object
* @param drawer_num (Optional) Integer with specific override to select another drawer
*/
bool wibble::WibbleDrawer::delete_drawer(WibbleConsole& wc, const int drawer_num)
{
    try
    {
        if(chest.empty())
        {
           wc.console_write_error_red("Empty chest, no drawers to delete!");
           return false;
        }

        if(drawer_num > 0 && (unsigned long)drawer_num > chest.size())
        {
           wc.console_write_error_red("Invalid drawer number specified. Aborting");
           return false;
        }

        unsigned long drawer_sel;
        if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
        {
            drawer_sel = drawer_num - 1;
        }
        else
        {
            drawer_sel = def_drawer;
        }
        wc.console_write_yellow_bold("Pending deletion:\n");
        list_drawer(wc, drawer_sel + 1, false, true);
        std::string dr_id = chest.at(drawer_sel).drawer_id;
        std::string confirm_term = "DELETE " + dr_id.substr(15, dr_id.length());
        wc.console_write("");
        wc.console_write_red_bold("━━━━━━━━━━━━━━━━━━━━━ [ DELETE ] ━━━━━━━━━━━━━━━━━━━━━━\n");
        wc.console_write_red("WARNING. Entire drawer directory and ALL files will be\n");
        wc.console_write_red("permanently deleted. To proceed with deletion, input\n");
        wc.console_write_red("confirmation term.\n\n");
        wc.console_write_red_bold("Input '" + confirm_term + "' (without quotes)\n\n");
        std::string user_confirm = wc.console_write_prompt("> ", "", "");
        wc.trim(user_confirm);
        if(user_confirm == confirm_term)
        {
            auto del_complete = WibbleUtility::remove_or_update_line_from_file(cache_dir, wc, dr_id, drawer_index, false, "");
            const auto drawer_dir = drawer_root + "/" + chest.at(drawer_sel).drawer_id;
            auto del_num = std::filesystem::remove_all(drawer_dir);
            if(del_complete && del_num > 0)
            { wc.console_write_success_green("Drawer with ID '" + dr_id + "' deleted."); }
            else
            { wc.console_write_error_red("Unexpected error removing drawer with ID '" + dr_id + "'."); }


            if(chest.size() > 1)
            {
                // at this point, chest size is still +1 with deleted drawer
               if(chest.size() > 2)       { wc.console_write_success_green("Please select your new default drawer.");                   }
               else if(chest.size() == 2) { wc.console_write_success_green("Single remaining drawer has been automatically selected."); }
               chest.clear();
               _inventory_chest();
               auto valid_selection = false;
               while(valid_selection != true)
               {
                   valid_selection = select_drawer(wc, -1);
                   if(! valid_selection)
                   {
                       wc.console_write_error_red("Invalid selection. Please select a new default drawer.");
                   }
               }
           }
        }
        else
        {
           wc.console_write_error_red("Deletion operation aborted/User cancelled.\n");
        }
        return true;
    }
    catch(std::invalid_argument const &inv)
    {
        wc.console_write("");
        wc.console_write_error_red("Please input a valid integer for the drawer index");
        return false;
    }
}

/**
* @brief Entirely delete a given drawer
* @param wc General WibbleConsole utility object
* @param drawer_num (Optional) Integer with specific override to select another drawer
*/
bool wibble::WibbleDrawer::display_chest(WibbleConsole& wc)
{
    if(chest.empty())
    {
        wc.console_write_yellow_bold(ARW + "Empty chest. Add some drawers!\n");
        return false;
    }

    auto index = 0;
    auto width = std::to_string(chest.size()).length();

    wc.console_write("───────────────────────────────────────");
    for(const auto& drawer: chest)
    {
        if(index == def_drawer)
        {
            wc.console_write_green_bold("─────────── SELECTED DRAWER ───────────\n");
        }
        if(ENABLECOLOUR)
        {
            std::cout << rang::fgB::gray << rang::style::bold << rang::bgB::blue << "[";
            std::cout << std::setfill('0') << std::setw(width);
            std::cout << (index + 1) << "]" << rang::style::reset << " ┃ ";
        }
        else
        {
            std::cout << "[" << std::setfill('0') << std::setw(width);
            std::cout << (index + 1) << "]" << " ┃ ";
        }
        wc.console_write_yellow_bold("ID         : ");
        std::cout << drawer.drawer_id << '\n';
        std::cout << std::setfill(' ') << std::setw(width+7) << "┃ ";
        wc.console_write_yellow_bold("Description: ");
        wc.console_write(drawer.drawer_desc);
        if(index == def_drawer)
        {
            wc.console_write_green_bold("───────────────────────────────────────\n");
        }
        wc.console_write("───────────────────────────────────────");
        ++index;
    }
    return true;
}

/**
* @brief List contents of selected drawer
* @param wc General WibbleConsole utility object
* @param drawer_num (Optional) Integer with specific override to select another drawer
* @param interactive (Optional) Whether to display drawer selector
* @param tree_mode Whether to display tree rather than ls view
*/
bool wibble::WibbleDrawer::list_drawer(WibbleConsole& wc, const int drawer_num, const bool interactive, const bool tree_mode)
{
    if(chest.empty())
    {
       wc.console_write_error_red("Empty chest/no drawers. Nothing to list.");
       return false;
    }

    try
    {
        unsigned long drawer_sel;
        if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
        {
            drawer_sel = drawer_num;
        }
        else if(interactive)
        {

            display_chest(wc);
            std::string def_drawer_num = wc.console_write_prompt("\nChoose drawer to display: ", "", "");
            wc.trim(def_drawer_num);
            drawer_sel = std::stol(def_drawer_num);
            if((drawer_sel == 0) || drawer_sel > chest.size())
            {
                wc.console_write("");
                wc.console_write_error_red("Invalid drawer selection. Please enter a choice between '1' and '" +
                    std::to_string(chest.size()) + "'.");
                return false;
            }
        }
        else
        {
            drawer_sel = def_drawer + 1;
        }
        wc.console_write("");
        wc.console_write_yellow_bold(ARW + "Listing drawer contents for drawer '" +
                                     chest.at(drawer_sel - 1).drawer_id + "':\n\n");
        std::cout << std::flush;
        std::string drawer_path = drawer_root + "/" + chest.at(drawer_sel - 1).drawer_id;
        if(tree_mode) { std::system(("tree -Ca " + drawer_path).c_str());        }
        else          { std::system(("ls -laF --color " + drawer_path).c_str()); }
        wc.console_write_yellow_bold("\nSize: ");
        std::cout << std::flush;
        std::system(("du -hs " + drawer_path + " | cut -f 1 | tr -d '\n'").c_str());
        std::cout << '\n';
        return true;
    }
    catch(std::invalid_argument const &inv)
    {
        wc.console_write("");
        wc.console_write_error_red("Please input a valid integer for the drawer index");
        return false;
    }
}

/**
* @brief Allow user to specify selected drawer
* @param wc General WibbleConsole utility object
* @param drawer_num (Optional) Integer with specific override to select another drawer
*/
bool wibble::WibbleDrawer::select_drawer(WibbleConsole& wc, const int drawer_num)
{
    try
    {   display_chest(wc);
        unsigned long def_drawer;
        if(drawer_num >= 1 && (unsigned long)drawer_num <= chest.size())
        {
            def_drawer = drawer_num;
        }
        else if(chest.size() > 1)
        {
            std::string def_drawer_num = wc.console_write_prompt("\nSelect new default drawer: ", "", "");
            wc.trim(def_drawer_num);
            def_drawer = std::stol(def_drawer_num);
            if((def_drawer == 0) || def_drawer > chest.size())
            {
                wc.console_write_error_red("Invalid drawer selection. Please enter a choice between '1' and '" +
                    std::to_string(chest.size()) + "'.");
                return false;
            }
        }
        else
        {
            def_drawer = 1;
        }
        def_drawer = def_drawer - 1;

        std::stringstream chest_def{WibbleIO::read_file_into_stringstream(drawer_index)};
        std::ofstream new_chest_def;
        new_chest_def.open(drawer_index + ".new", std::ios::out | std::ios::trunc);
        std::string line;
        auto first_line = true;
        while(std::getline(chest_def, line))
        {
            if(first_line)
            {
                new_chest_def << def_drawer << '\n';
                first_line = false;
                continue;
            }

            new_chest_def << line << '\n';
        }
        // clean up
        std::stringstream().swap(chest_def);
        new_chest_def.close();

        // Overwrite with new index file
        WibbleUtility::create_backup_and_overwrite_file(cache_dir, drawer_index + ".new", drawer_index);
        if(chest.empty())
        {
           wc.console_write_yellow_bold("Chest is now empty. Add a drawer to it to push files.");
        }
        else
        {
            wc.console_write_yellow_bold("\nSelected drawer\n\n");
            wc.console_write_yellow_bold(ARW + "Drawer ID         : ");
            wc.console_write(chest.at(def_drawer).drawer_id);
            wc.console_write_yellow_bold(ARW + "Drawer Description: ");
            wc.console_write(chest.at(def_drawer).drawer_desc);
            wc.console_write_success_green("Selected drawer updated.");
        }
        return true;
    }
    catch(std::invalid_argument const &inv)
    {
        wc.console_write_error_red("Please input a valid integer for the drawer index");
        return false;
    }
    catch(std::exception const &err)
    {
        wc.console_write_error_red("Input/output error");
        return false;
    }
}

/**
* @brief Add a new drawer to chest
* @param wc General WibbleConsole utility object
*/
bool wibble::WibbleDrawer::add_drawer(WibbleConsole& wc)
{
    std::string drawer_desc = wc.console_write_prompt("Input drawer description/name: ", "", "");
    wc.trim(drawer_desc);
    if(drawer_desc == "")
    {
        wc.console_write_error_red("Drawer name cannot be blank. Aborting.");
        return false;
    }
    if(WibbleIO::create_path(drawer_root))
    {
        const auto drawer_hash = WibbleRecord::generate_entry_hash();
        const auto drawer_line = drawer_hash + "|" + WibbleRecord::remove_pipe(drawer_desc) + "\n";
        if(! std::filesystem::exists(drawer_index))
        {
            // initial/first run only, set default drawer line
            WibbleIO::write_out_file_append("0\n" + drawer_line, drawer_index);
        }
        else
        {
            WibbleIO::write_out_file_append(drawer_line, drawer_index);
        }
        return WibbleIO::create_path(drawer_root + "/" + drawer_hash);
    }
    else
    {
        wc.console_write_error_red("Unable to prepare drawer output path. Aborting.");
        return false;
    }
}

/**
* @brief Parse the drawer index to build our "chest" definition
*/
void wibble::WibbleDrawer::_inventory_chest()
{
    if(std::filesystem::exists(std::filesystem::path(drawer_index)))
    {
        std::stringstream chest_def{WibbleIO::read_file_into_stringstream(drawer_index)};
        std::string line;
        auto first_line = true;
        while(std::getline(chest_def, line))
        {
            if(first_line)
            {
                def_drawer = std::stoi(line);
                first_line = false;
                //std::cout << "default drawer index: " << def_drawer << "\n\n";
                continue;
            }

            std::size_t fp_sep = line.find(FIELD_DELIM);
            if(fp_sep != std::string::npos)
            {
                const auto drawer_id = line.substr(0, fp_sep);
                const auto drawer_desc = line.substr(fp_sep + 1, line.length() - 1 - fp_sep);
                drawer a_drawer { drawer_id, drawer_desc };
                chest.push_back(a_drawer);
            }
        }
        // clean up
        std::stringstream().swap(chest_def);
    }
}
