#!/bin/bash
# WIBBLE_VERSION: "2024-06-15 14:44:16"
# WIBBLE_DESC_START
#   Editing wrapper to quickly edit an individual file or
#   main Node index file
#   Use with 'wdir' extension.
# WIBBLE_DESC_END

CLI_EDIT="nvim"
GUI_EDIT="emacsclient -n"
# rename index.md and change this if you want org-mode, etc.
NODE=index.md

echo "1) Edit Node using CLI editor"
echo "2) Edit Node using GUI editor"
echo "3) Input filename to edit"
echo ""
read -p "Choice > " CHOICE

[[ "$CHOICE" == "1" ]] && $CLI_EDIT $NODE
[[ "$CHOICE" == "2" ]] && $GUI_EDIT $NODE

if [[ -f "./$CHOICE" ]]; then
    read -p "Command [ nvim ] > " EDITCMD
    [[ "$EDITCMD" == "" ]] && EDITCMD=nvim
    $EDITCMD $CHOICE
fi
