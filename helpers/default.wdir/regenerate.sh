#!/bin/bash
# WIBBLE_VERSION: "2024-06-15 14:44:16"
# WIBBLE_DESC_START
#   Generate preview documents with pandoc
#   Customise to suit
#   Use with 'wdir' extension.
# WIBBLE_DESC_END


echo "Generating HTML..."
pandoc -o index.html \
       -s --css=simple.css \
       --toc \
       -f gfm \
       --metadata title="Node" \
       index.md

echo "Generating PDF..."
pandoc -o index.pdf \
       -f gfm \
       -V geometry:margin=1in \
       -V papersize:A4 \
       -V colorlinks:true \
       index.md
