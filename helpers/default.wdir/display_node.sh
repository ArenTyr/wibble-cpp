#!/bin/bash
# WIBBLE_VERSION: "2024-06-15 14:44:16"
# WIBBLE_DESC_START
#   Display wrapper to customise [pre-]viewing options
#   Use with 'wdir' extension.
# WIBBLE_DESC_END

BROWSER=librewolf
CONVIEW=batcat
PDFVIEW=mupdf

echo "Got $# and $1"

if [[ "$#" -ne 1 ]]; then
    $CONVIEW index.md
elif [[ "$1" == "1" ]]; then
    $CONVIEW index.md
elif [[ "$1" == "2" ]]; then
    $BROWSER index.html > /dev/null 2>&1
elif [[ "$1" == "3" ]]; then
    $PDFVIEW index.pdf > /dev/null 2>&1
fi
