#!/usr/bin/env bash
# ---
# https://codeberg.org/ArenTyr/wibble-cpp
# Author: Aren Tyr <aren.t.developer.l7b06@8shield.net>
# Copyright (C) 2024 Noctilucent Development / Aren Tyr.
# GPL 3 licensed
# ---
# This script installs/sets up the Wibble++ aliases

set -eu

WIBDIR="$HOME/.config/wibble"
BINDIR="$HOME/.local/bin"

echo "This will add the Wibble++ aliases into your .bashrc file"
echo "and copy the helper scripts into ~/.local/bin"
echo 'You should ensure ~/.local/bin is present in your $PATH'
echo ""
read -p "Proceed? (y/n) > " CHOICE

if [[ $CHOICE == "y" || $CHOICE == "Y" ]]; then
    mkdir -p $WIBDIR
    echo "Adding aliases..."
    cp -v wibble-aliases.bash $WIBDIR
    echo " " >> ~/.bashrc
    echo '[[ -f "$HOME/.config/wibble-aliases.bash" ]] && source $HOME/.config/wibble-aliases.bash' >> "$HOME/.bashrc"
    echo "Adding helpers..."
    cp -v wibble-find $BINDIR
    cp -v wibble-fuzzy-wrapper $BINDIR
    cp -v wibble-topic-fuzzy-wrapper $BINDIR
    echo "Done"
    echo ""
    echo "To pick up/verify your new changes, open a new terminal window."
    echo "If all is setup correctly, you should be able to invoke wibble++"
    echo "simply via the command 'wib'"
fi   

