#!/usr/bin/perl
# ---
#
# Bookmark helper/navigator script; wbm = "(w)ibble bookark favourites".
#
# Fast and simple curated bookmark navigator for favourite websites/urls.
# Browser bookmarks are fine, but sometimes you want rapid-fire access from
# the console too...
#
# Requires:
#
# - Nothing other than standard Perl libraries.
#
# Usage:
#   1. Edit $bookmarks_master to point at your desired output file.
#   2. Add your favourite URLs + descriptions into bookmark files
#      inside your specified $bookmarks_lib directory. The bookmark files
#      should end in the file extension `.lst`. You can define/have as
#      many files as you like/need.
#   3. Lines starting with '#' (first character) are comments.
#      Lines starting with '=' (first character) are display section dividers.
#      Lines starting with '*' (first character) are executable/shell commands
#      All other lines should be valid paths to desintation directories.
#   4. Example:
#      # Wibble++ awesome websites
#      = Wibble++ development directories
#      https://codeberg.org/ArenTyr/wibble
#      https://codeberg.org/ArenTyr/wibble-cpp
#      # Build manual
#      *test -f ./build.sh && ./build.sh && cd out
#      # etc...
#   5. Run command:
#        - wba [no args] = display all bookmarks
#        - wba [s|section] <substring> = display bookmarks in section that matches <substring>
#        - wba [t|title] = display bookmarks with title that matches that matches <substring>
#        - wba [list] = display all section names/listing
# ---
# https://codeberg.org/ArenTyr/wibble-cpp
# Author: Aren Tyr <aren.t.developer.l7b06@8shield.net>
# Copyright (C) 2024 Noctilucent Development / Aren Tyr.
# GPL 3 licensed
# ---

use strict;
use warnings;
use 5.010;
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;

# ==== ENVIRONMENT ====
# Edit the two values below to suitable locations
# Favourited list of directories, one per line, comment lines with '#',
# separator lines with '='
my $bookmarks_lib = "/path/to/wbm";
my $bookmarks_master = "/path/to/bm.lst";
# Edit this to your desired web browser command
my $browser_cmd = "librewolf";
# ==== [ END EDIT ] ===

my @marks;
my @adds;
my @bm_seps;
my @fl_seps;
my @sep_idx;

# nicely format numbers for vertical alignment
sub padded_number
{
    my ($total_num, $num_to_pad) = @_;
    my $zeropad = (length $total_num);
    return sprintf("%0${zeropad}d", $num_to_pad);
}

my $filter_sec = "";
my $filter_tit = "";
my $list_only = 0;

if(scalar @ARGV >= 1)
{
    if($ARGV[0] eq "refresh")
    {
        print colored("→ Regenerating bookmark cache.\n", 'bright_green', 'bold');
        print colored("→ Bookmarks master file   : ", 'bright_yellow');
        print colored("$bookmarks_master\n", 'grey20', 'bold');
        print colored("→ Gathering bookmarks from: ", 'bright_yellow');
        print colored("$bookmarks_lib\n", 'grey20', 'bold');
        my $res = system("cd $bookmarks_lib && cat *.lst > $bookmarks_master");
        if($res == 0) { print colored("→ Bookmarks regenerated successfully.\n", 'bright_green', 'bold'); }
        exit(0); 
    }
    elsif($ARGV[0] eq "config")
    {
        print colored("→ Displaying configuration:\n\n", 'bright_green', 'bold');
        print colored("Browser command      : ", 'bright_yellow', 'bold');
        print colored("$browser_cmd\n", 'grey20', 'bold');
        print colored("Bookmarks master file: ", 'bright_yellow', 'bold');
        print colored("$bookmarks_master\n", 'grey20', 'bold');
        exit(0); 
    }
    elsif($ARGV[0] eq "list")
    {
        print colored("→ Displaying sections:\n", 'bright_green', 'bold');
        $list_only = 1;
    }
    elsif($ARGV[0] eq "sections" || $ARGV[0] eq "s")
    {
        shift @ARGV;
        $filter_sec = join " ", @ARGV;
        print colored("→ Section filter in effect: $filter_sec\n\n", 'bright_green', 'bold');
    }
    elsif($ARGV[0] eq "title" || $ARGV[0] eq "t")
    {
        shift @ARGV;
        $filter_tit = join " ", @ARGV;
        print colored("→ Title filter in effect: ", 'bright_green', 'bold');
        print colored("$filter_tit\n\n", 'bright_yellow', 'bold');
    }
    else
    {
        print colored("→ Unknown command.\n→ Valid commands: [ refresh | config | list | [s]ection | [t]itle ].\n", 'bright_red', 'bold');
        exit(0); 
    }
}
else
{
    print colored("→ Displaying all bookmarks\n\n", 'bright_green', 'bold');
}


# parse the bookmarks file
open(my $FH, '<', $bookmarks_master) or die $!;
my $entries = 0;
my $eat_section = 0;
my $do_add = 0;
my $fltr_header = 0;

while(<$FH>)
{
   if(/^#/)      { $do_add = 0; next; }
   elsif(/^\s$/) { $do_add = 0; next; }
   elsif(/^=/)
   {
       $do_add = 0; 
       $eat_section = 0;
       push @bm_seps, substr $_, 2;
       push @sep_idx, $entries;

       # handle case of active section filter in effect
       if($filter_sec ne "" && /$filter_sec/i)
       {
           $eat_section = 1;
           $fltr_header = 1;
           push @fl_seps, substr $_, 2;
           next;
       }
   }
   elsif($filter_sec eq "" && $filter_tit eq "")
   {
       $do_add = 1;
   }
   elsif($filter_sec ne "")
   {
       if($eat_section == 1) { $do_add = 1; }
   }
   elsif($filter_tit ne "")
   {
       if(/$filter_tit/i)    { $do_add = 1; }
   }

   if($do_add == 1)
   {
       ++$entries;
       my @bm_entry = split("\\|", $_);
       my $url = $bm_entry[0];
       my $title = $bm_entry[1];
       chomp($url);
       chomp($title);
       push @adds, $url;
       push @marks, $title;
       $do_add = 0;
   }
}
close($FH);

# just for displaying section list only
if($list_only == 1)
{
    print "\n";
    foreach(@bm_seps) { print "- "; print colored("$_", 'bright_yellow', 'bold'); }
    exit(0); 
}

# case of no matches as result of filtering
if(scalar @marks == 0)
{
    print colored("→ No matches. Exiting.\n", 'bright_red', 'bold');
    exit(0); 
}

# output the list to screen
my $i = 0;
my $sep_iter = 0;

print colored("━━━━━━━━━━━━━━━ [ BOOKMARKS ] ━━━━━━━━━━━━━\n", 'bright_green', 'bold');
foreach(@marks)
{
    # for heading lines starting with =, kept in separate indexed array
    if($fltr_header == 1 && ((scalar @fl_seps) > 0 && (scalar @sep_idx) > $sep_iter && $i == $sep_idx[$sep_iter]))
    {
        print "\n"; print colored("$fl_seps[$sep_iter]\n", , 'bright_yellow', 'bold');
        ++$sep_iter; 
    }
    elsif((scalar @bm_seps) > 0 && (scalar @sep_idx) > $sep_iter && $i == $sep_idx[$sep_iter])
    { print "\n"; print colored("$bm_seps[$sep_iter]\n", 'bright_yellow', 'bold'); ++$sep_iter;  }

    my $y = padded_number(scalar @marks, $i + 1);
    print colored("[$y]", 'bright_white', 'on_bright_blue', 'bold');
    if (substr($adds[$i],0,1) eq "*")
    {
        my $cmd = substr($adds[$i],1);
        print colored(" CMD: $cmd\n", 'ansi214');
    }
    else { print " $_\n"; }
    
    ++$i;
}
print colored("\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━", 'bright_green', 'bold');
print colored("\n", 'bright_yellow', 'bold'); print " ";
print colored("> ", 'bright_yellow', 'bold'); print " ";
my $choice = <STDIN>;
chomp($choice);

# select and open the bookmark using the configured web browser command
if(looks_like_number($choice) && $choice >= 1 && $choice <= (scalar @adds))
{
    my $sel = $adds[$choice-1];
    my $chk = substr($sel,0,1);  
    # is it a command rather than directory?
    if ($chk eq "*")
    { 
        my $cmd = substr($sel,1);
        print colored("→ Executing command: '$cmd'\n", 'bright_green', 'bold');
        system("$cmd");
    }
    else
    {
        print colored("→ Opening URL: $sel\n", 'bright_green', 'bold');
        system("$browser_cmd $sel");
    }
}
