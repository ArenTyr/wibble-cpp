#!/usr/bin/perl
# ---
#
# Directory helper/navigator script; cdf = "cd (f)avourites".
#
# Fast and simple curated directory navigator for favourite directories.
# Scripts like z are good, but sometimes you just want an explicit
# curated list of common directories you cd into all the time.
#
# Requires:
#
# 1. Nothing other than standard Perl libraries.
#
# Usage:
#
# A) Setup
#   1. Edit the two values in the "ENVIRONMENT" section below.
#   2. Add the following values into your .bashrc file. DIRVAR
#      must point at the location you set for $ch_dir below. It is
#      used to store environment variable of directory to cd into.
#    ---
#     export CDF='/home'
#     DIRVAR="/$HOME/path/to/current_dir.wib"
#    ---
#   3. Now add following alias function into .bashrc:
#    ---
#     _cdf()
#     {
#         cdf  "$@" && source $DIRVAR && cd $CDF && pwd
#     }
#     alias cdd="_cdf"
#    ---
#   4. chmod+x, then simply run 'cdf' or any alias of the command (I use 'cdd')
#   5. Of course you could put all of this in a separate file and then source
#      it from .bashrc.
# B) Usage
#   1. Edit your 'fav_dirs.lst' file (pointed at by $favourite_dir variable)
#      and add your favourite directories.
#   2. Lines starting with '#' (first character) are comments.
#      Lines starting with '=' (first character) are display section dividers.
#      Lines starting with '*' (first character) are shell commands.
#      All other lines should be valid paths to desintation directories.
#   3. Example:
#      # Development dirs
#      = Wibble++ development directories
#      /home/aren/store/coll/src/codeberg/wibble-cpp
#      # etc...
#      = Backup directories
#      /mnt/archive/aren
#      # etc...
#   4. Run command:
#        - cdd [no args] = display all directories
#        - cdd [s|section] <substring> = display directories in section that matches <substring>
#        - cdd [p|path] = display directories with path that matches that matches <substring>
#        - cdd [list] = display all section names/listing
# ---
# https://codeberg.org/ArenTyr/wibble-cpp
# Author: Aren Tyr <aren.t.developer.l7b06@8shield.net>
# Copyright (C) 2024 Noctilucent Development / Aren Tyr.
# GPL 3 licensed
# ---

use strict;
use warnings;
use 5.010;
use Scalar::Util qw(looks_like_number);
use Term::ANSIColor;

# ==== ENVIRONMENT ====
# Edit the two values below to suitable locations
# Favourited list of directories, one per line, comment lines with '#',
# separator lines with '=', executable lines with '*'
my $favourite_dirs = "/path/to/.config/fav_dirs.lst";
# For persisting the new environment variable value *into*
my $ch_file = "/path/to/current_dir.wib";
# ==== [ END EDIT ] ===

my @dirs;
my @dir_seps;
my @fl_seps;
my @sep_idx;

# nicely format numbers for vertical alignment
sub padded_number
{
    my ($total_num, $num_to_pad) = @_;
    my $zeropad = (length $total_num);
    return sprintf("%0${zeropad}d", $num_to_pad);
}


my $filter_sec = "";
my $filter_pat = "";
my $list_only = 0;

if(scalar @ARGV >= 1)
{
    if($ARGV[0] eq "list")
    {
        print colored("→ Displaying sections:\n", 'bright_green', 'bold');
        $list_only = 1;
    }
    elsif($ARGV[0] eq "sections" || $ARGV[0] eq "s")
    {
        shift @ARGV;
        $filter_sec = join " ", @ARGV;
        print colored("→ Section filter in effect: $filter_sec\n\n", 'bright_green', 'bold');
    }
    elsif($ARGV[0] eq "path" || $ARGV[0] eq "p")
    {
        shift @ARGV;
        $filter_pat = join " ", @ARGV;
        print colored("→ Path/filename filter in effect: ", 'bright_green', 'bold');
        print colored("$filter_pat\n\n", 'bright_yellow', 'bold');
    }
    else
    {
        print colored("→ Unknown command.\n→ Valid commands: [ list | [s]ection | [p]ath ].\n", 'bright_red', 'bold');
        system("echo \"CDF='.'\" > $ch_file");
        exit(0); 
    }
}

# parse the favourite dirs file
open(my $FH, '<', $favourite_dirs) or die $!;
my $entries = 0;
my $eat_section = 0;
my $do_add = 0;
my $fltr_header = 0;

while(<$FH>)
{
   if(/^#/)      { $do_add = 0; next; }
   elsif(/^\s$/) { $do_add = 0; next; }
   elsif(/^=/)
   {
       $do_add = 0; 
       $eat_section = 0;
       push @dir_seps, substr $_, 2;
       push @sep_idx, $entries;

       # handle case of active section filter in effect
       if($filter_sec ne "" && /$filter_sec/i)
       {
           $eat_section = 1;
           $fltr_header = 1;
           push @fl_seps, substr $_, 2;
           next;
       }
   }
   elsif($filter_sec eq "" && $filter_pat eq "")
   {
       $do_add = 1;
   }
   elsif($filter_sec ne "")
   {
       if($eat_section == 1) { $do_add = 1; }
   }
   elsif($filter_pat ne "")
   {
       if(/$filter_pat/i)    { $do_add = 1; }
   }

   if($do_add == 1)
   {
       ++$entries;
       my $this_dir = $_;
       chomp($this_dir);
       push @dirs, $this_dir;
       $do_add = 0;
   }
}
close($FH);

# just for displaying section list only
if($list_only == 1)
{
    print "\n";
    foreach(@dir_seps) { print "- "; print colored("$_", 'bright_yellow', 'bold'); }
    system("echo \"CDF='.'\" > $ch_file");
    exit(0); 
}

# case of no matches as result of filtering
if(scalar @dirs == 0)
{
    print colored("→ No matches. Exiting.\n", 'bright_red', 'bold');
    system("echo \"CDF='.'\" > $ch_file");
    exit(0); 
}

# output the list to screen
my $i = 0;
my $sep_iter = 0;
print colored("━━━━━━━ [ FAVOURITE DIRECTORIES ] ━━━━━━━\n", 'bright_green', 'bold');
foreach(@dirs)
{
    # For heading lines starting with =, kept in separate indexed array
    if($fltr_header == 1 && ((scalar @fl_seps) > 0 && (scalar @sep_idx) > $sep_iter && $i == $sep_idx[$sep_iter]))
    {
        print "\n"; print colored("$fl_seps[$sep_iter]\n", , 'bright_yellow', 'bold');
        ++$sep_iter; 
    }
    elsif((scalar @dir_seps) > 0 && (scalar @sep_idx) > $sep_iter && $i == $sep_idx[$sep_iter])
    { print "\n"; print colored("$dir_seps[$sep_iter]\n", 'bright_yellow', 'bold'); ++$sep_iter; }

    my $y = padded_number(scalar @dirs, $i + 1);
    print colored("[$y]", 'bright_white', 'on_bright_blue', 'bold');
    if (substr($_,0,1) eq "*")
    {
        my $cmd = substr($_,1);
        print colored(" CMD: $cmd\n", 'ansi214');
    }
    else { print " $_\n" };
    ++$i;
}
print colored("\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━", 'bright_green', 'bold');
print colored("\n", 'bright_yellow', 'bold'); print " ";
print colored("> ", 'bright_yellow', 'bold'); print " ";
my $choice = <STDIN>;
chomp($choice);

# Put the user selected directory into environment variable inside temporary
# file, can then be sourced by parent shell in alias command
if(looks_like_number($choice) && $choice >= 1 && $choice <= (scalar @dirs))
{
    my $sel = $dirs[$choice-1];
    my $chk = substr($sel,0,1);  
    # is it a command rather than directory?
    if ($chk eq "*")
    { 
        my $cmd = substr($sel,1);
        print colored("→ Executing command: '$cmd'\n", 'bright_green', 'bold');
        system("$cmd");
        # ensure we remain in same directory
        system("echo \"CDF='.'\" > $ch_file");
    }
    else
    {
        my $env_var = "CDF='$dirs[$choice-1]'";
        system("echo \"$env_var\" > $ch_file");
    }
}
else # no match, so just stay in current directory
{
    system("echo \"CDF='.'\" > $ch_file");
}
