# Use with:
# [[ -f $HOME/.config/wibble/wibble-aliases.bash ]] && source $HOME/.config/wibble/wibble-aliases.bash
# ---
# https://codeberg.org/ArenTyr/wibble-cpp
# Author: Aren Tyr <aren.t.developer.l7b06@8shield.net>
# Copyright (C) 2024 Noctilucent Development / Aren Tyr.
# GPL 3 licensed
# ---
# ====== [BEGIN] wibble aliases ========================================================
export CDF="/home"
DIRVAR="$HOME/.config/wibble/current_dir.wib"
export WIB_DIR="."
WIB_CD="$HOME/.config/wibble/current_dir.wib"
_cdf()
{
    cdf  "$@" && source $DIRVAR && cd $CDF && pwd
}
_wib_cd_helper()
{
    [[ -f $WIB_CD ]] && source $WIB_CD && \
        [[ $WIB_DIR != "." ]] && \
        pushd $WIB_DIR && \
        echo "-> Entered '$WIB_DIR'. popd to go back."
}
_wib_helper()
{
    wibble++ "$@"
    _wib_cd_helper
}
_wib_fuzzy_helper()
{
    wibble-fuzzy-wrapper
    _wib_cd_helper
}
_wib_topic_fuzzy_helper()
{
    wibble-topic-fuzzy-wrapper
    _wib_cd_helper
}
_wib_find_helper()
{
    wibble-find "$@"
    _wib_cd_helper
}
alias cdd="_cdf"
alias wib="_wib_helper"
alias wf="_wib_find_helper"
alias wn="_wib_fuzzy_helper"
alias wt="_wib_topic_fuzzy_helper"
alias wibble="_wib_helper"
alias rm="wibble janitor --scrub"
alias del="/usr/bin/rm"
alias empty="wibble janitor --obliterate"
alias trash="wibble janitor --list"
# ====== [END  ] wibble aliases ========================================================
